PatLib
==============================

This is a generic c++ library with all kinds of functionality for use in my projects. It is meant to complement the standard library and boost. You might find something interesting if you look around. It is mostly header only.

I have integrated some external libraries, with some with modifications:

*  [array_view](https://github.com/wardw/array_view)
*  [termcolor](https://github.com/ikalnytskyi/termcolor)
*  [sse mathfun](https://github.com/RJVB/sse_mathfun)
*  [avx mathfun](http://software-lisc.fbk.eu/avx_mathfun/)
*  [Vectorized PCG32](https://github.com/wjakob/pcg32)

### Installing

At least a c++20 compiler with a few c++23 features ( auto(), if consteval ) is required to use it.

Required libraries:

*  [boost](https://www.boost.org/)
*  [range-v3](https://github.com/ericniebler/range-v3)
*  [Shared Access](https://gitlab.com/patlefort/sharedaccess)
*  [yamc](https://github.com/yohhoy/yamc)

Optional libraries:

*  [libhugetlbfs](https://github.com/libhugetlbfs/libhugetlbfs): For huge pages support.
*  [fmt](https://fmt.dev/)

```sh
cmake <source dir>
cmake --build .
sudo cmake --install .
```

### Usage

CMake integration:

```cmake
find_package(PatLib REQUIRED)
target_link_libraries(my_target PRIVATE PatLib::All)
```
