/*
	PatLib

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "patlib/base.hpp"
#include "patlib/accumulator.hpp"
#include "patlib/parallel/algorithm.hpp"
#include "patlib/array_view.hpp"
#include "patlib/asserter.hpp"
#include "patlib/back_buffered.hpp"
#include "patlib/masked_container_array.hpp"
#include "patlib/stable_block_vector.hpp"
#include "patlib/bitset.hpp"
#include "patlib/box.hpp"
#include "patlib/chrono.hpp"
#include "patlib/concepts.hpp"
#include "patlib/clone.hpp"
#include "patlib/cpu.hpp"
#include "patlib/crtp.hpp"
#include "patlib/endian.hpp"
#include "patlib/exception.hpp"
#include "patlib/factory.hpp"
#include "patlib/io.hpp"
#include "patlib/iterator.hpp"
#include "patlib/log.hpp"
#include "patlib/math.hpp"
#include "patlib/matrix.hpp"
#include "patlib/memory.hpp"
#include "patlib/marked_optional.hpp"
#include "patlib/memory_system.hpp"
#include "patlib/mutex.hpp"
#include "patlib/multi_tagged_span.hpp"
#include "patlib/omp.hpp"
#include "patlib/path.hpp"
#include "patlib/pcg32.hpp"
#include "patlib/pointer.hpp"
#include "patlib/range.hpp"
#include "patlib/rectangle.hpp"
#include "patlib/scoped.hpp"
#include "patlib/string.hpp"
#include "patlib/utility.hpp"
#include "patlib/task.hpp"
#include "patlib/term_color.hpp"
#include "patlib/thread_one_master.hpp"
#include "patlib/thread.hpp"
#include "patlib/type.hpp"
#include "patlib/variant.hpp"
#include "patlib/simd/all.hpp"

#ifdef PL_NUMA
	#include "patlib/numa.hpp"
#endif
