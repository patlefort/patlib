/*
	PatLib

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "base.hpp"
#include "thread.hpp"
#include "exception.hpp"
#include "algorithm.hpp"
#include "type.hpp"
#include "range.hpp"
#include "concepts.hpp"
#include "chrono.hpp"
#include "omp.hpp"

#include <utility>
#include <future>
#include <functional>
#include <exception>
#include <chrono>
#include <atomic>
#include <vector>
#include <mutex>
#include <condition_variable>
#include <algorithm>
#include <forward_list>
#include <memory>

#include <boost/lockfree/queue.hpp>
#include <boost/atomic.hpp>

#include <range/v3/algorithm.hpp>
#include <range/v3/view.hpp>

#include <shared_access/shared.hpp>

namespace patlib::task
{
	using namespace std::chrono_literals;

	namespace impl
	{
		void call_fc(auto &&f, std::promise<void> &p, auto&&... args)
		{
			std::invoke(PL_FWD(f), PL_FWD(args)...);
			p.set_value();
		}

		template <typename T_>
		void call_fc(auto &&f, std::promise<T_> &p, auto&&... args)
		{
			p.set_value(std::invoke(PL_FWD(f), PL_FWD(args)...));
		}

		template <typename T_>
		[[nodiscard]] bool is_ready(const std::future<T_> &f)
		{
			return !f.valid() || f.wait_for(0s) == std::future_status::ready;
		}

		template <CDurationOrTimePoint Time_>
		[[nodiscard]] std::chrono::steady_clock::time_point set_time(const Time_ &time)
		{
			if constexpr(CTimePoint<Time_>)
				return time;
			else
				return std::chrono::steady_clock::now() + time;
		}

	} // namespace impl

	template <typename T_>
	struct task_handle
	{
	public:
		std::future<T_> future;
		std::function<bool(const std::future<T_> &)> cancel_fc;
		std::function<bool(const std::future<T_> &, std::chrono::steady_clock::time_point)> reschedule_fc;

		bool cancel()
		{
			if(!cancel_fc)
				return false;

			return cancel_fc(future);
		}

		bool reschedule(const auto &t)
		{
			if(!reschedule_fc)
				return false;

			return reschedule_fc(future, impl::set_time(t));
		}
	};

	template <typename T_>
	constinit const bool is_task_handle = false;
	template <typename T_>
	constinit const bool is_task_handle<task_handle<T_>> = true;
	template <typename T_>
	concept CTaskHandle = is_task_handle<T_>;

	class PATLIB_API TaskException : public Exception
	{
	public:
		using Exception::Exception;

		[[nodiscard]] virtual const char *module() const noexcept override;
	};

	class PATLIB_API Scheduler final : public ThreadBase
	{
	private:

		struct queued_task
		{
			struct task_attributes
			{
				bool cancelled = false, running = false;
				std::chrono::steady_clock::time_point time;
			};

			std::shared_ptr<shared_access::shared<task_attributes>> attribs;
			std::function<void()> fc, destroyPromise;

			queued_task(
				std::chrono::steady_clock::time_point time,
				std::function<void()> fc_,
				std::function<void()> destroyPromise_) noexcept :
					attribs{std::make_shared<shared_access::shared<task_attributes>>(shared_access::in_place, false, false, time)},
					fc{std::move(fc_)},
					destroyPromise{std::move(destroyPromise_)}
				{}
		};

		using queue_type = boost::lockfree::queue<queued_task *>;
		using size_type = typename queue_type::size_type;

		queue_type mQueue;
		std::forward_list<queued_task *> mRunningTasks;
		std::vector<queued_task *> mPushBack;
		mutable std::condition_variable mQueueEmptyWait, mQueueCond;
		mutable std::mutex mQueueCondMutex;
		std::atomic<bool> mReadyToEnd{false};

		std::chrono::milliseconds mSleepDur{100};

		class [[nodiscard]] scoped_task_group : no_copy
		{
		private:
			#ifdef PL_OPENMP_TASKS
				std::atomic_flag mExceptSet;
				std::exception_ptr mExcept;
			#endif

			Scheduler &mScheduler;

		public:

			scoped_task_group(Scheduler &sched) : mScheduler{sched} {}
			~scoped_task_group()
				#ifdef PL_OPENMP_TASKS
					noexcept(false)
				#endif
			{
				#ifdef PL_OPENMP_TASKS
					mScheduler.task_wait();

					if(mExcept)
					{
						try { std::rethrow_exception(mExcept); }
						catch(...)
						{
							std::destroy_at(&mExcept);
							throw;
						}
					}
				#endif
			}

			auto &run(std::invocable auto fc, [[maybe_unused]] bool finalIf = false, [[maybe_unused]] bool doIf = true)
			{
				#ifndef PL_OPENMP_TASKS
					fc();
				#else
					mScheduler.light_task([fc = std::move(fc), this]
						{
							try
							{
								fc();
							} catch(...) {
								if(!mExceptSet.test_and_set(std::memory_order::acquire))
									mExcept = std::current_exception();
							}
						}, finalIf, doIf);
				#endif

				return *this;
			}
		};

	public:

		Scheduler(size_type size = 50);
		~Scheduler();

		virtual void pause() final;
		virtual void end() final;

		void sleep_duration(std::chrono::milliseconds dur) noexcept { mSleepDur = dur; }
		[[nodiscard]] auto sleep_duration() const noexcept { return mSleepDur; }

		#ifdef _OPENMP

			[[nodiscard]] int thread_limit() const noexcept
				{ return (int)omp_get_thread_limit(); }

			[[nodiscard]] int max_threads() const noexcept
				{ return (int)omp_get_max_threads(); }

			[[nodiscard]] int thread_num() const noexcept
				{ return (int)omp_get_thread_num(); }

			[[nodiscard]] int thread_level() const noexcept
				{ return (int)omp_get_level(); }

		#else

			[[nodiscard]] int thread_limit() const noexcept
				{ return (int)std::thread::hardware_concurrency(); }

			[[nodiscard]] int max_threads() const noexcept
				{ return (int)std::thread::hardware_concurrency(); }

			[[nodiscard]] int thread_num() const noexcept
				{ return 0; }

			[[nodiscard]] int thread_level() const noexcept
				{ return 0; }

		#endif

		#ifdef PL_OPENMP_TASKS

			void task_wait() const noexcept
			{
				#pragma omp taskwait
			}

			template <rg::input_range R_>
			void task_wait(R_ &&tasks) const
				requires CTaskHandle<rg::range_value_t<R_>>
			{
				#pragma omp taskwait

				for(auto &t : tasks)
					if(t.future.valid())
						t.future.get();
			}

		#else

			void task_wait(...) const noexcept {}

		#endif

		void task_group(std::invocable<scoped_task_group &> auto&& fc, [[maybe_unused]] size_type nbThreads = 0)
		{
			#ifndef PL_OPENMP_TASKS
				scoped_task_group tg{*this};

				PL_FWD(fc)(tg);
			#else

				if(omp_in_parallel())
				{
					#pragma omp taskgroup
					{
						{
							scoped_task_group tg{*this};

							PL_FWD(fc)(tg);
						}
					}
				}
				else
				{
					nbThreads = !nbThreads ? (size_type)omp_get_max_threads() : nbThreads;
					#pragma omp parallel num_threads(nbThreads)
					{
						#pragma omp single nowait
						{
							scoped_task_group tg{*this};

							PL_FWD(fc)(tg);
						}
					}
				}

			#endif
		}

		void sub_tasks(auto&&... args)
		{
			task_group([&](auto &tg){
				(tg.run(PL_FWD(args), false), ...);
			});
		}

		void light_task(std::invocable auto fc, [[maybe_unused]] bool finalIf = false, [[maybe_unused]] bool doIf = true)
		{
			#ifndef PL_OPENMP_TASKS
				fc();
			#else
				#pragma omp task firstprivate(fc) if(doIf) final(finalIf)
				fc();
			#endif
		}

		template <typename FC_, typename... Args_, CDurationOrTimePoint Time_ = std::chrono::milliseconds>
		auto push(FC_ fc, [[maybe_unused]] const Time_ &time = 0ms, Args_&&... args)
			requires std::invocable<FC_, Args_...>
		{
			using RetType = std::invoke_result_t<FC_, Args_...>;

			// clang openmp implementation doesn't support OMP_WAIT_POLICY=passive
			#ifndef PL_OPENMP_TASKS

				task_handle<RetType> handle{
					std::async(std::launch::async, std::move(fc), PL_FWD(args)...),
					[](const auto &/*future*/){ return false; },
					[](const auto &/*future*/, const CDurationOrTimePoint auto &){ return false; }
				};

			#else

				if(!started())
					start();

				auto promise = std::make_unique<std::promise<RetType>>();

				auto t = std::make_unique<queued_task>(
					impl::set_time(time),

					[this, fc = std::move(fc), promise = promise.get(), ...args = PL_FWD(args)]() mutable
					{
						try
						{
							impl::call_fc(fc, *promise, unwrap_ref(args)...);
						}
						catch(...)
						{
							promise->set_exception(std::current_exception());
						}

						delete promise;
					},

					[promise = promise.get()]{ delete promise; }
				);

				task_handle<RetType> handle{
					promise->get_future(),

					[this, attribs = t->attribs](const auto &future)
					{
						if(impl::is_ready(future))
							return false;

						return *attribs | shared_access::mode::excl |
							[&](auto &attribs)
							{
								if(attribs.running)
									return false;

								attribs.cancelled = true;
								return true;
							};
					},

					[this, attribs = t->attribs](const auto &future, const CDurationOrTimePoint auto &newTime)
					{
						if(impl::is_ready(future))
							return false;

						return *attribs | shared_access::mode::excl |
							[&](auto &attribs)
							{
								if(attribs.running || attribs.cancelled)
									return false;

								attribs.time = impl::set_time(newTime);
								return true;
							};
					}
				};

				if(!mQueue.bounded_push(t.get()))
					throw TaskException("Failed to push task into queue.");

				t.release();
				promise.release();

				mQueueCond.notify_all();

			#endif

			return handle;
		}

		decltype(auto) critical(std::invocable auto&& fc) const
		{
			#ifndef PL_OPENMP_TASKS
				return PL_FWD(fc)();
			#else

				using RetType = decltype(PL_FWD(fc)());

				if constexpr(std::is_void_v<RetType>)
				{
					#pragma omp critical
					PL_FWD(fc)();
				}
				else
				{
					RetType ret;
					#pragma omp critical
					{
						ret = PL_FWD(fc)();
					}

					return PL_FWD_RETURN(ret);
				}

			#endif
		}

		void yield() const noexcept
		{
			#pragma omp taskyield
		}

	protected:

		virtual bool prepare_to_end() final;

	private:

		virtual void process_loop() final;

	};

	class scoped_scheduler
	{
	public:
		scoped_scheduler(Scheduler &scheduler) : mScheduler{scheduler} {}

		~scoped_scheduler()
		{
			if(mScheduler.started())
			{
				mScheduler.end();
				mScheduler.join();
			}
		}

	private:
		Scheduler &mScheduler;
	};

	namespace impl
	{
		PL_CONST_FC( [[nodiscard]] Scheduler PATLIB_API &task_scheduler() );
	}
} // namespace patlib::task

namespace patlib
{
	// Globally accessible task system.
	inline auto &tasks = task::impl::task_scheduler();

	#ifdef PL_OPENMP_TASKS

		namespace impl_task_parallel
		{
			struct parallel_taskqueue_t : public impl_algo::basic_chunked_parallel_policy<parallel_taskqueue_t> {};
			struct parallel_taskqueue_or_parallel_t : public impl_algo::basic_chunked_parallel_policy<parallel_taskqueue_or_parallel_t> {};
			struct parallel_taskqueue_one_t : public impl_algo::basic_chunked_parallel_policy<parallel_taskqueue_one_t> {};

			[[nodiscard]] constexpr auto to_parallel_policy(const parallel_taskqueue_or_parallel_t &pol) noexcept
			{
				return policy::parallel_t{}.nb_threads(pol.nb_threads()).chunk_size(pol.chunk_size());
			}

			[[nodiscard]] constexpr auto to_task_queue_policy(const parallel_taskqueue_or_parallel_t &pol) noexcept
			{
				return parallel_taskqueue_t{}.nb_threads(pol.nb_threads()).chunk_size(pol.chunk_size());
			}

			void task_range_loop_one(auto&& r, auto&& fc)
			{
				tasks.task_group([&](auto &tg)
				{
					for(auto&& v : PL_FWD(r))
						tg.run([&fc, v = capturer{PL_FWD(v)}]{ PL_FWD(fc)(std::get<0>(v.vars)); });
				});
			}

			void task_range_loop(auto&& r, const parallel_taskqueue_t &pol, auto&& fc)
			{
				if(const auto chunkSize = pol.chunk_size(); chunkSize)
				{
					#pragma omp taskloop grainsize(chunkSize)
					for(auto&& v : PL_FWD(r))
						PL_FWD(fc)(PL_FWD(v));
				}
				else
				{
					#pragma omp taskloop
					for(auto&& v : PL_FWD(r))
						PL_FWD(fc)(PL_FWD(v));
				}
			}

			template <typename SizeT_>
			void task_loop(const SizeT_ size, const parallel_taskqueue_t &pol, auto&& fc)
			{
				if(const auto chunkSize = pol.chunk_size(); chunkSize)
				{
					#pragma omp taskloop grainsize(chunkSize)
					for(SizeT_ i=0; i<size; ++i)
						PL_FWD(fc)(i);
				}
				else
				{
					#pragma omp taskloop
					for(SizeT_ i=0; i<size; ++i)
						PL_FWD(fc)(i);
				}
			}

			void tag_invoke(tag_t<impl_algo::for_each>, CRandomAccessValueRange auto&& r, auto fc, auto proj, const parallel_taskqueue_t &pol)
				requires(
					rg::sized_range<decltype(r)> &&
					CIndirectlyInvocable<decltype(fc), rg::projected<rg::iterator_t<decltype(r)>, decltype(proj)>>)
			{
				const auto nbThreads = impl_algo::select_nb_threads(pol);

				const auto loopImpl = [&](auto&& catchingFc)
					{
						maybe_parallel_region(nbThreads, [&]{
								task_range_loop(PL_FWD(r), pol, [&](auto&& v){ PL_FWD(catchingFc)([&]{ std::invoke(fc, std::invoke(proj, PL_FWD(v))); }); });
							});
					};

				impl_algo::throwing_parallel_loop(loopImpl);
			}

			void tag_invoke(tag_t<impl_algo::for_each>, rg::input_range auto&& r, auto fc, auto proj, const parallel_taskqueue_one_t &pol)
				requires(
					CIndirectlyInvocable<decltype(fc), rg::projected<rg::iterator_t<decltype(r)>, decltype(proj)>>)
			{
				const auto nbThreads = impl_algo::select_nb_threads(pol);

				const auto loopImpl = [&](auto&& catchingFc)
					{
						maybe_parallel_region(nbThreads, [&]{
								task_range_loop_one(PL_FWD(r), [&](auto&& v){ PL_FWD(catchingFc)([&]{ std::invoke(fc, std::invoke(proj, PL_FWD(v))); }); });
							});
					};

				impl_algo::throwing_parallel_loop(loopImpl);
			}

			void tag_invoke(tag_t<impl_algo::for_each>, CTuple auto&& r, auto fc, auto proj, const parallel_taskqueue_one_t &pol)
			{
				const auto nbThreads = impl_algo::select_nb_threads(pol);

				maybe_parallel_region(nbThreads, [&]
				{
					tasks.task_group([&](auto &tg)
					{
						tuple_for_each(PL_FWD(r), [&](auto&& v){
							tg.run([&]{ std::invoke(fc, std::invoke(proj, PL_FWD(v))); });
						});
					});
				});
			}

			void tag_invoke(tag_t<impl_algo::for_each>, auto&& r, auto fc, auto proj, const parallel_taskqueue_or_parallel_t &pol)
			{
				if(omp_in_parallel())
					impl_algo::for_each(PL_FWD(r), std::move(fc), std::move(proj), to_task_queue_policy(pol));
				else
					impl_algo::for_each(PL_FWD(r), std::move(fc), std::move(proj), to_parallel_policy(pol));
			}

			void tag_invoke(tag_t<impl_algo::transform>, CRandomAccessValueRange auto&& r, CRandomAccessValueIterator auto into, auto fc, auto proj, const parallel_taskqueue_t &pol)
				requires(
					rg::sized_range<decltype(r)> &&
					CIndirectlyInvocable<decltype(fc), rg::projected<rg::iterator_t<decltype(r)>, decltype(proj)>> &&
					COutputValueIterator<decltype(into), rg::indirect_result_t<decltype(fc), rg::projected<rg::iterator_t<decltype(r)>, decltype(proj)>>>)
			{
				const auto nbThreads = impl_algo::select_nb_threads(pol);
				const auto nb = rg::size(PL_FWD(r));
				auto begin = PL_FWD(r).begin();

				const auto loopImpl = [&](auto&& catchingFc)
					{
						maybe_parallel_region(nbThreads, [&]
						{
							task_loop(nb, pol, [&](const auto i){
								PL_FWD(catchingFc)([&]{ *(into + i) = std::invoke(fc, std::invoke(proj, *(begin + i))); });
							});
						});
					};

				impl_algo::throwing_parallel_loop(loopImpl);
			}

			void tag_invoke(tag_t<impl_algo::transform>, rg::input_range auto&& r, auto into, auto fc, auto proj, const parallel_taskqueue_one_t &pol)
				requires(
					CIndirectlyInvocable<decltype(fc), rg::projected<rg::iterator_t<decltype(r)>, decltype(proj)>> &&
					COutputValueIterator<decltype(into), rg::indirect_result_t<decltype(fc), rg::projected<rg::iterator_t<decltype(r)>, decltype(proj)>>>)
			{
				const auto nbThreads = impl_algo::select_nb_threads(pol);

				const auto loopImpl = [&](auto&& catchingFc)
					{
						maybe_parallel_region(nbThreads, [&]
						{
							tasks.task_group([&](auto &tg)
							{
								for(auto&& v : PL_FWD(r))
									tg.run([&catchingFc, &fc, &proj, v = capturer{PL_FWD(v)}, into = into++]{ PL_FWD(catchingFc)([&]{ *into = std::invoke(fc, std::invoke(proj, std::get<0>(v.vars))); }); });
							});
						});
					};

				impl_algo::throwing_parallel_loop(loopImpl);
			}

			void tag_invoke(tag_t<impl_algo::transform>, CTuple auto&& r, auto fc, auto proj, const parallel_taskqueue_one_t &pol)
			{
				const auto nbThreads = impl_algo::select_nb_threads(pol);

				maybe_parallel_region(nbThreads, [&]
				{
					tasks.task_group([&](auto &tg)
					{
						tuple_for_each(PL_FWD(r), [&](auto&& v){
							tg.run([&]{ PL_FWD(v) = std::invoke(fc, std::invoke(proj, PL_FWD(v))); });
						});
					});
				});
			}

			void tag_invoke(tag_t<impl_algo::transform>, auto&& r, auto into, auto fc, auto proj, const parallel_taskqueue_or_parallel_t &pol)
			{
				if(omp_in_parallel())
					impl_algo::transform(PL_FWD(r), std::move(into), std::move(fc), std::move(proj), to_task_queue_policy(pol));
				else
					impl_algo::transform(PL_FWD(r), std::move(into), std::move(fc), std::move(proj), to_parallel_policy(pol));
			}

			void tag_invoke(tag_t<impl_algo::transform>, rg::random_access_range auto&& r1, rg::random_access_range auto&& r2, CRandomAccessValueIterator auto into, auto fc, auto proj1, auto proj2, const parallel_taskqueue_t &pol)
				requires(
					rg::sized_range<decltype(r1)> && rg::sized_range<decltype(r2)> &&
					CIndirectlyInvocable<decltype(fc), rg::projected<rg::iterator_t<decltype(r1)>, decltype(proj1)>, rg::projected<rg::iterator_t<decltype(r2)>, decltype(proj2)>> &&
					COutputValueIterator<decltype(into), rg::indirect_result_t<decltype(fc), rg::projected<rg::iterator_t<decltype(r1)>, decltype(proj1)>, rg::projected<rg::iterator_t<decltype(r2)>, decltype(proj2)>>>)
			{
				const auto nbThreads = impl_algo::select_nb_threads(pol);
				const auto nb = std::min<std::common_type_t<rg::range_size_t<decltype(r1)>, rg::range_size_t<decltype(r2)>>>(rg::size(PL_FWD(r1)), rg::size(PL_FWD(r2)));
				auto begin = PL_FWD(r1).begin();
				auto first2 = PL_FWD(r2).begin();

				const auto loopImpl = [&](auto&& catchingFc)
					{
						maybe_parallel_region(nbThreads, [&]
						{
							task_loop(nb, pol, [&](const auto i){
								PL_FWD(catchingFc)([&]{ *(into + i) = std::invoke(fc, std::invoke(proj1, *(begin + i)), std::invoke(proj2, *(first2 + i))); });
							});
						});
					};

				impl_algo::throwing_parallel_loop(loopImpl);
			}

			void tag_invoke(tag_t<impl_algo::transform>, rg::input_range auto&& r1, rg::input_range auto&& r2, auto into, auto fc, auto proj1, auto proj2, const parallel_taskqueue_one_t &pol)
				requires(
					CIndirectlyInvocable<decltype(fc), rg::projected<rg::iterator_t<decltype(r1)>, decltype(proj1)>, rg::projected<rg::iterator_t<decltype(r2)>, decltype(proj2)>> &&
					COutputValueIterator<decltype(into), rg::indirect_result_t<decltype(fc), rg::projected<rg::iterator_t<decltype(r1)>, decltype(proj1)>, rg::projected<rg::iterator_t<decltype(r2)>, decltype(proj2)>>>)
			{
				const auto nbThreads = impl_algo::select_nb_threads(pol);

				const auto loopImpl = [&](auto&& catchingFc)
					{
						maybe_parallel_region(nbThreads, [&]
						{
							tasks.task_group([&](auto &tg)
							{
								for(auto&& v : rgv::zip(PL_FWD(r1), PL_FWD(r2)))
									tg.run([&catchingFc, &fc, &proj1, &proj2, v, into = into++]{
										PL_FWD(catchingFc)([&]{
											*into = std::invoke(fc, std::invoke(proj1, std::get<0>(v)), std::invoke(proj2, std::get<1>(v)));
										});
									});
							});
						});
					};

				impl_algo::throwing_parallel_loop(loopImpl);
			}

			void tag_invoke(tag_t<impl_algo::transform>, auto&& r1, auto&& r2, auto into, auto fc, auto proj1, auto proj2, const parallel_taskqueue_or_parallel_t &pol)
			{
				if(omp_in_parallel())
					impl_algo::transform(PL_FWD(r1), PL_FWD(r2), into, std::move(fc), std::move(proj1), std::move(proj2), to_task_queue_policy(pol));
				else
					impl_algo::transform(PL_FWD(r1), PL_FWD(r2), into, std::move(fc), std::move(proj1), std::move(proj2), to_parallel_policy(pol));
			}

			void tag_invoke(tag_t<impl_algo::copy>, CRandomAccessValueRange auto&& r, CRandomAccessValueIterator auto into, const parallel_taskqueue_t &pol)
				requires(
					rg::sized_range<decltype(r)> &&
					COutputValueIterator<decltype(into), rg::range_reference_t<decltype(r)>>)
			{
				const auto nbThreads = impl_algo::select_nb_threads(pol);
				const auto nb = rg::size(PL_FWD(r));
				auto begin = PL_FWD(r).begin();

				const auto loopImpl = [&](auto&& catchingFc)
					{
						maybe_parallel_region(nbThreads, [&]
						{
							task_loop(nb, pol, [&](const auto i){
								PL_FWD(catchingFc)([&]{ *(into + i) = *(begin + i); });
							});
						});
					};

				impl_algo::throwing_parallel_loop(loopImpl);
			}

			void tag_invoke(tag_t<impl_algo::copy>, rg::input_range auto&& r, auto into, const parallel_taskqueue_one_t &pol)
				requires(
					rg::sized_range<decltype(r)> &&
					COutputValueIterator<decltype(into), rg::range_reference_t<decltype(r)>>)
			{
				const auto nbThreads = impl_algo::select_nb_threads(pol);

				const auto loopImpl = [&](auto&& catchingFc)
					{
						maybe_parallel_region(nbThreads, [&]
						{
							tasks.task_group([&](auto &tg)
							{
								for(auto&& v : PL_FWD(r))
									tg.run([&catchingFc, v = capturer{PL_FWD(v)}, into = into++]{ PL_FWD(catchingFc)([&]{ *into = PL_FWD(std::get<0>(v.vars)); }); });
							});
						});
					};

				impl_algo::throwing_parallel_loop(loopImpl);
			}

			void tag_invoke(tag_t<impl_algo::copy>, auto&& r, auto into, const parallel_taskqueue_or_parallel_t &pol)
			{
				if(omp_in_parallel())
					impl_algo::copy(PL_FWD(r), std::move(into), to_task_queue_policy(pol));
				else
					impl_algo::copy(PL_FWD(r), std::move(into), to_parallel_policy(pol));
			}
		} // namespace impl_task_parallel

		namespace policy
		{
			using impl_task_parallel::parallel_taskqueue_t;
			using impl_task_parallel::parallel_taskqueue_or_parallel_t;
			using impl_task_parallel::parallel_taskqueue_one_t;

			inline constexpr auto parallel_taskqueue = parallel_taskqueue_t{};
			inline constexpr auto parallel_taskqueue_one = parallel_taskqueue_one_t{};
			inline constexpr auto parallel_taskqueue_or_parallel = parallel_taskqueue_or_parallel_t{};
		} // namespace policy
	#else

		namespace policy
		{
			using parallel_taskqueue_t = parallel_t;
			using parallel_taskqueue_or_parallel_t = parallel_t;
			using parallel_taskqueue_one_t = parallel_t;

			inline constexpr auto parallel_taskqueue = parallel_taskqueue_t{};
			inline constexpr auto parallel_taskqueue_one = parallel_taskqueue_one_t{}.chunk_size(1);
			inline constexpr auto parallel_taskqueue_or_parallel = parallel_taskqueue_or_parallel_t{};
		} // namespace policy

	#endif
}
