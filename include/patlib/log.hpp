/*
	PatLib

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "base.hpp"
#include "type.hpp"
#include "pointer.hpp"
#include "term_color.hpp"
#include "utility.hpp"
#include "io.hpp"

#include <iostream>
#include <iomanip>
#include <type_traits>
#include <string_view>
#include <mutex>

#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/device/back_inserter.hpp>
#include <boost/hana/config.hpp>
#include <boost/hana/at_key.hpp>
#include <boost/hana/map.hpp>

#ifdef PL_FMT
	#include <fmt/format.h>

	#define PL_LIB_LOG_FMT
#endif

namespace patlib
{
	namespace logtag
	{
		struct log {};

		PL_MAKE_DERIVED_HANA_TAG(message, log)
		PL_MAKE_DERIVED_HANA_TAG(error,   log)
	}

	template <typename T_> concept CLogTag = std::derived_from<T_, logtag::log>;

	template <CLogTag Category_>
	class ILogger
	{
	public:
		virtual ~ILogger() = default;

		using category = Category_;

		virtual std::ostream &log_start(std::string_view moduleName, std::ostream &os) = 0;
		virtual void log_end(std::ostream &os) = 0;
	};

	template <CLogTag Category_>
	class VoidLogger : public ILogger<Category_>
	{
	private:
		boost::iostreams::stream<boost::iostreams::null_sink> mNullStream{ boost::iostreams::null_sink{} };

	public:

		virtual std::ostream &log_start(std::string_view /*moduleName*/, std::ostream &) override { return mNullStream; }
		virtual void log_end(std::ostream &) override {}
	};

	template <CLogTag Category_>
	class StdLogger : public ILogger<Category_>
	{
	private:
		static constexpr std::ostream *select_os()
		{
			if constexpr(std::is_same_v<Category_, logtag::error_t>)
				return &std::cerr;

			return &std::cout;
		}

	public:
		std::ostream *mOs = select_os();

		void stream(std::ostream *os) noexcept { mOs = os; }
		[[nodiscard]] auto &stream() const noexcept { return *mOs; }

		virtual std::ostream &log_start(std::string_view moduleName, std::ostream &os) override
		{
			if(termcolor::is_colorized(*mOs))
				os << termcolor::colorize;

			if constexpr(std::is_same_v<Category_, logtag::error_t>)
				os << termcolor::red <<
					'[' << termcolor::bold << termcolor::green << termcolor::on_grey << std::left << std::setfill(' ') << std::setw(20) <<
						moduleName <<
					termcolor::reset << termcolor::red << termcolor::on_grey << "] ";
			else
				os <<
					'[' << termcolor::bold << termcolor::green << termcolor::on_grey << std::left << std::setfill(' ') << std::setw(20) <<
						moduleName <<
					termcolor::reset << "] ";

			return *mOs;
		}

		virtual void log_end(std::ostream &os) override
		{
			os << termcolor::reset;
		}
	};

	namespace log_impl
	{
		template <typename Mutex_, CLogTag Category_>
		class [[nodiscard]] scoped_sync_log
		{
		private:
			ILogger<Category_> *logger;
			Mutex_ &mMut;
			std::ostream *mOs;

		#ifndef _WIN32
			static inline thread_local
		#endif
				std::string mBuf;

			using DevType = boost::iostreams::back_insert_device<std::string>;
			boost::iostreams::stream<DevType> mOsBuf{DevType{mBuf}, 0, 0};

		public:

			scoped_sync_log(std::string_view moduleName, ILogger<Category_> &log_, Mutex_ &mut_, const std::locale &loc) :
				logger{&log_}, mMut{mut_}
			{
				mBuf.clear();
				mOsBuf.imbue(loc);
				mOs = &logger->log_start(moduleName, mOsBuf);
			}

			~scoped_sync_log()
			{
				logger->log_end(mOsBuf);

				std::scoped_lock lg{mMut};
				*mOs << mBuf << std::endl;
			}

			[[nodiscard]] auto &stream() noexcept { return mOsBuf; }
			[[nodiscard]] const auto &buffer() const noexcept { return mBuf; }
			[[nodiscard]] auto &buffer() noexcept { return mBuf; }

			template <typename T_>
			friend scoped_sync_log &operator<<(scoped_sync_log &sl, const T_ &o)
				{ sl.mOsBuf << o; return sl; }

			template <typename T_>
			friend scoped_sync_log &&operator<<(scoped_sync_log &&sl, const T_ &o)
				{ std::move(sl).mOsBuf << o; return std::move(sl); }
		};
	} // namespace log_impl

	template <CLogTag... Tags_>
	class sync_logger
	{
	private:
		mutable std::mutex mutex;
		std::locale mLoc;

	public:
		using log_map_type = hn::map<
			hn::pair<hn::type<Tags_>, ILogger<Tags_> *>...
		>;

		sync_logger()
			: mLoc{io::try_get_locale("")}
		{}

		template <typename... T_>
		sync_logger(T_... logs) : sync_logger()
		{
			( (loggers[hn::first(logs)] = hn::second(logs)) , ... );
		}

		log_map_type loggers;

		template <CLogTag Category_>
		void set_logger(ILogger<Category_> &l)
		{
			loggers[hn::type_c<Category_>] = &l;
		}

		void set_locale(std::locale loc)
		{
			mLoc = std::move(loc);
		}

		template <CLogTag Category_>
		[[nodiscard]] auto log(std::string_view moduleName, const hn::basic_type<Category_> tag) const
		{
			return log_impl::scoped_sync_log<decltype(mutex), Category_>{moduleName, *assert_valid(loggers[tag]), mutex, mLoc};
		}

		#ifdef PL_FMT
			template <CLogTag Category_, typename... Fargs_>
			void log(std::string_view moduleName, const hn::basic_type<Category_> tag, fmt::format_string<Fargs_...> format, Fargs_&&... args) const
			{
				auto l = log_impl::scoped_sync_log<decltype(mutex), Category_>{moduleName, *assert_valid(loggers[tag]), mutex, mLoc};
				l.buffer().append(fmt::format(mLoc, format, PL_FWD(args)...));
			}
		#endif
	};

	using global_logger_type = sync_logger<logtag::message_t, logtag::error_t>;

	namespace log_impl
	{
		PL_CONST_FC( [[nodiscard]] global_logger_type PATLIB_API &get_logger() );
		PATLIB_API void set_standard_loggers();
	}

	inline global_logger_type &logger = log_impl::get_logger();

	template <CLogTag Category_ = logtag::message_t>
	[[nodiscard]] auto log(std::string_view moduleName, const hn::basic_type<Category_> tag = {})
		{ return logger.log(moduleName, tag); }

	#ifdef PL_FMT
		template <CLogTag Category_, typename... Fargs_>
		void log(std::string_view moduleName, const hn::basic_type<Category_> tag, fmt::format_string<Fargs_...> format, Fargs_&&... args)
			{ return logger.log(moduleName, tag, format, PL_FWD(args)...); }

		template <typename... Fargs_>
		void log(std::string_view moduleName, fmt::format_string<Fargs_...> format, Fargs_&&... args)
			{ return logger.log(moduleName, logtag::message, format, PL_FWD(args)...); }
	#endif


	struct [[nodiscard]] scoped_logger_guard
	{
	private:
		global_logger_type::log_map_type mLoggerBackup{logger.loggers};

	public:
		template <typename... Loggers_>
		scoped_logger_guard(Loggers_&... loggers)
		{
			( logger.set_logger(loggers), ... );
		}
		~scoped_logger_guard() { logger.loggers = mLoggerBackup; }

	};
}
