/*
	PatLib

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "base.hpp"
#include "math.hpp"
#include "simd/algorithm.hpp"
#include "type.hpp"
#include "matrix.hpp"
#include "io.hpp"
#include "utility.hpp"

#include <type_traits>
#include <utility>
#include <initializer_list>

#include <range/v3/range/concepts.hpp>
#include <range/v3/range/traits.hpp>
#include <range/v3/numeric/accumulate.hpp>

#ifdef PL_FMT
	#include <fmt/format.h>
#endif

namespace patlib
{
	namespace box_impl
	{
		using namespace simd::vector_op;

		template <simd::CNeutralOrPointGeometricVector VT_>
		struct box
		{
			using point_type = VT_;
			using dir_type = simd::rebind_to_dir<VT_>;

			static constexpr auto NbAxis = VT_::category::NbAxis;

			VT_ v1, v2;

			box() = default;
			box(std::initializer_list<zero_init_t>) noexcept { set_zero(); }
			box(const tag::zero_t) noexcept { set_zero(); }
			box(const tag::half_t c) noexcept : v1{c}, v2{c} {}
			box(const tag::one_t c) noexcept : v1{c}, v2{c} {}
			box(const tag::two_t c) noexcept : v1{c}, v2{c} {}
			box(const tag::nan_t c) noexcept requires CNumberPackFloat<VT_> { set_nan(); }
			box(const tag::infhighest_t) noexcept { set_infmax(); }
			box(const tag::inflowest_t) noexcept { set_inflowest(); }
			box(const VT_ v2_) noexcept
			{
				set({}, v2_);
			}
			box(const VT_ v1_, const auto v2_) noexcept
			{
				set(v1_, v2_);
			}
			box(const tag::range_init_c auto, rg::input_range auto &&r) noexcept
				requires std::convertible_to<rg::range_value_t<std::decay_t<decltype(r)>>, VT_>
			{
				*this = rg::accumulate(PL_FWD(r), box{tag::inflowest}, [](const auto &init, auto &&v){
						return init.join_point(PL_FWD(v));
					});
			}

			box(const box &other) = default;

			template <typename OVT_>
			explicit(!std::is_convertible_v<OVT_, VT_>) box(const box<OVT_> &other) noexcept :
				v1{static_cast<OVT_>(other.v1)}, v2{static_cast<OVT_>(other.v2)} {}

			void set(const VT_ v1_, const VT_ v2_) noexcept
			{
				v1 = v1_;
				v2 = v2_;
			}

			void set(const VT_ v1_, const size_p<dir_type> v2_) noexcept
			{
				v1 = v1_;
				v2 = v1_ + *v2_;
			}

			void set_zero() noexcept
			{
				v1.set_identity();
				v2.set_identity();
			}

			void set_nan() noexcept
			{
				v1.set_nan();
				v2.set_nan();
			}

			void set_infmax() noexcept
			{
				v1 = constants::inflowest<VT_>.as_point();
				v2 = constants::infhighest<VT_>.as_point();
			}

			void set_inflowest() noexcept
			{
				v1 = constants::infhighest<VT_>.as_point();
				v2 = constants::inflowest<VT_>.as_point();
			}

			[[nodiscard]] bool is_infinite() const noexcept requires CNumberPackFloat<VT_> { return mask_any((v1/oisinf) | (v2/oisinf)); }
			[[nodiscard]] bool is_nan() const noexcept requires CNumberPackFloat<VT_> { return mask_any((v1/oisnan) | (v2/oisnan)); }
			[[nodiscard]] bool is_normal() const noexcept requires CNumberPackFloat<VT_> { return mask_all((v1/oisnormal) & (v2/oisnormal)); }
			[[nodiscard]] bool is_valid() const noexcept { return true; }
			[[nodiscard]] bool is_valid() const noexcept requires CNumberPackFloat<VT_> { return mask_none((v1/oisinf) | (v2/oisinf) | (v1/oisnan) | (v2/oisnan)); }

			[[nodiscard]] szt largest_axis() const
			{
				const auto d = dims();
				return std::distance(d.begin(), rg::max_element(d | rgv::take_exactly(NbAxis)));
			}

			[[nodiscard]] szt smallest_axis() const
			{
				const auto d = dims();
				return std::distance(d.begin(), rg::min_element(d | rgv::take_exactly(NbAxis)));
			}

			[[nodiscard]] auto dims() const noexcept { return (v2 - v1).as_dir(); }
			[[nodiscard]] auto middle() const noexcept { return plmidpoint(v1, v2); }

			[[nodiscard]] auto area() const noexcept
			{
				if constexpr(NbAxis >= 3)
				{
					auto d = dims();

					return d /odot/ (d/orotate) * 2;
				}
				else if constexpr(NbAxis == 2)
				{
					auto d = dims();

					return d[0] * d[1];
				}
				else
					return dims().scalar();
			}

			[[nodiscard]] box clip(const box &b) const noexcept
				{ return { v1 /omax/ b.v1, v2 /omin/ b.v2 }; }

			[[nodiscard]] VT_ clip_vertex(VT_ v) const noexcept
				{ return v1 /omax/ v /omin/ v2; }

			[[nodiscard]] box join(const box &b) const noexcept
				{ return { v1 /omin/ b.v1, v2 /omax/ b.v2 }; }

			[[nodiscard]] box join_point(VT_ v) const noexcept
				{ return { v1 /omin/ v, v2 /omax/ v }; }

			[[nodiscard]] bool overlap(const box &b) const noexcept
				{ return simd::mask_axis(type_v<VT_>, (v1 <= b.v2) & (v2 >= b.v1)).all(); }

			[[nodiscard]] bool overlap(VT_ p) const noexcept
				{ return simd::mask_axis(type_v<VT_>, (p < v1) | (p > v2)).none(); }

			[[nodiscard]] bool fully_inside(const box &b) const noexcept
				{ return simd::mask_axis(type_v<VT_>, (b.v2 < v1) | (b.v1 > v2)).none(); }

			[[nodiscard]] constexpr bool is_zero_area() const noexcept
				{ return simd::mask_axis(type_v<VT_>, v2 == v1).any(); }

			[[nodiscard]] auto reorder() const noexcept
			{
				box nb{*this};
				condswap(nb.v1, nb.v2, nb.v2 < nb.v1);

				return nb;
			}

			[[nodiscard]] auto minimal_safe_increment() const noexcept
			{
				return (v1/oabs /omax/ (v2/oabs)).minimal_safe_increment();
			}

			[[nodiscard]] auto transform(const CMatrix4x4Transform<VT_> auto &matrix) const noexcept
			{
				std::array<VT_, 8> points;
				const auto &va1 = simd::vector_access(v1);
				const auto &va2 = simd::vector_access(v2);

				points[0] = v1;
				points[1] = v1;
				points[1].set(va2[0], size_v<0>);
				points[2] = v1;
				points[2].set(va2[0], size_v<0>);
				points[2].set(va2[1], size_v<1>);
				points[3] = v1;
				points[3].set(va2[1], size_v<1>);

				points[4] = v2;
				points[5] = v2;
				points[5].set(va1[0], size_v<0>);
				points[6] = v2;
				points[6].set(va1[0], size_v<0>);
				points[6].set(va1[1], size_v<1>);
				points[7] = v2;
				points[7].set(va1[1], size_v<1>);

				for(auto &v : points)
					v = matrix * normalize_point_p{v};

				return box{tag::range_init, points};
			}

			[[nodiscard]] auto operator*(const CMatrix4x4Transform<VT_> auto &matrix) const noexcept
				{ return transform(matrix); }

			[[nodiscard]] friend bool isnan(const box &v) noexcept
				requires CNumberPackFloat<VT_>
				{ return v.is_nan(); }

			[[nodiscard]] friend bool isinf(const box &v) noexcept
				requires CNumberPackFloat<VT_>
				{ return v.is_infinite(); }

			[[nodiscard]] friend bool isnormal(const box &v) noexcept
				requires CNumberPackFloat<VT_>
				{ return v.is_normal(); }

			[[nodiscard]] friend constexpr bool operator==(const box &lv, const box &rv) noexcept
				{ return simd::mask_axis(type_v<VT_>, (lv.v1 == rv.v1) & (lv.v2 == rv.v2)).all(); }

			[[nodiscard]] constexpr auto operator<=>(const box &rv) const noexcept
			{
				if(const auto cmp = v1 <=> rv.v1; cmp != 0)
					return cmp;

				return v2 <=> rv.v2;
			}

			template <typename CharT_, typename Traits_>
			friend auto &operator<<(std::basic_ostream<CharT_, Traits_> &os, const box &b)
			{
				os << b.v1 << ' ' << b.v2;

				return os;
			}

			template <typename CharT_, typename Traits_>
			friend auto &operator<=(std::basic_ostream<CharT_, Traits_> &os, const box &b)
			{
				using namespace io::rawio;

				os <= b.v1 <= b.v2;

				return os;
			}

			template <typename CharT_, typename Traits_>
			friend auto &operator>>(std::basic_istream<CharT_, Traits_> &is, box &b)
			{
				VT_ v1, v2;

				is >> v1 >> v2;
				b.set(v1, v2);

				return is;
			}

			template <typename CharT_, typename Traits_>
			friend auto &operator>=(std::basic_istream<CharT_, Traits_> &is, box &b)
			{
				using namespace io::rawio;

				VT_ v1, v2;

				is >= v1 >= v2;
				b.set(v1, v2);

				return is;
			}

			// For marked_optional default policy.
			static inline auto constexpr MarkedOptionalInitialValue = tag::nan;
			constexpr void reset() noexcept
					requires CNumberPackFloat<VT_>
				{ v1.set_nan(); }
			[[nodiscard]] constexpr bool has_value() const noexcept
					requires CNumberPackFloat<VT_>
				{ return mask_none(v1 /oisnan); }
		};
	} // namespace box_impl

	using box_impl::box;
}

#ifdef PL_FMT
namespace fmt
{
	template <typename T_, typename Char_>
	struct formatter<::patlib::box<T_>, Char_> : private formatter<typename ::patlib::box<T_>::point_type>
	{
		using base_formatter_type = formatter<typename ::patlib::box<T_>::point_type>;

		constexpr auto parse(format_parse_context& ctx)
			{ return base_formatter_type::parse(ctx); }

  	auto format(const ::patlib::box<T_> &p, auto &ctx) const
		{
			static constexpr char sep[] = " to ";

			ctx.advance_to(base_formatter_type::format(p.v1, ctx));
			ctx.advance_to(std::copy(std::begin(sep), std::end(sep), ctx.out()));
			return base_formatter_type::format(p.v2, ctx);
		}
	};
}
#endif
