/*
	PatLib

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "factory_fwd.hpp"

#include "base.hpp"
#include "type.hpp"
#include "memory.hpp"
#include "clone.hpp"

#include <type_traits>
#include <utility>
#include <memory>
#include <cstddef>
#include <functional>

#include <memory_resource>

namespace patlib
{
	template<typename ObjectType_, typename Alloc_ = std::allocator<ObjectType_>>
	struct smart_factory
	{
		using object_type = ObjectType_;

		uninitialized_value<Alloc_> mAlloc{Alloc_{}};

		template <typename... Args_>
		[[nodiscard]] auto create_shared(Args_&&... args)
		{
			return std::allocate_shared<ObjectType_>(*mAlloc, PL_FWD(args)...);
		}

		template <typename... Args_>
		[[nodiscard]] auto create_shared_inplace(void *, Args_&&... args)
		{
			return std::allocate_shared<ObjectType_>(*mAlloc, PL_FWD(args)...);
		}

		template <typename InterfaceType_, typename... Args_>
		[[nodiscard]] auto create_unique(type_c<InterfaceType_>, Args_&&... args)
		{
			const auto ptr = mAlloc->allocate(1);
			UniquePtr<InterfaceType_> uPtr;

			try
			{
				std::uninitialized_construct_using_allocator(ptr, *mAlloc, PL_FWD(args)...);

				if constexpr(std::same_as<InterfaceType_, ObjectType_>)
				{
					uPtr = {
						ptr,
						[this](InterfaceType_ *p){
							std::allocator_traits<Alloc_>::destroy(*mAlloc, p);
							mAlloc->deallocate(p, 1);
						}
					};
				}
				else
				{
					uPtr = {
						static_cast<InterfaceType_ *>(ptr),
						[this, ptr](InterfaceType_ *){
							std::allocator_traits<Alloc_>::destroy(*mAlloc, ptr);
							mAlloc->deallocate(ptr, 1);
						}
					};
				}
			}
			catch(...)
			{
				mAlloc->deallocate(ptr, 1);
				throw;
			}

			return uPtr;
		}

	};

	struct size_align
	{
		szt size, align;
	};

	template<typename ObjectInterface_>
	class IGenericFactory
	{
	public:
		virtual ~IGenericFactory() = default;

		using ObjectInterfaceType = std::remove_pointer_t<ObjectInterface_>;

		[[nodiscard]] virtual ObjectInterface_ create() = 0;
		virtual ObjectInterface_ create(void *ptr) = 0;
		virtual void destroy(ObjectInterface_ ptr) = 0;
		[[nodiscard]] virtual size_align storage_info() const noexcept = 0;
	};

	template<typename ObjectInterface_>
	class IGenericSmartFactory : public virtual IGenericFactory<ObjectInterface_ *>
	{
	public:
		virtual ~IGenericSmartFactory() = default;

		using typename IGenericFactory<ObjectInterface_ *>::ObjectInterfaceType;

		[[nodiscard]] virtual std::shared_ptr<ObjectInterface_> create_shared() = 0;
		[[nodiscard]] virtual UniquePtr<ObjectInterface_> create_unique() = 0;
	};

	class IMemoryResourceConsumer
	{
	public:
		virtual ~IMemoryResourceConsumer() = default;

		[[nodiscard]] virtual std::pmr::memory_resource *memory_resource() const = 0;
		virtual void memory_resource(std::pmr::memory_resource *res) = 0;
	};

	template<typename ObjectInterface_, typename ObjectType_, typename Alloc_ = std::allocator<ObjectType_>>
	class GenericFactoryBase : public virtual IGenericFactory<ObjectInterface_>
	{
	public:

		GenericFactoryBase(const Alloc_ &alloc = {})
			: mFactory{alloc} {}

		using object_type = ObjectType_;
		using pointer_type = typename std::allocator_traits<Alloc_>::pointer;
		using void_pointer_type = typename std::allocator_traits<Alloc_>::void_pointer;

		[[nodiscard]] pointer_type create_direct()
		{
			const auto ptr = mFactory.mAlloc->allocate(1);

			try
			{
				construct(ptr);
			}
			catch(...)
			{
				mFactory.mAlloc->deallocate(ptr, 1);
				throw;
			}

			return ptr;
		}

		void destroy_direct(pointer_type p)
		{
			std::allocator_traits<Alloc_>::destroy(*mFactory.mAlloc, std::to_address(p));
			mFactory.mAlloc->deallocate(p, 1);
		}

		[[nodiscard]] virtual ObjectInterface_ create() override
		{
			return create_direct();
		}

		virtual ObjectInterface_ create(void *ptr) override
		{
			const auto iptr = pointer_static_cast<ObjectType_>(ptr);
			construct(iptr);
			return iptr;
		}

		virtual void destroy(ObjectInterface_ ptr) override
		{
			const auto p = pointer_static_cast<ObjectType_>(ptr);
			destroy_direct(p);
		}

		[[nodiscard]] auto allocator() const noexcept { return *mFactory.mAlloc; }
		void allocator(const Alloc_ &a) { mFactory.mAlloc.emplace(a); }

		[[nodiscard]] virtual size_align storage_info() const noexcept final
			{ return {sizeof(ObjectType_), alignof(ObjectType_)}; }

		template <typename... Args_>
		[[nodiscard]] auto construct_shared(Args_&&... args)
		{
			return mFactory.create_shared(PL_FWD(args)...);
		}

		template <typename... Args_>
		[[nodiscard]] auto construct_unique(Args_&&... args)
		{
			return mFactory.create_unique(PL_FWD(args)...);
		}

	protected:
		smart_factory<ObjectType_, Alloc_> mFactory;

		template <typename... Args_>
		void construct(pointer_type ptr, Args_&&... args)
		{
			std::allocator_traits<Alloc_>::construct(*mFactory.mAlloc, std::to_address(ptr), PL_FWD(args)...);
		}

	};

	template <template<typename, typename, typename> typename Factory_, typename ObjectInterface_, typename ObjectType_>
	class GenericFactoryPolymorphicAllocator :
		public Factory_<ObjectInterface_, ObjectType_, std::pmr::polymorphic_allocator<ObjectType_>>,
		public virtual IMemoryResourceConsumer
	{
	public:

		[[nodiscard]] virtual std::pmr::memory_resource *memory_resource() const override { return this->allocator().resource(); }
		virtual void memory_resource(std::pmr::memory_resource *res) override { this->allocator({res}); }
	};

	template<typename ObjectInterface_, typename ObjectType_, typename Alloc_ = std::allocator<ObjectType_>>
	class GenericFactory : public GenericFactoryBase<ObjectInterface_, ObjectType_, Alloc_> {};

	template<typename ObjectInterface_, typename ObjectType_, typename Alloc_ = std::allocator<ObjectType_>>
	class GenericSmartFactoryAllocBase : public GenericFactoryBase<ObjectInterface_ *, ObjectType_, Alloc_>, public virtual IGenericSmartFactory<ObjectInterface_>
	{
	public:
		virtual ~GenericSmartFactoryAllocBase() = default;

		using base_type = GenericFactoryBase<ObjectInterface_ *, ObjectType_, Alloc_>;

		GenericSmartFactoryAllocBase(const Alloc_ &alloc = {})
			: base_type(alloc) {}

	};

	template<typename ObjectInterface_, typename ObjectType_, typename Alloc_ = std::allocator<ObjectType_>>
	class GenericSmartFactoryAlloc : public GenericSmartFactoryAllocBase<ObjectInterface_, ObjectType_, Alloc_>
	{
	public:

		[[nodiscard]] virtual std::shared_ptr<ObjectInterface_> create_shared() override
		{
			return this->construct_shared();
		}

		[[nodiscard]] virtual UniquePtr<ObjectInterface_> create_unique() override
		{
			return this->construct_unique(type_v<ObjectInterface_>);
		}
	};
}
