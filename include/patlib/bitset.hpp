/*
	PatLib

	Copyright (C) 2020 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "base.hpp"
#include "concepts.hpp"
#include "type.hpp"

#include <bitset>
#include <string>
#include <iostream>

namespace patlib
{
	namespace bitset_impl
	{
		template <typename T_, CEnum EnumType_>
		constexpr bool is_flag_pair = false;
		template <CEnum EnumType_, CBoolLike Value_>
		constexpr bool is_flag_pair<std::pair<EnumType_, Value_>, EnumType_> = true;

		template <typename T_, typename EnumType_>
		concept CFlagPair = CEnum<EnumType_> && is_flag_pair<T_, EnumType_>;
	} // namespace bitset_impl

	template <CEnum EnumType_, szt N_>
	class enum_bitset
	{
	private:
		friend struct std::hash<enum_bitset>;

		using ull_type = unsigned long long;
		using internal_bitset = std::bitset<N_>;

		internal_bitset mBits;

		constexpr enum_bitset(const internal_bitset &o)
			: mBits{o} {}

	public:

		using reference = typename internal_bitset::reference;
		using enum_type = EnumType_;

		constexpr enum_bitset() = default;
		constexpr enum_bitset(std::initializer_list<enum_type> bits) noexcept
		{
			for(const auto &b : bits)
				set(b);
		}

		constexpr enum_bitset(bitset_impl::CFlagPair<EnumType_> auto... bits) noexcept
			: mBits{ (ull_type{} | ... | (ull_type{std::get<1>(bits)} << underlying_value(std::get<0>(bits)))) } {}

		[[nodiscard]] constexpr bool operator==(const enum_bitset &o) const noexcept
			{ return mBits == o.mBits; }

		[[nodiscard]] constexpr bool operator[](enum_type pos) const
			{ return mBits[underlying_value(pos)]; }

		[[nodiscard]] reference operator[](enum_type pos)
			{ return mBits[underlying_value(pos)]; }

		[[nodiscard]] bool test(enum_type pos) const
			{ return mBits.test(underlying_value(pos)); }

		[[nodiscard]] bool all() const noexcept
			{ return mBits.all(); }

		[[nodiscard]] bool none() const noexcept
			{ return mBits.none(); }

		[[nodiscard]] bool any() const noexcept
			{ return mBits.any(); }

		[[nodiscard]] auto count() const noexcept
			{ return mBits.count(); }

		[[nodiscard]] constexpr auto size() const noexcept
			{ return mBits.size(); }

		auto &operator&=(const enum_bitset &o) noexcept
			{ mBits &= o.mBits; return *this; }
		auto &operator|=(const enum_bitset &o) noexcept
			{ mBits |= o.mBits; return *this; }
		auto &operator^=(const enum_bitset &o) noexcept
			{ mBits ^= o.mBits; return *this; }

		[[nodiscard]] enum_bitset operator~() const noexcept
			{ return ~mBits; }

		auto &operator<<=(const szt pos) noexcept
			{ mBits <<= pos; return *this; }
		[[nodiscard]] enum_bitset operator<<(const szt pos) const noexcept
			{ return mBits << pos; }
		auto &operator>>=(const szt pos) noexcept
			{ mBits >>= pos; return *this; }
		[[nodiscard]] enum_bitset operator>>(const szt pos) const noexcept
			{ return mBits >> pos; }

		auto &set() noexcept { mBits.set(); return *this; }
		auto &set(enum_type pos, bool value = true) { mBits.set(underlying_value(pos), value); return *this; }

		auto &reset() noexcept { mBits.reset(); return *this; }
		auto &reset(enum_type pos) { mBits.reset(underlying_value(pos)); return *this; }

		auto &flip() noexcept { mBits.flip(); return *this; }
		auto &flip(enum_type pos) { mBits.flip(underlying_value(pos)); return *this; }

		template <typename CharT_ = char, typename Traits_ = std::char_traits<CharT_>, typename Alloc_ = std::allocator<CharT_>>
		[[nodiscard]] auto to_string(CharT_ zero = CharT_('0'), CharT_ one = CharT_('1')) const
			{ return mBits.template to_string<CharT_, Traits_, Alloc_>(zero, one); }

		[[nodiscard]] auto to_ulong() const { return mBits.to_ulong(); }
		[[nodiscard]] auto to_ullong() const { return mBits.to_ullong(); }

		[[nodiscard]] friend enum_bitset operator&(const enum_bitset &ls, const enum_bitset &rs) noexcept
			{ return ls.mBits & rs.mBits; }
		[[nodiscard]] friend enum_bitset operator|(const enum_bitset &ls, const enum_bitset &rs) noexcept
			{ return ls.mBits | rs.mBits; }
		[[nodiscard]] friend enum_bitset operator^(const enum_bitset &ls, const enum_bitset &rs) noexcept
			{ return ls.mBits ^ rs.mBits; }

		template <typename CharT_, typename Traits_>
		friend auto &operator<<(std::basic_ostream<CharT_, Traits_> &os, const enum_bitset &b)
			{ return (os << b.mBits); }

		template <typename CharT_, typename Traits_>
		friend auto &operator>>(std::basic_istream<CharT_, Traits_> &is, enum_bitset &b)
			{ return (is >> b.mBits); }
	};
}

namespace std
{
	template <typename EnumType_, size_t N_>
	struct hash<::patlib::enum_bitset<EnumType_, N_>>
	{
	private:
		hash<bitset<N_>> mHash;

	public:
		size_t operator()(const ::patlib::enum_bitset<EnumType_, N_> &bs) const noexcept
			{ return mHash(bs.mBits); }
	};
}
