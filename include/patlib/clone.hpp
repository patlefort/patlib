/*
	PatLib

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <type_traits>
#include <utility>
#include <cstddef>
#include <memory>
#include <functional>
#include <memory_resource>

#include "clone_fwd.hpp"

namespace patlib
{
	template <typename T_>
	using UniquePtr = std::unique_ptr<T_, std::function<void(T_ *)>>;

	template <typename Base_>
	struct ICloneable
	{
		virtual ~ICloneable() = default;

		[[nodiscard]] virtual std::pair<szt, szt> clone_size() const = 0; // Return size, alignment
		[[nodiscard]] virtual Base_ *clone() const = 0;
		virtual void clone(void *ptr) const = 0;
		[[nodiscard]] virtual Base_ *clone(std::pmr::memory_resource &memRes) const = 0;
		[[nodiscard]] virtual std::unique_ptr<Base_> clone_unique() const = 0;
		[[nodiscard]] virtual UniquePtr<Base_> clone_unique(std::pmr::memory_resource &memRes) const = 0;
	};

	template <typename Base_, typename Derived_ = Base_>
	struct Cloneable : public virtual ICloneable<Base_>
	{
		[[nodiscard]] virtual std::pair<szt, szt> clone_size() const override { return {sizeof(Derived_), alignof(Derived_)}; }

		[[nodiscard]] virtual Base_ *clone() const override { return new Derived_(); }

		virtual void clone(void *ptr) const override { new (ptr) Derived_(); }

		[[nodiscard]] virtual Base_ *clone(std::pmr::memory_resource &memRes) const override
		{
			auto ptr = reinterpret_cast<Derived_ *>(memRes.allocate(sizeof(Derived_), alignof(Derived_)));

			try
			{
				construct_forward(ptr);
			}
			catch(...)
			{
				memRes.deallocate(ptr, sizeof(Derived_), alignof(Derived_));
				throw;
			}

			return ptr;
		}

		[[nodiscard]] virtual std::unique_ptr<Base_> clone_unique() const override
		{
			return std::unique_ptr<Base_>(clone());
		}

		[[nodiscard]] virtual UniquePtr<Base_> clone_unique(std::pmr::memory_resource &memRes) const override
		{
			auto ptr = clone(memRes);

			return UniquePtr<Base_>(ptr, [&memRes](Base_ *p){ auto *p2 = static_cast<Derived_ *>(p); std::destroy_at(p2); memRes.deallocate(p2, sizeof(Derived_), alignof(Derived_)); });
		}
	};


	template <typename Base_>
	struct ICloneableCopy
	{
		virtual ~ICloneableCopy() = default;

		[[nodiscard]] virtual std::pair<szt, szt> clone_copy_size() const = 0; // Return size, alignment
		[[nodiscard]] virtual Base_ *clone_copy() const = 0;
		virtual void clone_copy(void *ptr) const = 0;
		[[nodiscard]] virtual Base_ *clone_copy(std::pmr::memory_resource &memRes) const = 0;
		[[nodiscard]] virtual std::unique_ptr<Base_> clone_copy_unique() const = 0;
		[[nodiscard]] virtual UniquePtr<Base_> clone_copy_unique(std::pmr::memory_resource &memRes) const = 0;
	};

	template <typename Base_, typename Derived_ = Base_>
	struct CloneableCopy : public virtual ICloneableCopy<Base_>
	{
		[[nodiscard]] virtual std::pair<szt, szt> clone_copy_size() const override { return {sizeof(Derived_), alignof(Derived_)}; }

		[[nodiscard]] virtual Base_ *clone_copy() const override { return new Derived_(static_cast<const Derived_ &>(*this)); }

		virtual void clone_copy(void *ptr) const override { new (ptr) Derived_(static_cast<const Derived_ &>(*this)); }

		[[nodiscard]] virtual Base_ *clone_copy(std::pmr::memory_resource &memRes) const override
		{
			auto ptr = reinterpret_cast<Derived_ *>(memRes.allocate(sizeof(Derived_), alignof(Derived_)));

			try
			{
				construct_forward(ptr, static_cast<const Derived_ &>(*this));
			}
			catch(...)
			{
				memRes.deallocate(ptr, sizeof(Derived_), alignof(Derived_));
				throw;
			}

			return ptr;
		}

		[[nodiscard]] virtual std::unique_ptr<Base_> clone_copy_unique() const override
		{
			return std::unique_ptr<Base_>(clone_copy());
		}

		[[nodiscard]] virtual UniquePtr<Base_> clone_copy_unique(std::pmr::memory_resource &memRes) const override
		{
			auto ptr = clone_copy(memRes);

			return UniquePtr<Base_>(ptr, [&memRes](Base_ *p){ auto *p2 = static_cast<Derived_ *>(p); std::destroy_at(p2); memRes.deallocate(p2, sizeof(Derived_), alignof(Derived_)); });
		}
	};

	template <typename Base_, typename Derived_ = Base_>
	struct CloneableCopyAssignment : public virtual ICloneableCopy<Base_>
	{
		[[nodiscard]] virtual std::pair<szt, szt> clone_copy_size() const override { return {sizeof(Derived_), alignof(Derived_)}; }

		[[nodiscard]] virtual Base_ *clone_copy() const override { auto *n = new Derived_(); *n = static_cast<const Derived_ &>(*this); return n; }

		virtual void clone_copy(void *ptr) const override { new (ptr) Derived_(); *static_cast<Derived_ *>(ptr) = static_cast<const Derived_ &>(*this); }

		[[nodiscard]] virtual Base_ *clone_copy(std::pmr::memory_resource &memRes) const override
		{
			auto ptr = reinterpret_cast<Derived_ *>(memRes.allocate(sizeof(Derived_), alignof(Derived_)));

			try
			{
				construct_forward(ptr);
			}
			catch(...)
			{
				memRes.deallocate(ptr, sizeof(Derived_), alignof(Derived_));
				throw;
			}

			*ptr = static_cast<const Derived_ &>(*this);
			return ptr;
		}

		[[nodiscard]] virtual std::unique_ptr<Base_> clone_copy_unique() const override
		{
			return std::unique_ptr<Base_>(clone_copy());
		}

		[[nodiscard]] virtual UniquePtr<Base_> clone_copy_unique(std::pmr::memory_resource &memRes) const override
		{
			auto ptr = clone_copy(memRes);

			return UniquePtr<Base_>(ptr, [this, &memRes](Base_ *p){ auto *p2 = static_cast<Derived_ *>(p); std::destroy_at(p2); memRes.deallocate(p2, sizeof(Derived_), alignof(Derived_)); });
		}
	};

	template <typename Base_>
	class ICloneableNewOrCopy : public virtual ICloneable<Base_>, public virtual ICloneableCopy<Base_>
	{
	public:
		virtual ~ICloneableNewOrCopy() = default;
	};

	template <typename Base_, typename Derived_ = Base_>
	struct CloneableNewOrCopy : public virtual ICloneableNewOrCopy<Base_>, public Cloneable<Base_, Derived_>, public CloneableCopy<Base_, Derived_> {};
}

