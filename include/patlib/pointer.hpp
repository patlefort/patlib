/*
	PatLib

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "base.hpp"
#include "type.hpp"
#include "concepts.hpp"

#include <memory>
#include <utility>
#include <cstddef>

namespace patlib
{
	// For transforming a pointer or a smart pointer to a reference.
	[[nodiscard]] constexpr auto &to_ref(auto &obj) noexcept
		{ return obj; }

	[[nodiscard]] constexpr decltype(auto) to_ref(CPointerLike auto &obj) noexcept(noexcept(*obj))
		{ return *obj; }

	#define PL_LAMBDA_FORWARD_OBJ(setterFc, object) PL_LAMBDA_FORWARD(::patlib::to_ref(object).setterFc)


	template <CPointerLike T_>
	using pointer_dereference_t = decltype(*std::declval<T_>());

	template <typename T_ = void>
	[[nodiscard]] constexpr bool same_underlying_address(const CPointerLike auto &p1, const CPointerLike auto &p2) noexcept
		{ return static_cast<const T_ *>(std::to_address(p1)) == static_cast<const T_ *>(std::to_address(p2)); }


	template <typename T_, typename... Args_>
	constexpr void make_unique(std::unique_ptr<T_> &ptr, Args_&&... args)
		{ ptr = std::make_unique<T_>(PL_FWD(args)...); }

	template <typename T_, typename... Args_>
	constexpr void make_shared(std::shared_ptr<T_> &ptr, Args_&&... args)
		{ ptr = std::make_shared<T_>(PL_FWD(args)...); }

	template <typename T_>
	[[nodiscard]] constexpr auto move_pointer(T_* &ptr) noexcept
		{ return std::exchange(ptr, nullptr); }

	template <typename T_>
	constexpr void move_pointer(T_* &ptr1, T_* &ptr2) noexcept
		{ ptr1 = std::exchange(ptr2, nullptr); }

	template <typename T_>
	[[nodiscard]] constexpr const T_ *point_to_const_cast(T_ *p) noexcept
		{ return p; }

	template <typename T_>
	class unique_unmanaged_pointer : move_only
	{
	private:
		T_ *mP{};

	public:

		using pointer_type = T_;

		constexpr unique_unmanaged_pointer() = default;
		constexpr unique_unmanaged_pointer(T_ *p) noexcept : mP{p} {}

		constexpr unique_unmanaged_pointer(unique_unmanaged_pointer &&o) noexcept :
			mP{move_pointer(o.mP)} {}

		constexpr unique_unmanaged_pointer &operator=(unique_unmanaged_pointer &&o) noexcept
			{ move_pointer(mP, o.mP); return *this; }

		constexpr unique_unmanaged_pointer &operator=(T_ *p) noexcept { mP = p; return *this; }

		[[nodiscard]] constexpr auto &operator *() const noexcept { return *mP; }
		[[nodiscard]] constexpr auto operator->() const noexcept { return mP; }
		[[nodiscard]] constexpr auto get() const noexcept { return mP; }

		[[nodiscard]] constexpr auto operator &() noexcept { return &mP; }
		[[nodiscard]] constexpr auto operator &() const noexcept { return &mP; }

		[[nodiscard]] constexpr operator std::ptrdiff_t() const noexcept { return (std::ptrdiff_t)mP; }
		[[nodiscard]] constexpr operator T_ *() const noexcept { return mP; }

		[[nodiscard]] constexpr operator bool() const noexcept { return bool(mP); }

		[[nodiscard]] constexpr friend bool operator==(const unique_unmanaged_pointer &p1, const unique_unmanaged_pointer &p2) noexcept
			{ return p1.mP == p2.mP; }

		[[nodiscard]] constexpr friend bool operator==(const T_ *p1, const unique_unmanaged_pointer &p2) noexcept
			{ return p1 == p2.mP; }

		[[nodiscard]] constexpr friend bool operator==(const unique_unmanaged_pointer &p2, const T_ *p1) noexcept
			{ return p1 == p2.mP; }

		[[nodiscard]] constexpr friend bool operator==(std::nullptr_t, const unique_unmanaged_pointer &p) noexcept
			{ return p.mP == nullptr; }

		[[nodiscard]] constexpr friend bool operator==(const unique_unmanaged_pointer &p, std::nullptr_t) noexcept
			{ return p.mP == nullptr; }

		[[nodiscard]] constexpr auto operator<=>(const unique_unmanaged_pointer &p2) const noexcept
			{ return mP <=> p2.mP; }

		[[nodiscard]] constexpr auto operator<=>(std::nullptr_t) const noexcept
			{ return mP <=> nullptr; }

		[[nodiscard]] constexpr auto operator<=>(const T_ *p) const noexcept
			{ return mP <=> p; }
	};
}
