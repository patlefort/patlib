/*
	PatLib

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "thread_fwd.hpp"

#include "base.hpp"
#include "type.hpp"
#include "mutex.hpp"
#include "back_buffered.hpp"
#include "scoped.hpp"
#include "pointer.hpp"
#include "range.hpp"

#include <utility>
#include <array>
#include <functional>
#include <string_view>
#include <list>
#include <string>
#include <memory>
#include <chrono>
#include <mutex>
#include <condition_variable>
#include <atomic>
#include <thread>
#include <shared_mutex>
#include <vector>

#include <boost/signals2.hpp>
#include <boost/lockfree/spsc_queue.hpp>
#include <boost/lockfree/queue.hpp>

#include <yamc/yamc_rwlock_sched.hpp>
#include <yamc/alternate_shared_mutex.hpp>

namespace patlib
{
	using thread_events = std::array<boost::signals2::signal<void()>, 5>;

	class IDevice
	{
	public:
		virtual ~IDevice() = default;

		virtual void start() = 0;
		virtual void end() = 0;
		virtual void pause() = 0;
		virtual void restart() = 0;
		[[nodiscard]] virtual bool started() const = 0;
		[[nodiscard]] virtual bool waiting() const = 0;
		[[nodiscard]] virtual bool paused() const = 0;
		[[nodiscard]] virtual bool running() const = 0;
		virtual void join() = 0;
		virtual void priority(EnumThreadPriority p) = 0;
		[[nodiscard]] virtual EnumThreadPriority priority() const = 0;
		[[nodiscard]] virtual szt wait_count() const = 0;

		virtual void lock_execution() = 0;
		virtual void unlock_execution() = 0;
	};

	class IThreadedDevice : public virtual IDevice
	{
	public:
		virtual ~IThreadedDevice() = default;

		virtual boost::signals2::connection listen(const ThreadEvent ev, std::function<void()> fc) = 0;
	};

	template <typename Alloc_ = std::allocator<std::function<void()>>>
	class call_queue
	{
	protected:
		using queue_type = boost::lockfree::queue<std::function<void()> *>;

		queue_type mCallsQueue;
		Alloc_ mAlloc;

		void deleteEntry(std::function<void()> *f)
		{
			std::allocator_traits<Alloc_>::destroy(mAlloc, f);
			std::allocator_traits<Alloc_>::deallocate(mAlloc, f, 1);
		}

	public:

		call_queue(typename queue_type::size_type size)
			: mCallsQueue(size) {}

		~call_queue() { clear(); }

		bool push(std::invocable auto&& fc)
		{
			const auto ptr = std::allocator_traits<Alloc_>::allocate(mAlloc, 1);

			try
			{
				std::allocator_traits<Alloc_>::construct(mAlloc, ptr, PL_FWD(fc));
			}
			catch(...)
			{
				std::allocator_traits<Alloc_>::deallocate(mAlloc, ptr, 1);
				throw;
			}

			const bool res = mCallsQueue.push(ptr);

			if(!res)
				deleteEntry(ptr);

			return res;
		}

		void clear()
		{
			mCallsQueue.consume_all([this](auto&& f){ deleteEntry(f); });
		}

		auto process()
		{
			return mCallsQueue.consume_all([this](auto&& f){
				scoped_guard g{[&]{ deleteEntry(f); }};
				std::invoke(*f);
			});
		}

		auto process_one()
		{
			return mCallsQueue.consume_one([this](auto&& f){
				scoped_guard g{[&]{ deleteEntry(f); }};
				std::invoke(*f);
			});
		}
	};

	template <typename RequestType_>
	class request_token
	{
	public:
		using request_type = RequestType_;

	private:

		static constexpr request_type EmptyRequest{};

		std::atomic<bool> mEnabled{false};
		std::atomic<request_type> mRequest{EmptyRequest};
		mutable std::condition_variable mWaitRequest;
		mutable std::mutex mWaitMutex;

		bool try_request_impl(request_type request, auto &lock)
		{
			if(const auto cur = mRequest.load(); cur == EmptyRequest)
			{
				mRequest = request;
				lock.unlock();
				mWaitRequest.notify_all();
				return true;
			}
			else if(cur == request)
				return true;

			return false;
		}

	public:
		~request_token()
		{
			enabled(false);
		}

		[[nodiscard]] bool enabled() const noexcept { return mEnabled; }

		void enabled(bool en)
		{
			if(mEnabled != en)
			{
				{
					std::unique_lock waitLock{mWaitMutex};
					mRequest = {};
					mEnabled = en;
				}

				mWaitRequest.notify_all();
			}
		}

		void request(request_type request)
		{
			std::unique_lock waitLock{mWaitMutex};

			while(mEnabled && !try_request_impl(request, waitLock))
				mWaitRequest.wait(waitLock, [&]{ return mRequest == EmptyRequest || !mEnabled; });
		}

		bool try_request(request_type request)
		{
			if(!mEnabled)
				return false;

			std::unique_lock waitLock{mWaitMutex};
			return try_request_impl(request, waitLock);
		}

		[[nodiscard]] auto state() const noexcept { return mRequest.load(); }
		[[nodiscard]] bool requested(request_type request) const noexcept { return mRequest == request; }
		[[nodiscard]] bool has_request() const noexcept { return mRequest != EmptyRequest; }

		void wait_request(std::predicate auto &&pred) const
		{
			if(mRequest != EmptyRequest)
				return;

			std::unique_lock waitLock{mWaitMutex};
			mWaitRequest.wait(waitLock, [&]{ return mRequest != EmptyRequest || std::invoke(PL_FWD(pred)); });
		}

		template <typename Rep_, typename Period_>
		bool wait_request_for(const std::chrono::duration<Rep_, Period_> &relTime, std::predicate auto &&pred) const
		{
			if(mRequest != EmptyRequest)
				return true;

			std::unique_lock waitLock{mWaitMutex};
			return mWaitRequest.wait_for(waitLock, relTime, [&]{ return mRequest != EmptyRequest || std::invoke(PL_FWD(pred)); });
		}

		bool handle(request_type request)
		{
			if(mRequest != request)
				return false;

			clear();
			return true;
		}

		request_type handle()
		{
			const auto request = mRequest.load();
			if(request != EmptyRequest)
				clear();

			return request;
		}

		void clear()
		{
			{
				std::unique_lock waitLock{mWaitMutex};
				mRequest = {};
			}

			mWaitRequest.notify_all();
		}
	};

	class PATLIB_API ThreadBase : public virtual IThreadedDevice
	{
	public:
		ThreadBase() = default;

		virtual ~ThreadBase() override;

		// Can be executed outside the thread
		virtual void start() override;
		virtual void end() override;
		virtual void pause() override;
		virtual void restart() override;
		[[nodiscard]] virtual bool started() const override { return mActive; }
		[[nodiscard]] virtual bool waiting() const override { return mWaiting; }
		[[nodiscard]] virtual bool paused() const  override{ return mPaused; }
		[[nodiscard]] virtual bool running() const override { return started() && !paused(); }
		[[nodiscard]] virtual bool ready_to_start() { return true; }
		virtual void join() override;
		virtual void priority(EnumThreadPriority p) override;
		[[nodiscard]] virtual EnumThreadPriority priority() const override { return mPriority; }
		[[nodiscard]] virtual szt wait_count() const override { return mWaitCount; }

		virtual void lock_execution() override { mExecutionMutex.lock(); }
		virtual void unlock_execution() override { mExecutionMutex.unlock(); }

		[[nodiscard]] auto &execution_mutex() const noexcept { return mExecutionMutex; }

		// Events are executed within the thread
		virtual boost::signals2::connection listen(const ThreadEvent ev, std::function<void()> fc) override;

		virtual void name(std::string n);
		[[nodiscard]] const auto &name() const noexcept { return mName; }

	protected:

		request_token<ThreadRequest> mRequests;
		std::atomic<bool> mActive{false}, mWaiting{false}, mPaused{false};

		bool mBusyWait = false;

		std::thread mThread;
		std::string mName;
		EnumThreadPriority mPriority = EnumThreadPriority::Normal;
		std::atomic<szt> mWaitCount{0};

		thread_events mEvts;

		mutable yamc::alternate::basic_shared_mutex<yamc::rwlock::WriterPrefer> mExecutionMutex;

		// Executed within the thread
		void process();
		virtual void process_loop();
		bool process_events();

		void work_loop(std::predicate auto&& fc)
		{
			do
			{
				if(std::shared_lock g{mExecutionMutex}; !std::invoke(PL_FWD(fc)))
					break;

				if(!process_events())
					break;
			}
			while(mActive);
		}

		void trigger_event(ThreadEvent ev) { mEvts[(int)ev](); }

		virtual bool do_iteration() { return true; }
		virtual void on_end();
		virtual void on_pause();
		virtual void on_pause_end();
		virtual void on_start();
		virtual void on_restart();

		[[nodiscard]] virtual bool prepare_to_end() { return true; }
		[[nodiscard]] virtual bool prepare_to_pause() { return true; }

		template <typename Rep_, typename Per_>
		void sleep_for(const std::chrono::duration<Rep_, Per_> &duration) const
			{ std::this_thread::sleep_for(duration); }

		template <typename Clock_, typename Per_>
		void sleep_until(const std::chrono::time_point<Clock_, Per_> &tp) const
			{ std::this_thread::sleep_until(tp); }

		void yield() const noexcept { std::this_thread::yield(); }
	};

	template <typename ThreadType_>
	class ThreadDelegate : public virtual IThreadedDevice
	{
	public:
		virtual ~ThreadDelegate() = default;

		virtual void start() override { to_ref(mOtherThread).start(); }
		virtual void end() override { to_ref(mOtherThread).end(); }
		virtual void pause() override { to_ref(mOtherThread).pause(); }
		virtual void restart() override { to_ref(mOtherThread).restart(); }
		[[nodiscard]] virtual bool started() const override { return to_ref(mOtherThread).started(); }
		[[nodiscard]] virtual bool waiting() const override{ return to_ref(mOtherThread).waiting(); }
		[[nodiscard]] virtual bool paused() const override { return to_ref(mOtherThread).paused(); }
		[[nodiscard]] virtual bool running() const override { return to_ref(mOtherThread).running(); }
		virtual void join() override { to_ref(mOtherThread).join(); }
		virtual void priority(EnumThreadPriority p) override { return to_ref(mOtherThread).priority(p); }
		[[nodiscard]] virtual EnumThreadPriority priority() const override { return to_ref(mOtherThread).priority(); }
		[[nodiscard]] virtual szt wait_count() const override { return to_ref(mOtherThread).wait_count(); }
		virtual boost::signals2::connection listen(const ThreadEvent ev, std::function<void()> fc) override { return to_ref(mOtherThread).listen(ev, std::move(fc)); }

		virtual void lock_execution() override { to_ref(mOtherThread).lock_execution(); }
		virtual void unlock_execution() override { to_ref(mOtherThread).unlock_execution(); }

		virtual void name(std::string n) { to_ref(mOtherThread).name(std::move(n)); }
		[[nodiscard]] const auto &name() const noexcept { return to_ref(mOtherThread).name(); }

	protected:
		ThreadType_ mOtherThread;
	};

	template <typename ThreadType_>
	class ThreadDelegateWithin : public ThreadBase
	{
	public:

		virtual void lock_execution() override { ThreadBase::lock_execution(); to_ref(mOtherThread).lock_execution(); }
		virtual void unlock_execution() override { ThreadBase::unlock_execution(); to_ref(mOtherThread).unlock_execution(); }

		[[nodiscard]] virtual szt wait_count() const override { return ThreadBase::wait_count() + to_ref(mOtherThread).wait_count(); }
		virtual void priority(EnumThreadPriority p) override { ThreadBase::priority(p); to_ref(mOtherThread).priority(p); }

	protected:
		ThreadType_ mOtherThread;

		virtual void on_pause() override { to_ref(mOtherThread).pause(); ThreadBase::on_pause(); }
		virtual void on_pause_end() override { to_ref(mOtherThread).start(); ThreadBase::on_pause_end(); }
		virtual void on_start() override { to_ref(mOtherThread).start(); ThreadBase::on_start(); }
		virtual void on_restart() override { to_ref(mOtherThread).restart(); ThreadBase::on_restart(); }

		[[nodiscard]] virtual bool prepare_to_end() override
		{
			to_ref(mOtherThread).end();
			to_ref(mOtherThread).join();
			return true;
		}
	};

	class ThreadLambda : public ThreadBase
	{
	public:

		using lambda_function = std::function<bool(ThreadLambda &lambdaThread, request_token<ThreadRequest> &requests, bool &waiting, bool &readyToEnd)>;

		virtual void start() override
		{
			if(!mLambda)
				return;

			ThreadBase::start();
		}

		void lambda(lambda_function l) { mLambda = std::move(l); }
		[[nodiscard]] const lambda_function &lambda() const noexcept { return mLambda; }

	protected:
		lambda_function mLambda;
		std::atomic<bool> mReadyToEnd{false};

		[[nodiscard]] virtual bool prepare_to_end() override { return mReadyToEnd; }

		virtual void process_loop() override
		{
			work_loop([this]{
				if(!mLambda)
					return true;

				bool isWaiting_ = false, readyToEnd_ = false;

				const bool shouldContinue = mLambda(*this, mRequests, isWaiting_, readyToEnd_);

				if(!shouldContinue)
					mReadyToEnd = true;
				else
				{
					mWaiting = isWaiting_;
					mReadyToEnd = readyToEnd_;
				}

				return shouldContinue;
			});
		}
	};

	template <typename ThreadClass_>
	class ThreadPool : public virtual IDevice
	{
	public:

		using thread_type = ThreadClass_;

		std::list<thread_type> mThreads;

		virtual void start() override
		{
			for(auto &t : mThreads)
			{
				to_ref(t).priority(mPriority);
				to_ref(t).start();
			}
		}

		virtual void end() override
		{
			for(auto &t : mThreads)
				to_ref(t).end();
		}

		virtual void pause() override
		{
			for(auto &t : mThreads)
				to_ref(t).pause();
		}

		virtual void restart() override
		{
			for(auto &t : mThreads)
				to_ref(t).restart();
		}

		[[nodiscard]] virtual bool started() const override
		{
			return rg::any_of(mThreads, [](const auto &t){ return to_ref(t).started(); });
		}

		[[nodiscard]] virtual bool waiting() const override
		{
			return rg::all_of(mThreads, [](const auto &t){ return to_ref(t).waiting(); });
		}

		[[nodiscard]] virtual bool running() const override
		{
			return rg::any_of(mThreads, [](const auto &t){ return to_ref(t).running(); });
		}

		[[nodiscard]] virtual bool paused() const override
		{
			return rg::all_of(mThreads, [](const auto &t){ return to_ref(t).paused(); });
		}

		virtual void join() override
		{
			for(auto &t : mThreads)
				to_ref(t).join();
		}

		virtual void priority(EnumThreadPriority p) override
		{
			mPriority = p;

			for(auto &t : mThreads)
				to_ref(t).priority(p);
		}

		[[nodiscard]] virtual EnumThreadPriority priority() const override { return mPriority; }

		[[nodiscard]] virtual szt wait_count() const override
		{
			return rg::accumulate(mThreads, szt{}, rg::plus{}, [](const auto &t){ return to_ref(t).wait_count(); });
		}

		virtual void lock_execution() override
		{
			exception_safe_loop(mThreads, [](auto &t){ to_ref(t).lock_execution(); }, [](auto &t){ to_ref(t).unlock_execution(); });
		}

		virtual void unlock_execution() override
		{
			for(auto &t : mThreads)
				to_ref(t).unlock_execution();
		}

	protected:

		EnumThreadPriority mPriority = EnumThreadPriority::Normal;

	};

	template <typename ThreadClass_>
	class ThreadWithPool : public ThreadBase
	{
	public:

		using thread_type = ThreadClass_;

		ThreadPool<thread_type> mThreadPool;

		virtual void priority(EnumThreadPriority p) override
		{
			ThreadBase::priority(p);
			mThreadPool.priority(p);
		}

		[[nodiscard]] virtual szt wait_count() const override
			{ return ThreadBase::wait_count() + mThreadPool.wait_count(); }

		virtual void lock_execution() override
		{
			ThreadBase::lock_execution();

			try
			{
				mThreadPool.lock_execution();
			}
			catch(...)
			{
				ThreadBase::unlock_execution();

				throw;
			}
		}

		virtual void unlock_execution() override
		{
			ThreadBase::unlock_execution();
			mThreadPool.unlock_execution();
		}

	protected:

		virtual void on_start() override
		{
			mThreadPool.start();
			ThreadBase::on_start();
		}

		[[nodiscard]] virtual bool prepare_to_end() override
		{
			mThreadPool.end();
			mThreadPool.join();

			return ThreadBase::prepare_to_end();
		}

		virtual void on_pause() override
		{
			mThreadPool.pause();
			ThreadBase::on_pause();
		}

		virtual void on_pause_end() override
		{
			mThreadPool.start();
			ThreadBase::on_pause_end();
		}

		virtual void on_restart() override
		{
			mThreadPool.restart();
			ThreadBase::on_restart();
		}
	};



	template <typename WorkUnit_>
	class work_unit
	{
	public:
		WorkUnit_ mWorkData;

		using work_unit_type = WorkUnit_;

		void clear()
		{
			mWorkData.clear();
		}
	};

	template <typename WorkUnit_>
	class work_unit_list
	{
	public:
		using queue_type = boost::lockfree::spsc_queue<work_unit<WorkUnit_> *>;
		using size_type = typename queue_type::size_type;
		using work_unit_type = WorkUnit_;

		work_unit_list(size_type size)
			: mPrepared(size), mDone(size), mFree(size), mUnits(size), mSize{size}
		{
			for(auto &u : mUnits)
				mFree.push(&u);
		}

		queue_type mPrepared, mDone, mFree;
		std::vector<work_unit<WorkUnit_>> mUnits;

		auto consume_nb(auto &queue, size_type nb, auto&& fc)
		{
			while(nb)
			{
				if(!queue.consume_one(PL_FWD(fc)))
					break;

				--nb;
			}

			return mSize - nb;
		}

		void clear()
		{
			mPrepared.reset();
			mDone.reset();

			for(auto &u : mUnits)
				mFree.push(&u);
		}

		void clear_safe()
		{
			mPrepared.consume_all([](auto&&){});
			mDone.consume_all([](auto&&){});

			for(auto &u : mUnits)
				mFree.push(&u);
		}

		size_type prepare_all(auto&& fc)
		{
			return mFree.consume_all(
				[&](auto&& u)
				{
					if(PL_FWD(fc)(*u))
						mPrepared.push(u);
					else
						mFree.push(u);
				}
			);
		}

		bool prepare_one(auto&& fc)
		{
			return mFree.consume_one(
				[&](auto&& u)
				{
					if(PL_FWD(fc)(*u))
						mPrepared.push(u);
					else
						mFree.push(u);
				}
			);
		}

		size_type work_on(auto&& fc)
		{
			return consume_nb(mPrepared, mSize, [&](auto&& u){
				if(const auto state = PL_FWD(fc)(*u); state == EnumWorkUnitState::Done)
					mDone.push(u);
			});
		}

		auto *work_on_one()
		{
			typename queue_type::value_type v{nullptr};
			mPrepared.pop(v);
			return v;
		}

		bool work_on_one(auto&& fc)
		{
			return mPrepared.consume_one([&](auto&& u){
				if(const auto state = PL_FWD(fc)(*u); state == EnumWorkUnitState::Done)
					mDone.push(u);
			});
		}

		void work_done(work_unit<WorkUnit_> &unit)
		{
			mDone.push(&unit);
		}

		template <typename ProcessFC_>
		size_type process(ProcessFC_&& processfc)
		{
			return consume_nb(mDone, mSize, [&](auto&& u){
				if(const auto state = std::forward<ProcessFC_>(processfc)(*u); state == EnumWorkUnitState::Done)
					mFree.push(u);
			});
		}

		template <typename PrepareFC_, typename ProcessFC_>
		size_type process_and_prepare(ProcessFC_&& processfc, PrepareFC_&& preparefc)
		{
			return consume_nb(mDone, mSize, [&](auto&& u){
				if(const auto state = std::forward<ProcessFC_>(processfc)(*u); state == EnumWorkUnitState::Done)
					if(std::forward<PrepareFC_>(preparefc)(*u))
						mPrepared.push(u);
			});
		}

	protected:
		size_type mSize;
	};

	template <typename WorkUnit_>
	class back_buffered_work_units
	{
	public:

		using size_type = typename work_unit_list<WorkUnit_>::size_type;
		using work_unit_type = WorkUnit_;

		back_buffered<work_unit_list<WorkUnit_>> mWorkUnits;
		mutable std::shared_mutex mWorkingOnMutex;//, flipMutex;
		work_unit_list<WorkUnit_> *mWorkingOn{};

		back_buffered_work_units(size_type size) : mWorkUnits{std::in_place, size} {}

		[[nodiscard]] auto &front() noexcept { return mWorkUnits.front(); }
		[[nodiscard]] const auto &front() const noexcept { return mWorkUnits.front(); }
		[[nodiscard]] auto &back() noexcept { return mWorkUnits.back(); }
		[[nodiscard]] const auto &back() const noexcept { return *mWorkUnits.back(); }

		void call(auto&& fc)
			{ mWorkUnits.call(PL_FWD(fc)); }

		void call(auto&& fc) const
			{ mWorkUnits.call(PL_FWD(fc)); }

		bool flip() noexcept
		{
			std::shared_lock g1{mWorkingOnMutex};
			//std::unique_lock g2(flipMutex);

			// Flip only if not flipped already
			if(mWorkingOn != &mWorkUnits.back())
			{
				clear_back();
				mWorkUnits.flip();
				mWorkUnits.back().clear_safe();
				//log("") << "Flipped units.";
				return true;
			}
			else
			{
				clear_active();
				//log("") << "Cleared active.";
			}

			return false;
		}

		auto &work_on()
		{
			std::unique_lock g{mWorkingOnMutex};

			mWorkingOn = &mWorkUnits.front();
			return *mWorkingOn;
		}

		bool prepare_one(auto&& fc)
		{
			return mWorkUnits.front().prepare_one(PL_FWD(fc));
		}

		size_type prepare_all(auto&& fc)
		{
			return mWorkUnits.front().prepare_all(PL_FWD(fc));
		}

		size_type process(auto&& processfc)
		{
			return mWorkUnits.front().process(PL_FWD(processfc));
		}

		size_type process_and_prepare(auto&& processfc, auto&& preparefc)
		{
			//std::unique_lock g(flipMutex);

			return mWorkUnits.front().process_and_prepare(PL_FWD(processfc), PL_FWD(preparefc));
		}

		size_type work_on(auto&& fc)
		{
			{
				std::unique_lock g{mWorkingOnMutex};
				mWorkingOn = &mWorkUnits.front();
			}

			return mWorkingOn->work_on([&](auto&& u){
				auto state = PL_FWD(fc)(u);

				if(state == EnumWorkUnitState::Done && !this->working_on_current())
				{
					state = EnumWorkUnitState::Discard;
					//log("") << "Discarted work unit.";
				}

				return state;
			});
		}

		bool work_on_one(auto&& fc)
		{
			{
				std::unique_lock g{mWorkingOnMutex};
				mWorkingOn = &mWorkUnits.front();
			}

			return mWorkingOn->work_on_one([&](auto&& u){
				auto state = PL_FWD(fc)(u);

				if(state == EnumWorkUnitState::Done && !this->working_on_current())
				{
					state = EnumWorkUnitState::Discard;
					//log("") << "Discarted work unit.";
				}

				return state;
			});
		}

		[[nodiscard]] bool working_on_current() const noexcept
			{ std::shared_lock g{mWorkingOnMutex}; return mWorkingOn == &mWorkUnits.front(); }

		void clear()
		{
			clear_back();
			clear_active();
		}

		void clear_back()
		{
			mWorkUnits.back().clear();
		}

		void clear_active()
		{
			mWorkUnits.front().clear();
		}

	};

	template <typename Worker_, typename WorkUnit_, int ListSize_>
	class ThreadWorker : public ThreadBase
	{
	public:
		ThreadWorker()
		{
			mWorkUnits.front().resize(ListSize_);
			mWorkUnits.back().resize(ListSize_);
		}

		bool mDeleted = false;
		back_buffered_work_units<WorkUnit_> mWorkUnits;
		Worker_ mWorker;

		virtual void start() override
		{
			if(!mDeleted)
				ThreadBase::start();
		}

		void clear_work_units()
		{
			mWorkUnits.clear();
		}

		void clear_back_work_units()
		{
			mWorkUnits.clear_back();
		}

	protected:

		virtual void on_start() override
		{
			mWorker.start(mWorkUnits.front(), mWorkUnits.back());
			ThreadBase::on_start();
		}

		virtual void on_end() override
		{
			mWorker.end();
			ThreadBase::on_end();
		}

		virtual void on_restart() override
		{
			mWorker.restart();
			ThreadBase::on_restart();
		}

		virtual void on_pause() override
		{
			mWorker.pause();
			ThreadBase::on_pause();
		}

		virtual void on_pause_end() override
		{
			mWorker.unpause();
			ThreadBase::on_pause_end();
		}

		virtual void process_loop() override
		{
			work_loop([this]{
				mWaiting = true;

				mWorkUnits.work_on(
					[this](auto&& workUnit)
					{
						mWaiting = false;
						return mWorker.process(workUnit.mWorkData);
					}
				);

				mWorker.idle();

				return true;
			});
		}
	};

	template <typename Master_, typename Worker_, typename WorkUnit_, int ListSize_>
	class ThreadMaster : public ThreadWithPool<std::shared_ptr<ThreadWorker<Worker_, WorkUnit_, ListSize_>>>
	{
	public:

		using ThreadPool = ThreadWithPool<std::shared_ptr<ThreadWorker<Worker_, WorkUnit_, ListSize_>>>;

		Master_ mMaster;

		void nb_threads(uf32 nb) noexcept { mNbThreads = nb; }
		[[nodiscard]] uf32 nb_threads() const noexcept { return mNbThreads; }

		void flip() noexcept
		{
			mFlipFlag = true;
		}

	protected:

		uf32 mNbThreads = 0;
		std::atomic<bool> mFlipFlag{false};

		void clear_work_units()
		{
			for(auto &t : this->threadPool.threads)
				t->clear_work_units();
		}

		void clear_back_work_units()
		{
			for(auto &t : this->threadPool.threads)
				t->clear_back_work_units();
		}

		void prepare_work(ThreadWorker<Worker_, WorkUnit_, ListSize_> &t)
		{
			if(t.mDeleted)
				return;

			t.workUnits.prepare(
				[this](auto&& workUnit)
				{
					return mMaster.prepare(workUnit.mWorkData);
				}
			);
		}

		void prepare_work()
		{
			for(auto &t : this->threadPool.threads)
				prepare_work(*t);
		}

		void process_work(ThreadWorker<Worker_, WorkUnit_, ListSize_> &t)
		{
			if(t.mDeleted)
				return;

			t.workUnits.process(
				[this](auto&& workUnit)
				{
					return mMaster.process(workUnit.mWorkData);
				}
			);
		}

		void process_work()
		{
			for(auto &t : this->threadPool.threads)
				process_work(*t);
		}

		virtual void on_start() override
		{
			adjust_threads(false);
			clear_work_units();

			mMaster.start();

			ThreadPool::on_start();
		}

		virtual void on_end() override
		{
			mMaster.end();
			ThreadPool::on_end();
		}

		virtual void on_pause() override
		{
			mMaster.pause();
			ThreadPool::on_pause();
		}

		virtual void on_pause_end() override
		{
			mMaster.unpause();
			ThreadPool::on_pause_end();
		}

		void adjust_threads(bool startNew = true)
		{
			if(mNbThreads > this->threadPool.threads.size())
			{
				repeat(mNbThreads - this->threadPool.threads.size(), [&]{
					auto t = std::make_shared<ThreadWorker<Worker_, WorkUnit_, ListSize_>>();
					this->threadPool.threads.push_back(t);

					t->mDeleted = false;
					t->clear_work_units();
					mMaster.prepareWorker(t->worker, t->workUnits);
					t->priority(this->priority());

					if(startNew)
						t->start();
				});
			}
			else
			{
				for(auto &t : this->threadPool.threads | rgv::take_last(this->threadPool.threads.size() - mNbThreads))
				{
					t->end();
					mMaster.deleteWorker(t->worker);
					t->mDeleted = true;
				}
			}
		}

		void check_deleted_threads()
		{
			std::erase_if(this->threadPool.threads, [](const auto &t){ return t->mDeleted && !t->started(); });
		}

		void flip_units()
		{
			for(auto &t : this->threadPool.threads)
				t->workUnits.flip();
		}

		[[nodiscard]] virtual bool prepare_to_end() override
		{
			if(ThreadPool::prepare_to_end())
			{
				process_work();

				return true;
			}

			return false;
		}

		virtual void on_restart() override
		{
			flip_units();
			mMaster.restart();

			ThreadPool::on_restart();

			prepare_work();
		}

		virtual void process_loop() override
		{
			work_loop([this]{
				adjust_threads();

				if(mFlipFlag)
				{
					flip_units();
					mFlipFlag = false;
				}

				prepare_work();
				process_work();

				check_deleted_threads();

				mMaster.work();

				if(mMaster.workAllDone())
					this->end();
				else
					this->sleep_for(1);

				return true;
			});
		}
	};

}
