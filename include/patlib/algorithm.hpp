/*
	PatLib

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "base.hpp"
#include "type.hpp"
#include "math.hpp"
#include "memory.hpp"
#include "range.hpp"
#include "concepts.hpp"
#include "utility.hpp"

#include <algorithm>
#include <cstddef>
#include <utility>
#include <type_traits>
#include <tuple>
#include <compare>
#include <optional>

#include <range/v3/iterator/concepts.hpp>
#include <range/v3/range/concepts.hpp>

#include <range/v3/algorithm/copy.hpp>
#include <range/v3/algorithm/transform.hpp>
#include <range/v3/algorithm/for_each.hpp>

#ifdef _OPENMP
	#include <omp.h>
#endif

namespace patlib
{
	using policy_size_t = szt;

	namespace impl_algo
	{
		struct policy_base {};

		template <typename Derived_>
		struct basic_parallel_policy : policy_base
		{
			policy_size_t mNbThreads = 0;

			constexpr auto &nb_threads(policy_size_t s) noexcept { mNbThreads = s; return static_cast<Derived_ &>(*this); }
			[[nodiscard]] constexpr auto nb_threads() const noexcept { return mNbThreads; }
		};

		template <typename Derived_>
		struct basic_chunked_parallel_policy : public basic_parallel_policy<Derived_>
		{
			policy_size_t mChunkSize = 0;

			constexpr auto &chunk_size(policy_size_t s) noexcept { mChunkSize = s; return static_cast<Derived_ &>(*this); }
			[[nodiscard]] constexpr auto chunk_size() const noexcept { return mChunkSize; }
		};

		struct parallel_unbalanced_t : public basic_chunked_parallel_policy<parallel_unbalanced_t> {};
		struct parallel_balanced_t : public basic_chunked_parallel_policy<parallel_balanced_t> {};
		struct parallel_t : public basic_chunked_parallel_policy<parallel_t> {};
		struct sequential_t : policy_base {};
	} // namespace impl_algo

	template <typename T_>
	concept CParallelPolicy = std::is_base_of_v<impl_algo::policy_base, T_>;

	namespace policy
	{
		using impl_algo::parallel_unbalanced_t;
		using impl_algo::parallel_balanced_t;
		using impl_algo::parallel_t;
		using impl_algo::sequential_t;

		inline constexpr auto parallel_unbalanced = parallel_unbalanced_t{};
		inline constexpr auto parallel_balanced = parallel_balanced_t{};
		inline constexpr auto sequential = sequential_t{};
		inline constexpr auto parallel = parallel_t{};

		using default_policy_t = sequential_t;
		inline constexpr auto default_policy = default_policy_t{};

		enum class schedule : u32
		{
			// schedule kinds
			static_ = 0x1,
			dynamic = 0x2,
			guided = 0x3,
			auto_ = 0x4,
			// schedule modifier
			monotonic = 0x80000000u
		};

		#ifdef _OPENMP

			inline void set_schedule(schedule sched, policy_size_t chunkSize) noexcept
				{ omp_set_schedule(static_cast<omp_sched_t>(sched), (int)chunkSize); }

			[[nodiscard]] inline std::pair<schedule, policy_size_t> get_schedule() noexcept
			{
				omp_sched_t ompSched;
				int chunkSize;
				omp_get_schedule(&ompSched, &chunkSize);

				return {static_cast<schedule>(ompSched), (policy_size_t)chunkSize};
			}

		#else

			inline void set_schedule(schedule, policy_size_t) noexcept {}

			[[nodiscard]] inline std::pair<schedule, policy_size_t> get_schedule() noexcept
				{ return {schedule::static_, 0}; }

		#endif

		class [[nodiscard]] scoped_schedule
		{
		private:
			schedule mSched;
			policy_size_t mChunkSize;

		public:
			scoped_schedule() noexcept
				{ std::tie(mSched, mChunkSize) = get_schedule(); }
			scoped_schedule(policy_size_t chunkSize) noexcept
					: scoped_schedule()
				{ set_schedule(mSched, chunkSize); }
			scoped_schedule(schedule sched, policy_size_t chunkSize) noexcept
					: scoped_schedule()
				{ set_schedule(sched, chunkSize); }

			~scoped_schedule()
				{ set_schedule(mSched, mChunkSize); }
		};

	} // namespace policy

	namespace impl_algo
	{
		inline constexpr auto for_each = cpo_entry_with_defaults
		{
			properties<>,

			[](CTuple auto&& r, auto fc, auto proj, const auto &)
				{ tuple_for_each(PL_FWD(r), [&](auto&& v){ std::invoke(fc, std::invoke(proj, PL_FWD(v))); } ); },
			[](auto&& r, auto fc, auto proj, const auto &)
				{ rg::for_each(PL_FWD(r), std::move(fc), std::move(proj)); }
		};

		inline constexpr auto transform = cpo_entry_with_defaults
		{
			properties_t{p_pass_self},

			[](const auto &, CTuple auto&& r, auto fc, auto proj, const auto &)
				{ tuple_for_each(PL_FWD(r), [&](auto&& v){ PL_FWD(v) = std::invoke(fc, std::invoke(proj, PL_FWD(v))); } ); },
			[](const auto &self, auto&& r, auto fc, auto proj, const auto &pol)
				requires CInputAndOutputRange<decltype(r), rg::indirect_result_t<decltype(fc), rg::projected<rg::iterator_t<decltype(r)>, decltype(proj)>>>
				{ self(PL_FWD(r), PL_FWD(r).begin(), std::move(fc), std::move(proj), pol); },
			[](const auto &, auto&& r, auto into, auto fc, auto proj, const auto &)
				{ rg::transform(PL_FWD(r), std::move(into), std::move(fc), std::move(proj)); },
			[](const auto &, auto&& r1, auto&& r2, auto into, auto fc, auto proj1, auto proj2, const auto &)
				{ rg::transform(PL_FWD(r1), PL_FWD(r2), std::move(into), std::move(fc), std::move(proj1), std::move(proj2)); }
		};

		inline constexpr auto copy = cpo_entry_with_defaults
		{
			properties<>,

			[](auto&& r, auto into, const auto &)
				{ rg::copy(PL_FWD(r), std::move(into)); }
		};

		#ifdef _OPENMP
			[[nodiscard]] inline auto set_omp_schedule(const policy::parallel_t &pol) noexcept
			{
				if(pol.chunk_size())
					return policy::scoped_schedule{pol.chunk_size()};

				return policy::scoped_schedule{};
			}

			[[nodiscard]] policy_size_t select_nb_threads(const CParallelPolicy auto &pol) noexcept
				{ return !pol.nb_threads() ? (policy_size_t)omp_get_max_threads() : pol.nb_threads(); }
		#endif

		void throwing_parallel_loop(auto&& fc)
		{
			std::atomic_flag exceptSet;
			std::exception_ptr except;

			const auto loopFc =
				[&](auto&& f)
				{
					try { std::invoke(PL_FWD(f)); }
					catch(...)
					{
						if(!exceptSet.test_and_set(std::memory_order::acquire))
							except = std::current_exception();
					}
				};

			std::invoke(fc, loopFc);

			if(except)
				std::rethrow_exception(except);
		}
	} // namespace impl_algo

	inline constexpr auto transform_fn = overload_set
	{
		[]<typename Proj_ = std::identity, CParallelPolicy Policy_ = policy::default_policy_t>
		(auto&& r, auto fc, Proj_ proj = {}, const Policy_ &pol = {}) -> decltype(auto)
		{
			impl_algo::transform(PL_FWD(r), std::move(fc), std::move(proj), pol);
			return PL_FWD_RETURN(r);
		},

		[]<typename Proj_ = std::identity, CParallelPolicy Policy_ = policy::default_policy_t>
		(rg::input_range auto&& r, rg::input_or_output_iterator auto into, auto fc, Proj_ proj = {}, const Policy_ &pol = {}) -> decltype(auto)
		{
			impl_algo::transform(PL_FWD(r), std::move(into), std::move(fc), std::move(proj), pol);
			return PL_FWD_RETURN(r);
		},

		[]<typename Proj1_ = std::identity, typename Proj2_ = std::identity, CParallelPolicy Policy_ = policy::default_policy_t>
		(rg::input_range auto&& r1, rg::input_range auto&& r2, auto into, auto fc, Proj1_ proj1 = {}, Proj2_ proj2 = {}, const Policy_ &pol = {}) -> decltype(auto)
		{
			impl_algo::transform(PL_FWD(r1), PL_FWD(r2), std::move(into), std::move(fc), std::move(proj1), std::move(proj2), pol);
			return PL_FWD_RETURN(r1);
		}
	};

	inline constexpr auto for_each_fn = []<typename Proj_ = std::identity, CParallelPolicy Policy_ = policy::default_policy_t>
	(auto&& r, auto fc, Proj_ proj = {}, const Policy_ &pol = {}) -> decltype(auto)
	{
		impl_algo::for_each(PL_FWD(r), std::move(fc), std::move(proj), pol);
		return PL_FWD_RETURN(r);
	};

	inline constexpr auto copy_fn = []<CParallelPolicy Policy_ = policy::default_policy_t>
	(auto&& r, auto into, const Policy_ &pol = {}) -> decltype(auto)
	{
		impl_algo::copy(PL_FWD(r), std::move(into), pol);
		return PL_FWD_RETURN(r);
	};

	inline constexpr auto for_each = pipe(for_each_fn);
	inline constexpr auto transform = pipe(transform_fn);
	inline constexpr auto copy = pipe(copy_fn);

	template <std::integral SizeT_>
	constexpr void repeat(SizeT_ nb, std::invocable auto&& fc)
	{
		for(SizeT_ i=0; i<nb; ++i)
			PL_FWD(fc)();
	}


	// Any of
	template <typename... Ts_>
	struct any_of : public capturer<Ts_...>
	{
		using capturer<Ts_...>::capturer;
		using capturer<Ts_...>::vars;

		[[nodiscard]] constexpr auto operator<=>(auto&& v) const
			{ return impl::compare_3way(vars, size_v<0>, PL_FWD(v)); }
	};
	template <typename... Ts_>
	any_of(Ts_&&...) -> any_of<Ts_...>;

	template <typename... Ts_>
	[[nodiscard]] constexpr bool operator==(auto&& v, const any_of<Ts_...> &vset)
	{
		return std::apply([&](auto&&... values){ return ( ... || (PL_FWD(v) == PL_FWD(values)) ); }, vset.vars);
	}

	template <typename... Ts_>
	[[nodiscard]] constexpr bool operator==(const any_of<Ts_...> &vset, auto&& v)
	{
		return std::apply([&](auto&&... values){ return ( ... || (PL_FWD(values) == PL_FWD(v)) ); }, vset.vars);
	}

	template <typename... Ts_>
	[[nodiscard]] constexpr bool operator!=(auto&& v, const any_of<Ts_...> &vset)
	{
		return std::apply([&](auto&&... values){ return ( ... || (PL_FWD(v) != PL_FWD(values)) ); }, vset.vars);
	}

	template <typename... Ts_>
	[[nodiscard]] constexpr bool operator!=(const any_of<Ts_...> &vset, auto&& v)
	{
		return std::apply([&](auto&&... values){ return ( ... || (PL_FWD(values) != PL_FWD(v)) ); }, vset.vars);
	}

	template <typename... Ts_>
	[[nodiscard]] constexpr bool operator>(auto&& v, const any_of<Ts_...> &vset)
	{
		return std::apply([&](auto&&... values){ return ( ... || (PL_FWD(v) > PL_FWD(values)) ); }, vset.vars);
	}

	template <typename... Ts_>
	[[nodiscard]] constexpr bool operator>(const any_of<Ts_...> &vset, auto&& v)
	{
		return std::apply([&](auto&&... values){ return ( ... || (PL_FWD(values) > PL_FWD(v)) ); }, vset.vars);
	}

	template <typename... Ts_>
	[[nodiscard]] constexpr bool operator>=(auto&& v, const any_of<Ts_...> &vset)
	{
		return std::apply([&](auto&&... values){ return ( ... || (PL_FWD(v) >= PL_FWD(values)) ); }, vset.vars);
	}

	template <typename... Ts_>
	[[nodiscard]] constexpr bool operator>=(const any_of<Ts_...> &vset, auto&& v)
	{
		return std::apply([&](auto&&... values){ return ( ... || (PL_FWD(values) >= PL_FWD(v)) ); }, vset.vars);
	}

	template <typename... Ts_>
	[[nodiscard]] constexpr bool operator<(auto&& v, const any_of<Ts_...> &vset)
	{
		return std::apply([&](auto&&... values){ return ( ... || (PL_FWD(v) < PL_FWD(values)) ); }, vset.vars);
	}

	template <typename... Ts_>
	[[nodiscard]] constexpr bool operator<(const any_of<Ts_...> &vset, auto&& v)
	{
		return std::apply([&](auto&&... values){ return ( ... || (PL_FWD(values) < PL_FWD(v)) ); }, vset.vars);
	}

	template <typename... Ts_>
	[[nodiscard]] constexpr bool operator<=(auto&& v, const any_of<Ts_...> &vset)
	{
		return std::apply([&](auto&&... values){ return ( ... || (PL_FWD(v) <= PL_FWD(values)) ); }, vset.vars);
	}

	template <typename... Ts_>
	[[nodiscard]] constexpr bool operator<=(const any_of<Ts_...> &vset, auto&& v)
	{
		return std::apply([&](auto&&... values){ return ( ... || (PL_FWD(values) <= PL_FWD(v)) ); }, vset.vars);
	}

	// All of
	template <typename... Ts_>
	struct all_of : public capturer<Ts_...>
	{
		using capturer<Ts_...>::capturer;
		using capturer<Ts_...>::vars;

		[[nodiscard]] constexpr auto operator<=>(auto&& v) const
		{
			return impl::compare_3way(vars, size_v<0>, PL_FWD(v));
		}
	};
	template <typename... Ts_>
	all_of(Ts_&&...) -> all_of<Ts_...>;

	template <typename... Ts_>
	[[nodiscard]] constexpr bool operator==(auto&& v, const all_of<Ts_...> &vset)
	{
		return std::apply([&](auto&&... values){ return ( ... || (PL_FWD(v) == PL_FWD(values)) ); }, vset.vars);
	}

	template <typename... Ts_>
	[[nodiscard]] constexpr bool operator==(const all_of<Ts_...> &vset, auto&& v)
	{
		return std::apply([&](auto&&... values){ return ( ... || (PL_FWD(values) == PL_FWD(v)) ); }, vset.vars);
	}

	template <typename... Ts_>
	[[nodiscard]] constexpr bool operator!=(auto&& v, const all_of<Ts_...> &vset)
	{
		return std::apply([&](auto&&... values){ return ( ... || (PL_FWD(v) != PL_FWD(values)) ); }, vset.vars);
	}

	template <typename... Ts_>
	[[nodiscard]] constexpr bool operator!=(const all_of<Ts_...> &vset, auto&& v)
	{
		return std::apply([&](auto&&... values){ return ( ... || (PL_FWD(values) != PL_FWD(v)) ); }, vset.vars);
	}

	template <typename... Ts_>
	[[nodiscard]] constexpr bool operator>(auto&& v, const all_of<Ts_...> &vset)
	{
		return std::apply([&](auto&&... values){ return ( ... || (PL_FWD(v) > PL_FWD(values)) ); }, vset.vars);
	}

	template <typename... Ts_>
	[[nodiscard]] constexpr bool operator>(const all_of<Ts_...> &vset, auto&& v)
	{
		return std::apply([&](auto&&... values){ return ( ... || (PL_FWD(values) > PL_FWD(v)) ); }, vset.vars);
	}

	template <typename... Ts_>
	[[nodiscard]] constexpr bool operator>=(auto&& v, const all_of<Ts_...> &vset)
	{
		return std::apply([&](auto&&... values){ return ( ... || (PL_FWD(v) >= PL_FWD(values)) ); }, vset.vars);
	}

	template <typename... Ts_>
	[[nodiscard]] constexpr bool operator>=(const all_of<Ts_...> &vset, auto&& v)
	{
		return std::apply([&](auto&&... values){ return ( ... || (PL_FWD(values) >= PL_FWD(v)) ); }, vset.vars);
	}

	template <typename... Ts_>
	[[nodiscard]] constexpr bool operator<(auto&& v, const all_of<Ts_...> &vset)
	{
		return std::apply([&](auto&&... values){ return ( ... || (PL_FWD(v) < PL_FWD(values)) ); }, vset.vars);
	}

	template <typename... Ts_>
	[[nodiscard]] constexpr bool operator<(const all_of<Ts_...> &vset, auto&& v)
	{
		return std::apply([&](auto&&... values){ return ( ... || (PL_FWD(values) < PL_FWD(v)) ); }, vset.vars);
	}

	template <typename... Ts_>
	[[nodiscard]] constexpr bool operator<=(auto&& v, const all_of<Ts_...> &vset)
	{
		return std::apply([&](auto&&... values){ return ( ... || (PL_FWD(v) <= PL_FWD(values)) ); }, vset.vars);
	}

	template <typename... Ts_>
	[[nodiscard]] constexpr bool operator<=(const all_of<Ts_...> &vset, auto&& v)
	{
		return std::apply([&](auto&&... values){ return ( ... || (PL_FWD(values) <= PL_FWD(v)) ); }, vset.vars);
	}


	// Find a unique relation in a boost::bimaps::bimap
	[[nodiscard]] constexpr auto find_unique_relation(auto &map, const auto &left, const auto &right)
	{
		auto endit = map.end();
		for(auto it = map.lower_bound(left); it != endit && it->first == left; ++it)
			if(it->second == right)
				return it;

		return endit;
	}

	inline constexpr auto zero_range_fn = []<typename Range_>(Range_ &&cont)
	{
		if constexpr(CTrivial<rg::range_value_t<Range_>> && rg::contiguous_range<Range_> && rg::sized_range<Range_>)
		{
			if(!cont.empty())
				std::memset(std::addressof(*std::begin(cont)), 0, (szt)rg::size(cont) * sizeof(rg::range_value_t<Range_>));
		}
		else if constexpr(CTrivial<rg::range_value_t<Range_>> && CContainerHasData<Range_> && rg::sized_range<Range_>)
		{
			if(cont.data())
				std::memset(std::to_address(cont.data()), 0, (szt)cont.size() * sizeof(rg::range_value_t<Range_>));
		}
		else if constexpr(CContainerHasFill<Range_>)
			cont.fill({});
		else if constexpr(CContainerHasAssign<Range_>)
			cont.assign(cont.size(), {});
		else
			std::fill(std::begin(cont), std::end(cont), rg::range_value_t<Range_>{});
	};

	inline constexpr auto zero_range = pipe(zero_range_fn);

	inline constexpr auto accumulator_add = [](auto&& acc, auto &&cur){ return PL_FWD(acc) + PL_FWD(cur); };

	inline constexpr auto accumulator_join = []<CJoinable T_>(const T_ &acc, const T_ &cur){ return acc.join(cur); };

	template <typename T_>
	[[nodiscard]] constexpr auto accumulator_lerp(T_ b) noexcept
	{
		return [b = std::move(b)](const T_ &acc, const T_ &cur)
			{ return pllerp(acc, b, cur); };
	}

	template <typename... Args_, typename OP_ = decltype(accumulator_add)>
	[[nodiscard]] constexpr auto accumulator_tuple(auto op = {}) noexcept
	{
		return [op = std::move(op)](const auto &acc, const auto &cur){
			return construct_from_index_sequence<std::tuple<Args_...>>(size_v<sizeof...(Args_)>, [&]<szt I>(size_c<I>){
				return std::invoke(std::get<I>(acc), std::get<I>(cur));
			});
		};
	}

	template <rg::input_range Range_, typename T_, CInvocableConvertibleR<T_, T_, rg::range_value_t<Range_>> OP_ = decltype(accumulator_add), std::invocable<rg::range_reference_t<Range_>> Proj_ = std::identity>
	[[nodiscard]] constexpr T_ accumulate_until(Range_&& r, T_ init, std::predicate<T_> auto&& opUntil, OP_&& op = {}, Proj_&& proj = {})
	{
		for(const auto &v : PL_FWD(r))
		{
			if(std::invoke(opUntil, init))
				break;

			init = std::invoke(op, std::move(init), std::invoke(proj, v));
		}

		return init;
	}

	// More optimized than std::partition when we know how many are to the left.
	template <rg::input_range Range_, std::invocable<rg::range_reference_t<Range_>> Proj_ = std::identity>
	void partition_nb(Range_ &&r, rg::range_size_t<Range_> nbLeft, std::predicate<rg::range_reference_t<Range_>> auto &&checkFc, Proj_&& proj = {})
		requires rg::indirectly_swappable<rg::iterator_t<Range_>, rg::iterator_t<Range_>>
	{
		if(!nbLeft)
			return;

		if constexpr(rg::sized_range<Range_>)
			if(nbLeft == rg::size(PL_FWD(r)))
				return;

		for(auto it = std::begin(PL_FWD(r)); nbLeft; ++it, --nbLeft)
		{
			if(!std::invoke(checkFc, std::invoke(proj, *it)))
			{
				auto rit = it;
				while(!std::invoke(checkFc, std::invoke(proj, *(++rit))));

				std::iter_swap(it, rit);
			}
		}
	}

	// Even more optimized if we have a random access range.
	template <rg::random_access_range Range_, std::invocable<rg::range_reference_t<Range_>> Proj_ = std::identity>
	void partition_nb(Range_ &&r, rg::range_size_t<Range_> nbLeft, std::predicate<rg::range_reference_t<Range_>> auto &&checkFc, Proj_&& proj = {})
		requires rg::indirectly_swappable<rg::iterator_t<Range_>, rg::iterator_t<Range_>>
	{
		if(!nbLeft)
			return;

		if constexpr(rg::sized_range<Range_>)
			if(nbLeft == rg::size(PL_FWD(r)))
				return;

		auto last = std::begin(PL_FWD(r)) + nbLeft;
		auto l = std::end(PL_FWD(r));
		auto rit = last;

		for(auto it = std::begin(PL_FWD(r)); it!=last; ++it)
		{
			if(!std::invoke(checkFc, std::invoke(proj, *it)))
			{
				//rit = std::find_if(rit, l, PL_FWD(checkFc));
				rit = rg::find_if(rg::make_subrange(rit, l), checkFc, proj);
				std::iter_swap(it, rit);
				++rit;
			}
		}
	}

	constexpr auto erase_one(auto &container, const auto &v)
	{
		auto it = rg::find(container, v);
		return container.erase(it, it+1);
	}

	template <typename Container_>
	constexpr auto erase_one_if(Container_ &container, std::predicate<rg::range_reference_t<Container_>> auto &&pred)
	{
		auto it = rg::find_if(container, PL_FWD(pred));
		return container.erase(it, it+1);
	}

	[[nodiscard]] constexpr auto back_insert_copy(auto &container) noexcept
		{ return copy(std::back_inserter(container)); }

	template <typename MapType_>
	[[nodiscard]] constexpr std::optional<typename std::decay_t<MapType_>::mapped_type> find_key_value(MapType_&& r, auto&& k)
	{
		auto it = PL_FWD(r).find(PL_FWD(k));

		if(it != PL_FWD(r).end())
			return {it->second};

		return std::nullopt;
	}
}
