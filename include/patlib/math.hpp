/*
	PatLib

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "base.hpp"
#include "type.hpp"
#include "concepts.hpp"
#include "utility.hpp"

#include <type_traits>
#include <utility>
#include <limits>
#include <cmath>
#include <numbers>
#include <numeric>
#include <bitset>

#include "impl/intrinsics.hpp"

#include <patlib/simd/mathfun/mathfun.hpp>

namespace patlib
{
	namespace tag
	{
		PL_MAKE_DERIVED_TAG(zero, init)
		PL_MAKE_DERIVED_TAG(allone, init)
		PL_MAKE_DERIVED_TAG(one, init)
		PL_MAKE_DERIVED_TAG(two, init)
		PL_MAKE_DERIVED_TAG(half, init)
		PL_MAKE_DERIVED_TAG(identity, init)
		PL_MAKE_DERIVED_TAG(infhighest, init)
		PL_MAKE_DERIVED_TAG(inflowest, init)
		PL_MAKE_DERIVED_TAG(nan, init)
	} // namespace tag

	inline constexpr auto mask = cpo_entry_with_defaults
	{
		properties_t{p_nodiscard},

		[](const auto &v) noexcept -> std::bitset<1>
			{ return {static_cast<bool>(v)}; },

		[]<szt NB_>
		(const std::bitset<NB_> &v) noexcept
			{ return v; }
	};

	[[nodiscard]] constexpr bool mask_any(auto&& v) noexcept
		{ return mask(PL_FWD(v)).any(); }

	[[nodiscard]] constexpr bool mask_all(auto&& v) noexcept
		{ return mask(PL_FWD(v)).all(); }

	[[nodiscard]] constexpr bool mask_none(auto&& v) noexcept
		{ return mask(PL_FWD(v)).none(); }

	template <typename T_>
	inline constexpr bool isBitSet = false;
	template <szt NB_>
	inline constexpr bool isBitSet<std::bitset<NB_>> = true;
	template <typename T_>
	concept CBitSet = isBitSet<T_>;

	template <typename T_>
	concept CMaskable =
		requires(const T_ cn)
		{
			{ mask(cn) } -> CBitSet;
		};

	template <typename T_>
	concept CMaskableAndConvertibleToBool = CExplicitConvertibleTo<T_, bool> && CMaskable<T_>;

	template <typename T_>
	concept CNumberPack = CNumberBase<T_> &&
		requires(const T_ cn)
		{
			{ cn > cn } -> CMaskableAndConvertibleToBool;
			{ cn >= cn } -> CMaskableAndConvertibleToBool;
			{ cn <= cn } -> CMaskableAndConvertibleToBool;
			{ cn < cn } -> CMaskableAndConvertibleToBool;
			{ cn || cn } -> CMaskableAndConvertibleToBool;
			{ cn && cn } -> CMaskableAndConvertibleToBool;
			{ !cn } -> CMaskableAndConvertibleToBool;
			{ cn == cn } -> CMaskableAndConvertibleToBool;
			{ cn != cn } -> CMaskableAndConvertibleToBool;
		};

	template <typename T_>
	concept CNumberPackFloat = CNumberPack<T_> && !std::numeric_limits<T_>::is_integer;

	template <typename T_>
	concept CBitFieldPack = CBitFieldBase<T_> &&
		requires(const T_ cn)
		{
			{ cn || cn } -> CMaskableAndConvertibleToBool;
			{ cn && cn } -> CMaskableAndConvertibleToBool;
			{ !cn } -> CMaskableAndConvertibleToBool;
			{ cn == cn } -> CMaskableAndConvertibleToBool;
			{ cn != cn } -> CMaskableAndConvertibleToBool;
		};

	template <typename T_>
	concept CNumberPackInteger = CNumberPack<T_> && std::numeric_limits<T_>::is_integer &&
		requires(T_ n, const T_ cn)
		{
			{ n %= cn } -> std::same_as<T_&>;
			{ cn % cn } -> std::convertible_to<T_>;
		};

	template <typename T_>
	concept CBitFieldNumberPack = CBitFieldPack<T_> && CNumberPack<T_>;

	namespace constants
	{
		template <CNumberPack T_>
		inline constexpr auto zero = T_{};
		template <CNumberPack T_>
		inline constexpr auto one = static_cast<T_>(1);
		template <CNumberPack T_>
		inline constexpr auto two = static_cast<T_>(2);
		template <CNumberPackFloat T_>
		inline constexpr auto half = static_cast<T_>(0.5);

		template <CNumberPackFloat T_>
		inline constexpr auto sphere_pdf = std::numbers::inv_pi_v<T_> / static_cast<T_>(4);
		template <CNumberPackFloat T_>
		inline constexpr auto hemisphere_pdf = std::numbers::inv_pi_v<T_> / static_cast<T_>(2);

		template <CBitField T_>
		inline constexpr auto all_ones = ~T_{};

		template <CShiftableBitField T_, int nb>
		inline constexpr auto bits = fill_bits(nb, type_v<T_>);

		template <CNumberPack T_>
		inline constexpr auto inflowest = std::numeric_limits<T_>::lowest();

		template <CNumberPackFloat T_>
		inline constexpr auto inflowest<T_> = T_{-std::numeric_limits<T_>::infinity()};

		template <CNumberPack T_>
		inline constexpr auto infhighest = std::numeric_limits<T_>::max();

		template <CNumberPackFloat T_>
		inline constexpr auto infhighest<T_> = T_{std::numeric_limits<T_>::infinity()};
	} // namespace constants

	template <CNumberPack T_, CNumberPack Tin_>
	[[nodiscard]] constexpr bool fits_in(const T_ &v, type_c<Tin_> = {}) noexcept
	{
		if constexpr(std::numeric_limits<underlying_type_t<T_>>::max() >= std::numeric_limits<underlying_type_t<Tin_>>::max() && std::numeric_limits<underlying_type_t<T_>>::lowest() <= std::numeric_limits<underlying_type_t<Tin_>>::lowest())
			return true;
		else
			return mask_all((std::numeric_limits<underlying_type_t<Tin_>>::lowest() <= v) & (v <= std::numeric_limits<Tin_>::max()));
	}

	[[nodiscard]] constexpr auto radian_to_degree(const CNumberPack auto a) noexcept
		{ return a * (std::numbers::inv_pi_v<decltype(a)> * 180); }

	[[nodiscard]] constexpr auto degree_to_radian(const CNumberPack auto a) noexcept
		{ return a * (std::numbers::pi_v<decltype(a)> * (2.0 / 360.0)); }

	[[nodiscard]] constexpr auto pow2(const CNumberPack auto v) noexcept
		{ return v * v; }

	template <typename T_>
	[[nodiscard]] constexpr szt min_index(const T_ &v1, const T_ &v2) noexcept
		{ return v1 > v2; }

	template <typename T_>
	[[nodiscard]] constexpr szt max_index(const T_ &v1, const T_ &v2) noexcept
		{ return v1 <= v2; }

	// cos/sin overloads
	[[nodiscard]] constexpr auto plcos(const auto &v) noexcept
	{
		using std::cos; return cos(v);
	}

	[[nodiscard]] constexpr auto plsin(const auto &v) noexcept
	{
		using std::sin; return sin(v);
	}

	template <typename T_>
	struct sincos_res
	{
		T_ s, c;
	};
	template <typename T_>
	sincos_res(T_, T_) -> sincos_res<T_>;

	inline constexpr auto sincos = cpo_entry_with_defaults
	{
		properties_t{p_nodiscard},

		[](const auto &v) noexcept
			{ return sincos_res{plsin(v), plcos(v)}; }

		#if defined(__SSE__)
			,[](const float v) noexcept
			{
				simd::SseMathfun::v4sf s, c;
				simd::SseMathfun::sincos_ps(_mm_set_ss(v), s, c);
				return sincos_res{_mm_cvtss_f32(s), _mm_cvtss_f32(c)};
			}
		#endif
	};

	#if defined(__SSE__)

		[[nodiscard]] inline float plcos(const float v) noexcept
		{
			return _mm_cvtss_f32(simd::SseMathfun::cos_ps(_mm_set_ss(v)));
		}

		[[nodiscard]] inline float plsin(const float v) noexcept
		{
			return _mm_cvtss_f32(simd::SseMathfun::sin_ps(_mm_set_ss(v)));
		}

	#endif

	// abs overloads
	[[nodiscard]] constexpr auto plabs(const auto &v) noexcept
	{
		using std::abs; return abs(v);
	}

	// exp overloads
	[[nodiscard]] constexpr auto plexp(const auto &v) noexcept
	{
		using std::exp; return exp(v);
	}

	// exp2 overloads
	[[nodiscard]] constexpr auto plexp2(const auto &v) noexcept
	{
		using std::exp2; return exp2(v);
	}

	// exp2approx overloads
	inline constexpr auto exp2approx = cpo_entry_with_defaults
	{
		properties_t{p_nodiscard},

		[](const auto &v) noexcept
			{ using std::exp2; return exp2(v); }
	};

	// log overloads
	[[nodiscard]] constexpr auto pllog(const auto &v) noexcept
	{
		using std::log; return log(v);
	}

	// log2 overloads
	[[nodiscard]] constexpr auto pllog2(const auto &v) noexcept
	{
		using std::log2; return log2(v);
	}

	// log2approx overloads
	inline constexpr auto log2approx = cpo_entry_with_defaults
	{
		properties_t{p_nodiscard},

		[](const auto &v) noexcept
			{ using std::exp2; return log2(v); }
	};

	// log10 overloads
	[[nodiscard]] constexpr auto pllog10(const auto &v) noexcept
	{
		using std::log10; return log10(v);
	}

	// pow overloads
	[[nodiscard]] constexpr auto plpow(const auto &v, const auto &power) noexcept
	{
		using std::pow; return pow(v, power);
	}

	// max overloads
	template <typename T_>
	[[nodiscard]] constexpr auto plmax(const T_ &v1, const T_ &v2) noexcept -> decltype(auto)
	{
		using std::max; return max(v1, v2);
	}

	template <typename T_>
	[[nodiscard]] constexpr auto plmax(std::initializer_list<T_> ilist) noexcept
	{
		using std::max; return max(ilist);
	}

	// min overloads
	template <typename T_>
	[[nodiscard]] constexpr auto plmin(const T_ &v1, const T_ &v2) noexcept -> decltype(auto)
	{
		using std::min; return min(v1, v2);
	}

	template <typename T_>
	[[nodiscard]] constexpr auto plmin(std::initializer_list<T_> ilist) noexcept
	{
		using std::min; return min(ilist);
	}

	// mod overloads
	inline constexpr auto mod = cpo_entry_with_defaults
	{
		properties_t{p_nodiscard},

		[]<std::floating_point T_>
		(const T_ &v1, const T_ &v2) noexcept
			{ using std::fmod; return fmod(v1, v2); },

		[]<std::integral T_>
		(const T_ &v1, const T_ &v2) noexcept
			{ return v1 % v2; }
	};

	// floor overloads
	[[nodiscard]] constexpr auto plfloor(const auto &v1) noexcept
	{
		using std::floor; return floor(v1);
	}

	#if defined(__SSE4_1__)
		[[nodiscard]] inline float plfloor(const float v) noexcept
		{
			const auto vec = _mm_set_ss(v);
			return _mm_cvtss_f32(_mm_floor_ss(vec, vec));
		}

		[[nodiscard]] inline double plfloor(const double v) noexcept
		{
			const auto vec = _mm_set_sd(v);
			return _mm_cvtsd_f64(_mm_floor_sd(vec, vec));
		}
	#endif

	// ceil overloads
	[[nodiscard]] constexpr auto plceil(const auto &v1) noexcept
	{
		using std::ceil; return ceil(v1);
	}

	// round overloads
	[[nodiscard]] constexpr auto plround(const auto &v1) noexcept
	{
		using std::round; return round(v1);
	}

	// reciprocal overloads
	inline constexpr auto reciprocal = cpo_entry_with_defaults
	{
		properties_t{p_nodiscard},

		[](const std::floating_point auto &v1) noexcept
			{ return 1 / v1; }
	};

	// reciprocalapprox overloads
	inline constexpr auto reciprocalapprox = cpo_entry_with_defaults
	{
		properties_t{p_nodiscard},

		[](const std::floating_point auto &v1) noexcept
			{ return reciprocal(v1); }

		#if defined(__SSE__)
			,[](const float v1) noexcept -> float
			{
				__m128 vv = _mm_set_ss(v1);
				__m128 tmp = _mm_rcp_ss(vv);

				// Apply one Newton-Raphson step
				__m128 two = _mm_castsi128_ps( _mm_srli_epi32(_mm_slli_epi32(_mm_cmpeq_epi32(_mm_castps_si128(vv), _mm_castps_si128(vv)), 31), 1) );
				return _mm_cvtss_f32(_mm_mul_ss(tmp, _mm_sub_ss(two, _mm_mul_ss(tmp, vv))));
			}
		#endif
	};

	// lerp overloads
	template <typename T_>
	[[nodiscard]] constexpr auto pllerp(const T_ &a, const T_ &b, const T_ &t) noexcept
	{
		using std::lerp; return lerp(a, b, t);
	}

	// midpoint overloads
	template <typename T_>
	[[nodiscard]] constexpr auto plmidpoint(const T_ &a, const T_ &b) noexcept
	{
		using std::midpoint; return midpoint(a, b);
	}

	// recsqrt overloads
	inline constexpr auto recsqrt = cpo_entry_with_defaults
	{
		properties_t{p_nodiscard},

		[](const std::floating_point auto &v1) noexcept
			{ using std::sqrt; return reciprocal(sqrt(v1)); }
	};

	// recsqrtapprox overloads
	inline constexpr auto recsqrtapprox = cpo_entry_with_defaults
	{
		properties_t{p_nodiscard},

		[](const auto &v1) noexcept
			{ return recsqrt(v1); }

		#if defined(__SSE__)
			,[](const float v) noexcept -> float
			{
				if(!v)
					return 0;

				const auto vv = _mm_set_ss(v);
				const auto r = _mm_rsqrt_ss(vv);

				// Newton-Rhapson step
				return _mm_cvtss_f32(_mm_mul_ss( _mm_mul_ss(_mm_set_ss(0.5f), r), _mm_sub_ss(_mm_set_ss(3), _mm_mul_ss(_mm_mul_ss(vv, r), r)) ));
			}
		#endif
	};

	// sqrt overloads
	[[nodiscard]] constexpr auto plsqrt(const auto &v1) noexcept
	{
		using std::sqrt; return sqrt(v1);
	}

	// sqrtapprox overloads
	inline constexpr auto sqrtapprox = cpo_entry_with_defaults
	{
		properties_t{p_nodiscard},

		[](const auto &v1) noexcept
			{ return plsqrt(v1); }

		#if defined(__SSE__)
			,[](const float v1) noexcept -> float
				{ return recsqrtapprox(v1) * v1; }
		#endif
	};

	// andnot overloads
	inline constexpr auto andnot = cpo_entry_with_defaults
	{
		properties_t{p_nodiscard},

		[](const auto &v1, const auto &v2) noexcept
			{ return (!v1) & v2; }
	};

	// conditional select overloads
	inline constexpr auto select = cpo_entry_with_defaults
	{
		properties_t{p_nodiscard},

		[]<typename T_>
		(const T_ &v1, const T_ &v2, const auto &cond) noexcept
			{ return cond ? v1 : v2; },

		// Full mask select overloads
		[]<typename T_>
		(const T_ &v1, const auto &cond) noexcept
			{ return cond ? v1 : T_{}; }

	};

	// Conditional swap overloads
	inline constexpr auto condswap = cpo_entry_with_defaults
	{
		properties<>,

		[]<typename T1_, typename T2_>
		(T1_ &v1, T2_ &v2, const auto &cond) noexcept(CNoThrowSwappableWith<T1_, T2_>)
		{
			if(cond)
				adl_swap(v1, v2);
		}
	};

	// nextafter overloads
	template <typename T_>
	[[nodiscard]] constexpr auto plnextafter(const T_ &v1, const T_ &after) noexcept
	{
		using std::nextafter; return nextafter(v1, after);
	}

	template <CNumberPackFloat T_>
	[[nodiscard]] constexpr auto nextafterinf(const T_ &v1) noexcept
	{
		using std::nextafter; return nextafter(v1, select(-std::numeric_limits<T_>::infinity(), std::numeric_limits<T_>::infinity(), v1 < 0));
	}

	#ifdef __SSE__

		[[nodiscard]] inline float nextafterinf(const float v1) noexcept
		{
			const auto res = _mm_castsi128_ps(_mm_add_epi32(_mm_castps_si128(_mm_set_ss(v1)), _mm_set1_epi32(1)));

			float fres;
			_mm_store_ss(&fres, res);
			return fres;
		}

		[[nodiscard]] inline double nextafterinf(const double v1) noexcept
		{
			const auto res = _mm_castsi128_pd(_mm_add_epi64(_mm_castpd_si128(_mm_set_sd(v1)), _mm_set1_epi64x(1)));

			double fres;
			_mm_store_sd(&fres, res);
			return fres;
		}

	#endif

	[[nodiscard]] constexpr auto plerf(const auto x) noexcept
		{ using std::erf; return erf(x); }

	[[nodiscard]] constexpr auto plisinf(const auto &x) noexcept
		{ using std::isinf; return isinf(x); }

	[[nodiscard]] constexpr auto plisnan(const auto &x) noexcept
		{ using std::isnan; return isnan(x); }

	[[nodiscard]] constexpr auto plisnormal(const auto &x) noexcept
		{ using std::isnormal; return isnormal(x); }


	inline namespace math_tags_op
	{
		inline constexpr auto osin = make_custom_operator1([](auto&& v) noexcept { return plsin(PL_FWD(v)); });
		inline constexpr auto ocos = make_custom_operator1([](auto&& v) noexcept { return plcos(PL_FWD(v)); });
		inline constexpr auto oabs = make_custom_operator1([](auto&& v) noexcept { return plabs(PL_FWD(v)); });
		inline constexpr auto oexp = make_custom_operator1([](auto&& v) noexcept { return plexp(PL_FWD(v)); });
		inline constexpr auto oexp2 = make_custom_operator1([](auto&& v) noexcept { return plexp2(PL_FWD(v)); });
		inline constexpr auto oexp2approx = make_custom_operator1([](auto&& v) noexcept { return exp2approx(PL_FWD(v)); });
		inline constexpr auto olog = make_custom_operator1([](auto&& v) noexcept { return pllog(PL_FWD(v)); });
		inline constexpr auto olog2 = make_custom_operator1([](auto&& v) noexcept { return pllog2(PL_FWD(v)); });
		inline constexpr auto olog2approx = make_custom_operator1([](auto&& v) noexcept { return log2approx(PL_FWD(v)); });
		inline constexpr auto olog10 = make_custom_operator1([](auto&& v) noexcept { return pllog10(PL_FWD(v)); });
		inline constexpr auto opow = make_custom_operator2([](auto&& v1, auto&& v2) noexcept { return plpow(PL_FWD(v1), PL_FWD(v2)); });
		inline constexpr auto omax = make_custom_operator2([](auto&& v1, auto&& v2) noexcept -> decltype(auto) { return plmax(PL_FWD(v1), PL_FWD(v2)); });
		inline constexpr auto omin = make_custom_operator2([](auto&& v1, auto&& v2) noexcept -> decltype(auto) { return plmin(PL_FWD(v1), PL_FWD(v2)); });
		inline constexpr auto omod = make_custom_operator2([](auto&& v1, auto&& v2) noexcept { return plmod(PL_FWD(v1), PL_FWD(v2)); });
		inline constexpr auto ofloor = make_custom_operator1([](auto&& v) noexcept { return plfloor(PL_FWD(v)); });
		inline constexpr auto oceil = make_custom_operator1([](auto&& v) noexcept { return plceil(PL_FWD(v)); });
		inline constexpr auto oround = make_custom_operator1([](auto&& v) noexcept { return plround(PL_FWD(v)); });
		inline constexpr auto oreciprocal = make_custom_operator1([](auto&& v) noexcept { return reciprocal(PL_FWD(v)); });
		inline constexpr auto oreciprocalapprox = make_custom_operator1([](auto&& v) noexcept { return reciprocalapprox(PL_FWD(v)); });
		inline constexpr auto osqrt = make_custom_operator1([](auto&& v) noexcept { return plsqrt(PL_FWD(v)); });
		inline constexpr auto osqrtapprox = make_custom_operator1([](auto&& v) noexcept { return sqrtapprox(PL_FWD(v)); });
		inline constexpr auto orecsqrt = make_custom_operator1([](auto&& v) noexcept { return recsqrt(PL_FWD(v)); });
		inline constexpr auto orecsqrtapprox = make_custom_operator1([](auto&& v) noexcept { return recsqrtapprox(PL_FWD(v)); });
		inline constexpr auto oisinf = make_custom_operator1([](auto&& v) noexcept { return plisinf(PL_FWD(v)); });
		inline constexpr auto oisnan = make_custom_operator1([](auto&& v) noexcept { return plisnan(PL_FWD(v)); });
		inline constexpr auto oisnormal = make_custom_operator1([](auto&& v) noexcept { return plisnormal(PL_FWD(v)); });
		inline constexpr auto oandnot = make_custom_operator2([](auto&& v1, auto&& v2) noexcept { return andnot(PL_FWD(v1), PL_FWD(v2)); });
		inline constexpr auto onextafterinf = make_custom_operator1([](auto&& v) noexcept { return nextafterinf(PL_FWD(v)); });
		inline constexpr auto oerf = make_custom_operator1([](auto&& v) noexcept { return plerf(PL_FWD(v)); });
		inline constexpr auto opow2 = make_custom_operator1([](auto&& v) noexcept { return pow2(PL_FWD(v)); });
	} // namespace math_tags_op

	[[nodiscard]] constexpr auto is_power_of_2(const CBitFieldNumber auto v) noexcept
		{ return (v & (v - 1)) == 0; }

	[[nodiscard]] constexpr auto one_if_zero(const auto v) noexcept
		{ return select(v, decltype(v){1}, v != 0); }

	[[nodiscard]] constexpr auto zero_if_one(const auto v) noexcept
		{ return select(0, 1, v == 1); }

	// Return 1 if v is nan
	template <typename T_>
	[[nodiscard]] constexpr auto one_if_lezero(const T_ v) noexcept
		{ return select(v, T_{1}, v > 0); }

	template <typename T_>
	[[nodiscard]] constexpr auto plclamp(const T_ &v, const T_ &minv, const T_ &maxv) noexcept
		{ using std::clamp; return clamp(v, minv, maxv); }

	[[nodiscard]] constexpr auto zero_if_nan(const CNumberPackFloat auto &v) noexcept
		{ return select(0, v, plisnan(v)); }


	[[nodiscard]] constexpr auto select_index(const auto rnd, const std::unsigned_integral auto nb) noexcept
		{ return plmin<decltype(nb)>(rnd * nb, nb - 1); }

	template <CNumber T_>
	[[nodiscard]] constexpr T_ erf_inv(T_ x) noexcept
	{
		T_ p{};

		x = plclamp<T_>(x, -1 + std::numeric_limits<T_>::epsilon(), 1 - std::numeric_limits<T_>::epsilon());
		T_ w = -(pow2(1 - x) /opow2 /olog);

		if(w < 5)
		{
			w = w - 2.5;
			p = 2.81022636e-08;
			p = 3.43273939e-07 + p * w;
			p = -3.5233877e-06 + p * w;
			p = -4.39150654e-06 + p * w;
			p = 0.00021858087 + p * w;
			p = -0.00125372503 + p * w;
			p = -0.00417768164 + p * w;
			p = 0.246640727 + p * w;
			p = 1.50140941 + p * w;
		}
		else
		{
			w = plsqrt(w) - 3;
			p = -0.000200214257;
			p = 0.000100950558 + p * w;
			p = 0.00134934322 + p * w;
			p = -0.00367342844 + p * w;
			p = 0.00573950773 + p * w;
			p = -0.0076224613 + p * w;
			p = 0.00943887047 + p * w;
			p = 1.00167406 + p * w;
			p = 2.83297682 + p * w;
		}

		return p * x;
	}

	[[nodiscard]] constexpr auto prob_dendity(const CNumberPack auto p) noexcept
		{ return p / (1 - p); }

	template <CNumberPackFloat... TO_>
	[[nodiscard]] constexpr auto minimax_polynomial_fit([[maybe_unused]] CNumberPackFloat auto x, CNumberPackFloat auto c0, TO_... c) noexcept
	{
		if constexpr(!sizeof...(TO_))
			return c0;
		else
			return c0 + x * minimax_polynomial_fit(x, c...);
	}


	namespace impl
	{
		[[nodiscard]] constexpr bool is_valid_default(const CNumberPackFloat auto &v, const tag::nothing_t) noexcept
			{ return mask_none(plisnan(v) | plisinf(v)); }
	}

	// Operators

	namespace ops
	{
		// Comparison operators with explicit conversion to bool
		inline constexpr auto op_equal_to =         [](const auto &x, const auto &y) noexcept { return static_cast<bool>(x == y); };
		using op_equal_to_t = std::decay_t<decltype(op_equal_to)>;
		inline constexpr auto op_not_equal_to =     [](const auto &x, const auto &y) noexcept { return static_cast<bool>(x != y); };
		using op_not_equal_to_t = std::decay_t<decltype(op_not_equal_to)>;
		inline constexpr auto op_greater =          [](const auto &x, const auto &y) noexcept { return static_cast<bool>(x > y); };
		using op_greater_t = std::decay_t<decltype(op_greater)>;
		inline constexpr auto op_less =             [](const auto &x, const auto &y) noexcept { return static_cast<bool>(x < y); };
		using op_less_t = std::decay_t<decltype(op_less)>;
		inline constexpr auto op_greater_equal =    [](const auto &x, const auto &y) noexcept { return static_cast<bool>(x >= y); };
		using op_greater_equal_t = std::decay_t<decltype(op_greater_equal)>;
		inline constexpr auto op_less_equal =       [](const auto &x, const auto &y) noexcept { return static_cast<bool>(x <= y); };
		using op_less_equal_t = std::decay_t<decltype(op_less_equal)>;

		inline constexpr auto op_min = [](const auto &x, const auto &y) noexcept -> decltype(auto) { return plmin(x, y); };
		using op_min_t = std::decay_t<decltype(op_min)>;
		inline constexpr auto op_max = [](const auto &x, const auto &y) noexcept -> decltype(auto) { return plmax(x, y); };
		using op_max_t = std::decay_t<decltype(op_max)>;
		inline constexpr auto op_mod = [](const auto &x, const auto &y) noexcept -> decltype(auto) { return mod(x, y);; };
		using op_mod_t = std::decay_t<decltype(op_mod)>;
	} // namespace ops
}
