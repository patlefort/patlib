/*
	PatLib

	Copyright (C) 2020 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "base.hpp"

#include <chrono>

namespace patlib
{
	template <typename T_>
	constinit const bool is_time_point = false;

	template <typename Clock_, typename Dur_>
	constinit const bool is_time_point<std::chrono::time_point<Clock_, Dur_>> = true;

	template <typename T_>
	concept CTimePoint = is_time_point<T_>;

	template <typename T_>
	constinit const bool is_duration = false;

	template <typename Rep_, typename Period_>
	constinit const bool is_duration<std::chrono::duration<Rep_, Period_>> = true;

	template <typename T_>
	concept CDuration = is_duration<T_>;

	template <typename T_>
	concept CDurationOrTimePoint = CDuration<T_> || CTimePoint<T_>;

	// Stopwatch-like timer
	template <typename RepType_ = long double, typename ClockType_ = std::chrono::high_resolution_clock>
	class stop_watch
	{
	public:
		using rep_type = RepType_;
		using clock_type = ClockType_;
		using duration_type = std::chrono::duration<RepType_, typename ClockType_::period>;
		using time_point_type = std::chrono::time_point<clock_type, duration_type>;

		constexpr stop_watch() = default;

		template <typename DurT_>
		constexpr stop_watch(const DurT_ &dur) noexcept
			{ this->template duration(dur); }

		constexpr void start() noexcept
		{
			mPaused = false;
			mSavedTotalDuration = mSavedDuration = mTotalDuration = duration_type::zero();
			atomic_fence([&]{ mStartTime = mLastTime = clock_type::now(); });
		}

		constexpr void pause() noexcept
		{
			if(!mPaused)
			{
				mPaused = true;
				atomic_fence([&]{ mCurTime = clock_type::now(); });
				mSavedDuration += mCurTime - mLastTime;
				mSavedTotalDuration += mCurTime - mStartTime;
			}
		}

		constexpr void unpause() noexcept
		{
			if(mPaused)
			{
				mPaused = false;
				atomic_fence([&]{ mStartTime = mLastTime = clock_type::now(); });
			}
		}

		[[nodiscard]] constexpr bool paused() const noexcept { return mPaused; }

		template <typename DurT_>
		constexpr void duration(const DurT_ &dur) noexcept { mTickTimer = std::chrono::duration_cast<duration_type>(dur); }

		[[nodiscard]] constexpr const auto &duration() const noexcept { return mTickTimer; }

		constexpr bool check() noexcept
		{
			atomic_fence([&]{ mCurTime = clock_type::now(); });
			mTotalDuration = mSavedDuration + (mCurTime - mLastTime);

			if(mTotalDuration.count() >= mTickTimer.count())
			{
				mLastTime = mCurTime;
				mSavedDuration = duration_type::zero();
				return true;
			}

			return false;
		}

		[[nodiscard]] constexpr duration_type last_tick_duration() const noexcept { return mTotalDuration; }

		[[nodiscard]] constexpr rep_type period() const noexcept
			{ return mTotalDuration.count() * duration_type::period::num / duration_type::period::den; }

		[[nodiscard]] constexpr duration_type total_running_time() const noexcept
		{
			if(mPaused)
				return mSavedTotalDuration;

			duration_type dur;
			atomic_fence([&]{ dur = clock_type::now() - mStartTime; });

			return mSavedTotalDuration + dur;
		}

	private:

		time_point_type mCurTime, mLastTime, mStartTime;
		duration_type mTickTimer, mSavedDuration, mTotalDuration, mSavedTotalDuration;
		bool mPaused = true;
	};
}
