/*
	PatLib

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "base.hpp"
#include "type.hpp"
#include "utility.hpp"

#include <atomic>
#include <mutex>
#include <type_traits>
#include <cstddef>
#include <thread>
#include <istream>
#include <ostream>

namespace patlib
{
	template <typename Mutex_>
	class shared_mutex_adapter
	{
	private:
		Mutex_ &mMut;

	public:

		shared_mutex_adapter(Mutex_ &m) : mMut{m} {}

		void lock() { mMut.lock_shared(); }
		void unlock() { mMut.unlock_shared(); }
		bool try_lock() { return mMut.try_lock_shared(); }
	};

	template <typename Mutex_>
	class boost_mutex_adaptor
	{
	public:

		template <typename... Args_>
		boost_mutex_adaptor(Args_&&... args)
			: mObjMutex(PL_FWD(args)...) {}

		void lock() { mObjMutex.lock(); }
		void unlock() { mObjMutex.unlock(); }
		bool try_lock() { return mObjMutex.try_lock(); }

		template <typename T_ = Mutex_>
		void lock_shared() { mObjMutex.lock_sharable(); }
		template <typename T_ = Mutex_>
		bool try_lock_shared() { return mObjMutex.try_lock_sharable(); }
		template <typename T_ = Mutex_>
		void unlock_shared() { mObjMutex.unlock_sharable(); }

		template <typename Rep_, typename Period_>
    bool try_lock_shared_for(const std::chrono::duration<Rep_, Period_> &rel_time) { return mObjMutex.try_lock_sharable_for(rel_time); }
    template <typename Clock_, typename Duration_>
    bool try_lock_shared_until(const std::chrono::time_point<Clock_, Duration_> &t) { return mObjMutex.try_lock_sharable_until(t); }

		template <typename Rep_, typename Period_>
    bool try_lock_for(const std::chrono::duration<Rep_, Period_> &rel_time) { return mObjMutex.try_lock_for(rel_time); }
    template <typename Clock_, typename Duration_>
    bool try_lock_until(const std::chrono::time_point<Clock_, Duration_> &t) { return mObjMutex.try_lock_until(t); }

		[[nodiscard]] auto &mutex() const noexcept { return mObjMutex; }

		auto native_handle() const noexcept { return mObjMutex.native_handle(); }

	private:
		mutable Mutex_ mObjMutex;
	};

	template <typename Mutex_>
	class shared_mutex_assignable
	{
	public:

		shared_mutex_assignable() = default;
		shared_mutex_assignable(Mutex_ *m)
			: mObjMutex{m} {}

		void setMutex(Mutex_ *m) noexcept { mObjMutex = m; }
		[[nodiscard]] Mutex_ *getMutex() const noexcept { return mObjMutex; }

		void lock() { assert_valid(mObjMutex)->lock(); }
		void unlock() { assert_valid(mObjMutex)->unlock(); }
		bool try_lock() { return assert_valid(mObjMutex)->try_lock(); }
		void lock_shared() { assert_valid(mObjMutex)->lock_shared(); }
		bool try_lock_shared() { return assert_valid(mObjMutex)->try_lock_shared(); }
		void unlock_shared() { assert_valid(mObjMutex)->unlock_shared(); }

	private:
		mutable Mutex_ *mObjMutex{};
	};

	template <typename T_>
	void atomic_exchange(std::atomic<T_> &mv, const T_ v, std::predicate<T_, T_> auto&& op)
	{
		T_ prev = mv;
		while(PL_FWD(op)(prev, v) && !mv.compare_exchange_weak(prev, v));
	}

	namespace atomic_impl
	{
		template <typename T_>
		using atomic_wrapper_diftype_t = typename T_::difference_type;
	} // namespace atomic_impl

	template <typename AtomicType_>
	class atomic_wrapper
	{
	public:

		using value_type = typename AtomicType_::value_type;
		using difference_type = deferred_if<
			requires { typename AtomicType_::difference_type; },
			defer<atomic_impl::atomic_wrapper_diftype_t, AtomicType_>,
			std::conditional_t<
				std::is_floating_point_v<value_type>,
					value_type,
					std::conditional_t<
						std::is_integral_v<value_type>,
							std::ptrdiff_t,
							value_type
					>
			>
		>;

		static constinit const bool is_always_lock_free = AtomicType_::is_always_lock_free;

		atomic_wrapper() = default;

		constexpr atomic_wrapper(value_type v) noexcept
			: mA{v} {}

		constexpr atomic_wrapper(const atomic_wrapper &o) noexcept
			: mA{o.mA.load()} {}

		constexpr atomic_wrapper(atomic_wrapper &&o) noexcept
			: mA{std::move(o.mA.load())} {}

		constexpr auto &operator=(const atomic_wrapper &o) noexcept
			{ mA = o.load(); return *this; }

		constexpr auto &operator=(atomic_wrapper &&o) noexcept
			{ mA = std::move(o.load()); return *this; }

		auto &operator=(value_type v) noexcept
			{ mA = v; return *this; }

		[[nodiscard]] bool is_lock_free() const noexcept
			{ return mA.is_lock_free(); }

		void store(value_type v) noexcept
			{ mA.store(v); }

		template <typename MemOrder_>
		void store(value_type v, MemOrder_ order) noexcept
			{ mA.store(v, order); }

		[[nodiscard]] auto load() const noexcept
			{ return mA.load(); }

		template <typename MemOrder_>
		auto load(MemOrder_ order) const noexcept
			{ return mA.load(order); }

		[[nodiscard]] operator value_type() const noexcept
			{ return (value_type)mA; }

		auto exchange(value_type v) noexcept
			{ return mA.exchange(v); }

		template <typename MemOrder_>
		auto exchange(value_type v, MemOrder_ order) noexcept
			{ return mA.exchange(v, order); }

		template <typename... Args_>
		bool compare_exchange_weak(Args_&&... args) noexcept
			{ return mA.compare_exchange_weak(PL_FWD(args)...); }

		template <typename... Args_>
		bool compare_exchange_strong(Args_&&... args) noexcept
			{ return mA.compare_exchange_strong(PL_FWD(args)...); }

		template <typename... Args_>
		auto add(Args_&&... args) noexcept
			{ return mA.add(PL_FWD(args)...); }

		template <typename... Args_>
		auto sub(Args_&&... args) noexcept
			{ return mA.sub(PL_FWD(args)...); }

		template <typename... Args_>
		auto fetch_negate(Args_&&... args) noexcept
			{ return mA.fetch_negate(PL_FWD(args)...); }

		template <typename... Args_>
		void opaque_negate(Args_&&... args) noexcept
			{ mA.opaque_negate(PL_FWD(args)...); }

		template <typename... Args_>
		void opaque_add(Args_&&... args) noexcept
			{ mA.opaque_add(PL_FWD(args)...); }

		template <typename... Args_>
		void opaque_sub(Args_&&... args) noexcept
			{ mA.opaque_sub(PL_FWD(args)...); }

		template <typename... Args_>
		auto negate(Args_&&... args) noexcept
			{ return mA.negate(PL_FWD(args)...); }

		template <typename... Args_>
		auto fetch_add(Args_&&... args) noexcept
			{ return mA.fetch_add(PL_FWD(args)...); }

		template <typename... Args_>
		auto fetch_sub(Args_&&... args) noexcept
			{ return mA.fetch_sub(PL_FWD(args)...); }

		template <typename... Args_>
		auto fetch_and(Args_&&... args) noexcept
			{ return mA.fetch_and(PL_FWD(args)...); }

		template <typename... Args_>
		auto fetch_or(Args_&&... args) noexcept
			{ return mA.fetch_or(PL_FWD(args)...); }

		template <typename... Args_>
		auto fetch_xor(Args_&&... args) noexcept
			{ return mA.fetch_xor(PL_FWD(args)...); }

		template <typename TT_ = AtomicType_>
		auto operator++() noexcept
			{ return ++mA; }

		template <typename TT_ = AtomicType_>
		auto operator++(int) noexcept
			{ return mA++; }

		template <typename TT_ = AtomicType_>
		auto operator--() noexcept
			{ return --mA; }

		template <typename TT_ = AtomicType_>
		auto operator--(int) noexcept
			{ return mA--; }

		template <typename TT_ = AtomicType_>
		auto operator+=(value_type v) noexcept
			{ return mA += v; }

		template <typename TT_ = AtomicType_>
		auto operator-=(value_type v) noexcept
			{ return mA -= v; }

		template <typename TT_ = AtomicType_>
		auto operator&=(value_type v) noexcept
			{ return mA &= v; }

		template <typename TT_ = AtomicType_>
		auto operator|=(value_type v) noexcept
			{ return mA |= v; }

		template <typename TT_ = AtomicType_>
		auto operator^=(value_type v) noexcept
			{ return mA ^= v; }

		template <typename TT_ = AtomicType_>
		auto operator+=(const atomic_wrapper &v) noexcept
			{ return mA += v.load(); }

		template <typename TT_ = AtomicType_>
		auto operator-=(const atomic_wrapper &v) noexcept
			{ return mA -= v.load(); }

		template <typename TT_ = AtomicType_>
		auto operator&=(const atomic_wrapper &v) noexcept
			{ return mA &= v.load(); }

		template <typename TT_ = AtomicType_>
		auto operator|=(const atomic_wrapper &v) noexcept
			{ return mA |= v.load(); }

		template <typename TT_ = AtomicType_>
		auto operator^=(const atomic_wrapper &v) noexcept
			{ return mA ^= v.load(); }

	private:
		AtomicType_ mA{};
	};

	template <typename T_ = int>
	class round_robin_mutex
	{
	private:
		std::atomic<T_> mSpinToWin{};
		mutable std::mutex mSpinMutex;
		T_ mNb{};

	public:

		bool try_lock(T_ n, std::invocable auto&& fc)
		{
			if(T_ currentSpin = mSpinToWin; currentSpin == n)
			{
				PL_FWD(fc)();

				std::scoped_lock g{mSpinMutex};

				if(++currentSpin >= mNb)
					currentSpin = 0;

				mSpinToWin = currentSpin;

				return true;
			}

			return false;
		}

		void lock(T_ n, std::invocable auto&& fc, std::invocable auto&& wf)
		{
			while(!try_lock(n, PL_FWD(fc))) { PL_FWD(wf)(); }
		}

		void reset()
		{
			std::scoped_lock g{mSpinMutex};

			mSpinToWin = 0;
		}

		void resize(T_ nb)
		{
			std::scoped_lock g{mSpinMutex};

			mNb = nb;

			atomic_exchange(mSpinToWin, 0, [nb](auto&& prev, auto&&){ return prev >= nb; });
		}
	};

	void atomic_fence(std::invocable auto&& fc) noexcept(noexcept(fc()))
	{
		std::atomic_thread_fence(std::memory_order::relaxed);
			PL_FWD(fc)();
		std::atomic_thread_fence(std::memory_order::relaxed);
	}

	decltype(auto) atomic_fence_return(std::invocable auto&& fc) noexcept(noexcept(fc()))
	{
		std::atomic_thread_fence(std::memory_order::relaxed);
			decltype(auto) value = PL_FWD(fc)();
		std::atomic_thread_fence(std::memory_order::relaxed);

		return PL_FWD_RETURN(value);
	}
}
