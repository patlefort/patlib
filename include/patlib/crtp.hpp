/*
	PatLib

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <type_traits>
#include <utility>

namespace patlib
{
	#define PL_CRTP_BASE(DerivedType_) \
		[[nodiscard]] constexpr const auto &derived() const& { return static_cast<const DerivedType_&>(*this); } \
		[[nodiscard]] constexpr auto &derived() & { return static_cast<DerivedType_&>(*this); } \
		[[nodiscard]] constexpr const auto &&derived() const&& { return static_cast<const DerivedType_&&>(*this); } \
		[[nodiscard]] constexpr auto &&derived() && { return static_cast<DerivedType_&&>(*this); }

	#define PL_CRTP_DERIVED(BaseType_) \
		[[nodiscard]] constexpr const auto &base() const& { return static_cast<const BaseType_&>(*this); } \
		[[nodiscard]] constexpr auto &base() & { return static_cast<BaseType_&>(*this); } \
		[[nodiscard]] constexpr const auto &&base() const&& { return static_cast<const BaseType_&&>(*this); } \
		[[nodiscard]] constexpr auto &&base() && { return static_cast<BaseType_&&>(*this); }

	#define PL_CRTP_BASE_VIR(DerivedType_) \
		[[nodiscard]] const auto &derived() const& { return static_cast<const DerivedType_&>(*this); } \
		[[nodiscard]] auto &derived() & { return static_cast<DerivedType_&>(*this); } \
		[[nodiscard]] const auto &&derived() const&& { return static_cast<const DerivedType_&&>(*this); } \
		[[nodiscard]] auto &&derived() && { return static_cast<DerivedType_&&>(*this); }

	#define PL_CRTP_DERIVED_VIR(BaseType_) \
		[[nodiscard]] const auto &base() const& { return static_cast<const BaseType_&>(*this); } \
		[[nodiscard]] auto &base() & { return static_cast<BaseType_&>(*this); } \
		[[nodiscard]] const auto &&base() const&& { return static_cast<const BaseType_&&>(*this); } \
		[[nodiscard]] auto &&base() && { return static_cast<BaseType_&&>(*this); }

	template <typename DerivedType_>
	class crtp_base
	{
	public:
		PL_CRTP_BASE(DerivedType_)
	};

	template <>
	class crtp_base<void> {};

	namespace impl
	{
		template <typename DerivedType_, typename ThisType_>
		using crtp_typehelper_t = std::conditional_t<std::is_void_v<DerivedType_>, ThisType_, DerivedType_>;
	} // namespace impl

	template <typename DerivedType_, typename ThisType_, template <typename...> class BaseType_, typename... Args_>
	class crtp_class : public BaseType_<impl::crtp_typehelper_t<DerivedType_, ThisType_>, Args_...>
	{
	public:
		using real_derived_type = impl::crtp_typehelper_t<DerivedType_, ThisType_>;
		using real_base_type = BaseType_<real_derived_type, Args_...>;

		using real_base_type::real_base_type;

		PL_CRTP_DERIVED(real_base_type)
	};

	#define PL_CRTP_CLASS_DECLARE(...) ::patlib::crtp_class<__VA_ARGS__> \
		{ \
		public: \
		 \
			using crtp_base_type = ::patlib::crtp_class<__VA_ARGS__>; \
			using base_type = typename crtp_base_type::real_base_type; \
			using derived_type = typename crtp_base_type::real_derived_type; \
			 \
			using crtp_base_type::derived; \
			using crtp_base_type::base;

	#define PL_CRTP_CLASS_END };

	#define PL_CRTP_CLASS(classname, basename, ...) \
		class classname : public PL_CRTP_CLASS_DECLARE(DerivedType_, classname<DerivedType_, __VA_ARGS__>, basename, __VA_ARGS__)

	#define PL_CRTP_CLASS_FINAL(classname, basename, ...) \
		class classname : public PL_CRTP_CLASS_DECLARE(void, classname<__VA_ARGS__>, basename, __VA_ARGS__)

	#define PL_CRTP_PRIVATE_MIXIN(Type) \
		private: \
			 PL_CRTP_BASE(Type)
}
