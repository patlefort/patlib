/*
	PatLib

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "base.hpp"
#include "concepts.hpp"
#include "utility.hpp"
#include "type.hpp"

#include <type_traits>
#include <utility>
#include <tuple>
#include <iterator>
#include <cstddef>
#include <compare>

#include <range/v3/iterator/concepts.hpp>
#include <range/v3/iterator/traits.hpp>

namespace patlib
{
	namespace impl
	{
		template <typename T_, typename It_>
		concept CConvertibleToValueOrRef =
			std::convertible_to<T_, std::iter_value_t<It_>> || std::convertible_to<T_, std::iter_reference_t<It_>>;
	} // namespace impl

	template <typename T_>
	concept CForwardValueIterator =
		rg::input_iterator<T_> &&
		requires(T_ i)
		{
			{ i++ } -> std::same_as<T_>;
			{ *i++ } -> impl::CConvertibleToValueOrRef<T_>;
		};

	template <typename T_>
	concept CBidirectionalValueIterator =
		CForwardValueIterator<T_> &&
		requires(T_ i)
		{
			{ --i } -> std::same_as<T_&>;
			{ i-- } -> std::same_as<T_>;
			{ *i-- } -> impl::CConvertibleToValueOrRef<T_>;
		};

	template <typename T_>
	concept CRandomAccessValueIterator =
		CBidirectionalValueIterator<T_> &&
		rg::totally_ordered<T_> &&
		rg::sized_sentinel_for<T_, T_> &&
		requires(T_ i, rg::iter_difference_t<T_> n)
		{
			{ i + n } -> std::same_as<T_>;
			{ n + i } -> std::same_as<T_>;
			{ i - n } -> std::same_as<T_>;
			{ i += n } -> std::same_as<T_&>;
			{ i -= n } -> std::same_as<T_&>;
			{ i - i } -> std::same_as<decltype(n)>;
			{ i[n] } -> impl::CConvertibleToValueOrRef<T_>;
		};

	template <typename T_, typename ValueType_>
	concept COutputValueIterator =
		rg::input_or_output_iterator<T_> &&
		requires(T_ i, ValueType_ v)
		{
			{ *i = v };
			{ ++i } -> std::same_as<T_&>;
			{ i++ } -> std::convertible_to<const T_ &>;
			{ *i++ = v };
		};

	template<typename T_>
	using iter_arrow_t = decltype(std::declval<T_ &>().operator->());

	template<typename T_>
	concept CIterArrow = requires(T_ p) { { p.operator->() }; };

	template<typename T_>
	using iter_pointer_t = deferred_if<CIterArrow<T_>, defer<iter_arrow_t, T_>, std::add_pointer_t<rg::iter_reference_t<T_>>>;

	// Iterate a container with an index
	template <typename VT_, typename IndexType_>
	class index_vector_iterator
	{
	private:

		VT_ *mVec{};
		IndexType_ mIndex{};

	public:
		using value_type = typename VT_::value_type;
		using pointer = std::conditional_t<std::is_const_v<VT_>, typename VT_::const_pointer, typename VT_::pointer>;
		using reference = std::conditional_t<std::is_const_v<VT_>, typename VT_::const_reference, typename VT_::reference>;
		using iterator_category = std::random_access_iterator_tag;
		using difference_type = typename VT_::difference_type;

		constexpr index_vector_iterator() = default;
		constexpr index_vector_iterator(const index_vector_iterator &) = default;
		constexpr index_vector_iterator(VT_ &vec) noexcept : mVec{&vec} {}
		constexpr index_vector_iterator(VT_ &vec, const IndexType_ i_) noexcept : mVec{&vec}, mIndex{i_} {}

		template <typename OVT_>
		constexpr index_vector_iterator(const index_vector_iterator<OVT_, IndexType_> &other) noexcept
			requires (std::is_const_v<VT_> && !std::is_const_v<OVT_>)
			: mVec{&other.vector()}, mIndex{other.offset()} {}

		template <typename OVT_>
		constexpr index_vector_iterator &operator= (const index_vector_iterator<OVT_, IndexType_> &other) noexcept
			requires (std::is_const_v<VT_> && !std::is_const_v<OVT_>)
		{
			mIndex = other.offset();
		}

		[[nodiscard]] constexpr decltype(auto) operator *() const noexcept(noexcept((*mVec)[mIndex])) { return PL_FWD_RETURN((*mVec)[mIndex]); }

		[[nodiscard]] constexpr IndexType_ offset() const noexcept { return mIndex; }
		[[nodiscard]] constexpr VT_ &vector() const noexcept { return *mVec; }

		constexpr auto &operator-- () noexcept
		{
			--mIndex;
			return *this;
		}

		constexpr auto &operator++ () noexcept
		{
			++mIndex;
			return *this;
		}

		constexpr auto operator-- (int) noexcept
		{
			auto cp = *this;
			--(*this);
			return cp;
		}

		constexpr auto operator++ (int) noexcept
		{
			auto cp = *this;
			++(*this);
			return cp;
		}

		[[nodiscard]] constexpr auto *operator->() { return &(*mVec)[mIndex]; }
		[[nodiscard]] constexpr const auto *operator->() const { return &(*mVec)[mIndex]; }

		[[nodiscard]] constexpr decltype(auto) operator [] (const difference_type i) const noexcept { return *(*this + i); }

		[[nodiscard]] constexpr bool operator== (const auto &other) const noexcept { return offset() == other.offset(); }
		[[nodiscard]] constexpr auto operator<=> (const auto &other) const noexcept { return std::compare_three_way{}(offset(), other.offset()); }

		constexpr auto &operator += (const difference_type n) noexcept { mIndex += n; return *this; }
		constexpr auto &operator -= (const difference_type n) noexcept { mIndex -= n; return *this; }

		[[nodiscard]] constexpr auto operator -(const difference_type n) const noexcept { auto nit = *this; return (nit -= n); }

		[[nodiscard]] constexpr difference_type operator - (const index_vector_iterator &other) const noexcept { return (difference_type)offset() - (difference_type)other.offset(); }

	};

	template <typename VT_, typename IndexType_>
	[[nodiscard]] constexpr auto operator +(index_vector_iterator<VT_, IndexType_> it, const typename index_vector_iterator<VT_, IndexType_>::difference_type n) noexcept { return (it += n); }

	template <typename VT_, typename IndexType_>
	[[nodiscard]] constexpr auto operator +(const typename index_vector_iterator<VT_, IndexType_>::difference_type n, index_vector_iterator<VT_, IndexType_> it) noexcept { return (it += n); }


	template <typename IT_>
	class map_iterator_iterator
	{
	private:
		IT_ mIt{};

	public:
		using value_type = IT_;
		using pointer = IT_ *;
		using reference = IT_;
		using iterator_category = typename iterator_traits<IT_>::iterator_category;
		using difference_type = typename iterator_traits<IT_>::difference_type;

		constexpr map_iterator_iterator() = default;
		constexpr map_iterator_iterator(const map_iterator_iterator &other) = default;

		template <typename OT_>
		constexpr map_iterator_iterator(const map_iterator_iterator<OT_> &other)
			: mIt{*other} {}

		constexpr map_iterator_iterator(IT_ i) : mIt{i} {}

		[[nodiscard]] constexpr reference operator *() const noexcept { return mIt; }

		template <typename T_ = IT_>
		constexpr auto &operator-- ()
		{
			--mIt;
			return *this;
		}

		template <typename T_ = IT_>
		constexpr auto &operator++ ()
		{
			++mIt;
			return *this;
		}

		template <typename T_ = IT_>
		constexpr auto operator-- (int)
		{
			auto cp = *this;
			--(*this);
			return cp;
		}

		template <typename T_ = IT_>
		constexpr auto operator++ (int)
		{
			auto cp = *this;
			++(*this);
			return cp;
		}

		[[nodiscard]] constexpr decltype(auto) operator->() { return mIt.operator->(); }
		[[nodiscard]] constexpr decltype(auto) operator->() const { return mIt.operator->(); }

		template <typename T_ = IT_>
		[[nodiscard]] constexpr reference operator [] (const difference_type i) const noexcept { return *(*this + i); }

		[[nodiscard]] constexpr bool operator== (const auto &other) const { return mIt == *other; }
		[[nodiscard]] constexpr bool operator!= (const auto &other) const { return mIt != *other; }
		[[nodiscard]] constexpr auto operator<=> (const map_iterator_iterator &other) const
				requires std::three_way_comparable<IT_>
			{ return std::compare_three_way{}(mIt, *other); }

		template <typename T_ = IT_>
		constexpr auto &operator += (const difference_type n) { mIt += n; return *this; }

		template <typename T_ = IT_>
		constexpr auto &operator -= (const difference_type n) { mIt -= n; return *this; }

		template <typename T_ = IT_>
		[[nodiscard]] constexpr auto operator -(const difference_type n) const { auto nit = *this; return (nit -= n); }

		template <typename T_ = IT_>
		[[nodiscard]] constexpr difference_type operator - (const map_iterator_iterator &other) const
			{ return mIt - *other; }

	};

	template <typename IT_>
	[[nodiscard]] constexpr auto operator +(map_iterator_iterator<IT_> it, const typename map_iterator_iterator<IT_>::difference_type n) { return (it += n); }

	template <typename IT_>
	[[nodiscard]] constexpr auto operator +(const typename map_iterator_iterator<IT_>::difference_type n, map_iterator_iterator<IT_> it) { return (it += n); }
}
