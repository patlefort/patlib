/*
	PatLib

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "base.hpp"
#include "concepts.hpp"
#include "type.hpp"
#include "endian.hpp"
#include "memory.hpp"
#include "algorithm.hpp"
#include "exception.hpp"
#include "crtp.hpp"
#include "scoped.hpp"
#include "string.hpp"
#include "range.hpp"

#include <istream>
#include <ostream>
#include <locale>
#include <vector>
#include <array>
#include <string>
#include <string_view>
#include <cstring>
#include <charconv>
#include <cstdlib>
#include <iomanip>
#include <functional>
#include <cerrno>
#include <bit>

#include <experimental/iterator>

#include <boost/endian/arithmetic.hpp>

#include <boost/iostreams/device/array.hpp>
#include <boost/iostreams/device/back_inserter.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/filtering_stream.hpp>

#include <boost/range/algorithm/copy.hpp>

#include <boost/algorithm/string.hpp>

//#include <boost/tokenizer.hpp>

#ifdef PL_FMT
	#include <fmt/format.h>
	#include <fmt/ostream.h>
#endif


namespace patlib::io
{
	PATLIB_API extern const int xindex;

	template <typename CharT_, typename... Ts_>
	concept CSerializableOut = requires(const Ts_... o, std::basic_ostream<CharT_> &os) { { (os << ... << o) } -> std::same_as<std::basic_ostream<CharT_>&>; };
	template <typename CharT_, typename... Ts_>
	concept CSerializableIn = requires(Ts_... o, std::basic_istream<CharT_> &is) { { (is >> ... >> o) } -> std::same_as<std::basic_istream<CharT_>&>; };

	template <typename CharT_, typename... Ts_>
	concept CSerializableBinaryOut = requires(const Ts_... o, std::basic_ostream<CharT_> &os) { { (os <= ... <= o) } -> std::same_as<std::basic_ostream<CharT_>&>; };
	template <typename CharT_, typename... Ts_>
	concept CSerializableBinaryIn = requires(Ts_... o, std::basic_istream<CharT_> &is) { { (is >= ... >= o) } -> std::same_as<std::basic_istream<CharT_>&>; };

	template <typename CharT_, typename... Ts_>
	concept CSerializable = CSerializableOut<CharT_, Ts_...> && CSerializableIn<CharT_, Ts_...>;

	inline std::locale try_get_locale(const char * const str) noexcept
	{
		try{
			return std::locale{str};
		}catch(...){}

		return {};
	}

	inline std::locale try_get_locale(const std::string &str) noexcept
	{
		try{
			return std::locale{str};
		}catch(...){}

		return {};
	}

	class indent
	{
	public:
		indent(std::streamsize v_) : v{v_} {}

		template <typename CharT_, typename Traits_>
		auto &operator() (std::basic_ostream<CharT_, Traits_> &out) const
		{
			repeat(v, [&]{ out << out.fill(); });
			return out;
		}

		template <typename CharT_, typename Traits_>
		friend auto &operator<<(std::basic_ostream<CharT_, Traits_> &out, indent v)
		{
			return v(out);
		}

	private:
		std::streamsize v;
	};

	template <typename CharT_, typename Traits_>
	void readline_trim(std::basic_istream<CharT_, Traits_> &is, std::basic_string<CharT_> &line, const std::locale &loc = {})
	{
		is >> std::ws;
		std::getline(is, line);
		trim(line, loc);
	}

	template <typename CharT_, typename Traits_>
	[[nodiscard]] std::basic_string<CharT_> readline_trim(std::basic_istream<CharT_, Traits_> &is, const std::locale &loc = {})
	{
		std::basic_string<CharT_> line;
		readline_trim(is, line, loc);

		return line;
	}

	template <typename CharT_, typename Traits_>
	[[nodiscard]] std::basic_string<CharT_> readline(std::basic_istream<CharT_, Traits_> &is)
	{
		std::basic_string<CharT_> line;
		std::getline(is, line);

		return line;
	}

	template <typename CharT_, typename Traits_>
	void skip_available_bytes(std::basic_istream<CharT_, Traits_> &is)
	{
		CharT_ buf[128];

		while(is.readsome(buf, sizeof buf));
	}

	class skip
	{
	public:
		skip(std::streamsize v = 1, int delim = std::istream::traits_type::eof()) : mV{v}, mDelim{delim} {}

		template <typename CharT_, typename Traits_>
		auto &operator() (std::basic_istream<CharT_, Traits_> &is) const
		{
			is.ignore(mV, mDelim);
			return is;
		}

		template <typename CharT_, typename Traits_>
		friend auto &operator>>(std::basic_istream<CharT_, Traits_> &in, skip v)
		{
			return v(in);
		}

	private:
		std::streamsize mV;
		int mDelim;
	};

	template <typename CharT_, typename Traits_>
	auto &skip_one(std::basic_istream<CharT_, Traits_> &is)
		{ is.ignore(); return is; }

	template <typename CharT_, typename Traits_>
	auto &skip_eol(std::basic_istream<CharT_, Traits_> &is)
	{
		if(is.peek() == '\n')
			is.ignore();

		if(is.peek() == '\r')
			is.ignore();

		return is;
	}

	constexpr long
		flagBinary =               0b1,
		flagEndianLittle =         0b1 << 1,
		flagEndianBig =            0b1 << 2,
		flagIPC =                  0b1 << 3,
		flagFpConvert =            0b1 << 4;

	inline std::ios_base &endian_little(std::ios_base &io)
	{
		io.iword(xindex) &= ~flagEndianBig;
		io.iword(xindex) |= flagEndianLittle;
		return io;
	}

	inline std::ios_base &endian_big(std::ios_base &io)
	{
		io.iword(xindex) &= ~flagEndianLittle;
		io.iword(xindex) |= flagEndianBig;
		return io;
	}

	inline std::ios_base &endian_native(std::ios_base &io)
	{
		io.iword(xindex) &= ~(flagEndianLittle | flagEndianBig);

		if constexpr(native_endian == std::endian::big)
			endian_big(io);
		else if constexpr(native_endian == std::endian::little)
			endian_little(io);

		return io;
	}

	inline std::ios_base &binary(std::ios_base &io)
	{
		io.iword(xindex) |= flagBinary;
		return io;
	}

	inline std::ios_base &text(std::ios_base &io)
	{
		io.iword(xindex) &= ~flagBinary;
		return io;
	}

	inline std::ios_base &ipc_on(std::ios_base &io)
	{
		io.iword(xindex) |= flagIPC;
		return io;
	}

	inline std::ios_base &ipc_off(std::ios_base &io)
	{
		io.iword(xindex) &= ~flagIPC;
		return io;
	}


	[[nodiscard]] inline bool is_binary(std::ios_base &io)
		{ return bool(io.iword(xindex) & flagBinary); }

	[[nodiscard]] inline bool is_endian_little(std::ios_base &io)
		{ return bool(io.iword(xindex) & flagEndianLittle); }

	[[nodiscard]] inline bool is_endian_big(std::ios_base &io)
		{ return bool(io.iword(xindex) & flagEndianBig); }

	[[nodiscard]] inline bool is_endian_native(std::ios_base &io)
	{
		if constexpr(native_endian == std::endian::big)
			return is_endian_big(io);
		else if constexpr(native_endian == std::endian::little)
			return is_endian_little(io);
		else
			return !is_endian_big(io) && !is_endian_little(io);
	}

	[[nodiscard]] inline bool is_ipc(std::ios_base &io)
		{ return bool(io.iword(xindex) & flagIPC); }

	[[nodiscard]] inline std::endian get_endian(std::ios_base &io)
	{
		if(is_endian_little(io))
			return std::endian::little;
		if(is_endian_big(io))
			return std::endian::big;

		return std::endian::native;
	}

	inline void set_endian(std::ios_base &io, std::endian endian)
	{
		if(endian == std::endian::little)
			endian_little(io);
		else if(endian == std::endian::big)
			endian_big(io);
		else
			endian_native(io);
	}

	template <typename CharT_, typename Traits_ = std::char_traits<CharT_>>
	class [[nodiscard]] scoped_exceptions_state : no_copy
	{
		std::basic_ios<CharT_, Traits_> &mIos;
		std::ios_base::iostate mOldState;

	public:
		scoped_exceptions_state(std::basic_ios<CharT_, Traits_> &ios) : mIos{ios}, mOldState{ios.exceptions()} {}

		scoped_exceptions_state(std::basic_ios<CharT_, Traits_> &ios, std::ios_base::iostate state) : mIos{ios}, mOldState{ios.exceptions()}
		{
			mIos.exceptions(state);
		}

		~scoped_exceptions_state()
		{
			mIos.exceptions(mOldState);
		}
	};

	template <typename CharT_, typename Traits_ = std::char_traits<CharT_>>
	class [[nodiscard]] scoped_rdstate_saver_all : no_copy
	{
		std::basic_ios<CharT_, Traits_> &mIos;
		std::ios_base::iostate mOldState;

	public:
		scoped_rdstate_saver_all(std::basic_ios<CharT_, Traits_> &ios) : mIos{ios}, mOldState{ios.rdstate()} {}

		~scoped_rdstate_saver_all()
			{ mIos.clear(mOldState); }
	};

	template <typename CharT_, typename Traits_ = std::char_traits<CharT_>>
	class [[nodiscard]] scoped_rdstate_saver : no_copy
	{
		std::basic_ios<CharT_, Traits_> &mIos;
		std::ios_base::iostate mOldState, mMask;

	public:
		scoped_rdstate_saver(std::basic_ios<CharT_, Traits_> &ios, std::ios_base::iostate mask) : mIos{ios}, mOldState{ios.rdstate()}, mMask{mask} {}

		~scoped_rdstate_saver()
			{ mIos.clear((mOldState & mMask) | (mIos.rdstate() & ~mMask)); }
	};

	template <typename CharT_, typename Traits_ = std::char_traits<CharT_>>
	class [[nodiscard]] scoped_format_flags_saver_all : no_copy
	{
		std::basic_ios<CharT_, Traits_> &mIos;
		std::ios_base::fmtflags mOldState;

	public:
		scoped_format_flags_saver_all(std::basic_ios<CharT_, Traits_> &ios) : mIos{ios}, mOldState{ios.flags()} {}

		~scoped_format_flags_saver_all()
			{ mIos.flags(mOldState); }
	};

	template <typename CharT_, typename Traits_ = std::char_traits<CharT_>>
	class [[nodiscard]] scoped_format_flags_saver : no_copy
	{
		std::basic_ios<CharT_, Traits_> &mIos;
		std::ios_base::fmtflags mOldState, mMask;

	public:
		scoped_format_flags_saver(std::basic_ios<CharT_, Traits_> &ios, std::ios_base::fmtflags mask) : mIos{ios}, mOldState{ios.flags()}, mMask{mask} {}

		~scoped_format_flags_saver()
			{ mIos.flags((mOldState & mMask) | (mIos.rdstate() & ~mMask)); }
	};

	template <typename CharT_, typename Traits_ = std::char_traits<CharT_>>
	void stream_safe_from(std::basic_ostream<CharT_, Traits_> &stream, auto&& fc)
	{
		std::ostream newStream{stream.rdbuf()};
		newStream.copyfmt(stream);

		PL_FWD(fc)(newStream);
	}

	template <typename CharT_, typename Traits_ = std::char_traits<CharT_>>
	void stream_safe_from(std::basic_istream<CharT_, Traits_> &stream, auto&& fc)
	{
		std::istream newStream{stream.rdbuf()};
		newStream.copyfmt(stream);

		PL_FWD(fc)(newStream);
	}


	// Need this to read hex floats until it is supported by std::istream.
	template <typename T_>
	class read_value_wrapper : no_copy
	{
	private:
		using decayed_type = decay_t<T_>;
		T_ mValue;

	public:

		constexpr read_value_wrapper(T_ v) : mValue{PL_FWD(v)} {}

		template <typename CharT_, typename Traits_>
			requires CSerializableIn<CharT_, T_>
		auto &operator() (std::basic_istream<CharT_, Traits_> &is)
		{
			if constexpr(std::same_as<CharT_, char> && std::is_floating_point_v<decayed_type>)
			{
				std::array<char, 1024> buf;
				buf[buf.size() - 1] = '\0';

				std::istream::sentry st{is};

				if(!st)
					return is;

				auto it = buf.begin();
				for(; it != buf.end() - 1; ++it)
				{
					const auto ch = is.rdbuf()->sgetc();

					if(ch == std::istream::traits_type::eof() || std::isspace(ch))
						break;

					is.get(*it);
					if(!is)
						break;
				}

				*it = '\0';

				if constexpr(std::is_same_v<decayed_type, float>)
					mValue = std::strtof(buf.data(), nullptr);
				else if constexpr(std::is_same_v<decayed_type, double>)
					mValue = std::strtod(buf.data(), nullptr);
				else if constexpr(std::is_same_v<decayed_type, long double>)
					mValue = std::strtold(buf.data(), nullptr);
				else
					static_assert(dependent_false<decayed_type>, "Unsupported floating point type.");

				if(errno == ERANGE)
					throw Exception("Value out of range.");
				/*else if(errno)
					throw Exception{std::string{"Failed parsing value: "} + std::strerror(errno)};*/

				/*const auto res = std::from_chars(buf.data(), std::addressof(*it), mValue, std::chars_format::hex);

				if(res.ec == std::errc::result_out_of_range)
					throw Exception("Value out of range.");
				else if(res.ptr == buf.data())
					throw Exception("Failed parsing floating-point value.");*/
			}
			else
			{
				if constexpr(std::is_same_v<decayed_type, char> || std::is_same_v<decayed_type, unsigned char>)
				{
					int tmp;
					is >> tmp;
					if(is)
						mValue = static_cast<decayed_type>(tmp);
				}
				else
					is >> mValue;
			}

			return is;
		}

		template <typename CharT_, typename Traits_>
		friend auto &operator>>(std::basic_istream<CharT_, Traits_> &in, read_value_wrapper&& v)
		{
			return v(in);
		}

		template <typename CharT_, typename Traits_>
		[[noreturn]] friend auto &operator>=(std::basic_istream<CharT_, Traits_> &, read_value_wrapper&&)
		{
			static_assert(dependent_false<CharT_>, "read_value_wrapper shouldn't be used with binary serialization.");
		}
	};
	template <typename T_>
	read_value_wrapper(T_ &&) -> read_value_wrapper<T_ &&>;
	template <typename T_>
	read_value_wrapper(T_ &) -> read_value_wrapper<T_ &>;


	template <std::floating_point T_>
	struct write_hex_float
	{
		const T_ &v;

		template <typename CharT_, typename Traits_>
		auto &operator() (std::basic_ostream<CharT_, Traits_> &os) const
		{
			if constexpr(std::same_as<CharT_, char>)
			{
				std::array<char, 1024> chs;

				auto res = std::to_chars(chs.data(), chs.data() + chs.size(), v, std::chars_format::hex);

				if(res.ec != std::errc{})
					throw Exception{std::string{"Failed reading floating-point value: "} + std::make_error_condition(res.ec).message()};

				os.write(chs.data(), static_cast<std::streamsize>(res.ptr - chs.data()));
			}
			else
			{
				scoped_format_flags_saver ss{os, std::ios_base::floatfield};
				os << std::hexfloat << v;
			}

			return os;
		}

		template <typename CharT_, typename Traits_>
		friend auto &operator<<(std::basic_ostream<CharT_, Traits_> &os, write_hex_float&& v)
		{
			return v(os);
		}

		template <typename CharT_, typename Traits_>
		[[noreturn]] friend auto &operator<=(std::basic_ostream<CharT_, Traits_> &, write_hex_float&&)
		{
			static_assert(dependent_false<CharT_>, "write_hex_float shouldn't be used with binary serialization.");
		}
	};
	template <typename T_>
	write_hex_float(const T_ &) -> write_hex_float<T_>;

	namespace rawio
	{
		template <typename T_, typename CharT_, typename Traits_>
		std::basic_ostream<CharT_, Traits_> &operator<= (std::basic_ostream<CharT_, Traits_> &os, const T_ &data)
		{
			if constexpr(std::is_arithmetic_v<T_>)
			{
				const auto checkAndWrite = [&](auto Order_)
					{
						using endianbuf = boost::endian::endian_arithmetic<Order_, T_, sizeof(T_) * 8, boost::endian::align::yes>;
						endianbuf vbuf{data};
						os.write(reinterpret_cast<const char *>(vbuf.data()), sizeof(typename endianbuf::value_type));
					};

				if(is_endian_little(os))
					checkAndWrite(endian_constant_boost<boost::endian::order::little>);
				else if(is_endian_big(os))
					checkAndWrite(endian_constant_boost<boost::endian::order::big>);
				else
					checkAndWrite(endian_constant_boost<boost::endian::order::native>);
			}
			else
				os.write(reinterpret_cast<const char *>(&data), sizeof(T_));

			return os;
		}

		template <typename T_, typename CharT_, typename Traits_>
		std::basic_istream<CharT_, Traits_> &operator>= (std::basic_istream<CharT_, Traits_> &is, T_ &data)
		{
			if constexpr(std::is_arithmetic_v<T_>)
			{
				const auto read_value_impl = [&]<boost::endian::order Order_>()
					{
						using endianbuf = boost::endian::endian_arithmetic<Order_, T_, sizeof(T_) * 8, boost::endian::align::yes>;

						endianbuf vbuf{};
						is >= vbuf;

						return vbuf.value();
					};

				if(is_endian_little(is))
					data = read_value_impl.template operator()<boost::endian::order::little>();
				else if(is_endian_big(is))
					data = read_value_impl.template operator()<boost::endian::order::big>();
				else
					data = read_value_impl.template operator()<boost::endian::order::native>();
			}
			else
				is.read(reinterpret_cast<char *>(&data), sizeof(T_));

			return is;
		}

		template <typename T_>
		struct raw : no_copy
		{
			constexpr raw(T_ v) : mValue{PL_FWD(v)} {}

			T_ mValue;

			template <typename CharT_, typename Traits_>
			friend std::basic_istream<CharT_, Traits_> &operator>= (std::basic_istream<CharT_, Traits_> &is, raw &&data)
				{ return is >= data.mValue; };

			template <typename CharT_, typename Traits_>
			friend std::basic_ostream<CharT_, Traits_> &operator<= (std::basic_ostream<CharT_, Traits_> &os, const raw &data)
				{ return os <= data.mValue; };
		};
		template <typename T_>
		raw(T_&) -> raw<T_&>;
		template <typename T_>
		raw(T_&&) -> raw<T_&&>;

	} // namespace rawio

	template <typename T_, typename CharT_, typename Traits_>
	[[nodiscard]] auto read_value(std::basic_istream<CharT_, Traits_> &is, type_c<T_> = {})
	{
		T_ v;
		is >> read_value_wrapper{v};
		return v;
	}

	inline constexpr auto failure_bits = std::ios_base::failbit | std::ios_base::badbit;

	struct failure_t
	{
		friend constexpr bool operator==(std::ios_base::iostate state, failure_t) noexcept
			{ return state & failure_bits; }
		friend constexpr bool operator==(failure_t, std::ios_base::iostate state) noexcept
			{ return state & failure_bits; }
	};

	inline constexpr auto failure = failure_t{};

	struct throw_on_p : public param<std::ios_base::iostate> { using param<std::ios_base::iostate>::param; };

	inline constexpr auto throw_on_failure = throw_on_p{failure_bits};

	template <typename CT_>
	[[nodiscard]] constexpr auto stream_into(CT_ &&container)
	{
		using dev_type = boost::iostreams::back_insert_device<decay_t<CT_>>;
		return boost::iostreams::stream<dev_type>{dev_type{container}, 0, 0};
	}

	/*template <typename CT_>
	[[nodiscard]] constexpr auto stream_into(CT_ &&container, throw_on_p exceptions)
	{
		auto ss = stream_into(PL_FWD(container));
		ss.exceptions(*exceptions);
		return ss;
	}*/

	template <typename CT_, typename FC_>
		requires (!std::same_as<std::decay_t<FC_>, throw_on_p>)
	constexpr decltype(auto) stream_into(CT_ &&container, FC_ &&fc)
		{ return std::invoke(PL_FWD(fc), stream_into(PL_FWD(container)), PL_FWD(container)); }

	template <typename CT_>
	constexpr decltype(auto) stream_into(CT_ &&container, throw_on_p exceptions, auto &&fc)
	{
		auto ss = stream_into(PL_FWD(container));
		ss.exceptions(*exceptions);
		return std::invoke(PL_FWD(fc), ss, PL_FWD(container));
	}

	template <rg::contiguous_range CT_>
		requires rg::sized_range<CT_>
	[[nodiscard]] constexpr auto stream_from(CT_ &&container)
	{
		return boost::iostreams::stream<boost::iostreams::basic_array_source<rg::range_value_t<CT_>>>
			{rg::data(container), rg::size(container)};
	}

	/*template <rg::contiguous_range CT_>
		requires rg::sized_range<CT_>
	[[nodiscard]] constexpr auto stream_from(CT_ &&container, throw_on_p exceptions)
	{
		auto ss = stream_from(PL_FWD(container));
		ss.exceptions(*exceptions);
		return ss;
	}*/

	template <rg::contiguous_range CT_, typename FC_>
		requires (rg::sized_range<CT_> && !std::same_as<std::decay_t<FC_>, throw_on_p>)
	constexpr decltype(auto) stream_from(CT_ &&container, FC_ &&fc)
		{ return std::invoke(PL_FWD(fc), stream_from(PL_FWD(container)), PL_FWD(container)); }

	template <rg::contiguous_range CT_>
		requires rg::sized_range<CT_>
	constexpr decltype(auto) stream_from(CT_ &&container, throw_on_p exceptions, auto &&fc)
	{
		auto ss = stream_from(PL_FWD(container));
		ss.exceptions(*exceptions);
		return std::invoke(PL_FWD(fc), ss, PL_FWD(container));
	}

	struct uncompressed_t {};
	constexpr uncompressed_t uncompressed{};

	template <typename CharT_, typename Traits_>
	void write_compressed(std::basic_ostream<CharT_, Traits_> &os, auto&& compressor, auto &&fc)
	{
		boost::iostreams::filtering_stream<boost::iostreams::output, CharT_, Traits_> filterBuffer;
		filterBuffer.push(PL_FWD(compressor));
		filterBuffer.push(os);

		filterBuffer.copyfmt(os);

		PL_FWD(fc)(filterBuffer);
	}

	template <typename CharT_, typename Traits_>
	void read_compressed(std::basic_istream<CharT_, Traits_> &is, auto&& decompressor, auto &&fc)
	{
		boost::iostreams::filtering_stream<boost::iostreams::input, CharT_, Traits_> filterBuffer;
		filterBuffer.push(PL_FWD(decompressor));
		filterBuffer.push(is);

		filterBuffer.copyfmt(is);

		PL_FWD(fc)(filterBuffer);
	}

	template <typename CharT_, typename Traits_>
	void read_stream(std::basic_istream<CharT_, Traits_> &is, auto&& fc, auto&& decompressor = uncompressed)
	{
		if constexpr(is_a<decltype(decompressor), uncompressed_t>)
			PL_FWD(fc)(is);
		else
			read_compressed(is, PL_FWD(decompressor), PL_FWD(fc));
	}

	template <typename CharT_, typename Traits_>
	void write_stream(std::basic_ostream<CharT_, Traits_> &os, auto&& fc, auto&& compressor = uncompressed)
	{
		if constexpr(is_a<decltype(compressor), uncompressed_t>)
			PL_FWD(fc)(os);
		else
			write_compressed(os, PL_FWD(compressor), PL_FWD(fc));
	}

	namespace impl
	{
		struct mode_tag_base {};
		struct direction_tag_base {};
	} // namespace impl

	PL_MAKE_DERIVED_TAG(mode_text,   impl::mode_tag_base)
	PL_MAKE_DERIVED_TAG(mode_binary, impl::mode_tag_base)
	PL_MAKE_DERIVED_TAG(mode_select, impl::mode_tag_base)

	template <typename T_>
	concept CMode = std::is_base_of_v<impl::mode_tag_base, T_>;

	PL_MAKE_DERIVED_TAG(dir_in,  impl::direction_tag_base)
	PL_MAKE_DERIVED_TAG(dir_out, impl::direction_tag_base)

	template <typename T_>
	concept CDirection = std::is_base_of_v<impl::direction_tag_base, T_>;

	template <typename T_, typename CharT_>
	constinit const bool is_streamable_tuple_in = false;
	template <typename CharT_, typename... Ts_>
		requires CSerializableIn<CharT_, Ts_...>
	constinit const bool is_streamable_tuple_in<std::tuple<Ts_...>, CharT_> = true;

	template <typename T_, typename CharT_>
	constinit const bool is_streamable_tuple_out = false;
	template <typename CharT_, typename... Ts_>
		requires CSerializableOut<CharT_, Ts_...>
	constinit const bool is_streamable_tuple_out<std::tuple<Ts_...>, CharT_> = true;

	template <typename T_, typename CharT_>
	concept CSreamableTupleIn = CTuple<T_> && is_streamable_tuple_in<T_, CharT_>;
	template <typename T_, typename CharT_>
	concept CSreamableTupleOut = CTuple<T_> && is_streamable_tuple_out<T_, CharT_>;

	inline constexpr auto yield = [](auto, auto&& fc, auto&&... v)
		noexcept(noexcept(std::invoke(PL_FWD(fc), PL_FWD(v)...)))
		{ std::invoke(PL_FWD(fc), PL_FWD(v)...); };

	using yield_t = decltype(yield);

	template <typename T_>
	concept CYield = std::same_as<T_, yield_t>;

	template <typename T_>
	concept CStreamProjectorBase =
		requires { typename T_::Mode; } &&
		CMode<typename T_::Mode>;

	template <typename T_, typename CharT_>
	concept CStreamProjectorInBinary =
		CStreamProjectorBase<T_> && CSerializableBinaryIn<CharT_, T_> &&
		requires(T_ p, std::basic_istream<CharT_> is)
		{
			{ p(is, mode_binary) };
		};
	template <typename T_, typename CharT_>
	concept CStreamProjectorInText =
		CStreamProjectorBase<T_> && CSerializableIn<CharT_, T_> &&
		requires(T_ p, std::basic_istream<CharT_> is)
		{
			{ p(is, mode_text) };
		};
	template <typename T_, typename CharT_>
	concept CStreamProjectorIn = CStreamProjectorInBinary<T_, CharT_> || CStreamProjectorInText<T_, CharT_>;
	template <typename T_, typename CharT_>
	concept CStreamProjectorOutBinary =
		CStreamProjectorBase<T_> && CSerializableBinaryOut<CharT_, T_> &&
		requires(T_ p, std::basic_ostream<CharT_> os)
		{
			{ p(os, mode_binary) };
		};
	template <typename T_, typename CharT_>
	concept CStreamProjectorOutText =
		CStreamProjectorBase<T_> && CSerializableOut<CharT_, T_> &&
		requires(T_ p, std::basic_ostream<CharT_> os)
		{
			{ p(os, mode_text) };
		};
	template <typename T_, typename CharT_>
	concept CStreamProjectorOut = CStreamProjectorOutBinary<T_, CharT_> || CStreamProjectorOutText<T_, CharT_>;

	template <typename T_, typename CharT_>
	concept CStreamProjector = CStreamProjectorIn<T_, CharT_> || CStreamProjectorOut<T_, CharT_>;

	namespace impl
	{
		template <typename CharT_, typename Traits_, typename T_>
		void reader(type_c<T_>, mode_text_t, std::basic_istream<CharT_, Traits_> &is, auto &into)
		{
			T_ v;
			is >> read_value_wrapper{v};
			into = std::move(v);
		}

		template <typename CharT_, typename Traits_, typename T_>
		void reader(type_c<T_>, mode_text_t, std::basic_istream<CharT_, Traits_> &, auto &into, auto&& v)
		{
			into = std::move(v);
		}

		template <typename CharT_, typename Traits_, typename T_>
		void reader(type_c<T_>, mode_text_t, std::basic_istream<CharT_, Traits_> &is, auto &into, auto&& v, auto&& proj)
		{
			is >> read_value_wrapper{PL_FWD(proj)};
			if(is)
				into = std::move(v);
		}

		template <typename CharT_, typename Traits_, typename T_>
		void reader(type_c<T_>, mode_binary_t, std::basic_istream<CharT_, Traits_> &is, auto &into)
		{
			T_ v;
			is >= v;
			into = std::move(v);
		}

		template <typename CharT_, typename Traits_, typename T_>
		void reader(type_c<T_>, mode_binary_t, std::basic_istream<CharT_, Traits_> &, auto &into, auto&& v)
		{
			into = std::move(v);
		}

		template <typename CharT_, typename Traits_, typename T_>
		void reader(type_c<T_>, mode_binary_t, std::basic_istream<CharT_, Traits_> &is, auto &into, auto&& v, auto&& proj)
		{
			is >= PL_FWD(proj);
			if(is)
				into = std::move(v);
		}

		template <typename CharT_, typename Traits_>
		auto projector_fc(std::basic_ios<CharT_, Traits_> &ios, auto iofc) noexcept
		{
			return [&ios, iofc = std::move(iofc)](auto&&... args){ iofc(PL_FWD(args)...); return ios.rdstate(); };
		}

		template <typename Derived_>
		class streamable_out
		{
			PL_CRTP_BASE(Derived_)

			template <typename CharT_, typename Traits_>
			friend std::basic_ostream<CharT_, Traits_> &operator<<(std::basic_ostream<CharT_, Traits_> &os, Derived_ v)
				{ return v(os); }

			template <typename CharT_, typename Traits_>
			friend std::basic_ostream<CharT_, Traits_> &operator<=(std::basic_ostream<CharT_, Traits_> &os, Derived_ v)
				{ return v(os); }
		};

		template <typename Derived_>
		class streamable_in
		{
			PL_CRTP_BASE(Derived_)

			template <typename CharT_, typename Traits_>
			friend std::basic_istream<CharT_, Traits_> &operator>>(std::basic_istream<CharT_, Traits_> &in, Derived_ v)
				{ return v(in); }

			template <typename CharT_, typename Traits_>
			friend std::basic_istream<CharT_, Traits_> &operator>=(std::basic_istream<CharT_, Traits_> &in, Derived_ v)
				{ return v(in); }
		};

		template <typename Derived_>
		class streamable_io : streamable_out<Derived_>, streamable_in<Derived_> {};

		template <typename Derived_>
		class mode_selector
		{
		private:
			PL_CRTP_BASE(Derived_)

			template <typename Der_, typename StreamT_>
			static StreamT_ &select_mode(Der_&& d, StreamT_ &s)
			{
				using Mode = typename decay_t<Der_>::Mode;

				if constexpr(std::same_as<Mode, mode_select_t>)
				{
					if(is_binary(s))
						PL_FWD(d)(s, mode_binary);
					else
						PL_FWD(d)(s, mode_text);
				}
				else
					PL_FWD(d)(s, Mode{});

				return s;
			}

		public:

			template <typename CharT_, typename Traits_>
			auto &operator() (std::basic_ostream<CharT_, Traits_> &os)
			{
				static_assert(CStreamProjectorOut<Derived_, CharT_>);
				return select_mode(derived(), os);
			}

			template <typename CharT_, typename Traits_>
			auto &operator() (std::basic_istream<CharT_, Traits_> &is)
			{
				static_assert(CStreamProjectorIn<Derived_, CharT_>);
				return select_mode(derived(), is);
			}
		};

	} // namespace impl

	PL_MAKE_TAG(empty_delim)

	template <bool SkipC_, typename Projector_ = yield_t, CMode Mode_ = mode_select_t>
	class value :
		impl::streamable_io<value<SkipC_, Projector_, Mode_>>,
		public impl::mode_selector<value<SkipC_, Projector_, Mode_>>
	{
	public:
		using Mode = Mode_;

		constexpr value(Mode_, bool_c<SkipC_>, Projector_ proj = {}) : mProj{std::move(proj)} {}
		constexpr value(bool_c<SkipC_>, Projector_ proj = {}) : mProj{std::move(proj)} {}

		using impl::mode_selector<value<SkipC_, Projector_, Mode_>>::operator();

	private:
		[[no_unique_address]] Projector_ mProj;

	public:
		template <typename CharT_, typename Traits_>
		void operator() (std::basic_ostream<CharT_, Traits_> &os, mode_text_t)
		{
			std::invoke(mProj, dir_out, impl::projector_fc(os, [&](auto&& v){ os << PL_FWD(v); }));
		}

		template <typename CharT_, typename Traits_>
		void operator() (std::basic_ostream<CharT_, Traits_> &os, mode_binary_t)
		{
			std::invoke(mProj, dir_out, impl::projector_fc(os, [&](auto&& v){
				using namespace rawio;
				os <= PL_FWD(v);
			}));
		}

		template <typename CharT_, typename Traits_>
		void operator() (std::basic_istream<CharT_, Traits_> &is, mode_text_t)
		{
			const auto rd = impl::projector_fc(is, [&](auto&& v){
					is >> read_value_wrapper{PL_FWD(v)};
					if constexpr(SkipC_)
						is >> skip_one;
				});

			if constexpr(std::is_void_v<decltype(std::invoke(mProj, dir_in, rd))>)
				std::invoke(mProj, dir_in, rd);
			else
				is >> std::invoke(mProj, dir_in, rd);
		}

		template <typename CharT_, typename Traits_>
		void operator() (std::basic_istream<CharT_, Traits_> &is, mode_binary_t)
		{
			const auto rd = impl::projector_fc(is, [&](auto&& v){
				using namespace rawio;
				is >= PL_FWD(v);
			});

			if constexpr(std::is_void_v<decltype(std::invoke(mProj, dir_in, rd))>)
				std::invoke(mProj, dir_in, rd);
			else
			{
				using namespace rawio;
				is >= std::invoke(mProj, dir_in, rd);
			}
		}
	};

	template <typename T_>
	concept CIORange = rg::range<T_> || CTuple<T_>;

	template <CIORange Range_, typename Delim_, typename Projector_ = yield_t, CMode Mode_ = mode_select_t>
	class range :
		impl::streamable_io<range<Range_, Delim_, Projector_, Mode_>>,
		public impl::mode_selector<range<Range_, Delim_, Projector_, Mode_>>
	{
	public:
		using Mode = Mode_;

		template <typename DT_, typename R_>
		constexpr range(Mode_, DT_ &&delim, R_ &&r, bool binarySafe = false, Projector_ p = {}) :
			mR{PL_FWD(r)}, mProj{std::move(p)}, mBinarySafe{binarySafe}, mDelim{PL_FWD(delim)} {}

		using impl::mode_selector<range<Range_, Delim_, Projector_, Mode_>>::operator();

	private:
		Range_ mR;
		bool mBinarySafe = false;
		[[no_unique_address]] Projector_ mProj;
		[[no_unique_address]] Delim_ mDelim;

	public:
		template <typename CharT_, typename Traits_>
		void operator() (std::basic_ostream<CharT_, Traits_> &os, mode_text_t)
		{
			if constexpr(CTuple<Range_>)
			{
				tuple_for_each_index(mR, [&]<szt I_>(size_c<I_>, auto&& v){
					std::invoke(mProj, dir_out, impl::projector_fc(os, [&](auto&& v){ os << PL_FWD(v); }), PL_FWD(v));

					if constexpr(!empty_delim_c<Delim_> && I_ + 1 < std::tuple_size_v<decay_t<Range_>>)
						os << mDelim;
				});
			}
			else
			{
				auto end = mR.end();
				auto it = mR.begin(), next = it;

				if(next != end)
				{
					++next;
					for(; it!=end; ++it)
					{
						std::invoke(mProj, dir_out, impl::projector_fc(os, [&](auto&& v){ os << PL_FWD(v); }), PL_FWD(*it));
						if(!os)
							break;

						if constexpr(!empty_delim_c<Delim_>)
							if(next != end)
							{
								os << mDelim;
								++next;
							}
					}
				}
			}
		}

		template <typename CharT_, typename Traits_>
		void operator() (std::basic_ostream<CharT_, Traits_> &os, mode_binary_t)
		{
			using namespace rawio;

			const auto normalWrite = [&]
				{
					const auto wfc = [&](auto&& v){ std::invoke(mProj, dir_out, impl::projector_fc(os, [&](auto&& v){ os <= PL_FWD(v); }), PL_FWD(v)); };

					if constexpr(CTuple<Range_>)
						mR | for_each(wfc);
					else
					{
						for(auto &&v : mR)
						{
							wfc(PL_FWD(v));
							if(!os)
								break;
						}
					}
				};

			if constexpr(CTuple<Range_>)
				normalWrite();
			else
			{
				if(mBinarySafe)
				{
					if constexpr(CTrivial<rg::range_value_t<Range_>> && rg::contiguous_range<Range_> && !CStreamProjector<rg::range_value_t<Range_>, CharT_>)
					{
						if(is_endian_native(os))
						{
							os.write(reinterpret_cast<const char *>(std::addressof(*mR.begin())), sizeof(rg::range_value_t<Range_>) * rg::size(mR));
							//std::cout << "Using os.write\n";
						}
						else
							normalWrite();
					}
					else
						normalWrite();
				}
				else
				{
					if constexpr(CYield<Projector_> && CTrivial<rg::range_value_t<Range_>> && rg::contiguous_range<Range_> && !CStreamProjector<rg::range_value_t<Range_>, CharT_>)
					{
						if(is_endian_native(os))
						{
							os.write(reinterpret_cast<const char *>(std::addressof(*mR.begin())), sizeof(rg::range_value_t<Range_>) * rg::size(mR));
							//std::cout << "Using os.write\n";
						}
						else
							normalWrite();
					}
					else
						normalWrite();
				}
			}
		}

		template <typename CharT_, typename Traits_>
		void operator() (std::basic_istream<CharT_, Traits_> &is, mode_text_t)
		{
			const auto rfc = [&](auto&& v)
				{
					std::invoke(mProj, dir_in, impl::projector_fc(is, [&](auto&& v){ is >> read_value_wrapper{PL_FWD(v)}; }), PL_FWD(v));
				};

			if constexpr(CTuple<Range_>)
				mR | for_each(rfc);
			else
			{
				for(auto &&v : mR)
				{
					rfc(PL_FWD(v));
					if(!is)
						break;
				}
			}
		}

		template <typename CharT_, typename Traits_>
		void operator() (std::basic_istream<CharT_, Traits_> &is, mode_binary_t)
		{
			using namespace rawio;

			const auto normalRead = [&]
				{
					const auto rfc = [&](auto&& v){ std::invoke(mProj, dir_in, impl::projector_fc(is, [&](auto&& v){ is >= PL_FWD(v); }), PL_FWD(v)); };

					if constexpr(CTuple<Range_>)
						mR | for_each(rfc);
					else
					{
						for(auto &&v : mR)
						{
							rfc(PL_FWD(v));
							if(!is)
								break;
						}
					}
				};

			if constexpr(CTuple<Range_>)
				normalRead();
			else
			{
				if(mBinarySafe)
				{
					if constexpr(CTrivial<rg::range_value_t<Range_>> && rg::contiguous_range<Range_> && !CStreamProjector<rg::range_value_t<Range_>, CharT_>)
					{
						if(is_endian_native(is))
						{
							is.read(reinterpret_cast<char *>(std::addressof(*mR.begin())), sizeof(rg::range_value_t<Range_>) * rg::size(mR));
							//std::cout << "Using is.read\n";
						}
						else
							normalRead();
					}
					else
						normalRead();
				}
				else
				{
					if constexpr(CYield<Projector_> && CTrivial<rg::range_value_t<Range_>> && rg::contiguous_range<Range_> && !CStreamProjector<rg::range_value_t<Range_>, CharT_>)
					{
						if(is_endian_native(is))
						{
							is.read(reinterpret_cast<char *>(std::addressof(*mR.begin())), sizeof(rg::range_value_t<Range_>) * rg::size(mR));
							//std::cout << "Using is.read\n";
						}
						else
							normalRead();
					}
					else
						normalRead();
				}
			}
		}
	};
	template <typename Mode_, CIORange Range_, typename Delim_>
	range(Mode_, const Delim_ *, Range_ &&) -> range<Range_, const Delim_ *, yield_t, Mode_>;
	template <typename Mode_, CIORange Range_, typename Delim_>
	range(Mode_, const Delim_ *, Range_ &&, bool) -> range<Range_, const Delim_ *, yield_t, Mode_>;
	template <typename Mode_, CIORange Range_, typename Projector_, typename Delim_>
	range(Mode_, const Delim_ *, Range_ &&, bool, Projector_) -> range<Range_, const Delim_ *, Projector_, Mode_>;
	template <typename Mode_, CIORange Range_, typename Delim_>
	range(Mode_, const Delim_ &, Range_ &&) -> range<Range_, Delim_, yield_t, Mode_>;
	template <typename Mode_, CIORange Range_, typename Delim_>
	range(Mode_, const Delim_ &, Range_ &&, bool) -> range<Range_, Delim_, yield_t, Mode_>;
	template <typename Mode_, CIORange Range_, typename Projector_, typename Delim_>
	range(Mode_, const Delim_ &, Range_ &&, bool, Projector_) -> range<Range_, Delim_, Projector_, Mode_>;


	template <typename T_, rg::output_iterator<T_> IT_, typename Projector_ = yield_t, CMode Mode_ = mode_select_t>
	class fillinto :
		impl::streamable_in<fillinto<T_, IT_, Projector_, Mode_>>,
		public impl::mode_selector<fillinto<T_, IT_, Projector_, Mode_>>
	{
	public:
		using Mode = Mode_;

		constexpr fillinto(Mode_, type_c<T_>, IT_ into, szt nb, bool binarySafe = false, Projector_ proj = {}) :
			mInto{std::move(into)}, mNb{nb}, mProj{std::move(proj)}, mBinarySafe{binarySafe} {}

		constexpr fillinto(type_c<T_>, IT_ into, szt nb, bool binarySafe = false, Projector_ proj = {}) :
			mInto{std::move(into)}, mNb{nb}, mProj{std::move(proj)}, mBinarySafe{binarySafe} {}

		using impl::mode_selector<fillinto<T_, IT_, Projector_, Mode_>>::operator();

	private:
		IT_ mInto;
		szt mNb;
		bool mBinarySafe = false;
		[[no_unique_address]] Projector_ mProj;

	public:
		template <typename CharT_, typename Traits_>
		void operator() (std::basic_istream<CharT_, Traits_> &is, mode_text_t)
		{
			const auto pfc = impl::projector_fc(is, [&](auto&&... v){
					impl::reader(type_v<T_>, mode_text, is, *mInto++, PL_FWD(v)...);
				});

			for(szt i=0; i<mNb && is; ++i)
				std::invoke(mProj, dir_in, pfc);
		}

		template <typename CharT_, typename Traits_>
		void operator() (std::basic_istream<CharT_, Traits_> &is, mode_binary_t)
		{
			using namespace rawio;

			const auto genNormal = [&]
				{
					const auto pfc = impl::projector_fc(is, [&](auto&&... v){
							impl::reader(type_v<T_>, mode_binary, is, *mInto++, PL_FWD(v)...);
						});

					for(szt i=0; i<mNb && is; ++i)
						std::invoke(mProj, dir_in, pfc);
				};

			if(mBinarySafe)
			{
				if constexpr(rg::contiguous_iterator<IT_>)
				{
					if constexpr(CTrivial<rg::iter_value_t<IT_>> && !CStreamProjectorIn<rg::iter_value_t<IT_>, CharT_>)
					{
						if(is_endian_native(is))
						{
							is.read(reinterpret_cast<char *>(std::addressof(*mInto)), sizeof(rg::iter_value_t<IT_>) * mNb);
							//std::cout << "Using is.read\n";
						}
						else
							genNormal();
					}
					else
						genNormal();
				}
				else
					genNormal();
			}
			else
			{
				if constexpr(CYield<Projector_> && rg::contiguous_iterator<IT_>)
				{
					if constexpr(CTrivial<rg::iter_value_t<IT_>> && !CStreamProjectorIn<rg::iter_value_t<IT_>, CharT_>)
					{
						if(is_endian_native(is))
						{
							is.read(reinterpret_cast<char *>(std::addressof(*mInto)), sizeof(rg::iter_value_t<IT_>) * mNb);
							//std::cout << "Using is.read\n";
						}
						else
							genNormal();
					}
					else
						genNormal();
				}
				else
					genNormal();
			}
		}
	};
	template <typename T_, typename IT_, typename Projector_, typename Mode_>
	fillinto(Mode_, type_c<T_>, IT_, szt, bool, Projector_) -> fillinto<T_, IT_, Projector_, Mode_>;
	template <typename T_, typename IT_, typename Projector_>
	fillinto(type_c<T_>, IT_, szt, bool, Projector_) -> fillinto<T_, IT_, Projector_>;


	[[nodiscard]] constexpr auto fill_container(CMode auto mode, auto&& c, szt nb, bool binarySafe, auto&& proj)
	{
		if constexpr((mode == mode_select || mode == mode_binary) && rg::contiguous_range<decay_t<decltype(c)>> && CTrivial<rg::range_value_t<decay_t<decltype(c)>>>)
		{
			PL_FWD(c).resize(nb);
			return fillinto{mode, type_v<rg::range_value_t<decay_t<decltype(c)>>>, PL_FWD(c).begin(), nb, binarySafe, PL_FWD(proj)};
		}
		else
		{
			try_reserve(PL_FWD(c), nb);
			return fillinto{mode, type_v<rg::range_value_t<decay_t<decltype(c)>>>, std::back_inserter(PL_FWD(c)), nb, binarySafe, PL_FWD(proj)};
		}
	}

	[[nodiscard]] constexpr auto fill_container(auto&& c, szt nb, bool binarySafe, auto&& proj)
	{
		return fill_container(mode_select, PL_FWD(c), nb, binarySafe, PL_FWD(proj));
	}

	template <typename T_, CMode Mode_ = mode_select_t>
	class read : impl::streamable_in<read<T_, Mode_>>, public impl::mode_selector<read<T_, Mode_>>
	{
	public:
		using Mode = Mode_;

		constexpr read(Mode_, T_ &v) noexcept : mV{&v} {}
		constexpr read(T_ &v) noexcept : mV{&v} {}

		using impl::mode_selector<read<T_, Mode_>>::operator();

	private:
		T_ *mV;

	public:
		template <typename CharT_, typename Traits_>
		void operator() (std::basic_istream<CharT_, Traits_> &is, mode_text_t)
		{
			is >> read_value_wrapper{*mV};
		}

		template <typename CharT_, typename Traits_>
		void operator() (std::basic_istream<CharT_, Traits_> &is, mode_binary_t)
		{
			using namespace rawio;
			is >= *mV;
		}
	};
	template <typename T_, typename Mode_>
	read(Mode_, T_ &) -> read<T_, Mode_>;
	template <typename T_>
	read(T_ &) -> read<T_, mode_select_t>;

	template <typename Tout_, bool Throw_ = true, CMode Mode_ = mode_select_t>
	[[nodiscard]] constexpr auto translated_value(auto&& val, type_c<Tout_>, bool_c<Throw_> = {}, Mode_ = {})
	{
		return value{Mode_{}, bool_false,
				overload_set{
					[&]<typename Tin = decay_t<decltype(val)>>(dir_in_t, auto&& fc)
					{
						Tout_ v;

						if(PL_FWD(fc)(v) != failure)
						{
							if constexpr(Throw_ && CNumberPack<Tin> && CNumberPack<Tout_>)
								if(!fits_in(v, type_v<Tin>))
									throw std::range_error{"Value would be out of range if converted."};

							PL_FWD(val) = static_cast<Tin>(v);
						}
					},
					[&]<typename Tin = decay_t<decltype(val)>>(dir_out_t, auto&& fc)
					{
						if constexpr(Throw_ && CNumberPack<Tin> && CNumberPack<Tout_>)
							if(!fits_in(val, type_v<Tout_>))
								throw std::range_error{"Value would be out of range if converted."};

						PL_FWD(fc)(static_cast<Tout_>(val));
					}
				}
			};
	}

	template <typename Tin_, typename Tout_, bool Throw_ = true, CMode Mode_ = mode_select_t>
	[[nodiscard]] constexpr auto translated_value_proj(type_c<Tin_>, type_c<Tout_>, bool_c<Throw_> = {}, Mode_ = {})
	{
		if constexpr(std::same_as<Tout_, Tin_>)
			return yield;
		else
			return [](CDirection auto, auto&& fc, auto&& v) { PL_FWD(fc)(translated_value(PL_FWD(v), type_v<Tout_>, bool_v<Throw_>, Mode_{})); };
	}

	template <typename Tout_, bool Throw_ = true, CMode Mode_ = mode_select_t>
	[[nodiscard]] constexpr auto translated_value_range(auto &&r, bool binarySafe, type_c<Tout_>, bool_c<Throw_> = {}, Mode_ = {})
	{
		return range{
				Mode_{}, '\n', rgv::all(PL_FWD(r)), binarySafe,
				translated_value_proj(type_v<rg::range_value_t<decltype(r)>>, type_v<Tout_>, bool_v<Throw_>, Mode_{})
			};
	}

	template <CMode Mode_ = mode_select_t>
	[[nodiscard]] constexpr auto atomic_value(auto &&atomicv, Mode_ = {})
	{
		using T = typename decay_t<decltype(atomicv)>::value_type;

		return value{Mode_{}, bool_false,
				[&](CDirection auto dir, auto&& fc)
				{
					if constexpr(dir == dir_in)
					{
						T v;
						if(PL_FWD(fc)(v) != failure)
							PL_FWD(atomicv).store(v, std::memory_order::relaxed);
					}
					else
					{
						PL_FWD(fc)(PL_FWD(atomicv).load(std::memory_order::relaxed));
					}
				}
			};
	}

	template <CMode Mode_ = mode_select_t>
	[[nodiscard]] constexpr auto atomic_range(auto &&r, Mode_ = {})
	{
		return range{
			Mode_{}, '\n', rgv::all(PL_FWD(r)), false,
			[](CDirection auto, auto&& fc, auto&& v) { PL_FWD(fc)(atomic_value(PL_FWD(v), Mode_{})); }
		};
	}

	template <typename T_>
	[[nodiscard]] constexpr auto zero_initialize_proj(type_c<T_>, auto&& proj)
	{
		return [proj = PL_FWD(proj)](CDirection auto dir, auto&& fc, auto&&... v){
			if constexpr(dir == dir_in)
			{
				T_ iv{};
				std::invoke(proj, dir, [&](auto&&... vs){ return PL_FWD(fc)(iv, PL_FWD(vs)...); }, iv);
			}
			else
			{
				std::invoke(proj, dir, PL_FWD(fc), PL_FWD(v)...);
			}
		};
	}

	template <typename T_>
	[[nodiscard]] constexpr auto default_initialize_proj(type_c<T_>, auto&& proj)
	{
		return [proj = PL_FWD(proj)](CDirection auto dir, auto&& fc, auto&&... v){
			if constexpr(dir == dir_in)
			{
				T_ iv;
				std::invoke(proj, dir, [&](auto&&... vs){ return PL_FWD(fc)(iv, PL_FWD(vs)...); }, iv);
			}
			else
			{
				std::invoke(proj, dir, PL_FWD(fc), PL_FWD(v)...);
			}
		};
	}

	template <typename T_, CMode Mode_ = mode_select_t>
	class write : impl::streamable_out<write<T_, Mode_>>, public impl::mode_selector<write<T_, Mode_>>
	{
	public:
		using Mode = Mode_;

		template <typename OT_>
		constexpr write(Mode_, OT_ &&v) noexcept : mV{PL_FWD(v)} {}

		using impl::mode_selector<write<T_, Mode_>>::operator();

	private:
		T_ mV;

	public:
		template <typename CharT_, typename Traits_>
		void operator() (std::basic_ostream<CharT_, Traits_> &os, mode_text_t)
		{
			os << mV;
		}

		template <typename CharT_, typename Traits_>
		void operator() (std::basic_ostream<CharT_, Traits_> &os, mode_binary_t)
		{
			using namespace rawio;
			os <= mV;
		}
	};
	template <typename T_, typename Mode_>
	write(Mode_, T_&&) -> write<T_, Mode_>;


	template <rg::input_range CT_, CMode Mode_ = mode_select_t, typename Projector_ = yield_t, std::invocable<szt> OnReadNb_ = decltype(do_nothing)>
	[[nodiscard]] constexpr auto container(CT_ &c, Mode_ = {}, bool binarySafe = false, Projector_ proj = {}, [[maybe_unused]] OnReadNb_ onReadNb = do_nothing)
	{
		// Read/write size of container
		return value{mode_text, bool_v<true>, [&c, binarySafe, proj = std::move(proj), onReadNb = std::move(onReadNb)](CDirection auto dir, auto&& fc){
			if constexpr(dir == dir_in)
			{
				szt nb;
				if(PL_FWD(fc)(nb) != failure)
					onReadNb(nb);
				return fill_container(Mode_{}, c, nb, binarySafe, proj);
			}
			else
			{
				PL_FWD(fc)(range{mode_text, empty_delim, std::make_tuple(
					c.size(), '\n',
					range{Mode_{}, '\n', rgv::all(c), binarySafe, proj}
				)});
			}
		}};
	}


	template <typename T_, typename CharT_ = std::string::value_type>
	[[nodiscard]] constexpr auto to_string_loc(T_&& value, const std::locale &loc)
	{
		return stream_into(std::basic_string<CharT_>{}, throw_on_failure, [&](auto&& os, auto&& str){
			os.imbue(loc);
			if constexpr(std::is_floating_point_v<remove_cvref_t<T_>>)
				os << std::setprecision(std::numeric_limits<remove_cvref_t<T_>>::digits10 + 1) << std::defaultfloat;
			os << std::dec << PL_FWD(value);
			return str;
		});
	}

	template <typename T_>
	[[nodiscard]] constexpr auto to_string_noloc(T_ &&value)
	{
		if constexpr(std::is_arithmetic_v<remove_cvref_t<T_>> && !std::is_same_v<remove_cvref_t<T_>, bool>)
		{
			std::array<char, 1024> chs;

			const auto res = [&]{
					if constexpr(std::is_floating_point_v<T_>)
					{
						auto *cur = chs.begin();
						if(value < 0)
						{
							*cur++ = '-';
							*cur++ = '0';
						}
						else
						{
							*cur++ = '0';
							*cur++ = 'x';
						}

						auto res = std::to_chars(cur, chs.end(), value, std::chars_format::hex);

						if(value < 0)
							chs[2] = 'x';

						return res;
					}
					else
						return std::to_chars(chs.begin(), chs.end(), value);
				}();

			if(res.ec != std::errc{})
				throw Exception{std::string{"Failed to serialize value: "} + std::make_error_condition(res.ec).message()};

			return std::string{chs.data(), res.ptr};
			//return std::string{std::string_view{chs.data(), std::string_view::size_type(res.ptr - chs.data())}};
			//return boost::lexical_cast<std::basic_string<CharT_>>(PL_FWD(value));
		}
		else
		{
			return stream_into(std::string{}, throw_on_failure, [&](auto&& os, auto&& str){
				os << PL_FWD(value); return str;
			});
		}
	}

	/*template <typename T_>
	std::string toStrNoLocValue(T_ value)
	{
		std::array<char, 256> chs;

		std::to_chars(chs.data(), chs.data() + chs.size(), value);

		return {std::string_view{chs.data(), chs.size()}};
		//return boost::lexical_cast<std::basic_string<CharT_>>(value);
	}*/

	template <typename T_, typename CharT_>
	[[nodiscard]] constexpr auto from_string_loc(std::basic_string_view<CharT_> value, const std::locale &loc)
	{
		T_ res{};

		stream_from(value, [&](auto&& is, auto&&){
			is.imbue(loc);
			is >> res;
		});

		return res;
	}

	template <typename T_, typename CharT_>
	[[nodiscard]] constexpr auto from_string_loc(const std::basic_string<CharT_> &value, const std::locale &loc)
		{ return from_string_loc<T_>(std::basic_string_view<CharT_>{value}, loc); }

	template <std::floating_point T_>
	[[nodiscard]] constexpr auto read_float(const char *str, char **str_end)
	{
		T_ parsed;

		if constexpr(std::is_same_v<T_, float>) // Why u no read hexfloat libstdc++?
		{
			parsed = std::strtof(str, str_end);
		}
		else if constexpr(std::is_same_v<T_, double>)
		{
			parsed = std::strtod(str, str_end);
		}
		else if constexpr(std::is_same_v<T_, long double>)
		{
			parsed = std::strtold(str, str_end);
		}
		else
			static_assert(dependent_false<T_>, "Unsupported or invalid floating point type.");

		if(errno == ERANGE)
			throw Exception("Value out of range.");
		/*else if(errno)
			throw Exception{std::string{"Failed to parse value: "} + std::strerror(errno)};*/
		return parsed;
	}

	template <typename T_>
	[[nodiscard]] constexpr auto from_string_noloc(std::string_view value, type_c<T_> = {})
	{
		if constexpr(std::is_floating_point_v<T_>) // Why u no read hexfloat libstdc++?
		{
			return read_float<T_>(value.data(), nullptr);
		}
		else if constexpr(std::is_arithmetic_v<T_> && !std::is_same_v<T_, bool>)
		{
			T_ parsed{};

			const auto res = std::from_chars(value.data(), value.data() + value.size(), parsed);

			if(res.ec != std::errc{})
				throw Exception{std::string{"Failed parsing value: "} + std::make_error_condition(res.ec).message()};

			return parsed;
			//return boost::lexical_cast<T_>(value.data(), value.size());
		}
		else
		{
			return stream_from(value, [&](auto&& is, auto&&){
					T_ parsed{};
					is >> parsed;
					return parsed;
				});
		}
	}

	template <typename CharT_ = char>
	[[nodiscard]] constexpr auto to_hex_string(std::basic_string_view<CharT_> str)
	{
		std::basic_string<CharT_> res;

		return stream_into(res, throw_on_failure, [&](auto&& os, auto&& s){
			os << std::setw(2) << std::setfill(CharT_{'0'}) << std::hex << std::uppercase;
			str | copy(std::ostream_iterator<u32>(os, " "));
			return s;
		});
	}

	template <typename CharT_ = char>
	[[nodiscard]] constexpr auto format_size(szt size, const std::locale &loc = {})
	{
		constexpr std::array sizes{"B", "KB", "MB", "GB", "TB", "PB"};
		std::basic_string<CharT_> res;

		return stream_into(res, throw_on_failure, [&](auto&& os, auto&& s){
			os.imbue(loc);

			auto dsize = static_cast<double>(size);
			u32 index = 0;
			while(++index < sizes.size() && dsize >= 1024)
				dsize /= 1024;

			os << std::setprecision(5) << dsize << ' ' << sizes[index - 1];
			return s;
		});
	}

	// Serializable objects
	template <typename CharT_ = char>
	class ISerializable
	{
	public:
		virtual ~ISerializable() = default;

		using char_type = CharT_;
		using ostream_type = std::basic_ostream<CharT_>;
		using istream_type = std::basic_istream<CharT_>;

		virtual void serialize(ostream_type &outputStream) const = 0;
		virtual void unserialize(istream_type &inputStream) = 0;

		friend auto &operator<<(ostream_type &os, const ISerializable &s)
			{ s.serialize(os); return os; }

		friend auto &operator>>(istream_type &is, ISerializable &s)
			{ s.unserialize(is); return is; }

		friend auto &operator<<(ISerializable &s, std::basic_string_view<CharT_> data)
		{
			stream_from(data, [&](auto&& is, auto&&){ s.unserialize(is); });
			return s;
		}

		friend auto &operator<<(std::basic_string<CharT_> &data, const ISerializable &s)
		{
			stream_into(data, [&](auto&& os, auto&&){ s.serialize(os); });
			return s;
		}
	};


	struct data_header
	{
		bool mIsBinary = false;
		std::string mCompressor;
		std::endian mEndian = native_endian;

		data_header() = default;
		data_header(std::ios_base &io)
		{
			read_from(io);
		}

		void read_from(std::ios_base &io)
		{
			mIsBinary = is_binary(io);
			mEndian = get_endian(io);
		}

		void apply_to(std::ios_base &io) const
		{
			if(mIsBinary)
				binary(io);
			else
				text(io);

			set_endian(io, mEndian);
		}

		friend std::ostream &operator<< (std::ostream &os, const data_header &h)
		{
			os << "format: " << (h.mIsBinary ? "binary" : "text") << '\n';

			if(h.mIsBinary)
				os << "endian: " << stream_endian_string{h.mEndian} << '\n';

			if(!h.mCompressor.empty())
				os << "compressor: " << h.mCompressor << '\n';
			else
				os << "compressor: uncompressed\n";

			return os << '\n';
		}

		friend std::istream &operator>> (std::istream &is, data_header &h)
		{
			for(const auto &line : rg::getlines(is))
			{
				if(line.empty())
					break;

				if(line.starts_with("format:"))
					h.mIsBinary = trim(remove_prefix(std::string_view{line}, 7)) == "binary";
				else if(line.starts_with("endian:"))
					h.mEndian = from_string(type_v<std::endian>, trim(remove_prefix(std::string_view{line}, 7)));
				else if(line.starts_with("compressor:"))
				{
					h.mCompressor = trim(remove_prefix(std::string_view{line}, 11));
					if(h.mCompressor == "uncompressed")
						h.mCompressor.clear();
				}
			}

			return is;
		}
	};

	#ifdef PL_FMT

		#define PL_LIB_IO_FMT

		// Can be used to disambiguate specializations for fmt::formatter if for example the type is also a valid range.
		template <typename T_>
		struct value_formatter
		{
			const T_ &value;
		};
		template <typename T_>
		value_formatter(const T_ &) -> value_formatter<T_>;

		template <typename T_>
		struct quoted
		{
			const T_ &value;
		};
		template <typename T_>
		quoted(const T_ &) -> quoted<T_>;

		template <typename T_>
		struct memory_size
		{
			const T_ &value;
		};
		template <typename T_>
		memory_size(const T_ &) -> memory_size<T_>;

	#endif
}

#ifdef PL_FMT
namespace fmt
{
	template <typename T_, typename Char_>
	struct formatter<::patlib::io::value_formatter<T_>, Char_> : private formatter<T_, Char_>
	{
		using base_formatter_type = formatter<T_, Char_>;

		using base_formatter_type::parse;

		auto format(const ::patlib::io::value_formatter<T_> &v, auto &ctx) const
			{ return base_formatter_type::format(v.value, ctx); }
	};

	template <typename T_, typename Char_>
	struct formatter<::patlib::io::quoted<T_>, Char_> : private formatter<T_, Char_>
	{
		using base_formatter_type = formatter<T_, Char_>;

		using base_formatter_type::parse;

		auto format(const ::patlib::io::quoted<T_> &v, auto &ctx) const
		{
			auto out = ctx.out();
			*out++ = '"';
			ctx.advance_to(out);
			base_formatter_type::format(v.value, ctx);
			*out++ = '"';

			return out;
		}
	};

	template <typename T_, typename Char_>
	struct formatter<::patlib::io::memory_size<T_>, Char_> : private formatter<T_, Char_>
	{
		using base_formatter_type = formatter<T_, Char_>;

		using base_formatter_type::parse;

		auto format(const ::patlib::io::memory_size<T_> &v, auto &ctx) const
		{
			constexpr std::array sizes{"B", "KB", "MB", "GB", "TB", "PB"};

			auto size = v.value;
			std::size_t index = 0;
			while(++index < sizes.size() && size >= 1024)
				size /= 1024;

			base_formatter_type::format(size, ctx);

			auto out = ctx.out();
			*out++ = ' ';
			std::string_view sizeStr{sizes[index - 1]};

			return std::copy(sizeStr.begin(), sizeStr.end(), out);
		}
	};
}
#endif
