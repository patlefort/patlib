/*
	PatLib

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "base.hpp"
#include "concepts.hpp"
#include "utility.hpp"

#include <type_traits>
#include <utility>

namespace patlib
{
	// Create 2 of an object, a front and a back object that can be flipped.
	template <typename T_>
	class back_buffered
	{
	private:

		T_ mO1{}, mO2{}, *mFront{&mO1}, *mBack{&mO2};

	public:

		using value_type = T_;

		constexpr back_buffered() = default;
		constexpr back_buffered(const back_buffered &o) noexcept(CNoThrowCopyContructible<T_>) :
			mO1{o.mO1}, mO2{o.mO2} {}
		constexpr back_buffered(back_buffered &&o) noexcept(CNoThrowMoveContructible<T_>) :
			mO1{std::move(o.mO1)}, mO2{std::move(o.mO2)} {}
		template <typename... Args_>
		constexpr explicit back_buffered(std::in_place_t, Args_&&... args)
			noexcept(CNoThrowContructible<T_, Args_...>) :
			mO1{PL_FWD(args)...}, mO2{PL_FWD(args)...} {}

		constexpr auto &operator=(const back_buffered &o) noexcept(CNoThrowCopyAssignable<T_>)
			{ mO1 = o.mO1; mO2 = o.mO2; return *this; }
		constexpr auto &operator=(back_buffered &&o) noexcept(CNoThrowMoveAssignable<T_>)
			{ mO1 = std::move(o.mO1); mO2 = std::move(o.mO2); return *this; }

		constexpr void flip() noexcept
			{ std::swap(mFront, mBack); }

		[[nodiscard]] constexpr T_ &front() noexcept { return *mFront; }
		[[nodiscard]] constexpr const T_ &front() const noexcept { return *mFront; }
		[[nodiscard]] constexpr T_ &back() noexcept { return *mBack; }
		[[nodiscard]] constexpr const T_ &back() const noexcept { return *mBack; }

		template <typename FC_>
		constexpr void call(FC_ &&fc) noexcept(noexcept(PL_FWD(fc)(*mFront)))
			{ call_on(PL_FWD(fc), *mFront, *mBack); }

		template <typename FC_>
		constexpr void call(FC_ &&fc) const noexcept(noexcept(PL_FWD(fc)(*mFront)))
			{ call_on(PL_FWD(fc), std::as_const(*mFront), std::as_const(*mBack)); }
	};

	// Create 2 of an object, a front and a back object that can be flipped. The object must be swappable.
	template <std::swappable T_>
	class back_buffered_swap
	{
	private:

		T_ mFront{}, mBack{};

	public:

		using value_type = T_;

		constexpr back_buffered_swap() = default;
		template <typename... Args_>
		constexpr back_buffered_swap(std::in_place_t, Args_&&... args)
			noexcept(CNoThrowContructible<T_, Args_...>) :
			mFront{PL_FWD(args)...}, mBack{PL_FWD(args)...} {}

		constexpr void flip() noexcept(std::is_nothrow_swappable_v<T_>)
			{ adl_swap(mFront, mBack); }

		[[nodiscard]] constexpr T_ &front() noexcept { return mFront; }
		[[nodiscard]] constexpr const T_ &front() const noexcept { return mFront; }
		[[nodiscard]] constexpr T_ &back() noexcept { return mBack; }
		[[nodiscard]] constexpr const T_ &back() const noexcept { return mBack; }

		template <typename FC_>
		constexpr void call(FC_ &&fc) noexcept(noexcept(fc(mFront)))
			{ call_on(PL_FWD(fc), mFront, mBack); }

		template <typename FC_>
		constexpr void call(FC_ &&fc) const noexcept(noexcept(fc(mFront)))
			{ call_on(PL_FWD(fc), mFront, mBack); }
	};
}
