/*
	PatLib

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "base.hpp"

#include <type_traits>
#include <functional>
#include <utility>
#include <concepts>
#include <limits>

namespace patlib
{
	template<typename T1_, typename T2_>
	concept CSameSize = (sizeof(T1_) == sizeof(T2_));

	template<typename T_>
	concept CTrivial = std::is_trivial_v<T_>;

	template<typename T_>
	concept CTriviallyCopyable = std::is_trivially_copyable_v<T_>;

	template<typename T_>
	concept CEnum = std::is_enum_v<T_>;

	template<typename T_>
	concept CUnion = std::is_union_v<T_>;

	template<typename T_>
	concept CFunction = std::is_function_v<T_>;

	template<typename T_>
	concept CObject = std::is_object_v<T_>;

	template<typename T_>
	concept CClass = std::is_class_v<T_>;

	template<typename T_>
	concept CNullPointer = std::is_null_pointer_v<T_>;

	template<typename T_>
	concept CReference = std::is_reference_v<T_>;

	template<typename T_>
	concept CPointer = std::is_pointer_v<T_>;

	template<typename T_>
	concept CVoid = std::is_void_v<T_>;

	template<typename T_>
	concept CNotVoid = !std::is_void_v<T_>;

	template<typename T_>
	concept CBool = std::same_as<T_, bool>;

	template<typename T_>
	concept CBoolLike = std::convertible_to<T_, bool>;

	template <typename T_>
	concept CPointerLike = CPointer<T_> ||
		(
			requires(T_ p)
			{
				{ p == nullptr } -> CBoolLike;
				{ p != nullptr } -> CBoolLike;
				{ *p } -> CReference;
				{ p.operator->() } -> CPointer;
			}
		);

	template<typename T_>
	concept CArithmetic = std::integral<T_> || std::floating_point<T_>;

	template <typename From_, typename Into_>
	concept CExplicitConvertibleTo = requires(const From_ from) { { static_cast<Into_>(from) } -> std::same_as<Into_>; };

	template <typename F_, typename R_, typename... Args_>
	concept CInvocableR = std::invocable<F_, Args_...> && std::same_as<std::invoke_result_t<F_, Args_...>, R_>;

	template <typename F_, typename R_, typename... Args_>
	concept CInvocableConvertibleR = std::invocable<F_, Args_...> && std::convertible_to<std::invoke_result_t<F_, Args_...>, R_>;

	template <typename T_, typename... Ts_>
	concept CAnyOf = (... || std::same_as<T_, Ts_>);

	template <typename T_, typename... Ts_>
	concept CNoneOf = (... && !std::same_as<T_, Ts_>);

	template<typename T_>
	concept CDefaultConstructible = std::is_default_constructible_v<T_>;
	template<typename T_>
	concept CTriviallyDefaultContructible = std::is_trivially_default_constructible_v<T_>;
	template<typename T_>
	concept CNoThrowDefaultContructible = std::is_nothrow_default_constructible_v<T_>;

	template<typename T_>
	concept CDestructible = std::is_destructible_v<T_>;
	template<typename T_>
	concept CTriviallyDestructible = std::is_trivially_destructible_v<T_>;
	template<typename T_>
	concept CNoThrowDestructible = std::is_nothrow_destructible_v<T_>;

	template<typename T_, typename... Args_>
	concept CContructible = std::is_constructible_v<T_, Args_...>;
	template<typename T_, typename... Args_>
	concept CTriviallyContructible = std::is_trivially_constructible_v<T_, Args_...>;
	template<typename T_, typename... Args_>
	concept CNoThrowContructible = std::is_nothrow_constructible_v<T_, Args_...>;

	template<typename T_>
	concept CCopyContructible = std::is_copy_constructible_v<T_>;
	template<typename T_>
	concept CTriviallyCopyContructible = std::is_trivially_copy_constructible_v<T_>;
	template<typename T_>
	concept CNoThrowCopyContructible = std::is_nothrow_copy_constructible_v<T_>;

	template<typename T_>
	concept CMoveContructible = std::is_move_constructible_v<T_>;
	template<typename T_>
	concept CTriviallyMoveContructible = std::is_trivially_move_constructible_v<T_>;
	template<typename T_>
	concept CNoThrowMoveContructible = std::is_nothrow_move_constructible_v<T_>;

	template<typename T_>
	concept CCopyAssignable = std::is_copy_assignable_v<T_>;
	template<typename T_>
	concept CTriviallyCopyAssignable = std::is_trivially_copy_assignable_v<T_>;
	template<typename T_>
	concept CNoThrowCopyAssignable = std::is_nothrow_copy_assignable_v<T_>;

	template<typename T_>
	concept CMoveAssignable = std::is_move_assignable_v<T_>;
	template<typename T_>
	concept CTriviallyMoveAssignable = std::is_trivially_move_assignable_v<T_>;
	template<typename T_>
	concept CNoThrowMoveAssignable = std::is_nothrow_move_assignable_v<T_>;

	template<typename T_>
	concept CSwappable = std::is_swappable_v<T_>;
	template<typename T_, typename U_>
	concept CSwappableWith = std::is_swappable_with_v<T_, U_>;
	template<typename T_, typename U_>
	concept CNoThrowSwappableWith = std::is_nothrow_swappable_with_v<T_, U_>;
	template<typename T_>
	concept CNoThrowSwappable = std::is_nothrow_swappable_v<T_>;

	template <typename T_>
	concept CNumberBase = std::semiregular<T_> &&
		std::numeric_limits<T_>::is_specialized &&
		requires(T_ n, const T_ cn)
		{
			{ cn + cn } -> std::convertible_to<T_>;
			{ cn - cn } -> std::convertible_to<T_>;
			{ cn * cn } -> std::convertible_to<T_>;
			{ cn / cn } -> std::convertible_to<T_>;
			{ n += cn } -> std::same_as<T_&>;
			{ n -= cn } -> std::same_as<T_&>;
			{ n *= cn } -> std::same_as<T_&>;
			{ n /= cn } -> std::same_as<T_&>;
			{ ++n } -> std::same_as<T_ &>;
			{ n++ } -> std::convertible_to<T_>;
			{ --n } -> std::same_as<T_ &>;
			{ n-- } -> std::convertible_to<T_>;
			{ +cn } -> std::convertible_to<T_>;
			{ -cn } -> std::convertible_to<T_>;
		};

	template <typename T_>
	concept CNumber = CNumberBase<T_> &&
		requires(T_ n, const T_ cn)
		{
			{ cn > cn } -> CBoolLike;
			{ cn >= cn } -> CBoolLike;
			{ cn <= cn } -> CBoolLike;
			{ cn < cn } -> CBoolLike;
			{ cn || cn } -> CBoolLike;
			{ cn && cn } -> CBoolLike;
			{ !cn } -> CBoolLike;
			{ cn == cn } -> CBoolLike;
			{ cn != cn } -> CBoolLike;
		};

	template <typename T_>
	concept CShiftable =
		requires(T_ n, const T_ cn)
		{
			{ n >>= 1 } -> std::same_as<T_&>;
			{ n <<= 1 } -> std::same_as<T_&>;
			{ cn >> 1 } -> std::convertible_to<T_>;
			{ cn << 1 } -> std::convertible_to<T_>;
		};

	template <typename T_>
	concept CBitFieldBase = std::semiregular<T_> &&
		requires(T_ n, const T_ cn)
		{
			{ cn | cn } -> std::convertible_to<T_>;
			{ cn & cn } -> std::convertible_to<T_>;
			{ cn ^ cn } -> std::convertible_to<T_>;
			{ n |= cn } -> std::same_as<T_&>;
			{ n &= cn } -> std::same_as<T_&>;
			{ n ^= cn } -> std::same_as<T_&>;
			{ ~cn } -> std::convertible_to<T_>;
		};

	template <typename T_>
	concept CBitField = CBitFieldBase<T_> &&
		requires(T_ n, const T_ cn)
		{
			{ cn || cn } -> CBoolLike;
			{ cn && cn } -> CBoolLike;
			{ !cn } -> CBoolLike;
			{ cn == cn } -> CBoolLike;
			{ cn != cn } -> CBoolLike;
		};

	template <typename T_>
	concept CShiftableBitField = CBitField<T_> && CShiftable<T_>;

	template <typename T_>
	concept CNumberFloat = CNumber<T_> && !std::numeric_limits<T_>::is_integer;

	template <typename T_>
	concept CNumberInteger = CNumber<T_> && std::numeric_limits<T_>::is_integer &&
		requires(T_ n, const T_ cn)
		{
			{ n %= cn } -> std::same_as<T_&>;
			{ cn % cn } -> std::convertible_to<T_>;
		};

	template <typename T_>
	concept CBitFieldNumber = CBitField<T_> && CNumber<T_>;

	template <typename T_>
	concept CJoinable = requires(const T_ o) { { o.join(o) } -> std::same_as<T_>; };
}
