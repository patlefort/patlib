/*
	PatLib

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "base.hpp"
#include "math.hpp"
#include "simd/basic.hpp"
#include "type.hpp"
#include "io.hpp"
#include "utility.hpp"

#include <type_traits>
#include <utility>
#include <initializer_list>
#include <iterator>
#include <istream>
#include <ostream>

#include <range/v3/range/concepts.hpp>
#include <range/v3/range/traits.hpp>
#include <range/v3/numeric/accumulate.hpp>

#ifdef PL_FMT
	#include <fmt/format.h>
	#include <fmt/ranges.h>
#endif

namespace patlib
{
	namespace impl_rect
	{
		using namespace simd::vector_op;

		template <std::integral Type_>
		class rectangle_base
		{
		public:

			using value_type = Type_;
			using point_type = point2d<Type_>;
			using dir_type = dir2d<Type_>;

			point_type p1{}, p2{};

			constexpr rectangle_base() = default;
			constexpr rectangle_base(std::initializer_list<zero_init_t>) noexcept : p1{}, p2{} {}
			constexpr rectangle_base(const tag::zero_t) noexcept : p1{tag::zero}, p2{tag::zero} {}
			constexpr rectangle_base(const tag::infhighest_t) noexcept { set_infmax(); }
			constexpr rectangle_base(const tag::inflowest_t) noexcept { set_inflowest(); }
			constexpr rectangle_base(const Type_ ax1, const Type_ ay1, const Type_ ax2, const Type_ ay2) noexcept
				: p1{ax1, ay1}, p2{ax2, ay2} {}

			constexpr rectangle_base(const size_p<dir_type> p2_) noexcept
				: p1{}, p2{*p2_} {}
			constexpr rectangle_base(const size_p<value_type> p2_) noexcept
				: p1{}, p2{*p2_} {}

			constexpr rectangle_base(const point_type p1_, const point_type p2_) noexcept
				: p1{p1_}, p2{p2_} {}

			constexpr rectangle_base(const point_type p1_, const size_p<dir_type> p2_) noexcept
				: p1{p1_}, p2{p1_ + *p2_} {}
			constexpr rectangle_base(const point_type p1_, const size_p<value_type> p2_) noexcept
				: p1{p1_}, p2{p1_ + *p2_} {}

			template <typename T_>
			constexpr explicit(!std::is_convertible_v<T_, Type_>) rectangle_base(const rectangle_base<T_> &other) noexcept :
				p1{static_cast<Type_>(other.p1)}, p2{static_cast<Type_>(other.p2)} {}

			constexpr void set_zero() noexcept
			{
				p1.set_zero();
				p2.set_zero();
			}

			constexpr void set_infmax() noexcept
			{
				p1 = constants::inflowest<value_type>;
				p2 = constants::infhighest<value_type>;
			}

			constexpr void set_inflowest() noexcept
			{
				p1 = constants::infhighest<value_type>;
				p2 = constants::inflowest<value_type>;
			}

			[[nodiscard]] constexpr auto area() const noexcept
				{ return (p2[1] - p1[1]) * (p2[0] - p1[0]); }

			[[nodiscard]] constexpr auto dims() const noexcept
				{ return static_cast<simd::rebind_to_dir<point_type>>(p2 - p1); }

			[[nodiscard]] constexpr auto middle() const noexcept { return plmidpoint(p1, p2); }

			[[nodiscard]] constexpr bool overlap(const rectangle_base &rect) const noexcept
				{ return mask_all((p1 < rect.p2) & (p2 >= rect.p1)); }

			[[nodiscard]] constexpr bool overlap(point_type p) const noexcept
				{ return mask_none((p < p1) | (p >= p2)); }

			[[nodiscard]] constexpr bool fully_inside(const rectangle_base &rect) const noexcept
				{ return mask_none((rect.p2 <= p1) | (rect.p1 >= p2)); }

			[[nodiscard]] constexpr bool is_same_size(const rectangle_base &rect) const noexcept
				{ return mask_all(dims() == rect.dims()); }

			[[nodiscard]] constexpr bool is_zero_area() const noexcept
				{ return mask_any(p2 == p1); }

			constexpr void loop(std::invocable<point_type> auto&& fc) const
			{
				for(Type_ y=p1[1]; y<p2[1]; ++y)
					for(Type_ x=p1[0]; x<p2[0]; ++x)
						PL_FWD(fc)(point_type{x, y});
			}

			constexpr void loop_local_offset(std::invocable<point_type, Type_> auto&& fc) const
			{
				Type_ offset{};

				for(Type_ y=p1[1]; y<p2[1]; ++y)
					for(Type_ x=p1[0]; x<p2[0]; ++x, ++offset)
						PL_FWD(fc)(point_type{x, y}, offset);
			}

			[[nodiscard]] constexpr auto operator<=>(const rectangle_base &rv) const noexcept
			{
				if(const auto cmp = p1 <=> rv.p1; cmp != 0)
					return cmp;

				return p2 <=> rv.p2;
			}

			[[nodiscard]] friend constexpr bool operator==(const rectangle_base &lv, const rectangle_base &rv) noexcept
				{ return mask_all((lv.p1 == rv.p1) & (lv.p2 == rv.p2)); }
		};

		template <typename T_>
		class rect_iterator
		{
		public:
			using value_type = point2d<T_>;
			using pointer = value_type *;
			using reference = value_type;
			using iterator_category = std::random_access_iterator_tag;
			using difference_type = typename std::incrementable_traits<T_>::difference_type;

		private:
			const rectangle_base<T_> *mR{};
			point2d<T_> mP{};
			T_ mOffset{};

		public:

			constexpr rect_iterator() = default;
			constexpr rect_iterator(const rect_iterator &other) = default;
			constexpr rect_iterator(const rectangle_base<T_> &r) noexcept : mR{&r}, mP{r.p1} {}

			// Offset for end of range will be the offset r.p1[0], r.p2[1]
			constexpr rect_iterator(const rectangle_base<T_> &r, bool /*startsAtEnd*/) noexcept : mR{&r}, mP{r.p1[0], r.p2[1]} {}

			[[nodiscard]] constexpr reference operator *() const noexcept { return mP; }

			[[nodiscard]] constexpr auto position() const noexcept { return mP; }
			[[nodiscard]] constexpr auto offset() const noexcept { return mP[1] * (mR->p2[0] - mR->p1[0]) + mP[0]; }
			[[nodiscard]] constexpr auto local_offset() const noexcept { return mOffset; }
			[[nodiscard]] constexpr const auto &rect() const noexcept { return *mR; }

			constexpr auto &operator-- () noexcept
			{
				--mOffset;

				if(--mP[0] < mR->p1[0])
				{
					mP[0] = mR->p2[0] - 1;
					--mP[1];
				}

				return *this;
			}

			constexpr auto &operator++ () noexcept
			{
				++mOffset;

				if(++mP[0] >= mR->p2[0])
				{
					mP[0] = mR->p1[0];
					++mP[1];
				}

				return *this;
			}

			constexpr auto operator-- (int) noexcept
			{
				auto cp = *this;
				--(*this);
				return cp;
			}

			constexpr auto operator++ (int) noexcept
			{
				auto cp = *this;
				++(*this);
				return cp;
			}

			[[nodiscard]] constexpr reference operator [] (const difference_type i) const noexcept { return *(*this + i); }

			template <typename OT_>
			[[nodiscard]] constexpr auto operator <=> (const rect_iterator<OT_> &other) const noexcept
				{ return std::compare_three_way{}(offset(), other.offset()); }

			template <typename OT_>
			[[nodiscard]] constexpr bool operator == (const rect_iterator<OT_> &other) const noexcept
				{ return bool(mP == other.mP); }

			constexpr auto &operator += (const difference_type n) noexcept
			{
				mOffset += n;

				const auto d = mR->dims();
				mP[0] = mOffset % d[0];
				mP[1] = mOffset / d[0];

				mP += mR->p1;

				return *this;
			}

			constexpr auto &operator -= (const difference_type n) noexcept { return *this += -n; }

			[[nodiscard]] constexpr auto operator -(const difference_type n) const noexcept
				{ auto nit = *this; return (nit -= n); }

			template <typename OT_>
			[[nodiscard]] constexpr auto operator - (const rect_iterator<OT_> &other) const noexcept
				{ return (difference_type)offset() - (difference_type)other.offset(); }

			[[nodiscard]] friend constexpr auto operator +(rect_iterator it, const difference_type n) noexcept { auto nit = it; return (nit += n); }
			[[nodiscard]] friend constexpr auto operator +(const difference_type n, rect_iterator it) noexcept { auto nit = it; return (nit += n); }

		};

		template <std::integral Type_ = if32>
		class rectangle : public impl_rect::rectangle_base<Type_>
		{
		public:

			using impl_rect::rectangle_base<Type_>::rectangle_base;
			using typename impl_rect::rectangle_base<Type_>::point_type;
			using impl_rect::rectangle_base<Type_>::p1;
			using impl_rect::rectangle_base<Type_>::p2;

			using value_type = Type_;
			using size_type = Type_;
			using difference_type = typename std::incrementable_traits<Type_>::difference_type;
			using iterator = impl_rect::rect_iterator<value_type>;
			using const_iterator = iterator;
			using reverse_iterator = std::reverse_iterator<iterator>;
			using const_reverse_iterator = reverse_iterator;

			constexpr rectangle(const tag::range_init_c auto, rg::input_range auto &&r) noexcept
				requires std::convertible_to<rg::range_value_t<std::decay_t<decltype(r)>>, point_type>
			{
				*this = rg::accumulate(PL_FWD(r), rectangle{tag::inflowest}, [](const auto &init, auto &&v){
						return init.join_point(PL_FWD(v));
					});
			}

			[[nodiscard]] constexpr auto begin() const noexcept { return iterator{*this}; }
			[[nodiscard]] constexpr auto end() const noexcept { return iterator{*this, true}; }
			[[nodiscard]] constexpr auto cbegin() const noexcept { return iterator{*this}; }
			[[nodiscard]] constexpr auto cend() const noexcept { return iterator{*this, true}; }
			[[nodiscard]] constexpr auto rbegin() const noexcept { return reverse_iterator{end()}; }
			[[nodiscard]] constexpr auto rend() const noexcept { return reverse_iterator{begin()}; }
			[[nodiscard]] constexpr auto crbegin() const noexcept { return reverse_iterator{end()}; }
			[[nodiscard]] constexpr auto crend() const noexcept { return reverse_iterator{begin()}; }

			[[nodiscard]] constexpr auto size() const noexcept { return this->area(); }
			[[nodiscard]] constexpr bool empty() const noexcept { return size() > 0; }

			[[nodiscard]] constexpr rectangle clip(const rectangle &rect) const noexcept
				{ return { p1 /omax/ rect.p1, p2 /omin/ rect.p2 }; }

			[[nodiscard]] constexpr point_type clip_point(point_type v) const noexcept
				{ return p1 /omax/ v /omin/ (p2 - 1); }

			[[nodiscard]] constexpr rectangle join(const rectangle &rect) const noexcept
				{ return { p1 /omin/ rect.p1, p2 /omax/ rect.p2 }; }

			[[nodiscard]] constexpr rectangle join_point(point_type p) const noexcept
				{ return { p1 /omin/ p, p2 /omax/ p }; }

			[[nodiscard]] constexpr rectangle reorder() const noexcept
			{
				rectangle nr{*this};

				if(nr.p2[0] < nr.p1[0])
					adl_swap(nr.p2[0], nr.p1[0]);
				if(nr.p2[1] < nr.p1[1])
					adl_swap(nr.p2[1], nr.p1[1]);

				return nr;
			}

			template <typename CharT_, typename Traits_>
			friend auto &operator>>(std::basic_istream<CharT_, Traits_> &is, rectangle &r)
				{ return (is >> r.p1 >> r.p2); }

			template <typename CharT_, typename Traits_>
			friend auto &operator>=(std::basic_istream<CharT_, Traits_> &is, rectangle &r)
				{ using namespace io::rawio; return (is >= r.p1 >= r.p2); }

			template <typename CharT_, typename Traits_>
			friend auto &operator<<(std::basic_ostream<CharT_, Traits_> &os, const rectangle &r)
				{ return (os << r.p1 << ' ' << r.p2); }

			template <typename CharT_, typename Traits_>
			friend auto &operator<=(std::basic_ostream<CharT_, Traits_> &os, const rectangle &r)
				{ using namespace io::rawio; return (os <= r.p1 <= r.p2); }
		};

	} // namespace impl_rect

	using impl_rect::rectangle;
}

#ifdef PL_FMT
namespace fmt
{
	template <typename T_, typename Char_>
	struct formatter<::patlib::io::value_formatter<::patlib::rectangle<T_>>, Char_>
		: private formatter<typename ::patlib::rectangle<T_>::point_type>
	{
		using base_formatter_type = formatter<typename ::patlib::rectangle<T_>::point_type>;

		using base_formatter_type::parse;

  	auto format(const ::patlib::io::value_formatter<::patlib::rectangle<T_>> &p, auto &ctx) const
		{
			static constexpr char sep[] = " to ";

			ctx.advance_to(base_formatter_type::format(p.value.p1, ctx));
			ctx.advance_to(std::copy(std::begin(sep), std::end(sep), ctx.out()));
			return base_formatter_type::format(p.value.p2, ctx);
		}
	};
}
#endif
