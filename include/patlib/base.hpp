/*
	PatLib

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#if (__cplusplus <= 201703L)
	#error "PatLib requires at least a C++20 compiler"
#endif

#include <cstdint>
#include <cstddef>
#include <type_traits>

#include <boost/config.hpp>
#include <boost/predef.h>

#ifdef __COUNTER__
	#define PL_ANONYMOUS(s) BOOST_JOIN(s, __COUNTER__)
#else
	#define PL_ANONYMOUS(s) BOOST_JOIN(s, __LINE__)
#endif

#ifdef __PRETTY_FUNCTION__
	#define PL_FUNC __PRETTY_FUNCTION__
#else
	#define PL_FUNC __func__
#endif

#ifdef PatLib_SHARED
	#ifdef PatLib_EXPORTS
		#define PATLIB_API BOOST_SYMBOL_EXPORT
	#else
		#define PATLIB_API BOOST_SYMBOL_IMPORT
	#endif
#else
	#define PATLIB_API
#endif

#define PL_FWD(...) std::forward<decltype(__VA_ARGS__)>(__VA_ARGS__)

#ifdef __has_attribute
	#if __has_attribute(pure)
		#define PL_PURE_FC(...) __VA_ARGS__ __attribute__((pure))
	#else
		#define PL_PURE_FC(...) __VA_ARGS__
	#endif

	#if __has_attribute(const)
		#define PL_CONST_FC(...) __VA_ARGS__ __attribute__((const))
	#else
		#define PL_CONST_FC(...) __VA_ARGS__
	#endif
#else
	#define PL_PURE_FC(...) __VA_ARGS__
	#define PL_CONST_FC(...) __VA_ARGS__
#endif

namespace ranges::views {}
namespace boost::hana {}

namespace patlib
{
	inline namespace base_types
	{
		using u8 = std::uint8_t;
		using u16 = std::uint16_t;
		using u32 = std::uint32_t;
		using u64 = std::uint64_t;

		using i8 = std::int8_t;
		using i16 = std::int16_t;
		using i32 = std::int32_t;
		using i64 = std::int64_t;

		using uf8 = std::uint_fast8_t;
		using uf16 = std::uint_fast16_t;
		using uf32 = std::uint_fast32_t;
		using uf64 = std::uint_fast64_t;

		using if8 = std::int_fast8_t;
		using if16 = std::int_fast16_t;
		using if32 = std::int_fast32_t;
		using if64 = std::int_fast64_t;

		using smint = std::intmax_t;
		using umint = std::uintmax_t;

		using szt = std::size_t;
	}

	namespace rg = ranges;
	namespace rgv = rg::views;
	namespace hn = boost::hana;

	inline constexpr auto do_nothing = [](auto&&...) noexcept {};
	using do_nothing_t = decltype(do_nothing);

	#define PL_FORWARD_RETURN_LAMBDA_HELPER(...) noexcept(noexcept(__VA_ARGS__)) -> decltype(__VA_ARGS__) { return __VA_ARGS__; }

	#define PL_LAMBDA_FORWARD(...) [&](auto&&... args) PL_FORWARD_RETURN_LAMBDA_HELPER((__VA_ARGS__)(PL_FWD(args)...))
	#define PL_LAMBDA_FORWARD_THIS(fc) PL_LAMBDA_FORWARD(this->fc)

	#define PL_ANONYMOUS_TYPE decltype([]{})
}
