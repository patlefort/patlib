/*
	PatLib

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "base.hpp"
#include "memory.hpp"
#include "range.hpp"

#include <string>
#include <string_view>
#include <locale>
#include <type_traits>
#include <cstring>
#include <cstddef>
#include <cctype>
#include <ranges>

#include <boost/algorithm/string/trim.hpp>

namespace patlib
{
	template <typename CharT_ = char, typename Traits_ = std::char_traits<CharT_>>
	[[nodiscard]] constexpr auto trim(std::basic_string_view<CharT_, Traits_> sv)
	{
		const auto issp = [](const auto c){ return std::isspace((int)c) != 0; };

		sv.remove_prefix(
			std::distance( sv.begin(), std::find_if_not(sv.begin(), sv.end(), issp) ));
		sv.remove_suffix(
			std::distance( sv.rbegin(), std::find_if_not(sv.rbegin(), sv.rend(), issp) ));

		return sv;
	}

	template <typename CharT_ = char, typename Traits_ = std::char_traits<CharT_>>
	[[nodiscard]] constexpr auto trim_copy(const std::basic_string<CharT_, Traits_> &s)
	{
		return boost::algorithm::trim_copy(s);
	}

	template <typename CharT_ = char, typename Traits_ = std::char_traits<CharT_>>
	constexpr auto &trim(std::basic_string<CharT_, Traits_> &s)
	{
		boost::algorithm::trim(s);
		return s;
	}

	template <typename CharT_ = char, typename Traits_ = std::char_traits<CharT_>>
	[[nodiscard]] constexpr auto trim(std::basic_string_view<CharT_, Traits_> sv, const std::locale &loc)
	{
		const auto issp = [&](const auto c){ return std::isspace(c, loc); };

		sv.remove_prefix(
			std::distance( sv.begin(), std::find_if_not(sv.begin(), sv.end(), issp) ));
		sv.remove_suffix(
			std::distance( sv.rbegin(), std::find_if_not(sv.rbegin(), sv.rend(), issp) ));

		return sv;
	}

	template <typename CharT_ = char, typename Traits_ = std::char_traits<CharT_>>
	[[nodiscard]] constexpr auto trim_copy(const std::basic_string<CharT_, Traits_> &s, const std::locale &loc)
	{
		return boost::algorithm::trim_copy(s, loc);
	}

	template <typename CharT_ = char, typename Traits_ = std::char_traits<CharT_>>
	constexpr auto &trim(std::basic_string<CharT_, Traits_> &s, const std::locale &loc)
	{
		boost::algorithm::trim(s, loc);
		return s;
	}

	template <typename CharT_ = char, typename Traits_ = std::char_traits<CharT_>>
	[[nodiscard]] constexpr auto remove_prefix(std::basic_string_view<CharT_, Traits_> sv, typename std::basic_string_view<CharT_, Traits_>::size_type nb)
	{
		sv.remove_prefix(nb);
		return sv;
	}

	template <typename CharT_ = char, typename Traits_ = std::char_traits<CharT_>>
	[[nodiscard]] constexpr auto remove_suffix(std::basic_string_view<CharT_, Traits_> sv, typename std::basic_string_view<CharT_, Traits_>::size_type nb)
	{
		sv.remove_suffix(nb);
		return sv;
	}

	constexpr decltype(auto) strconcat(auto &&into, auto&&... args)
	{
		constexpr auto getSize = [](auto&& i)
			{
				if constexpr(std::is_pointer_v<std::remove_cvref_t<decltype(i)>>)
					return std::strlen(i);
				else if constexpr(std::is_integral_v<std::remove_cvref_t<decltype(i)>>)
					return 1;
				else if constexpr(rg::sized_range<decltype(i)>)
					return rg::size(PL_FWD(i)) - std::is_bounded_array_v<std::remove_cvref_t<decltype(i)>>;
			};

		const auto totalSize = ( into.size() + ... + getSize(PL_FWD(args)) );
		try_reserve(into, totalSize);

		const auto doInsert = [&](auto &into, auto&& i)
			{
				if constexpr(std::is_pointer_v<std::remove_cvref_t<decltype(i)>> || std::is_integral_v<std::remove_cvref_t<decltype(i)>>)
					into += PL_FWD(i);
				else if constexpr(rg::contiguous_range<decltype(i)> && rg::sized_range<decltype(i)>)
					into.append(range_data(PL_FWD(i)), getSize(PL_FWD(i)));
				else
					into.append(rg::cbegin(PL_FWD(i)), rg::cend(PL_FWD(i)));
			};

		( doInsert(into, PL_FWD(args)), ... );

		return PL_FWD_RETURN(into);
	}

	template <typename... Ts_>
	[[nodiscard]] constexpr auto concat_char_arrays(const Ts_ &... a) noexcept
	{
		constexpr auto totalSize = (0 + ... + (rg::size(Ts_{}) - std::is_bounded_array_v<Ts_>) );
		std::array<char, totalSize> res{};

		auto it = res.begin();
		((it = std::copy(rg::begin(a), rg::end(a) - std::is_array_v<Ts_>, it)), ...);

		return res;
	}

	class scoped_locale_assign : no_copy
	{
	public:
		scoped_locale_assign(int category, const char* locale)
		{
			mOld = std::setlocale(category, locale);
			mCategory = category;
		}

		~scoped_locale_assign() noexcept(false)
		{
			std::setlocale(mCategory, mOld);
		}

	private:
		char *mOld;
		int mCategory;
	};

	inline namespace string_view_op
	{
		[[nodiscard]] inline auto operator+ (std::string_view str, std::string_view sview)
		{
			std::string ns;
			ns.reserve(str.size() + sview.size());
			ns.append(str.data(), str.size());

			return ns.append(sview.data(), sview.size());
		}

		[[nodiscard]] inline auto operator+ (std::string_view str, char c)
		{
			std::string ns;
			ns.reserve(str.size() + 1);
			ns.append(str.data(), str.size());

			return ns += c;
		}

		[[nodiscard]] inline auto operator+ (std::wstring_view str, std::wstring_view sview)
		{
			std::wstring ns;
			ns.reserve(str.size() + sview.size());
			ns.append(str.data(), str.size());

			return ns.append(sview.data(), sview.size());
		}

		[[nodiscard]] inline auto operator+ (std::wstring_view str, wchar_t c)
		{
			std::wstring ns;
			ns.reserve(str.size() + 1);
			ns.append(str.data(), str.size());

			return ns += c;
		}
	} // namespace string_view_op

	template <typename Range_>
	[[nodiscard]] constexpr auto to_string_view(Range_ &&c) noexcept
		requires
			(rg::contiguous_range<Range_> && rg::sized_range<Range_> && !std::is_array_v<Range_> &&
			!(CPointer<Range_> || std::is_unbounded_array_v<Range_>))
	{
		return std::basic_string_view<std::remove_cv_t<rg::range_value_t<Range_>>>{PL_FWD(c)};
	}

	template <typename CharT_>
		requires (CPointer<CharT_> || std::is_unbounded_array_v<CharT_>)
	[[nodiscard]] constexpr auto to_string_view(const CharT_ &cStr) noexcept
		{ return std::basic_string_view<std::decay_t<std::remove_pointer_t<std::decay_t<CharT_>>>>{cStr}; }

	template <typename CharT_, szt N_>
	[[nodiscard]] constexpr auto to_string_view(const CharT_ (&cStr)[N_]) noexcept
		{ return std::basic_string_view<CharT_>{cStr, N_ - 1}; }

	template <typename CharT_ = char>
	inline constexpr auto string_view_adaptor = std::ranges::views::transform(
			[](auto&& str_range) { return std::basic_string_view<CharT_>{PL_FWD(str_range).begin(), PL_FWD(str_range).end()}; }
		);

	template <typename CharT_ = char>
	inline constexpr auto string_adaptor = std::ranges::views::transform(
			[](auto&& str_range) { return std::basic_string<CharT_>{PL_FWD(str_range).begin(), PL_FWD(str_range).end()}; }
		);

	template <typename CharT_ = char>
	constexpr auto split_string(CharT_ ch)
		{ return std::ranges::views::split(ch) | string_view_adaptor<CharT_>; }

	constexpr auto split_string_no_empty(auto ch)
	{
		return split_string(ch)
			| std::ranges::views::filter([](const auto &str){
					return !str.empty();
			});
	}
}
