/*
	PatLib

	Copyright (C) 2021 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "../base.hpp"
#include "../algorithm.hpp"
#include "../omp.hpp"

namespace patlib::impl_algo
{
#ifdef _OPENMP
	template <typename T_>
	concept COMPLoopPolicy = CParallelPolicy<T_> && CAnyOf<T_,
		policy::parallel_unbalanced_t,
		policy::parallel_balanced_t,
		policy::parallel_t>;

	// BUG: Causes ambiguity.
	//template <COMPLoopPolicy Policy_>
	void tag_invoke(tag_t<for_each>, CRandomAccessValueRange auto&& r, auto fc, auto proj, const COMPLoopPolicy auto &pol)
		requires(
			CIndirectlyInvocable<decltype(fc), rg::projected<rg::iterator_t<decltype(r)>, decltype(proj)>>)
	{
		const auto nbThreads = select_nb_threads(pol);
		[[maybe_unused]] const auto chunkSize = pol.chunk_size();

		const auto loopImpl = [&](auto&& catchingFc)
			{
				const auto handleElement = [&](auto&& v)
					{ PL_FWD(catchingFc)([&]{ std::invoke(fc, std::invoke(proj, PL_FWD(v))); }); };

				if constexpr(std::same_as<decay_t<decltype(pol)>, policy::parallel_t>)
				{
					const auto sched = set_omp_schedule(pol);

					#pragma omp parallel for num_threads(nbThreads)
					for(auto&& v : PL_FWD(r))
						handleElement(PL_FWD(v));
				}
				else if constexpr(std::same_as<decay_t<decltype(pol)>, policy::parallel_balanced_t>)
				{
					#pragma omp parallel for num_threads(nbThreads) schedule(dynamic, chunkSize)
					for(auto&& v : PL_FWD(r))
						handleElement(PL_FWD(v));
				}
				else
				{
					if(chunkSize > 0)
					{
						#pragma omp parallel for num_threads(nbThreads) schedule(static, chunkSize)
						for(auto&& v : PL_FWD(r))
							handleElement(PL_FWD(v));
					}
					else
					{
						#pragma omp parallel for num_threads(nbThreads) schedule(static)
						for(auto&& v : PL_FWD(r))
							handleElement(PL_FWD(v));
					}
				}
			};

		throwing_parallel_loop(loopImpl);
	}

	void tag_invoke(tag_t<transform>, CRandomAccessValueRange auto&& r, CRandomAccessValueIterator auto into, auto fc, auto proj, const COMPLoopPolicy auto &pol)
		requires(
			rg::sized_range<decltype(r)> &&
			CIndirectlyInvocable<decltype(fc), rg::projected<rg::iterator_t<decltype(r)>, decltype(proj)>> &&
			COutputValueIterator<decltype(into), rg::indirect_result_t<decltype(fc), rg::projected<rg::iterator_t<decltype(r)>, decltype(proj)>>>)
	{
		const auto nbThreads = select_nb_threads(pol);
		[[maybe_unused]] const auto chunkSize = pol.chunk_size();

		rg::iter_difference_t<decltype(into)> i{};

		const auto loopImpl = [&](auto&& catchingFc)
			{
				const auto handleElement = [&](auto&& v, auto &i)
					{ PL_FWD(catchingFc)([&]{ *(into + i++) = std::invoke(fc, std::invoke(proj, PL_FWD(v))); }); };

				if constexpr(std::same_as<decay_t<decltype(pol)>, policy::parallel_t>)
				{
					const auto sched = set_omp_schedule(pol);

					#pragma omp parallel for num_threads(nbThreads) linear(i)
					for(auto &&v : PL_FWD(r))
						handleElement(PL_FWD(v), i);
				}
				else if constexpr(std::same_as<decay_t<decltype(pol)>, policy::parallel_balanced_t>)
				{
					#pragma omp parallel for num_threads(nbThreads) linear(i) schedule(dynamic, chunkSize)
					for(auto &&v : PL_FWD(r))
						handleElement(PL_FWD(v), i);
				}
				else
				{
					if(chunkSize > 0)
					{
						#pragma omp parallel for num_threads(nbThreads) linear(i) schedule(static, chunkSize)
						for(auto &&v : PL_FWD(r))
							handleElement(PL_FWD(v), i);
					}
					else
					{
						#pragma omp parallel for num_threads(nbThreads) linear(i) schedule(static)
						for(auto &&v : PL_FWD(r))
							handleElement(PL_FWD(v), i);
					}
				}
			};

		throwing_parallel_loop(loopImpl);
	}

	void tag_invoke(tag_t<transform>, CRandomAccessValueRange auto&& r1, CRandomAccessValueRange auto&& r2, CRandomAccessValueIterator auto into, auto fc, auto proj1, auto proj2, const COMPLoopPolicy auto &pol)
		requires(
			rg::sized_range<decltype(r1)> && rg::sized_range<decltype(r2)> &&
			CIndirectlyInvocable<decltype(fc), rg::projected<rg::iterator_t<decltype(r1)>, decltype(proj1)>, rg::projected<rg::iterator_t<decltype(r2)>, decltype(proj2)>> &&
			COutputValueIterator<decltype(into), rg::indirect_result_t<decltype(fc), rg::projected<rg::iterator_t<decltype(r1)>, decltype(proj1)>, rg::projected<rg::iterator_t<decltype(r2)>, decltype(proj2)>>>)
	{
		const auto nbThreads = select_nb_threads(pol);
		[[maybe_unused]] const auto chunkSize = pol.chunk_size();

		rg::iter_difference_t<decltype(into)> i{};

		const auto loopImpl = [&](auto&& catchingFc)
			{
				const auto handleElement = [&](auto&& v1, auto&& v2, auto &i)
					{
						PL_FWD(catchingFc)([&]{
							*(into + i++) = std::invoke(fc, std::invoke(proj1, PL_FWD(v1)), std::invoke(proj2, PL_FWD(v2)));
						});
					};

				if constexpr(std::same_as<decay_t<decltype(pol)>, policy::parallel_t>)
				{
					const auto sched = set_omp_schedule(pol);

					#pragma omp parallel for num_threads(nbThreads) linear(i)
					for(auto&& vs : rgv::zip(PL_FWD(r1), PL_FWD(r2)))
						handleElement(std::get<0>(PL_FWD(vs)), std::get<1>(PL_FWD(vs)), i);
				}
				else if constexpr(std::same_as<decay_t<decltype(pol)>, policy::parallel_balanced_t>)
				{
					#pragma omp parallel for num_threads(nbThreads) linear(i) schedule(dynamic, chunkSize)
					for(auto&& vs : rgv::zip(PL_FWD(r1), PL_FWD(r2)))
						handleElement(std::get<0>(PL_FWD(vs)), std::get<1>(PL_FWD(vs)), i);
				}
				else
				{
					if(chunkSize > 0)
					{
						#pragma omp parallel for num_threads(nbThreads) linear(i) schedule(static, chunkSize)
						for(auto&& vs : rgv::zip(PL_FWD(r1), PL_FWD(r2)))
							handleElement(std::get<0>(PL_FWD(vs)), std::get<1>(PL_FWD(vs)), i);
					}
					else
					{
						#pragma omp parallel for num_threads(nbThreads) linear(i) schedule(static)
						for(auto&& vs : rgv::zip(PL_FWD(r1), PL_FWD(r2)))
							handleElement(std::get<0>(PL_FWD(vs)), std::get<1>(PL_FWD(vs)), i);
					}
				}
			};

		throwing_parallel_loop(loopImpl);
	}

	void tag_invoke(tag_t<copy>, CRandomAccessValueRange auto&& r, CRandomAccessValueIterator auto into, const COMPLoopPolicy auto &pol)
		requires (COutputValueIterator<decltype(into), rg::range_reference_t<decltype(r)>> && rg::sized_range<decltype(r)>)
	{
		const auto nbThreads = select_nb_threads(pol);
		[[maybe_unused]] const auto chunkSize = pol.chunk_size();

		rg::iter_difference_t<decltype(into)> i{};

		const auto loopImpl = [&](auto&& catchingFc)
			{
				const auto handleElement = [&](auto&& v, auto &i)
					{ PL_FWD(catchingFc)([&]{ *(into + i++) = PL_FWD(v); }); };

				if constexpr(std::same_as<decay_t<decltype(pol)>, policy::parallel_t>)
				{
					const auto sched = set_omp_schedule(pol);

					#pragma omp parallel for num_threads(nbThreads) linear(i)
					for(auto &&v : PL_FWD(r))
						handleElement(PL_FWD(v), i);
				}
				else if constexpr(std::same_as<decay_t<decltype(pol)>, policy::parallel_balanced_t>)
				{
					#pragma omp parallel for num_threads(nbThreads) linear(i) schedule(dynamic, chunkSize)
					for(auto &&v : PL_FWD(r))
						handleElement(PL_FWD(v), i);
				}
				else
				{
					if(chunkSize > 0)
					{
						#pragma omp parallel for num_threads(nbThreads) linear(i) schedule(static, chunkSize)
						for(auto &&v : PL_FWD(r))
							handleElement(PL_FWD(v), i);
					}
					else
					{
						#pragma omp parallel for num_threads(nbThreads) linear(i) schedule(static)
						for(auto &&v : PL_FWD(r))
							handleElement(PL_FWD(v), i);
					}
				}
			};

		throwing_parallel_loop(loopImpl);
	}
#endif
}
