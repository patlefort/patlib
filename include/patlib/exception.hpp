/*
	PatLib

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "base.hpp"
#include "concepts.hpp"

#include <exception>
#include <string>
#include <string_view>
#include <experimental/source_location>

namespace patlib
{
	class PATLIB_API Exception : public std::exception
	{
	public:
		Exception(std::string_view message, const std::experimental::source_location &sl = std::experimental::source_location::current()) noexcept;
		virtual ~Exception() = default;

		[[nodiscard]] virtual std::string_view message() const noexcept
		{
			return mMessage;
		}

		[[nodiscard]] virtual const char *what() const noexcept override
		{
			return mMessage.c_str();
		}

		[[nodiscard]] virtual const char *module() const noexcept
		{
			return "";
		}

	protected:
		std::string mMessage;
	};

	template <typename T_>
	concept CExceptionHandler = std::invocable<T_, std::exception_ptr>;

	void recurse_nested_exceptions(std::exception_ptr eptr, CExceptionHandler auto&& fc)
	{
		if(!eptr)
			return;

		PL_FWD(fc)(eptr);

		try
		{
			std::rethrow_exception(eptr);
		}
		catch(const std::exception &e)
		{
			try
			{
				std::rethrow_if_nested(e);
			}
			catch(...)
			{
				recurse_nested_exceptions(std::current_exception(), PL_FWD(fc));
			}
		}
		catch(...) {}
	}
}
