/*
	PatLib

	Copyright (C) 2022 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "base.hpp"
#include "type.hpp"
#include "utility.hpp"
#include "concepts.hpp"

#include <bit>
#include <optional>
#include <cmath>

namespace patlib
{
	namespace marked_optional_impl
	{
		template <typename T_>
		class storage
		{
		public:
			using value_type = T_;

			constexpr storage() = default;

			PL_DEFAULT_COPYMOVE(storage)

			template <typename... Args_>
				requires std::constructible_from<T_, Args_...>
			constexpr explicit storage(std::in_place_t, Args_&&... args) :
				m_value{PL_FWD(args)...} {}

			template <typename U_, typename... Args_>
				requires std::constructible_from<T_, std::initializer_list<U_>, Args_...>
			constexpr explicit storage(std::in_place_t, std::initializer_list<U_> ilist, Args_&&... args) :
				m_value{ilist, PL_FWD(args)...} {}

			template <typename U_ = T_>
				requires (
					std::constructible_from<T_, U_&&> &&
					!std::same_as<std::remove_cvref_t<U_>, storage<T_>>)
			constexpr explicit(!std::convertible_to<U_&&, T_>) storage(U_&& v) :
				m_value{PL_FWD(v)} {}

			[[nodiscard]] constexpr const T_ &value() const & noexcept { return m_value; }
			[[nodiscard]] constexpr T_ &value() & noexcept { return m_value; }
			[[nodiscard]] constexpr const T_ &&value() const && noexcept { return std::move(m_value); }
			[[nodiscard]] constexpr T_ &&value() && noexcept { return std::move(m_value); }

		private:
			T_ m_value;
		};

		template <typename T_>
		struct optional_policy : public storage<std::optional<T_>>
		{
			using type = T_;

			PL_DEFAULT_COPYMOVE(optional_policy)
			using storage<std::optional<T_>>::storage;

			constexpr optional_policy() = default;

			template <typename... Args_>
				requires std::constructible_from<T_, Args_...>
			constexpr explicit optional_policy(std::in_place_t, Args_&&... args) :
				storage<std::optional<T_>>(std::in_place, std::in_place, PL_FWD(args)...) {}

			template <typename U_, typename... Args_>
				requires std::constructible_from<T_, std::initializer_list<U_>, Args_...>
			constexpr explicit optional_policy(std::in_place_t, std::initializer_list<U_> ilist, Args_&&... args) :
				storage<std::optional<T_>>(std::in_place, std::in_place, ilist, PL_FWD(args)...) {}

			template <typename NewT_>
			using rebind = optional_policy<NewT_>;

			[[nodiscard]] constexpr const type &value() const & noexcept { return storage<std::optional<T_>>::value().value(); }
			[[nodiscard]] constexpr type &value() & noexcept { return storage<std::optional<T_>>::value().value(); }
			[[nodiscard]] constexpr const type &&value() const && noexcept { return std::move(storage<std::optional<T_>>::value().value()); }
			[[nodiscard]] constexpr type &&value() && noexcept { return std::move(storage<std::optional<T_>>::value().value()); }

			template <typename U_>
				//requires std::assignable_from<T_, U_&&>
			constexpr void set(U_&& v) { storage<std::optional<T_>>::value() = PL_FWD(v); }

			constexpr void mark()
				{ storage<std::optional<T_>>::value().reset(); }

			[[nodiscard]] constexpr bool is_marked() const noexcept
				{ return !storage<std::optional<T_>>::value().has_value(); }
		};

		template <typename T_>
		struct default_policy : public optional_policy<T_>
		{
			template <typename NewT_>
			using rebind = default_policy<NewT_>;

			using optional_policy<T_>::optional_policy;
		};

		template <typename T_>
		struct default_policy<T_*> : public storage<T_*>
		{
			using type = T_*;

			PL_DEFAULT_COPYMOVE(default_policy)
			using storage<type>::storage;
			using storage<type>::value;

			constexpr default_policy() noexcept : storage<type>( std::in_place, std::bit_cast<type>(std::numeric_limits<std::uintptr_t>::max()) ) {}

			template <typename NewT_>
			using rebind = default_policy<NewT_>;

			constexpr void set(type v) noexcept { value() = v; }

			constexpr void mark() noexcept
				{ value() = std::bit_cast<type>(std::numeric_limits<std::uintptr_t>::max()); }

			[[nodiscard]] constexpr bool is_marked() const noexcept
				{ return std::bit_cast<std::uintptr_t>(value()) == std::numeric_limits<std::uintptr_t>::max(); }
		};

		template <typename T_>
			requires CReference<T_>
		struct default_policy<T_> : default_policy<std::add_pointer_t<T_>>
		{
			using type = T_;

			PL_DEFAULT_COPYMOVE(default_policy)

			constexpr default_policy() = default;

			constexpr explicit default_policy(std::in_place_t, type arg) noexcept :
				default_policy<std::add_pointer_t<T_>>(std::in_place, &arg) {}

			template <typename NewT_>
			using rebind = default_policy<NewT_>;

			constexpr void set(type v) noexcept
				{ static_cast<default_policy<std::add_pointer_t<T_>> &>(*this).value() = &v; }

			using default_policy<std::add_pointer_t<T_>>::mark;
			using default_policy<std::add_pointer_t<T_>>::is_marked;

			[[nodiscard]] constexpr const type value() const noexcept { return *static_cast<const default_policy<std::add_pointer_t<T_>> &>(*this).value(); }
			[[nodiscard]] constexpr type value() noexcept { return *static_cast<default_policy<std::add_pointer_t<T_>> &>(*this).value(); }
		};

		template<typename T_>
		concept CMarkableType =
			requires(T_ n, const T_ cn)
			{
				{ cn.has_value() } noexcept -> std::same_as<bool>;
				{ n.reset() } noexcept;
			};

		template<typename T_>
		concept CHasInitialValue = CMarkableType<T_> && requires { { T_::MarkedOptionalInitialValue } -> std::convertible_to<T_>; };

		template <typename T_>
			requires CMarkableType<T_>
		struct default_policy<T_> : public storage<T_>
		{
			using type = T_;

			PL_DEFAULT_COPYMOVE(default_policy)
			using storage<type>::storage;
			using storage<type>::value;

			constexpr default_policy() requires (!CHasInitialValue<T_>) :
				storage<type>()
			{}

			constexpr default_policy() requires CHasInitialValue<T_> :
				storage<type>(std::in_place, T_::MarkedOptionalInitialValue)
			{}

			template <typename NewT_>
			using rebind = default_policy<NewT_>;

			template <typename U_>
				//requires std::assignable_from<T_, U_&&>
			constexpr void set(U_&& v) { value() = PL_FWD(v); }

			constexpr void mark()
				{ value().reset(); }

			[[nodiscard]] constexpr bool is_marked() const noexcept
				{ return !value().has_value(); }
		};

		template <typename T_>
			requires std::floating_point<T_>
		struct default_policy<T_> : public storage<T_>
		{
			using type = T_;

			PL_DEFAULT_COPYMOVE(default_policy)
			using storage<type>::storage;
			using storage<type>::value;

			constexpr default_policy() noexcept : storage<type>(std::in_place, std::numeric_limits<T_>::quiet_NaN()) {}

			template <typename NewT_>
			using rebind = default_policy<NewT_>;

			constexpr void set(T_ v) noexcept { value() = v; }

			constexpr void mark() noexcept
				{ value() = std::numeric_limits<T_>::quiet_NaN(); }

			[[nodiscard]] constexpr bool is_marked() const noexcept
				{ return std::isnan(value()); }
		};

		template <typename T_, typename NewT_>
		concept CRebindablePolicy = requires { typename T_::template rebind<NewT_>; };

		template <typename Policy_, typename>
		struct try_rebind_policy_helper
		{
			using type = Policy_;
		};

		template <typename Policy_, typename NewT_>
			requires CRebindablePolicy<Policy_, NewT_>
		struct try_rebind_policy_helper<Policy_, NewT_>
		{
			using type = typename Policy_::template rebind<NewT_>;
		};

		template <typename Policy_, typename NewT>
		using try_rebind_policy = typename try_rebind_policy_helper<Policy_, NewT>::type;

	} // namespace marked_optional_impl

	template<typename T_>
	concept CMarkablePolicy =
		requires
		{
			typename T_::type;
		} &&
		requires(T_ pol, const T_ cpol, const typename T_::type cn)
		{
			{ pol.mark() };
			{ pol.set(cn) };
			{ cpol.is_marked() } noexcept -> std::same_as<bool>;
			{ cpol.value() } noexcept -> std::same_as<const typename T_::type &>;
			{ pol.value() } noexcept -> std::same_as<typename T_::type &>;
		};

	struct nullopt_t {};
	inline constexpr auto nullopt = nullopt_t{};

	template<typename T_>
	using marked_optional_default_policy = marked_optional_impl::default_policy<T_>;

	template<typename T_>
	using marked_optional_optional_policy = marked_optional_impl::optional_policy<T_>;

	template <typename T_>
	using marked_optional_default_storage = marked_optional_impl::storage<T_>;

	template <typename T_, CMarkablePolicy Policy_ = marked_optional_default_policy<T_>>
	class marked_optional
	{
	public:
		using value_type = T_;
		using policy_type = Policy_;

		constexpr marked_optional() = default;
		constexpr marked_optional(nullopt_t) : marked_optional() {}

		PL_DEFAULT_COPYMOVE(marked_optional)

		template <typename... Args_>
			requires std::constructible_from<T_, Args_...>
		constexpr explicit marked_optional(std::in_place_t, Args_&&... args) :
			m_storage{std::in_place, PL_FWD(args)...} {}

		template <typename U_, typename... Args_>
			requires std::constructible_from<T_, std::initializer_list<U_>, Args_...>
		constexpr explicit marked_optional(std::in_place_t, std::initializer_list<U_> ilist, Args_&&... args) :
			m_storage{std::in_place, ilist, PL_FWD(args)...} {}

		template <typename U_ = T_>
			requires (
				std::constructible_from<T_, U_&&> &&
				!std::same_as<std::remove_cvref_t<U_>, nullopt_t> &&
				!std::same_as<std::remove_cvref_t<U_>, marked_optional<T_, Policy_>>)
		constexpr explicit(!std::convertible_to<U_&&, T_>) marked_optional(U_&& v) :
			m_storage{std::in_place, PL_FWD(v)} {}

		constexpr auto &operator=(nullopt_t) noexcept
			{ m_storage.mark(); return *this; }

		template <typename U_ = T_>
			requires (
				std::assignable_from<T_, U_&&> &&
				!std::same_as<std::remove_cvref_t<U_>, nullopt_t> &&
				!std::same_as<std::remove_cvref_t<U_>, marked_optional<T_, Policy_>>)
		constexpr auto &operator=(U_&& v) noexcept(std::is_nothrow_assignable_v<T_, U_&&>)
			{ m_storage.set(PL_FWD(v)); return *this; }

		[[nodiscard]] constexpr const T_ &value() const & noexcept { return m_storage.value(); }
		[[nodiscard]] constexpr T_ &value() & noexcept { return m_storage.value(); }
		[[nodiscard]] constexpr const T_ &&value() const && noexcept
				requires (!CReference<T_>)
			{ return std::move(std::move(m_storage).value()); }
		[[nodiscard]] constexpr const T_ &value() const && noexcept
				requires CReference<T_>
			{ return m_storage.value(); }
		[[nodiscard]] constexpr T_ &&value() && noexcept
				requires (!CReference<T_>)
			{ return std::move(std::move(m_storage).value()); }
		[[nodiscard]] constexpr T_ &value() && noexcept
				requires CReference<T_>
			{ return m_storage.value(); }

		[[nodiscard]] constexpr std::add_pointer_t<T_> to_pointer() noexcept
				requires CReference<T_>
			{ return has_value() ? &value() : nullptr; }

		[[nodiscard]] constexpr const std::remove_reference_t<T_> * operator->() const noexcept { return &value(); }
		[[nodiscard]] constexpr std::remove_reference_t<T_>* operator->() noexcept { return &value(); }

		[[nodiscard]] constexpr bool has_value() const noexcept { return !m_storage.is_marked(); }
		[[nodiscard]] constexpr explicit operator bool() const noexcept { return has_value(); }

		[[nodiscard]] constexpr const T_& operator*() const & noexcept { return value(); }
		[[nodiscard]] constexpr T_& operator*() & noexcept { return value(); }
		[[nodiscard]] constexpr const T_&& operator*() const && noexcept
				requires (!CReference<T_>)
			{ return std::move(value()); }
		[[nodiscard]] constexpr const T_& operator*() const && noexcept
				requires CReference<T_>
			{ return value(); }
		[[nodiscard]] constexpr T_&& operator*() && noexcept
				requires (!CReference<T_>)
			{ return std::move(value()); }
		[[nodiscard]] constexpr T_& operator*() && noexcept
				requires CReference<T_>
			{ return value(); }

		[[nodiscard]] constexpr T_ value_or(auto&& default_value) const &
			{ return has_value() ? value() : static_cast<T_>(PL_FWD(default_value)); }
		[[nodiscard]] constexpr T_ value_or(auto&& default_value) const &&
				requires (!CReference<T_>)
			{ return has_value() ? std::move(value()) : static_cast<T_>(PL_FWD(default_value)); }
		[[nodiscard]] constexpr T_ value_or(auto&& default_value) const &&
				requires CReference<T_>
			{ return has_value() ? value() : static_cast<T_>(PL_FWD(default_value)); }

		template <typename F_>
		[[nodiscard]] constexpr auto and_then(F_&& f) &
		{
			if(has_value())
				return std::invoke(PL_FWD(f), value());
			else
				return std::remove_cvref_t<std::invoke_result_t<F_, T_&>>();
		}

		template <typename F_>
		[[nodiscard]] constexpr auto and_then(F_&& f) const &
		{
			if(has_value())
				return std::invoke(PL_FWD(f), value());
			else
				return std::remove_cvref_t<std::invoke_result_t<F_, const T_&>>();
		}

		template <typename F_>
		[[nodiscard]] constexpr auto and_then(F_&& f) &&
		{
			if(has_value())
				if constexpr(CReference<T_>)
					return std::invoke(PL_FWD(f), value());
				else
					return std::invoke(PL_FWD(f), std::move(value()));
			else
				return std::remove_cvref_t<std::invoke_result_t<F_, T_>>();
		}

		template <typename F_>
		[[nodiscard]] constexpr auto and_then(F_&& f) const &&
		{
			if(has_value())
				if constexpr(CReference<T_>)
					return std::invoke(PL_FWD(f), value());
				else
					return std::invoke(PL_FWD(f), std::move(value()));
			else
				return std::remove_cvref_t<std::invoke_result_t<F_, const T_>>();
		}

		template <typename F_, typename NewPolicy_ = marked_optional_impl::try_rebind_policy<Policy_, std::remove_cvref_t<std::invoke_result_t<F_, T_>>>>
		[[nodiscard]] constexpr auto transform(F_&& f, type_c<NewPolicy_> = {}) &
			-> marked_optional<std::invoke_result_t<F_, T_>, NewPolicy_>
		{
			if(has_value())
				return std::invoke(PL_FWD(f), value());
			else
				return {};
		}

		template <typename F_, typename NewPolicy_ = marked_optional_impl::try_rebind_policy<Policy_, std::remove_cvref_t<std::invoke_result_t<F_, const T_>>>>
		[[nodiscard]] constexpr auto transform(F_&& f, type_c<NewPolicy_> = {}) const &
			-> marked_optional<std::invoke_result_t<F_, const T_>, NewPolicy_>
		{
			if(has_value())
				return std::invoke(PL_FWD(f), value());
			else
				return {};
		}

		template <typename F_, typename NewPolicy_ = marked_optional_impl::try_rebind_policy<Policy_, std::remove_cvref_t<std::invoke_result_t<F_, T_>>>>
		[[nodiscard]] constexpr auto transform(F_&& f, type_c<NewPolicy_> = {}) &&
			-> marked_optional<std::invoke_result_t<F_, T_>, NewPolicy_>
		{
			if(has_value())
				if constexpr(CReference<T_>)
					return std::invoke(PL_FWD(f), value());
				else
					return std::invoke(PL_FWD(f), std::move(value()));
			else
				return {};
		}

		template <typename F_, typename NewPolicy_ = marked_optional_impl::try_rebind_policy<Policy_, std::remove_cvref_t<std::invoke_result_t<F_, const T_>>>>
		[[nodiscard]] constexpr auto transform(F_&& f, type_c<NewPolicy_> = {}) const &&
			-> marked_optional<std::invoke_result_t<F_, const T_>, NewPolicy_>
		{
			if(has_value())
				if constexpr(CReference<T_>)
					return std::invoke(PL_FWD(f), value());
				else
					return std::invoke(PL_FWD(f), std::move(value()));
			else
				return {};
		}

		template <typename F_>
		[[nodiscard]] constexpr marked_optional or_else(F_&& f) const &
		{
			if(has_value())
				return *this;
			else
				return PL_FWD(f)();
		}

		template <typename F_>
		[[nodiscard]] constexpr marked_optional or_else(F_&& f) &&
		{
			if(has_value())
				return std::move(*this);
			else
				return PL_FWD(f)();
		}

		constexpr void swap(marked_optional &other) noexcept(CNoThrowSwappable<T_>)
				requires CSwappable<Policy_>
			{ adl_swap(m_storage, other.m_storage); }

		constexpr void reset() noexcept { m_storage.mark(); }

		template <typename... Args_>
			requires (std::constructible_from<T_, Args_...> && !CReference<T_>)
		constexpr T_ &emplace(Args_&&... args)
		{
			recreate(m_storage, std::in_place, PL_FWD(args)...);
			return value();
		}

		template <typename U_, typename... Args_>
			requires (std::constructible_from<T_, std::initializer_list<U_>, Args_...> && !CReference<T_>)
		constexpr T_ &emplace(std::initializer_list<U_> ilist, Args_&&... args)
		{
			recreate(m_storage, std::in_place, ilist, PL_FWD(args)...);
			return value();
		}

	private:
		Policy_ m_storage;
	};

	template <typename T_> constinit bool is_marked_optional = false;
	template <typename T_, typename Policy_> constinit bool is_marked_optional<marked_optional<T_, Policy_>> = true;

	template <typename T_, typename U_>
	[[nodiscard]] constexpr auto operator==(const marked_optional<T_> &opt, const U_ &v) noexcept
		{ return opt && *opt == v; }

	template <typename T_, typename U_>
	[[nodiscard]] constexpr auto operator==(const U_ &v, const marked_optional<T_> &opt) noexcept
		{ return opt && v == *opt; }

	template <typename T_, typename U_>
	[[nodiscard]] constexpr auto operator!=(const marked_optional<T_> &opt, const U_ &v) noexcept
		{ return !opt || *opt != v; }

	template <typename T_, typename U_>
	[[nodiscard]] constexpr auto operator!=(const U_ &v, const marked_optional<T_> &opt) noexcept
		{ return !opt || v != *opt; }

	template <typename T_, typename U_>
	[[nodiscard]] constexpr auto operator<(const marked_optional<T_> &opt, const U_ &v) noexcept
		{ return !opt || *opt < v; }

	template <typename T_, typename U_>
	[[nodiscard]] constexpr auto operator<(const U_ &v, const marked_optional<T_> &opt) noexcept
		{ return opt && v < *opt; }

	template <typename T_, typename U_>
	[[nodiscard]] constexpr auto operator>(const marked_optional<T_> &opt, const U_ &v) noexcept
		{ return opt && *opt > v; }

	template <typename T_, typename U_>
	[[nodiscard]] constexpr auto operator>(const U_ &v, const marked_optional<T_> &opt) noexcept
		{ return !opt || v > *opt; }

	template <typename T_, typename U_>
	[[nodiscard]] constexpr auto operator<=(const marked_optional<T_> &opt, const U_ &v) noexcept
		{ return !opt || *opt <= v; }

	template <typename T_, typename U_>
	[[nodiscard]] constexpr auto operator<=(const U_ &v, const marked_optional<T_> &opt) noexcept
		{ return opt && v <= *opt; }

	template <typename T_, typename U_>
	[[nodiscard]] constexpr auto operator>=(const marked_optional<T_> &opt, const U_ &v) noexcept
		{ return opt && *opt >= v; }

	template <typename T_, typename U_>
	[[nodiscard]] constexpr auto operator>=(const U_ &v, const marked_optional<T_> &opt) noexcept
		{ return !opt || v >= *opt; }

	template <typename T_, typename Policy_>
	[[nodiscard]] constexpr std::strong_ordering operator<=>(const marked_optional<T_, Policy_> &opt, nullopt_t) noexcept
		{ return opt.has_value() <=> false; }

	template <typename T_, typename Policy_, std::three_way_comparable_with<T_> U_, typename UPolicy_>
	[[nodiscard]] constexpr std::compare_three_way_result_t<T_, U_> operator<=>(const marked_optional<T_, Policy_> &lhs, const marked_optional<U_, UPolicy_> &rhs) noexcept
		{ return lhs && rhs ? *lhs <=> *rhs : lhs.has_value() <=> rhs.has_value(); }

	template <typename T_, typename Policy_, std::three_way_comparable_with<T_> U_>
		requires (!is_marked_optional<U_>)
	[[nodiscard]] constexpr std::compare_three_way_result_t<T_, U_> operator<=>(const marked_optional<T_, Policy_> &lhs, const U_ &rhs) noexcept
		{ return lhs.has_value() ? *lhs <=> rhs : std::strong_ordering::less; }

	template <typename T_>
	using uninitialized_value = marked_optional<T_, marked_optional_optional_policy<T_>>;
}
