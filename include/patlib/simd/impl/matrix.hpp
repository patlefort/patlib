/*
	PatLib

	Copyright (C) 2022 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <utility>
#include <type_traits>
#include <cstddef>

#include <patlib/type.hpp>
#include <patlib/math.hpp>
#include <patlib/utility.hpp>

#include "base.hpp"
#include "algo.hpp"

namespace patlib::simd
{
	// Some low level matrix functions which can be specialized to the type of vector
	namespace matrix_impl
	{
		inline constexpr auto matrix_transpose = cpo_entry{properties_t{p_nodiscard}};
		inline constexpr auto matrix_adjoint = cpo_entry{properties_t{p_nodiscard}};
		inline constexpr auto matrix_determinant = cpo_entry{properties_t{p_nodiscard}};
		inline constexpr auto matrix_mul = cpo_entry{properties_t{p_nodiscard}};
	}

	namespace impl
	{
		template <typename T_>
		[[nodiscard]] constexpr T_ det2x2(const T_ a, const T_ b, const T_ c, const T_ d) noexcept
		{
			return a * d - b * c;
		}

		template <typename T_>
		[[nodiscard]] constexpr T_ det3x3(const T_ a1, const T_ a2, const T_ a3, const T_ b1, const T_ b2, const T_ b3, const T_ c1, const T_ c2, const T_ c3) noexcept
		{
			return
				a1 * det2x2(b2, b3, c2, c3)
				- b1 * det2x2(a2, a3, c2, c3)
				+ c1 * det2x2(a2, a3, b2, b3);
		}

		template <typename Type1_, typename Impl1_, typename Cat1_>
		[[nodiscard]] constexpr auto tag_invoke(
			tag_t<matrix_impl::matrix_transpose>,
			const std::array<vector<Type1_, size_c<4>,
			Impl1_, categories::geometric_t<Cat1_>>, 4> &v) noexcept
		{
			using VT = typename std::decay_t<decltype(v)>::value_type;

			const auto &ar0 = vector_access(v[0]);
			const auto &ar1 = vector_access(v[1]);
			const auto &ar2 = vector_access(v[2]);
			const auto &ar3 = vector_access(v[3]);

			return std::decay_t<decltype(v)>{
				VT{ar0[0], ar1[0], ar2[0], ar3[0]},
				VT{ar0[1], ar1[1], ar2[1], ar3[1]},
				VT{ar0[2], ar1[2], ar2[2], ar3[2]},
				VT{ar0[3], ar1[3], ar2[3], ar3[3]}
			};
		}

		template <typename Type1_, typename Impl1_, typename Cat1_>
		[[nodiscard]] constexpr auto tag_invoke(
			tag_t<matrix_impl::matrix_adjoint>,
			const std::array<vector<Type1_, size_c<4>,
			Impl1_, categories::geometric_t<Cat1_>>, 4> &v) noexcept
		{
			using VT = typename std::decay_t<decltype(v)>::value_type;

			const auto &v0a = vector_access(v[0]);
			const auto &v1a = vector_access(v[1]);
			const auto &v2a = vector_access(v[2]);
			const auto &v3a = vector_access(v[3]);

			return std::decay_t<decltype(v)>{
				VT{
					det3x3(v1a[1], v1a[2], v1a[3], v2a[1], v2a[2], v2a[3], v3a[1], v3a[2], v3a[3]),
					-det3x3(v0a[1], v0a[2], v0a[3], v2a[1], v2a[2], v2a[3], v3a[1], v3a[2], v3a[3]),
					det3x3(v0a[1], v0a[2], v0a[3], v1a[1], v1a[2], v1a[3], v3a[1], v3a[2], v3a[3]),
					-det3x3(v0a[1], v0a[2], v0a[3], v1a[1], v1a[2], v1a[3], v2a[1], v2a[2], v2a[3])
				},

				VT{
					-det3x3(v1a[0], v1a[2], v1a[3], v2a[0], v2a[2], v2a[3], v3a[0], v3a[2], v3a[3]),
					det3x3(v0a[0], v0a[2], v0a[3], v2a[0], v2a[2], v2a[3], v3a[0], v3a[2], v3a[3]),
					-det3x3(v0a[0], v0a[2], v0a[3], v1a[0], v1a[2], v1a[3], v3a[0], v3a[2], v3a[3]),
					det3x3(v0a[0], v0a[2], v0a[3], v1a[0], v1a[2], v1a[3], v2a[0], v2a[2], v2a[3])
				},

				VT{
					det3x3(v1a[0], v1a[1], v1a[3], v2a[0], v2a[1], v2a[3], v3a[0], v3a[1], v3a[3]),
					-det3x3(v0a[0], v0a[1], v0a[3], v2a[0], v2a[1], v2a[3], v3a[0], v3a[1], v3a[3]),
					det3x3(v0a[0], v0a[1], v0a[3], v1a[0], v1a[1], v1a[3], v3a[0], v3a[1], v3a[3]),
					-det3x3(v0a[0], v0a[1], v0a[3], v1a[0], v1a[1], v1a[3], v2a[0], v2a[1], v2a[3])
				},

				VT{
					-det3x3(v1a[0], v1a[1], v1a[2], v2a[0], v2a[1], v2a[2], v3a[0], v3a[1], v3a[2]),
					det3x3(v0a[0], v0a[1], v0a[2], v2a[0], v2a[1], v2a[2], v3a[0], v3a[1], v3a[2]),
					-det3x3(v0a[0], v0a[1], v0a[2], v1a[0], v1a[1], v1a[2], v3a[0], v3a[1], v3a[2]),
					det3x3(v0a[0], v0a[1], v0a[2], v1a[0], v1a[1], v1a[2], v2a[0], v2a[1], v2a[2])
				}
			};
		}

		template <typename Type1_, typename Impl1_, typename Cat1_>
		[[nodiscard]] constexpr auto tag_invoke(
			tag_t<matrix_impl::matrix_determinant>,
			const std::array<vector<Type1_, size_c<4>,
			Impl1_, categories::geometric_t<Cat1_>>, 4> &v) noexcept
		{
			const auto &v0a = vector_access(v[0]);
			const auto &v1a = vector_access(v[1]);
			const auto &v2a = vector_access(v[2]);
			const auto &v3a = vector_access(v[3]);

			return v0a[0] * det3x3(v1a[1], v1a[2], v1a[3], v2a[1], v2a[2], v2a[3], v3a[1], v3a[2], v3a[3])
         - v1a[0] * det3x3(v0a[1], v0a[2], v0a[3], v2a[1], v2a[2], v2a[3], v3a[1], v3a[2], v3a[3])
         + v2a[0] * det3x3(v0a[1], v0a[2], v0a[3], v1a[1], v1a[2], v1a[3], v3a[1], v3a[2], v3a[3])
         - v3a[0] * det3x3(v0a[1], v0a[2], v0a[3], v1a[1], v1a[2], v1a[3], v2a[1], v2a[2], v2a[3]);
		}

		template <typename Type1_, typename Impl1_, typename Cat1_, typename Type2_, typename Impl2_, typename Cat2_>
		[[nodiscard]] constexpr auto tag_invoke(
			tag_t<matrix_impl::matrix_mul>,
			const std::array<vector<Type1_, size_c<4>,
			Impl1_, categories::geometric_t<Cat1_>>, 4> &m,
			const vector<Type2_, size_c<4>,
			Impl2_, categories::geometric_t<Cat2_>> v) noexcept
		{
			return decltype(m[0] * v){
				v /odot/ m[0],
				v /odot/ m[1],
				v /odot/ m[2],
				v /odot/ m[3]
			};
		}

		template <typename Type1_, typename Impl1_, typename Cat1_, typename Type2_, typename Impl2_, typename Cat2_>
		[[nodiscard]] constexpr auto tag_invoke(
			tag_t<matrix_impl::matrix_mul>,
			const std::array<vector<Type1_, size_c<4>, Impl1_,
			categories::geometric_t<Cat1_>>, 4> &m1,
			const std::array<vector<Type2_, size_c<4>,
			Impl2_, categories::geometric_t<Cat2_>>, 4> &m2) noexcept
		{
			return std::array<decltype(matrix_impl::matrix_mul(m1, m2[0])), 4>{
				matrix_impl::matrix_mul(m1, m2[0]),
				matrix_impl::matrix_mul(m1, m2[1]),
				matrix_impl::matrix_mul(m1, m2[2]),
				matrix_impl::matrix_mul(m1, m2[3])
			};
		}

	} // namespace impl
}
