/*
	PatLib

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <patlib/type.hpp>

namespace patlib::simd
{
	template <szt FromNbValues_, typename Cat_, szt ToNbValues_, szt NbAxis_>
	constinit const bool is_valid_implicit_vector_conversion<FromNbValues_, categories::geometric_t<Cat_, NbAxis_>, ToNbValues_, categories::geometric_t<Cat_, NbAxis_>> = true;
	template <szt FromNbValues_, szt ToNbValues_, szt NbAxis_>
	constinit const bool is_valid_implicit_vector_conversion<FromNbValues_, categories::geometric_t<categories::geometric_dir_t, NbAxis_>, ToNbValues_, categories::geometric_t<void, NbAxis_>> = true;
	template <szt FromNbValues_, szt ToNbValues_, szt NbAxis_>
	constinit const bool is_valid_implicit_vector_conversion<FromNbValues_, categories::geometric_t<void, NbAxis_>, ToNbValues_, categories::geometric_t<categories::geometric_dir_t, NbAxis_>> = true;
	template <szt FromNbValues_, szt ToNbValues_, szt NbAxis_>
	constinit const bool is_valid_implicit_vector_conversion<FromNbValues_, categories::geometric_t<categories::geometric_point_t, NbAxis_>, ToNbValues_, categories::geometric_t<void, NbAxis_>> = true;
	template <szt FromNbValues_, szt ToNbValues_, szt NbAxis_>
	constinit const bool is_valid_implicit_vector_conversion<FromNbValues_, categories::geometric_t<void, NbAxis_>, ToNbValues_, categories::geometric_t<categories::geometric_point_t, NbAxis_>> = true;
} // namespace patlib::simd

namespace patlib::simd::impl
{
	template <typename... VT1_, typename... VT2_>
	void convert(const vector_storage<VT1_...> &from, vector_storage<VT2_...> &into) noexcept
	{
		const auto &arFrom = vector_access(from);

		vector_init_loop(into, [&]<szt I>(size_c<I> i){
			if constexpr(I >= vector_storage<VT1_...>::NbValues)
				return typename vector_storage<VT2_...>::value_type{};
			else
				return arFrom[i];
		}, size_v<vector_storage<VT2_...>::NbValues>);
	}

	template <typename Type1_, typename NB1_, typename StorageT1_, szt NbAxis1_, typename Type2_, typename NB2_, szt NbAxis2_, typename Cat2_, typename StorageT2_>
	void convert(const vector_storage<Type1_, NB1_, categories::geometric_t<categories::geometric_point_t, NbAxis1_>, StorageT1_> &from, vector_storage<Type2_, NB2_, categories::geometric_t<Cat2_, NbAxis2_>, StorageT2_> &into) noexcept
		requires (
			CVoid<Cat2_> ||
				(!CGeometricVectorTagWithW<categories::geometric_t<categories::geometric_point_t, NbAxis1_>, NB1_::value> &&
				CGeometricVectorTagWithW<categories::geometric_t<Cat2_, NbAxis2_>, NB2_::value>))
	{
		const auto &arFrom = vector_access(from);

		constexpr auto W = []() -> Type2_ {
				if constexpr(CVoid<Cat2_>)
					return 1;
				else
					return get_gvec_last_value<Cat2_, Type2_>();
			}();

		vector_init_loop(into, [&]<szt I>(size_c<I> i){
			if constexpr(I == NbAxis2_)
				return W;
			else if constexpr(I < std::min(NbAxis1_, NbAxis2_))
				return arFrom[i];
			else
				return Type2_{};
		}, size_v<NB2_::value>);
	}

	template <typename Type1_, typename NB1_, szt NbAxis1_, typename StorageT1_, typename Type2_, typename NB2_, szt NbAxis2_, typename Cat2_, typename StorageT2_>
	void convert(const vector_storage<Type1_, NB1_, categories::geometric_t<categories::geometric_dir_t, NbAxis1_>, StorageT1_> &from, vector_storage<Type2_, NB2_, categories::geometric_t<Cat2_, NbAxis2_>, StorageT2_> &into) noexcept
		requires (
			CVoid<Cat2_> ||
				(!CGeometricVectorTagWithW<categories::geometric_t<categories::geometric_dir_t, NbAxis1_>, NB1_::value> &&
				CGeometricVectorTagWithW<categories::geometric_t<Cat2_, NbAxis2_>, NB2_::value>))
	{
		const auto &arFrom = vector_access(from);

		constexpr auto W = []() -> Type2_ {
				if constexpr(CVoid<Cat2_>)
					return {};
				else
					return get_gvec_last_value<Cat2_, Type2_>();
			}();

		vector_init_loop(into, [&]<szt I>(size_c<I> i){
			if constexpr(I == NbAxis2_)
				return W;
			else if constexpr(I < std::min(NbAxis1_, NbAxis2_))
				return arFrom[i];
			else
				return Type2_{};
		}, size_v<NB2_::value>);
	}

	template <typename Type1_, typename StorageT1_, typename Type2_, typename StorageT2_>
	void convert(const vector_storage<Type1_, size_c<3>, categories::color_t, StorageT1_> &from, vector_storage<Type2_, size_c<4>, categories::color_t, StorageT2_> &into) noexcept
	{
		const auto &arFrom = vector_access(from);

		vector_init(into, [&](auto&& v){
			index_sequence_loop(size_v<3>, [&](auto i){ v[i] = arFrom[i]; });

			v[size_v<3>] = 1;
		});
	}
} // namespace patlib::simd::impl
