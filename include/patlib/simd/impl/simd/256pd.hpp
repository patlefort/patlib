/*
	PatLib

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "base.hpp"

#ifndef PL_LIB_VECTOR_256_SIMD
	#define PL_LIB_VECTOR_256_SIMD
#endif

#ifndef PL_LIB_VECTOR_256_DOUBLE_SIMD
	#define PL_LIB_VECTOR_256_DOUBLE_SIMD
#endif

#ifndef PL_LIB_VECTOR_DOUBLE_SIMD
	#define PL_LIB_VECTOR_DOUBLE_SIMD
#endif

namespace patlib::simd::impl
{
	[[nodiscard]] inline double mm256_horizontal_add(__m256d v1, __m256d v2) noexcept
	{
		auto sum = _mm256_hadd_pd(v1, v2);
		auto res = _mm_add_pd( _mm256_extractf128_pd(sum, 1), _mm256_castpd256_pd128(sum) );

		return _mm_cvtsd_f64(res);
	}

	[[nodiscard]] inline double mm256_horizontal_sub(__m256d v1, __m256d v2) noexcept
	{
		auto sum = _mm256_hsub_pd(v1, v2);
		auto res = _mm_sub_pd( _mm256_extractf128_pd(sum, 1), _mm256_castpd256_pd128(sum) );

		return _mm_cvtsd_f64(res);
	}

	// http://stackoverflow.com/questions/10833234/4-horizontal-double-precision-sums-in-one-go-with-avx
	[[nodiscard]] inline __m256d mm256_horizontal_add_vector(__m256d a, __m256d b, __m256d c, __m256d d) noexcept
	{
		// {a[0]+a[1], b[0]+b[1], a[2]+a[3], b[2]+b[3]}
		auto sumab = _mm256_hadd_pd(a, b);
		// {c[0]+c[1], d[0]+d[1], c[2]+c[3], d[2]+d[3]}
		auto sumcd = _mm256_hadd_pd(c, d);

		// {a[0]+a[1], b[0]+b[1], c[2]+c[3], d[2]+d[3]}
		auto blend = _mm256_blend_pd(sumab, sumcd, 0b1100);
		// {a[2]+a[3], b[2]+b[3], c[0]+c[1], d[0]+d[1]}
		auto perm = _mm256_permute2f128_pd(sumab, sumcd, 0x21);

		return _mm256_add_pd(perm, blend);
	}

	[[nodiscard]] inline __m256d mm256_horizontal_sub_vector(__m256d a, __m256d b, __m256d c, __m256d d) noexcept
	{
		auto sumab = _mm256_hsub_pd(a, b);
		auto sumcd = _mm256_hsub_pd(c, d);

		auto blend = _mm256_blend_pd(sumab, sumcd, 0b1100);
		auto perm = _mm256_permute2f128_pd(sumab, sumcd, 0x21);

		return _mm256_sub_pd(perm, blend);
	}

	[[nodiscard]] inline __m256d mm256_horizontal_add_vector(__m256d a, __m256d b) noexcept
	{
		// {a[0]+a[1], b[0]+b[1], a[2]+a[3], b[2]+b[3]}
		auto sumab = _mm256_hadd_pd(a, b);

		// {a[0]+a[1], b[0]+b[1], c[2]+c[3], d[2]+d[3]}
		auto blend = _mm256_blend_pd(sumab, sumab, 0b1100);
		// {a[2]+a[3], b[2]+b[3], c[0]+c[1], d[0]+d[1]}
		auto perm = _mm256_permute2f128_pd(sumab, sumab, 0x21);

		return _mm256_add_pd(perm, blend);
	}

	[[nodiscard]] inline __m256d mm256_horizontal_sub_vector(__m256d a, __m256d b) noexcept
	{
		auto sumab = _mm256_hsub_pd(a, b);

		auto blend = _mm256_blend_pd(sumab, sumab, 0b1100);
		auto perm = _mm256_permute2f128_pd(sumab, sumab, 0x21);

		return _mm256_sub_pd(perm, blend);
	}

	struct wrapped__m256d { using type = __m256d; };

	template <typename Category_, typename Derived_>
	class vector_simd_256d_storage :
		public vector_simd_storage_base<double, wrapped__m256d, 4, Category_>,
		public iteratable<double, 4, vector_simd_256d_storage<Category_, Derived_>>
	{
		PL_CRTP_BASE(Derived_)
		using storage_type = vector_simd_storage_base<double, wrapped__m256d, 4, Category_>;

	public:
		static constinit const auto NbValues = storage_type::NbValues;

		using typename storage_type::value_type;
		using typename storage_type::aligned_array_type;
		using storage_type::mVector;
		using storage_type::storage_type;

		vector_simd_256d_storage() = default;
		vector_simd_256d_storage(const value_type v) noexcept : storage_type{ _mm256_set1_pd(v) } {}
		vector_simd_256d_storage(const value_type v, const value_type w) noexcept : storage_type{ _mm256_set_pd(w, v, v, v) } {}
		PL_VECTOR_VARCTOR(vector_simd_256d_storage, _mm256_set_pd)
		explicit vector_simd_256d_storage(const tag::array_init_t, const value_type *v, tag::aligned_init_t = {}) noexcept : storage_type{ _mm256_load_pd(v) } {}
		explicit vector_simd_256d_storage(const tag::array_init_t, const value_type *v, tag::unaligned_init_t) noexcept : storage_type{ _mm256_loadu_pd(v) } {}
		vector_simd_256d_storage(const aligned_array_type &v) noexcept : storage_type{ _mm256_load_pd(v().data()) } {}

		vector_simd_256d_storage(const tag::zero_t) noexcept : storage_type{{}} {}
		vector_simd_256d_storage(const tag::identity_t, const value_type last = 1) noexcept { set_identity(last); }
		vector_simd_256d_storage(const tag::allone_t) noexcept { set_all_one(); }
		vector_simd_256d_storage(const tag::one_t) noexcept { set_one(); }
		vector_simd_256d_storage(const tag::two_t) noexcept { set_two(); }
		vector_simd_256d_storage(const tag::half_t) noexcept { set_half(); }
		vector_simd_256d_storage(const tag::nan_t) noexcept { set_nan(); }

		[[nodiscard]] value_type operator[] (const szt index) const noexcept
		{
			#if BOOST_COMP_MSVC
				return mVector.m256_f64[index];
			#else
				return get_array()()[index];
			#endif
		}

		template <szt index>
		[[nodiscard]] value_type operator[] (size_c<index>) const noexcept
		{
			if constexpr(index > 1)
			{
				auto v = _mm256_shuffle_pd(mVector, mVector, (index - 2) << 2);
				v = _mm256_permute2f128_pd(v, v, 1);
				return _mm_cvtsd_f64(_mm256_castpd256_pd128(v));
			}
			else if constexpr(index == 0)
			{
				return _mm_cvtsd_f64(_mm256_castpd256_pd128(mVector));
			}
			else
			{
				auto v = _mm256_shuffle_pd(mVector, mVector, index);
				return _mm_cvtsd_f64(_mm256_castpd256_pd128(v));
			}
		}

		auto &set_zero() noexcept
		{
			mVector = _mm256_setzero_pd();
			return derived();
		}

		auto &set(const value_type v) noexcept
		{
			mVector = _mm256_set1_pd(v);
			return derived();
		}

		auto &set_all_one() noexcept
		{
			#ifdef __AVX2__
				mVector = _mm256_castsi256_pd(_mm256_cmpeq_epi64(_mm256_castpd_si256(mVector), _mm256_castpd_si256(mVector)));
			#else
				mVector = _mm256_castsi256_pd(_mm256_set1_epi64x(~u64(0)));
			#endif

			return derived();
		}

		auto &set_one() noexcept
		{
			#ifdef __AVX2__
				mVector = _mm256_castsi256_pd( _mm256_srli_epi64(_mm256_slli_epi64(_mm256_cmpeq_epi64(_mm256_castpd_si256(mVector), _mm256_castpd_si256(mVector)), 54), 2) );
			#else
				mVector = _mm256_set1_pd(1.0);
			#endif

			return derived();
		}

		auto &set_half() noexcept
		{
			#ifdef __AVX2__
				mVector = _mm256_castsi256_pd( _mm256_srli_epi64(_mm256_slli_epi64(_mm256_cmpeq_epi64(_mm256_castpd_si256(mVector), _mm256_castpd_si256(mVector)), 55), 2) );
			#else
				mVector = _mm256_set1_pd(0.5);
			#endif

			return derived();
		}

		auto &set_two() noexcept
		{
			#ifdef __AVX2__
				mVector = _mm256_castsi256_pd( _mm256_srli_epi64(_mm256_slli_epi64(_mm256_cmpeq_epi64(_mm256_castpd_si256(mVector), _mm256_castpd_si256(mVector)), 63), 1) );
			#else
				mVector = _mm256_set1_pd(2.0);
			#endif

			return derived();
		}

		auto &set_nan() noexcept
		{
			return (derived() = std::numeric_limits<value_type>::quiet_NaN());
		}

		auto &set_all(const value_type a, const value_type b, const value_type c, const value_type d) noexcept
		{
			mVector = _mm256_set_pd(d, c, b, a);
			return derived();
		}

		auto &set_identity(const value_type last = 1) noexcept
		{
			mVector = _mm256_setzero_pd();
			//XMM_INDEX_SET_FLOAT(mVector, 3, 1.0f);
			//((value_type *)&mVector)[3] = 1.0;
			//_mm256_insert_pd(mVector, _mm256_set1_pd(1.0f), 3);
			set(last, size_v<3>);

			return derived();
		}

		auto &set(const value_type *v) noexcept
		{
			mVector = _mm256_load_pd(v);
			return derived();
		}

		auto &set(const aligned_array_type &v) noexcept
		{
			mVector = _mm256_load_pd(v().data());
			return derived();
		}

		auto &set_unaligned(const value_type *v) noexcept
		{
			mVector = _mm256_loadu_pd(v);
			return derived();
		}

		template <szt index>
		auto &set(const value_type v, size_c<index> = {}) noexcept
		{
			mVector = _mm256_blend_pd(mVector, _mm256_set1_pd(v), 1 << index);

			return derived();
		}

		void get_all(value_type *v) const noexcept
		{
			_mm256_storeu_pd(v, mVector);
		}

		void get_all_aligned(value_type *v) const noexcept
		{
			_mm256_store_pd(v, mVector);
		}

		[[nodiscard]] aligned_array_type get_array() const noexcept
		{
			aligned_array_type a; get_all_aligned(a().data());
			return a;
		}

		auto &set(const value_type v, const int index) noexcept
		{
			//_mm256_insert_pd(mVector, _mm256_set_sd(v), index);
			//((value_type *)&mVector)[index] = v;
			auto va = get_array();
			va()[index] = v;
			set(va);

			return derived();
		}

		void scalar(value_type &dest) noexcept
		{
			_mm_store_sd(&dest, _mm256_extractf128_pd(mVector, 0));
		}

		[[nodiscard]] value_type scalar() const noexcept
		{
			return _mm_cvtsd_f64(_mm256_extractf128_pd(mVector, 0));
		}
	};

	template <typename Category_>
	class vector<double, size_c<4>, types::simd_vector_t<types::simd_256_t>, Category_> :
		public vector_storage<double, size_c<4>, Category_, vector_simd_256d_storage<Category_, vector<double, size_c<4>, types::simd_vector_t<types::simd_256_t>, Category_>>>,
		public vector_base<vector<double, size_c<4>, types::simd_vector_t<types::simd_256_t>, Category_>>,
		public common_op<vector<double, size_c<4>, types::simd_vector_t<types::simd_256_t>, Category_>>
	{
		using common_op_type = common_op<vector<double, size_c<4>, types::simd_vector_t<types::simd_256_t>, Category_>>;
		using storage_type = vector_storage<double, size_c<4>, Category_, vector_simd_256d_storage<Category_, vector<double, size_c<4>, types::simd_vector_t<types::simd_256_t>, Category_>>>;

	public:
		using typename storage_type::value_type;
		using typename storage_type::category;
		using storage_type::mVector;
		using storage_type::storage_type;

		using impl_type = types::simd_vector_t<types::simd_256_t>;

		static constinit const auto NbValues = storage_type::NbValues;
		static constinit const bool HardwareImpl = true;


		[[nodiscard]] vector andnot(vector v) const noexcept
		{
			return _mm256_andnot_pd(mVector, v.mVector);
		}

		[[nodiscard]] vector next_after() const noexcept
		{
			return _mm256_castsi256_pd(simd::AvxMathfun::mf_mm256_add_epi64(_mm256_castpd_si256(mVector), _mm256_set1_epi64x(1)));
		}

		[[nodiscard]] vector abs() const noexcept
		{
			#ifdef __AVX512__
				return _mm256_and_pd(mVector, _mm256_castsi256_pd(_mm256_srli_epi64(_mm256_set1_epi64x(-1), 1)));
			#else
				return _mm256_and_pd(mVector, _mm256_castsi256_pd(_mm256_set1_epi64x(0x7FFFFFFFFFFFFFFF))); // _mm256_max_pd(mVector, _mm256_sub_pd(_mm256_setzero_pd(), mVector));
			#endif
		}

		[[nodiscard]] vector pow(const value_type power) const noexcept
		{
			return vector_transform(*this, [&](auto&&, auto&& v){ return plpow(v, power); });
		}

		[[nodiscard]] vector log() const noexcept
		{
			return vector_transform(*this, [](auto&&, auto&& v){ return pllog(v); });
		}

		[[nodiscard]] vector log2() const noexcept
		{
			return vector_transform(*this, [](auto&&, auto&& v){ return pllog2(v); });
		}

		[[nodiscard]] vector log2approx() const noexcept
		{
			return vector_transform(*this, [](auto&&, auto&& v){ return patlib::log2approx(v); });
		}

		[[nodiscard]] vector log10() const noexcept
		{
			return vector_transform(*this, [](auto&&, auto&& v){ return pllog10(v); });
		}

		[[nodiscard]] vector exp() const noexcept
		{
			return vector_transform(*this, [](auto&&, auto&& v){ return plexp(v); });
		}

		[[nodiscard]] vector exp2() const noexcept
		{
			return vector_transform(*this, [](auto&&, auto&& v){ return plexp2(v); });
		}

		[[nodiscard]] vector exp2approx() const noexcept
		{
			return vector_transform(*this, [](auto&&, auto&& v){ return patlib::exp2approx(v); });
		}

		[[nodiscard]] vector sin() const noexcept
		{
			return vector_transform(*this, [](auto&&, auto&& v){ return plsin(v); });
		}

		[[nodiscard]] vector cos() const noexcept
		{
			return vector_transform(*this, [](auto&&, auto&& v){ return plcos(v); });
		}

		[[nodiscard]] auto sincos() const noexcept
		{
			return sincos_res{sin(), cos()};
		}

		[[nodiscard]] int cmp_scalar_ge(vector v) const noexcept
		{
			return _mm_comige_sd(_mm256_extractf128_pd(mVector, 0), _mm256_extractf128_pd(v.mVector, 0));
		}

		[[nodiscard]] int cmp_scalar_gt(vector v) const noexcept
		{
			return _mm_comigt_sd(_mm256_extractf128_pd(mVector, 0), _mm256_extractf128_pd(v.mVector, 0));
		}

		[[nodiscard]] int cmp_scalar_eq(vector v) const noexcept
		{
			return _mm_comieq_sd(_mm256_extractf128_pd(mVector, 0), _mm256_extractf128_pd(v.mVector, 0));
		}

		[[nodiscard]] int cmp_scalar_le(vector v) const noexcept
		{
			return _mm_comile_sd(_mm256_extractf128_pd(mVector, 0), _mm256_extractf128_pd(v.mVector, 0));
		}

		[[nodiscard]] int cmp_scalar_lt(vector v) const noexcept
		{
			return _mm_comilt_sd(_mm256_extractf128_pd(mVector, 0), _mm256_extractf128_pd(v.mVector, 0));
		}


		[[nodiscard]] vector min(vector v) const noexcept
		{
			return _mm256_min_pd(mVector, v.mVector);
		}

		[[nodiscard]] vector max(vector v) const noexcept
		{
			return _mm256_max_pd(mVector, v.mVector);
		}

		[[nodiscard]] vector min_scalar(vector v) const noexcept
		{
			return _mm256_insertf128_pd(mVector, _mm_min_sd(_mm256_extractf128_pd(mVector, 0), _mm256_extractf128_pd(v.mVector, 0)), 0);
		}

		[[nodiscard]] vector max_scalar(vector v) const noexcept
		{
			return _mm256_insertf128_pd(mVector, _mm_max_sd(_mm256_extractf128_pd(mVector, 0), _mm256_extractf128_pd(v.mVector, 0)), 0);
		}

		[[nodiscard]] value_type min3() const noexcept
		{
			auto vals = this->get_array();

			return plmin( {vals()[0], vals()[1], vals()[2]} );
		}

		[[nodiscard]] value_type hmin() const noexcept
		{
			auto vals = this->get_array();

			return plmin( {vals()[0], vals()[1], vals()[2], vals()[3]} );
		}

		[[nodiscard]] value_type max3() const noexcept
		{
			auto vals = this->get_array();

			return plmax( {vals()[0], vals()[1], vals()[2]} );
		}

		[[nodiscard]] value_type hmax() const noexcept
		{
			auto vals = this->get_array();

			return plmax( {vals()[0], vals()[1], vals()[2], vals()[3]} );
		}

		[[nodiscard]] vector floor() const noexcept
		{
			return _mm256_floor_pd(mVector);
		}

		[[nodiscard]] vector ceil() const noexcept
		{
			return _mm256_ceil_pd(mVector);
		}

		[[nodiscard]] vector round() const noexcept
		{
			return _mm256_round_pd(mVector, _MM_FROUND_TO_NEAREST_INT | _MM_FROUND_NO_EXC);
		}

		[[nodiscard]] vector sqrt() const noexcept
		{
			return _mm256_sqrt_pd(mVector);
		}

		[[nodiscard]] vector sqrtapprox() const noexcept
		{
			return sqrt();
		}

		[[nodiscard]] vector sqrt_scalar() const noexcept
		{
			return _mm256_blend_pd(sqrt().mVector, mVector, 0b1110);
		}

		[[nodiscard]] vector sqrtapprox_scalar() const noexcept
		{
			return sqrt_scalar();
		}

		[[nodiscard]] vector recsqrt() const noexcept
		{
			return sqrt().reciprocal();
		}

		[[nodiscard]] vector recsqrtapprox() const noexcept
		{
			return recsqrt();
		}

		[[nodiscard]] vector hmul() const noexcept
		{
			const auto r = *this * shuffle<1, 0, 3, 2>();
			return r * vector{_mm256_permute2f128_pd(r.mVector, r.mVector, 0b1)};
		}

		[[nodiscard]] vector hadd() const noexcept
		{
			return mm256_horizontal_add(mVector, mVector);
		}

		[[nodiscard]] vector hsub() const noexcept
		{
			return mm256_horizontal_sub(mVector, mVector);
		}

		[[nodiscard]] vector inverse_sign() const noexcept
		{
			//#ifdef __AVX512__
				return _mm256_xor_pd(mVector, _mm256_castsi256_pd(_mm256_set1_epi64x( (u64)1 << 63 )));
			/*#else
				return _mm256_sub_pd(_mm256_setzero_pd(), mVector);
			#endif*/
		}

		[[nodiscard]] vector reciprocal() const noexcept
		{
			return _mm256_div_pd(_mm256_set1_pd(1.0), mVector);
		}

		[[nodiscard]] vector reciprocalapprox() const noexcept
		{
			return reciprocal();
		}

		[[nodiscard]] vector movehl(vector v) const noexcept
		{
			return _mm256_unpackhi_pd(mVector, v.mVector);
		}

		[[nodiscard]] vector movelh(vector v) const noexcept
		{
			return _mm256_unpacklo_pd(mVector, v.mVector);
		}

		template <int m0, int m1, int m2, int m3>
		[[nodiscard]] vector shuffle(vector v) const noexcept
		{
			return _mm256_shuffle_pd(mVector, v.mVector, m0 & (m1<<1) & (m2<<2) & (m3<<3));
		}

		template <int m0, int m1, int m2, int m3>
		[[nodiscard]] vector shuffle() const noexcept
		{
			return _mm256_shuffle_pd(mVector, mVector, m0 & (m1<<1) & (m2<<2) & (m3<<3));
		}

		[[nodiscard]] int move_mask() const noexcept
		{
			return _mm256_movemask_pd(mVector);
		}

		[[nodiscard]] value_type dot(vector v) const noexcept
		{
			__m256d tmp1;

			tmp1 = _mm256_mul_pd(mVector, v.mVector);
			tmp1 = mm256_horizontal_add_vector(tmp1, tmp1);

			return _mm_cvtsd_f64(_mm256_castpd256_pd128(tmp1));
		}

		template <szt mask = 0xFF>
		[[nodiscard]] vector dot_m(masked<mask, vector> v) const noexcept
		{
			static constinit const auto maskInt = static_cast<int>(mask);

			__m256d tmp1;

			if constexpr(mask == 0xFF)
			{
				tmp1 = _mm256_mul_pd(mVector, v.v.mVector);
				tmp1 = mm256_horizontal_add_vector(tmp1, tmp1);
			}
			else
			{
				tmp1 = _mm256_mul_pd(mVector, v.v.mVector);
				tmp1 = _mm256_and_pd(tmp1, _mm256_castsi256_pd(_mm256_set_epi64x( constants::all_ones<long long int> * ((maskInt>>7)&0x1), constants::all_ones<long long int> * ((maskInt>>6)&0x1), constants::all_ones<long long int> * ((maskInt>>5)&0x1), constants::all_ones<long long int> * ((maskInt>>4)&0x1) )));
				tmp1 = mm256_horizontal_add_vector(tmp1, tmp1);
				tmp1 = _mm256_and_pd(tmp1, _mm256_castsi256_pd(_mm256_set_epi64x( constants::all_ones<long long int> * ((maskInt>>3)&0x1), constants::all_ones<long long int> * ((maskInt>>2)&0x1), constants::all_ones<long long int> * ((maskInt>>1)&0x1), constants::all_ones<long long int> * (maskInt&0x1) )));
			}

			return tmp1;
		}

		template <szt mask = 0xFF>
		[[nodiscard]] value_type dot(masked<mask, vector> v) const noexcept
		{
			return dot_m(masked{size_v<1 | (mask << 4)>, v.v}).scalar();
		}

		[[nodiscard]] value_type dot() const noexcept
		{
			__m256d tmp1;

			tmp1 = _mm256_mul_pd(mVector, mVector);
			tmp1 = mm256_horizontal_add_vector(tmp1, tmp1);

			return _mm_cvtsd_f64(_mm256_castpd256_pd128(tmp1));
		}

		using common_op_type::operator+=;
		using common_op_type::operator-=;
		using common_op_type::operator/=;
		using common_op_type::operator*=;
		using common_op_type::operator%=;
		using common_op_type::operator|=;
		using common_op_type::operator&=;
		using common_op_type::operator^=;
		using common_op_type::operator>>=;
		using common_op_type::operator<<=;

		[[nodiscard]] vector operator+ (vector v) const noexcept
		{
			return _mm256_add_pd(mVector, v.mVector);
		}

		[[nodiscard]] vector operator- (vector v) const noexcept
		{
			return _mm256_sub_pd(mVector, v.mVector);
		}

		[[nodiscard]] vector operator* (vector v) const noexcept
		{
			return _mm256_mul_pd(mVector, v.mVector);
		}

		[[nodiscard]] vector operator/ (vector v) const noexcept
		{
			return _mm256_div_pd(mVector, v.mVector);
		}

		[[nodiscard]] vector operator% (vector v) const noexcept
		{
			#ifdef __AVX512__
				__m256d c = _mm256_div_pd(mVector, v.mVector);
				__m256i i = _mm256_cvttpd_epi64(c);
				__m256d cTrunc = _mm256_cvtepi64_pd(i);
				__m256d base = _mm256_mul_pd(cTrunc, v.mVector);
				return _mm256_sub_pd(mVector, base);
			#else
				__m256d cTrunc = _mm256_floor_pd(_mm256_div_pd(mVector, v.mVector));
				__m256d base = _mm256_mul_pd(cTrunc, v.mVector);
				return _mm256_sub_pd(mVector, base);
			#endif
		}

		[[nodiscard]] vector operator+ (const value_type v) const noexcept
		{
			return _mm256_add_pd(mVector, _mm256_set1_pd(v));
		}

		[[nodiscard]] vector operator- (const value_type v) const noexcept
		{
			return _mm256_sub_pd(mVector, _mm256_set1_pd(v));
		}

		[[nodiscard]] vector operator* (const value_type v) const noexcept
		{
			return _mm256_mul_pd(mVector, _mm256_set1_pd(v));
		}

		[[nodiscard]] vector operator/ (const value_type v) const noexcept
		{
			return _mm256_div_pd(mVector, _mm256_set1_pd(v));
		}

		[[nodiscard]] vector operator% (const value_type v) const noexcept
		{
			__m256d vv = _mm256_set1_pd(v);

			#ifdef __AVX512__
				__m256d c = _mm256_div_pd(mVector, vv);
				__m256i i = _mm256_cvttpd_epi64(c);
				__m256d cTrunc = _mm256_cvtepi64_pd(i);
				__m256d base = _mm256_mul_pd(cTrunc, vv);
				return _mm256_sub_pd(mVector, base);
			#else
				__m256d cTrunc = _mm256_floor_pd(_mm256_div_pd(mVector, vv));
				__m256d base = _mm256_mul_pd(cTrunc, vv);
				return _mm256_sub_pd(mVector, base);
			#endif
		}

		[[nodiscard]] vector operator== (vector v) const noexcept
		{
			return _mm256_cmp_pd(mVector, v.mVector, 0);
		}

		[[nodiscard]] vector operator!= (vector v) const noexcept
		{
			return _mm256_cmp_pd(mVector, v.mVector, 4);
		}

		[[nodiscard]] vector operator< (vector v) const noexcept
		{
			return _mm256_cmp_pd(mVector, v.mVector, 1);
		}

		[[nodiscard]] vector operator<= (vector v) const noexcept
		{
			return _mm256_cmp_pd(mVector, v.mVector, 2);
		}

		[[nodiscard]] vector operator> (vector v) const noexcept
		{
			return _mm256_cmp_pd(mVector, v.mVector, 6);
		}

		[[nodiscard]] vector operator>= (vector v) const noexcept
		{
			return _mm256_cmp_pd(mVector, v.mVector, 5);
		}

		[[nodiscard]] vector operator== (const value_type v) const noexcept
		{
			return _mm256_cmp_pd(mVector, _mm256_set1_pd(v), 0);
		}

		[[nodiscard]] vector operator!= (const value_type v) const noexcept
		{
			return _mm256_cmp_pd(mVector, _mm256_set1_pd(v), 4);
		}

		[[nodiscard]] vector operator< (const value_type v) const noexcept
		{
			return _mm256_cmp_pd(mVector, _mm256_set1_pd(v), 1);
		}

		[[nodiscard]] vector operator<= (const value_type v) const noexcept
		{
			return _mm256_cmp_pd(mVector, _mm256_set1_pd(v), 2);
		}

		[[nodiscard]] vector operator> (const value_type v) const noexcept
		{
			return _mm256_cmp_pd(mVector, _mm256_set1_pd(v), 6);
		}

		[[nodiscard]] vector operator>= (const value_type v) const noexcept
		{
			return _mm256_cmp_pd(mVector, _mm256_set1_pd(v), 5);
		}

		[[nodiscard]] vector operator& (vector v) const noexcept
		{
			return _mm256_and_pd(mVector, v.mVector);
		}

		[[nodiscard]] vector operator| (vector v) const noexcept
		{
			return _mm256_or_pd(mVector, v.mVector);
		}

		[[nodiscard]] vector operator^ (vector v) const noexcept
		{
			return _mm256_xor_pd(mVector, v.mVector);
		}

		[[nodiscard]] vector operator& (const i64 v) const noexcept
		{
			return _mm256_and_pd(mVector, _mm256_castsi256_pd(_mm256_set1_epi64x(v)));
		}

		[[nodiscard]] vector operator| (const i64 v) const noexcept
		{
			return _mm256_or_pd(mVector, _mm256_castsi256_pd(_mm256_set1_epi64x(v)));
		}

		[[nodiscard]] vector operator^ (const i64 v) const noexcept
		{
			return _mm256_xor_pd(mVector, _mm256_castsi256_pd(_mm256_set1_epi64x(v)));
		}

		[[nodiscard]] vector operator>> (const int v) const noexcept
		{
			return _mm256_castsi256_pd(simd::AvxMathfun::mf_mm256_srli_epi64(_mm256_castpd_si256(mVector), v));
		}

		[[nodiscard]] vector operator<< (const int v) const noexcept
		{
			return _mm256_castsi256_pd(simd::AvxMathfun::mf_mm256_slli_epi64(_mm256_castpd_si256(mVector), v));
		}

		[[nodiscard]] vector operator>> (vector v) const noexcept
		{
			return _mm256_castsi256_pd(_mm256_srlv_epi64(_mm256_castpd_si256(mVector), _mm256_castpd_si256(v.mVector)));
		}

		[[nodiscard]] vector operator<< (vector v) const noexcept
		{
			return _mm256_castsi256_pd(_mm256_sllv_epi64(_mm256_castpd_si256(mVector), _mm256_castpd_si256(v.mVector)));
		}


		[[nodiscard]] vector operator! () const noexcept
		{
			return _mm256_cmp_pd(mVector, _mm256_setzero_pd(), 0);
		}

		[[nodiscard]] vector operator- () const noexcept
		{
			return inverse_sign();
		}

		[[nodiscard]] vector operator+ () const noexcept
		{
			return *this;
		}

		[[nodiscard]] vector is_nan() const noexcept
		{
			return _mm256_cmp_pd(mVector, mVector, 3);
		}

		[[nodiscard]] vector is_infinite() const noexcept
		{
			const __m256d INF = _mm256_set1_pd(std::numeric_limits<value_type>::infinity());

			return _mm256_cmp_pd(abs().mVector, INF, _CMP_EQ_OQ);
		}

		[[nodiscard]] vector is_normal() const noexcept
			{ return ~(is_infinite() | is_nan() | (*this == vector{})); }

		[[nodiscard]] bool is_all_zero() const noexcept
		{
			return (!*this).move_mask() == 0xF;
		}

		vector &operator++ () noexcept
			{ _mm256_add_pd(mVector, _mm256_set1_pd(1)); return *this; }

		vector &operator-- () noexcept
			{ _mm256_sub_pd(mVector, _mm256_set1_pd(1)); return *this; }

		vector operator++ (int) noexcept
			{ auto cp = *this; ++(*this); return cp; }

		vector operator-- (int) noexcept
			{ auto cp = *this; --(*this); return cp; }

		[[nodiscard]] vector operator~ () const noexcept
		{
			return _mm256_andnot_pd(mVector, _mm256_cmp_pd(mVector, mVector, 0));
		}

		[[nodiscard]] vector blendv(vector v, vector mask) const noexcept
			{ return _mm256_blendv_pd(mVector, v.mVector, mask.mVector); }

		template <int mask>
		[[nodiscard]] vector blend(vector v) const noexcept
			{ return _mm256_blend_pd(mVector, v.mVector, mask); }

		[[nodiscard]] vector unpacklo(vector v) const noexcept
			{ return _mm256_unpacklo_pd(mVector, v.mVector); }

		[[nodiscard]] vector unpackhi(vector v) const noexcept
			{ return _mm256_unpackhi_pd(mVector, v.mVector); }

		[[nodiscard]] operator __m256d() const noexcept
			{ return mVector; }

		[[nodiscard]] auto mask() const noexcept
			{ return std::bitset<NbValues>{static_cast<std::make_unsigned_t<int>>(move_mask())}; }

		[[nodiscard]] explicit operator bool() const noexcept
			{ return mask().all(); }

	};


	template <typename Category_, typename Derived_>
	class vector_simd_256d_4_storage :
		public other_vector_storage<double, size_c<4>, types::simd_vector_t<types::simd_256_t>, void, Category_, Derived_>
	{
		PL_CRTP_BASE(Derived_)
		using storage_type = other_vector_storage<double, size_c<4>, types::simd_vector_t<types::simd_256_t>, void, Category_, Derived_>;
		using typename storage_type::storage_vector_type;

	public:
		using typename storage_type::value_type;
		using storage_type::v1;
		using storage_type::storage_type;

		vector_simd_256d_4_storage() = default;
		vector_simd_256d_4_storage(const __m256d v) noexcept : storage_type{v} {}
		vector_simd_256d_4_storage(const value_type v) noexcept : storage_type{storage_vector_type{v}} {}
		vector_simd_256d_4_storage(const value_type v, const value_type w) noexcept : storage_type{storage_vector_type{v, w}} {}
		vector_simd_256d_4_storage(const value_type x, const value_type y, const value_type z) noexcept : storage_type{storage_vector_type{x, y, z, 0}} {}
		vector_simd_256d_4_storage(const value_type x, const value_type y, const value_type z, const value_type w) noexcept : storage_type{storage_vector_type{x, y, z, w}} {}
	};

	template <typename Category_>
	class vector<double, size_c<4>, types::simd_vector_t<types::simd_256_t>, categories::geometric_t<Category_>> :
		public vector_storage<double, size_c<4>, categories::geometric_t<Category_, 3>, vector_simd_256d_4_storage<categories::geometric_t<Category_, 3>, vector<double, size_c<4>, types::simd_vector_t<types::simd_256_t>, categories::geometric_t<Category_>>>>,
		public vector_base<vector<double, size_c<4>, types::simd_vector_t<types::simd_256_t>, categories::geometric_t<Category_>>>,
		public common_op<vector<double, size_c<4>, types::simd_vector_t<types::simd_256_t>, categories::geometric_t<Category_>>>
	{
		using common_op_type = common_op<vector<double, size_c<4>, types::simd_vector_t<types::simd_256_t>, categories::geometric_t<Category_>>>;
		using storage_type = vector_storage<double, size_c<4>, categories::geometric_t<Category_, 3>, vector_simd_256d_4_storage<categories::geometric_t<Category_, 3>, vector<double, size_c<4>, types::simd_vector_t<types::simd_256_t>, categories::geometric_t<Category_>>>>;

	public:

		using impl_type = types::simd_vector_t<types::simd_256_t>;

		static constexpr auto LastValue = get_gvec_last_value<Category_, double>();
		static constinit const auto NbValues = storage_type::NbValues;
		static constinit const bool HardwareImpl = true;

		using typename storage_type::value_type;
		using typename storage_type::category;
		using storage_type::v1;
		using storage_type::storage_type;

		vector() = default;
		vector(const value_type v) noexcept : storage_type{v} {}
		vector(const value_type v) noexcept requires (!CVoid<Category_>) : storage_type{v, LastValue} {}
		vector(const value_type x, const value_type y, const value_type z) noexcept : storage_type{x, y, z, LastValue} {}
		vector(const tag::identity_t) noexcept { set_identity(); }

		template <typename OTag_>
		explicit(!is_valid_implicit_vector_conversion<4, categories::geometric_t<OTag_, 3>, 4, category>) vector(const vector<double, size_c<4>, types::simd_vector_t<types::simd_256_t>, categories::geometric_t<OTag_, 3>> &o) noexcept :
			storage_type{o.v1} {}


		vector &set_identity() noexcept
		{
			v1.set_identity(LastValue);
			return *this;
		}

		[[nodiscard]] vector abs() const noexcept
		{
			return v1.abs();
		}

		[[nodiscard]] vector andnot(vector v) const noexcept
		{
			return vector{v1.andnot(v.v1)};
		}

		[[nodiscard]] vector next_after() const noexcept
		{
			return vector{v1.next_after()};
		}

		[[nodiscard]] value_type minimal_safe_increment() const noexcept
		{
			const auto vabs = abs();
			return (vabs.next_after() - vabs).max3();
		}

		[[nodiscard]] vector pow(const value_type power) const noexcept
		{
			return vector{v1.pow(power)};
		}

		[[nodiscard]] vector log() const noexcept
		{
			return vector{v1.log()};
		}

		[[nodiscard]] vector log2() const noexcept
		{
			return vector{v1.log2()};
		}

		[[nodiscard]] vector log2approx() const noexcept
		{
			return vector{v1.log2approx()};
		}

		[[nodiscard]] vector log10() const noexcept
		{
			return vector{v1.log10()};
		}

		[[nodiscard]] vector exp() const noexcept
		{
			return vector{v1.exp()};
		}

		[[nodiscard]] vector exp2() const noexcept
		{
			return vector{v1.exp2()};
		}

		[[nodiscard]] vector exp2approx() const noexcept
		{
			return vector{v1.exp2approx()};
		}

		[[nodiscard]] vector sin() const noexcept
		{
			return vector{v1.sin()};
		}

		[[nodiscard]] vector cos() const noexcept
		{
			return vector{v1.cos()};
		}

		[[nodiscard]] auto sincos() const noexcept
		{
			return sincos_res{sin(), cos()};
		}

		[[nodiscard]] vector sqrt() const noexcept
		{
			return vector{v1.sqrt()};
		}

		[[nodiscard]] vector sqrtapprox() const noexcept
		{
			return vector{v1.sqrtapprox()};
		}

		[[nodiscard]] vector sqrt_scalar() const noexcept
		{
			return vector{v1.sqrt_scalar()};
		}

		[[nodiscard]] vector sqrtapprox_scalar() const noexcept
		{
			return vector{v1.sqrtapprox_scalar()};
		}

		[[nodiscard]] vector recsqrt() const noexcept
		{
			return vector{v1.recsqrt()};
		}

		[[nodiscard]] vector recsqrtapprox() const noexcept
		{
			return vector{v1.recsqrtapprox()};
		}

		void reflect(vector fromDir) noexcept
		{
			__m256d tmp1;

			tmp1 = _mm256_mul_pd(v1.mVector, fromDir.v1.mVector);
			tmp1 = mm256_horizontal_add_vector(tmp1, tmp1);

			//v1.mVector = _mm_mul_ps(_mm_sub_ps(v1.mVector, fromDir.v1.mVector), _mm_add_ps(tmp1, tmp1));
			v1.mVector = _mm256_sub_pd(v1.mVector, _mm256_mul_pd(fromDir.v1.mVector, _mm256_add_pd(tmp1, tmp1)));
			//v1 = v1 - fromDir * (dir.dot(fromDir) * Real(2.0));
		}

		[[nodiscard]] vector rotate() const noexcept
		{
			// a b c d -> b c d a
			const auto ar1 = this->get_array();

			return {ar1()[1], ar1()[2], ar1()[3], ar1()[0]};
		}

		template <int m0, int m1, int m2, int m3>
		[[nodiscard]] vector shuffle(vector v) const noexcept
		{
			const auto ar1 = this->get_array();
			const auto ar2 = v.get_array();

			return {ar1()[m0], ar1()[m1], ar2()[m2], ar2()[m3]};
		}

		template <int m0, int m1, int m2, int m3>
		[[nodiscard]] vector shuffle() const noexcept
		{
			const auto ar1 = this->get_array();

			return {ar1()[m0], ar1()[m1], ar1()[m2], ar1()[m3]};
		}

		[[nodiscard]] vector movehl(vector v) const noexcept
		{
			__m128d un1[2], un2[2];
			std::memcpy(&un1, &v1.mVector, sizeof(v1.mVector));
			std::memcpy(&un2, &v.v1.mVector, sizeof(v.v1.mVector));

			return _mm256_set_m128d(un1[1], un2[1]);
		}

		[[nodiscard]] vector movelh(vector v) const noexcept
		{
			__m128d un1[2], un2[2];
			std::memcpy(&un1, &v1.mVector, sizeof(v1.mVector));
			std::memcpy(&un2, &v.v1.mVector, sizeof(v.v1.mVector));

			return _mm256_set_m128d(un1[0], un2[0]);
		}

		[[nodiscard]] int move_mask() const noexcept
		{
			return v1.move_mask();
		}

		[[nodiscard]] vector normalize() const noexcept
		{
			return *this / dot_m(masked{size_v<0xFF>, *this}).sqrt();
		}

		[[nodiscard]] vector normalize_approx() const noexcept
		{
			return *this * dot_m(masked{size_v<0xFF>, *this}).recsqrtapprox();
		}

		[[nodiscard]] vector normalize(const value_type length) const noexcept
		{
			return vector{v1 / length};
		}

		[[nodiscard]] vector normalize_point() const noexcept
			requires (CPointVectorTag<category> || CNeutralGeometricVectorTag<category>)
		{
			return *this / this->template shuffle<3, 3, 3, 3>();
		}

		[[nodiscard]] auto as_point() const noexcept
		{
			using T = rebind_to_point<vector>;
			auto r = static_cast<T>(*this);

			r.set(1, size_v<3>);

			return r;
		}

		[[nodiscard]] auto as_dir() const noexcept
		{
			using T = rebind_to_dir<vector>;
			auto r = static_cast<T>(*this);

			r.set(0, size_v<3>);

			return r;
		}

		// http://fastcpp.blogspot.ca/2011/04/vector-cross-product-using-sse-code.html
		[[nodiscard]] vector cross(vector v) const noexcept
		{
			return (*this * v.template shuffle<1, 2, 0, 3>() - this->template shuffle<1, 2, 0, 3>() * v).template shuffle<1, 2, 0, 3>();
		}

		[[nodiscard]] value_type dot(vector v) const noexcept
		{
			return v1.dot(v.v1);
		}

		[[nodiscard]] value_type dot() const noexcept
		{
			return v1.dot();
		}

		template <szt mask = 0xFF>
		[[nodiscard]] value_type dot(masked<mask, vector> v) const noexcept
		{
			return v1.dot(masked{size_v<mask>, v.v.v1});
		}

		template <szt mask = 0xFF>
		[[nodiscard]] vector dot_m(masked<mask, vector> v) const noexcept
		{
			return v1.dot_m(masked{size_v<mask>, v.v.v1});
		}

		[[nodiscard]] value_type length() const noexcept
		{
			auto tmp1 = _mm256_mul_pd(v1.mVector, v1.mVector);
			auto l = mm256_horizontal_add(tmp1, tmp1);

			return plsqrt(l);
		}

		[[nodiscard]] value_type length_approx() const noexcept
		{
			auto tmp1 = _mm256_mul_pd(v1.mVector, v1.mVector);
			auto l = mm256_horizontal_add(tmp1, tmp1);

			return patlib::sqrtapprox(l);
		}

		[[nodiscard]] vector min(vector v) const noexcept
		{
			return vector{v1.min(v.v1)};
		}

		[[nodiscard]] value_type hmin() const noexcept
		{
			return v1.hmin();
		}

		[[nodiscard]] value_type min3() const noexcept
		{
			return v1.min3();
		}

		[[nodiscard]] vector min_scalar(vector v) const noexcept
		{
			return vector{v1.min_scalar(v.v1)};
		}

		[[nodiscard]] vector max_scalar(vector v) const noexcept
		{
			return vector{v1.max_scalar(v.v1)};
		}

		[[nodiscard]] vector max(vector v) const noexcept
		{
			return vector{v1.max(v.v1)};
		}

		[[nodiscard]] value_type hmax() const noexcept
		{
			return v1.hmax();
		}

		[[nodiscard]] value_type max3() const noexcept
		{
			return v1.max3();
		}

		[[nodiscard]] szt max3_index() const noexcept
		{
			auto v = v1.get_array();

			value_type maxVal = v()[0];
			szt maxIndex = 0;

			if(v()[1] > maxVal)
			{
				maxIndex = 1;
				maxVal = v()[1];
			}

			if(v()[2] > maxVal)
				maxIndex = 2;

			return maxIndex;
		}

		[[nodiscard]] szt min3_index() const noexcept
		{
			auto v = v1.get_array();

			value_type minVal = v()[0];
			szt minIndex = 0;

			if(v()[1] < minVal)
			{
				minIndex = 1;
				minVal = v()[1];
			}

			if(v()[2] < minVal)
				minIndex = 2;

			return minIndex;
		}

		[[nodiscard]] szt abs_min3_index() const noexcept
		{
			auto v = v1.get_array();

			value_type minVal = plabs(v()[0]), cVal = plabs(v()[1]);
			szt minIndex = 0;

			if(cVal < minVal)
			{
				minIndex = 1;
				minVal = cVal;
			}

			if(plabs(v()[2]) < minVal)
				minIndex = 2;

			return minIndex;
		}

		[[nodiscard]] szt abs_max3_index() const noexcept
		{
			auto v = v1.get_array();

			value_type maxVal = plabs(v()[0]), cVal = plabs(v()[1]);
			szt maxIndex = 0;

			if(cVal > maxVal)
			{
				maxIndex = 1;
				maxVal = cVal;
			}

			if(plabs(v()[2]) > maxVal)
				maxIndex = 2;

			return maxIndex;
		}

		[[nodiscard]] vector floor() const noexcept
		{
			return vector{v1.floor()};
		}

		[[nodiscard]] vector ceil() const noexcept
		{
			return vector{v1.ceil()};
		}

		[[nodiscard]] vector round() const noexcept
		{
			return vector{v1.round()};
		}

		[[nodiscard]] vector inverse_sign() const noexcept
		{
			return vector{v1.inverse_sign()};
		}

		[[nodiscard]] vector reciprocal() const noexcept
		{
			return vector{v1.reciprocal()};
		}

		[[nodiscard]] vector reciprocalapprox() const noexcept
		{
			return vector{v1.reciprocalapprox()};
		}

		[[nodiscard]] vector hmul() const noexcept
		{
			return vector{v1.hmul()};
		}

		[[nodiscard]] vector hadd() const noexcept
		{
			return vector{v1.hadd()};
		}

		[[nodiscard]] vector hsub() const noexcept
		{
			return vector{v1.hsub()};
		}

		[[nodiscard]] vector find_orthogonal() const noexcept
		{
			auto ov = this->get_array();
			const auto ovb = ov;

			const auto maxIndex = abs_max3_index(), nextIndex = maxIndex == 2 ? 0 : maxIndex + 1;

			ov()[nextIndex] = ovb()[maxIndex];
			ov()[maxIndex] = -ovb()[nextIndex];

			const auto orthogonal = cross(vector{ov});

			//orthogonal.set(v1[3], 3);

			return orthogonal;
		}

		[[nodiscard]] int cmp_scalar_ge(vector v) const noexcept
		{
			return v1.cmp_scalar_ge(v.v1);
		}

		[[nodiscard]] int cmp_scalar_gt(vector v) const noexcept
		{
			return v1.cmp_scalar_gt(v.v1);
		}

		[[nodiscard]] int cmp_scalar_eq(vector v) const noexcept
		{
			return v1.cmp_scalar_eq(v.v1);
		}

		[[nodiscard]] int cmp_scalar_le(vector v) const noexcept
		{
			return v1.cmp_scalar_le(v.v1);
		}

		[[nodiscard]] int cmp_scalar_lt(vector v) const noexcept
		{
			return v1.cmp_scalar_lt(v.v1);
		}

		[[nodiscard]] vector is_nan() const noexcept
		{
			return vector{v1.is_nan()};
		}

		[[nodiscard]] vector is_infinite() const noexcept
		{
			return vector{v1.is_infinite()};
		}

		[[nodiscard]] vector is_normal() const noexcept
			{ return vector{v1.is_normal()}; }

		[[nodiscard]] bool is_all_zero() const noexcept
		{
			return v1.is_all_zero();
		}

		using common_op_type::operator+=;
		using common_op_type::operator-=;
		using common_op_type::operator/=;
		using common_op_type::operator*=;
		using common_op_type::operator%=;
		using common_op_type::operator|=;
		using common_op_type::operator&=;
		using common_op_type::operator^=;
		using common_op_type::operator>>=;
		using common_op_type::operator<<=;


		[[nodiscard]] vector operator+ (vector v) const noexcept
		{
			return vector{v1 + v.v1};
		}

		[[nodiscard]] vector operator- (vector v) const noexcept
		{
			return vector{v1 - v.v1};
		}

		[[nodiscard]] vector operator* (vector v) const noexcept
		{
			return vector{v1 * v.v1};
		}

		[[nodiscard]] vector operator/ (vector v) const noexcept
		{
			return vector{v1 / v.v1};
		}

		[[nodiscard]] vector operator% (vector v) const noexcept
		{
			return vector{v1 % v.v1};
		}

		[[nodiscard]] vector operator+ (const value_type &v) const noexcept
		{
			return vector{v1 + v};
		}

		[[nodiscard]] vector operator- (const value_type &v) const noexcept
		{
			return vector{v1 - v};
		}

		[[nodiscard]] vector operator* (const value_type &v) const noexcept
		{
			return vector{v1 * v};
		}

		[[nodiscard]] vector operator/ (const value_type &v) const noexcept
		{
			return vector{v1 / v};
		}

		[[nodiscard]] vector operator% (const value_type &v) const noexcept
		{
			return vector{v1 % v};
		}

		[[nodiscard]] vector operator== (vector v) const noexcept
		{
			return vector{v1 == v.v1};
		}

		[[nodiscard]] vector operator!= (vector v) const noexcept
		{
			return vector{v1 != v.v1};
		}

		[[nodiscard]] vector operator< (vector v) const noexcept
		{
			return vector{v1 < v.v1};
		}

		[[nodiscard]] vector operator<= (vector v) const noexcept
		{
			return vector{v1 <= v.v1};
		}

		[[nodiscard]] vector operator> (vector v) const noexcept
		{
			return vector{v1 > v.v1};
		}

		[[nodiscard]] vector operator>= (vector v) const noexcept
		{
			return vector{v1 >= v.v1};
		}

		[[nodiscard]] vector operator== (const value_type v) const noexcept
		{
			return vector{v1 == v};
		}

		[[nodiscard]] vector operator!= (const value_type v) const noexcept
		{
			return vector{v1 != v};
		}

		[[nodiscard]] vector operator< (const value_type v) const noexcept
		{
			return vector{v1 < v};
		}

		[[nodiscard]] vector operator<= (const value_type v) const noexcept
		{
			return vector{v1 <= v};
		}

		[[nodiscard]] vector operator> (const value_type v) const noexcept
		{
			return vector{v1 > v};
		}

		[[nodiscard]] vector operator>= (const value_type v) const noexcept
		{
			return vector{v1 >= v};
		}

		[[nodiscard]] vector operator& (vector v) const noexcept
		{
			return vector{v1 & v.v1};
		}

		[[nodiscard]] vector operator| (vector v) const noexcept
		{
			return vector{v1 | v.v1};
		}

		[[nodiscard]] vector operator^ (vector v) const noexcept
		{
			return vector{v1 ^ v.v1};
		}

		[[nodiscard]] vector operator& (const i64 v) const noexcept
		{
			return vector{v1 & v};
		}

		[[nodiscard]] vector operator| (const i64 v) const noexcept
		{
			return vector{v1 | v};
		}

		[[nodiscard]] vector operator^ (const i64 v) const noexcept
		{
			return vector{v1 ^ v};
		}

		[[nodiscard]] vector operator>> (const int v) const noexcept
		{
			return vector{v1 >> v};
		}

		[[nodiscard]] vector operator<< (const int v) const noexcept
		{
			return vector{v1 << v};
		}

		#ifdef __AVX2__
			[[nodiscard]] vector operator>> (vector v) const noexcept
			{
				return vector{v1 >> v.v1};
			}

			[[nodiscard]] vector operator<< (vector v) const noexcept
			{
				return vector{v1 << v.v1};
			}
		#endif

		[[nodiscard]] vector operator! () const noexcept
		{
			return vector{!v1};
		}

		[[nodiscard]] vector operator- () const noexcept
		{
			return vector{-v1};
		}

		[[nodiscard]] vector operator+ () const noexcept
		{
			return vector{+v1};
		}

		[[nodiscard]] vector operator~ () const noexcept
		{
			return vector{~v1};
		}

		vector &operator++ () noexcept
			{ ++v1; return *this; }

		vector &operator-- () noexcept
			{ --v1; return *this; }

		vector operator++ (int) noexcept
			{ return v1++; }

		vector operator-- (int) noexcept
			{ return v1--; }

		[[nodiscard]] vector blendv(vector v, vector mask) const noexcept
			{ return vector{v1.blendv(v.v1, mask.v1)}; }

		template <int mask>
		[[nodiscard]] vector blend(vector v) const noexcept
			{ return vector{v1.template blend<mask>(v.v1)}; }

		/*vector unpacklo(vector v) const noexcept
			{ return v1.unpacklo(v.v1); }

		vector unpackhi(vector v) const noexcept
			{ return v1.unpackhi(v.v1); }*/

		//[[nodiscard]] explicit operator int() const noexcept
			//{ return (int)v1; }

		[[nodiscard]] auto mask() const noexcept
			{ return v1.mask(); }

		[[nodiscard]] explicit operator bool() const noexcept
			{ return static_cast<bool>(v1); }
	};


	template <>
	class vector<double, size_c<4>, types::simd_vector_t<types::simd_256_t>, categories::color_t> :
		public vector_storage<double, size_c<4>, categories::color_t, vector_simd_256d_4_storage<categories::color_t, vector<double, size_c<4>, types::simd_vector_t<types::simd_256_t>, categories::color_t>>>,
		public vector_base<vector<double, size_c<4>, types::simd_vector_t<types::simd_256_t>, categories::color_t>>,
		public common_op<vector<double, size_c<4>, types::simd_vector_t<types::simd_256_t>, categories::color_t>>
	{
		using common_op_type = common_op<vector<double, size_c<4>, types::simd_vector_t<types::simd_256_t>, categories::color_t>>;
		using storage_type = vector_storage<double, size_c<4>, categories::color_t, vector_simd_256d_4_storage<categories::color_t, vector<double, size_c<4>, types::simd_vector_t<types::simd_256_t>, categories::color_t>>>;

	public:

		using impl_type = types::simd_vector_t<types::simd_256_t>;

		static constexpr double LastValue = 1;
		static constinit const bool HardwareImpl = true;

		using typename storage_type::value_type;
		using typename storage_type::category;
		using storage_type::v1;
		using storage_type::storage_type;

		vector() = default;
		vector(const value_type v) noexcept : storage_type{v, LastValue} {}
		vector(const value_type x, const value_type y, const value_type z) noexcept : storage_type{x, y, z, LastValue} {}
		vector(const tag::identity_t) noexcept { set_identity(); }


		vector &set_identity() noexcept
		{
			v1.set_identity(LastValue);
			return *this;
		}

		[[nodiscard]] vector abs() const noexcept
		{
			return v1.abs();
		}

		[[nodiscard]] vector andnot(vector v) const noexcept
		{
			return vector{v1.andnot(v.v1)};
		}

		[[nodiscard]] vector next_after() const noexcept
		{
			return vector{v1.next_after()};
		}

		[[nodiscard]] value_type minimal_safe_increment() const noexcept
		{
			const auto vabs = abs();
			return (vabs.next_after() - vabs).max3();
		}

		[[nodiscard]] vector pow(const value_type power) const noexcept
		{
			return vector{v1.pow(power)};
		}

		[[nodiscard]] vector log() const noexcept
		{
			return vector{v1.log()};
		}

		[[nodiscard]] vector log2() const noexcept
		{
			return vector{v1.log2()};
		}

		[[nodiscard]] vector log2approx() const noexcept
		{
			return vector{v1.log2approx()};
		}

		[[nodiscard]] vector log10() const noexcept
		{
			return vector{v1.log10()};
		}

		[[nodiscard]] vector exp() const noexcept
		{
			return vector{v1.exp()};
		}

		[[nodiscard]] vector exp2() const noexcept
		{
			return vector{v1.exp2()};
		}

		[[nodiscard]] vector exp2approx() const noexcept
		{
			return vector{v1.exp2approx()};
		}

		[[nodiscard]] vector sin() const noexcept
		{
			return vector{v1.sin()};
		}

		[[nodiscard]] vector cos() const noexcept
		{
			return vector{v1.cos()};
		}

		[[nodiscard]] auto sincos() const noexcept
		{
			return sincos_res{sin(), cos()};
		}

		[[nodiscard]] vector sqrt() const noexcept
		{
			return vector{v1.sqrt()};
		}

		[[nodiscard]] vector sqrtapprox() const noexcept
		{
			return vector{v1.sqrtapprox()};
		}

		[[nodiscard]] vector sqrt_scalar() const noexcept
		{
			return vector{v1.sqrt_scalar()};
		}

		[[nodiscard]] vector sqrtapprox_scalar() const noexcept
		{
			return vector{v1.sqrtapprox_scalar()};
		}

		[[nodiscard]] vector recsqrt() const noexcept
		{
			return vector{v1.recsqrt()};
		}

		[[nodiscard]] vector recsqrtapprox() const noexcept
		{
			return vector{v1.recsqrtapprox()};
		}

		[[nodiscard]] vector rotate() const noexcept
		{
			// a b c d -> b c d a
			const auto ar1 = this->get_array();

			return {ar1()[1], ar1()[2], ar1()[3], ar1()[0]};
		}

		template <int m0, int m1, int m2, int m3>
		[[nodiscard]] vector shuffle(vector v) const noexcept
		{
			const auto ar1 = this->get_array();
			const auto ar2 = v.get_array();

			return {ar1()[m0], ar1()[m1], ar2()[m2], ar2()[m3]};
		}

		template <int m0, int m1, int m2, int m3>
		[[nodiscard]] vector shuffle() const noexcept
		{
			const auto ar1 = this->get_array();

			return {ar1()[m0], ar1()[m1], ar1()[m2], ar1()[m3]};
		}

		[[nodiscard]] vector movehl(vector v) const noexcept
		{
			__m128d un1[2], un2[2];
			std::memcpy(&un1, &v1.mVector, sizeof(v1.mVector));
			std::memcpy(&un2, &v.v1.mVector, sizeof(v.v1.mVector));

			return _mm256_set_m128d(un1[1], un2[1]);
		}

		[[nodiscard]] vector movelh(vector v) const noexcept
		{
			__m128d un1[2], un2[2];
			std::memcpy(&un1, &v1.mVector, sizeof(v1.mVector));
			std::memcpy(&un2, &v.v1.mVector, sizeof(v.v1.mVector));

			return _mm256_set_m128d(un1[0], un2[0]);
		}

		[[nodiscard]] int move_mask() const noexcept
		{
			return v1.move_mask();
		}

		[[nodiscard]] vector normalize() const noexcept
		{
			return *this / dot_m(masked{size_v<0xF1>, *this}).sqrt();
		}

		[[nodiscard]] vector normalize_approx() const noexcept
		{
			return *this * dot_m(masked{size_v<0xF1>, *this}).recsqrtapprox();
		}

		[[nodiscard]] vector normalize(const value_type length) const noexcept
		{
			return vector{v1 / length};
		}

		// http://fastcpp.blogspot.ca/2011/04/vector-cross-product-using-sse-code.html
		[[nodiscard]] vector cross(vector v) const noexcept
		{
			return (*this * v.template shuffle<1, 2, 0, 3>() - this->template shuffle<1, 2, 0, 3>() * v).template shuffle<1, 2, 0, 3>();
		}

		[[nodiscard]] value_type dot(vector v) const noexcept
		{
			return v1.dot(v.v1);
		}

		[[nodiscard]] value_type dot() const noexcept
		{
			return v1.dot();
		}

		template <szt mask = 0xFF>
		[[nodiscard]] value_type dot(masked<mask, vector> v) const noexcept
		{
			return v1.dot(masked{size_v<mask>, v.v.v1});
		}

		template <szt mask = 0xFF>
		[[nodiscard]] vector dot_m(masked<mask, vector> v) const noexcept
		{
			return v1.dot_m(masked{size_v<mask>, v.v.v1});
		}

		[[nodiscard]] value_type length() const noexcept
		{
			auto tmp1 = _mm256_mul_pd(v1.mVector, v1.mVector);
			auto l = mm256_horizontal_add(tmp1, tmp1);

			return plsqrt(l);
		}

		[[nodiscard]] value_type length_approx() const noexcept
		{
			auto tmp1 = _mm256_mul_pd(v1.mVector, v1.mVector);
			auto l = mm256_horizontal_add(tmp1, tmp1);

			return patlib::sqrtapprox(l);
		}

		[[nodiscard]] vector min(vector v) const noexcept
		{
			return vector{v1.min(v.v1)};
		}

		[[nodiscard]] value_type hmin() const noexcept
		{
			return v1.hmin();
		}

		[[nodiscard]] value_type min3() const noexcept
		{
			return v1.min3();
		}

		[[nodiscard]] vector min_scalar(vector v) const noexcept
		{
			return vector{v1.min_scalar(v.v1)};
		}

		[[nodiscard]] vector max_scalar(vector v) const noexcept
		{
			return vector{v1.max_scalar(v.v1)};
		}

		[[nodiscard]] vector max(vector v) const noexcept
		{
			return vector{v1.max(v.v1)};
		}

		[[nodiscard]] value_type hmax() const noexcept
		{
			return v1.hmax();
		}

		[[nodiscard]] value_type max3() const noexcept
		{
			return v1.max3();
		}

		[[nodiscard]] szt max3_index() const noexcept
		{
			auto v = v1.get_array();

			value_type maxVal = v()[0];
			szt maxIndex = 0;

			if(v()[1] > maxVal)
			{
				maxIndex = 1;
				maxVal = v()[1];
			}

			if(v()[2] > maxVal)
				maxIndex = 2;

			return maxIndex;
		}

		[[nodiscard]] szt min3_index() const noexcept
		{
			auto v = v1.get_array();

			value_type minVal = v()[0];
			szt minIndex = 0;

			if(v()[1] < minVal)
			{
				minIndex = 1;
				minVal = v()[1];
			}

			if(v()[2] < minVal)
				minIndex = 2;

			return minIndex;
		}

		[[nodiscard]] szt abs_min3_index() const noexcept
		{
			auto v = v1.get_array();

			value_type minVal = plabs(v()[0]), cVal = plabs(v()[1]);
			szt minIndex = 0;

			if(cVal < minVal)
			{
				minIndex = 1;
				minVal = cVal;
			}

			if(plabs(v()[2]) < minVal)
				minIndex = 2;

			return minIndex;
		}

		[[nodiscard]] szt abs_max3_index() const noexcept
		{
			auto v = v1.get_array();

			value_type maxVal = plabs(v()[0]), cVal = plabs(v()[1]);
			szt maxIndex = 0;

			if(cVal > maxVal)
			{
				maxIndex = 1;
				maxVal = cVal;
			}

			if(plabs(v()[2]) > maxVal)
				maxIndex = 2;

			return maxIndex;
		}

		[[nodiscard]] vector floor() const noexcept
		{
			return vector{v1.floor()};
		}

		[[nodiscard]] vector ceil() const noexcept
		{
			return vector{v1.ceil()};
		}

		[[nodiscard]] vector round() const noexcept
		{
			return vector{v1.round()};
		}

		[[nodiscard]] vector inverse_sign() const noexcept
		{
			return vector{v1.inverse_sign()};
		}

		[[nodiscard]] vector reciprocal() const noexcept
		{
			return vector{v1.reciprocal()};
		}

		[[nodiscard]] vector reciprocalapprox() const noexcept
		{
			return vector{v1.reciprocalapprox()};
		}

		[[nodiscard]] vector hmul() const noexcept
		{
			return vector{v1.hmul()};
		}

		[[nodiscard]] vector hadd() const noexcept
		{
			return vector{v1.hadd()};
		}

		[[nodiscard]] vector hsub() const noexcept
		{
			return vector{v1.hsub()};
		}

		[[nodiscard]] int cmp_scalar_ge(vector v) const noexcept
		{
			return v1.cmp_scalar_ge(v.v1);
		}

		[[nodiscard]] int cmp_scalar_gt(vector v) const noexcept
		{
			return v1.cmp_scalar_gt(v.v1);
		}

		[[nodiscard]] int cmp_scalar_eq(vector v) const noexcept
		{
			return v1.cmp_scalar_eq(v.v1);
		}

		[[nodiscard]] int cmp_scalar_le(vector v) const noexcept
		{
			return v1.cmp_scalar_le(v.v1);
		}

		[[nodiscard]] int cmp_scalar_lt(vector v) const noexcept
		{
			return v1.cmp_scalar_lt(v.v1);
		}

		[[nodiscard]] vector is_nan() const noexcept
		{
			return vector{v1.is_nan()};
		}

		[[nodiscard]] vector is_infinite() const noexcept
		{
			return vector{v1.is_infinite()};
		}

		[[nodiscard]] vector is_normal() const noexcept
			{ return vector{v1.is_normal()}; }

		[[nodiscard]] bool is_all_zero() const noexcept
		{
			return v1.is_all_zero();
		}

		using common_op_type::operator+=;
		using common_op_type::operator-=;
		using common_op_type::operator/=;
		using common_op_type::operator*=;
		using common_op_type::operator%=;
		using common_op_type::operator|=;
		using common_op_type::operator&=;
		using common_op_type::operator^=;
		using common_op_type::operator>>=;
		using common_op_type::operator<<=;

		[[nodiscard]] vector operator+ (vector v) const noexcept
		{
			return vector{v1 + v.v1};
		}

		[[nodiscard]] vector operator- (vector v) const noexcept
		{
			return vector{v1 - v.v1};
		}

		[[nodiscard]] vector operator* (vector v) const noexcept
		{
			return vector{v1 * v.v1};
		}

		[[nodiscard]] vector operator/ (vector v) const noexcept
		{
			return vector{v1 / v.v1};
		}

		[[nodiscard]] vector operator% (vector v) const noexcept
		{
			return vector{v1 % v.v1};
		}

		[[nodiscard]] vector operator+ (const value_type &v) const noexcept
		{
			return vector{v1 + v};
		}

		[[nodiscard]] vector operator- (const value_type &v) const noexcept
		{
			return vector{v1 - v};
		}

		[[nodiscard]] vector operator* (const value_type &v) const noexcept
		{
			return vector{v1 * v};
		}

		[[nodiscard]] vector operator/ (const value_type &v) const noexcept
		{
			return vector{v1 / v};
		}

		[[nodiscard]] vector operator% (const value_type &v) const noexcept
		{
			return vector{v1 % v};
		}

		[[nodiscard]] vector operator== (vector v) const noexcept
		{
			return vector{v1 == v.v1};
		}

		[[nodiscard]] vector operator!= (vector v) const noexcept
		{
			return vector{v1 != v.v1};
		}

		[[nodiscard]] vector operator< (vector v) const noexcept
		{
			return vector{v1 < v.v1};
		}

		[[nodiscard]] vector operator<= (vector v) const noexcept
		{
			return vector{v1 <= v.v1};
		}

		[[nodiscard]] vector operator> (vector v) const noexcept
		{
			return vector{v1 > v.v1};
		}

		[[nodiscard]] vector operator>= (vector v) const noexcept
		{
			return vector{v1 >= v.v1};
		}

		[[nodiscard]] vector operator== (const value_type v) const noexcept
		{
			return vector{v1 == v};
		}

		[[nodiscard]] vector operator!= (const value_type v) const noexcept
		{
			return vector{v1 != v};
		}

		[[nodiscard]] vector operator< (const value_type v) const noexcept
		{
			return vector{v1 < v};
		}

		[[nodiscard]] vector operator<= (const value_type v) const noexcept
		{
			return vector{v1 <= v};
		}

		[[nodiscard]] vector operator> (const value_type v) const noexcept
		{
			return vector{v1 > v};
		}

		[[nodiscard]] vector operator>= (const value_type v) const noexcept
		{
			return vector{v1 >= v};
		}

		[[nodiscard]] vector operator& (vector v) const noexcept
		{
			return vector{v1 & v.v1};
		}

		[[nodiscard]] vector operator| (vector v) const noexcept
		{
			return vector{v1 | v.v1};
		}

		[[nodiscard]] vector operator^ (vector v) const noexcept
		{
			return vector{v1 ^ v.v1};
		}

		[[nodiscard]] vector operator& (const i64 v) const noexcept
		{
			return vector{v1 & v};
		}

		[[nodiscard]] vector operator| (const i64 v) const noexcept
		{
			return vector{v1 | v};
		}

		[[nodiscard]] vector operator^ (const i64 v) const noexcept
		{
			return vector{v1 ^ v};
		}

		[[nodiscard]] vector operator>> (const int v) const noexcept
		{
			return vector{v1 >> v};
		}

		[[nodiscard]] vector operator<< (const int v) const noexcept
		{
			return vector{v1 << v};
		}

		#ifdef __AVX2__
			[[nodiscard]] vector operator>> (vector v) const noexcept
			{
				return vector{v1 >> v.v1};
			}

			[[nodiscard]] vector operator<< (vector v) const noexcept
			{
				return vector{v1 << v.v1};
			}
		#endif

		[[nodiscard]] vector operator! () const noexcept
		{
			return vector{!v1};
		}

		[[nodiscard]] vector operator- () const noexcept
		{
			return vector{-v1};
		}

		[[nodiscard]] vector operator+ () const noexcept
		{
			return vector{+v1};
		}

		[[nodiscard]] vector operator~ () const noexcept
		{
			return vector{~v1};
		}

		vector &operator++ () noexcept
			{ ++v1; return *this; }

		vector &operator-- () noexcept
			{ --v1; return *this; }

		vector operator++ (int) noexcept
			{ return v1++; }

		vector operator-- (int) noexcept
			{ return v1--; }

		[[nodiscard]] vector blendv(vector v, vector mask) const noexcept
			{ return vector{v1.blendv(v.v1, mask.v1)}; }

		template <int mask>
		[[nodiscard]] vector blend(vector v) const noexcept
			{ return vector{v1.template blend<mask>(v.v1)}; }

		//[[nodiscard]] explicit operator int() const noexcept
			//{ return (int)v1; }

		[[nodiscard]] auto mask() const noexcept
			{ return v1.mask(); }

		[[nodiscard]] explicit operator bool() const noexcept
			{ return static_cast<bool>(v1); }
	};
}
