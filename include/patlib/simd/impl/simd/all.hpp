/*
	PatLib

	Copyright (C) 2020 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#ifdef __SSE4_1__
	#include "128ps.hpp"
	#include "128pd.hpp"
#endif

#ifdef __AVX__
	#include "256ps.hpp"
	#include "256pd.hpp"
#endif

namespace patlib::simd::impl
{
	#if defined(PL_LIB_VECTOR_256_FLOAT_SIMD) && defined(PL_LIB_VECTOR_128_FLOAT_SIMD)
		template <typename Cat1_, typename Der1_, typename Cat2_, typename Der2_>
		void convert(const vector_storage<float, size_c<4>, Cat1_, vector_simd_128s_storage<Cat1_, Der1_>> &from, vector_storage<float, size_c<8>, Cat2_, vector_simd_256s_storage<Cat2_, Der2_>> &into)
		{
			into.mVector = _mm256_castps128_ps256(from.mVector);
		}

		template <typename Cat1_, typename Der1_, typename Cat2_, typename Der2_>
		void convert(const vector_storage<float, size_c<8>, Cat2_, vector_simd_256s_storage<Cat2_, Der2_>> &from, vector_storage<float, size_c<4>, Cat1_, vector_simd_128s_storage<Cat1_, Der1_>> &into)
		{
			into.mVector = _mm256_castps256_ps128(from.mVector);
		}
	#endif

	#if defined(PL_LIB_VECTOR_256_DOUBLE_SIMD) && defined(PL_LIB_VECTOR_128_DOUBLE_SIMD)
		template <typename Cat1_, typename Der1_, typename Cat2_, typename Der2_>
		void convert(const vector_storage<double, size_c<4>, Cat1_, vector_simd_128d_storage<Cat1_, Der1_>> &from, vector_storage<double, size_c<8>, Cat2_, vector_simd_256d_storage<Cat2_, Der2_>> &into)
		{
			into.mVector = _mm256_castpd128_pd256(from.mVector);
		}

		template <typename Cat1_, typename Der1_, typename Cat2_, typename Der2_>
		void convert(const vector_storage<double, size_c<8>, Cat2_, vector_simd_256d_storage<Cat2_, Der2_>> &from, vector_storage<double, size_c<4>, Cat1_, vector_simd_128d_storage<Cat1_, Der1_>> &into)
		{
			into.mVector = _mm256_castpd256_pd128(from.mVector);
		}
	#endif
}