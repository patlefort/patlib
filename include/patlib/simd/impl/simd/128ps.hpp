/*
	PatLib

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "base.hpp"

#ifndef PL_LIB_VECTOR_128_SIMD
	#define PL_LIB_VECTOR_128_SIMD
#endif

#ifndef PL_LIB_VECTOR_128_FLOAT_SIMD
	#define PL_LIB_VECTOR_128_FLOAT_SIMD
#endif

#ifndef PL_LIB_VECTOR_FLOAT_SIMD
	#define PL_LIB_VECTOR_FLOAT_SIMD
#endif

namespace patlib::simd::impl
{
	struct wrapped__m128 { using type = __m128; };

	template <typename Category_, typename Derived_>
	class vector_simd_128s_storage :
		public vector_simd_storage_base<float, wrapped__m128, 4, Category_>,
		public iteratable<float, 4, vector_simd_128s_storage<Category_, Derived_>>
	{
		PL_CRTP_BASE(Derived_)
		using storage_type = vector_simd_storage_base<float, wrapped__m128, 4, Category_>;

	public:
		static constinit const auto NbValues = storage_type::NbValues;

		using typename storage_type::value_type;
		using typename storage_type::aligned_array_type;
		using storage_type::mVector;
		using storage_type::storage_type;

		vector_simd_128s_storage() = default;
		vector_simd_128s_storage(const value_type v) noexcept : storage_type{ _mm_set_ps1(v) } {}
		vector_simd_128s_storage(const value_type v, const value_type w) noexcept : storage_type{ _mm_set_ps(w, v, v, v) } {}
		PL_VECTOR_VARCTOR(vector_simd_128s_storage, _mm_set_ps)
		explicit vector_simd_128s_storage(const tag::array_init_t, const value_type *v, tag::aligned_init_t = {}) noexcept : storage_type{ _mm_load_ps(v) } {}
		explicit vector_simd_128s_storage(const tag::array_init_t, const value_type *v, tag::unaligned_init_t) noexcept : storage_type{ _mm_loadu_ps(v) } {}
		vector_simd_128s_storage(const aligned_array_type &v) noexcept : storage_type{ _mm_load_ps(v().data()) } {}

		vector_simd_128s_storage(const tag::zero_t) noexcept : storage_type{{}} {}
		vector_simd_128s_storage(const tag::identity_t, const value_type last = 1) noexcept { set_identity(last); }
		vector_simd_128s_storage(const tag::allone_t) noexcept { set_all_one(); }
		vector_simd_128s_storage(const tag::one_t) noexcept { set_one(); }
		vector_simd_128s_storage(const tag::two_t) noexcept { set_two(); }
		vector_simd_128s_storage(const tag::half_t) noexcept { set_half(); }
		vector_simd_128s_storage(const tag::nan_t) noexcept { set_nan(); }


		[[nodiscard]] value_type operator[] (const szt index) const noexcept
		{
			#if BOOST_COMP_MSVC
				return mVector.m128_f32[index];
			#else
				return this->get_array()()[index];
			#endif
		}

		template <szt index>
		[[nodiscard]] value_type operator[] (size_c<index>) const noexcept
		{
			if constexpr(index == 0)
				return _mm_cvtss_f32(mVector);
			else
				return _mm_cvtss_f32(_mm_shuffle_ps(mVector, mVector, _MM_SHUFFLE(0, 0, 0, index)));
		}

		auto &set_zero() noexcept
		{
			mVector = _mm_setzero_ps();
			return derived();
		}

		auto &set(const value_type v) noexcept
		{
			mVector = _mm_set_ps1(v);
			return derived();
		}

		auto &set_all_one() noexcept
		{
			mVector = _mm_castsi128_ps(_mm_cmpeq_epi32(_mm_castps_si128(mVector), _mm_castps_si128(mVector)));
			return derived();
		}

		auto &set_one() noexcept
		{
			mVector = _mm_castsi128_ps( _mm_srli_epi32(_mm_slli_epi32(_mm_cmpeq_epi32(_mm_castps_si128(mVector), _mm_castps_si128(mVector)), 25), 2) );
			return derived();
		}

		auto &set_half() noexcept
		{
			mVector = _mm_castsi128_ps( _mm_srli_epi32(_mm_slli_epi32(_mm_cmpeq_epi32(_mm_castps_si128(mVector), _mm_castps_si128(mVector)), 26), 2) );
			return derived();
		}

		auto &set_two() noexcept
		{
			mVector = _mm_castsi128_ps( _mm_srli_epi32(_mm_slli_epi32(_mm_cmpeq_epi32(_mm_castps_si128(mVector), _mm_castps_si128(mVector)), 31), 1) );
			return derived();
		}

		auto &set_nan() noexcept
		{
			return (derived() = std::numeric_limits<value_type>::quiet_NaN());
		}

		auto &set_all(const value_type x, const value_type y, const value_type z, const value_type w) noexcept
		{
			mVector = _mm_set_ps(w, z, y, x);
			return derived();
		}

		auto &set(const value_type *v) noexcept
		{
			mVector = _mm_load_ps(v);
			return derived();
		}

		auto &set_unaligned(const value_type *v) noexcept
		{
			mVector = _mm_loadu_ps(v);
			return derived();
		}

		auto &set(const aligned_array_type &v) noexcept
		{
			mVector = _mm_load_ps(v().data());
			return derived();
		}

		void get_all(value_type *v) const noexcept
		{
			_mm_storeu_ps(v, mVector);
		}

		void get_all_aligned(value_type *v) const noexcept
		{
			_mm_store_ps(v, mVector);
		}

		[[nodiscard]] aligned_array_type get_array() const noexcept
		{
			aligned_array_type a; get_all_aligned(a().data());
			return a;
		}

		auto &set(const value_type v, const int index) noexcept
		{
			auto va = get_array();
			va()[index] = v;
			set(va);

			return derived();
		}

		template <szt index>
		auto &set(const value_type v, size_c<index> = {}) noexcept
		{
			mVector = _mm_insert_ps(mVector, _mm_set_ss(v), (index << 4));

			return derived();
		}

		auto &set_identity(const value_type last = 1) noexcept
		{
			mVector = _mm_insert_ps(_mm_setzero_ps(), _mm_set_ss(last), 0b110111);

			return derived();
		}
	};

	template <typename Category_>
	class vector<float, size_c<4>, types::simd_vector_t<types::simd_128_t>, Category_> :
		public vector_storage<float, size_c<4>, Category_, vector_simd_128s_storage<Category_, vector<float, size_c<4>, types::simd_vector_t<types::simd_128_t>, Category_>>>,
		public vector_base<vector<float, size_c<4>, types::simd_vector_t<types::simd_128_t>, Category_>>,
		public common_op<vector<float, size_c<4>, types::simd_vector_t<types::simd_128_t>, Category_>>
	{
		using Base = vector_base<vector<float, size_c<4>, types::simd_vector_t<types::simd_128_t>, Category_>>;
		using common_op_type = common_op<vector<float, size_c<4>, types::simd_vector_t<types::simd_128_t>, Category_>>;
		using storage_type = vector_storage<float, size_c<4>, Category_, vector_simd_128s_storage<Category_, vector<float, size_c<4>, types::simd_vector_t<types::simd_128_t>, Category_>>>;

	public:
		static constinit const bool HardwareImpl = true;
		static constinit const auto NbValues = storage_type::NbValues;

		using typename storage_type::value_type;
		using typename storage_type::category;
		using typename storage_type::simd_type;
		using storage_type::mVector;
		using storage_type::storage_type;

		using impl_type = types::simd_vector_t<types::simd_128_t>;


		[[nodiscard]] vector andnot(vector v) const noexcept
		{
			return _mm_andnot_ps(mVector, v.mVector);
		}

		[[nodiscard]] vector next_after() const noexcept
		{
			return _mm_castsi128_ps(_mm_add_epi32(_mm_castps_si128(mVector), _mm_set1_epi32(1)));
		}

		[[nodiscard]] vector abs() const noexcept
		{
			return _mm_and_ps(mVector, _mm_castsi128_ps(_mm_srli_epi32(_mm_set1_epi32(-1), 1)));
		}

		[[nodiscard]] vector pow(const value_type power) const noexcept
		{
			return select((abs().log() * power).exp(), *this > tag::zero);
		}

		[[nodiscard]] vector log() const noexcept
		{
			return simd::SseMathfun::log_ps(mVector);
		}

		[[nodiscard]] vector log2() const noexcept
		{
			return vector_transform(*this, [](auto&&, auto&& v){ return pllog2(v); });
		}

		// From http://cgit.freedesktop.org/mesa/mesa/tree/src/gallium/auxiliary/tgsi/tgsi_sse2.c?h=7.9#n727
		template <int Degree_ = 5>
		[[nodiscard]] vector log2approx(std::integral_constant<int, Degree_> = {}) const noexcept
		{
			auto expmask = _mm_set1_epi32(0x7f800000);
			auto mantmask = _mm_set1_epi32(0x007fffff);
			auto one = _mm_set1_ps(1.0f);

			auto i = _mm_castps_si128(mVector);

			/* exp = (value_type) exponent(x) */
			auto exp = _mm_cvtepi32_ps(_mm_sub_epi32(_mm_srli_epi32(_mm_and_si128(i, expmask), 23), _mm_set1_epi32(127)));

			/* mant = (value_type) mantissa(x) */
			vector mant = _mm_or_ps(_mm_castsi128_ps(_mm_and_si128(i, mantmask)), one);

			vector logmant;

			/* Minimax polynomial fit of log2(x)/(x - 1), for x in range [1, 2[
			* These coefficients can be generate with
			* http://www.boost.org/doc/libs/1_36_0/libs/math/doc/sf_and_dist/html/math_toolkit/toolkit/internals2/minimax.html
			*/
			static_assert(Degree_ >= 3 && Degree_ <= 6, "Degree_ must be between 2 and 5.");

			if constexpr(Degree_ == 6)
				logmant = minimax_polynomial_fit(mant, 3.11578814719469302614f, -3.32419399085241980044f, 2.59883907202499966007f, -1.23152682416275988241f, 0.318212422185251071475f, -0.0344359067839062357313f);
			else if constexpr(Degree_ == 5)
				logmant = minimax_polynomial_fit(mant, 2.8882704548164776201f, -2.52074962577807006663f, 1.48116647521213171641f, -0.465725644288844778798f, 0.0596515482674574969533f);
			else if constexpr(Degree_ == 4)
				logmant = minimax_polynomial_fit(mant, 2.61761038894603480148f, -1.75647175389045657003f, 0.688243882994381274313f, -0.107254423828329604454f);
			else if constexpr(Degree_ == 3)
				logmant = minimax_polynomial_fit(mant, 2.28330284476918490682f, -1.04913055217340124191f, 0.204446009836232697516f);

			/* This effectively increases the polynomial degree by one, but ensures that log2(1) == 0*/
			logmant *= mant - one;

			return logmant + exp;
		}

		[[nodiscard]] vector log10() const noexcept
		{
			return vector_transform(*this, [](auto&&, auto&& v){ return pllog10(v); });
		}

		[[nodiscard]] vector exp() const noexcept
		{
			return simd::SseMathfun::exp_ps(mVector);
		}

		[[nodiscard]] vector exp2() const noexcept
		{
			return vector_transform(*this, [](auto&&, auto&& v){ return plexp2(v); });
		}

		// From http://cgit.freedesktop.org/mesa/mesa/tree/src/gallium/auxiliary/tgsi/tgsi_sse2.c?h=7.9#n727
		template <int Degree_ = 3>
		[[nodiscard]] vector exp2approx(std::integral_constant<int, Degree_> = {}) const noexcept
		{
			auto res = _mm_min_ps(mVector, _mm_set1_ps( 129.00000f));
			res = _mm_max_ps(res, _mm_set1_ps(-126.99999f));

			/* ipart = int(x - 0.5) */
			auto ipart = _mm_cvtps_epi32(_mm_sub_ps(res, _mm_set1_ps(0.5f)));

			/* fpart = x - ipart */
			vector fpart = _mm_sub_ps(res, _mm_cvtepi32_ps(ipart));

			/* expipart = (value_type) (1 << ipart) */
			auto expipart = _mm_castsi128_ps(_mm_slli_epi32(_mm_add_epi32(ipart, _mm_set1_epi32(127)), 23));

			/* minimax polynomial fit of 2**x, in range [-0.5, 0.5[ */
			static_assert(Degree_ >= 2 && Degree_ <= 5, "Degree_ must be between 2 and 5.");

			vector expfpart;

			if constexpr(Degree_ == 5)
				expfpart = minimax_polynomial_fit(fpart, 9.9999994e-1f, 6.9315308e-1f, 2.4015361e-1f, 5.5826318e-2f, 8.9893397e-3f, 1.8775767e-3f);
			else if constexpr(Degree_ == 4)
				expfpart = minimax_polynomial_fit(fpart, 1.0000026f, 6.9300383e-1f, 2.4144275e-1f, 5.2011464e-2f, 1.3534167e-2f);
			else if constexpr(Degree_ == 3)
				expfpart = minimax_polynomial_fit(fpart, 9.9992520e-1f, 6.9583356e-1f, 2.2606716e-1f, 7.8024521e-2f);
			else if constexpr(Degree_ == 2)
				expfpart = minimax_polynomial_fit(fpart, 1.0017247f, 6.5763628e-1f, 3.3718944e-1f);

			return expfpart * expipart;
		}



		[[nodiscard]] vector sin() const noexcept
		{
			return simd::SseMathfun::sin_ps(mVector);
		}

		[[nodiscard]] vector cos() const noexcept
		{
			return simd::SseMathfun::cos_ps(mVector);
		}

		[[nodiscard]] auto sincos() const noexcept
		{
			simd_type s, c;
			simd::SseMathfun::sincos_ps(mVector, s, c);
			return sincos_res{vector{s}, vector{c}};
		}

		void scalar(value_type &dest) noexcept
		{
			_mm_store_ss(&dest, mVector);
		}

		[[nodiscard]] value_type scalar() const noexcept
		{
			return _mm_cvtss_f32(mVector);
		}

		[[nodiscard]] int cmp_scalar_ge(vector v) const noexcept
		{
			return _mm_comige_ss(mVector, v.mVector);
		}

		[[nodiscard]] int cmp_scalar_gt(vector v) const noexcept
		{
			return _mm_comigt_ss(mVector, v.mVector);
		}

		[[nodiscard]] int cmp_scalar_eq(vector v) const noexcept
		{
			return _mm_comieq_ss(mVector, v.mVector);
		}

		[[nodiscard]] int cmp_scalar_le(vector v) const noexcept
		{
			return _mm_comile_ss(mVector, v.mVector);
		}

		[[nodiscard]] int cmp_scalar_lt(vector v) const noexcept
		{
			return _mm_comilt_ss(mVector, v.mVector);
		}


		[[nodiscard]] vector min(vector v) const noexcept
		{
			return _mm_min_ps(mVector, v.mVector);
		}

		[[nodiscard]] vector max(vector v) const noexcept
		{
			return _mm_max_ps(mVector, v.mVector);
		}

		[[nodiscard]] vector min_scalar(vector v) const noexcept
		{
			return _mm_min_ss(mVector, v.mVector);
		}

		[[nodiscard]] vector max_scalar(vector v) const noexcept
		{
			return _mm_max_ss(mVector, v.mVector);
		}

		[[nodiscard]] value_type hmin() const noexcept
		{
			auto vals = this->get_array();

			return plmin( {vals()[0], vals()[1], vals()[2], vals()[3]} );
		}

		[[nodiscard]] value_type min3() const noexcept
		{
			auto vals = this->get_array();

			return plmin( {vals()[0], vals()[1], vals()[2]} );
		}

		[[nodiscard]] value_type hmax() const noexcept
		{
			auto vals = this->get_array();

			return plmax( {vals()[0], vals()[1], vals()[2], vals()[3]} );
		}

		[[nodiscard]] value_type max3() const noexcept
		{
			auto vals = this->get_array();

			return plmax( {vals()[0], vals()[1], vals()[2]} );
		}

		[[nodiscard]] vector floor() const noexcept
		{
			return _mm_floor_ps(mVector);
		}

		[[nodiscard]] vector ceil() const noexcept
		{
			return _mm_ceil_ps(mVector);
		}

		[[nodiscard]] vector round() const noexcept
		{
			return _mm_round_ps(mVector, _MM_FROUND_TO_NEAREST_INT | _MM_FROUND_NO_EXC);
		}

		[[nodiscard]] vector sqrt() const noexcept
		{
			return _mm_sqrt_ps(mVector);
		}

		[[nodiscard]] vector sqrtapprox() const noexcept
		{
			return recsqrtapprox() * *this;
		}

		[[nodiscard]] vector sqrt_scalar() const noexcept
		{
			return _mm_sqrt_ss(mVector);
		}

		[[nodiscard]] vector sqrtapprox_scalar() const noexcept
		{
			const auto r = _mm_rsqrt_ss(mVector);

			// Newton-Rhapson step
			const auto r2 = _mm_mul_ss( _mm_mul_ss(_mm_set_ss(0.5f), r), _mm_sub_ss(_mm_set_ss(3), _mm_mul_ss(_mm_mul_ss(mVector, r), r)) );

			return _mm_mul_ss(r2, mVector);
		}

		[[nodiscard]] vector recsqrt() const noexcept
		{
			return sqrt().reciprocal();
		}

		[[nodiscard]] vector recsqrtapprox() const noexcept
		{
			const auto r = vector{_mm_rsqrt_ps(mVector)};

			// Newton-Rhapson step
			return r * tag::half * (vector{3} - *this * (r * r));
		}

		[[nodiscard]] vector hmul() const noexcept
		{
			const auto r = *this * _mm_movehdup_ps(mVector);
			return r * r.movehl(r);
		}

		[[nodiscard]] vector hsub() const noexcept
		{
			const auto r = _mm_hsub_ps(mVector, mVector);
			return _mm_hsub_ps(r, r);
		}

		[[nodiscard]] vector hadd() const noexcept
		{
			const auto r = _mm_hadd_ps(mVector, mVector);
			return _mm_hadd_ps(r, r);
		}

		[[nodiscard]] vector inverse_sign() const noexcept
		{
			return _mm_xor_ps(mVector, _mm_castsi128_ps(_mm_set1_epi32(0x80000000)));
		}

		[[nodiscard]] vector reciprocal() const noexcept
		{
			return vector{tag::one} / *this;
		}

		[[nodiscard]] vector reciprocalapprox() const noexcept
		{
			// Apply one Newton-Raphson step
			__m128 tmp = _mm_rcp_ps(mVector);
			__m128 two = _mm_castsi128_ps( _mm_srli_epi32(_mm_slli_epi32(_mm_cmpeq_epi32(_mm_castps_si128(mVector), _mm_castps_si128(mVector)), 31), 1) );
			return _mm_mul_ps(tmp, _mm_sub_ps(two, _mm_mul_ps(tmp, mVector)));
		}

		[[nodiscard]] vector movehl(vector v) const noexcept
		{
			return _mm_movehl_ps(mVector, v.mVector); // v2c v2d v1a v1b
		}

		[[nodiscard]] vector movelh(vector v) const noexcept
		{
			return _mm_movelh_ps(mVector, v.mVector); // v1a v1b v2a v2b
		}

		template <int m0, int m1, int m2, int m3>
		[[nodiscard]] vector shuffle(vector v) const noexcept
		{
			return _mm_shuffle_ps(mVector, v.mVector, _MM_SHUFFLE(m3, m2, m1, m0));
		}

		template <int m0, int m1, int m2, int m3>
		[[nodiscard]] vector shuffle() const noexcept
		{
			return _mm_castsi128_ps(_mm_shuffle_epi32(_mm_castps_si128(mVector), _MM_SHUFFLE(m3, m2, m1, m0)));
		}

		[[nodiscard]] int move_mask() const noexcept
		{
			return _mm_movemask_ps(mVector);
		}

		[[nodiscard]] value_type dot(vector v) const noexcept
		{
			#ifdef __SSE4_1__
				return _mm_cvtss_f32(_mm_dp_ps(mVector, v.mVector, 0xF1));
			#else
				__m128 tmp1;

				tmp1 = _mm_mul_ps(mVector, v.mVector);
				tmp1 = _mm_hadd_ps(tmp1, tmp1);
				tmp1 = _mm_hadd_ps(tmp1, tmp1);

				return _mm_cvtss_f32(tmp1);
			#endif
		}

		template <szt mask = 0xFF>
		[[nodiscard]] vector dot_m(masked<mask, vector> v) const noexcept
		{
			static constinit const auto maskInt = static_cast<int>(mask);

			#ifdef __SSE4_1__
				return _mm_dp_ps(mVector, v.v.mVector, mask);
			#else
				__m128 tmp1;

				if constexpr(mask == 0xFF)
				{
					tmp1 = _mm_mul_ps(v1.mVector, v2.mVector);
					tmp1 = _mm_hadd_ps(tmp1, tmp1);
					tmp1 = _mm_hadd_ps(tmp1, tmp1);
				}
				else
				{
					tmp1 = _mm_mul_ps(mVector, v.v.mVector);
					tmp1 = _mm_and_ps(tmp1, _mm_castsi128_ps(_mm_set_epi32( constants::all_ones<int> * ((maskInt>>7)&0x1), constants::all_ones<int> * ((maskInt>>6)&0x1), constants::all_ones<int> * ((maskInt>>5)&0x1), constants::all_ones<int> * ((maskInt>>4)&0x1) )));
					tmp1 = _mm_hadd_ps(tmp1, tmp1);
					tmp1 = _mm_hadd_ps(tmp1, tmp1);
					tmp1 = _mm_and_ps(tmp1, _mm_castsi128_ps(_mm_set_epi32( constants::all_ones<int> * ((maskInt>>3)&0x1), constants::all_ones<int> * ((maskInt>>2)&0x1), constants::all_ones<int> * ((maskInt>>1)&0x1), constants::all_ones<int> * ((maskInt)&0x1) )));
				}

				return tmp1;
			#endif
		}

		template <szt mask = 0xFF>
		[[nodiscard]] value_type dot(masked<mask, vector> v) const noexcept
		{
			return dot_m(masked{size_v<1 | (mask << 4)>, v.v}).scalar();
		}

		[[nodiscard]] value_type dot() const noexcept
		{
			return dot(*this);
		}

		[[nodiscard]] vector operator+ (vector v) const noexcept
		{
			return _mm_add_ps(mVector, v.mVector);
		}

		[[nodiscard]] vector operator- (vector v) const noexcept
		{
			return _mm_sub_ps(mVector, v.mVector);
		}

		[[nodiscard]] vector operator* (vector v) const noexcept
		{
			return _mm_mul_ps(mVector, v.mVector);
		}

		[[nodiscard]] vector operator/ (vector v) const noexcept
		{
			return _mm_div_ps(mVector, v.mVector);
		}

		[[nodiscard]] vector operator% (vector v) const noexcept
		{
			__m128 c = _mm_div_ps(mVector, v.mVector);
			__m128i i = _mm_cvttps_epi32(c);
			__m128 cTrunc = _mm_cvtepi32_ps(i);
			__m128 base = _mm_mul_ps(cTrunc, v.mVector);
			return _mm_sub_ps(mVector, base);
		}

		using common_op_type::operator+=;
		using common_op_type::operator-=;
		using common_op_type::operator/=;
		using common_op_type::operator*=;
		using common_op_type::operator%=;
		using common_op_type::operator|=;
		using common_op_type::operator&=;
		using common_op_type::operator^=;
		using common_op_type::operator>>=;
		using common_op_type::operator<<=;

		[[nodiscard]] vector operator+ (const value_type v) const noexcept
		{
			return _mm_add_ps(mVector, _mm_set_ps1(v));
		}

		[[nodiscard]] vector operator- (const value_type v) const noexcept
		{
			return _mm_sub_ps(mVector, _mm_set_ps1(v));
		}

		[[nodiscard]] vector operator* (const value_type v) const noexcept
		{
			return _mm_mul_ps(mVector, _mm_set_ps1(v));
		}

		[[nodiscard]] vector operator/ (const value_type v) const noexcept
		{
			return _mm_div_ps(mVector, _mm_set_ps1(v));
		}

		[[nodiscard]] vector operator% (const value_type v) const noexcept
		{
			__m128 vv = _mm_set_ps1(v);
			__m128 c = _mm_div_ps(mVector, vv);
			__m128i i = _mm_cvttps_epi32(c);
			__m128 cTrunc = _mm_cvtepi32_ps(i);
			__m128 base = _mm_mul_ps(cTrunc, vv);
			return _mm_sub_ps(mVector, base);
		}

		[[nodiscard]] vector operator== (vector v) const noexcept
		{
			return _mm_cmpeq_ps(mVector, v.mVector);
		}

		[[nodiscard]] vector operator!= (vector v) const noexcept
		{
			return _mm_cmpneq_ps(mVector, v.mVector);
		}

		[[nodiscard]] vector operator< (vector v) const noexcept
		{
			return _mm_cmplt_ps(mVector, v.mVector);
		}

		[[nodiscard]] vector operator<= (vector v) const noexcept
		{
			return _mm_cmple_ps(mVector, v.mVector);
		}

		[[nodiscard]] vector operator> (vector v) const noexcept
		{
			return _mm_cmpgt_ps(mVector, v.mVector);
		}

		[[nodiscard]] vector operator>= (vector v) const noexcept
		{
			return _mm_cmpge_ps(mVector, v.mVector);
		}

		[[nodiscard]] vector operator== (const value_type v) const noexcept
		{
			return _mm_cmpeq_ps(mVector, _mm_set_ps1(v));
		}

		[[nodiscard]] vector operator!= (const value_type v) const noexcept
		{
			return _mm_cmpneq_ps(mVector, _mm_set_ps1(v));
		}

		[[nodiscard]] vector operator< (const value_type v) const noexcept
		{
			return _mm_cmplt_ps(mVector, _mm_set_ps1(v));
		}

		[[nodiscard]] vector operator<= (const value_type v) const noexcept
		{
			return _mm_cmple_ps(mVector, _mm_set_ps1(v));
		}

		[[nodiscard]] vector operator> (const value_type v) const noexcept
		{
			return _mm_cmpgt_ps(mVector, _mm_set_ps1(v));
		}

		[[nodiscard]] vector operator>= (const value_type v) const noexcept
		{
			return _mm_cmpge_ps(mVector, _mm_set_ps1(v));
		}

		[[nodiscard]] vector operator! () const noexcept
		{
			return _mm_cmpeq_ps(mVector, _mm_setzero_ps());
		}

		[[nodiscard]] vector operator- () const noexcept
		{
			return inverse_sign();
		}

		[[nodiscard]] vector operator+ () const noexcept
		{
			return *this;
		}

		[[nodiscard]] vector operator~ () const noexcept
		{
			return _mm_andnot_ps(mVector, _mm_cmpeq_ps(mVector, mVector));
		}

		[[nodiscard]] vector operator& (vector v) const noexcept
		{
			return _mm_and_ps(mVector, v.mVector);
		}

		[[nodiscard]] vector operator| (vector v) const noexcept
		{
			return _mm_or_ps(mVector, v.mVector);
		}

		[[nodiscard]] vector operator^ (vector v) const noexcept
		{
			return _mm_xor_ps(mVector, v.mVector);
		}

		[[nodiscard]] vector operator& (const int v) const noexcept
		{
			return _mm_and_ps(mVector, _mm_castsi128_ps(_mm_set1_epi32(v)));
		}

		[[nodiscard]] vector operator| (const int v) const noexcept
		{
			return _mm_or_ps(mVector, _mm_castsi128_ps(_mm_set1_epi32(v)));
		}

		[[nodiscard]] vector operator^ (const int v) const noexcept
		{
			return _mm_xor_ps(mVector, _mm_castsi128_ps(_mm_set1_epi32(v)));
		}

		[[nodiscard]] vector operator>> (const int v) const noexcept
		{
			return _mm_castsi128_ps(_mm_srli_epi32(_mm_castps_si128(mVector), v));
		}

		[[nodiscard]] vector operator<< (const int v) const noexcept
		{
			return _mm_castsi128_ps(_mm_slli_epi32(_mm_castps_si128(mVector), v));
		}

		[[nodiscard]] vector operator>> (vector v) const noexcept
		{
			return _mm_castsi128_ps(_mm_srl_epi32(_mm_castps_si128(mVector), _mm_castps_si128(v.mVector)));
		}

		[[nodiscard]] vector operator<< (vector v) const noexcept
		{
			return _mm_castsi128_ps(_mm_sll_epi32(_mm_castps_si128(mVector), _mm_castps_si128(v.mVector)));
		}

		vector &operator++ () noexcept
			{ _mm_add_ps(mVector, _mm_set1_ps(1)); return *this; }

		vector &operator-- () noexcept
			{ _mm_sub_ps(mVector, _mm_set1_ps(1)); return *this; }

		vector operator++ (int) noexcept
			{ auto cp = *this; ++(*this); return cp; }

		vector operator-- (int) noexcept
			{ auto cp = *this; --(*this); return cp; }

		[[nodiscard]] vector blendv(vector v, vector mask) const noexcept
		#ifdef __SSE4_1__
			{ return _mm_blendv_ps(mVector, v.mVector, mask.mVector); }
		#else
			{
				auto res = this->get_array();
				auto va = v.get_array(), amask = mask.get_array();

				for(u32 i=0; i<NbValues; ++i)
					if(amask()[i])
						res()[i] = va()[i];

				return res;
			}
		#endif

		template <int mask>
		[[nodiscard]] vector blend(vector v) const noexcept
		#ifdef __SSE4_1__
			{ return _mm_blend_ps(mVector, v.mVector, mask); }
		#else
			{
				auto res = this->get_array();
				auto va = v.get_array();

				for(u32 i=0; i<NbValues; ++i)
					if(mask & (1<<i))
						res()[i] = va()[i];

				return res;
			}
		#endif

		[[nodiscard]] vector unpacklo(vector v) const noexcept
			{ return _mm_unpacklo_ps(mVector, v.mVector); }

		[[nodiscard]] vector unpackhi(vector v) const noexcept
			{ return _mm_unpackhi_ps(mVector, v.mVector); }

		[[nodiscard]] vector is_nan() const noexcept
			{ return _mm_cmpunord_ps(mVector, mVector); }

		[[nodiscard]] vector is_normal() const noexcept
			{ return ~(is_infinite() | is_nan() | (*this == vector{})); }

		[[nodiscard]] vector is_infinite() const noexcept
		{
			const __m128 INF = _mm_set_ps1(std::numeric_limits<value_type>::infinity());

			return _mm_cmpeq_ps(abs().mVector, INF);
		}

		[[nodiscard]] bool is_all_zero() const noexcept
		{
			return (!*this).move_mask() == 0xF;
		}

		[[nodiscard]] operator __m128() const noexcept
			{ return mVector; }

		//[[nodiscard]] explicit operator int() const noexcept
			//{ return move_mask(); }

		[[nodiscard]] auto mask() const noexcept
			{ return std::bitset<NbValues>{static_cast<std::make_unsigned_t<int>>(move_mask())}; }

		[[nodiscard]] explicit operator bool() const noexcept
			{ return mask().all(); }
	};



	template <typename Category_, typename Derived_>
	class vector_simd_128s_4_storage :
		public other_vector_storage<float, size_c<4>, types::simd_vector_t<types::simd_128_t>, void, Category_, Derived_>
	{
		PL_CRTP_BASE(Derived_)
		using storage_type = other_vector_storage<float, size_c<4>, types::simd_vector_t<types::simd_128_t>, void, Category_, Derived_>;
		using typename storage_type::storage_vector_type;

	public:
		using typename storage_type::value_type;
		using typename storage_type::category;
		using storage_type::v1;
		using storage_type::storage_type;

		vector_simd_128s_4_storage() = default;
		vector_simd_128s_4_storage(const __m128 v) noexcept : storage_type{v} {}
		vector_simd_128s_4_storage(const value_type v) noexcept : storage_type{storage_vector_type{v}} {}
		vector_simd_128s_4_storage(const value_type v, const value_type w) noexcept : storage_type{storage_vector_type{v, w}} {}
		vector_simd_128s_4_storage(const value_type x, const value_type y, const value_type z) noexcept : storage_type{storage_vector_type{x, y, z, 0}} {}
		vector_simd_128s_4_storage(const value_type x, const value_type y, const value_type z, const value_type w) noexcept : storage_type{storage_vector_type{x, y, z, w}} {}
	};

	template <typename Category_>
	class vector<float, size_c<4>, types::simd_vector_t<types::simd_128_t>, categories::geometric_t<Category_, 3>> :
		public vector_storage<float, size_c<4>, categories::geometric_t<Category_, 3>, vector_simd_128s_4_storage<categories::geometric_t<Category_, 3>, vector<float, size_c<4>, types::simd_vector_t<types::simd_128_t>, categories::geometric_t<Category_, 3>>>>,
		public vector_base<vector<float, size_c<4>, types::simd_vector_t<types::simd_128_t>, categories::geometric_t<Category_, 3>>>,
		public common_op<vector<float, size_c<4>, types::simd_vector_t<types::simd_128_t>, categories::geometric_t<Category_, 3>>>
	{
		using Base = vector_base<vector<float, size_c<4>, types::simd_vector_t<types::simd_128_t>, categories::geometric_t<Category_, 3>>>;
		using common_op_type = common_op<vector<float, size_c<4>, types::simd_vector_t<types::simd_128_t>, categories::geometric_t<Category_, 3>>>;
		using storage_type = vector_storage<float, size_c<4>, categories::geometric_t<Category_, 3>, vector_simd_128s_4_storage<categories::geometric_t<Category_, 3>, vector<float, size_c<4>, types::simd_vector_t<types::simd_128_t>, categories::geometric_t<Category_, 3>>>>;

	public:

		using impl_type = types::simd_vector_t<types::simd_128_t>;

		static constexpr auto LastValue = get_gvec_last_value<Category_, float>();
		static constinit const bool HardwareImpl = true;

		using typename storage_type::value_type;
		using typename storage_type::category;
		using storage_type::v1;
		using storage_type::storage_type;

		vector() = default;
		vector(const value_type v) noexcept : storage_type{v} {}
		vector(const value_type v) noexcept requires (!CVoid<Category_>) : storage_type{v, LastValue} {}
		vector(const value_type x, const value_type y, const value_type z) noexcept : storage_type{x, y, z, LastValue} {}
		vector(const tag::identity_t) noexcept { set_identity(); }

		template <typename OTag_>
			explicit(!is_valid_implicit_vector_conversion<4, categories::geometric_t<OTag_, 3>, 4, category>)
		vector(const vector<float, size_c<4>, types::simd_vector_t<types::simd_128_t>, categories::geometric_t<OTag_, 3>> &o) noexcept :
			storage_type{o.v1} {}


		vector &set_identity() noexcept
		{
			v1.set_identity(LastValue);
			return *this;
		}

		vector abs() const noexcept
		{
			return v1.abs();
		}

		[[nodiscard]] vector andnot(vector v) const noexcept
		{
			return v1.andnot(v.v1);
		}

		[[nodiscard]] vector next_after() const noexcept
		{
			return v1.next_after();
		}

		[[nodiscard]] value_type minimal_safe_increment() const noexcept
		{
			const auto vabs = abs();
			return (vabs.next_after() - vabs).max3();
		}

		[[nodiscard]] vector pow(const value_type power) const noexcept
		{
			return v1.pow(power);
		}

		[[nodiscard]] vector log() const noexcept
		{
			return v1.log();
		}

		[[nodiscard]] vector log2() const noexcept
		{
			return v1.log2();
		}

		template <int Degree_ = 5>
		[[nodiscard]] vector log2approx(std::integral_constant<int, Degree_> d = {}) const noexcept
		{
			return v1.log2approx(d);
		}

		[[nodiscard]] vector log10() const noexcept
		{
			return v1.log10();
		}

		[[nodiscard]] vector exp() const noexcept
		{
			return v1.exp();
		}

		[[nodiscard]] vector exp2() const noexcept
		{
			return v1.exp2();
		}

		template <int Degree_ = 3>
		[[nodiscard]] vector exp2approx(std::integral_constant<int, Degree_> d = {}) const noexcept
		{
			return v1.exp2approx(d);
		}

		[[nodiscard]] vector sin() const noexcept
		{
			return v1.sin();
		}

		[[nodiscard]] vector cos() const noexcept
		{
			return v1.cos();
		}

		[[nodiscard]] auto sincos() const noexcept
		{
			const auto r = v1.sincos();
			return sincos_res{vector{r.s}, vector{r.c}};
		}

		[[nodiscard]] vector sqrt() const noexcept
		{
			return v1.sqrt();
		}

		[[nodiscard]] vector sqrtapprox() const noexcept
		{
			return v1.sqrtapprox();
		}

		[[nodiscard]] vector sqrt_scalar() const noexcept
		{
			return v1.sqrt_scalar();
		}

		[[nodiscard]] vector sqrtapprox_scalar() const noexcept
		{
			return v1.sqrtapprox_scalar();
		}

		[[nodiscard]] vector recsqrt() const noexcept
		{
			return v1.recsqrt();
		}

		[[nodiscard]] vector recsqrtapprox() const noexcept
		{
			return v1.recsqrtapprox();
		}

		void reflect(vector fromDir) noexcept
		{
			__m128 tmp1;

			tmp1 = _mm_mul_ps(v1.mVector, fromDir.v1.mVector);
			tmp1 = _mm_hadd_ps(tmp1, tmp1);
			tmp1 = _mm_hadd_ps(tmp1, tmp1);

			v1.mVector = _mm_sub_ps(v1.mVector, _mm_mul_ps(fromDir.v1.mVector, _mm_add_ps(tmp1, tmp1)));
		}

		[[nodiscard]] vector rotate() const noexcept
		{
			// a b c d -> b c d a
			return vector{_mm_castsi128_ps(_mm_shuffle_epi32(_mm_castps_si128(v1.mVector), 0x39))};
		}

		[[nodiscard]] vector movehl(vector v) const noexcept
		{
			return v1.movehl(v.v1);
		}

		[[nodiscard]] vector movelh(vector v) const noexcept
		{
			return v1.movelh(v.v1);
		}

		[[nodiscard]] int move_mask() const noexcept
		{
			return v1.move_mask();
		}

		[[nodiscard]] vector normalize() const noexcept
		{
			return *this / dot_m(masked{size_v<0xFF>, *this}).sqrt();
		}

		[[nodiscard]] vector normalize_approx() const noexcept
		{
			return *this * dot_m(masked{size_v<0xFF>, *this}).recsqrtapprox();
		}

		[[nodiscard]] vector normalize(const value_type length) const noexcept
		{
			return vector{v1 / length};
		}

		[[nodiscard]] vector normalize_point() const noexcept
			requires (CPointVectorTag<category> || CNeutralGeometricVectorTag<category>)
		{
			return vector{v1 / v1.template shuffle<3, 3, 3, 3>()};
		}

		[[nodiscard]] auto as_point() const noexcept
		{
			using T = rebind_to_point<vector>;
			auto r = static_cast<T>(*this);

			r.set(1, size_v<3>);

			return r;
		}

		[[nodiscard]] auto as_dir() const noexcept
		{
			using T = rebind_to_dir<vector>;
			auto r = static_cast<T>(*this);

			r.set(0, size_v<3>);

			return r;
		}

		// http://fastcpp.blogspot.ca/2011/04/vector-cross-product-using-sse-code.html
		[[nodiscard]] vector cross(vector v) const noexcept
		{
			return vector{v1 * v.v1.template shuffle<1, 2, 0, 3>() - v1.template shuffle<1, 2, 0, 3>() * v.v1}.template shuffle<1, 2, 0, 3>();
		}

		template <int m0, int m1, int m2, int m3>
		[[nodiscard]] vector shuffle(vector v) const noexcept
		{
			return v1.template shuffle<m0, m1, m2, m3>(v.v1);
		}

		template <int m0, int m1, int m2, int m3>
		[[nodiscard]] vector shuffle() const noexcept
		{
			return v1.template shuffle<m0, m1, m2, m3>();
		}

		[[nodiscard]] value_type dot(vector v) const noexcept
		{
			return v1.dot(v.v1);
		}

		[[nodiscard]] value_type dot() const
		{
			return v1.dot();
		}

		template <szt mask = 0xFF>
		[[nodiscard]] vector dot(masked<mask, vector> v) const noexcept
		{
			return v1.dot(masked{size_v<mask>, v.v.v1});
		}

		template <szt mask = 0xFF>
		[[nodiscard]] vector dot_m(masked<mask, vector> v) const noexcept
		{
			return v1.dot_m(masked{size_v<mask>, v.v.v1});
		}

		[[nodiscard]] value_type length() const noexcept
		{
			return dot_m(masked{size_v<0xF1>, *this}).sqrt_scalar().scalar();
		}

		[[nodiscard]] value_type length_approx() const noexcept
		{
			return dot_m(masked{size_v<0xF1>, *this}).sqrtapprox_scalar().scalar();
		}

		[[nodiscard]] vector min(vector v) const noexcept
		{
			return v1.min(v.v1);
		}

		[[nodiscard]] value_type hmin() const noexcept
		{
			return v1.hmin();
		}

		[[nodiscard]] value_type min3() const noexcept
		{
			return v1.min3();
		}

		[[nodiscard]] vector min_scalar(vector v) const noexcept
		{
			return v1.min_scalar(v.v1);
		}

		[[nodiscard]] vector max_scalar(vector v) const noexcept
		{
			return v1.max_scalar(v.v1);
		}

		[[nodiscard]] vector max(vector v) const noexcept
		{
			return v1.max(v.v1);
		}

		[[nodiscard]] value_type hmax() const noexcept
		{
			return v1.hmax();
		}

		[[nodiscard]] value_type max3() const noexcept
		{
			return v1.max3();
		}

		[[nodiscard]] vector floor() const noexcept
		{
			return v1.floor();
		}

		[[nodiscard]] vector ceil() const noexcept
		{
			return v1.ceil();
		}

		[[nodiscard]] vector round() const noexcept
		{
			return v1.round();
		}

		[[nodiscard]] szt max3_index() const noexcept
		{
			auto v = v1.get_array();

			value_type maxVal = v()[0];
			szt maxIndex = 0;

			if(v()[1] > maxVal)
			{
				maxIndex = 1;
				maxVal = v()[1];
			}

			if(v()[2] > maxVal)
				maxIndex = 2;

			return maxIndex;
		}

		[[nodiscard]] szt min3_index() const noexcept
		{
			auto v = v1.get_array();

			value_type minVal = v()[0];
			szt minIndex = 0;

			if(v()[1] < minVal)
			{
				minIndex = 1;
				minVal = v()[1];
			}

			if(v()[2] < minVal)
				minIndex = 2;

			return minIndex;
		}

		[[nodiscard]] szt abs_min3_index() const noexcept
		{
			auto v = v1.get_array();

			value_type minVal = plabs(v()[0]), cVal = plabs(v()[1]);
			szt minIndex = 0;

			if(cVal < minVal)
			{
				minIndex = 1;
				minVal = cVal;
			}

			if(plabs(v()[2]) < minVal)
				minIndex = 2;

			return minIndex;
		}

		[[nodiscard]] szt abs_max3_index() const noexcept
		{
			auto v = v1.get_array();

			value_type maxVal = plabs(v()[0]), cVal = plabs(v()[1]);
			szt maxIndex = 0;

			if(cVal > maxVal)
			{
				maxIndex = 1;
				maxVal = cVal;
			}

			if(plabs(v()[2]) > maxVal)
				maxIndex = 2;

			return maxIndex;
		}

		[[nodiscard]] vector inverse_sign() const noexcept
		{
			return v1.inverse_sign();
		}

		[[nodiscard]] vector reciprocal() const noexcept
		{
			return v1.reciprocal();
		}

		[[nodiscard]] vector reciprocalapprox() const noexcept
		{
			return v1.reciprocalapprox();
		}

		[[nodiscard]] vector hmul() const noexcept
		{
			return v1.hmul();
		}

		[[nodiscard]] vector hadd() const noexcept
		{
			return v1.hadd();
		}

		[[nodiscard]] vector hsub() const noexcept
		{
			return v1.hsub();
		}

		[[nodiscard]] vector find_orthogonal() const noexcept
		{
			auto ov = this->get_array();
			const auto ovb = ov;

			const auto maxIndex = abs_max3_index(), nextIndex = maxIndex == 2 ? 0 : maxIndex + 1;

			ov()[nextIndex] = ovb()[maxIndex];
			ov()[maxIndex] = -ovb()[nextIndex];

			const auto orthogonal = cross(vector{ov});

			return orthogonal;
		}

		[[nodiscard]] int cmp_scalar_ge(vector v) const noexcept
		{
			return _mm_comige_ss(v1.mVector, v.v1.mVector);
		}

		[[nodiscard]] int cmp_scalar_gt(vector v) const noexcept
		{
			return _mm_comigt_ss(v1.mVector, v.v1.mVector);
		}

		[[nodiscard]] int cmp_scalar_eq(vector v) const noexcept
		{
			return _mm_comieq_ss(v1.mVector, v.v1.mVector);
		}

		[[nodiscard]] int cmp_scalar_le(vector v) const noexcept
		{
			return _mm_comile_ss(v1.mVector, v.v1.mVector);
		}

		[[nodiscard]] int cmp_scalar_lt(vector v) const noexcept
		{
			return _mm_comilt_ss(v1.mVector, v.v1.mVector);
		}

		[[nodiscard]] vector is_nan() const noexcept
		{
			return v1.is_nan();
		}

		[[nodiscard]] vector is_infinite() const noexcept
		{
			return v1.is_infinite();
		}

		[[nodiscard]] vector is_normal() const noexcept
			{ return v1.is_normal(); }

		[[nodiscard]] bool is_all_zero() const noexcept
		{
			return v1.is_all_zero();
		}

		using common_op_type::operator+=;
		using common_op_type::operator-=;
		using common_op_type::operator/=;
		using common_op_type::operator*=;
		using common_op_type::operator%=;
		using common_op_type::operator|=;
		using common_op_type::operator&=;
		using common_op_type::operator^=;
		using common_op_type::operator>>=;
		using common_op_type::operator<<=;

		[[nodiscard]] vector operator+ (vector v) const noexcept
		{
			return vector{v1 + v.v1};
		}

		[[nodiscard]] vector operator- (vector v) const noexcept
		{
			return vector{v1 - v.v1};
		}

		[[nodiscard]] vector operator* (vector v) const noexcept
		{
			return vector{v1 * v.v1};
		}

		[[nodiscard]] vector operator/ (vector v) const noexcept
		{
			return vector{v1 / v.v1};
		}

		[[nodiscard]] vector operator% (vector v) const noexcept
		{
			return vector{v1 % v.v1};
		}

		[[nodiscard]] vector operator+ (const value_type &v) const noexcept
		{
			return vector{v1 + v};
		}

		[[nodiscard]] vector operator- (const value_type &v) const noexcept
		{
			return vector{v1 - v};
		}

		[[nodiscard]] vector operator* (const value_type &v) const noexcept
		{
			return vector{v1 * v};
		}

		[[nodiscard]] vector operator/ (const value_type &v) const noexcept
		{
			return vector{v1 / v};
		}

		[[nodiscard]] vector operator% (const value_type &v) const noexcept
		{
			return vector{v1 % v};
		}

		[[nodiscard]] vector operator== (vector v) const noexcept
		{
			return vector{v1 == v.v1};
		}

		[[nodiscard]] vector operator!= (vector v) const noexcept
		{
			return vector{v1 != v.v1};
		}

		[[nodiscard]] vector operator< (vector v) const noexcept
		{
			return vector{v1 < v.v1};
		}

		[[nodiscard]] vector operator<= (vector v) const noexcept
		{
			return vector{v1 <= v.v1};
		}

		[[nodiscard]] vector operator> (vector v) const noexcept
		{
			return vector{v1 > v.v1};
		}

		[[nodiscard]] vector operator>= (vector v) const noexcept
		{
			return vector{v1 >= v.v1};
		}

		[[nodiscard]] vector operator== (const value_type v) const noexcept
		{
			return v1 == v;
		}

		[[nodiscard]] vector operator!= (const value_type v) const noexcept
		{
			return v1 != v;
		}

		[[nodiscard]] vector operator< (const value_type v) const noexcept
		{
			return v1 < v;
		}

		[[nodiscard]] vector operator<= (const value_type v) const noexcept
		{
			return v1 <= v;
		}

		[[nodiscard]] vector operator> (const value_type v) const noexcept
		{
			return v1 > v;
		}

		[[nodiscard]] vector operator>= (const value_type v) const noexcept
		{
			return v1 >= v;
		}

		[[nodiscard]] vector operator! () const noexcept
		{
			return vector{!v1};
		}

		[[nodiscard]] vector operator- () const noexcept
		{
			return vector{-v1};
		}

		[[nodiscard]] vector operator+ () const noexcept
		{
			return vector{+v1};
		}

		[[nodiscard]] vector operator& (vector v) const noexcept
		{
			return vector{v1 & v.v1};
		}

		[[nodiscard]] vector operator| (vector v) const noexcept
		{
			return vector{v1 | v.v1};
		}

		[[nodiscard]] vector operator^ (vector v) const noexcept
		{
			return vector{v1 ^ v.v1};
		}

		[[nodiscard]] vector operator& (const int v) const noexcept
		{
			return v1 & v;
		}

		[[nodiscard]] vector operator| (const int v) const noexcept
		{
			return v1 | v;
		}

		[[nodiscard]] vector operator^ (const int v) const noexcept
		{
			return v1 ^ v;
		}

		[[nodiscard]] vector operator>> (const int v) const noexcept
		{
			return v1 >> v;
		}

		[[nodiscard]] vector operator<< (const int v) const noexcept
		{
			return v1 << v;
		}

		[[nodiscard]] vector operator>> (vector v) const noexcept
		{
			return vector{v1 >> v.v1};
		}

		[[nodiscard]] vector operator<< (vector v) const noexcept
		{
			return vector{v1 << v.v1};
		}

		vector &operator++ () noexcept
			{ ++v1; return *this; }

		vector &operator-- () noexcept
			{ --v1; return *this; }

		vector operator++ (int) noexcept
			{ return v1++; }

		vector operator-- (int) noexcept
			{ return v1--; }

		[[nodiscard]] vector operator~ () const noexcept
		{
			return ~v1;
		}

		[[nodiscard]] vector blendv(vector v, vector mask) const noexcept
			{ return vector{v1.blendv(v.v1, mask.v1)}; }

		template <int mask>
		[[nodiscard]] vector blend(vector v) const noexcept
			{ return vector{v1.template blend<mask>(v.v1)}; }

		[[nodiscard]] vector unpacklo(vector v) const noexcept
			{ return vector{v1.unpacklo(v.v1)}; }

		[[nodiscard]] vector unpackhi(vector v) const noexcept
			{ return vector{v1.unpackhi(v.v1)}; }

		//[[nodiscard]] explicit operator int() const noexcept
			//{ return (int)v1; }

		[[nodiscard]] auto mask() const noexcept
			{ return v1.mask(); }

		[[nodiscard]] explicit operator bool() const noexcept
			{ return static_cast<bool>(v1); }
	};



	template <>
	class vector<float, size_c<4>, types::simd_vector_t<types::simd_128_t>, categories::color_t> :
		public vector_storage<float, size_c<4>, categories::color_t, vector_simd_128s_4_storage<categories::color_t, vector<float, size_c<4>, types::simd_vector_t<types::simd_128_t>, categories::color_t>>>,
		public vector_base<vector<float, size_c<4>, types::simd_vector_t<types::simd_128_t>, categories::color_t>>,
		public common_op<vector<float, size_c<4>, types::simd_vector_t<types::simd_128_t>, categories::color_t>>
	{
		using Base = vector_base<vector<float, size_c<4>, types::simd_vector_t<types::simd_128_t>, categories::color_t>>;
		using common_op_type = common_op<vector<float, size_c<4>, types::simd_vector_t<types::simd_128_t>, categories::color_t>>;
		using storage_type = vector_storage<float, size_c<4>, categories::color_t, vector_simd_128s_4_storage<categories::color_t, vector<float, size_c<4>, types::simd_vector_t<types::simd_128_t>, categories::color_t>>>;

	public:

		using impl_type = types::simd_vector_t<types::simd_128_t>;

		static constexpr auto LastValue = 1;
		static constinit const bool HardwareImpl = true;

		using typename storage_type::value_type;
		using typename storage_type::category;
		using storage_type::v1;
		using storage_type::storage_type;

		vector() = default;
		vector(const value_type v) noexcept : storage_type{v} {}
		vector(const value_type x, const value_type y, const value_type z) noexcept : storage_type{x, y, z, LastValue} {}
		vector(const tag::identity_t) noexcept { set_identity(); }


		vector &set_identity() noexcept
		{
			v1.set_identity(LastValue);
			return *this;
		}

		vector abs() const noexcept
		{
			return v1.abs();
		}

		[[nodiscard]] vector andnot(vector v) const noexcept
		{
			return v1.andnot(v.v1);
		}

		[[nodiscard]] vector next_after() const noexcept
		{
			return v1.next_after();
		}

		[[nodiscard]] value_type minimal_safe_increment() const noexcept
		{
			const auto vabs = abs();
			return (vabs.next_after() - vabs).max3();
		}

		[[nodiscard]] vector pow(const value_type power) const noexcept
		{
			return v1.pow(power);
		}

		[[nodiscard]] vector log() const noexcept
		{
			return v1.log();
		}

		[[nodiscard]] vector log2() const noexcept
		{
			return v1.log2();
		}

		template <int Degree_ = 5>
		[[nodiscard]] vector log2approx(std::integral_constant<int, Degree_> d = {}) const noexcept
		{
			return v1.log2approx(d);
		}

		[[nodiscard]] vector log10() const noexcept
		{
			return v1.log10();
		}

		[[nodiscard]] vector exp() const noexcept
		{
			return v1.exp();
		}

		[[nodiscard]] vector exp2() const noexcept
		{
			return v1.exp2();
		}

		template <int Degree_ = 3>
		[[nodiscard]] vector exp2approx(std::integral_constant<int, Degree_> d = {}) const noexcept
		{
			return v1.exp2approx(d);
		}

		[[nodiscard]] vector sin() const noexcept
		{
			return v1.sin();
		}

		[[nodiscard]] vector cos() const noexcept
		{
			return v1.cos();
		}

		[[nodiscard]] auto sincos() const noexcept
		{
			const auto r = v1.sincos();
			return sincos_res{vector{r.s}, vector{r.c}};
		}

		[[nodiscard]] vector sqrt() const noexcept
		{
			return v1.sqrt();
		}

		[[nodiscard]] vector sqrtapprox() const noexcept
		{
			return v1.sqrtapprox();
		}

		[[nodiscard]] vector sqrt_scalar() const noexcept
		{
			return v1.sqrt_scalar();
		}

		[[nodiscard]] vector sqrtapprox_scalar() const noexcept
		{
			return v1.sqrtapprox_scalar();
		}

		[[nodiscard]] vector recsqrt() const noexcept
		{
			return v1.recsqrt();
		}

		[[nodiscard]] vector recsqrtapprox() const noexcept
		{
			return v1.recsqrtapprox();
		}

		[[nodiscard]] vector rotate() const noexcept
		{
			// a b c d -> b c d a
			return vector{_mm_castsi128_ps(_mm_shuffle_epi32(_mm_castps_si128(v1.mVector), 0x39))};
		}

		[[nodiscard]] vector movehl(vector v) const noexcept
		{
			return v1.movehl(v.v1);
		}

		[[nodiscard]] vector movelh(vector v) const noexcept
		{
			return v1.movelh(v.v1);
		}

		[[nodiscard]] int move_mask() const noexcept
		{
			return v1.move_mask();
		}

		[[nodiscard]] vector normalize() const noexcept
		{
			return *this / dot_m(masked{size_v<0xFF>, *this}).sqrt();
		}

		[[nodiscard]] vector normalize_approx() const noexcept
		{
			return *this * dot_m(masked{size_v<0xFF>, *this}).recsqrtapprox();
		}

		[[nodiscard]] vector normalize(const value_type length) const noexcept
		{
			return vector{v1 / length};
		}

		// http://fastcpp.blogspot.ca/2011/04/vector-cross-product-using-sse-code.html
		[[nodiscard]] vector cross(vector v) const noexcept
		{
			return vector{v1 * v.v1.template shuffle<1, 2, 0, 3>() - v1.template shuffle<1, 2, 0, 3>() * v.v1}.template shuffle<1, 2, 0, 3>();
		}

		template <int m0, int m1, int m2, int m3>
		[[nodiscard]] vector shuffle(vector v) const noexcept
		{
			return v1.template shuffle<m0, m1, m2, m3>(v.v1);
		}

		template <int m0, int m1, int m2, int m3>
		[[nodiscard]] vector shuffle() const noexcept
		{
			return v1.template shuffle<m0, m1, m2, m3>();
		}

		[[nodiscard]] value_type dot(vector v) const noexcept
		{
			return v1.dot(v.v1);
		}

		[[nodiscard]] value_type dot() const
		{
			return v1.dot();
		}

		template <szt mask = 0xFF>
		[[nodiscard]] vector dot(masked<mask, vector> v) const noexcept
		{
			return v1.dot(masked{size_v<mask>, v.v.v1});
		}

		template <szt mask = 0xFF>
		[[nodiscard]] vector dot_m(masked<mask, vector> v) const noexcept
		{
			return v1.dot_m(masked{size_v<mask>, v.v.v1});
		}

		[[nodiscard]] value_type length() const noexcept
		{
			return dot_m(masked{size_v<0xF1>, *this}).sqrt_scalar().scalar();
		}

		[[nodiscard]] value_type length_approx() const noexcept
		{
			return dot_m(masked{size_v<0xF1>, *this}).sqrtapprox_scalar().scalar();
		}

		[[nodiscard]] vector min(vector v) const noexcept
		{
			return v1.min(v.v1);
		}

		[[nodiscard]] value_type hmin() const noexcept
		{
			return v1.hmin();
		}

		[[nodiscard]] value_type min3() const noexcept
		{
			return v1.min3();
		}

		[[nodiscard]] vector min_scalar(vector v) const noexcept
		{
			return v1.min_scalar(v.v1);
		}

		[[nodiscard]] vector max_scalar(vector v) const noexcept
		{
			return v1.max_scalar(v.v1);
		}

		[[nodiscard]] vector max(vector v) const noexcept
		{
			return v1.max(v.v1);
		}

		[[nodiscard]] value_type hmax() const noexcept
		{
			return v1.hmax();
		}

		[[nodiscard]] value_type max3() const noexcept
		{
			return v1.max3();
		}

		[[nodiscard]] vector floor() const noexcept
		{
			return v1.floor();
		}

		[[nodiscard]] vector ceil() const noexcept
		{
			return v1.ceil();
		}

		[[nodiscard]] vector round() const noexcept
		{
			return v1.round();
		}

		[[nodiscard]] szt max3_index() const noexcept
		{
			auto v = v1.get_array();

			value_type maxVal = v()[0];
			szt maxIndex = 0;

			if(v()[1] > maxVal)
			{
				maxIndex = 1;
				maxVal = v()[1];
			}

			if(v()[2] > maxVal)
				maxIndex = 2;

			return maxIndex;
		}

		[[nodiscard]] szt min3_index() const noexcept
		{
			auto v = v1.get_array();

			value_type minVal = v()[0];
			szt minIndex = 0;

			if(v()[1] < minVal)
			{
				minIndex = 1;
				minVal = v()[1];
			}

			if(v()[2] < minVal)
				minIndex = 2;

			return minIndex;
		}

		[[nodiscard]] szt abs_min3_index() const noexcept
		{
			auto v = v1.get_array();

			value_type minVal = plabs(v()[0]), cVal = plabs(v()[1]);
			szt minIndex = 0;

			if(cVal < minVal)
			{
				minIndex = 1;
				minVal = cVal;
			}

			if(plabs(v()[2]) < minVal)
				minIndex = 2;

			return minIndex;
		}

		[[nodiscard]] szt abs_max3_index() const noexcept
		{
			auto v = v1.get_array();

			value_type maxVal = plabs(v()[0]), cVal = plabs(v()[1]);
			szt maxIndex = 0;

			if(cVal > maxVal)
			{
				maxIndex = 1;
				maxVal = cVal;
			}

			if(plabs(v()[2]) > maxVal)
				maxIndex = 2;

			return maxIndex;
		}

		[[nodiscard]] vector inverse_sign() const noexcept
		{
			return v1.inverse_sign();
		}

		[[nodiscard]] vector reciprocal() const noexcept
		{
			return v1.reciprocal();
		}

		[[nodiscard]] vector reciprocalapprox() const noexcept
		{
			return v1.reciprocalapprox();
		}

		[[nodiscard]] vector hmul() const noexcept
		{
			return v1.hmul();
		}

		[[nodiscard]] vector hadd() const noexcept
		{
			return v1.hadd();
		}

		[[nodiscard]] vector hsub() const noexcept
		{
			return v1.hsub();
		}

		[[nodiscard]] vector find_orthogonal() const noexcept
		{
			auto ov = this->get_array();
			const auto ovb = ov;

			const auto maxIndex = abs_max3_index(), nextIndex = maxIndex == 2 ? 0 : maxIndex + 1;

			ov()[nextIndex] = ovb()[maxIndex];
			ov()[maxIndex] = -ovb()[nextIndex];

			const auto orthogonal = cross(vector{ov});

			return orthogonal;
		}

		[[nodiscard]] int cmp_scalar_ge(vector v) const noexcept
		{
			return _mm_comige_ss(v1.mVector, v.v1.mVector);
		}

		[[nodiscard]] int cmp_scalar_gt(vector v) const noexcept
		{
			return _mm_comigt_ss(v1.mVector, v.v1.mVector);
		}

		[[nodiscard]] int cmp_scalar_eq(vector v) const noexcept
		{
			return _mm_comieq_ss(v1.mVector, v.v1.mVector);
		}

		[[nodiscard]] int cmp_scalar_le(vector v) const noexcept
		{
			return _mm_comile_ss(v1.mVector, v.v1.mVector);
		}

		[[nodiscard]] int cmp_scalar_lt(vector v) const noexcept
		{
			return _mm_comilt_ss(v1.mVector, v.v1.mVector);
		}

		[[nodiscard]] vector is_nan() const noexcept
		{
			return v1.is_nan();
		}

		[[nodiscard]] vector is_infinite() const noexcept
		{
			return v1.is_infinite();
		}

		[[nodiscard]] vector is_normal() const noexcept
			{ return v1.is_normal(); }

		[[nodiscard]] bool is_all_zero() const noexcept
		{
			return v1.is_all_zero();
		}

		using common_op_type::operator+=;
		using common_op_type::operator-=;
		using common_op_type::operator/=;
		using common_op_type::operator*=;
		using common_op_type::operator%=;
		using common_op_type::operator|=;
		using common_op_type::operator&=;
		using common_op_type::operator^=;
		using common_op_type::operator>>=;
		using common_op_type::operator<<=;

		[[nodiscard]] vector operator+ (vector v) const noexcept
		{
			return vector{v1 + v.v1};
		}

		[[nodiscard]] vector operator- (vector v) const noexcept
		{
			return vector{v1 - v.v1};
		}

		[[nodiscard]] vector operator* (vector v) const noexcept
		{
			return vector{v1 * v.v1};
		}

		[[nodiscard]] vector operator/ (vector v) const noexcept
		{
			return vector{v1 / v.v1};
		}

		[[nodiscard]] vector operator% (vector v) const noexcept
		{
			return vector{v1 % v.v1};
		}

		[[nodiscard]] vector operator+ (const value_type &v) const noexcept
		{
			return vector{v1 + v};
		}

		[[nodiscard]] vector operator- (const value_type &v) const noexcept
		{
			return vector{v1 - v};
		}

		[[nodiscard]] vector operator* (const value_type &v) const noexcept
		{
			return vector{v1 * v};
		}

		[[nodiscard]] vector operator/ (const value_type &v) const noexcept
		{
			return vector{v1 / v};
		}

		[[nodiscard]] vector operator% (const value_type &v) const noexcept
		{
			return vector{v1 % v};
		}

		[[nodiscard]] vector operator== (vector v) const noexcept
		{
			return vector{v1 == v.v1};
		}

		[[nodiscard]] vector operator!= (vector v) const noexcept
		{
			return vector{v1 != v.v1};
		}

		[[nodiscard]] vector operator< (vector v) const noexcept
		{
			return vector{v1 < v.v1};
		}

		[[nodiscard]] vector operator<= (vector v) const noexcept
		{
			return vector{v1 <= v.v1};
		}

		[[nodiscard]] vector operator> (vector v) const noexcept
		{
			return vector{v1 > v.v1};
		}

		[[nodiscard]] vector operator>= (vector v) const noexcept
		{
			return vector{v1 >= v.v1};
		}

		[[nodiscard]] vector operator== (const value_type v) const noexcept
		{
			return v1 == v;
		}

		[[nodiscard]] vector operator!= (const value_type v) const noexcept
		{
			return v1 != v;
		}

		[[nodiscard]] vector operator< (const value_type v) const noexcept
		{
			return v1 < v;
		}

		[[nodiscard]] vector operator<= (const value_type v) const noexcept
		{
			return v1 <= v;
		}

		[[nodiscard]] vector operator> (const value_type v) const noexcept
		{
			return v1 > v;
		}

		[[nodiscard]] vector operator>= (const value_type v) const noexcept
		{
			return v1 >= v;
		}

		[[nodiscard]] vector operator! () const noexcept
		{
			return vector{!v1};
		}

		[[nodiscard]] vector operator- () const noexcept
		{
			return vector{-v1};
		}

		[[nodiscard]] vector operator+ () const noexcept
		{
			return vector{+v1};
		}

		[[nodiscard]] vector operator& (vector v) const noexcept
		{
			return vector{v1 & v.v1};
		}

		[[nodiscard]] vector operator| (vector v) const noexcept
		{
			return vector{v1 | v.v1};
		}

		[[nodiscard]] vector operator^ (vector v) const noexcept
		{
			return vector{v1 ^ v.v1};
		}

		[[nodiscard]] vector operator& (const int v) const noexcept
		{
			return v1 & v;
		}

		[[nodiscard]] vector operator| (const int v) const noexcept
		{
			return v1 | v;
		}

		[[nodiscard]] vector operator^ (const int v) const noexcept
		{
			return v1 ^ v;
		}

		[[nodiscard]] vector operator>> (const int v) const noexcept
		{
			return v1 >> v;
		}

		[[nodiscard]] vector operator<< (const int v) const noexcept
		{
			return v1 << v;
		}

		[[nodiscard]] vector operator>> (vector v) const noexcept
		{
			return vector{v1 >> v.v1};
		}

		[[nodiscard]] vector operator<< (vector v) const noexcept
		{
			return vector{v1 << v.v1};
		}

		vector &operator++ () noexcept
			{ ++v1; return *this; }

		vector &operator-- () noexcept
			{ --v1; return *this; }

		vector operator++ (int) noexcept
			{ return v1++; }

		vector operator-- (int) noexcept
			{ return v1--; }

		[[nodiscard]] vector operator~ () const noexcept
		{
			return ~v1;
		}

		[[nodiscard]] vector blendv(vector v, vector mask) const noexcept
			{ return vector{v1.blendv(v.v1, mask.v1)}; }

		template <int mask>
		[[nodiscard]] vector blend(vector v) const noexcept
			{ return vector{v1.template blend<mask>(v.v1)}; }

		[[nodiscard]] vector unpacklo(vector v) const noexcept
			{ return vector{v1.unpacklo(v.v1)}; }

		[[nodiscard]] vector unpackhi(vector v) const noexcept
			{ return vector{v1.unpackhi(v.v1)}; }

		//[[nodiscard]] explicit operator int() const noexcept
			//{ return (int)v1; }

		[[nodiscard]] auto mask() const noexcept
			{ return v1.mask(); }

		[[nodiscard]] explicit operator bool() const noexcept
			{ return static_cast<bool>(v1); }
	};
}
