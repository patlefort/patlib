/*
	PatLib

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "base.hpp"

#ifndef PL_LIB_VECTOR_256_SIMD
	#define PL_LIB_VECTOR_256_SIMD
#endif

#ifndef PL_LIB_VECTOR_256_FLOAT_SIMD
	#define PL_LIB_VECTOR_256_FLOAT_SIMD
#endif

#ifndef PL_LIB_VECTOR_FLOAT_SIMD
	#define PL_LIB_VECTOR_FLOAT_SIMD
#endif

namespace patlib::simd::impl
{
	struct wrapped__m256 { using type = __m256; };

	template <typename Category_, typename Derived_>
	class vector_simd_256s_storage :
		public vector_simd_storage_base<float, wrapped__m256, 8, Category_>,
		public iteratable<float, 8, vector_simd_256s_storage<Category_, Derived_>>
	{
		PL_CRTP_BASE(Derived_)
		using storage_type = vector_simd_storage_base<float, wrapped__m256, 8, Category_>;

	public:
		static constinit const auto NbValues = storage_type::NbValues;

		using typename storage_type::value_type;
		using typename storage_type::aligned_array_type;
		using storage_type::mVector;
		using storage_type::storage_type;

		vector_simd_256s_storage() = default;
		vector_simd_256s_storage(const value_type v) noexcept : storage_type{ _mm256_set1_ps(v) } {}
		vector_simd_256s_storage(const value_type v, const value_type w) noexcept : storage_type{ _mm256_set_ps(w, v, v, v, v, v, v, v) } {}
		PL_VECTOR_VARCTOR(vector_simd_256s_storage, _mm256_set_ps)
		explicit vector_simd_256s_storage(const tag::array_init_t, const value_type *v, tag::aligned_init_t = {}) noexcept : storage_type{ _mm256_load_ps(v) } {}
		explicit vector_simd_256s_storage(const tag::array_init_t, const value_type *v, tag::unaligned_init_t) noexcept : storage_type{ _mm256_loadu_ps(v) } {}
		vector_simd_256s_storage(const aligned_array_type &v) noexcept : storage_type{ _mm256_load_ps(v().data()) } {}

		vector_simd_256s_storage(const tag::zero_t) noexcept : storage_type{{}} {}
		vector_simd_256s_storage(const tag::identity_t, const value_type last = 1) noexcept { set_identity(last); }
		vector_simd_256s_storage(const tag::allone_t) noexcept { set_all_one(); }
		vector_simd_256s_storage(const tag::one_t) noexcept { set_one(); }
		vector_simd_256s_storage(const tag::two_t) noexcept { set_two(); }
		vector_simd_256s_storage(const tag::half_t) noexcept { set_half(); }
		vector_simd_256s_storage(const tag::nan_t) noexcept { set_nan(); }

		[[nodiscard]] value_type operator[] (const szt index) const noexcept
		{
			#if BOOST_COMP_MSVC
				return mVector.m256_f32[index];
			#else
				return get_array()()[index];
			#endif
		}

		template <szt index>
		[[nodiscard]] value_type operator[] (size_c<index>) const noexcept
		{
			if constexpr(index > 3)
			{
				auto v = _mm256_shuffle_ps(mVector, mVector, _MM_SHUFFLE(0, 0, 0, index - 4));
				v = _mm256_permute2f128_ps(v, v, 1);
				return _mm_cvtss_f32(_mm256_castps256_ps128(v));
			}
			else if constexpr(index == 0)
			{
				return _mm_cvtss_f32(_mm256_castps256_ps128(mVector));
			}
			else
			{
				auto v = _mm256_shuffle_ps(mVector, mVector, _MM_SHUFFLE(0, 0, 0, index));
				return _mm_cvtss_f32(_mm256_castps256_ps128(v));
			}
		}

		auto &set_zero() noexcept
		{
			mVector = _mm256_setzero_ps();
			return derived();
		}

		auto &set(const value_type v) noexcept
		{
			mVector = _mm256_set1_ps(v);
			return derived();
		}

		auto &set_all_one() noexcept
		{
			#ifdef __AVX2__
				mVector = _mm256_castsi256_ps(_mm256_cmpeq_epi32(_mm256_castps_si256(mVector), _mm256_castps_si256(mVector)));
			#else
				mVector = _mm256_castsi256_ps(_mm256_set1_epi32(~u32(0)));
			#endif

			return derived();
		}

		auto &set_one() noexcept
		{
			#ifdef __AVX2__
				mVector = _mm256_castsi256_ps( simd::AvxMathfun::mf_mm256_srli_epi32(simd::AvxMathfun::mf_mm256_slli_epi32(_mm256_cmpeq_epi32(_mm256_castps_si256(mVector), _mm256_castps_si256(mVector)), 25), 2) );
			#else
				mVector = _mm256_set1_ps(1.0);
			#endif

			return derived();
		}

		auto &set_half() noexcept
		{
			#ifdef __AVX2__
				mVector = _mm256_castsi256_ps( simd::AvxMathfun::mf_mm256_srli_epi32(simd::AvxMathfun::mf_mm256_slli_epi32(_mm256_cmpeq_epi32(_mm256_castps_si256(mVector), _mm256_castps_si256(mVector)), 26), 2) );
			#else
				mVector = _mm256_set1_ps(0.5);
			#endif

			return derived();
		}

		auto &set_two() noexcept
		{
			#ifdef __AVX2__
				mVector = _mm256_castsi256_ps( simd::AvxMathfun::mf_mm256_srli_epi32(simd::AvxMathfun::mf_mm256_slli_epi32(_mm256_cmpeq_epi32(_mm256_castps_si256(mVector), _mm256_castps_si256(mVector)), 31), 1) );
			#else
				mVector = _mm256_set1_ps(2.0);
			#endif

			return derived();
		}

		auto &set_nan() noexcept
		{
			return (derived() = std::numeric_limits<value_type>::quiet_NaN());
		}

		auto &set_all(const value_type a, const value_type b, const value_type c, const value_type d, const value_type e, const value_type f, const value_type g, const value_type h) noexcept
		{
			mVector = _mm256_set_ps(a, b, c, d, e, f, g, h);
			return derived();
		}

		auto &set(const value_type *v) noexcept
		{
			mVector = _mm256_load_ps(v);
			return derived();
		}

		auto &set(const aligned_array_type &v) noexcept
		{
			mVector = _mm256_load_ps(v().data());
			return derived();
		}

		auto &set_unaligned(const value_type *v) noexcept
		{
			mVector = _mm256_loadu_ps(v);
			return derived();
		}

		template <szt index>
		auto &set(const value_type v, size_c<index> = {}) noexcept
		{
			mVector = _mm256_blend_ps(mVector, _mm256_set1_ps(v), 1 << index);

			return derived();
		}

		void get_all(value_type *v) const noexcept
		{
			_mm256_storeu_ps(v, mVector);
		}

		void get_all_aligned(value_type *v) const noexcept
		{
			_mm256_store_ps(v, mVector);
		}

		[[nodiscard]] aligned_array_type get_array() const noexcept
		{
			aligned_array_type a; get_all_aligned(a().data());
			return a;
		}

		auto &set(const value_type v, const int index) noexcept
		{
			//_mm256_insert_ps(mVector, _mm256_set_ss(v), index);
			//((value_type *)&mVector)[index] = v;
			auto va = get_array();
			va()[index] = v;
			set(va);

			return derived();
		}

		auto &set_identity(const value_type last = 1) noexcept
		{
			mVector = _mm256_setzero_ps();
			//XMM_INDEX_SET_FLOAT(mVector, 3, 1.0f);
			//((value_type *)&mVector)[7] = 1.0f;
			//_mm256_insert_ps(mVector, _mm256_set1_ps(1.0f), 3);
			set(last, size_v<7>);

			return derived();
		}

		void scalar(value_type &dest) noexcept
		{
			_mm_store_ss(&dest, _mm256_extractf128_ps(mVector, 0));
		}

		[[nodiscard]] value_type scalar() const noexcept
		{
			return _mm_cvtss_f32(_mm256_extractf128_ps(mVector, 0));
		}
	};

	template <typename Category_>
	class vector<float, size_c<8>, types::simd_vector_t<types::simd_256_t>, Category_> :
		public vector_storage<float, size_c<8>, Category_, vector_simd_256s_storage<Category_, vector<float, size_c<8>, types::simd_vector_t<types::simd_256_t>, Category_>>>,
		public vector_base<vector<float, size_c<8>, types::simd_vector_t<types::simd_256_t>, Category_>>,
		public common_op<vector<float, size_c<8>, types::simd_vector_t<types::simd_256_t>, Category_>>
	{
		using common_op_type = common_op<vector<float, size_c<8>, types::simd_vector_t<types::simd_256_t>, Category_>>;
		using storage_type = vector_storage<float, size_c<8>, Category_, vector_simd_256s_storage<Category_, vector<float, size_c<8>, types::simd_vector_t<types::simd_256_t>, Category_>>>;

	public:
		using typename storage_type::value_type;
		using typename storage_type::category;
		using typename storage_type::simd_type;
		using storage_type::mVector;
		using storage_type::storage_type;

		using impl_type = types::simd_vector_t<types::simd_256_t>;

		static constinit const auto NbValues = storage_type::NbValues;
		static constinit const bool HardwareImpl = true;


		[[nodiscard]] vector andnot(vector v) const noexcept
		{
			return _mm256_andnot_ps(mVector, v.mVector);
		}

		[[nodiscard]] vector next_after() const noexcept
		{
			return _mm256_castsi256_ps(simd::AvxMathfun::mf_mm256_add_epi32(_mm256_castps_si256(mVector), _mm256_set1_epi32(1)));
		}

		[[nodiscard]] vector abs() const noexcept
		{
			#ifdef __AVX2__
				return _mm256_and_ps(mVector, _mm256_castsi256_ps(simd::AvxMathfun::mf_mm256_srli_epi32(_mm256_set1_epi32(-1), 1)));
			#else
				return _mm256_and_ps(mVector, _mm256_castsi256_ps(_mm256_set1_epi32(0x7FFFFFFF)));
			#endif
		}

		[[nodiscard]] vector pow(const value_type power) const noexcept
		{
			return select((abs().log() * power).exp(), *this > tag::zero);
		}

		[[nodiscard]] vector log() const noexcept
		{
			return simd::AvxMathfun::log256_ps(mVector);
		}

		[[nodiscard]] vector log2() const noexcept
		{
			return vector_transform(*this, [](auto&&, auto&& v){ return pllog2(v); });
		}

		[[nodiscard]] vector log2approx() const noexcept
		{
			return vector_transform(*this, [](auto&&, auto&& v){ return patlib::log2approx(v); });
		}

		[[nodiscard]] vector log10() const noexcept
		{
			return vector_transform(*this, [](auto&&, auto&& v){ return pllog10(v); });
		}

		[[nodiscard]] vector exp() const noexcept
		{
			return simd::AvxMathfun::exp256_ps(mVector);
		}

		[[nodiscard]] vector exp2() const noexcept
		{
			return vector_transform(*this, [](auto&&, auto&& v){ return plexp2(v); });
		}

		[[nodiscard]] vector exp2approx() const noexcept
		{
			return vector_transform(*this, [](auto&&, auto&& v){ return patlib::exp2approx(v); });
		}

		[[nodiscard]] vector sin() const noexcept
		{
			return simd::AvxMathfun::sin256_ps(mVector);
		}

		[[nodiscard]] vector cos() const noexcept
		{
			return simd::AvxMathfun::cos256_ps(mVector);
		}

		[[nodiscard]] auto sincos() const noexcept
		{
			simd_type s, c;
			simd::AvxMathfun::sincos256_ps(mVector, s, c);
			return sincos_res{vector{s}, vector{c}};
		}

		[[nodiscard]] int cmp_scalar_ge(vector v) const noexcept
		{
			return _mm_comige_ss(_mm256_extractf128_ps(mVector, 0), _mm256_extractf128_ps(v.mVector, 0));
		}

		[[nodiscard]] int cmp_scalar_gt(vector v) const noexcept
		{
			return _mm_comigt_ss(_mm256_extractf128_ps(mVector, 0), _mm256_extractf128_ps(v.mVector, 0));
		}

		[[nodiscard]] int cmp_scalar_eq(vector v) const noexcept
		{
			return _mm_comieq_ss(_mm256_extractf128_ps(mVector, 0), _mm256_extractf128_ps(v.mVector, 0));
		}

		[[nodiscard]] int cmp_scalar_le(vector v) const noexcept
		{
			return _mm_comile_ss(_mm256_extractf128_ps(mVector, 0), _mm256_extractf128_ps(v.mVector, 0));
		}

		[[nodiscard]] int cmp_scalar_lt(vector v) const noexcept
		{
			return _mm_comilt_ss(_mm256_extractf128_ps(mVector, 0), _mm256_extractf128_ps(v.mVector, 0));
		}


		[[nodiscard]] vector min(vector v) const noexcept
		{
			return _mm256_min_ps(mVector, v.mVector);
		}

		[[nodiscard]] vector max(vector v) const noexcept
		{
			return _mm256_max_ps(mVector, v.mVector);
		}

		[[nodiscard]] vector min_scalar(vector v) const noexcept
		{
			return _mm256_insertf128_ps(mVector, _mm_min_ss(_mm256_extractf128_ps(mVector, 0), _mm256_extractf128_ps(v.mVector, 0)), 0);
		}

		[[nodiscard]] vector max_scalar(vector v) const noexcept
		{
			return _mm256_insertf128_ps(mVector, _mm_max_ss(_mm256_extractf128_ps(mVector, 0), _mm256_extractf128_ps(v.mVector, 0)), 0);
		}

		[[nodiscard]] value_type hmin() const noexcept
		{
			auto vals = this->get_array();

			return plmin( {vals()[0], vals()[1], vals()[2], vals()[3], vals()[4], vals()[5], vals()[6], vals()[7]} );
		}

		[[nodiscard]] value_type hmax() const noexcept
		{
			auto vals = this->get_array();

			return plmax( {vals()[0], vals()[1], vals()[2], vals()[3], vals()[4], vals()[5], vals()[6], vals()[7]} );
		}

		[[nodiscard]] vector floor() const noexcept
		{
			return _mm256_floor_ps(mVector);
		}

		[[nodiscard]] vector ceil() const noexcept
		{
			return _mm256_ceil_ps(mVector);
		}

		[[nodiscard]] vector round() const noexcept
		{
			return _mm256_round_ps(mVector, _MM_FROUND_TO_NEAREST_INT | _MM_FROUND_NO_EXC);
		}

		[[nodiscard]] vector sqrt() const noexcept
		{
			return _mm256_sqrt_ps(mVector);
		}

		[[nodiscard]] vector sqrtapprox() const noexcept
		{
			return recsqrtapprox() * *this;
		}

		[[nodiscard]] vector sqrt_scalar() const noexcept
		{
			return sqrt();
		}

		[[nodiscard]] vector sqrtapprox_scalar() const noexcept
		{
			const auto ex = _mm256_extractf128_ps(mVector, 0);
			const auto r = _mm_rsqrt_ss(ex);

			// Newton-Rhapson step
			const auto r2 = _mm_mul_ss( _mm_mul_ss(_mm_set_ss(0.5f), r), _mm_sub_ss(_mm_set_ss(3), _mm_mul_ss(_mm_mul_ss(ex, r), r)) );

			return _mm256_set_m128(_mm_mul_ss(r2, ex), _mm256_extractf128_ps(mVector, 1));
		}

		[[nodiscard]] vector recsqrt() const noexcept
		{
			return sqrt().reciprocal();
		}

		[[nodiscard]] vector recsqrtapprox() const noexcept
		{
			const auto r = vector{_mm256_rsqrt_ps(mVector)};

			// Newton-Rhapson step
			return r * tag::half * (vector{3} - *this * (r * r));
		}

		[[nodiscard]] vector hmul() const noexcept
		{
			const auto r = *this * _mm256_movehdup_ps(mVector);
			return r * _mm256_permute2f128_ps(r.mVector, r.mVector, 0b1);
		}

		[[nodiscard]] vector hadd() const noexcept
		{
			auto r = _mm256_hadd_ps(mVector, mVector);
			r = _mm256_hadd_ps(r, r);
			return _mm256_hadd_ps(r, r);
		}

		[[nodiscard]] vector hsub() const noexcept
		{
			auto r = _mm256_hsub_ps(mVector, mVector);
			r = _mm256_hsub_ps(r, r);
			return _mm256_hsub_ps(r, r);
		}

		[[nodiscard]] vector inverse_sign() const noexcept
		{
			return _mm256_xor_ps(mVector, _mm256_castsi256_ps(_mm256_set1_epi32(0x80000000)));
			//return _mm256_sub_ps(_mm256_setzero_ps(), mVector);
		}

		[[nodiscard]] vector reciprocal() const noexcept
		{
			return vector{tag::one} / *this;
		}

		[[nodiscard]] vector reciprocalapprox() const noexcept
		{
			// Apply one Newton-Raphson step

			__m256 tmp = _mm256_rcp_ps(mVector);
			return _mm256_mul_ps(tmp, _mm256_sub_ps(_mm256_set1_ps(2.0f), _mm256_mul_ps(tmp, mVector)));
		}

		[[nodiscard]] vector movehl(vector v) const noexcept
		{
			return _mm256_unpackhi_ps(mVector, v.mVector);
		}

		[[nodiscard]] vector movelh(vector v) const noexcept
		{
			return _mm256_unpacklo_ps(mVector, v.mVector);
		}

		template <int m0, int m1, int m2, int m3>
		[[nodiscard]] vector shuffle(vector v) const noexcept
		{
			return _mm256_shuffle_ps(mVector, v.mVector, _MM_SHUFFLE(m3, m2, m1, m0));
		}

		template <int m0, int m1, int m2, int m3>
		[[nodiscard]] vector shuffle() const noexcept
		{
			return _mm256_shuffle_ps(mVector, mVector, _MM_SHUFFLE(m3, m2, m1, m0));
		}

		[[nodiscard]] int move_mask() const noexcept
		{
			return _mm256_movemask_ps(mVector);
		}

		[[nodiscard]] value_type dot(vector v) const noexcept
		{
			__m256 tmp1 = _mm256_dp_ps(mVector, v.mVector, 0xF1);

			return _mm_cvtss_f32(_mm256_extractf128_ps(tmp1, 0));
		}

		template <szt mask = 0xFF>
		[[nodiscard]] vector dot_m(masked<mask, vector> v) const noexcept
		{
			return _mm256_dp_ps(mVector, v.v.mVector, static_cast<int>(mask));
		}

		template <szt mask = 0xFF>
		[[nodiscard]] value_type dot(masked<mask, vector> v) const noexcept
		{
			return
				_mm_cvtss_f32(
						_mm_add_ss(
								_mm_dp_ps(_mm256_castps256_ps128(mVector), _mm256_castps256_ps128(v.v.mVector), 1 | (static_cast<int>(mask) << 4)),
								_mm_dp_ps(
										_mm256_castps256_ps128(_mm256_shuffle_ps(mVector, mVector, _MM_SHUFFLE(0, 0, 3, 2))),
										_mm256_castps256_ps128(_mm256_shuffle_ps(v.v.mVector, v.v.mVector, _MM_SHUFFLE(0, 0, 3, 2))),
										1 | static_cast<int>(mask & 0xF0)
									)
							)
					);
		}

		[[nodiscard]] value_type dot() const noexcept
		{
			return dot(*this);
		}

		using common_op_type::operator+=;
		using common_op_type::operator-=;
		using common_op_type::operator/=;
		using common_op_type::operator*=;
		using common_op_type::operator%=;
		using common_op_type::operator|=;
		using common_op_type::operator&=;
		using common_op_type::operator^=;
		using common_op_type::operator>>=;
		using common_op_type::operator<<=;

		[[nodiscard]] vector operator+ (vector v) const noexcept
		{
			return _mm256_add_ps(mVector, v.mVector);
		}

		[[nodiscard]] vector operator- (vector v) const noexcept
		{
			return _mm256_sub_ps(mVector, v.mVector);
		}

		[[nodiscard]] vector operator* (vector v) const noexcept
		{
			return _mm256_mul_ps(mVector, v.mVector);
		}

		[[nodiscard]] vector operator/ (vector v) const noexcept
		{
			return _mm256_div_ps(mVector, v.mVector);
		}

		[[nodiscard]] vector operator% (vector v) const noexcept
		{
			__m256 c = _mm256_div_ps(mVector, v.mVector);
			__m256i i = _mm256_cvttps_epi32(c);
			__m256 cTrunc = _mm256_cvtepi32_ps(i);
			__m256 base = _mm256_mul_ps(cTrunc, v.mVector);
			return _mm256_sub_ps(mVector, base);
		}

		[[nodiscard]] vector operator+ (const value_type v) const noexcept
		{
			return _mm256_add_ps(mVector, _mm256_set1_ps(v));
		}

		[[nodiscard]] vector operator- (const value_type v) const noexcept
		{
			return _mm256_sub_ps(mVector, _mm256_set1_ps(v));
		}

		[[nodiscard]] vector operator* (const value_type v) const noexcept
		{
			return _mm256_mul_ps(mVector, _mm256_set1_ps(v));
		}

		[[nodiscard]] vector operator/ (const value_type v) const noexcept
		{
			return _mm256_div_ps(mVector, _mm256_set1_ps(v));
		}

		[[nodiscard]] vector operator% (const value_type v) const noexcept
		{
			__m256 vv = _mm256_set1_ps(v);
			__m256 c = _mm256_div_ps(mVector, vv);
			__m256i i = _mm256_cvttps_epi32(c);
			__m256 cTrunc = _mm256_cvtepi32_ps(i);
			__m256 base = _mm256_mul_ps(cTrunc, vv);
			return _mm256_sub_ps(mVector, base);
		}

		[[nodiscard]] vector operator== (vector v) const noexcept
		{
			return _mm256_cmp_ps(mVector, v.mVector, 0);
		}

		[[nodiscard]] vector operator!= (vector v) const noexcept
		{
			return _mm256_cmp_ps(mVector, v.mVector, 4);
		}

		[[nodiscard]] vector operator< (vector v) const noexcept
		{
			return _mm256_cmp_ps(mVector, v.mVector, 1);
		}

		[[nodiscard]] vector operator<= (vector v) const noexcept
		{
			return _mm256_cmp_ps(mVector, v.mVector, 2);
		}

		[[nodiscard]] vector operator> (vector v) const noexcept
		{
			return _mm256_cmp_ps(mVector, v.mVector, 6);
		}

		[[nodiscard]] vector operator>= (vector v) const noexcept
		{
			return _mm256_cmp_ps(mVector, v.mVector, 5);
		}

		[[nodiscard]] vector operator== (const value_type v) const noexcept
		{
			return _mm256_cmp_ps(mVector, _mm256_set1_ps(v), 0);
		}

		[[nodiscard]] vector operator!= (const value_type v) const noexcept
		{
			return _mm256_cmp_ps(mVector, _mm256_set1_ps(v), 4);
		}

		[[nodiscard]] vector operator< (const value_type v) const noexcept
		{
			return _mm256_cmp_ps(mVector, _mm256_set1_ps(v), 1);
		}

		[[nodiscard]] vector operator<= (const value_type v) const noexcept
		{
			return _mm256_cmp_ps(mVector, _mm256_set1_ps(v), 2);
		}

		[[nodiscard]] vector operator> (const value_type v) const noexcept
		{
			return _mm256_cmp_ps(mVector, _mm256_set1_ps(v), 6);
		}

		[[nodiscard]] vector operator>= (const value_type v) const noexcept
		{
			return _mm256_cmp_ps(mVector, _mm256_set1_ps(v), 5);
		}

		[[nodiscard]] vector operator& (vector v) const noexcept
		{
			return _mm256_and_ps(mVector, v.mVector);
		}

		[[nodiscard]] vector operator| (vector v) const noexcept
		{
			return _mm256_or_ps(mVector, v.mVector);
		}

		[[nodiscard]] vector operator^ (vector v) const noexcept
		{
			return _mm256_xor_ps(mVector, v.mVector);
		}

		[[nodiscard]] vector operator& (const int v) const noexcept
		{
			return _mm256_and_ps(mVector, _mm256_castsi256_ps(_mm256_set1_epi32(v)));
		}

		[[nodiscard]] vector operator| (const int v) const noexcept
		{
			return _mm256_or_ps(mVector, _mm256_castsi256_ps(_mm256_set1_epi32(v)));
		}

		[[nodiscard]] vector operator^ (const int v) const noexcept
		{
			return _mm256_xor_ps(mVector, _mm256_castsi256_ps(_mm256_set1_epi32(v)));
		}

		[[nodiscard]] vector operator>> (const int v) const noexcept
		{
			return _mm256_castsi256_ps(simd::AvxMathfun::mf_mm256_srli_epi32(_mm256_castps_si256(mVector), v));
		}

		[[nodiscard]] vector operator<< (const int v) const noexcept
		{
			return _mm256_castsi256_ps(simd::AvxMathfun::mf_mm256_slli_epi32(_mm256_castps_si256(mVector), v));
		}

		#ifdef __AVX2__
			[[nodiscard]] vector operator>> (vector v) const noexcept
			{
				return _mm256_castsi256_ps(_mm256_srlv_epi32(_mm256_castps_si256(mVector), _mm256_castps_si256(v.mVector)));
			}

			[[nodiscard]] vector operator<< (vector v) const noexcept
			{
				return _mm256_castsi256_ps(_mm256_sllv_epi32(_mm256_castps_si256(mVector), _mm256_castps_si256(v.mVector)));
			}
		#endif

		[[nodiscard]] vector operator! () const noexcept
		{
			return _mm256_cmp_ps(mVector, _mm256_setzero_ps(), 0);
		}

		[[nodiscard]] vector operator- () const noexcept
		{
			return inverse_sign();
		}

		[[nodiscard]] vector operator+ () const noexcept
		{
			return *this;
		}

		[[nodiscard]] vector is_nan() const noexcept
		{
			return _mm256_cmp_ps(mVector, mVector, 3);
		}

		[[nodiscard]] vector is_infinite() const noexcept
		{
			const __m256 INF = _mm256_set1_ps(std::numeric_limits<value_type>::infinity());

			return _mm256_cmp_ps(abs().mVector, INF, _CMP_EQ_OQ);
		}

		[[nodiscard]] vector is_normal() const noexcept
			{ return ~(is_infinite() | is_nan() | (*this == vector{})); }

		[[nodiscard]] bool is_all_zero() const noexcept
		{
			return (!*this).move_mask() == 0xFF;
		}

		vector &operator++ () noexcept
			{ _mm256_add_ps(mVector, _mm256_set1_ps(1)); return *this; }

		vector &operator-- () noexcept
			{ _mm256_sub_ps(mVector, _mm256_set1_ps(1)); return *this; }

		vector operator++ (int) noexcept
			{ auto cp = *this; ++(*this); return cp; }

		vector operator-- (int) noexcept
			{ auto cp = *this; --(*this); return cp; }

		[[nodiscard]] vector operator~ () const noexcept
		{
			return _mm256_andnot_ps(mVector, _mm256_cmp_ps(mVector, mVector, 0));
		}

		[[nodiscard]] vector blendv(vector v, vector mask) const noexcept
			{ return _mm256_blendv_ps(mVector, v.mVector, mask.mVector); }

		template <int mask>
		[[nodiscard]] vector blend(vector v) const noexcept
			{ return _mm256_blend_ps(mVector, v.mVector, mask); }

		[[nodiscard]] vector unpacklo(vector v) const noexcept
			{ return _mm256_unpacklo_ps(mVector, v.mVector); }

		[[nodiscard]] vector unpackhi(vector v) const noexcept
			{ return _mm256_unpackhi_ps(mVector, v.mVector); }

		[[nodiscard]] operator __m256() const noexcept
			{ return mVector; }

		[[nodiscard]] auto mask() const noexcept
			{ return std::bitset<NbValues>{static_cast<std::make_unsigned_t<int>>(move_mask())}; }

		[[nodiscard]] explicit operator bool() const noexcept
			{ return mask().all(); }
	};
}
