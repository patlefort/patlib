/*
	PatLib

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "../base.hpp"
#include <patlib/impl/intrinsics.hpp>
#include <patlib/simd/mathfun/mathfun.hpp>

#define PL_LIB_VECTOR_SIMD

namespace patlib::simd
{
	namespace types
	{
		template<typename T_ = void> struct simd_vector_t { using type = T_; };
		struct simd_128_t {};
		struct simd_256_t {};
	} // namespace types

	template <typename Type_, typename NbValues_, typename Impl_, typename Category_ = void>
	using simd_vector = vector<Type_, NbValues_, types::simd_vector_t<Impl_>, Category_>;

	template <typename Type_, typename NbValues_, typename Impl_, typename VType_ = void>
	using simd_geometric_vector = geometric_vector<Type_, NbValues_, types::simd_vector_t<Impl_>, VType_>;

	template <typename Type_, typename NbValues_, typename VType_ = void>
	using simd_128_geometric_vector = simd_geometric_vector<Type_, NbValues_, types::simd_128_t, VType_>;

	template <typename Type_, typename NbValues_, typename VType_ = void>
	using simd_256_geometric_vector = simd_geometric_vector<Type_, NbValues_, types::simd_256_t, VType_>;

	template <typename T_> constexpr bool is_simd_vector_v = false;
	template <typename T_> constexpr bool is_simd_vector_v<types::simd_vector_t<T_>> = true;
	template <typename Type_, typename NbValues_, typename Implem_, typename Category_> constexpr bool is_simd_vector_v<vector<Type_, NbValues_, types::simd_vector_t<Implem_>, Category_>> = true;

	template <typename T_> concept CSimdVector = CVector<T_> && is_simd_vector_v<T_>;
	template <typename T_> concept CSimd256Vector = CSimdVector<T_> && std::same_as<typename T_::impl_type::type, types::simd_256_t>;
	template <typename T_> concept CSimd128Vector = CSimdVector<T_> && std::same_as<typename T_::impl_type::type, types::simd_128_t>;

	namespace impl
	{
		template <typename Type_, typename SimdType_, szt NB_, typename Category_>
		class vector_simd_storage_base
		{
		public:
			static constinit const szt NbValues = NB_;
			static constinit const bool RefAccess = false;
			static constinit const bool HardwareImpl = true;
			static constinit const bool Shiftable = true;
			static constinit const bool ConstExpr = false;

			using value_type = Type_;
			using category = Category_;
			using simd_type = typename SimdType_::type;

			using aligned_array_type = aligned_type<std::array<value_type, NbValues>, alignof(simd_type)>;

			simd_type mVector;

			vector_simd_storage_base() = default;
			vector_simd_storage_base(simd_type v) : mVector{v} {}
			vector_simd_storage_base(std::initializer_list<zero_init_t>) noexcept : mVector{} {}

			[[nodiscard]] operator simd_type() const noexcept
				{ return mVector; }
		};

		template <typename T_, szt NB_, typename Impl_, typename Category_, typename... ArgsC_>
		[[nodiscard]] constexpr auto tag_invoke(tag_t<select>, const vector<T_, size_c<NB_>, Impl_, Category_> v1, const vector<T_, size_c<NB_>, types::simd_vector_t<Impl_>, Category_> v2, const vector<ArgsC_...> cond) noexcept
		{
			static_assert(vector<ArgsC_...>::NbValues >= NB_, "cond must have at least as many elements as v1 and v2.");

			return v2.template blendv(v1, cond);
		}

		template <typename T_, szt NB_, typename Impl_, typename Category_, typename... ArgsC_>
		[[nodiscard]] constexpr auto tag_invoke(tag_t<select>, const vector<T_, size_c<NB_>, const types::simd_vector_t<Impl_>, Category_> v1, const vector<ArgsC_...> cond) noexcept
			{ return v1 & cond; }

		template <typename T_, szt NB_, typename Impl_, typename Category_, typename... ArgsC_>
		constexpr void tag_invoke(tag_t<condswap>, vector<T_, size_c<NB_>, types::simd_vector_t<Impl_>, Category_> &v1, vector<T_, size_c<NB_>, types::simd_vector_t<Impl_>, Category_> &v2, const vector<ArgsC_...> cond) noexcept
		{
			static_assert(vector<ArgsC_...>::NbValues >= NB_, "cond must have at least as many elements as v1 and v2.");

			const auto bk = v2;
			v2 = v2.template blendv(v1, cond);
			v1 = bk.template blendv(bk, cond);
		}

		template <typename Type1_, typename Impl1_, typename Cat1_, typename Type2_, typename Impl2_, typename Cat2_>
		[[nodiscard]] constexpr auto tag_invoke(tag_t<matrix_impl::matrix_mul>, const std::array<vector<Type1_, size_c<4>, Impl1_, categories::geometric_t<Cat1_>>, 4> &m, const vector<Type2_, size_c<4>, types::simd_vector_t<Impl2_>, categories::geometric_t<Cat2_>> v) noexcept
		{
			return
				v.dot_m(masked{size_v<0xF1>, m[0]}) +
				v.dot_m(masked{size_v<0xF2>, m[1]}) +
				v.dot_m(masked{size_v<0xF4>, m[2]}) +
				v.dot_m(masked{size_v<0xF8>, m[3]});
		}

		template <typename Type1_, typename Impl1_, typename Cat1_, typename Type2_, typename Impl2_, typename Cat2_>
		[[nodiscard]] constexpr auto tag_invoke(tag_t<matrix_impl::matrix_mul>, const std::array<vector<Type1_, size_c<4>, types::simd_vector_t<Impl1_>, categories::geometric_t<Cat1_>>, 4> &m, const vector<Type2_, size_c<4>, Impl2_, categories::geometric_t<Cat2_>> v) noexcept
		{
			return
				m[0].dot_m(masked{size_v<0xF1>, v}) +
				m[1].dot_m(masked{size_v<0xF2>, v}) +
				m[2].dot_m(masked{size_v<0xF4>, v}) +
				m[3].dot_m(masked{size_v<0xF8>, v});
		}

		template <typename Type1_, typename Impl1_, typename Cat1_, typename Type2_, typename Impl2_, typename Cat2_>
		[[nodiscard]] constexpr auto tag_invoke(tag_t<matrix_impl::matrix_mul>, const std::array<vector<Type1_, size_c<4>, types::simd_vector_t<Impl1_>, categories::geometric_t<Cat1_>>, 4> &m, const vector<Type2_, size_c<4>, types::simd_vector_t<Impl2_>, categories::geometric_t<Cat2_>> v) noexcept
		{
			return
				v.dot_m(masked{size_v<0xF1>, m[0]}) +
				v.dot_m(masked{size_v<0xF2>, m[1]}) +
				v.dot_m(masked{size_v<0xF4>, m[2]}) +
				v.dot_m(masked{size_v<0xF8>, m[3]});
		}

		template <typename Type1_, typename Impl1_, typename Cat1_>
		[[nodiscard]] constexpr auto tag_invoke(tag_t<matrix_impl::matrix_transpose>, const std::array<vector<Type1_, size_c<4>, types::simd_vector_t<types::simd_128_t>, categories::geometric_t<Cat1_>>, 4> &v) noexcept
		{
			const auto l02 = v[0].unpacklo(v[2]);
			const auto h02 = v[0].unpackhi(v[2]);
			const auto l13 = v[1].unpacklo(v[3]);
			const auto h13 = v[1].unpackhi(v[3]);

			return std::decay_t<decltype(v)>{
				l02.unpacklo(l13),
				l02.unpackhi(l13),
				h02.unpacklo(h13),
				h02.unpackhi(h13)
			};
		}
	} // namespace impl
}
