/*
	PatLib

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "base.hpp"

#ifndef PL_LIB_VECTOR_128_SIMD
	#define PL_LIB_VECTOR_128_SIMD
#endif

#ifndef PL_LIB_VECTOR_128_DOUBLE_SIMD
	#define PL_LIB_VECTOR_128_DOUBLE_SIMD
#endif

#ifndef PL_LIB_VECTOR_DOUBLE_SIMD
	#define PL_LIB_VECTOR_DOUBLE_SIMD
#endif

namespace patlib::simd::impl
{
	struct wrapped__m128d { using type = __m128d; };

	template <typename Category_, typename Derived_>
	class vector_simd_128d_storage :
		public vector_simd_storage_base<double, wrapped__m128d, 2, Category_>,
		public iteratable<double, 2, vector_simd_128d_storage<Category_, Derived_>>
	{
	private:
		PL_CRTP_BASE(Derived_)

		using storage_type = vector_simd_storage_base<double, wrapped__m128d, 2, Category_>;

	public:
		using typename storage_type::value_type;
		using typename storage_type::aligned_array_type;
		using storage_type::mVector;
		using storage_type::storage_type;

		vector_simd_128d_storage() = default;
		vector_simd_128d_storage(const value_type x, const value_type y) noexcept : storage_type{ _mm_set_pd(y, x) } {}
		vector_simd_128d_storage(const value_type v) noexcept : storage_type{ _mm_set1_pd(v) } {}
		explicit vector_simd_128d_storage(const tag::array_init_t, const value_type *v, tag::aligned_init_t = {}) noexcept : storage_type{ _mm_load_pd(v) } {}
		explicit vector_simd_128d_storage(const tag::array_init_t, const value_type *v, tag::unaligned_init_t) noexcept : storage_type{ _mm_loadu_pd(v) } {}
		vector_simd_128d_storage(const aligned_array_type &v) noexcept : storage_type{ _mm_load_pd(v().data()) } {}

		vector_simd_128d_storage(const tag::zero_t) noexcept : storage_type{{}} {}
		vector_simd_128d_storage(const tag::identity_t, const value_type last = 1) noexcept { set_identity(last); }
		vector_simd_128d_storage(const tag::allone_t) noexcept { set_all_one(); }
		vector_simd_128d_storage(const tag::one_t) noexcept { set_one(); }
		vector_simd_128d_storage(const tag::two_t) noexcept { set_two(); }
		vector_simd_128d_storage(const tag::half_t) noexcept { set_half(); }
		vector_simd_128d_storage(const tag::nan_t) noexcept { set_nan(); }


		[[nodiscard]] value_type operator[] (const szt index) const noexcept
		{
			#if BOOST_COMP_MSVC
				return mVector.m128d_f64[index];
			#else
				return get_array()()[index];
			#endif
		}

		template <szt index>
		[[nodiscard]] value_type operator[] (size_c<index>) const noexcept
		{
			if constexpr(index == 0)
				return _mm_cvtsd_f64(mVector);
			else
				return _mm_cvtsd_f64(_mm_shuffle_pd(mVector, mVector, index));
		}

		auto &set_zero() noexcept
		{
			mVector = _mm_setzero_pd();
			return derived();
		}

		auto &set_identity(const value_type last = 1) noexcept
		{
			set_all(0, last);
			return derived();
		}

		auto &set(const value_type v) noexcept
		{
			mVector = _mm_set1_pd(v);
			return derived();
		}

		auto &set_all_one() noexcept
		{
			#ifdef __SSE4_2__
				mVector = _mm_castsi128_pd(_mm_cmpeq_epi64(_mm_castpd_si128(mVector), _mm_castpd_si128(mVector)));
			#else
				mVector = _mm_castsi128_pd(_mm_set_epi64x(~u64(0), ~u64(0)));
			#endif

			return derived();
		}

		auto &set_one() noexcept
		{
			#ifdef __SSE4_2__
				mVector = _mm_castsi128_pd( _mm_srli_epi64(_mm_slli_epi64(_mm_cmpeq_epi64(_mm_castpd_si128(mVector), _mm_castpd_si128(mVector)), 54), 2));
			#else
				mVector = _mm_set1_pd(1.0);
			#endif

			return derived();
		}

		auto &set_half() noexcept
		{
			#ifdef __SSE4_2__
				mVector = _mm_castsi128_pd( _mm_srli_epi64(_mm_slli_epi64(_mm_cmpeq_epi64(_mm_castpd_si128(mVector), _mm_castpd_si128(mVector)), 55), 2));
			#else
				mVector = _mm_set1_pd(0.5);
			#endif

			return derived();
		}

		auto &set_two() noexcept
		{
			#ifdef __SSE4_2__
				mVector = _mm_castsi128_pd( _mm_srli_epi64(_mm_slli_epi64(_mm_cmpeq_epi64(_mm_castpd_si128(mVector), _mm_castpd_si128(mVector)), 63), 1));
			#else
				mVector = _mm_set1_pd(2.0);
			#endif

			return derived();
		}

		auto &set_nan() noexcept
		{
			return (derived() = std::numeric_limits<value_type>::quiet_NaN());
		}

		auto &set_all(const value_type x, const value_type y) noexcept
		{
			mVector = _mm_set_pd(y, x);
			return derived();
		}

		auto &set(const value_type *v) noexcept
		{
			mVector = _mm_load_pd(v);
			return derived();
		}

		auto &set_unaligned(const value_type *v) noexcept
		{
			mVector = _mm_loadu_pd(v);
			return derived();
		}

		auto &set(const aligned_array_type &v) noexcept
		{
			mVector = _mm_load_pd(v().data());
			return derived();
		}

		void get_all(value_type *v) const noexcept
		{
			_mm_storeu_pd(v, mVector);
		}

		void get_all_aligned(value_type *v) const noexcept
		{
			_mm_store_pd(v, mVector);
		}

		[[nodiscard]] aligned_array_type get_array() const noexcept
		{
			aligned_array_type a; get_all_aligned(a().data());
			return a;
		}

		template <szt index>
		auto &set(const value_type v, size_c<index> = {}) noexcept
		{
			mVector = _mm_blend_pd(mVector, _mm_set1_pd(v), 1 << index);

			return derived();
		}

		auto &set(const value_type v, const int index) noexcept
		{
			//XMM_INDEX_SET_DOUBLE(mVector, index, v);
			auto va = get_array();
			va()[index] = v;
			set(va);

			return derived();
		}

		void scalar(value_type &dest) noexcept
		{
			_mm_store_sd(&dest, mVector);
		}

		[[nodiscard]] value_type scalar() const noexcept
		{
			return _mm_cvtsd_f64(mVector);
		}
	};

	template <typename Category_>
	class vector<double, size_c<2>, types::simd_vector_t<types::simd_128_t>, Category_> :
		public vector_storage<double, size_c<2>, Category_, vector_simd_128d_storage<Category_, vector<double, size_c<2>, types::simd_vector_t<types::simd_128_t>, Category_>>>,
		public vector_base<vector<double, size_c<2>, types::simd_vector_t<types::simd_128_t>, Category_>>,
		public common_op<vector<double, size_c<2>, types::simd_vector_t<types::simd_128_t>, Category_>>
	{
		using common_op_type = common_op<vector<double, size_c<2>, types::simd_vector_t<types::simd_128_t>, Category_>>;
		using storage_type = vector_storage<double, size_c<2>, Category_, vector_simd_128d_storage<Category_, vector<double, size_c<2>, types::simd_vector_t<types::simd_128_t>, Category_>>>;

	public:
		static constinit const auto NbValues = storage_type::NbValues;
		static constinit const bool HardwareImpl = true;

		using typename storage_type::value_type;
		using typename storage_type::category;
		using storage_type::mVector;
		using storage_type::storage_type;

		using impl_type = types::simd_vector_t<types::simd_128_t>;

		[[nodiscard]] vector andnot(vector v) const noexcept
		{
			return _mm_andnot_pd(mVector, v.mVector);
		}

		[[nodiscard]] vector next_after() const noexcept
		{
			return _mm_castsi128_pd(_mm_add_epi64(_mm_castpd_si128(mVector), _mm_set1_epi64x(1)));
		}

		[[nodiscard]] vector abs() const noexcept
		{
			return _mm_and_pd(mVector, _mm_castsi128_pd(_mm_srli_epi64(_mm_set1_epi64x(-1), 1)));
		}

		[[nodiscard]] int cmp_scalar_ge(vector v) const noexcept
		{
			return _mm_comige_sd(mVector, v.mVector);
		}

		[[nodiscard]] int cmp_scalar_gt(vector v) const noexcept
		{
			return _mm_comigt_sd(mVector, v.mVector);
		}

		[[nodiscard]] int cmp_scalar_eq(vector v) const noexcept
		{
			return _mm_comieq_sd(mVector, v.mVector);
		}

		[[nodiscard]] int cmp_scalar_le(vector v) const noexcept
		{
			return _mm_comile_sd(mVector, v.mVector);
		}

		[[nodiscard]] int cmp_scalar_lt(vector v) const noexcept
		{
			return _mm_comilt_sd(mVector, v.mVector);
		}


		[[nodiscard]] vector min(vector v) const noexcept
		{
			return _mm_min_pd(mVector, v.mVector);
		}

		[[nodiscard]] vector max(vector v) const noexcept
		{
			return _mm_max_pd(mVector, v.mVector);
		}

		[[nodiscard]] vector min_scalar(vector v) const noexcept
		{
			return _mm_min_sd(mVector, v.mVector);
		}

		[[nodiscard]] vector max_scalar(vector v) const noexcept
		{
			return _mm_max_sd(mVector, v.mVector);
		}

		[[nodiscard]] vector floor() const noexcept
		{
			return _mm_floor_pd(mVector);
		}

		[[nodiscard]] vector ceil() const noexcept
		{
			return _mm_ceil_pd(mVector);
		}

		[[nodiscard]] vector round() const noexcept
		{
			return _mm_round_pd(mVector, _MM_FROUND_TO_NEAREST_INT | _MM_FROUND_NO_EXC);
		}

		[[nodiscard]] vector pow(const value_type power) const noexcept
		{
			return vector_transform(*this, [&](auto&&, auto&& v){ return plpow(v, power); });
		}

		[[nodiscard]] vector log() const noexcept
		{
			return vector_transform(*this, [](auto&&, auto&& v){ return pllog(v); });
		}

		[[nodiscard]] vector log2() const noexcept
		{
			return vector_transform(*this, [](auto&&, auto&& v){ return pllog2(v); });
		}

		[[nodiscard]] vector log2approx() const noexcept
		{
			return vector_transform(*this, [](auto&&, auto&& v){ return patlib::log2approx(v); });
		}

		[[nodiscard]] vector log10() const noexcept
		{
			return vector_transform(*this, [](auto&&, auto&& v){ return pllog10(v); });
		}

		[[nodiscard]] vector exp() const noexcept
		{
			return vector_transform(*this, [](auto&&, auto&& v){ return plexp(v); });
		}

		[[nodiscard]] vector exp2() const noexcept
		{
			return vector_transform(*this, [](auto&&, auto&& v){ return plexp2(v); });
		}

		[[nodiscard]] vector exp2approx() const noexcept
		{
			return vector_transform(*this, [](auto&&, auto&& v){ return patlib::exp2approx(v); });
		}

		[[nodiscard]] vector sin() const noexcept
		{
			return vector_transform(*this, [](auto&&, auto&& v){ return plsin(v); });
		}

		[[nodiscard]] vector cos() const noexcept
		{
			return vector_transform(*this, [](auto&&, auto&& v){ return plcos(v); });
		}

		[[nodiscard]] auto sincos() const noexcept
		{
			return sincos_res{sin(), cos()};
		}

		[[nodiscard]] value_type hmin() const noexcept
		{
			auto vals = this->get_array();

			return plmin(vals()[0], vals()[1]);
		}

		[[nodiscard]] value_type hmax() const noexcept
		{
			auto vals = this->get_array();

			return plmax(vals()[0], vals()[1]);
		}

		[[nodiscard]] vector sqrt() const noexcept
		{
			return _mm_sqrt_pd(mVector);
		}

		[[nodiscard]] vector sqrtapprox() const noexcept
		{
			return sqrt();
		}

		[[nodiscard]] vector sqrt_scalar() const noexcept
		{
			return _mm_sqrt_sd(mVector, mVector);
		}

		[[nodiscard]] vector sqrtapprox_scalar() const noexcept
		{
			return sqrt_scalar();
		}

		[[nodiscard]] vector recsqrt() const noexcept
		{
			return sqrt().reciprocal();
		}

		[[nodiscard]] vector recsqrtapprox() const noexcept
		{
			return recsqrt();
		}

		[[nodiscard]] vector hmul() const noexcept
		{
			return *this * shuffle<1, 0>();
		}

		[[nodiscard]] vector hadd() const noexcept
		{
			return _mm_hadd_pd(mVector, mVector);
		}

		[[nodiscard]] vector hsub() const noexcept
		{
			return _mm_hsub_pd(mVector, mVector);
		}

		[[nodiscard]] vector inverse_sign() const noexcept
		{
			#ifdef __SSE2__
				return _mm_xor_pd(mVector, _mm_castsi128_pd(_mm_set1_epi64x( (u64)1 << 63 )));
			#else
				return _mm_sub_pd(_mm_setzero_pd(), mVector);
			#endif
		}

		[[nodiscard]] vector reciprocal() const noexcept
		{
			__m128d one = _mm_castsi128_pd( _mm_srli_epi64(_mm_slli_epi64(_mm_cmpeq_epi64(_mm_castpd_si128(mVector), _mm_castpd_si128(mVector)), 54), 2));
			return _mm_div_pd(one, mVector);
		}

		[[nodiscard]] vector reciprocalapprox() const noexcept
		{
			return reciprocal();
		}

		template <int m0, int m1>
		[[nodiscard]] vector shuffle(vector v) const noexcept
		{
			return _mm_shuffle_pd(mVector, v.mVector, m0 & (m1<<1));
		}

		template <int m0, int m1>
		[[nodiscard]] vector shuffle() const noexcept
		{
			return _mm_castsi128_pd(_mm_shuffle_epi32(_mm_castpd_si128(mVector), _MM_SHUFFLE(m1*2 + 1, m1*2, m0*2 + 1, m0*2)));
		}

		[[nodiscard]] int move_mask() const noexcept
		{
			return _mm_movemask_pd(mVector);
		}

		[[nodiscard]] value_type dot(vector v) const noexcept
		{
			#ifdef __SSE4_1__
				return _mm_cvtsd_f64(_mm_dp_pd(mVector, v.mVector, 0xF3));
			#else
				__m128d tmp1;

				tmp1 = _mm_mul_pd(mVector, v.mVector);
				tmp1 = _mm_hadd_pd(tmp1, tmp1);

				return _mm_cvtsd_f64(tmp1);
			#endif
		}

		template <szt mask = 0xFF>
		[[nodiscard]] vector dot_m(masked<mask, vector> v) const noexcept
		{
			static constinit const auto maskInt = static_cast<int>(mask);

			#ifdef __SSE4_1__
				return _mm_dp_pd(mVector, v.v.mVector, maskInt);
			#else
				__m128d tmp1;

				if constexpr(mask == 0xFF)
				{
					tmp1 = _mm_mul_pd(mVector, v.v.mVector);
					tmp1 = _mm_hadd_pd(tmp1, tmp1);
				}
				else
				{
					tmp1 = _mm_mul_pd(mVector, v.v.mVector);
					tmp1 = _mm_and_pd(tmp1, _mm_castsi128_pd(_mm_set_epi32( constants::all_ones<int> * ((maskInt>>7)&0x1), constants::all_ones<int> * ((maskInt>>6)&0x1), constants::all_ones<int> * ((maskInt>>5)&0x1), constants::all_ones<int> * ((maskInt>>4)&0x1) )));
					tmp1 = _mm_hadd_pd(tmp1, tmp1);
					tmp1 = _mm_and_pd(tmp1, _mm_castsi128_pd(_mm_set_epi32( constants::all_ones<int> * ((maskInt>>3)&0x1), constants::all_ones<int> * ((maskInt>>2)&0x1), constants::all_ones<int> * ((maskInt>>1)&0x1), constants::all_ones<int> * ((maskInt)&0x1) )));
				}

				return tmp1;
			#endif
		}

		template <szt mask = 0xFF>
		[[nodiscard]] value_type dot(masked<mask, vector> v) const noexcept
		{
			return dot_m(masked{size_v<0x1 | (mask&0b11 << 4)>, v.v}).scalar();
		}

		[[nodiscard]] value_type dot() const noexcept
		{
			return dot(*this);
		}

		using common_op_type::operator+=;
		using common_op_type::operator-=;
		using common_op_type::operator/=;
		using common_op_type::operator*=;
		using common_op_type::operator%=;
		using common_op_type::operator|=;
		using common_op_type::operator&=;
		using common_op_type::operator^=;
		using common_op_type::operator>>=;
		using common_op_type::operator<<=;

		[[nodiscard]] vector operator+ (vector v) const noexcept
		{
			return _mm_add_pd(mVector, v.mVector);
		}

		[[nodiscard]] vector operator- (vector v) const noexcept
		{
			return _mm_sub_pd(mVector, v.mVector);
		}

		[[nodiscard]] vector operator* (vector v) const noexcept
		{
			return _mm_mul_pd(mVector, v.mVector);
		}

		[[nodiscard]] vector operator/ (vector v) const noexcept
		{
			return _mm_div_pd(mVector, v.mVector);
		}

		[[nodiscard]] vector operator% (vector v) const noexcept
		{
			#ifdef __AVX512__
				__m128d c = _mm_div_pd(mVector, v.mVector);
				__m128i i = _mm_cvttpd_epi64(c);
				__m128d cTrunc = _mm_cvtepi64_pd(i);
				__m128d base = _mm_mul_pd(cTrunc, v.mVector);
				return _mm_sub_pd(mVector, base);
			#else
				__m128d cTrunc = _mm_floor_pd(_mm_div_pd(mVector, v.mVector));
				__m128d base = _mm_mul_pd(cTrunc, v.mVector);
				return _mm_sub_pd(mVector, base);
			#endif
		}

		[[nodiscard]] vector operator+ (const value_type v) const noexcept
		{
			return _mm_add_pd(mVector, _mm_set1_pd(v));
		}

		[[nodiscard]] vector operator- (const value_type v) const noexcept
		{
			return _mm_sub_pd(mVector, _mm_set1_pd(v));
		}

		[[nodiscard]] vector operator* (const value_type v) const noexcept
		{
			return _mm_mul_pd(mVector, _mm_set1_pd(v));
		}

		[[nodiscard]] vector operator/ (const value_type v) const noexcept
		{
			return _mm_div_pd(mVector, _mm_set1_pd(v));
		}

		[[nodiscard]] vector operator% (const value_type v) const noexcept
		{
			__m128d vv = _mm_set1_pd(v);

			#ifdef __AVX512__

				__m128d c = _mm_div_pd(mVector, vv);
				__m128i i = _mm_cvttpd_epi64(c);
				__m128d cTrunc = _mm_cvtepi64_pd(i);
				__m128d base = _mm_mul_pd(cTrunc, vv);
				return _mm_sub_pd(mVector, base);
			#else
				__m128d cTrunc = _mm_floor_pd(_mm_div_pd(mVector, vv));
				__m128d base = _mm_mul_pd(cTrunc, vv);
				return _mm_sub_pd(mVector, base);
			#endif
		}

		[[nodiscard]] vector operator== (vector v) const noexcept
		{
			return _mm_cmpeq_pd(mVector, v.mVector);
		}

		[[nodiscard]] vector operator!= (vector v) const noexcept
		{
			return _mm_cmpneq_pd(mVector, v.mVector);
		}

		[[nodiscard]] vector operator< (vector v) const noexcept
		{
			return _mm_cmplt_pd(mVector, v.mVector);
		}

		[[nodiscard]] vector operator<= (vector v) const noexcept
		{
			return _mm_cmple_pd(mVector, v.mVector);
		}

		[[nodiscard]] vector operator> (vector v) const noexcept
		{
			return _mm_cmpgt_pd(mVector, v.mVector);
		}

		[[nodiscard]] vector operator>= (vector v) const noexcept
		{
			return _mm_cmpge_pd(mVector, v.mVector);
		}

		[[nodiscard]] vector operator== (const value_type v) const noexcept
		{
			return _mm_cmpeq_pd(mVector, _mm_set1_pd(v));
		}

		[[nodiscard]] vector operator!= (const value_type v) const noexcept
		{
			return _mm_cmpneq_pd(mVector, _mm_set1_pd(v));
		}

		[[nodiscard]] vector operator< (const value_type v) const noexcept
		{
			return _mm_cmplt_pd(mVector, _mm_set1_pd(v));
		}

		[[nodiscard]] vector operator<= (const value_type v) const noexcept
		{
			return _mm_cmple_pd(mVector, _mm_set1_pd(v));
		}

		[[nodiscard]] vector operator> (const value_type v) const noexcept
		{
			return _mm_cmpgt_pd(mVector, _mm_set1_pd(v));
		}

		[[nodiscard]] vector operator>= (const value_type v) const noexcept
		{
			return _mm_cmpge_pd(mVector, _mm_set1_pd(v));
		}

		[[nodiscard]] vector operator! () const noexcept
		{
			return _mm_cmpeq_pd(mVector, _mm_setzero_pd());
		}

		[[nodiscard]] vector operator- () const noexcept
		{
			return inverse_sign();
		}

		[[nodiscard]] vector operator+ () const noexcept
		{
			return *this;
		}

		[[nodiscard]] vector operator& (vector v) const noexcept
		{
			return _mm_and_pd(mVector, v.mVector);
		}

		[[nodiscard]] vector operator| (vector v) const noexcept
		{
			return _mm_or_pd(mVector, v.mVector);
		}

		[[nodiscard]] vector operator^ (vector v) const noexcept
		{
			return _mm_xor_pd(mVector, v.mVector);
		}

		[[nodiscard]] vector operator& (const i64 v) const noexcept
		{
			return _mm_and_pd(mVector, _mm_castsi128_pd(_mm_set1_epi64x(v)));
		}

		[[nodiscard]] vector operator| (const i64 v) const noexcept
		{
			return _mm_or_pd(mVector, _mm_castsi128_pd(_mm_set1_epi64x(v)));
		}

		[[nodiscard]] vector operator^ (const i64 v) const noexcept
		{
			return _mm_xor_pd(mVector, _mm_castsi128_pd(_mm_set1_epi64x(v)));
		}

		[[nodiscard]] vector operator>> (const int v) const noexcept
		{
			return _mm_castsi128_pd(_mm_srli_epi64(_mm_castpd_si128(mVector), v));
		}

		[[nodiscard]] vector operator<< (const int v) const noexcept
		{
			return _mm_castsi128_pd(_mm_slli_epi64(_mm_castpd_si128(mVector), v));
		}

		[[nodiscard]] vector operator>> (vector v) const noexcept
		{
			return _mm_castsi128_pd(_mm_srl_epi64(_mm_castpd_si128(mVector), _mm_castpd_si128(v.mVector)));
		}

		[[nodiscard]] vector operator<< (vector v) const noexcept
		{
			return _mm_castsi128_pd(_mm_sll_epi64(_mm_castpd_si128(mVector), _mm_castpd_si128(v.mVector)));
		}

		[[nodiscard]] vector is_nan() const noexcept
		{
			return _mm_cmpunord_pd(mVector, mVector);
		}

		[[nodiscard]] vector is_infinite() const noexcept
		{
			const __m128d INF = _mm_set1_pd(std::numeric_limits<value_type>::infinity());

			return _mm_cmpeq_pd(abs().mVector, INF);
		}

		[[nodiscard]] vector is_normal() const noexcept
			{ return ~(is_infinite() | is_nan() | (*this == vector{})); }

		[[nodiscard]] bool is_all_zero() const noexcept
		{
			return (!*this).move_mask() == 0x3;
		}

		vector &operator++ () noexcept
			{ _mm_add_pd(mVector, _mm_set1_pd(1)); return *this; }

		vector &operator-- () noexcept
			{ _mm_sub_pd(mVector, _mm_set1_pd(1)); return *this; }

		vector operator++ (int) noexcept
			{ auto cp = *this; ++(*this); return cp; }

		vector operator-- (int) noexcept
			{ auto cp = *this; --(*this); return cp; }

		[[nodiscard]] vector operator~ () const noexcept
		{
			return _mm_andnot_pd(mVector, _mm_cmpeq_pd(mVector, mVector));
		}

		[[nodiscard]] vector blendv(vector v, vector mask) const noexcept
		#ifdef __SSE4_1__
			{ return _mm_blendv_pd(mVector, v.mVector, mask.mVector); }
		#else
			{
				auto res = this->get_array();
				auto va = v.get_array(), amask = mask.get_array();

				for(szt i=0; i<NbValues; ++i)
					if(amask()[i])
						res()[i] = va()[i];

				return res;
			}
		#endif

		template <int mask>
		[[nodiscard]] vector blend(vector v) const noexcept
		#ifdef __SSE4_1__
			{ return _mm_blend_pd(mVector, v.mVector, mask); }
		#else
			{
				auto res = this->get_array();
				auto va = v.get_array();

				for(szt i=0; i<NbValues; ++i)
					if(mask & (1<<i))
						res()[i] = va()[i];

				return res;
			}
		#endif

		[[nodiscard]] vector unpacklo(vector v) const noexcept
			{ return _mm_unpacklo_pd(mVector, v.mVector); }

		[[nodiscard]] vector unpackhi(vector v) const noexcept
			{ return _mm_unpackhi_pd(mVector, v.mVector); }

		[[nodiscard]] auto mask() const noexcept
			{ return std::bitset<NbValues>{static_cast<std::make_unsigned_t<int>>(move_mask())}; }

		[[nodiscard]] explicit operator bool() const noexcept
			{ return mask().all(); }
	};


	template <typename Category_, typename Derived_>
	class vector_simd_128d_4_storage :
		public iteratable<double, 2, vector_simd_128d_4_storage<Category_, Derived_>>
	{
	private:
		PL_CRTP_BASE(Derived_)

	public:
		static constinit const szt NbValues = 4;
		static constinit const bool RefAccess = false;
		static constinit const bool HardwareImpl = true;
		static constinit const bool Shiftable = true;
		static constinit const bool ConstExpr = false;

		using value_type = double;
		using simd_type = vector<double, size_c<2>, types::simd_vector_t<types::simd_128_t>>;
		using aligned_array_type = aligned_type<std::array<value_type, NbValues>, alignof(simd_type)>;
		using category = Category_;

		simd_type v1, v2;

		vector_simd_128d_4_storage() = default;
		vector_simd_128d_4_storage(std::initializer_list<zero_init_t>) noexcept : v1{}, v2{} {}
		vector_simd_128d_4_storage(const simd_type &pv1, const simd_type &pv2) noexcept : v1{pv1}, v2{pv2} {}

		vector_simd_128d_4_storage(const value_type v) noexcept : v1{v}, v2{v} {}
		vector_simd_128d_4_storage(const value_type v, const value_type w) noexcept : v1{v}, v2{v, w} {}
		vector_simd_128d_4_storage(const value_type x, const value_type y, const value_type z) noexcept : v1{x, y}, v2{z, 0} {}
		vector_simd_128d_4_storage(const value_type x, const value_type y, const value_type z, const value_type w) noexcept :
			v1{x, y}, v2{z, w} {}

		template <typename Alignment_ = tag::aligned_init_t>
		explicit vector_simd_128d_4_storage(const tag::array_init_t t, const value_type *v, Alignment_ a = {}) noexcept : v1{t, v, a}, v2{t, v+2, a} {}
		vector_simd_128d_4_storage(const aligned_array_type &v) noexcept : vector_simd_128d_4_storage(tag::array_init, v().data()) {}

		vector_simd_128d_4_storage(const tag::zero_t) noexcept : v1{tag::zero}, v2{tag::zero} {}
		vector_simd_128d_4_storage(const tag::identity_t) noexcept { set_identity(); }
		vector_simd_128d_4_storage(const tag::allone_t) noexcept : v1{tag::allone}, v2{tag::allone} {}
		vector_simd_128d_4_storage(const tag::one_t) noexcept : v1{tag::one}, v2{tag::one} {}
		vector_simd_128d_4_storage(const tag::two_t) noexcept : v1{tag::two}, v2{tag::two} {}
		vector_simd_128d_4_storage(const tag::half_t) noexcept : v1{tag::half}, v2{tag::half} {}
		vector_simd_128d_4_storage(const tag::nan_t) noexcept : v1{tag::nan}, v2{tag::nan} {}


		[[nodiscard]] value_type operator[] (const szt index) const noexcept
		{
			return index > 1 ? v2[index - 2] : v1[index];
		}

		template <szt index>
		[[nodiscard]] value_type operator[] (size_c<index>) const noexcept
		{
			if constexpr(index > 1)
				return v2[size_v<index - 2>];
			else
				return v1[size_v<index>];
		}

		auto &set_identity() noexcept
		{
			v1.set_zero();
			v2.set_identity();
			return derived();
		}

		auto &set_zero() noexcept
		{
			v1.set_zero();
			v2.set_zero();
			return derived();
		}

		auto &set_all_one() noexcept
		{
			v1.set_all_one();
			v2.set_all_one();
			return derived();
		}

		auto &set_one() noexcept
		{
			v1.set_one();
			v2.set_one();
			return derived();
		}

		auto &set_half() noexcept
		{
			v1.set_half();
			v2.set_half();
			return derived();
		}

		auto &set_two() noexcept
		{
			v1.set_two();
			v2.set_two();
			return derived();
		}

		auto &set_nan() noexcept
		{
			v1.set_nan();
			v2.set_nan();
			return derived();
		}

		auto &set(const value_type v) noexcept
		{
			v1.set(v);
			v2.set(v);
			return derived();
		}

		auto &set_all(const value_type x, const value_type y, const value_type z, const value_type w) noexcept
		{
			v1.set_all(x, y);
			v2.set_all(z, w);
			return derived();
		}

		auto &set(const value_type *v) noexcept
		{
			v1.set(v);
			v2.set(v+2);
			return derived();
		}

		auto &set_unaligned(const value_type *v) noexcept
		{
			v1.set_unaligned(v);
			v2.set_unaligned(v+2);
			return derived();
		}

		auto &set(const aligned_array_type &v) noexcept
		{
			v1.set(v().data());
			v2.set(v().data()+2);

			return derived();
		}

		template <szt index>
		auto &set(const value_type v, size_c<index> = {}) noexcept
		{
			if constexpr(index > 1)
				v2.set<index - 2>(v, size_v<index - 2>);
			else
				v1.set(v, size_v<index>);

			return derived();
		}

		void get_all(value_type *v) const noexcept
		{
			v1.get_all(v);
			v2.get_all(v+2);
		}

		void get_all_aligned(value_type *v) const noexcept
		{
			v1.get_all_aligned(v);
			v2.get_all_aligned(v+2);
		}

		[[nodiscard]] aligned_array_type get_array() const noexcept
		{
			aligned_array_type a; get_all_aligned(a().data());
			return a;
		}

		auto &set(const value_type v, const int index) noexcept
		{
			if(index > 1)
				v2.set(v, index - 2);
			else
				v1.set(v, index);

			//((value_type *)(&v1.mVector))[index] = v;
			//XMM_INDEX_SET_DOUBLE(v1.mVector, index, v);
			return derived();
		}

		void scalar(value_type &dest) noexcept
		{
			v1.scalar(dest);
		}

		[[nodiscard]] value_type scalar() const noexcept
		{
			return v1.scalar();
		}
	};

	template <typename Category_>
	class vector<double, size_c<4>, types::simd_vector_t<types::simd_128_t>, categories::geometric_t<Category_, 3>> :
		public vector_storage<double, size_c<4>, categories::geometric_t<Category_, 3>, vector_simd_128d_4_storage<categories::geometric_t<Category_, 3>, vector<double, size_c<4>, types::simd_vector_t<types::simd_128_t>, categories::geometric_t<Category_, 3>>>>,
		public vector_base<vector<double, size_c<4>, types::simd_vector_t<types::simd_128_t>, categories::geometric_t<Category_, 3>>>,
		public common_op<vector<double, size_c<4>, types::simd_vector_t<types::simd_128_t>, categories::geometric_t<Category_, 3>>>
	{
		using common_op_type = common_op<vector<double, size_c<4>, types::simd_vector_t<types::simd_128_t>, categories::geometric_t<Category_, 3>>>;
		using storage_type = vector_storage<double, size_c<4>, categories::geometric_t<Category_, 3>, vector_simd_128d_4_storage<categories::geometric_t<Category_, 3>, vector<double, size_c<4>, types::simd_vector_t<types::simd_128_t>, categories::geometric_t<Category_, 3>>>>;

	public:

		using impl_type = types::simd_vector_t<types::simd_128_t>;

		static constexpr auto LastValue = get_gvec_last_value<Category_, double>();
		static constinit const auto NbValues = storage_type::NbValues;
		static constinit const bool HardwareImpl = true;

		using typename storage_type::value_type;
		using typename storage_type::category;
		using typename storage_type::simd_type;
		using storage_type::v1; using storage_type::v2;
		using storage_type::storage_type;

		vector() = default;
		vector(const value_type v) noexcept : storage_type{v} {}
		vector(const value_type v) noexcept requires (!CVoid<Category_>) : storage_type{v, LastValue} {}
		vector(const value_type x, const value_type y, const value_type z) noexcept : storage_type{x, y, z, LastValue} {}
		vector(const tag::identity_t) noexcept { set_identity(); }

		template <typename OTag_>
			explicit(!is_valid_implicit_vector_conversion<4, categories::geometric_t<OTag_, 3>, 4, category>)
		vector(const vector<double, size_c<4>, impl_type, categories::geometric_t<OTag_, 3>> &o) noexcept :
			storage_type{o.v1, o.v2} {}


		vector &set_identity() noexcept
		{
			v1.set_zero();
			v2.set_all(0, LastValue);
			return *this;
		}

		[[nodiscard]] vector abs() const noexcept
		{
			return {v1.abs(), v2.abs()};
		}

		[[nodiscard]] vector andnot(vector v) const noexcept
		{
			return {v1.andnot(v.v1), v2.andnot(v.v2)};
		}

		[[nodiscard]] vector next_after() const noexcept
		{
			return {v1.next_after(), v2.next_after()};
		}

		[[nodiscard]] value_type minimal_safe_increment() const noexcept
		{
			const auto vabs = abs();
			return (vabs.next_after() - vabs).max3();
		}

		[[nodiscard]] vector pow(const value_type power) const noexcept
		{
			return {v1.pow(power), v2.pow(power)};
		}

		[[nodiscard]] vector log() const noexcept
		{
			return {v1.log(), v2.log()};
		}

		[[nodiscard]] vector log2() const noexcept
		{
			return {v1.log2(), v2.log2()};
		}

		[[nodiscard]] vector log2approx() const noexcept
		{
			return {v1.log2approx(), v2.log2approx()};
		}

		[[nodiscard]] vector log10() const noexcept
		{
			return {v1.log10(), v2.log10()};
		}

		[[nodiscard]] vector exp() const noexcept
		{
			return {v1.exp(), v2.exp()};
		}

		[[nodiscard]] vector exp2() const noexcept
		{
			return {v1.exp2(), v2.exp2()};
		}

		[[nodiscard]] vector exp2approx() const noexcept
		{
			return {v1.exp2approx(), v2.exp2approx()};
		}

		[[nodiscard]] vector sin() const noexcept
		{
			return {v1.sin(), v2.sin()};
		}

		[[nodiscard]] vector cos() const noexcept
		{
			return {v1.cos(), v2.cos()};
		}

		[[nodiscard]] auto sincos() const noexcept
		{
			return sincos_res{sin(), cos()};
		}

		[[nodiscard]] vector sqrt() const noexcept
		{
			return {v1.sqrt(), v2.sqrt()};
		}

		[[nodiscard]] vector sqrtapprox() const noexcept
		{
			return {v1.sqrtapprox(), v2.sqrtapprox()};
		}

		[[nodiscard]] vector sqrt_scalar() const noexcept
		{
			return {v1.sqrt_scalar(), v2};
		}

		[[nodiscard]] vector sqrtapprox_scalar() const noexcept
		{
			return {v1.sqrtapprox_scalar(), v2};
		}

		[[nodiscard]] vector recsqrt() const noexcept
		{
			return {v1.recsqrt(), v2.recsqrt()};
		}

		[[nodiscard]] vector recsqrtapprox() const noexcept
		{
			return {v1.recsqrtapprox(), v2.recsqrtapprox()};
		}

		[[nodiscard]] vector rotate() const noexcept
		{
			// a b c d -> b c d a
			return {v1.template shuffle<1, 0>(v2), v2.template shuffle<1, 0>(v1)};
		}

		template <int m0, int m1, int m2, int m3>
		[[nodiscard]] vector shuffle(vector v) const noexcept
		{
			const auto ar1 = this->get_array();
			const auto ar2 = v.get_array();

			return {ar1()[m0], ar1()[m1], ar2()[m2], ar2()[m3]};
		}

		template <int m0, int m1, int m2, int m3>
		[[nodiscard]] vector shuffle() const noexcept
		{
			return this->template shuffle<m0, m1, m2, m3>(*this);
		}

		[[nodiscard]] vector movehl(vector v) const noexcept
		{
			vector res;

			res.v1 = v2;
			res.v2 = v.v2;

			return res;
		}

		[[nodiscard]] vector movelh(vector v) const noexcept
		{
			vector res;

			res.v1 = v1;
			res.v2 = v.v1;

			return res;
		}

		[[nodiscard]] int move_mask() const noexcept
		{
			return v1.move_mask() | (v2.move_mask() << 2);
		}

		void reflect(vector fromDir) noexcept
		{
			__m128d tmp1, tmp2;

			tmp1 = _mm_mul_pd(v1.mVector, fromDir.v1.mVector);
			tmp2 = _mm_mul_pd(v2.mVector, fromDir.v2.mVector);
			tmp2 = _mm_add_pd(_mm_hadd_pd(tmp1, tmp1), _mm_hadd_pd(tmp2, tmp2));
			tmp2 = _mm_add_pd(tmp2, tmp2);

			v1.mVector = _mm_mul_pd(_mm_sub_pd(v1.mVector, fromDir.v1.mVector), tmp2);
			v2.mVector = _mm_mul_pd(_mm_sub_pd(v2.mVector, fromDir.v2.mVector), tmp2);

			v1.mVector = _mm_sub_pd(v1.mVector, _mm_mul_pd(fromDir.v1.mVector, tmp2));
			v2.mVector = _mm_sub_pd(v2.mVector, _mm_mul_pd(fromDir.v2.mVector, tmp2));
		}

		[[nodiscard]] vector normalize() const noexcept
		{
			return *this / dot_m(masked{size_v<0xFF>, *this}).sqrt();
		}

		[[nodiscard]] vector normalize_approx() const noexcept
		{
			return *this * dot_m(masked{size_v<0xFF>, *this}).recsqrtapprox();
		}

		[[nodiscard]] vector normalize(const value_type length) const noexcept
		{
			simd_type tv{length};

			return {v1 / tv, v2 / tv};
		}

		[[nodiscard]] vector normalize_point() const noexcept
			requires (CPointVectorTag<category> || CNeutralGeometricVectorTag<category>)
		{
			auto w = v2.template shuffle<1, 1>();

			return {v1 / w, v2 / w};
		}

		[[nodiscard]] auto as_point() const noexcept
		{
			using T = rebind_to_point<vector>;
			auto r = static_cast<T>(*this);

			r.set(1, size_v<3>);

			return r;
		}

		[[nodiscard]] auto as_dir() const noexcept
		{
			using T = rebind_to_dir<vector>;
			auto r = static_cast<T>(*this);

			r.set(0, size_v<3>);

			return r;
		}

		[[nodiscard]] vector cross(vector v) const noexcept
		{
			auto v1a = this->get_array();
			auto v2a = v.get_array();

			return {
				v1a()[1] * v2a()[2] - v1a()[2] * v2a()[1],
				v1a()[2] * v2a()[0] - v1a()[0] * v2a()[2],
				v1a()[0] * v2a()[1] - v1a()[1] * v2a()[0],
				v1a()[3]
			};
		}

		[[nodiscard]] value_type dot(vector v) const noexcept
		{
			return (v1.dot(masked{size_v<0xF1>, v.v1}) + v2.dot(masked{size_v<0xF1>, v.v2})).scalar();
		}

		[[nodiscard]] value_type dot() const noexcept
		{
			return (v1.dot(masked{size_v<0xF1>, v1}) + v2.dot(masked{size_v<0xF1>, v2})).scalar();
		}

		template <szt mask = 0xFF>
		[[nodiscard]] vector dot_m(masked<mask, vector> v) const noexcept
		{
			static constinit const u32
				maskb1 = ~mask&0x3,
				maskb2 = (~mask&0xC) >> 2;

			auto dp =
				v1.dot_m(masked{size_v<0x3 | (mask&0x30)>, v.v.v1}) +
				v2.dot_m(masked{size_v<0x3 | ((mask&0xC0)>>2)>, v.v.v2});

			return {
				dp.template blend<maskb1>(tag::zero),
				dp.template blend<maskb2>(tag::zero)
			};
		}

		template <szt mask = 0xFF>
		[[nodiscard]] value_type dot(masked<mask, vector> v) const noexcept
		{
			return
				v1.dot(masked{size_v<mask&0x11>, v.v.v1}) +
				v2.dot(masked{size_v<((mask&0b1100)>>2)>, v.v.v2});
		}

		[[nodiscard]] value_type length() const noexcept
		{
			return dot_m(masked{size_v<0xF1>, *this}).sqrt_scalar().scalar();
		}

		[[nodiscard]] value_type length_approx() const noexcept
		{
			return dot_m(masked{size_v<0xF1>, *this}).sqrtapprox_scalar().scalar();
		}

		[[nodiscard]] vector min(vector v) const noexcept
		{
			return {v1.min(v.v1), v2.min(v.v2)};
		}

		[[nodiscard]] value_type hmin() const noexcept
		{
			return plmin(v1.hmin(), v2.hmin());
		}

		[[nodiscard]] value_type min3() const noexcept
		{
			return plmin(v1.hmin(), v2[size_v<0>]);
		}

		[[nodiscard]] vector min_scalar(vector v) const noexcept
		{
			vector res{v};

			res.v1 = res.v1.min_scalar(v.v1);

			return res;
		}

		[[nodiscard]] vector max_scalar(vector v) const noexcept
		{
			vector res{v};

			res.v1 = res.v1.max_scalar(v.v1);

			return res;
		}

		[[nodiscard]] vector max(vector v) const noexcept
		{
			return {v1.max(v.v1), v2.max(v.v2)};
		}

		[[nodiscard]] value_type hmax() const noexcept
		{
			return plmax(v1.hmax(), v2.hmax());
		}

		[[nodiscard]] value_type max3() const noexcept
		{
			return plmax(v1.hmax(), v2[size_v<0>]);
		}

		[[nodiscard]] szt max3_index() const noexcept
		{
			auto va1 = v1.get_array();
			auto va2 = v2.get_array();

			value_type maxVal = va1()[0];
			szt maxIndex = 0;

			if(va1()[1] > maxVal)
			{
				maxIndex = 1;
				maxVal = va1()[1];
			}

			if(va2()[0] > maxVal)
				maxIndex = 2;

			return maxIndex;
		}

		[[nodiscard]] szt min3_index() const noexcept
		{
			auto va1 = v1.get_array();
			auto va2 = v2.get_array();

			value_type minVal = va1()[0];
			szt minIndex = 0;

			if(va1()[1] < minVal)
			{
				minIndex = 1;
				minVal = va1()[1];
			}

			if(va2()[0] < minVal)
				minIndex = 2;

			return minIndex;
		}

		[[nodiscard]] szt abs_min3_index() const noexcept
		{
			auto va1 = v1.get_array();
			auto va2 = v2.get_array();

			value_type minVal = plabs(va1()[0]), cVal = plabs(va1()[1]);
			szt minIndex = 0;

			if(cVal < minVal)
			{
				minIndex = 1;
				minVal = cVal;
			}

			if(plabs(va2()[0]) < minVal)
				minIndex = 2;

			return minIndex;
		}

		[[nodiscard]] szt abs_max3_index() const noexcept
		{
			auto va1 = v1.get_array();
			auto va2 = v2.get_array();

			value_type maxVal = plabs(va1()[0]), cVal = plabs(va1()[1]);
			szt maxIndex = 0;

			if(cVal > maxVal)
			{
				maxIndex = 1;
				maxVal = cVal;
			}

			if(plabs(va2()[0]) > maxVal)
				maxIndex = 2;

			return maxIndex;
		}

		[[nodiscard]] vector floor() const noexcept
		{
			return {v1.floor(), v2.floor()};
		}

		[[nodiscard]] vector ceil() const noexcept
		{
			return {v1.ceil(), v2.ceil()};
		}

		[[nodiscard]] vector round() const noexcept
		{
			return {v1.round(), v2.round()};
		}

		[[nodiscard]] vector inverse_sign() const noexcept
		{
			return {v1.inverse_sign(), v2.inverse_sign()};
		}

		[[nodiscard]] vector reciprocal() const noexcept
		{
			return {v1.reciprocal(), v2.reciprocal()};
		}

		[[nodiscard]] vector reciprocalapprox() const noexcept
		{
			return {v1.reciprocalapprox(), v2.reciprocalapprox()};
		}

		[[nodiscard]] vector hmul() const noexcept
		{
			const auto r = v1.hmul() * v2.hmul();
			return {r, r};
		}

		[[nodiscard]] vector hadd() const noexcept
		{
			const auto r = v1.hadd() + v2.hadd();
			return {r, r};
		}

		[[nodiscard]] vector hsub() const noexcept
		{
			const auto r = v1.hsub() - v2.hsub();
			return {r, r};
		}

		[[nodiscard]] vector find_orthogonal() const noexcept
		{
			auto ov = this->get_array();
			const auto ovb = ov;

			const auto maxIndex = abs_max3_index(), nextIndex = maxIndex == 2 ? 0 : maxIndex + 1;

			ov()[nextIndex] = ovb()[maxIndex];
			ov()[maxIndex] = -ovb()[nextIndex];

			const auto orthogonal = cross(vector{ov});

			//orthogonal.set(v1[3], 3);

			return orthogonal;
		}

		[[nodiscard]] int cmp_scalar_ge(vector v) const noexcept
		{
			return _mm_comige_sd(v1.mVector, v.v1.mVector);
		}

		[[nodiscard]] int cmp_scalar_gt(vector v) const noexcept
		{
			return _mm_comigt_sd(v1.mVector, v.v1.mVector);
		}

		[[nodiscard]] int cmp_scalar_eq(vector v) const noexcept
		{
			return _mm_comieq_sd(v1.mVector, v.v1.mVector);
		}

		[[nodiscard]] int cmp_scalar_le(vector v) const noexcept
		{
			return _mm_comile_sd(v1.mVector, v.v1.mVector);
		}

		[[nodiscard]] int cmp_scalar_lt(vector v) const noexcept
		{
			return _mm_comilt_sd(v1.mVector, v.v1.mVector);
		}

		[[nodiscard]] vector is_nan() const noexcept
		{
			return {v1.is_nan(), v2.is_nan()};
		}

		[[nodiscard]] vector is_infinite() const noexcept
		{
			return {v1.is_infinite(), v2.is_infinite()};
		}

		[[nodiscard]] vector is_normal() const noexcept
			{ return {v1.is_normal(), v2.is_normal()}; }

		[[nodiscard]] bool is_all_zero() const noexcept
		{
			return v1.is_all_zero() && v2.is_all_zero();
		}

		using common_op_type::operator+=;
		using common_op_type::operator-=;
		using common_op_type::operator/=;
		using common_op_type::operator*=;
		using common_op_type::operator%=;
		using common_op_type::operator|=;
		using common_op_type::operator&=;
		using common_op_type::operator^=;
		using common_op_type::operator>>=;
		using common_op_type::operator<<=;

		[[nodiscard]] vector operator+ (vector v) const noexcept
		{
			return {v1 + v.v1, v2 + v.v2};
		}

		[[nodiscard]] vector operator- (vector v) const noexcept
		{
			return {v1 - v.v1, v2 - v.v2};
		}

		[[nodiscard]] vector operator* (vector v) const noexcept
		{
			return {v1 * v.v1, v2 * v.v2};
		}

		[[nodiscard]] vector operator/ (vector v) const noexcept
		{
			return {v1 / v.v1, v2 / v.v2};
		}

		[[nodiscard]] vector operator% (vector v) const noexcept
		{
			return {v1 % v.v1, v2 % v.v2};
		}

		[[nodiscard]] vector operator+ (const value_type &v) const noexcept
		{
			simd_type vt{v};
			return {v1 + vt, v2 + vt};
		}

		[[nodiscard]] vector operator- (const value_type &v) const noexcept
		{
			simd_type vt{v};
			return {v1 - vt, v2 - vt};
		}

		[[nodiscard]] vector operator* (const value_type &v) const noexcept
		{
			simd_type vt{v};
			return {v1 * vt, v2 * vt};
		}

		[[nodiscard]] vector operator/ (const value_type &v) const noexcept
		{
			simd_type vt{v};
			return {v1 / vt, v2 / vt};
		}

		[[nodiscard]] vector operator% (const value_type &v) const noexcept
		{
			simd_type vt{v};
			return {v1 % vt, v2 % vt};
		}

		[[nodiscard]] vector operator== (vector v) const noexcept
		{
			return {v1 == v.v1, v2 == v.v2};
		}

		[[nodiscard]] vector operator!= (vector v) const noexcept
		{
			return {v1 != v.v1, v2 != v.v2};
		}

		[[nodiscard]] vector operator< (vector v) const noexcept
		{
			return {v1 < v.v1, v2 < v.v2};
		}

		[[nodiscard]] vector operator<= (vector v) const noexcept
		{
			return {v1 <= v.v1, v2 <= v.v2};
		}

		[[nodiscard]] vector operator> (vector v) const noexcept
		{
			return {v1 > v.v1, v2 > v.v2};
		}

		[[nodiscard]] vector operator>= (vector v) const noexcept
		{
			return {v1 >= v.v1, v2 >= v.v2};
		}

		[[nodiscard]] vector operator== (const value_type v) const noexcept
		{
			simd_type vt{v};
			return {v1 == vt, v2 == vt};
		}

		[[nodiscard]] vector operator!= (const value_type v) const noexcept
		{
			simd_type vt{v};
			return {v1 != vt, v2 != vt};
		}

		[[nodiscard]] vector operator< (const value_type v) const noexcept
		{
			simd_type vt{v};
			return {v1 < vt, v2 < vt};
		}

		[[nodiscard]] vector operator<= (const value_type v) const noexcept
		{
			simd_type vt{v};
			return {v1 <= vt, v2 <= vt};
		}

		[[nodiscard]] vector operator> (const value_type v) const noexcept
		{
			simd_type vt{v};
			return {v1 > vt, v2 > vt};
		}

		[[nodiscard]] vector operator>= (const value_type v) const noexcept
		{
			simd_type vt{v};
			return {v1 >= vt, v2 >= vt};
		}

		[[nodiscard]] vector operator! () const noexcept
		{
			return {!v1, !v2};
		}

		[[nodiscard]] vector operator- () const noexcept
		{
			return vector{-v1, -v2};
		}

		[[nodiscard]] vector operator+ () const noexcept
		{
			return vector{+v1, +v2};
		}

		[[nodiscard]] vector operator& (vector v) const noexcept
		{
			return {v1 & v.v1, v2 & v.v2};
		}

		[[nodiscard]] vector operator| (vector v) const noexcept
		{
			return {v1 | v.v1, v2 | v.v2};
		}

		[[nodiscard]] vector operator^ (vector v) const noexcept
		{
			return {v1 ^ v.v1, v2 ^ v.v2};
		}

		[[nodiscard]] vector operator& (const i64 v) const noexcept
		{
			return {v1 & v, v2 & v};
		}

		[[nodiscard]] vector operator| (const i64 v) const noexcept
		{
			return {v1 | v, v2 | v};
		}

		[[nodiscard]] vector operator^ (const i64 v) const noexcept
		{
			return {v1 ^ v, v2 ^ v};
		}

		[[nodiscard]] vector operator>> (const int v) const noexcept
		{
			return {v1 >> v, v2 >> v};
		}

		[[nodiscard]] vector operator<< (const int v) const noexcept
		{
			return {v1 << v, v2 << v};
		}

		[[nodiscard]] vector operator>> (vector v) const noexcept
		{
			return {v1 >> v.v1, v2 >> v.v2};
		}

		[[nodiscard]] vector operator<< (vector v) const noexcept
		{
			return {v1 << v.v1, v2 << v.v2};
		}

		vector &operator++ () noexcept
			{ ++v1; ++v2; return *this; }

		vector &operator-- () noexcept
			{ --v1; --v2; return *this; }

		vector operator++ (int) noexcept
			{ return {v1++, v2++}; }

		vector operator-- (int) noexcept
			{ return {v1--, v2--}; }

		[[nodiscard]] vector operator~ () const noexcept
		{
			return {~v1, ~v2};
		}

		[[nodiscard]] vector blendv(vector v, vector mask) const noexcept
			{ return {v1.blendv(v.v1, mask.v1), v2.blendv(v.v2, mask.v2)}; }

		template <int mask>
		[[nodiscard]] vector blend(vector v) const noexcept
			{ static constinit const u32 mask2 = mask >> 2; return {v1.template blend<mask&0x3>(v.v1), v2.template blend<mask2>(v.v2)}; }

		[[nodiscard]] vector unpacklo(vector v) const noexcept
			{ return {v1.unpacklo(v.v1), v1.unpackhi(v.v1)}; }

		[[nodiscard]] vector unpackhi(vector v) const noexcept
			{ return {v2.unpacklo(v.v2), v2.unpackhi(v.v2)}; }

		//[[nodiscard]] explicit operator int() const noexcept
			//{ return (int)v1 | ((int)v2 << 2); }

		[[nodiscard]] auto mask() const noexcept
		{
			return std::bitset<NbValues>{static_cast<std::make_unsigned_t<int>>(v1.move_mask() | (v2.move_mask() << 2))};
		}

		[[nodiscard]] explicit operator bool() const noexcept
			{ return static_cast<bool>(v1) & static_cast<bool>(v2); }
	};


	template <>
	class vector<double, size_c<4>, types::simd_vector_t<types::simd_128_t>, categories::color_t> :
		public vector_storage<double, size_c<4>, categories::color_t, vector_simd_128d_4_storage<categories::color_t, vector<double, size_c<4>, types::simd_vector_t<types::simd_128_t>, categories::color_t>>>,
		public vector_base<vector<double, size_c<4>, types::simd_vector_t<types::simd_128_t>, categories::color_t>>,
		public common_op<vector<double, size_c<4>, types::simd_vector_t<types::simd_128_t>, categories::color_t>>
	{
		using common_op_type = common_op<vector<double, size_c<4>, types::simd_vector_t<types::simd_128_t>, categories::color_t>>;
		using storage_type = vector_storage<double, size_c<4>, categories::color_t, vector_simd_128d_4_storage<categories::color_t, vector<double, size_c<4>, types::simd_vector_t<types::simd_128_t>, categories::color_t>>>;

	public:

		using impl_type = types::simd_vector_t<types::simd_128_t>;

		static constexpr double LastValue = 1;
		static constinit const auto NbValues = storage_type::NbValues;
		static constinit const bool HardwareImpl = true;

		using typename storage_type::value_type;
		using typename storage_type::category;
		using storage_type::v1; using storage_type::v2;
		using storage_type::storage_type;

		vector() = default;
		vector(const value_type v) noexcept : storage_type{v, LastValue} {}
		vector(const value_type x, const value_type y, const value_type z) noexcept : storage_type{x, y, z, LastValue} {}
		vector(const tag::identity_t) noexcept { set_identity(); }


		vector &set_identity() noexcept
		{
			v1.set_zero();
			v2.set_all(0, LastValue);
			return *this;
		}

		[[nodiscard]] vector abs() const noexcept
		{
			return {v1.abs(), v2.abs()};
		}

		[[nodiscard]] vector andnot(vector v) const noexcept
		{
			return {v1.andnot(v.v1), v2.andnot(v.v2)};
		}

		template <szt index>
		auto &set(const value_type v, size_c<index> = {}) noexcept
		{
			if constexpr(index > 1)
				v2.set(v, size_v<index - 2>);
			else
				v1.set(v, size_v<index>);

			return *this;
		}

		void get_all(value_type *v) const noexcept
		{
			v1.get_all(v);
			v2.get_all(v+2);
		}

		void get_all_aligned(value_type *v) const noexcept
		{
			v1.get_all_aligned(v);
			v2.get_all_aligned(v+2);
		}

		[[nodiscard]] aligned_array_type get_array() const noexcept
		{
			aligned_array_type a; get_all_aligned(a().data());
			return a;
		}

		vector &set(const value_type v, const int index) noexcept
		{
			if(index > 1)
				v2.set(v, index - 2);
			else
				v1.set(v, index);

			//((value_type *)(&v1.mVector))[index] = v;
			//XMM_INDEX_SET_DOUBLE(v1.mVector, index, v);
			return *this;
		}

		[[nodiscard]] vector next_after() const noexcept
		{
			return {v1.next_after(), v2.next_after()};
		}

		[[nodiscard]] value_type minimal_safe_increment() const noexcept
		{
			const auto vabs = abs();
			return (vabs.next_after() - vabs).max3();
		}

		[[nodiscard]] vector pow(const value_type power) const noexcept
		{
			return {v1.pow(power), v2.pow(power)};
		}

		[[nodiscard]] vector log() const noexcept
		{
			return {v1.log(), v2.log()};
		}

		[[nodiscard]] vector log2() const noexcept
		{
			return {v1.log2(), v2.log2()};
		}

		[[nodiscard]] vector log2approx() const noexcept
		{
			return {v1.log2approx(), v2.log2approx()};
		}

		[[nodiscard]] vector log10() const noexcept
		{
			return {v1.log10(), v2.log10()};
		}

		[[nodiscard]] vector exp() const noexcept
		{
			return {v1.exp(), v2.exp()};
		}

		[[nodiscard]] vector exp2() const noexcept
		{
			return {v1.exp2(), v2.exp2()};
		}

		[[nodiscard]] vector exp2approx() const noexcept
		{
			return {v1.exp2approx(), v2.exp2approx()};
		}

		[[nodiscard]] vector sin() const noexcept
		{
			return {v1.sin(), v2.sin()};
		}

		[[nodiscard]] vector cos() const noexcept
		{
			return {v1.cos(), v2.cos()};
		}

		[[nodiscard]] auto sincos() const noexcept
		{
			return sincos_res{sin(), cos()};
		}

		[[nodiscard]] vector sqrt() const noexcept
		{
			return {v1.sqrt(), v2.sqrt()};
		}

		[[nodiscard]] vector sqrtapprox() const noexcept
		{
			return {v1.sqrtapprox(), v2.sqrtapprox()};
		}

		[[nodiscard]] vector sqrt_scalar() const noexcept
		{
			return {v1.sqrt_scalar(), v2};
		}

		[[nodiscard]] vector sqrtapprox_scalar() const noexcept
		{
			return {v1.sqrtapprox_scalar(), v2};
		}

		[[nodiscard]] vector recsqrt() const noexcept
		{
			return {v1.recsqrt(), v2.recsqrt()};
		}

		[[nodiscard]] vector recsqrtapprox() const noexcept
		{
			return {v1.recsqrtapprox(), v2.recsqrtapprox()};
		}

		[[nodiscard]] vector rotate() const noexcept
		{
			// a b c d -> b c d a
			return {v1.template shuffle<1, 0>(v2), v2.template shuffle<1, 0>(v1)};
		}

		template <int m0, int m1, int m2, int m3>
		[[nodiscard]] vector shuffle(vector v) const noexcept
		{
			const auto ar1 = this->get_array();
			const auto ar2 = v.get_array();

			return {ar1()[m0], ar1()[m1], ar2()[m2], ar2()[m3]};
		}

		template <int m0, int m1, int m2, int m3>
		[[nodiscard]] vector shuffle() const noexcept
		{
			return this->template shuffle<m0, m1, m2, m3>(*this);
		}

		[[nodiscard]] vector movehl(vector v) const noexcept
		{
			vector res;

			res.v1 = v2;
			res.v2 = v.v2;

			return res;
		}

		[[nodiscard]] vector movelh(vector v) const noexcept
		{
			vector res;

			res.v1 = v1;
			res.v2 = v.v1;

			return res;
		}

		[[nodiscard]] int move_mask() const noexcept
		{
			return v1.move_mask() | (v2.move_mask() << 2);
		}

		void reflect(vector fromDir) noexcept
		{
			__m128d tmp1, tmp2;

			tmp1 = _mm_mul_pd(v1.mVector, fromDir.v1.mVector);
			tmp2 = _mm_mul_pd(v2.mVector, fromDir.v2.mVector);
			tmp2 = _mm_add_pd(_mm_hadd_pd(tmp1, tmp1), _mm_hadd_pd(tmp2, tmp2));
			tmp2 = _mm_add_pd(tmp2, tmp2);

			v1.mVector = _mm_mul_pd(_mm_sub_pd(v1.mVector, fromDir.v1.mVector), tmp2);
			v2.mVector = _mm_mul_pd(_mm_sub_pd(v2.mVector, fromDir.v2.mVector), tmp2);

			v1.mVector = _mm_sub_pd(v1.mVector, _mm_mul_pd(fromDir.v1.mVector, tmp2));
			v2.mVector = _mm_sub_pd(v2.mVector, _mm_mul_pd(fromDir.v2.mVector, tmp2));
		}

		[[nodiscard]] vector normalize() const noexcept
		{
			return *this / dot_m(masked{size_v<0xFF>, *this}).sqrt();
		}

		[[nodiscard]] vector normalize_approx() const noexcept
		{
			return *this * dot_m(masked{size_v<0xFF>, *this}).recsqrtapprox();
		}

		[[nodiscard]] vector normalize(const value_type length) const noexcept
		{
			simd_type tv{length};

			return {v1 / tv, v2 / tv};
		}

		[[nodiscard]] vector cross(vector v) const noexcept
		{
			auto v1a = this->get_array();
			auto v2a = v.get_array();

			return {
				v1a()[1] * v2a()[2] - v1a()[2] * v2a()[1],
				v1a()[2] * v2a()[0] - v1a()[0] * v2a()[2],
				v1a()[0] * v2a()[1] - v1a()[1] * v2a()[0],
				v1a()[3]
			};
		}

		[[nodiscard]] value_type dot(vector v) const noexcept
		{
			return (v1.dot_m(masked{size_v<0xF1>, v.v1}) + v2.dot_m(masked{size_v<0xF1>, v.v2})).scalar();
		}

		[[nodiscard]] value_type dot() const noexcept
		{
			return v1.dot() + v2.dot();
		}

		template <szt mask = 0xFF>
		[[nodiscard]] vector dot_m(masked<mask, vector> v) const noexcept
		{
			static constinit const u32
				maskb1 = ~mask&0x3,
				maskb2 = (~mask&0xC) >> 2;

			auto dp =
				v1.dot_m(masked{size_v<0x3 | (mask&0x30)>, v.v.v1}) +
				v2.dot_m(masked{size_v<0x3 | ((mask&0xC0)>>2)>, v.v.v2});

			return {
				dp.template blend<maskb1>(tag::zero),
				dp.template blend<maskb2>(tag::zero)
			};
		}

		template <szt mask = 0xFF>
		[[nodiscard]] value_type dot(masked<mask, vector> v) const noexcept
		{
			return
				v1.dot(masked{size_v<mask&0x11>, v.v.v1}) +
				v2.dot(masked{size_v<((mask&0b1100)>>2)>, v.v.v2});
		}

		[[nodiscard]] value_type length() const noexcept
		{
			return dot_m(masked{size_v<0xF1>, *this}).sqrt_scalar().scalar();
		}

		[[nodiscard]] value_type length_approx() const noexcept
		{
			return dot_m(masked{size_v<0xF1>, *this}).sqrtapprox_scalar().scalar();
		}

		[[nodiscard]] vector min(vector v) const noexcept
		{
			return {v1.min(v.v1), v2.min(v.v2)};
		}

		[[nodiscard]] value_type hmin() const noexcept
		{
			return plmin(v1.hmin(), v2.hmin());
		}

		[[nodiscard]] value_type min3() const noexcept
		{
			return plmin(v1.hmin(), v2[size_v<0>]);
		}

		[[nodiscard]] vector min_scalar(vector v) const noexcept
		{
			vector res{v};

			res.v1 = res.v1.min_scalar(v.v1);

			return res;
		}

		[[nodiscard]] vector max_scalar(vector v) const noexcept
		{
			vector res{v};

			res.v1 = res.v1.max_scalar(v.v1);

			return res;
		}

		[[nodiscard]] vector max(vector v) const noexcept
		{
			return {v1.max(v.v1), v2.max(v.v2)};
		}

		[[nodiscard]] value_type hmax() const noexcept
		{
			return plmax(v1.hmax(), v2.hmax());
		}

		[[nodiscard]] value_type max3() const noexcept
		{
			return plmax(v1.hmax(), v2[size_v<0>]);
		}

		[[nodiscard]] szt max3_index() const noexcept
		{
			auto va1 = v1.get_array();
			auto va2 = v2.get_array();

			value_type maxVal = va1()[0];
			szt maxIndex = 0;

			if(va1()[1] > maxVal)
			{
				maxIndex = 1;
				maxVal = va1()[1];
			}

			if(va2()[0] > maxVal)
				maxIndex = 2;

			return maxIndex;
		}

		[[nodiscard]] szt min3_index() const noexcept
		{
			auto va1 = v1.get_array();
			auto va2 = v2.get_array();

			value_type minVal = va1()[0];
			szt minIndex = 0;

			if(va1()[1] < minVal)
			{
				minIndex = 1;
				minVal = va1()[1];
			}

			if(va2()[0] < minVal)
				minIndex = 2;

			return minIndex;
		}

		[[nodiscard]] szt abs_min3_index() const noexcept
		{
			auto va1 = v1.get_array();
			auto va2 = v2.get_array();

			value_type minVal = plabs(va1()[0]), cVal = plabs(va1()[1]);
			szt minIndex = 0;

			if(cVal < minVal)
			{
				minIndex = 1;
				minVal = cVal;
			}

			if(plabs(va2()[0]) < minVal)
				minIndex = 2;

			return minIndex;
		}

		[[nodiscard]] szt abs_max3_index() const noexcept
		{
			auto va1 = v1.get_array();
			auto va2 = v2.get_array();

			value_type maxVal = plabs(va1()[0]), cVal = plabs(va1()[1]);
			szt maxIndex = 0;

			if(cVal > maxVal)
			{
				maxIndex = 1;
				maxVal = cVal;
			}

			if(plabs(va2()[0]) > maxVal)
				maxIndex = 2;

			return maxIndex;
		}

		[[nodiscard]] vector floor() const noexcept
		{
			return {v1.floor(), v2.floor()};
		}

		[[nodiscard]] vector ceil() const noexcept
		{
			return {v1.ceil(), v2.ceil()};
		}

		[[nodiscard]] vector round() const noexcept
		{
			return {v1.round(), v2.round()};
		}

		[[nodiscard]] vector inverse_sign() const noexcept
		{
			return {v1.inverse_sign(), v2.inverse_sign()};
		}

		[[nodiscard]] vector reciprocal() const noexcept
		{
			return {v1.reciprocal(), v2.reciprocal()};
		}

		[[nodiscard]] vector reciprocalapprox() const noexcept
		{
			return {v1.reciprocalapprox(), v2.reciprocalapprox()};
		}

		[[nodiscard]] vector hmul() const noexcept
		{
			const auto r = v1.hmul() * v2.hmul();
			return {r, r};
		}

		[[nodiscard]] vector hadd() const noexcept
		{
			const auto r = v1.hadd() + v2.hadd();
			return {r, r};
		}

		[[nodiscard]] vector hsub() const noexcept
		{
			const auto r = v1.hsub() - v2.hsub();
			return {r, r};
		}

		[[nodiscard]] int cmp_scalar_ge(vector v) const noexcept
		{
			return _mm_comige_sd(v1.mVector, v.v1.mVector);
		}

		[[nodiscard]] int cmp_scalar_gt(vector v) const noexcept
		{
			return _mm_comigt_sd(v1.mVector, v.v1.mVector);
		}

		[[nodiscard]] int cmp_scalar_eq(vector v) const noexcept
		{
			return _mm_comieq_sd(v1.mVector, v.v1.mVector);
		}

		[[nodiscard]] int cmp_scalar_le(vector v) const noexcept
		{
			return _mm_comile_sd(v1.mVector, v.v1.mVector);
		}

		[[nodiscard]] int cmp_scalar_lt(vector v) const noexcept
		{
			return _mm_comilt_sd(v1.mVector, v.v1.mVector);
		}

		[[nodiscard]] vector is_nan() const noexcept
		{
			return {v1.is_nan(), v2.is_nan()};
		}

		[[nodiscard]] vector is_infinite() const noexcept
		{
			return {v1.is_infinite(), v2.is_infinite()};
		}

		[[nodiscard]] vector is_normal() const noexcept
			{ return {v1.is_normal(), v2.is_normal()}; }

		[[nodiscard]] bool is_all_zero() const noexcept
		{
			return v1.is_all_zero() && v2.is_all_zero();
		}

		using common_op_type::operator+=;
		using common_op_type::operator-=;
		using common_op_type::operator/=;
		using common_op_type::operator*=;
		using common_op_type::operator%=;
		using common_op_type::operator|=;
		using common_op_type::operator&=;
		using common_op_type::operator^=;
		using common_op_type::operator>>=;
		using common_op_type::operator<<=;

		[[nodiscard]] vector operator+ (vector v) const noexcept
		{
			return {v1 + v.v1, v2 + v.v2};
		}

		[[nodiscard]] vector operator- (vector v) const noexcept
		{
			return {v1 - v.v1, v2 - v.v2};
		}

		[[nodiscard]] vector operator* (vector v) const noexcept
		{
			return {v1 * v.v1, v2 * v.v2};
		}

		[[nodiscard]] vector operator/ (vector v) const noexcept
		{
			return {v1 / v.v1, v2 / v.v2};
		}

		[[nodiscard]] vector operator% (vector v) const noexcept
		{
			return {v1 % v.v1, v2 % v.v2};
		}

		[[nodiscard]] vector operator+ (const value_type &v) const noexcept
		{
			simd_type vt{v};
			return {v1 + vt, v2 + vt};
		}

		[[nodiscard]] vector operator- (const value_type &v) const noexcept
		{
			simd_type vt{v};
			return {v1 - vt, v2 - vt};
		}

		[[nodiscard]] vector operator* (const value_type &v) const noexcept
		{
			simd_type vt{v};
			return {v1 * vt, v2 * vt};
		}

		[[nodiscard]] vector operator/ (const value_type &v) const noexcept
		{
			simd_type vt{v};
			return {v1 / vt, v2 / vt};
		}

		[[nodiscard]] vector operator% (const value_type &v) const noexcept
		{
			simd_type vt{v};
			return {v1 % vt, v2 % vt};
		}

		[[nodiscard]] vector operator== (vector v) const noexcept
		{
			return {v1 == v.v1, v2 == v.v2};
		}

		[[nodiscard]] vector operator!= (vector v) const noexcept
		{
			return {v1 != v.v1, v2 != v.v2};
		}

		[[nodiscard]] vector operator< (vector v) const noexcept
		{
			return {v1 < v.v1, v2 < v.v2};
		}

		[[nodiscard]] vector operator<= (vector v) const noexcept
		{
			return {v1 <= v.v1, v2 <= v.v2};
		}

		[[nodiscard]] vector operator> (vector v) const noexcept
		{
			return {v1 > v.v1, v2 > v.v2};
		}

		[[nodiscard]] vector operator>= (vector v) const noexcept
		{
			return {v1 >= v.v1, v2 >= v.v2};
		}

		[[nodiscard]] vector operator== (const value_type v) const noexcept
		{
			simd_type vt{v};
			return {v1 == vt, v2 == vt};
		}

		[[nodiscard]] vector operator!= (const value_type v) const noexcept
		{
			simd_type vt{v};
			return {v1 != vt, v2 != vt};
		}

		[[nodiscard]] vector operator< (const value_type v) const noexcept
		{
			simd_type vt{v};
			return {v1 < vt, v2 < vt};
		}

		[[nodiscard]] vector operator<= (const value_type v) const noexcept
		{
			simd_type vt{v};
			return {v1 <= vt, v2 <= vt};
		}

		[[nodiscard]] vector operator> (const value_type v) const noexcept
		{
			simd_type vt{v};
			return {v1 > vt, v2 > vt};
		}

		[[nodiscard]] vector operator>= (const value_type v) const noexcept
		{
			simd_type vt{v};
			return {v1 >= vt, v2 >= vt};
		}

		[[nodiscard]] vector operator! () const noexcept
		{
			return {!v1, !v2};
		}

		[[nodiscard]] vector operator- () const noexcept
		{
			return vector{-v1, -v2};
		}

		[[nodiscard]] vector operator+ () const noexcept
		{
			return vector{+v1, +v2};
		}

		[[nodiscard]] vector operator& (vector v) const noexcept
		{
			return {v1 & v.v1, v2 & v.v2};
		}

		[[nodiscard]] vector operator| (vector v) const noexcept
		{
			return {v1 | v.v1, v2 | v.v2};
		}

		[[nodiscard]] vector operator^ (vector v) const noexcept
		{
			return {v1 ^ v.v1, v2 ^ v.v2};
		}

		[[nodiscard]] vector operator& (const i64 v) const noexcept
		{
			return {v1 & v, v2 & v};
		}

		[[nodiscard]] vector operator| (const i64 v) const noexcept
		{
			return {v1 | v, v2 | v};
		}

		[[nodiscard]] vector operator^ (const i64 v) const noexcept
		{
			return {v1 ^ v, v2 ^ v};
		}

		[[nodiscard]] vector operator>> (const int v) const noexcept
		{
			return {v1 >> v, v2 >> v};
		}

		[[nodiscard]] vector operator<< (const int v) const noexcept
		{
			return {v1 << v, v2 << v};
		}

		[[nodiscard]] vector operator>> (vector v) const noexcept
		{
			return {v1 >> v.v1, v2 >> v.v2};
		}

		[[nodiscard]] vector operator<< (vector v) const noexcept
		{
			return {v1 << v.v1, v2 << v.v2};
		}

		vector &operator++ () noexcept
			{ ++v1; ++v2; return *this; }

		vector &operator-- () noexcept
			{ --v1; --v2; return *this; }

		vector operator++ (int) noexcept
			{ return {v1++, v2++}; }

		vector operator-- (int) noexcept
			{ return {v1--, v2--}; }

		[[nodiscard]] vector operator~ () const noexcept
		{
			return {~v1, ~v2};
		}

		[[nodiscard]] vector blendv(vector v, vector mask) const noexcept
			{ return {v1.blendv(v.v1, mask.v1), v2.blendv(v.v2, mask.v2)}; }

		template <int mask>
		[[nodiscard]] vector blend(vector v) const noexcept
			{ static constinit const u32 mask2 = mask >> 2; return {v1.template blend<mask&0x3>(v.v1), v2.template blend<mask2>(v.v2)}; }

		[[nodiscard]] vector unpacklo(vector v) const noexcept
			{ return {v1.unpacklo(v.v1), v1.unpackhi(v.v1)}; }

		[[nodiscard]] vector unpackhi(vector v) const noexcept
			{ return {v2.unpacklo(v.v2), v2.unpackhi(v.v2)}; }

		//[[nodiscard]] explicit operator int() const noexcept
			//{ return (int)v1 | ((int)v2 << 2); }

		[[nodiscard]] auto mask() const noexcept
		{
			return std::bitset<NbValues>{static_cast<std::make_unsigned_t<int>>(v1.move_mask() | (v2.move_mask() << 2))};
		}

		[[nodiscard]] explicit operator bool() const noexcept
			{ return static_cast<bool>(v1) & static_cast<bool>(v2); }
	};
}
