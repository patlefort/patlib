/*
	PatLib

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <utility>
#include <type_traits>
#include <cstddef>

#include <patlib/type.hpp>
#include <patlib/math.hpp>
#include <patlib/utility.hpp>

#include "base.hpp"

namespace patlib::simd
{
	template <szt NB_, CVector VT_>
	[[nodiscard]] constexpr std::bitset<NB_> mask_nb(size_c<NB_>, const VT_ &v) noexcept
		{ return v.mask().to_ullong(); }

	namespace impl
	{
		// Operators

		template<CVector ResVT_, CVector T1_, CVector T2_>
		[[nodiscard]] constexpr ResVT_ apply_op(const type_c<ResVT_>, const T1_ v1, const T2_ v2, auto&& op) noexcept
		{
			if constexpr(ResVT_::HardwareImpl)
				return PL_FWD(op)(static_cast<ResVT_>(v1), static_cast<ResVT_>(v2));
			else
			{
				constexpr auto MinNbValues = std::min(T1_::NbValues, T2_::NbValues);

				const auto &va1 = vector_access(v1);
				const auto &va2 = vector_access(v2);

				return construct_from_index_sequence<ResVT_>(size_v<MinNbValues>, [&](auto&& i){ return PL_FWD(op)(va1[i], va2[i]); });
			}
		}

		template<CVector ResVT_, typename T1_, CVector T2_>
		[[nodiscard]] constexpr ResVT_ apply_op(const type_c<ResVT_>, const T1_ v1, const T2_ v2, auto&& op) noexcept
		{
			if constexpr(ResVT_::HardwareImpl)
				return PL_FWD(op)(static_cast<ResVT_>(v1), static_cast<ResVT_>(v2));
			else
			{
				const auto &va = vector_access(v2);
				return construct_from_index_sequence<ResVT_>(size_v<T2_::NbValues>, [&](auto&& i){ return PL_FWD(op)(v1, va[i]); });
			}
		}

		template<CVector ResVT_, CVector T1_, typename T2_>
		[[nodiscard]] constexpr ResVT_ apply_op(const type_c<ResVT_>, const T1_ v1, const T2_ v2, auto&& op) noexcept
		{
			if constexpr(ResVT_::HardwareImpl)
				return PL_FWD(op)(static_cast<ResVT_>(v1), static_cast<typename ResVT_::value_type>(v2));
			else
			{
				const auto &va = vector_access(v1);
				return construct_from_index_sequence<ResVT_>(size_v<T1_::NbValues>, [&](auto&& i){ return PL_FWD(op)(va[i], v2); });
			}
		}

		template <typename Category_, typename ResTag_>
		using SelectTag = std::conditional_t<std::is_void_v<ResTag_>, Category_, ResTag_>;

		template <typename Type_, typename NbValues_, typename Implem_, typename Category_>
		using SelectVectorType = std::conditional_t<
			CValidVectorImpl<vector<Type_, NbValues_, Implem_, Category_>>,
			vector<Type_, NbValues_, Implem_, Category_>,
			best_vector_t<Type_, NbValues_, Category_>
		>;

		template <typename Type_, CVector VT_>
		using rebind_best_vector = SelectVectorType<Type_, size_c<VT_::NbValues>, typename VT_::impl_type, typename VT_::category>;

		// do_op apply the operation $op on 2 values where at least one is a vector and tries to
		// deduce the type of the new vector based on decltype and some rules.
		template<typename OP_,
			typename Type1_, typename NbValues1_, typename Implem1_, typename Cat1_,
			typename T_, CVectorTag ResTag_ = void
			>
		[[nodiscard]] constexpr auto do_op(const vector<Type1_, NbValues1_, Implem1_, Cat1_> v1, const T_ v2, OP_&& op, type_c<ResTag_> = {}) noexcept
		{
			if constexpr(!std::is_convertible_v<T_, Type1_>)
			{
				using VTT1 = vector<Type1_, NbValues1_, Implem1_, SelectTag<Cat1_, ResTag_>>;

				static_assert(CInitTag<T_> || std::convertible_to<T_, VTT1>, "Right-hand side cannot be converted to left-hand vector type.");

				return PL_FWD(op)(v1, static_cast<VTT1>(v2));
			}
			else
			{
				// Priorise the type of the vector if both are integral or float
				using TT = decltype(PL_FWD(op)(v1[size_v<0>], v2));
				using ResType = std::conditional_t<
					(CNumberPackFloat<TT> && CNumberPackFloat<Type1_>) ||
					(CNumberPackInteger<TT> && CNumberPackInteger<Type1_> ||
					vector<Type1_, NbValues1_, Implem1_, Cat1_>::HardwareImpl),
					Type1_,
					TT
				>;
				using FinalType = SelectVectorType<ResType, NbValues1_, Implem1_, SelectTag<Cat1_, ResTag_>>;

				return apply_op(type_v<FinalType>, v1, v2, PL_FWD(op));
			}
		}

		template<typename OP_,
			typename Type1_, typename NbValues1_, typename Implem1_, typename Cat1_,
			typename T_, CVectorTag ResTag_ = void
			>
		[[nodiscard]] constexpr auto do_op(const T_ v1, const vector<Type1_, NbValues1_, Implem1_, Cat1_> v2, OP_&& op, type_c<ResTag_> = {}) noexcept
		{
			if constexpr(!std::is_convertible_v<T_, Type1_>)
			{
				using VTT1 = vector<Type1_, NbValues1_, Implem1_, SelectTag<Cat1_, ResTag_>>;

				static_assert(CInitTag<T_> || std::convertible_to<T_, VTT1>, "Left-hand side cannot be converted to right-hand vector type.");

				return PL_FWD(op)(static_cast<VTT1>(v1), v2);
			}
			else
			{
				// Priorise the type of the vector if both are integral or float
				using TT = decltype(PL_FWD(op)(v1, v2[size_v<0>]));
				using ResType = std::conditional_t<
					(CNumberPackFloat<TT> && CNumberPackFloat<Type1_>) ||
					((CNumberPackInteger<TT> && CNumberPackInteger<Type1_>) ||
					vector<Type1_, NbValues1_, Implem1_, Cat1_>::HardwareImpl),
					Type1_,
					TT
				>;
				using FinalType = SelectVectorType<ResType, NbValues1_, Implem1_, SelectTag<Cat1_, ResTag_>>;

				return apply_op(type_v<FinalType>, v1, v2, PL_FWD(op));
			}
		}

		template<typename OP_,
			typename Type1_, typename NbValues1_, typename Implem1_, typename Cat1_,
			typename Type2_, typename NbValues2_, typename Implem2_, typename Cat2_,
			CVectorTag ResTag_ = void
		>
		[[nodiscard]] constexpr auto do_op(const vector<Type1_, NbValues1_, Implem1_, Cat1_> v1, const vector<Type2_, NbValues2_, Implem2_, Cat2_> v2, OP_&& op, type_c<ResTag_> = {}, size_c<0> = {}) noexcept
		{
			using ResType = decltype(PL_FWD(op)(v1[size_v<0>], v2[size_v<0>]));

			if constexpr(std::is_void_v<ResTag_>)
			{
				if constexpr(!std::is_same_v<Type1_, Type2_> && std::is_same_v<ResType, Type2_>)
				{
					static_assert(std::convertible_to<vector<Type1_, NbValues1_, Implem1_, Cat1_>, vector<Type2_, NbValues2_, Implem2_, Cat2_>>, "Left-hand vector not convertible to right-hand type.");

					using RT = std::conditional_t<
						vector<Type2_, NbValues2_, Implem2_, Cat2_>::HardwareImpl,
						Type2_,
						ResType
					>;

					return apply_op(type_v<SelectVectorType<RT, NbValues2_, Implem2_, Cat2_>>, v1, v2, PL_FWD(op));
				}
				else
				{
					static_assert(std::convertible_to<vector<Type2_, NbValues2_, Implem2_, Cat2_>, vector<Type1_, NbValues1_, Implem1_, Cat1_>>, "Right-hand vector not convertible to left-hand type.");

					using RT = std::conditional_t<
						vector<Type1_, NbValues1_, Implem1_, Cat1_>::HardwareImpl,
						Type1_,
						ResType
					>;

					return apply_op(type_v<SelectVectorType<RT, NbValues1_, Implem1_, Cat1_>>, v1, v2, PL_FWD(op));
				}
			}
			else
			{
				if constexpr(!std::is_same_v<Type1_, Type2_> && std::is_same_v<ResType, Type2_>)
				{
					using RT = std::conditional_t<
						vector<Type2_, NbValues2_, Implem2_, Cat2_>::HardwareImpl,
						Type2_,
						ResType
					>;

					return apply_op(type_v<SelectVectorType<RT, NbValues2_, Implem2_, ResTag_>>, v1, v2, PL_FWD(op));
				}
				else
				{
					using RT = std::conditional_t<
						vector<Type1_, NbValues1_, Implem1_, Cat1_>::HardwareImpl,
						Type1_,
						ResType
					>;

					return apply_op(type_v<SelectVectorType<RT, NbValues1_, Implem1_, ResTag_>>, v1, v2, PL_FWD(op));
				}
			}
		}

		// Operations between a point and a direction vertex will return a point vertex by default
		template<typename OP_,
			typename Type1_, typename NbValues1_, typename Implem1_, szt NbAxis_,
			typename Type2_, typename NbValues2_, typename Implem2_
			>
		[[nodiscard]] constexpr auto do_op(const vector<Type1_, NbValues1_, Implem1_, categories::geometric_t<categories::geometric_point_t, NbAxis_>> v1, const vector<Type2_, NbValues2_, Implem2_, categories::geometric_t<categories::geometric_dir_t, NbAxis_>> v2, OP_&& op) noexcept
			{ return do_op(v1, v2, PL_FWD(op), type_v<categories::geometric_t<categories::geometric_point_t, NbAxis_>>); }

		template<typename OP_,
			typename Type1_, typename NbValues1_, typename Implem1_, szt NbAxis_,
			typename Type2_, typename NbValues2_, typename Implem2_
			>
		[[nodiscard]] constexpr auto do_op(const vector<Type1_, NbValues1_, Implem1_, categories::geometric_t<categories::geometric_dir_t, NbAxis_>> v1, const vector<Type2_, NbValues2_, Implem2_, categories::geometric_t<categories::geometric_point_t, NbAxis_>> v2, OP_&& op) noexcept
			{ return do_op(v1, v2, PL_FWD(op), type_v<categories::geometric_t<categories::geometric_point_t, NbAxis_>>); }

		// Operations on only one vector
		template<CVector VT1_>
		[[nodiscard]] constexpr auto do_op(const VT1_ v1, auto&& op) noexcept
		{
			const auto &va1 = vector_access(v1);
			return construct_from_index_sequence<VT1_>(size_v<VT1_::NbValues>, [&](auto&& i){ return PL_FWD(op)(va1[i]); });
		}


		#define PL_VECTOR_OP_IMPL(op) \
			template <CVector T1_, CVector T2_> \
			[[nodiscard]] constexpr auto operator op(const T1_ v1, const T2_ v2) noexcept \
				requires (CNumberPack<typename T1_::value_type> && CNumberPack<typename T2_::value_type> && !std::same_as<T2_, typename T1_::value_type> && !std::same_as<T1_, typename T2_::value_type>) \
				{ return do_op(v1, v2, [](auto&& va1, auto&& va2){ return va1 op va2; }); } \
				 \
			template <CVector T1_> \
			[[nodiscard]] constexpr auto operator op(const T1_ v1, const typename T1_::value_type v2) noexcept \
				requires CNumberPack<typename T1_::value_type> \
				{ return do_op(v1, v2, [](auto&& va1, auto&& va2){ return va1 op va2; }); } \
				 \
			template <CVector T1_, typename T_> \
			[[nodiscard]] constexpr auto operator op(const T1_ v1, const T_ v2) noexcept \
				requires (CNumberPack<typename T1_::value_type> && !CVector<T_> && !std::same_as<typename T1_::value_type, T_> && std::convertible_to<T_, T1_>) \
				{ return do_op(v1, v2, [](auto&& va1, auto&& va2){ return va1 op va2; }); } \
				 \
			template <CVector T1_, typename T_> \
			[[nodiscard]] constexpr auto operator op(const T_ v1, const T1_ v2) noexcept \
				requires (CNumberPack<typename T1_::value_type> && !CVector<T_> && !std::same_as<typename T1_::value_type, T_> && std::convertible_to<T_, T1_>) \
				{ return do_op(v1, v2, [](auto&& va1, auto&& va2){ return va1 op va2; }); } \
				 \
			template <CVector VT_> \
			[[nodiscard]] constexpr auto operator op(const typename VT_::value_type v1, const VT_ vt) noexcept \
				requires CNumberPack<typename VT_::value_type> \
				{ return VT_{v1} op vt; }

		#define PL_VECTOR_BITSOP_IMPL(op) \
			template <CVector T1_, CVector T2_> \
			[[nodiscard]] constexpr auto operator op(const T1_ v1, const T2_ v2) noexcept \
				requires (CBitFieldPack<typename T1_::value_type> && !std::same_as<T2_, typename T1_::value_type> && !std::same_as<T1_, typename T2_::value_type>) \
				{ return do_op(v1, v2, [](auto&& va1, auto&& va2){ return va1 op va2; }); } \
				 \
			template <CVector T1_> \
			[[nodiscard]] constexpr auto operator op(const T1_ v1, const typename T1_::value_type v2) noexcept \
				requires CBitFieldPack<typename T1_::value_type> \
				{ return do_op(v1, v2, [](auto&& va1, auto&& va2){ return va1 op va2; }); } \
				 \
			template <CVector T1_, typename T_> \
			[[nodiscard]] constexpr auto operator op(const T1_ v1, const T_ v2) noexcept \
				requires (CBitFieldPack<typename T1_::value_type> && !CVector<T_> && !std::same_as<typename T1_::value_type, T_> && std::convertible_to<T_, T1_>) \
				{ return do_op(v1, v2, [](auto&& va1, auto&& va2){ return va1 op va2; }); } \
				 \
			template <CVector T1_, typename T_> \
			[[nodiscard]] constexpr auto operator op(const T_ v1, const T1_ v2) noexcept \
				requires (CBitFieldPack<typename T1_::value_type> && !CVector<T_> && !std::same_as<typename T1_::value_type, T_> && std::convertible_to<T_, T1_>) \
				{ return do_op(v1, v2, [](auto&& va1, auto&& va2){ return va1 op va2; }); } \
				 \
			template <CVector VT_> \
			[[nodiscard]] constexpr auto operator op(const typename VT_::value_type v1, const VT_ vt) noexcept \
				requires CBitFieldPack<typename VT_::value_type> \
				{ return VT_{v1} op vt; }

		#define PL_VECTOR_SHIFTOP_IMPL(op) \
			template <CIntegralVector T2_, typename ValType1_, typename... T1s_> \
			[[nodiscard]] constexpr auto operator op(const vector<ValType1_, T1s_...> v1, const T2_ v2) noexcept \
				requires (vector<ValType1_, T1s_...>::Shiftable && CBitFieldPack<ValType1_> && CShiftable<ValType1_>) \
				{ return apply_op(type_v<vector<ValType1_, T1s_...>>, v1, v2, [](auto&& va1, auto&& va2){ return va1 op va2; }); } \
				 \
			template <typename ValType1_, typename... T1s_> \
			[[nodiscard]] constexpr auto operator op(const vector<ValType1_, T1s_...> v1, const int v2) noexcept \
				requires (vector<ValType1_, T1s_...>::Shiftable && CBitFieldPack<ValType1_> && CShiftable<ValType1_>) \
				{ return apply_op(type_v<vector<ValType1_, T1s_...>>, v1, v2, [](auto&& va1, auto&& va2){ return va1 op va2; }); }

		#define PL_VECTOR_CMP_OP_IMPL(op) \
			template <CVector T1_, CVector T2_> \
			[[nodiscard]] constexpr auto operator op(const T1_ v1, const T2_ v2) noexcept \
				{ return do_op(v1, v2, [](auto&& va1, auto&& va2){ return va1 op va2; }); } \
				 \
			template <CVector T1_> \
			[[nodiscard]] constexpr auto operator op(const T1_ v1, const typename T1_::value_type v2) noexcept \
				{ return do_op(v1, v2, [](auto&& va1, auto&& va2){ return va1 op va2; }); } \
				 \
			template <CVector T1_, typename T_> \
			[[nodiscard]] constexpr auto operator op(const T1_ v1, const T_ v2) noexcept \
				requires (!CVector<T_> && !std::same_as<typename T1_::value_type, T_> && std::convertible_to<T_, T1_>) \
				{ return do_op(v1, v2, [](auto&& va1, auto&& va2){ return va1 op va2; }); } \
				 \
			template <CVector T1_, typename T_> \
			[[nodiscard]] constexpr auto operator op(const T_ v1, const T1_ v2) noexcept \
				requires (!CVector<T_> && !std::same_as<typename T1_::value_type, T_> && std::convertible_to<T_, T1_>) \
				{ return do_op(v1, v2, [](auto&& va1, auto&& va2){ return va1 op va2; }); } \
				 \
			template <CVector VT_> \
			[[nodiscard]] constexpr auto operator op(const typename VT_::value_type v1, const VT_ vt) noexcept \
				requires CNumberPack<typename VT_::value_type> \
				{ return VT_{v1} op vt; }

		PL_VECTOR_CMP_OP_IMPL(==)
		PL_VECTOR_CMP_OP_IMPL(!=)
		PL_VECTOR_CMP_OP_IMPL(>=)
		PL_VECTOR_CMP_OP_IMPL(>)
		PL_VECTOR_CMP_OP_IMPL(<)
		PL_VECTOR_CMP_OP_IMPL(<=)
		PL_VECTOR_OP_IMPL(+)
		PL_VECTOR_OP_IMPL(-)
		PL_VECTOR_OP_IMPL(*)
		PL_VECTOR_OP_IMPL(/)
		PL_VECTOR_BITSOP_IMPL(|)
		PL_VECTOR_BITSOP_IMPL(&)
		PL_VECTOR_BITSOP_IMPL(^)
		PL_VECTOR_SHIFTOP_IMPL(<<)
		PL_VECTOR_SHIFTOP_IMPL(>>)

		template <typename T1_, typename T2_, typename... Ts1_, typename... Ts2_>
		[[nodiscard]] constexpr auto operator +(vector<T1_, Ts1_..., categories::geometric_t<categories::geometric_point_t>> v1, vector<T2_, Ts2_..., categories::geometric_t<categories::geometric_dir_t>> v2) noexcept
			requires (CNumberPack<T1_> && CNumberPack<T2_>)
			{ return do_op(v1, v2, [](auto&& va1, auto&& va2){ return va1 + va2; }, type_v<categories::geometric_t<categories::geometric_point_t>>); }

		template <typename T1_, typename T2_, typename... Ts1_, typename... Ts2_>
		[[nodiscard]] constexpr auto operator +(vector<T1_, Ts1_..., categories::geometric_t<categories::geometric_dir_t>> v1, vector<T2_, Ts2_..., categories::geometric_t<categories::geometric_point_t>> v2) noexcept
			requires (CNumberPack<T1_> && CNumberPack<T2_>)
			{ return do_op(v1, v2, [](auto&& va1, auto&& va2){ return va1 + va2; }, type_v<categories::geometric_t<categories::geometric_point_t>>); }

		template <typename T1_, typename T2_, typename... Ts1_, typename... Ts2_>
		[[nodiscard]] constexpr auto operator -(vector<T1_, Ts1_..., categories::geometric_t<categories::geometric_point_t>> v1, vector<T2_, Ts2_..., categories::geometric_t<categories::geometric_dir_t>> v2) noexcept
			requires (CNumberPack<T1_> && CNumberPack<T2_>)
			{ return do_op(v1, v2, [](auto&& va1, auto&& va2){ return va1 - va2; }, type_v<categories::geometric_t<categories::geometric_point_t>>); }

		template <typename T1_, typename T2_, typename... Ts1_, typename... Ts2_>
		[[nodiscard]] constexpr auto operator -(vector<T1_, Ts1_..., categories::geometric_t<categories::geometric_point_t>> v1, vector<T2_, Ts2_..., categories::geometric_t<categories::geometric_point_t>> v2) noexcept
			requires (CNumberPack<T1_> && CNumberPack<T2_>)
			{ return do_op(v1, v2, [](auto&& va1, auto&& va2){ return va1 - va2; }, type_v<categories::geometric_t<categories::geometric_dir_t>>); }

		// Modulo
		template <CVector T1_, CVector T2_>
		[[nodiscard]] constexpr auto operator %(const T1_ v1, const T2_ v2) noexcept
			requires (CNumberPack<typename T1_::value_type> && CNumberPack<typename T2_::value_type>)
			{ return do_op(v1, v2, [](auto&& va1, auto&& va2){ return mod(va1, va2); }); }

		template <CVector T1_>
		[[nodiscard]] constexpr auto operator %(const T1_ v1, const typename T1_::value_type v2) noexcept
			requires CNumberPack<typename T1_::value_type>
			{ return do_op(v1, v2, [](auto&& va1, auto&& va2){ return mod(va1, va2); }); }

		template <CVector T1_, typename T_>
		[[nodiscard]] constexpr auto operator %(const T1_ v1, const T_ v2) noexcept
			requires (CNumberPack<typename T1_::value_type> && !CVector<T_> && !std::is_same_v<typename T1_::value_type, T_> && std::is_convertible_v<T_, T1_>)
			{ return do_op(v1, v2, [](auto&& va1, auto&& va2){ return mod(va1, va2); }); }

		template <CVector T1_, typename T_>
		[[nodiscard]] constexpr auto operator %(const T_ v1, const T1_ &v2) noexcept
			requires (CNumberPack<typename T1_::value_type> && !CVector<T_> && !std::is_same_v<typename T1_::value_type, T_> && std::is_convertible_v<T_, T1_>)
			{ return do_op(v1, v2, [](auto&& va1, auto&& va2){ return mod(va1, va2); }); }

		template <CVector VT_>
		[[nodiscard]] constexpr auto operator %(const typename VT_::value_type v1, const VT_ vt) noexcept
			requires CNumberPack<typename VT_::value_type>
			{ return VT_{v1} % vt; }


		template <CVector VT_>
		[[nodiscard]] constexpr auto plmin_index(const VT_ v, const std::bitset<VT_::NbValues> &mask) noexcept
			-> std::pair<int, typename VT_::value_type>
		{
			const auto &ar = vector_access(v);
			int index = -1;

			auto res = constants::infhighest<underlying_type_t<VT_>>;

			for(szt i=0; i<ar.size(); ++i)
			{
				if(mask[i] && ar[i] < res)
				{
					res = ar[i];
					index = i;
				}
			}

			return {index, res};
		}

		template <CVector VT_>
		[[nodiscard]] constexpr auto plmin_index(const VT_ v) noexcept
		{
			const auto &ar = vector_access(v);
			int index = -1;
			auto minv = constants::infhighest<underlying_type_t<VT_>>;

			for(szt i=0; i<ar.size(); ++i)
			{
				if(ar[i] < minv)
				{
					minv = ar[i];
					index = i;
				}
			}

			return index;
		}

		template <CVector VT_>
		[[nodiscard]] constexpr auto plmax_index(const VT_ v, const std::bitset<VT_::NbValues> &mask) noexcept
			-> std::pair<int, typename VT_::value_type>
		{
			const auto &ar = vector_access(v);
			int index = -1;

			auto res = constants::inflowest<underlying_type_t<VT_>>;

			for(szt i=0; i<ar.size(); ++i)
			{
				if(mask[i] && ar[i] > res)
				{
					res = ar[i];
					index = i;
				}
			}

			return {index, res};
		}

		template <CVector VT_>
		[[nodiscard]] constexpr auto plmax_index(const VT_ v) noexcept
		{
			const auto &ar = vector_access(v);
			int index = -1;
			auto maxv = constants::inflowest<underlying_type_t<VT_>>;

			for(szt i=0; i<ar.size(); ++i)
			{
				if(ar[i] > maxv)
				{
					maxv = ar[i];
					index = i;
				}
			}

			return index;
		}

		#define PL_VECTOR_OVERLOAD_FC1(fc, tag, ...) \
			template <typename Type1_, typename NbValues1_, typename Implem1_, typename Cat1_> \
			[[nodiscard]] constexpr auto fc(const vector<Type1_, NbValues1_, Implem1_, Cat1_> v1) noexcept \
				requires ((__VA_ARGS__) && requires(vector<Type1_, NbValues1_, Implem1_, Cat1_> v) { {v.tag()} -> CNotVoid; }) \
				{ return v1.tag(); } \
			\
			template < \
				typename Type1_, typename NbValues1_, typename Implem1_, typename Cat1_ \
				> \
			[[nodiscard]] constexpr auto fc(const vector<Type1_, NbValues1_, Implem1_, Cat1_> v1) noexcept \
				requires ((__VA_ARGS__) && !(requires(vector<Type1_, NbValues1_, Implem1_, Cat1_> v) { {v.tag()} -> CNotVoid; })) \
			{ \
				using VTT1 = vector<Type1_, NbValues1_, Implem1_, Cat1_>; \
				[[maybe_unused]] constexpr auto MinNbValues = VTT1::NbValues;

		#define PL_VECTOR_OVERLOAD_FC1_END \
			}

		#define PL_VECTOR_OVERLOAD_TAG_INVOKE_FC1(fc, tag, ...) \
			template <typename Type1_, typename NbValues1_, typename Implem1_, typename Cat1_> \
			[[nodiscard]] constexpr auto tag_invoke(tag_t<fc>, const vector<Type1_, NbValues1_, Implem1_, Cat1_> v1) noexcept \
				requires ((__VA_ARGS__) && requires(vector<Type1_, NbValues1_, Implem1_, Cat1_> v) { {v.tag()} -> CNotVoid; }) \
				{ return v1.tag(); } \
			\
			template < \
				typename Type1_, typename NbValues1_, typename Implem1_, typename Cat1_ \
				> \
			[[nodiscard]] constexpr auto tag_invoke(tag_t<fc>, const vector<Type1_, NbValues1_, Implem1_, Cat1_> v1) noexcept \
				requires ((__VA_ARGS__) && !(requires(vector<Type1_, NbValues1_, Implem1_, Cat1_> v) { {v.tag()} -> CNotVoid; })) \
			{ \
				using VTT1 = vector<Type1_, NbValues1_, Implem1_, Cat1_>; \
				[[maybe_unused]] constexpr auto MinNbValues = VTT1::NbValues;

		#define PL_VECTOR_OVERLOAD_TAG_INVOKE_FC1_END \
			}

		#define PL_VECTOR_OVERLOAD_FC2(fc, tag, ...) \
			template < \
				typename Type1_, typename NbValues1_, typename Implem1_, typename Cat1_, \
				typename Type2_, typename NbValues2_, typename Implem2_, typename Cat2_ \
				> \
			[[nodiscard]] constexpr auto fc(const vector<Type1_, NbValues1_, Implem1_, Cat1_> v1, const vector<Type2_, NbValues2_, Implem2_, Cat2_> v2) noexcept \
				requires ((__VA_ARGS__) && requires(vector<Type1_, NbValues1_, Implem1_, Cat1_> v1, vector<Type2_, NbValues2_, Implem2_, Cat2_> v2) { {v1.tag(v2)} -> CNotVoid; }) \
				{ return v1.tag(v2); } \
			\
			template < \
				typename Type1_, typename NbValues1_, typename Implem1_, typename Cat1_, \
				typename Type2_, typename NbValues2_, typename Implem2_, typename Cat2_ \
				> \
			[[nodiscard]] constexpr auto fc(const vector<Type1_, NbValues1_, Implem1_, Cat1_> v1, const vector<Type2_, NbValues2_, Implem2_, Cat2_> v2) noexcept \
				requires ((__VA_ARGS__) && !(requires(vector<Type1_, NbValues1_, Implem1_, Cat1_> v1, vector<Type2_, NbValues2_, Implem2_, Cat2_> v2) { {v1.tag(v2)} -> CNotVoid; })) \
			{ \
				using VTT1 = vector<Type1_, NbValues1_, Implem1_, Cat1_>; \
				using VTT2 = vector<Type2_, NbValues2_, Implem2_, Cat2_>; \
				[[maybe_unused]] constexpr auto MinNbValues = std::min(VTT1::NbValues, VTT2::NbValues);

		#define PL_VECTOR_OVERLOAD_FC2_END \
			}

		#define PL_VECTOR_OVERLOAD_TAG_INVOKE_FC2(fc, tag, ...) \
			template < \
				typename Type1_, typename NbValues1_, typename Implem1_, typename Cat1_, \
				typename Type2_, typename NbValues2_, typename Implem2_, typename Cat2_ \
				> \
			[[nodiscard]] constexpr auto tag_invoke(tag_t<fc>, const vector<Type1_, NbValues1_, Implem1_, Cat1_> v1, const vector<Type2_, NbValues2_, Implem2_, Cat2_> v2) noexcept \
				requires ((__VA_ARGS__) && requires(vector<Type1_, NbValues1_, Implem1_, Cat1_> v1, vector<Type2_, NbValues2_, Implem2_, Cat2_> v2) { {v1.tag(v2)} -> CNotVoid; }) \
				{ return v1.tag(v2); } \
			\
			template < \
				typename Type1_, typename NbValues1_, typename Implem1_, typename Cat1_, \
				typename Type2_, typename NbValues2_, typename Implem2_, typename Cat2_ \
				> \
			[[nodiscard]] constexpr auto tag_invoke(tag_t<fc>, const vector<Type1_, NbValues1_, Implem1_, Cat1_> v1, const vector<Type2_, NbValues2_, Implem2_, Cat2_> v2) noexcept \
				requires ((__VA_ARGS__) && !(requires(vector<Type1_, NbValues1_, Implem1_, Cat1_> v1, vector<Type2_, NbValues2_, Implem2_, Cat2_> v2) { {v1.tag(v2)} -> CNotVoid; })) \
			{ \
				using VTT1 = vector<Type1_, NbValues1_, Implem1_, Cat1_>; \
				using VTT2 = vector<Type2_, NbValues2_, Implem2_, Cat2_>; \
				[[maybe_unused]] constexpr auto MinNbValues = std::min(VTT1::NbValues, VTT2::NbValues);

		#define PL_VECTOR_OVERLOAD_TAG_INVOKE_FC2_END \
			}

		#define PL_VECTOR_OVERLOAD_FC2_MASKED(fc, tag, ...) \
			template < \
				typename Type1_, typename NbValues1_, typename Implem1_, typename Cat1_, \
				szt Mask_, typename Type2_, typename NbValues2_, typename Implem2_, typename Cat2_ \
				> \
			[[nodiscard]] constexpr auto fc(const vector<Type1_, NbValues1_, Implem1_, Cat1_> v1, const masked<Mask_, vector<Type2_, NbValues2_, Implem2_, Cat2_>> v2) noexcept \
				requires ((__VA_ARGS__) && requires(vector<Type1_, NbValues1_, Implem1_, Cat1_> v1, masked<Mask_, vector<Type2_, NbValues2_, Implem2_, Cat2_>> v2) { {v1.tag(v2)} -> CNotVoid; }) \
				{ return v1.tag(v2); } \
			\
			template < \
				typename Type1_, typename NbValues1_, typename Implem1_, typename Cat1_, \
				szt Mask_, typename Type2_, typename NbValues2_, typename Implem2_, typename Cat2_ \
				> \
			[[nodiscard]] constexpr auto fc(const vector<Type1_, NbValues1_, Implem1_, Cat1_> v1, const masked<Mask_, vector<Type2_, NbValues2_, Implem2_, Cat2_>> v2) noexcept \
				requires ((__VA_ARGS__) && !(requires(vector<Type1_, NbValues1_, Implem1_, Cat1_> v1, masked<Mask_, vector<Type2_, NbValues2_, Implem2_, Cat2_>> v2) { {v1.tag(v2)} -> CNotVoid; })) \
			{ \
				using VTT1 = vector<Type1_, NbValues1_, Implem1_, Cat1_>; \
				using VTT2 = vector<Type2_, NbValues2_, Implem2_, Cat2_>; \
				[[maybe_unused]] constexpr auto MinNbValues = std::min(VTT1::NbValues, VTT2::NbValues);

		#define PL_VECTOR_OVERLOAD_FC2_MASKED_END \
			}

		#define PL_VECTOR_OVERLOAD_FC2_SAME(fc, tag, ...) \
			template <typename Type1_, typename NbValues1_, typename Implem1_, typename Cat1_> \
			[[nodiscard]] constexpr auto fc(const vector<Type1_, NbValues1_, Implem1_, Cat1_> v1, const vector<Type1_, NbValues1_, Implem1_, Cat1_> v2) noexcept \
				requires ((__VA_ARGS__) && requires(vector<Type1_, NbValues1_, Implem1_, Cat1_> v1, vector<Type1_, NbValues1_, Implem1_, Cat1_> v2) { {v1.tag(v2)} -> CNotVoid; }) \
				{ return v1.tag(v2); } \
			\
			template < \
				typename Type1_, typename NbValues1_, typename Implem1_, typename Cat1_ \
				> \
			[[nodiscard]] constexpr auto fc(const vector<Type1_, NbValues1_, Implem1_, Cat1_> v1, const vector<Type1_, NbValues1_, Implem1_, Cat1_> v2) noexcept \
				requires ((__VA_ARGS__) && !(requires(vector<Type1_, NbValues1_, Implem1_, Cat1_> v1, vector<Type1_, NbValues1_, Implem1_, Cat1_> v2) { {v1.tag(v2)} -> CNotVoid; })) \
			{ \
				using VTT1 = vector<Type1_, NbValues1_, Implem1_, Cat1_>; \
				[[maybe_unused]] constexpr auto MinNbValues = VTT1::NbValues;

		#define PL_VECTOR_OVERLOAD_FC2_SAME_END \
			}

		// Modulo
		template <CVector VT_>
		[[nodiscard]] constexpr auto tag_invoke(tag_t<mod>, const VT_ v1, const auto m) noexcept
			requires CNumberPack<typename VT_::value_type>
			{ return v1 % m; }

		// and not
		PL_VECTOR_OVERLOAD_TAG_INVOKE_FC2(andnot, andnot, CBitFieldPack<Type1_> && CBitFieldPack<Type2_>)
			return do_op(v1, v2, [&](auto&& va1, auto&& va2){ return andnot(va1, va2); });
		PL_VECTOR_OVERLOAD_TAG_INVOKE_FC2_END

		// round
		PL_VECTOR_OVERLOAD_FC1(round, round, CNumberPackFloat<Type1_>)
			return do_op(v1, [&](auto&& va1){ return plround(va1); });
		PL_VECTOR_OVERLOAD_FC1_END

		// floor
		PL_VECTOR_OVERLOAD_FC1(floor, floor, CNumberPackFloat<Type1_>)
			return do_op(v1, [&](auto&& va1){ return plfloor(va1); });
		PL_VECTOR_OVERLOAD_FC1_END

		// ceil
		PL_VECTOR_OVERLOAD_FC1(ceil, ceil, CNumberPackFloat<Type1_>)
			return do_op(v1, [&](auto&& va1){ return plceil(va1); });
		PL_VECTOR_OVERLOAD_FC1_END

		// reciprocal
		PL_VECTOR_OVERLOAD_TAG_INVOKE_FC1(reciprocal, reciprocal, CNumberPackFloat<Type1_>)
			return do_op(v1, [&](auto&& va1){ return reciprocal(va1); });
		PL_VECTOR_OVERLOAD_TAG_INVOKE_FC1_END

		// reciprocalapprox
		PL_VECTOR_OVERLOAD_TAG_INVOKE_FC1(reciprocalapprox, reciprocalapprox, CNumberPackFloat<Type1_>)
			return do_op(v1, [&](auto&& va1){ return reciprocalapprox(va1); });
		PL_VECTOR_OVERLOAD_TAG_INVOKE_FC1_END

		// maximum
		PL_VECTOR_OVERLOAD_FC2_SAME(max, max, true)
			return do_op(v1, v2, [&](auto&& va1, auto&& va2){ return plmax(va1, va2); });
		PL_VECTOR_OVERLOAD_FC2_SAME_END

		// minimum
		PL_VECTOR_OVERLOAD_FC2_SAME(min, min, true)
			return do_op(v1, v2, [&](auto&& va1, auto&& va2){ return plmin(va1, va2); });
		PL_VECTOR_OVERLOAD_FC2_SAME_END

		// absolute
		PL_VECTOR_OVERLOAD_FC1(abs, abs, CNumberPack<Type1_>)
			return do_op(v1, [&](auto&& va1){ return plabs(va1); });
		PL_VECTOR_OVERLOAD_FC1_END

		// unpacklo
		PL_VECTOR_OVERLOAD_FC2(unpacklo, unpacklo, true)
			VTT1 n{};

			const auto &va1 = vector_access(v1);
			const auto &va2 = vector_access(v2);

			{
				scoped_vector_modify vmod{n};
				for(szt i=0; i<MinNbValues/2; ++i)
				{
					vmod[i*2] = va1[i];
					vmod[i*2+1] = va2[i];
				}
			}

			return n;
		PL_VECTOR_OVERLOAD_FC2_END

		// unpackhi
		PL_VECTOR_OVERLOAD_FC2(unpackhi, unpackhi, true)
			VTT1 n{};

			const auto &va1 = vector_access(v1);
			const auto &va2 = vector_access(v2);

			{
				scoped_vector_modify vmod{n};
				for(szt i=0; i<MinNbValues/2; ++i)
				{
					vmod[i*2] = va1[MinNbValues/2 + i];
					vmod[i*2+1] = va2[MinNbValues/2 + i];
				}
			}

			return n;
		PL_VECTOR_OVERLOAD_FC2_END

		// movehl
		PL_VECTOR_OVERLOAD_FC2(movehl, movehl, true)
			static_assert(NbValues1_::value == 4 && NbValues2_::value == 4, "movehl requires vectors with 4 elements.");
			return VTT1{v1[size_v<0>], v1[size_v<1>], v2[size_v<0>], v2[size_v<1>]};
		PL_VECTOR_OVERLOAD_FC2_END

		// movelh
		PL_VECTOR_OVERLOAD_FC2(movelh, movelh, true)
			static_assert(NbValues1_::value == 4 && NbValues2_::value == 4, "movelh requires vectors with 4 elements.");
			return VTT1{v1[size_v<0>], v1[size_v<1>], v2[size_v<0>], v2[size_v<1>]};
		PL_VECTOR_OVERLOAD_FC2_END

		// pow
		template <CVector VT_, typename PT_>
		[[nodiscard]] constexpr auto pow(const VT_ v1, const PT_ power) noexcept
			requires CNumberPack<typename VT_::value_type>
		{
			if constexpr(VT_::HardwareImpl)
				return v1.pow(power);
			else
				return do_op(v1, [&](auto&& va1){ return plpow(va1, power); });
		}

		// log
		PL_VECTOR_OVERLOAD_FC1(log, log, CNumberPackFloat<Type1_>)
			return do_op(v1, [&](auto&& va1){ return pllog(va1); });
		PL_VECTOR_OVERLOAD_FC1_END

		// log2
		PL_VECTOR_OVERLOAD_FC1(log2, log2, CNumberPackFloat<Type1_>)
			return do_op(v1, [&](auto&& va1){ return pllog2(va1); });
		PL_VECTOR_OVERLOAD_FC1_END

		// log2approx
		PL_VECTOR_OVERLOAD_TAG_INVOKE_FC1(log2approx, log2approx, CNumberPackFloat<Type1_>)
			return do_op(v1, [&](auto&& va1){ return log2approx(va1); });
		PL_VECTOR_OVERLOAD_TAG_INVOKE_FC1_END

		// log10
		PL_VECTOR_OVERLOAD_FC1(log10, log10, CNumberPackFloat<Type1_>)
			return do_op(v1, [&](auto&& va1){ return pllog10(va1); });
		PL_VECTOR_OVERLOAD_FC1_END

		// exp
		PL_VECTOR_OVERLOAD_FC1(exp, exp, CNumberPackFloat<Type1_>)
			return do_op(v1, [&](auto&& va1){ return plexp(va1); });
		PL_VECTOR_OVERLOAD_FC1_END

		// exp2
		PL_VECTOR_OVERLOAD_FC1(exp2, exp2, CNumberPackFloat<Type1_>)
			return do_op(v1, [&](auto&& va1){ return plexp2(va1); });
		PL_VECTOR_OVERLOAD_FC1_END

		// exp2approx
		PL_VECTOR_OVERLOAD_TAG_INVOKE_FC1(exp2approx, exp2approx, CNumberPackFloat<Type1_>)
			return do_op(v1, [&](auto&& va1){ return exp2approx(va1); });
		PL_VECTOR_OVERLOAD_TAG_INVOKE_FC1_END

		// sin
		PL_VECTOR_OVERLOAD_FC1(sin, sin, CNumberPackFloat<Type1_>)
			return do_op(v1, [&](auto&& va1){ return plsin(va1); });
		PL_VECTOR_OVERLOAD_FC1_END

		// cos
		PL_VECTOR_OVERLOAD_FC1(cos, cos, CNumberPackFloat<Type1_>)
			return do_op(v1, [&](auto&& va1){ return plcos(va1); });
		PL_VECTOR_OVERLOAD_FC1_END

		// square root
		PL_VECTOR_OVERLOAD_FC1(sqrt, sqrt, CNumberPackFloat<Type1_>)
			return do_op(v1, [&](auto&& va1){ return plsqrt(va1); });
		PL_VECTOR_OVERLOAD_FC1_END

		// square root approx
		PL_VECTOR_OVERLOAD_TAG_INVOKE_FC1(sqrtapprox, sqrtapprox, CNumberPackFloat<Type1_>)
			return do_op(v1, [&](auto&& va1){ return sqrtapprox(va1); });
		PL_VECTOR_OVERLOAD_TAG_INVOKE_FC1_END

		// reciprocal square root
		PL_VECTOR_OVERLOAD_TAG_INVOKE_FC1(recsqrt, recsqrt, CNumberPackFloat<Type1_>)
			return do_op(v1, [&](auto&& va1){ return recsqrt(va1); });
		PL_VECTOR_OVERLOAD_TAG_INVOKE_FC1_END

		// reciprocal square root approx
		PL_VECTOR_OVERLOAD_TAG_INVOKE_FC1(recsqrtapprox, recsqrtapprox, CNumberPackFloat<Type1_>)
			return do_op(v1, [&](auto&& va1){ return recsqrtapprox(va1); });
		PL_VECTOR_OVERLOAD_TAG_INVOKE_FC1_END

		// dot
		PL_VECTOR_OVERLOAD_FC2(dot, dot, CNumberPack<Type1_> && CNumberPack<Type2_>)
			const auto &va1 = vector_access(v1);
			const auto &va2 = vector_access(v2);

			auto res = va1[0] * va2[0];

			for(szt i=1; i<MinNbValues; ++i)
				res += va1[i] * va2[i];

			return res;
		PL_VECTOR_OVERLOAD_FC2_END

		PL_VECTOR_OVERLOAD_FC2_MASKED(dot, dot, CNumberPack<Type1_> && CNumberPack<Type2_>)
			const auto &va1 = vector_access(v1);
			const auto &va2 = vector_access(v2);

			auto res = va1[0] * va2[0];

			for(szt i=1; i<MinNbValues; ++i)
				res += Mask_ & (szt{1} << i) ? va1[i] * va2[i] : decltype(res){};

			return res;
		PL_VECTOR_OVERLOAD_FC2_END

		PL_VECTOR_OVERLOAD_FC1(dot, dot, CNumberPack<Type1_>)
			return dot(v1, v1);
		PL_VECTOR_OVERLOAD_FC1_END

		// shuffle
		template <int m0, int m1, int m2, int m3, CVector VT1_, CVector VT2_>
		[[nodiscard]] constexpr auto shuffle(const VT1_ v1, const VT2_ v2) noexcept
			requires requires(VT1_ v1, VT2_ v2) { {v1.template shuffle<m0, m1, m2, m3>(v2)} -> std::convertible_to<VT1_>; }
		{
			return v1.template shuffle<m0, m1, m2, m3>(v2);
		}

		template <int m0, int m1, int m2, int m3, CVector VT1_, CVector VT2_>
		[[nodiscard]] constexpr auto shuffle(const VT1_ v1, const VT2_ v2) noexcept
		{
			return VT1_{v1[size_v<m0>], v1[size_v<m1>], v2[size_v<m2>], v2[size_v<m3>]};
		}

		template <int m0, int m1, int m2, int m3, CVector VT1_>
		[[nodiscard]] constexpr auto shuffle(const VT1_ v1) noexcept
			{ return shuffle<m0, m1, m2, m3>(v1, v1); }


		// blend
		template <u32 mask, CVector VT1_, CVector VT2_>
		[[nodiscard]] constexpr auto blend(const VT1_ v1, const VT2_ v2) noexcept
			requires requires(VT1_ v1, VT2_ v2) { {v1.template blend<mask>(v2)} -> std::convertible_to<VT1_>; }
			{ return v1.template blend<mask>(v2); }

		template <u32 mask, CVector VT1_, CVector VT2_>
		[[nodiscard]] constexpr auto blend(const VT1_ v1, const VT2_ v2) noexcept
		{
			constexpr auto MinNbValues = std::min(VT1_::NbValues, VT2_::NbValues);

			using ResVT = decltype(v1 * v2);
			using ResVTType = typename ResVT::value_type;
			ResVT n{};

			const auto &va1 = vector_access(v1);
			const auto &va2 = vector_access(v2);

			vector_init_loop(n, [&](auto&& i){ return mask & (1<<i) ? (ResVTType)va2[i] : (ResVTType)va1[i]; }, size_v<MinNbValues>);

			return n;
		}

		// cross product
		PL_VECTOR_OVERLOAD_FC2(cross, cross, CNumberPack<Type1_> && CNumberPack<Type2_>)
			static_assert(NbValues1_::value >= 3 && NbValues2_::value >= 3, "Cross product requires vectors with at least 3 elements.");

			const auto &va1 = vector_access(v1);
			const auto &va2 = vector_access(v2);

			using ResVT = decltype(v1 * v2);

			ResVT res{
				va1[1] * va2[2] - va1[2] * va2[1],
				va1[2] * va2[0] - va1[0] * va2[2],
				va1[0] * va2[1] - va1[1] * va2[0]
			};

			if constexpr(NbValues1_::value == 4)
				set<3>(res, 0);

			return res;
		PL_VECTOR_OVERLOAD_FC2_END

		// hadd
		PL_VECTOR_OVERLOAD_FC1(hadd, hadd, CNumberPack<Type1_>)
			const auto &va1 = vector_access(v1);
			Type1_ res{va1[0]};

			for(szt i=1; i<MinNbValues; ++i)
				res += va1[i];

			VTT1 r{};
			return r.set(res, size_v<0>);
		PL_VECTOR_OVERLOAD_FC1_END

		// hmul
		PL_VECTOR_OVERLOAD_FC1(hmul, hmul, CNumberPack<Type1_>)
			const auto &va1 = vector_access(v1);
			Type1_ res{va1[0]};

			for(szt i=1; i<MinNbValues; ++i)
				res *= va1[i];

			VTT1 r{};
			return r.set(res, size_v<0>);
		PL_VECTOR_OVERLOAD_FC1_END

		// hsub
		PL_VECTOR_OVERLOAD_FC1(hsub, hsub, CNumberPack<Type1_>)
			const auto &va1 = vector_access(v1);
			Type1_ res{va1[0]};

			for(szt i=1; i<MinNbValues; ++i)
				res -= va1[i];

			VTT1 r{};
			return r.set(res, size_v<0>);
		PL_VECTOR_OVERLOAD_FC1_END

		// rotate
		PL_VECTOR_OVERLOAD_FC1(rotate, rotate, true)
			VTT1 n{};

			const auto &va1 = vector_access(v1);

			constexpr auto last = MinNbValues - 1;
			vector_init_loop(n, [&](auto&& i){ return va1[i.value + 1]; }, size_v<last>);

			n.template set<last>(va1[0]);

			return n;
		PL_VECTOR_OVERLOAD_FC1_END

		// Custom vector operators
		inline namespace vector_op
		{
			inline constexpr auto odot = make_custom_operator2([](auto&& v1, auto&& v2) noexcept { return dot(PL_FWD(v1), PL_FWD(v2)); });
			inline constexpr auto osdot = make_custom_operator1([](auto&& v) noexcept { return dot(PL_FWD(v), PL_FWD(v)); });
			inline constexpr auto ocross = make_custom_operator2([](auto&& v1, auto&& v2) noexcept { return cross(PL_FWD(v1), PL_FWD(v2)); });
			inline constexpr auto ohadd = make_custom_operator1([](auto&& v) noexcept { return hadd(PL_FWD(v)); });
			inline constexpr auto ohsub = make_custom_operator1([](auto&& v) noexcept { return hsub(PL_FWD(v)); });
			inline constexpr auto ohmul = make_custom_operator1([](auto&& v) noexcept { return hmul(PL_FWD(v)); });
			inline constexpr auto omovehl = make_custom_operator2([](auto&& v1, auto&& v2) noexcept { return movehl(PL_FWD(v1), PL_FWD(v2)); });
			inline constexpr auto omovelh = make_custom_operator2([](auto&& v1, auto&& v2) noexcept { return movelh(PL_FWD(v1), PL_FWD(v2)); });
			inline constexpr auto orotate = make_custom_operator1([](auto&& v) noexcept { return rotate(PL_FWD(v)); });
			inline constexpr auto ounpacklo = make_custom_operator2([](auto&& v1, auto&& v2) noexcept { return unpacklo(PL_FWD(v1), PL_FWD(v2)); });
			inline constexpr auto ounpackhi = make_custom_operator2([](auto&& v1, auto&& v2) noexcept { return unpackhi(PL_FWD(v1), PL_FWD(v2)); });

			template <int m0, int m1, int m2, int m3>
			inline constexpr auto oshuffle = make_custom_operator2([](auto&& v1, auto&& v2) noexcept
				{ return shuffle<m0, m1, m2, m3>(PL_FWD(v1), PL_FWD(v2)); });

			template <int m0, int m1, int m2, int m3>
			inline constexpr auto osshuffle = make_custom_operator1([](auto&& v) noexcept { return shuffle<m0, m1, m2, m3>(PL_FWD(v)); });

			template <u32 mask>
			inline constexpr auto oblend = make_custom_operator2([](auto&& v1, auto&& v2) noexcept
				{ return blend<mask>(PL_FWD(v1), PL_FWD(v2)); });

		} // namespace vector_op

		// length
		PL_VECTOR_OVERLOAD_FC1(length, length, CNumberPackFloat<Type1_>)
			return v1 /osdot /osqrt;
		PL_VECTOR_OVERLOAD_FC1_END

		inline namespace vector_op
			{ inline constexpr auto olength = make_custom_operator1([](auto&& v) noexcept { return length(PL_FWD(v)); }); }

		// length approx
		PL_VECTOR_OVERLOAD_FC1(lengthapprox, length_approx, CNumberPackFloat<Type1_>)
			return v1 /osdot /osqrtapprox;
		PL_VECTOR_OVERLOAD_FC1_END

		inline namespace vector_op
			{ inline constexpr auto olengthapprox = make_custom_operator1([](auto&& v) noexcept { return lengthapprox(PL_FWD(v)); }); }

		// normalize
		PL_VECTOR_OVERLOAD_FC1(normalize, normalize, CNumberPackFloat<Type1_>)
			return v1 * (v1 /osdot /orecsqrt);
		PL_VECTOR_OVERLOAD_FC1_END

		template <CVector VT1_, typename LT_>
		[[nodiscard]] constexpr auto normalize(const VT1_ v1, const LT_ lght) noexcept
			requires requires(VT1_ v1, LT_ lght) { {v1.normalize(lght)} -> std::convertible_to<VT1_>; }
			{ return v1.normalize(lght); }

		template <CVector VT1_, typename LT_>
		[[nodiscard]] constexpr auto normalize(const VT1_ v1, const LT_ lght) noexcept
			{ return v1 * (lght /oreciprocal); }

		inline namespace vector_op
		{
			inline constexpr auto onormalize = make_custom_operator1([](auto&& v) noexcept { return normalize(PL_FWD(v)); });
			inline constexpr auto olnormalize = make_custom_operator2([](auto&& v1, auto&& v2) noexcept { return normalize(PL_FWD(v1), PL_FWD(v2)); });
		}

		// normalize approx
		PL_VECTOR_OVERLOAD_FC1(normalizeapprox, normalize_approx, CNumberPackFloat<Type1_>)
			return v1 * (v1 /osdot /orecsqrtapprox);
		PL_VECTOR_OVERLOAD_FC1_END

		inline namespace vector_op
			{ inline constexpr auto onormalizeapprox = make_custom_operator1([](auto&& v) noexcept { return normalizeapprox(PL_FWD(v)); }); }

		// find_orthogonal
		PL_VECTOR_OVERLOAD_FC1(find_orthogonal, find_orthogonal, CNumberPack<Type1_>)
			static_assert(NbValues1_::value >= 3, "find_orthogonal requires a vector with at least 3 elements.");

			szt maxIndex = v1.abs_max3_index(), nextIndex = maxIndex == 2 ? 0 : maxIndex + 1;

			const auto &va1 = vector_access(v1);

			VTT1 ov{v1};
			ov.set(va1[maxIndex], nextIndex);
			ov.set(-va1[nextIndex], maxIndex);

			return v1 /ocross/ ov;
		PL_VECTOR_OVERLOAD_FC1_END

		inline namespace vector_op
			{ inline constexpr auto ofind_orthogonal = make_custom_operator1([](auto&& v) noexcept { return find_orthogonal(PL_FWD(v)); }); }

		// lerp
		template <CFloatVector VT_>
		[[nodiscard]] constexpr auto lerp(const VT_ a, const VT_ b, const VT_ t) noexcept
		{
			const auto x = a + (b - a) * t;

			const auto y = select(
				select(x, b, b < x),
				select(x, b, b > x),
				(t > 1) == (b > a)
			);

			return select(
				t * b + (1 - t) * a,
				select(b, y, t == 1),
				((a <= 0) & (b >= 0)) | ((a >= 0) & (b <= 0))
			);
		}

		// midpoint
		template <CVector VT_>
		[[nodiscard]] constexpr auto midpoint(const VT_ &a,  const VT_ &b) noexcept
			requires CNumberPack<typename VT_::value_type>
		{
			if constexpr(CFloatVector<VT_>)
			{
				const auto lo = std::numeric_limits<VT_>::min() * 2;
				const auto hi = std::numeric_limits<VT_>::max() / 2;
				const auto absa = a /oabs;
				const auto absb = b /oabs;

				const auto ad2 = a / 2;
				const auto bd2 = b / 2;
				const auto r1 = (a + b) / 2;
				const auto r2 = a + bd2;
				const auto r3 = ad2 + b;
				const auto r4 = ad2 + bd2;

				auto res = select(r1,
					select(r2,
						select(r3, r4, absb < lo),
						absa < lo),
					(absa <= hi) & (absb <= hi));

				if constexpr(CPointVector<VT_>)
					res = res.as_point();

				return res;
			}
			else
				return select( a + (b - a) / 2, a - (a - b) / 2, a <= b );
		}


		// Checks
		template <CVector VT_>
		[[nodiscard]] constexpr auto tag_invoke(tag_t<mask>, const VT_ &v) noexcept
			{ return v.mask(); }

		[[nodiscard]] constexpr auto isinf(CFloatVector auto const v) noexcept
			{ return v.is_infinite(); }

		[[nodiscard]] constexpr auto isnan(CFloatVector auto const v) noexcept
			{ return v.is_nan(); }

		[[nodiscard]] constexpr auto isnormal(CFloatVector auto const v) noexcept
			{ return v.is_normal(); }

		[[nodiscard]] constexpr bool tag_invoke(tag_t<is_valid>, CFloatColorVector auto const &c, const tag::nothing_t) noexcept
			{ return mask_none((c/oisnan) | (c/oisinf) | (c < 0)); }

		// sincos overloads
		[[nodiscard]] constexpr auto tag_invoke(tag_t<sincos>, CVector auto const v) noexcept
			{ return v.sincos(); }


		// conditional select overloads
		template <CVector VT_, CVector VCond_>
		[[nodiscard]] constexpr auto tag_invoke(tag_t<select>, const VT_ &v1, const VT_ &v2, const VCond_ &cond) noexcept
		{
			static_assert(VCond_::NbValues >= VT_::NbValues, "cond must have at least as many elements as v1 and v2.");

			// This doesn't work if NaNs are present
			//return (v1 * cond) + (v2 * !cond);

			const auto &va1 = vector_access(v1);
			const auto &va2 = vector_access(v2);
			const auto &vac = vector_access(cond);

			return construct_from_index_sequence<VT_>(size_v<VT_::NbValues>,
				[&](auto&& i){ return select(va1[i], va2[i], vac[i]); }
			);
		}

		// Full mask select
		template <CVector VT_, CVector VCond_>
		[[nodiscard]] constexpr auto tag_invoke(tag_t<select>, const VT_ &v1, const VCond_ &cond) noexcept
		{
			static_assert(VCond_::NbValues >= VT_::NbValues, "cond must have at least as many elements as v1.");

			const auto &va1 = vector_access(v1);
			const auto &vac = vector_access(cond);

			return construct_from_index_sequence<VT_>(size_v<VT_::NbValues>,
				[&](auto&& i){ return select(va1[i], vac[i]); }
			);
		}

		// Conditional swap overloads
		template <CVector VT_, CVector VCond_>
		constexpr void tag_invoke(tag_t<condswap>, VT_ &v1, VT_ &v2, const VCond_ cond) noexcept
		{
			static_assert(VCond_::NbValues >= VT_::NbValues, "cond must have at least as many elements as v1 and v2.");

			scoped_vector_modify v1mod{v1};
			scoped_vector_modify v2mod{v2};
			const auto &vac = vector_access(cond);

			for(szt i=0; i<VT_::NbValues; ++i)
				condswap(v1mod[i], v2mod[i], vac[i]);
		}
	} // namespace impl

	namespace vector_op = impl::vector_op;

	using namespace vector_op;


	template <CGeometricVector GVT_, CVector VT_>
	[[nodiscard]] constexpr auto mask_axis(type_c<GVT_>, const VT_ &v) noexcept
		{ return mask_nb(size_v<GVT_::category::NbAxis>, v); }

	template <CColorVector VT_>
	[[nodiscard]] constexpr auto mask_color(const VT_ &v) noexcept
		{ return mask_nb(size_v<3>, v); }
}
