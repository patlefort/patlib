/*
	PatLib

	Copyright (C) 2020 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <utility>
#include <type_traits>
#include <cstddef>
#include <string>
#include <ostream>
#include <istream>
#include <iomanip>

#include "base.hpp"

#include <patlib/concepts.hpp>
#include <patlib/io.hpp>

namespace patlib::simd::impl
{
	template <typename CharT_, typename Traits_, CVector VT_>
		requires (std::same_as<CharT_, char> && CArithmetic<typename VT_::value_type>)
	auto &operator<<(std::basic_ostream<CharT_, Traits_> &os, VT_ v)
	{
		std::array<CharT_, 1024> buff;
		const auto &v2 = vector_access(v);
		auto *cur = buff.data(), *end = cur + buff.size();

		const auto printOne = [&](auto n)
			{
				auto res = [&]{
						if constexpr(std::is_floating_point_v<typename VT_::value_type>)
						{
							if(end - 2 <= cur)
								return std::to_chars_result{cur, std::errc::value_too_large};

							auto *xpos = cur + 2;
							if(n < 0)
							{
								*cur++ = '-';
								*cur++ = '0';
							}
							else
							{
								*cur++ = '0';
								*cur++ = 'x';
							}

							auto res = std::to_chars(cur, end, n, std::chars_format::hex);

							if(n < 0)
								*xpos = 'x';

							return res;
						}
						else if constexpr(std::is_same_v<typename VT_::value_type, char> || std::is_same_v<typename VT_::value_type, unsigned char>)
							return std::to_chars(cur, end, (int)n);
						else
							return std::to_chars(cur, end, n);
					}();

				if(res.ec != std::errc{})
					os.setstate(std::ios_base::failbit);

				cur = res.ptr;

				return res;
			};

		index_sequence_loop(size_v<VT_::NbValues - 1>, [&](auto&& i){
			auto res = printOne(v2[i]);

			if(cur != end)
				*cur++ = ' ';
		});

		printOne(v2[size_v<VT_::NbValues - 1>]);

		return os << std::basic_string_view<CharT_, Traits_>{buff.data(), cur};
	}

	template <typename CharT_, typename Traits_, CVector VT_>
		requires (!std::same_as<CharT_, char> || !CArithmetic<typename VT_::value_type>)
	auto &operator<<(std::basic_ostream<CharT_, Traits_> &os, VT_ v)
	{
		const auto &v2 = vector_access(v);

		index_sequence_loop(size_v<VT_::NbValues - 1>, [&](auto&& i){
			os << v2[i] << ' ';
		});

		return os << v2[size_v<VT_::NbValues - 1>];
	}

	template <CVector VT_, typename FC_>
	bool read_vector(VT_ &v, FC_ &&readVal)
	{
		static constinit const auto NbValues = VT_::NbValues;

		bool success = true;

		vector_modify(v, [&](auto &&va){

			if(!PL_FWD(readVal)(va[0]))
			{
				success = false;
			}
			else
			{
				if constexpr(NbValues == 1)
					return;

				if(!PL_FWD(readVal)(va[1]))
					for(szt i=1; i<NbValues; ++i)
						va[i] = va[0];
				else
				{
					if constexpr(NbValues == 2)
						return;

					if(!PL_FWD(readVal)(va[2]))
					{
						va[NbValues - 1] = va[1];
						for(szt i=1; i<NbValues - 1; ++i)
							va[i] = va[0];
					}
					else
					{
						if constexpr(NbValues == 3)
							return;

						szt i = 3;
						for(; i<NbValues; ++i)
						{
							if(!PL_FWD(readVal)(va[i]))
								break;
						}

						for(; i<NbValues; ++i)
							va[i] = 0;
					}
				}
			}
		});

		return success;
	}

	std::istream& operator>>(std::istream &is, CVector auto &v)
	{
		const bool success = std::invoke([&]
			{
				const io::scoped_exceptions_state eg{is, std::ios_base::goodbit};
				const io::scoped_rdstate_saver sg{is, ~std::ios_base::eofbit};

				return read_vector(v,
					[&](auto &into)
					{
						is >> io::read_value_wrapper{into};
						return (bool)is;
					}
				);
			});

		if(!success)
			is.setstate(std::ios_base::failbit);

		return is;
	}

	std::ostream &operator<=(std::ostream &os, CVector auto const &v)
	{
		const auto &va = vector_access(v);

		for(const auto &v : va)
			os <= ::patlib::io::rawio::raw{v};

		return os;
	}

	std::istream &operator>=(std::istream &is, CVector auto &v)
	{
		vector_modify(v, [&](auto &va){
			for(szt i=0; i<v.size(); ++i)
				is >= ::patlib::io::rawio::raw{va[i]};
		});

		return is;
	}

	template <CVector VT_>
	[[nodiscard]] auto from_string_noloc(const std::string &value, type_c<VT_> = {})
	{
		using VT = typename VT_::value_type;

		VT_ res;

		if constexpr(std::is_floating_point_v<VT>)
		{
			const char *cur = value.data();
			const char * const last = cur + value.size();

			/*const bool success = read_vector(res,
				[&](auto &into)
				{
					//char *next;
					//into = io::read_float<VT>(cur, &next);
					auto [p, ec] = std::from_chars(cur, last, into, std::chars_format::hex);
					if(ec != std::errc())
						return false;

					cur = p;

					if(cur == last)
						return false;

					return true;
				}
			);*/

			const bool success = read_vector(res,
					[&](auto &into)
					{
						char *next;
						into = io::read_float<VT>(cur, &next);

						if(cur == next)
							return false;

						cur = next;
						return true;
					}
				);

			if(!success)
				throw Exception("Failed to parse vector from string.");
		}
		else
		{
			io::stream_from(value, io::throw_on_failure, [&](auto&& is, auto&&){
				is >> res;
			});
		}

		return res;
	}
}
