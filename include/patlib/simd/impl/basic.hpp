/*
	PatLib

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <cstddef>
#include <type_traits>
#include <limits>
#include <utility>
#include <bitset>
#include <initializer_list>
#include <array>
#include <iterator>

#include <patlib/crtp.hpp>
#include <patlib/type.hpp>
#include <patlib/math.hpp>

#include "base.hpp"

namespace patlib::simd
{
	namespace types
	{
		struct basic_property {};

		struct constexpr_t : basic_property {};

		template <typename... Props_>
		struct basic_vector_t : Props_... {};
	}

	template <typename T_>
	concept CBasicProperty = std::derived_from<T_, types::basic_property>;

	template <typename Type_, typename NbValues_, typename Category_ = void, CBasicProperty... Props_>
	using basic_vector = vector<Type_, NbValues_, types::basic_vector_t<Props_...>, Category_>;

	template <typename NbValues_>
	using bool_vector = basic_vector<bool, NbValues_>;

	template <typename Type_, typename NbValues_, typename VType_ = void, szt NbAxis_ = 3, CBasicProperty... Props_>
	using basic_geometric_vector = geometric_vector<Type_, NbValues_, types::basic_vector_t<Props_...>, VType_, NbAxis_>;

	template <typename T_, szt NbValues_> using values = basic_vector<T_, size_c<NbValues_>, void, types::constexpr_t>;

	template <typename T_> using point2d = basic_geometric_vector<T_, size_c<2>, categories::geometric_point_t, 2>;
	template <typename T_> using point3d = basic_geometric_vector<T_, size_c<3>, categories::geometric_point_t, 3>;
	template <typename T_> using dir2d = basic_geometric_vector<T_, size_c<2>, categories::geometric_dir_t, 2>;
	template <typename T_> using dir3d = basic_geometric_vector<T_, size_c<3>, categories::geometric_dir_t, 3>;

	template <typename T_> constinit const bool is_basic_vector_v = false;
	template <CBasicProperty... Props_> constinit const bool is_basic_vector_v<types::basic_vector_t<Props_...>> = true;
	template <typename Type_, typename NbValues_, typename Implem_, typename Category_> constinit const bool is_basic_vector_v<vector<Type_, NbValues_, Implem_, Category_>> = is_basic_vector_v<Implem_>;
	template <typename T_> concept CBasicVector = CVector<T_> && is_basic_vector_v<T_>;


	template <szt I, typename T_>
	[[nodiscard]] constexpr auto get(T_ &&v) noexcept
		requires CVector<remove_cvref_t<T_>>
		{ return PL_FWD(v)[size_v<I>]; }


	namespace impl
	{
		#define PL_VECTORLOOP for(szt i=0; i<NbValues; ++i)
		#define PL_VECTORLOOPNB(nb) for(szt i=0; i<(nb); ++i)


		template <typename NbValues_, typename Category_>
		class vector_bool_storage :
			public iteratable<bool, NbValues_::value, vector_bool_storage<NbValues_, Category_>>
		{
		public:
			static constinit const szt NbValues = NbValues_::value;
			static constinit const bool RefAccess = true;
			static constinit const bool HardwareImpl = false;
			static constinit const bool Shiftable = false;
			static constinit const bool ConstExpr = true;

			using value_type = bool;
			using internal_array_type = std::bitset<NbValues>;
			using aligned_array_type = aligned_type<internal_array_type>;
			using category = Category_;

			internal_array_type v;

		private:

			using MaskType = unsigned long long;

		public:
			constexpr vector_bool_storage() = default;
			constexpr vector_bool_storage(std::initializer_list<zero_init_t>) noexcept : v{} {}
			constexpr vector_bool_storage(const tag::zero_t) noexcept : vector_bool_storage(false) {}
			constexpr vector_bool_storage(const tag::allone_t) noexcept : vector_bool_storage(true) {}
			constexpr vector_bool_storage(const tag::one_t) noexcept : vector_bool_storage(true) {}

			constexpr vector_bool_storage(const aligned_array_type &av) noexcept : v{} { v = av(); }
			explicit constexpr vector_bool_storage(const internal_array_type &av) noexcept : v{av} {}

			constexpr vector_bool_storage(const value_type av) noexcept : v{av ? constants::all_ones<MaskType> : MaskType{}} {}
			template <typename... V_> vector_bool_storage(const value_type v1, V_&&... av) noexcept :
				v{
					call_from_variadic_loop(
						[&](auto&&... args){ return make_bitset(size_v<NbValues>, PL_FWD(args)...); },
						[&]<szt I_>(const size_c<I_> i, const auto v){ return std::make_pair(static_cast<MaskType>(I_), v); },
						v1, PL_FWD(av)...
					)
				}
			{}

			template <typename Alignment_ = tag::aligned_init_t>
			explicit constexpr vector_bool_storage(const tag::array_init_t, const value_type *ar, Alignment_ = Alignment_{}) noexcept { PL_VECTORLOOP v[i] = ar[i]; }


			[[nodiscard]] constexpr auto operator[] (const szt index) const noexcept { return v[index]; }

			template <szt index>
			[[nodiscard]] constexpr auto operator[] (size_c<index>) noexcept
				{ return v[index]; }

			template <szt index>
			[[nodiscard]] constexpr auto operator[] (size_c<index>) const noexcept
				{ return v[index]; }

			constexpr auto &set(const value_type value, const szt index) noexcept
				{ v[index] = value; return *this; }

			template <szt index>
			constexpr auto &set(const value_type value, size_c<index> = {}) noexcept
				{ v[index] = value; return *this; }

			template <typename T_ = value_type>
			constexpr auto &set_all_one() noexcept
				{ v.set(); return *this; }

			constexpr auto &set_zero() noexcept
				{ v.reset(); return *this; }

			constexpr auto &set_one() noexcept
				{ v.set(); return *this; }

			template <typename... V_>
			constexpr auto &set_all(V_&&... av) noexcept
				{ *this = {(value_type)av...}; return *this; }

			[[nodiscard]] constexpr auto get_array() const noexcept
				{ return aligned_array_type{v}; }

			[[nodiscard]] constexpr value_type scalar() const noexcept
				{ return v[0]; }
		};

		template <typename NbValues_, typename Category_, CBasicProperty... Props_>
		class vector<bool, NbValues_, types::basic_vector_t<Props_...>, Category_> :
			public vector_storage<bool, NbValues_, Category_, vector_bool_storage<NbValues_, Category_>>,
			public vector_base<vector<bool, NbValues_, types::basic_vector_t<Props_...>, Category_>>,
			public common_op<vector<bool, NbValues_, types::basic_vector_t<Props_...>, Category_>>
		{
			using common_op_type = common_op<vector<bool, NbValues_, types::basic_vector_t<Props_...>, Category_>>;
			using storage_type = vector_storage<bool, NbValues_, Category_, vector_bool_storage<NbValues_, Category_>>;

		public:
			static constinit const auto NbValues = storage_type::NbValues;
			static constinit const bool HardwareImpl = false;

			using impl_type = types::basic_vector_t<Props_...>;

			using storage_type::v;
			using storage_type::storage_type;

			using common_op_type::operator|=;
			using common_op_type::operator&=;
			using common_op_type::operator^=;

			[[nodiscard]] constexpr auto operator& (const vector o) const noexcept
				{ return vector{v & o.v}; }

			[[nodiscard]] constexpr auto operator| (const vector o) const noexcept
				{ return vector{v | o.v}; }

			[[nodiscard]] constexpr auto operator^ (const vector o) const noexcept
				{ return vector{v ^ o.v}; }

			[[nodiscard]] constexpr auto operator! () const noexcept
				{ return vector{~v}; }

			[[nodiscard]] constexpr auto operator~ () const noexcept
				{ return vector{~v}; }

			[[nodiscard]] constexpr auto operator== (const vector o) const noexcept
				{ return vector{~v ^ o.v}; }

			[[nodiscard]] constexpr auto operator!= (const vector o) const noexcept
				{ return vector{v ^ o.v}; }

			[[nodiscard]] constexpr auto blendv(vector o, vector mask) const noexcept
			{
				vector n;

				PL_VECTORLOOP
					n[i] = mask[i] ? o[i] : v[i];

				return n;
			}

			[[nodiscard]] constexpr bool is_all_zero() const noexcept
				{ return v.none(); }

			[[nodiscard]] constexpr auto mask() const noexcept
				{ return v; }

			[[nodiscard]] constexpr explicit operator bool() const noexcept
				{ return v.all(); }
		};

		template <typename Type_, szt Nb_, bool ConstExpr_>
		struct vector_basic_storage_internal_array
		{
			using internal_array_type = std::array<Type_, Nb_>;

			internal_array_type v;
		};

		template <typename Type_, szt Nb_>
		struct vector_basic_storage_internal_array<Type_, Nb_, true>
		{
			using internal_array_type = std::array<Type_, Nb_>;

			internal_array_type v{};
		};

		template <typename Type_, typename NbValues_, typename Category_, bool ConstExpr_, typename Derived_>
			requires (CNumberPack<Type_> || CBitFieldPack<Type_> || (CVector<Type_> && CBoolUnder<Type_>))
		class vector_basic_storage : private vector_basic_storage_internal_array<Type_, NbValues_::value, ConstExpr_>
		{
		private:
			using internal_storage = vector_basic_storage_internal_array<Type_, NbValues_::value, ConstExpr_>;

			PL_CRTP_BASE(Derived_)

		public:
			static constinit const szt NbValues = NbValues_::value;
			static constinit const bool RefAccess = true;
			static constinit const bool Shiftable = true;
			static constinit const bool ConstExpr = ConstExpr_;

			using value_type = Type_;
			using typename internal_storage::internal_array_type;
			using aligned_array_type = aligned_type<internal_array_type>;
			using category = Category_;

		private:

			using internal_storage::v;

		public:

			// Initialization
			constexpr vector_basic_storage() = default;
			constexpr vector_basic_storage(std::initializer_list<zero_init_t>) noexcept : internal_storage{{}} {}
			constexpr vector_basic_storage(const tag::zero_t) noexcept : internal_storage{{}} {}
			constexpr vector_basic_storage(const tag::allone_t) noexcept requires CBitFieldPack<value_type> : vector_basic_storage(~value_type{}) {}
			constexpr vector_basic_storage(const tag::one_t) noexcept : vector_basic_storage(constants::one<value_type>) {}
			constexpr vector_basic_storage(const tag::two_t) noexcept : vector_basic_storage(constants::two<value_type>) {}
			constexpr vector_basic_storage(const tag::half_t) noexcept requires CNumberPackFloat<value_type> : vector_basic_storage(constants::half<value_type>) {}
			constexpr vector_basic_storage(const tag::identity_t) noexcept { set_identity(); }
			constexpr vector_basic_storage(const tag::nan_t) noexcept requires CNumberPackFloat<value_type> : vector_basic_storage(std::numeric_limits<value_type>::quiet_NaN()) {}

			template <typename... V_>
			constexpr vector_basic_storage(const value_type v1, const value_type v2, const V_ &... av) noexcept : internal_storage{v1, v2, av...} {}
			constexpr vector_basic_storage(const aligned_array_type &av) noexcept { PL_VECTORLOOP v[i] = av()[i]; }
			explicit constexpr vector_basic_storage(const internal_array_type &av) noexcept : internal_storage{av} {}
			constexpr vector_basic_storage(const value_type av) noexcept : internal_storage{ construct_from_index_sequence<internal_array_type>(size_v<NbValues>, [&](auto){ return av; }) } {}
			constexpr vector_basic_storage(const value_type v1, const value_type v2) noexcept
				requires (NbValues > 1)
				{ PL_VECTORLOOPNB(NbValues-1) v[i] = v1; v[NbValues-1] = v2; }

			template <typename AT_, typename Alignment_ = tag::aligned_init_t>
			explicit constexpr vector_basic_storage(const tag::array_init_t, const AT_ * const ar, const Alignment_ = Alignment_{}) noexcept
				{ PL_VECTORLOOP v[i] = ar[i]; }


			// Modification and access
			[[nodiscard]] constexpr decltype(auto) operator[] (const szt index) noexcept { return v[index]; }
			[[nodiscard]] constexpr decltype(auto) operator[] (const szt index) const noexcept { return v[index]; }

			template <szt index>
			[[nodiscard]] constexpr decltype(auto) operator[] (size_c<index>) noexcept
				{ return v[index]; }
			template <szt index>
			[[nodiscard]] constexpr decltype(auto) operator[] (size_c<index>) const noexcept
				{ return v[index]; }

			constexpr auto &set(const value_type value, const szt index) noexcept
				{ v[index] = value; return derived(); }

			template <szt index>
			constexpr auto &set(const value_type value, size_c<index> = {}) noexcept
				{ v[index] = value; return derived(); }

			template <typename T_ = value_type>
			constexpr auto &set_all_one() noexcept
				requires CBitFieldPack<value_type>
				{ PL_VECTORLOOP v[i] = ~T_{}; return derived(); }

			constexpr auto &set_zero() noexcept
				{ *this = value_type{}; return derived(); }

			constexpr auto &set_one() noexcept
				{ *this = value_type{1}; return derived(); }

			constexpr auto &set_half() noexcept
				{ *this = value_type{0.5}; return derived(); }

			constexpr auto &set_two() noexcept
				{ *this = value_type{2}; return derived(); }

			constexpr auto &set_identity() noexcept
			{
				*this = value_type{};

				if constexpr(CGeometricVectorTagWithW<category, NbValues>)
					v[category::NbAxis] = get_gvec_last_value<typename category::type, value_type>();
				else if constexpr(categories::color_c<category>)
					v[NbValues - 1] = 1;

				return derived();
			}

			constexpr auto &set_nan() noexcept
				requires CNumberPackFloat<value_type>
				{ *this = std::numeric_limits<value_type>::quiet_NaN(); return derived(); }

			template <typename... V_>
			constexpr auto &set_all(V_&&... av) noexcept
				{ *this = {(value_type)av...}; return derived(); }

			[[nodiscard]] constexpr aligned_array_type get_array() const noexcept
				{ return aligned_array_type{v}; }

			[[nodiscard]] constexpr const value_type &scalar() const noexcept
				{ return v[0]; }

			[[nodiscard]] constexpr value_type &scalar() noexcept
				{ return v[0]; }

			constexpr void scalar(Type_ &dest) noexcept
				{ dest = v[0]; }

			// Iterator
			using reference = typename internal_array_type::reference;
			using const_reference = typename internal_array_type::const_reference;
			using pointer = typename internal_array_type::pointer;
			using const_pointer = typename internal_array_type::const_pointer;
			using size_type = typename internal_array_type::size_type;
			using difference_type = typename internal_array_type::difference_type;
			using iterator = typename internal_array_type::iterator;
			using const_iterator = typename internal_array_type::const_iterator;
			using reverse_iterator = typename internal_array_type::reverse_iterator;
			using const_reverse_iterator = typename internal_array_type::const_reverse_iterator;

			[[nodiscard]] constexpr auto begin() noexcept { return std::begin(v); }
			[[nodiscard]] constexpr auto begin() const noexcept { return std::begin(v); }
			[[nodiscard]] constexpr auto cbegin() const noexcept { return std::cbegin(v); }
			[[nodiscard]] constexpr auto end() noexcept { return std::end(v); }
			[[nodiscard]] constexpr auto end() const noexcept { return std::end(v); }
			[[nodiscard]] constexpr auto cend() const noexcept { return std::cend(v); }

			[[nodiscard]] constexpr auto rbegin() noexcept { return std::rbegin(v); }
			[[nodiscard]] constexpr auto rbegin() const noexcept { return std::rbegin(v); }
			[[nodiscard]] constexpr auto crbegin() const noexcept { return std::crbegin(v); }
			[[nodiscard]] constexpr auto rend() noexcept { return std::rend(v); }
			[[nodiscard]] constexpr auto rend() const noexcept { return std::rend(v); }
			[[nodiscard]] constexpr auto crend() const noexcept { return std::crend(v); }
		};

		// Basic and simple vector using a regular array.
		// One big difference with SSE vectors is to ability to access individual elements by reference with operator [].
		template <typename Type_, typename NbValues_, typename Category_, CBasicProperty... Props_>
			requires (CNumberPack<Type_> || CBitFieldPack<Type_> || (CVector<Type_> && CBoolUnder<Type_>))
		class vector<Type_, NbValues_, types::basic_vector_t<Props_...>, Category_> :
			public vector_storage<Type_, NbValues_, Category_, vector_basic_storage<Type_, NbValues_, Category_, std::derived_from<types::basic_vector_t<Props_...>, types::constexpr_t>, vector<Type_, NbValues_, types::basic_vector_t<Props_...>, Category_>>>,
			public vector_base<vector<Type_, NbValues_, types::basic_vector_t<Props_...>, Category_>>,
			public common_op<vector<Type_, NbValues_, types::basic_vector_t<Props_...>, Category_>>
		{
			using common_op_type = common_op<vector<Type_, NbValues_, types::basic_vector_t<Props_...>, Category_>>;
			using storage_type = vector_storage<Type_, NbValues_, Category_, vector_basic_storage<Type_, NbValues_, Category_, std::derived_from<types::basic_vector_t<Props_...>, types::constexpr_t>, vector<Type_, NbValues_, types::basic_vector_t<Props_...>, Category_>>>;

			[[nodiscard]] constexpr const auto &storage() const { return static_cast<const storage_type&>(*this); }
			[[nodiscard]] constexpr auto &storage() { return static_cast<storage_type&>(*this); }

		public:
			static constinit const auto NbValues = storage_type::NbValues;
			static constinit const bool HardwareImpl = false;

			using typename storage_type::value_type;
			using typename storage_type::category;
			using impl_type = types::basic_vector_t<Props_...>;

			using storage_type::storage_type;

			[[nodiscard]] constexpr auto as_point() const noexcept
				requires CGeometricVectorTag<category>
			{
				using T = rebind_to_point<std::decay_t<decltype(*this)>>;
				auto r = static_cast<T>(*this);

				if constexpr(CGeometricVectorWithW<T>)
					r[T::category::NbAxis] = 1;

				return r;
			}

			[[nodiscard]] constexpr auto as_dir() const noexcept
				requires CGeometricVectorTag<category>
			{
				using T = rebind_to_dir<std::decay_t<decltype(*this)>>;
				auto r = static_cast<T>(*this);

				if constexpr(CGeometricVectorWithW<T>)
					r[T::category::NbAxis] = {};

				return r;
			}

			[[nodiscard]] constexpr auto normalize_point() const noexcept
				requires (CPointVectorTag<category> || CNeutralGeometricVectorTag<category>)
			{
				if constexpr(CGeometricVectorTagWithW<category, NbValues>)
					return *this / storage()[category::NbAxis];
				else
					return *this;
			}


			using common_op_type::operator+=;
			using common_op_type::operator-=;
			using common_op_type::operator/=;
			using common_op_type::operator*=;
			using common_op_type::operator%=;
			using common_op_type::operator|=;
			using common_op_type::operator&=;
			using common_op_type::operator^=;
			using common_op_type::operator>>=;
			using common_op_type::operator<<=;

			[[nodiscard]] constexpr auto operator -() const noexcept
				{ return construct_from_index_sequence<rebind_vector<decltype(-storage()[0]), vector>>(size_v<NbValues>, [&](auto&& i){ return -storage()[i]; }); }

			[[nodiscard]] constexpr auto operator +() const noexcept
				{ return construct_from_index_sequence<rebind_vector<decltype(+storage()[0]), vector>>(size_v<NbValues>, [&](auto&& i){ return +storage()[i]; }); }

			constexpr auto &operator ++() noexcept
				{ PL_VECTORLOOP ++storage()[i]; return *this; }

			constexpr auto &operator --() noexcept
				{ PL_VECTORLOOP --storage()[i]; return *this; }

			constexpr auto operator ++(int) noexcept
				{ auto cp = *this; PL_VECTORLOOP ++storage()[i]; return cp; }

			constexpr auto operator --(int) noexcept
				{ auto cp = *this; PL_VECTORLOOP --storage()[i]; return cp; }

			[[nodiscard]] constexpr auto operator !() const noexcept
				{ return construct_from_index_sequence<rebind_vector<decltype(!storage()[0]), vector>>(size_v<NbValues>, [&](auto&& i){ return !storage()[i]; }); }

			//PL_VECTOROP_CMP_ALL

			[[nodiscard]] constexpr auto operator ~() const noexcept
				requires CBitFieldPack<Type_>
				{ return construct_from_index_sequence<vector>(size_v<NbValues>, [&](auto&& i){ return ~storage()[i]; }); }

			[[nodiscard]] constexpr auto sincos() const noexcept
				{ sincos_res<vector> r; PL_VECTORLOOP { std::tie(r.first[i], r.second[i]) = ::patlib::sincos(storage()[i]); } return r; }

			[[nodiscard]] constexpr auto hmin() const noexcept
			{
				Type_ res {storage()[0]};

				for(szt i=1; i<NbValues; ++i)
					res = plmin(res, storage()[i]);

				return res;
			}

			[[nodiscard]] constexpr auto hmax() const noexcept
			{
				Type_ res {storage()[0]};

				for(szt i=1; i<NbValues; ++i)
					res = plmax(res, storage()[i]);

				return res;
			}

			[[nodiscard]] constexpr auto max3() const noexcept
				requires is_greaterequal_v<NbValues, 3>
				{ return plmax(plmax(storage()[0], storage()[1]), storage()[2]); }

			[[nodiscard]] constexpr auto min3() const noexcept
				requires is_greaterequal_v<NbValues, 3>
				{ return plmin(plmin(storage()[0], storage()[1]), storage()[2]); }

			[[nodiscard]] constexpr auto abs_min3_index() const noexcept
				requires is_greaterequal_v<NbValues, 3>
			{
				auto minVal = plabs(storage()[0]), cVal = plabs(storage()[1]);
				szt minIndex = 0;

				if(cVal < minVal)
				{
					minIndex = 1;
					minVal = cVal;
				}

				if(plabs(storage()[2]) < minVal)
					minIndex = 2;

				return minIndex;
			}

			[[nodiscard]] constexpr auto min3_index() const noexcept
				requires is_greaterequal_v<NbValues, 3>
			{
				auto minVal = storage()[0], cVal = storage()[1];
				szt minIndex = 0;

				if(cVal < minVal)
				{
					minIndex = 1;
					minVal = cVal;
				}

				if(storage()[2] < minVal)
					minIndex = 2;

				return minIndex;
			}

			[[nodiscard]] constexpr auto abs_max3_index() const noexcept
				requires is_greaterequal_v<NbValues, 3>
			{
				auto maxVal = plabs(storage()[0]), cVal = plabs(storage()[1]);
				szt maxIndex = 0;

				if(cVal > maxVal)
				{
					maxIndex = 1;
					maxVal = cVal;
				}

				if(plabs(storage()[2]) > maxVal)
					maxIndex = 2;

				return maxIndex;
			}

			[[nodiscard]] constexpr auto max3_index() const noexcept
				requires is_greaterequal_v<NbValues, 3>
			{
				auto maxVal = storage()[0], cVal = storage()[1];
				szt maxIndex = 0;

				if(cVal > maxVal)
				{
					maxIndex = 1;
					maxVal = cVal;
				}

				if(storage()[2] > maxVal)
					maxIndex = 2;

				return maxIndex;
			}

			[[nodiscard]] constexpr auto cmp_scalar_ge(vector o) const noexcept
				{ rebind_vector<decltype(storage()[0] >= o[0]), vector> n; n.set(storage()[0] >= o[0], 0); return n; }

			[[nodiscard]] constexpr auto cmp_scalar_gt(vector o) const noexcept
				{ rebind_vector<decltype(storage()[0] > o[0]), vector> n; n.set(storage()[0] > o[0], 0); return n; }

			[[nodiscard]] constexpr auto cmp_scalar_eq(vector o) const noexcept
				{ rebind_vector<decltype(storage()[0] == o[0]), vector> n; n.set(storage()[0] == o[0], 0); return n; }

			[[nodiscard]] constexpr auto cmp_scalar_le(vector o) const noexcept
				{ rebind_vector<decltype(storage()[0] <= o[0]), vector> n; n.set(storage()[0] <= o[0], 0); return n; }

			[[nodiscard]] constexpr auto cmp_scalar_lt(vector o) const noexcept
				{ rebind_vector<decltype(storage()[0] < o[0]), vector> n; n.set(storage()[0] < o[0], 0); return n; }

			[[nodiscard]] constexpr auto min_scalar(vector o) const noexcept
				{ vector n{*this}; n[0] = plmin(storage()[0], o[0]); return n; }

			[[nodiscard]] constexpr auto max_scalar(vector o) const noexcept
				{ vector n{*this}; n[0] = plmax(storage()[0], o[0]); return n; }

			[[nodiscard]] constexpr auto next_after(vector o) const noexcept
				requires CNumberPackFloat<Type_>
				{ vector n; PL_VECTORLOOP n[i] = nextafterinf(storage()[i]); return n; }

			[[nodiscard]] constexpr auto minimal_safe_increment() const noexcept
				requires CNumberPackFloat<Type_> && is_greaterequal_v<NbValues, 3>
			{
				const auto vabs = plabs(*this).max3();

				return nextafterinf(vabs) - vabs;
			}

			[[nodiscard]] constexpr auto blendv(vector o, vector mask) const noexcept
			{
				vector n;

				PL_VECTORLOOP
					n[i] = mask_any(mask[i]) ? o[i] : storage()[i];

				return n;
			}

			[[nodiscard]] constexpr auto is_nan() const noexcept
				requires CNumberPackFloat<Type_>
				{ rebind_vector<decltype(plisnan(storage()[0])), vector> res; PL_VECTORLOOP res.set(plisnan(storage()[i]), i); return res; }

			[[nodiscard]] constexpr auto is_infinite() const noexcept
				requires CNumberPackFloat<Type_>
				{ rebind_vector<decltype(plisinf(storage()[0])), vector> res; PL_VECTORLOOP res.set(plisinf(storage()[i]), i); return res; }

			[[nodiscard]] constexpr auto is_normal() const noexcept
				requires CNumberPackFloat<Type_>
				{ rebind_vector<decltype(plisnormal(storage()[0])), vector> res; PL_VECTORLOOP res.set(plisnormal(storage()[i]), i); return res; }

			[[nodiscard]] constexpr bool is_all_zero() const noexcept
				{ bool res = true; PL_VECTORLOOP res &= (storage()[i] == 0); return res; }

			//[[nodiscard]] explicit operator int() const noexcept
				//{ int res = 0; PL_VECTORLOOP res |= (mask_any(storage()[i]) ? 1 : 0) << i; return res; }

			[[nodiscard]] constexpr auto mask() const noexcept
			{
				std::bitset<NbValues> m;
				PL_VECTORLOOP m.set(i, mask_any(storage()[i]));
				return m;
			}

			[[nodiscard]] explicit constexpr operator bool() const noexcept
				{ bool res = true; PL_VECTORLOOP res &= mask_all(storage()[i]); return res; }

			// For marked_optional default policy.
			static inline auto constexpr MarkedOptionalInitialValue = tag::nan;
			constexpr void reset() noexcept
					requires CNumberPackFloat<Type_>
				{ storage()[0] = std::numeric_limits<Type_>::quiet_NaN(); }
			[[nodiscard]] constexpr bool has_value() const noexcept
					requires CNumberPackFloat<Type_>
				{ return mask_none(storage()[0] /oisnan); }
		};

		template <CBasicVector VT_>
		[[nodiscard]] constexpr auto lerp(const VT_ &a, const VT_ &b, const auto &t) noexcept
			requires CNumberPackFloat<typename VT_::value_type>
		{
			VT_ nv;

			for(szt i=0; i<VT_::NbValues; ++i)
				nv[i] = pllerp(a[i], b[i], t);

			return nv;
		}

		template <CBasicVector VT_>
		[[nodiscard]] constexpr auto midpoint(const VT_ &a, const VT_ &b) noexcept
			requires CNumberPack<typename VT_::value_type>
		{
			VT_ nv;

			for(szt i=0; i<VT_::NbValues; ++i)
				nv[i] = plmidpoint(a[i], b[i]);

			return nv;
		}

	} // namespace impl

}
