/*
	PatLib

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <cstddef>
#include <type_traits>
#include <limits>
#include <utility>
#include <bitset>

#include <patlib/base.hpp>
#include <patlib/type.hpp>
#include <patlib/math.hpp>
#include <patlib/iterator.hpp>

namespace patlib::simd
{
	namespace categories
	{
		struct vector_tag {};

		PL_MAKE_DERIVED_TAG(color, vector_tag)

		template<typename T_ = void, szt NbAxis_ = 3> struct geometric_t : vector_tag
		{
			using type = T_;
			static constinit const auto NbAxis = NbAxis_;
		};
		template<typename T_ = void, szt NbAxis_ = 3> constinit const auto geometric = geometric_t<T_, NbAxis_>{};

		struct geometric_point_t {};
		struct geometric_dir_t {};
	} // namespace categories

	template <typename Category_> concept CVectorTag = std::is_void_v<Category_> || std::is_base_of_v<categories::vector_tag, Category_>;

	template <typename Category_> constinit const bool is_geometric_vector_tag_v = false;
	template <typename Category_, szt NbAxis_> constinit const bool is_geometric_vector_tag_v<categories::geometric_t<Category_, NbAxis_>> = true;
	template <typename Category_> concept CGeometricVectorTag = CVectorTag<Category_> && is_geometric_vector_tag_v<Category_>;

	template <typename Category_> constinit const bool is_point_vector_tag_v = false;
	template <szt NbAxis_> constinit const bool is_point_vector_tag_v<categories::geometric_t<categories::geometric_point_t, NbAxis_>> = true;
	template <typename Category_> concept CPointVectorTag = CGeometricVectorTag<Category_> && is_point_vector_tag_v<Category_>;

	template <typename Category_> constinit const bool is_dir_vector_tag_v = false;
	template <szt NbAxis_> constinit const bool is_dir_vector_tag_v<categories::geometric_t<categories::geometric_dir_t, NbAxis_>> = true;
	template <typename Category_> concept CDirVectorTag = CGeometricVectorTag<Category_> && is_dir_vector_tag_v<Category_>;

	template <typename Category_> constinit const bool is_neutral_geometric_vector_tag_v = false;
	template <szt NbAxis_> constinit const bool is_neutral_geometric_vector_tag_v<categories::geometric_t<void, NbAxis_>> = true;
	template <typename Category_> concept CNeutralGeometricVectorTag = CGeometricVectorTag<Category_> && is_neutral_geometric_vector_tag_v<Category_>;

	template <typename Category_, typename RT_>
	[[nodiscard]] consteval RT_ get_gvec_last_value() noexcept
	{
		if constexpr(std::is_same_v<Category_, categories::geometric_dir_t>)
			return 0;
		else
			return 1;
	}

	namespace impl
	{
		// Base vector type. Template specialization is used for the implementation. Implem_ is used as a tag to differentiate between different implementations.
		// Category_ can be used to differentiate between different type of vectors, such as point versus direction vector.
		template <typename Type_ = void, typename NbValues_ = void, typename Implem_ = void, typename Category_ = void>
		struct vector
		{
			using not_specialized = void;
		};

		template <typename T_>
		concept CValidVectorImpl = !requires{ typename T_::not_specialized; };

		template <typename Type_, typename NbValues_, typename Category_ = void, typename Specialized_ = void>
		struct best_vector {};

		struct specialized {};

		template <typename Type_, typename NbValues_, typename Category_ = void>
		using best_vector_t = typename best_vector<Type_, NbValues_, Category_, specialized>::type;

		template <typename T_> constinit const bool is_vector_v = false;
		template <typename... Args> constinit const bool is_vector_v<vector<Args...>> = true;

		template <typename T_> constinit const bool is_geometric_vector_v = false;
		template <typename Type_, typename NbValues_, typename Implem_, typename Category_, szt NbAxis_> constinit const bool is_geometric_vector_v<vector<Type_, NbValues_, Implem_, categories::geometric_t<Category_, NbAxis_>>> = true;

		template <typename T_> constinit const bool is_color_vector_v = false;
		template <typename Type_, typename NbValues_, typename Implem_> constinit const bool is_color_vector_v<vector<Type_, NbValues_, Implem_, categories::color_t>> = true;

		template <typename T_>
		struct vector_traits {};

		template <typename Type_, typename NbValues_, typename Implem_, typename Category_>
		struct vector_traits<vector<Type_, NbValues_, Implem_, Category_>>
		{
			static constinit const szt NbValues = NbValues_::value;
			using value_type = Type_;
			using impl_type = Implem_;
			using category = Category_;
		};

	} // namespace impl

	using impl::vector;
	using impl::best_vector_t;
	using impl::vector_traits;

	template <typename Type_, typename NbValues_, typename Impl_, typename VType_ = void, szt NbAxis_ = 3>
	using geometric_vector = vector<Type_, NbValues_, Impl_, categories::geometric_t<VType_, NbAxis_>>;

	template <typename Type_, typename NbValues_, typename VType_ = void, szt NbAxis_ = 3>
	using best_gvec_t = best_vector_t<Type_, NbValues_, categories::geometric_t<VType_, NbAxis_>>;

	template <typename Type_, typename NbValues_, typename Impl_>
	using color_vector = vector<Type_, NbValues_, Impl_, categories::color_t>;

	template <typename Type_, typename NbValues_>
	using best_color_t = best_vector_t<Type_, NbValues_, categories::color_t>;

	template <typename T_>
	concept CVector = impl::is_vector_v<std::decay_t<T_>> && impl::CValidVectorImpl<std::decay_t<T_>> &&
		requires
		{
			{ auto(std::decay_t<T_>::NbValues) } -> std::same_as<szt>;
			{ auto(std::decay_t<T_>::RefAccess) } -> std::same_as<bool>;
			{ auto(std::decay_t<T_>::HardwareImpl) } -> std::same_as<bool>;
			{ auto(std::decay_t<T_>::ConstExpr) } -> std::same_as<bool>;

			typename std::decay_t<T_>::value_type;
			typename std::decay_t<T_>::impl_type;
			typename std::decay_t<T_>::category;
		};

	template <typename T_>
	concept CIntegralVector = CVector<T_> && CNumberPackInteger<T_>;
	template <typename T_>
	concept CFloatVector = CVector<T_> && CNumberPackFloat<T_>;

	template <typename T_>
	concept CGeometricVector = CVector<T_> && impl::is_geometric_vector_v<std::decay_t<T_>>;
	template <typename T_>
	concept CNeutralGeometricVector = CGeometricVector<T_> && CNeutralGeometricVectorTag<typename T_::category>;
	template <typename T_>
	concept CPointVector = CGeometricVector<T_> && CPointVectorTag<typename T_::category>;
	template <typename T_>
	concept CNeutralOrPointGeometricVector = CGeometricVector<T_> && !CDirVectorTag<typename T_::category>;
	template <typename T_>
	concept CDirVector = CGeometricVector<T_> && CDirVectorTag<typename T_::category>;
	template <typename T_>
	concept CNeutralOrDirGeometricVector = CGeometricVector<T_> && !CPointVectorTag<typename T_::category>;

	template <typename T_, szt NbValues_> constinit const bool is_geometric_vector_tag_with_w = false;
	template <typename T_, szt NbAxis_, szt NbValues_>
	constinit const bool is_geometric_vector_tag_with_w<categories::geometric_t<T_, NbAxis_>, NbValues_> = NbValues_ > NbAxis_;
	template <typename T_, szt NbValues_>
	concept CGeometricVectorTagWithW = CGeometricVectorTag<T_> && is_geometric_vector_tag_with_w<T_, NbValues_>;

	template <typename T_> constinit const bool is_geometric_vector_with_w = false;
	template <typename Type_, typename NbValues_, typename VType_, typename Category_>
	constinit const bool is_geometric_vector_with_w<vector<Type_, NbValues_, VType_, Category_>> = CGeometricVectorTagWithW<Category_, NbValues_::value>;
	template <typename T_>
	concept CGeometricVectorWithW = CGeometricVector<T_> && is_geometric_vector_with_w<std::decay_t<T_>>;

	template <typename VT1_, typename VT2_>
	concept CSameNbAxis = CGeometricVector<VT1_> && CGeometricVector<VT2_> && std::decay_t<VT1_>::category::NbAxis == std::decay_t<VT2_>::category::NbAxis;

	template <typename T_, szt NB_>
	concept CNbAxis = CGeometricVector<T_> && (std::decay_t<T_>::category::NbAxis == NB_);

	template <typename T_>
	concept CColorVector = CVector<T_> && impl::is_color_vector_v<std::decay_t<T_>>;

	template <typename T_>
	concept CFloatColorVector = CColorVector<T_> && CFloatVector<T_>;
	template <typename T_>
	concept CIntegralColorVector = CColorVector<T_> && CIntegralVector<T_>;

	template <typename T_, CVector VT_>
	using rebind_vector = vector<T_, size_c<VT_::NbValues>, typename VT_::impl_type, typename VT_::category>;

	template <typename T_, CVector VT_>
	using rebind_best_vector = best_vector_t<T_, size_c<VT_::NbValues>, typename VT_::category>;

	template <typename T_>
	struct rebind_vector_under
	{
		template <typename OT_>
		using rebind = OT_;
	};

	template <typename Type_, typename NbValues_, typename Implem_, typename Category_>
	struct rebind_vector_under<vector<Type_, NbValues_, Implem_, Category_>>
	{
		template <typename T_>
		using rebind = vector<typename rebind_vector_under<Type_>::template rebind<T_>, NbValues_, Implem_, Category_>;
	};

	template <typename T_, CVector VT_>
	using rebind_vector_under_t = typename rebind_vector_under<VT_>::template rebind<T_>;

	template <typename T_>
	struct rebind_best_vector_under
	{
		template <typename OT_>
		using rebind = OT_;
	};

	template <typename Type_, typename NbValues_, typename Implem_, typename Category_>
	struct rebind_best_vector_under<vector<Type_, NbValues_, Implem_, Category_>>
	{
		template <typename T_>
		using rebind = best_vector_t<typename rebind_best_vector_under<Type_>::template rebind<T_>, NbValues_, Category_>;
	};

	template <typename T_, CVector VT_>
	using rebind_best_vector_under_t = typename rebind_best_vector_under<VT_>::template rebind<T_>;

	template <typename T_, CVector VT_>
	using rebind_vector_tag = vector<typename VT_::value_type, size_c<VT_::NbValues>, typename VT_::impl_type, T_>;

	template <typename T_, CVector VT_>
	using rebind_vector_impl = vector<typename VT_::value_type, size_c<VT_::NbValues>, T_, typename VT_::category>;

	template <typename T_, CVector VT_>
	using rebind_vector_size = vector<typename VT_::value_type, T_, typename VT_::impl_type, typename VT_::category>;

	template <CGeometricVector VT_>
	using rebind_to_point = vector<typename VT_::value_type, size_c<VT_::NbValues>, typename VT_::impl_type, std::conditional_t<std::is_void_v<typename VT_::category::type>, categories::geometric_t<void, VT_::category::NbAxis>, categories::geometric_t<categories::geometric_point_t, VT_::category::NbAxis>>>;

	template <CGeometricVector VT_>
	using rebind_to_dir = vector<typename VT_::value_type, size_c<VT_::NbValues>, typename VT_::impl_type, std::conditional_t<std::is_void_v<typename VT_::category::type>, categories::geometric_t<void, VT_::category::NbAxis>, categories::geometric_t<categories::geometric_dir_t, VT_::category::NbAxis>>>;

	template <CGeometricVector VT_>
	constinit const auto geometric_vector_axis_mask = fill_bits(VT_::category::NbAxis);

	template <szt FromNbValues_, typename FromTag_, szt ToNbValues_, typename ToTag_>
	constinit const bool is_valid_implicit_vector_conversion = std::is_same_v<FromTag_, ToTag_> && FromNbValues_ == ToNbValues_;

	#define PL_VECTOR_VARCTOR(classname, initfc) \
		template <typename... Args_> \
			requires (NbValues >= 3) \
		classname(const value_type x, const value_type y, const value_type z, Args_&&... args) noexcept \
		{ \
			static constexpr auto sizeofArgs = sizeof...(args); \
			const auto ap = [&](auto&&... args) { mVector = initfc(args..., z, y, x); }; \
			auto tpl = std::forward_as_tuple(PL_FWD(args)...); \
				\
			index_sequence_call(size_v<NbValues - 3>, [&](const auto... i){ \
				const auto select = [&]<szt II_>(const size_c<II_>) \
					{ \
						constexpr auto i2 = size_v<NbValues - 4 - II_>; \
						if constexpr(i2() >= sizeofArgs) \
							return value_type{}; \
						else \
							return std::get<i2()>(tpl); \
					}; \
						\
				ap( select(i)... ); \
			}); \
		}

	template <szt Mask_, typename T_>
	struct [[nodiscard]] masked
	{
		static constinit const auto Mask = Mask_;

		T_ v;

		constexpr masked(size_c<Mask_>, T_ v_) noexcept(CNoThrowMoveContructible<T_>) : v{std::move(v_)} {}
	};

	// Functions and classes to modify elements of a vector

	template <typename T1_>
	[[nodiscard]] constexpr const auto &vector_access(const T1_ &v) noexcept
		requires T1_::RefAccess
		{ return v; }

	template <typename T1_>
	[[nodiscard]] constexpr auto vector_access(const T1_ &v) noexcept
		{ return *v.get_array(); }

	template <typename T1_>
	constexpr decltype(auto) vector_modify(T1_&& v, auto &&fc)
	{
		if constexpr(remove_cvref_t<T1_>::RefAccess)
		{
			PL_FWD(fc)(PL_FWD(v));
		}
		else
		{
			auto ar = PL_FWD(v).get_array();
			PL_FWD(fc)(*ar);
			PL_FWD(v) = ar;
		}

		return PL_FWD_RETURN(v);
	}

	template <typename VectorType_>
	struct scoped_vector_modify {};
	template <typename VectorType_>
	scoped_vector_modify(VectorType_ &) -> scoped_vector_modify<VectorType_>;

	template <typename... T1_>
		requires (!vector<T1_...>::RefAccess)
	struct [[nodiscard]] scoped_vector_modify<vector<T1_...>>
	{
	private:
		using VT = vector<T1_...>;

		VT &vref;
		typename VT::aligned_array_type va;

	public:

		constexpr scoped_vector_modify(VT &v_) noexcept
			: vref{v_}, va{v_.get_array()} {}

		~scoped_vector_modify() { vref = va; }

		[[nodiscard]] constexpr auto &operator [](const szt i) noexcept { return va()[i]; }
		[[nodiscard]] constexpr const auto &operator [](const szt i) const noexcept { return va()[i]; }

		[[nodiscard]] constexpr auto &operator *() noexcept { return *va; }
		[[nodiscard]] constexpr const auto &operator *() const noexcept { return *va; }

		[[nodiscard]] constexpr auto &operator() () noexcept { return *va; }
		[[nodiscard]] constexpr const auto &operator() () const noexcept { return *va; }
	};

	template <typename... T1_>
		requires (vector<T1_...>::RefAccess)
	struct [[nodiscard]] scoped_vector_modify<vector<T1_...>>
	{
		using VT = vector<T1_...>;

		constexpr scoped_vector_modify(VT &v_) noexcept
			: v{v_} {}

		[[nodiscard]] constexpr auto &operator [](const szt i) noexcept { return v[i]; }
		[[nodiscard]] constexpr const auto &operator [](const szt i) const noexcept { return v[i]; }

		[[nodiscard]] constexpr auto &operator *() { return v; }
		[[nodiscard]] constexpr const auto &operator *() const { return v; }

		[[nodiscard]] constexpr auto &operator() () { return v; }
		[[nodiscard]] constexpr const auto &operator() () const { return v; }

	private:
		VT &v;
	};

	template <CVector T1_, CVector T2_, szt nb = std::min(T1_::NbValues, T2_::NbValues)>
	constexpr auto &vector_transform(const T1_ v, T2_ &into, auto &&fc, const size_c<nb> n = {}) noexcept
	{
		{
			scoped_vector_modify vmod{into};
			const auto &vread = vector_access(v);

			index_sequence_loop(n,
				[&](auto&& i){ vmod[i] = PL_FWD(fc)(i, vread[i]); }
			);
		}

		return into;
	}

	template <CVector T1_, szt nb = T1_::NbValues>
	[[nodiscard]] constexpr auto vector_transform(const T1_ v, auto &&fc, const size_c<nb> n = {}) noexcept
	{
		T1_ res;

		{
			scoped_vector_modify vmod{res};
			const auto &vread = vector_access(v);

			index_sequence_loop(n,
				[&](auto&& i){ vmod[i] = PL_FWD(fc)(i, vread[i]); }
			);
		}

		return res;
	}

	template <typename T1_>
	constexpr decltype(auto) vector_init(T1_&& v, auto &&fc) noexcept
	{
		if constexpr(remove_cvref_t<T1_>::RefAccess)
		{
			PL_FWD(fc)(PL_FWD(v));
		}
		else
		{
			typename remove_cvref_t<T1_>::aligned_array_type ar{};

			PL_FWD(fc)(*ar);

			PL_FWD(v) = ar;
		}

		return PL_FWD_RETURN(v);
	}

	template <typename T1_, szt NB_ = remove_cvref_t<T1_>::NbValues>
	constexpr decltype(auto) vector_init_loop(T1_&& v, auto &&fc, const size_c<NB_> n = {}) noexcept
	{
		if constexpr(remove_cvref_t<T1_>::RefAccess)
		{
			index_sequence_loop(n,
				[&](auto&& i){ PL_FWD(v)[i] = PL_FWD(fc)(i); }
			);
		}
		else
		{
			typename remove_cvref_t<T1_>::aligned_array_type res{};

			index_sequence_loop(n,
				[&](auto&& i){ res()[i] = PL_FWD(fc)(i); }
			);

			PL_FWD(v) = res;
		}

		return PL_FWD_RETURN(v);
	}


	// Implements some operators and functions used commonly in vectors
	template <typename VT_>
	class vector_base
	{
		PL_CRTP_BASE(VT_)

	public:

		static_assert(is_greater_v<vector_traits<VT_>::NbValues, 0>, "Vectors cannot have 0 values.");

		[[nodiscard]] constexpr auto size() const noexcept { return VT_::NbValues; }
		[[nodiscard]] constexpr auto max_size() const noexcept { return VT_::NbValues; }
		[[nodiscard]] constexpr decltype(auto) front() & noexcept { return derived().template operator[] (size_v<0>); }
		[[nodiscard]] constexpr decltype(auto) front() const& noexcept { return derived().template operator[] (size_v<0>); }
		[[nodiscard]] constexpr decltype(auto) front() && noexcept { return std::move(*this).derived().template operator[] (size_v<0>); }
		[[nodiscard]] constexpr decltype(auto) front() const&& noexcept { return std::move(*this).derived().template operator[] (size_v<0>); }
		[[nodiscard]] constexpr decltype(auto) back() & noexcept { return derived().template operator[] (size_v<size() - 1>); }
		[[nodiscard]] constexpr decltype(auto) back() const& noexcept { return derived().template operator[] (size_v<size() - 1>); }
		[[nodiscard]] constexpr decltype(auto) back() && noexcept { return std::move(*this).derived().template operator[] (size_v<size() - 1>); }
		[[nodiscard]] constexpr decltype(auto) back() const&& noexcept { return std::move(*this).derived().template operator[] (size_v<size() - 1>); }

		template <typename T_>
		constexpr void fill(const T_ v) noexcept { derived() = v; }

		// For marked_optional default policy.
		static inline auto constexpr MarkedOptionalInitialValue = tag::nan;
		constexpr void reset() noexcept
				requires CNumberPackFloat<typename VT_::value_type>
			{ derived().set_nan(); }
		[[nodiscard]] constexpr bool has_value() const noexcept
				requires CNumberPackFloat<typename VT_::value_type>
			{ return mask_none(derived() /oisnan); }
	};

	template <typename VT_>
	class common_op
	{
		PL_CRTP_BASE(VT_)

	public:

		constexpr auto &operator+=(const auto o) noexcept
			requires requires(VT_ v) { {v + o} -> std::convertible_to<VT_>; }
			{ return (derived() = derived() + o); }

		constexpr auto &operator-=(const auto o) noexcept
			requires requires(VT_ v) { {v - o} -> std::convertible_to<VT_>; }
			{ return (derived() = derived() - o); }

		constexpr auto &operator/=(const auto o) noexcept
			requires requires(VT_ v) { {v / o} -> std::convertible_to<VT_>; }
			{ return (derived() = derived() / o); }

		constexpr auto &operator*=(const auto o) noexcept
			requires requires(VT_ v) { {v * o} -> std::convertible_to<VT_>; }
			{ return (derived() = derived() * o); }

		constexpr auto &operator%=(const auto o) noexcept
			requires requires(VT_ v) { {v % o} -> std::convertible_to<VT_>; }
			{ return (derived() = derived() % o); }

		constexpr auto &operator&=(const auto o) noexcept
			requires requires(VT_ v) { {v & o} -> std::convertible_to<VT_>; }
			{ return (derived() = derived() & o); }

		constexpr auto &operator|=(const auto o) noexcept
			requires requires(VT_ v) { {v | o} -> std::convertible_to<VT_>; }
			{ return (derived() = derived() | o); }

		constexpr auto &operator^=(const auto o) noexcept
			requires requires(VT_ v) { {v ^ o} -> std::convertible_to<VT_>; }
			{ return (derived() = derived() ^ o); }

		constexpr auto &operator<<=(const auto o) noexcept
			requires requires(VT_ v) { {v << o} -> std::convertible_to<VT_>; }
			{ return (derived() = derived() << o); }

		constexpr auto &operator>>=(const auto o) noexcept
			requires requires(VT_ v) { {v >> o} -> std::convertible_to<VT_>; }
			{ return (derived() = derived() >> o); }

		[[nodiscard]] constexpr auto operator<=>(const CVector auto &o) const noexcept
			requires std::totally_ordered_with<typename vector_traits<VT_>::value_type, typename std::decay_t<decltype(o)>::value_type>
		{
			const auto &va1 = vector_access(derived());
			const auto &va2 = vector_access(o);

			return rg::lexicographical_compare(va1, va2);
		}
	};

	template <typename ValueType_, szt NbValues_, typename VT_>
	class iteratable
	{
		PL_CRTP_BASE(VT_)

	public:

		using value_type = ValueType_;
		using reference = value_type;
		using const_reference = value_type;
		using pointer = value_type *;
		using const_pointer = const value_type *;
		using size_type = szt;
		using difference_type = std::ptrdiff_t;

		using iterator = index_vector_iterator<VT_, difference_type>;
		using const_iterator = index_vector_iterator<const VT_, difference_type>;
		using reverse_iterator = std::reverse_iterator<iterator>;
		using const_reverse_iterator = std::reverse_iterator<const_iterator>;

		[[nodiscard]] constexpr auto begin() noexcept { return iterator{derived()}; }
		[[nodiscard]] constexpr auto begin() const noexcept { return const_iterator{derived()}; }
		[[nodiscard]] constexpr auto cbegin() const noexcept { return const_iterator{derived()}; }
		[[nodiscard]] constexpr auto end() noexcept { return iterator{derived(), NbValues_}; }
		[[nodiscard]] constexpr auto end() const noexcept { return const_iterator{derived(), NbValues_}; }
		[[nodiscard]] constexpr auto cend() const noexcept { return const_iterator{derived(), NbValues_}; }

		[[nodiscard]] constexpr auto rbegin() noexcept { return reverse_iterator{end()}; }
		[[nodiscard]] constexpr auto rbegin() const noexcept { return const_reverse_iterator{end()}; }
		[[nodiscard]] constexpr auto crbegin() const noexcept { return const_reverse_iterator{cend()}; }
		[[nodiscard]] constexpr auto rend() noexcept { return reverse_iterator{begin()}; }
		[[nodiscard]] constexpr auto rend() const noexcept { return const_reverse_iterator{begin()}; }
		[[nodiscard]] constexpr auto crend() const noexcept { return const_reverse_iterator{cbegin()}; }
	};

	template <typename VT1_, typename VT2_>
	void convert(const VT1_ &from, VT2_ &into) = delete;

	template <typename Type_, typename NbValues_, typename Category_, typename StorageType_>
	class vector_storage : public StorageType_
	{
	private:
		PL_CRTP_DERIVED(StorageType_)

	public:
		static constinit const szt NbValues = NbValues_::value;

		using value_type = Type_;
		using category = Category_;

		constexpr vector_storage() = default;

		using StorageType_::StorageType_;

		template <typename OT_, typename ONB_, typename OCat_, typename OST_>
			explicit(!is_valid_implicit_vector_conversion<ONB_::value, OCat_, NbValues, category> ||
				!std::convertible_to<OT_, value_type>)
		constexpr vector_storage(const vector_storage<OT_, ONB_, OCat_, OST_> &v) noexcept
		{
			static_assert(CExplicitConvertibleTo<OT_, value_type>, "vector types must be convertible.");
			convert(v, *this);
		}

		template <rg::input_range Range_>
			requires (!CVector<Range_> && std::convertible_to<rg::range_reference_t<Range_>, typename StorageType_::value_type>)
		explicit constexpr vector_storage(Range_&& r)
		{
			for(auto&& [v, i] : rgv::enumerate(PL_FWD(r) | rgv::take(StorageType_::NbValues)))
				base().set(PL_FWD(v), i);
		}
	};

	template <typename Type_, typename NbValues_, typename Impl_, typename StorageCategory_, typename Category_, typename Derived_>
	class other_vector_storage
	{
		PL_CRTP_BASE(Derived_)

	public:
		using storage_vector_type = vector<Type_, NbValues_, Impl_, StorageCategory_>;

		static constinit const szt NbValues = NbValues_::value;
		static constinit const bool RefAccess = storage_vector_type::RefAccess;
		static constinit const bool Shiftable = storage_vector_type::Shiftable;
		static constinit const bool ConstExpr = storage_vector_type::ConstExpr;

		using value_type = Type_;
		using aligned_array_type = typename storage_vector_type::aligned_array_type;
		using category = Category_;

		using iterator = typename storage_vector_type::iterator;
		using const_iterator = typename storage_vector_type::const_iterator;
		using reverse_iterator = typename storage_vector_type::reverse_iterator;
		using const_reverse_iterator = typename storage_vector_type::const_reverse_iterator;

		storage_vector_type v1;

		[[nodiscard]] constexpr auto begin() noexcept { return v1.begin(); }
		[[nodiscard]] constexpr auto begin() const noexcept { return v1.begin(); }
		[[nodiscard]] constexpr auto cbegin() const noexcept { return v1.cbegin(); }
		[[nodiscard]] constexpr auto end() noexcept { return v1.end(); }
		[[nodiscard]] constexpr auto end() const noexcept { return v1.end(); }
		[[nodiscard]] constexpr auto cend() const noexcept { return v1.cend(); }

		[[nodiscard]] constexpr auto rbegin() noexcept { return v1.rbegin(); }
		[[nodiscard]] constexpr auto rbegin() const noexcept { return v1.rbegin(); }
		[[nodiscard]] constexpr auto crbegin() const noexcept { return v1.crbegin(); }
		[[nodiscard]] constexpr auto rend() noexcept { return v1.rend(); }
		[[nodiscard]] constexpr auto rend() const noexcept { return v1.rend(); }
		[[nodiscard]] constexpr auto crend() const noexcept { return v1.crend(); }

		other_vector_storage() = default;
		other_vector_storage(std::initializer_list<zero_init_t>) noexcept : v1{} {}
		other_vector_storage(const storage_vector_type &pv1) noexcept : v1{pv1} {}

		other_vector_storage(const value_type v) noexcept : v1{v} {}

		template <typename Alignment_ = tag::aligned_init_t>
		explicit other_vector_storage(const tag::array_init_t t, const value_type *v, Alignment_ a = {}) noexcept : v1(t, v, a) {}
		other_vector_storage(const aligned_array_type &v) noexcept : v1{v} {}

		other_vector_storage(const tag::zero_t) noexcept : v1{tag::zero} {}
		other_vector_storage(const tag::identity_t) noexcept : v1{tag::identity} {}
		other_vector_storage(const tag::allone_t) noexcept requires CBitFieldPack<value_type> : v1{tag::allone} {}
		other_vector_storage(const tag::one_t) noexcept : v1{tag::one} {}
		other_vector_storage(const tag::two_t) noexcept : v1{tag::two} {}
		other_vector_storage(const tag::half_t) noexcept requires CNumberPackFloat<value_type> : v1{tag::half} {}
		other_vector_storage(const tag::nan_t) noexcept requires CNumberPackFloat<value_type> : v1{tag::nan} {}


		[[nodiscard]] decltype(auto) operator[] (const szt index) const noexcept
			{ return v1[index]; }

		template <szt index>
		[[nodiscard]] decltype(auto) operator[] (size_c<index> i) const noexcept
			{	return v1[i]; }

		auto &set_zero() noexcept
		{
			v1.set_zero();
			return derived();
		}

		auto &set_all_one() noexcept
			requires CBitFieldPack<value_type>
		{
			v1.set_all_one();
			return derived();
		}

		auto &set_one() noexcept
		{
			v1.set_one();
			return derived();
		}

		auto &set_half() noexcept
			requires CNumberPackFloat<value_type>
		{
			v1.set_half();
			return derived();
		}

		auto &set_two() noexcept
		{
			v1.set_two();
			return derived();
		}

		auto &set_nan() noexcept
			requires CNumberPackFloat<value_type>
		{
			v1.set_nan();
			return derived();
		}

		auto &set_identity() noexcept
		{
			v1.set_identity();
			return derived();
		}

		auto &set(const value_type v) noexcept
		{
			v1.set(v);
			return derived();
		}

		auto &set_all(const value_type x, const value_type y, const value_type z, const value_type w) noexcept
		{
			v1.set_all(x, y, z, w);
			return derived();
		}

		auto &set(const value_type *v) noexcept
		{
			v1.set(v);
			return derived();
		}

		auto &set_unaligned(const value_type *v) noexcept
		{
			v1.set_unaligned(v);
			return derived();
		}

		auto &set(const aligned_array_type &v) noexcept
		{
			v1.set(v);
			return derived();
		}

		template <szt index>
		auto &set(const value_type v, size_c<index> i = {}) noexcept
		{
			v1.set(v, i);
			return derived();
		}

		void get_all(value_type *v) const noexcept
		{
			v1.get_all(v);
		}

		void get_all_aligned(value_type *v) const noexcept
		{
			v1.get_all_aligned(v);
		}

		[[nodiscard]] aligned_array_type get_array() const noexcept
		{
			return v1.get_array();
		}

		auto &set(const value_type v, const int index) noexcept
		{
			v1.set(v, index);
			return derived();
		}

		void scalar(value_type &dest) noexcept
		{
			v1.scalar(dest);
		}

		[[nodiscard]] value_type scalar() const noexcept
		{
			return v1.scalar();
		}
	};


} // namespace patlib::simd

namespace std
{
	template <typename T_, typename NbValues_, typename... Ts_>
	struct tuple_size<::patlib::simd::vector<T_, NbValues_, Ts_...>> : public integral_constant<size_t, NbValues_::value> {};

	template <size_t I_, typename T_, typename... Ts_>
	struct tuple_element<I_, ::patlib::simd::vector<T_, Ts_...>> { using type = T_; };

	template <typename T_, typename... Ts_>
		requires numeric_limits<T_>::is_specialized
	struct numeric_limits<::patlib::simd::vector<T_, Ts_...>>
	{
		using VT = ::patlib::simd::vector<T_, Ts_...>;
		using underlimits = numeric_limits<T_>;

		static constexpr bool is_specialized = true;
		static constexpr auto is_signed = underlimits::is_signed;
		static constexpr auto is_integer = underlimits::is_integer;
		static constexpr auto is_exact = underlimits::is_exact;
		static constexpr auto has_infinity = underlimits::has_infinity;
		static constexpr auto has_quiet_NaN = underlimits::has_quiet_NaN;
		static constexpr auto has_signaling_NaN = underlimits::has_signaling_NaN;
		static constexpr auto has_denorm = underlimits::has_denorm;
		static constexpr auto has_denorm_loss = underlimits::has_denorm_loss;
		static constexpr auto round_style = underlimits::round_style;
		static constexpr auto is_iec559 = underlimits::is_iec559;
		static constexpr auto is_bounded = underlimits::is_bounded;
		static constexpr auto is_modulo = underlimits::is_modulo;
		static constexpr auto digits = underlimits::digits;
		static constexpr auto digits10 = underlimits::digits10;
		static constexpr auto max_digits10 = underlimits::max_digits10;
		static constexpr auto radix = underlimits::radix;
		static constexpr auto min_exponent = underlimits::min_exponent;
		static constexpr auto min_exponent10 = underlimits::min_exponent10;
		static constexpr auto max_exponent = underlimits::max_exponent;
		static constexpr auto max_exponent10 = underlimits::max_exponent10;
		static constexpr auto traps = underlimits::traps;
		static constexpr auto tinyness_before = underlimits::tinyness_before;

		static inline constexpr auto min() noexcept { return VT{underlimits::min()}; }
		static inline constexpr auto lowest() noexcept { return VT{underlimits::lowest()}; }
		static inline constexpr auto max() noexcept { return VT{underlimits::max()}; }
		static inline constexpr auto epsilon() noexcept requires ::patlib::simd::CFloatVector<VT> { return VT{underlimits::epsilon()}; }
		static inline constexpr auto round_error() noexcept requires ::patlib::simd::CFloatVector<VT> { return VT{underlimits::round_error()}; }
		static inline constexpr auto infinity() noexcept requires ::patlib::simd::CFloatVector<VT> { return VT{underlimits::infinity()}; }
		static inline constexpr auto quiet_NaN() noexcept requires ::patlib::simd::CFloatVector<VT> { return VT{underlimits::quiet_NaN()}; }
		static inline constexpr auto signaling_NaN() noexcept requires ::patlib::simd::CFloatVector<VT> { return VT{underlimits::signaling_NaN()}; }
		static inline constexpr auto denorm_min() noexcept requires ::patlib::simd::CFloatVector<VT> { return VT{underlimits::denorm_min()}; }
	};

	namespace numbers
	{
		template <typename... Ts_>
			requires (::patlib::simd::CFloatVector<::patlib::simd::vector<Ts_...>> && !::patlib::simd::vector<Ts_...>::ConstExpr)
		inline auto e_v<::patlib::simd::vector<Ts_...>> = ::patlib::simd::vector<Ts_...>{e_v<typename ::patlib::simd::vector_traits<::patlib::simd::vector<Ts_...>>::value_type>};
		template <typename... Ts_>
			requires (::patlib::simd::CFloatVector<::patlib::simd::vector<Ts_...>> && !::patlib::simd::vector<Ts_...>::ConstExpr)
		inline auto log2e_v<::patlib::simd::vector<Ts_...>> = ::patlib::simd::vector<Ts_...>{log2e_v<typename ::patlib::simd::vector_traits<::patlib::simd::vector<Ts_...>>::value_type>};
		template <typename... Ts_>
			requires (::patlib::simd::CFloatVector<::patlib::simd::vector<Ts_...>> && !::patlib::simd::vector<Ts_...>::ConstExpr)
		inline auto log10e_v<::patlib::simd::vector<Ts_...>> = ::patlib::simd::vector<Ts_...>{log10e_v<typename ::patlib::simd::vector_traits<::patlib::simd::vector<Ts_...>>::value_type>};
		template <typename... Ts_>
			requires (::patlib::simd::CFloatVector<::patlib::simd::vector<Ts_...>> && !::patlib::simd::vector<Ts_...>::ConstExpr)
		inline auto pi_v<::patlib::simd::vector<Ts_...>> = ::patlib::simd::vector<Ts_...>{pi_v<typename ::patlib::simd::vector_traits<::patlib::simd::vector<Ts_...>>::value_type>};
		template <typename... Ts_>
			requires (::patlib::simd::CFloatVector<::patlib::simd::vector<Ts_...>> && !::patlib::simd::vector<Ts_...>::ConstExpr)
		inline auto inv_pi_v<::patlib::simd::vector<Ts_...>> = ::patlib::simd::vector<Ts_...>{inv_pi_v<typename ::patlib::simd::vector_traits<::patlib::simd::vector<Ts_...>>::value_type>};
		template <typename... Ts_>
			requires (::patlib::simd::CFloatVector<::patlib::simd::vector<Ts_...>> && !::patlib::simd::vector<Ts_...>::ConstExpr)
		inline auto inv_sqrtpi_v<::patlib::simd::vector<Ts_...>> = ::patlib::simd::vector<Ts_...>{inv_sqrtpi_v<typename ::patlib::simd::vector_traits<::patlib::simd::vector<Ts_...>>::value_type>};
		template <typename... Ts_>
			requires (::patlib::simd::CFloatVector<::patlib::simd::vector<Ts_...>> && !::patlib::simd::vector<Ts_...>::ConstExpr)
		inline auto ln2_v<::patlib::simd::vector<Ts_...>> = ::patlib::simd::vector<Ts_...>{ln2_v<typename ::patlib::simd::vector_traits<::patlib::simd::vector<Ts_...>>::value_type>};
		template <typename... Ts_>
			requires (::patlib::simd::CFloatVector<::patlib::simd::vector<Ts_...>> && !::patlib::simd::vector<Ts_...>::ConstExpr)
		inline auto ln10_v<::patlib::simd::vector<Ts_...>> = ::patlib::simd::vector<Ts_...>{ln10_v<typename ::patlib::simd::vector_traits<::patlib::simd::vector<Ts_...>>::value_type>};
		template <typename... Ts_>
			requires (::patlib::simd::CFloatVector<::patlib::simd::vector<Ts_...>> && !::patlib::simd::vector<Ts_...>::ConstExpr)
		inline auto sqrt2_v<::patlib::simd::vector<Ts_...>> = ::patlib::simd::vector<Ts_...>{sqrt2_v<typename ::patlib::simd::vector_traits<::patlib::simd::vector<Ts_...>>::value_type>};
		template <typename... Ts_>
			requires (::patlib::simd::CFloatVector<::patlib::simd::vector<Ts_...>> && !::patlib::simd::vector<Ts_...>::ConstExpr)
		inline auto sqrt3_v<::patlib::simd::vector<Ts_...>> = ::patlib::simd::vector<Ts_...>{sqrt3_v<typename ::patlib::simd::vector_traits<::patlib::simd::vector<Ts_...>>::value_type>};
		template <typename... Ts_>
			requires (::patlib::simd::CFloatVector<::patlib::simd::vector<Ts_...>> && !::patlib::simd::vector<Ts_...>::ConstExpr)
		inline auto inv_sqrt3_v<::patlib::simd::vector<Ts_...>> = ::patlib::simd::vector<Ts_...>{inv_sqrt3_v<typename ::patlib::simd::vector_traits<::patlib::simd::vector<Ts_...>>::value_type>};
		template <typename... Ts_>
			requires (::patlib::simd::CFloatVector<::patlib::simd::vector<Ts_...>> && !::patlib::simd::vector<Ts_...>::ConstExpr)
		inline auto egamma_v<::patlib::simd::vector<Ts_...>> = ::patlib::simd::vector<Ts_...>{egamma_v<typename ::patlib::simd::vector_traits<::patlib::simd::vector<Ts_...>>::value_type>};
		template <typename... Ts_>
			requires (::patlib::simd::CFloatVector<::patlib::simd::vector<Ts_...>> && !::patlib::simd::vector<Ts_...>::ConstExpr)
		inline auto phi_v<::patlib::simd::vector<Ts_...>> = ::patlib::simd::vector<Ts_...>{phi_v<typename ::patlib::simd::vector_traits<::patlib::simd::vector<Ts_...>>::value_type>};

		template <typename... Ts_>
			requires (::patlib::simd::CFloatVector<::patlib::simd::vector<Ts_...>> && ::patlib::simd::vector<Ts_...>::ConstExpr)
		inline constexpr auto e_v<::patlib::simd::vector<Ts_...>> = ::patlib::simd::vector<Ts_...>{e_v<typename ::patlib::simd::vector_traits<::patlib::simd::vector<Ts_...>>::value_type>};
		template <typename... Ts_>
			requires (::patlib::simd::CFloatVector<::patlib::simd::vector<Ts_...>> && ::patlib::simd::vector<Ts_...>::ConstExpr)
		inline constexpr auto log2e_v<::patlib::simd::vector<Ts_...>> = ::patlib::simd::vector<Ts_...>{log2e_v<typename ::patlib::simd::vector_traits<::patlib::simd::vector<Ts_...>>::value_type>};
		template <typename... Ts_>
			requires (::patlib::simd::CFloatVector<::patlib::simd::vector<Ts_...>> && ::patlib::simd::vector<Ts_...>::ConstExpr)
		inline constexpr auto log10e_v<::patlib::simd::vector<Ts_...>> = ::patlib::simd::vector<Ts_...>{log10e_v<typename ::patlib::simd::vector_traits<::patlib::simd::vector<Ts_...>>::value_type>};
		template <typename... Ts_>
			requires (::patlib::simd::CFloatVector<::patlib::simd::vector<Ts_...>> && ::patlib::simd::vector<Ts_...>::ConstExpr)
		inline constexpr auto pi_v<::patlib::simd::vector<Ts_...>> = ::patlib::simd::vector<Ts_...>{pi_v<typename ::patlib::simd::vector_traits<::patlib::simd::vector<Ts_...>>::value_type>};
		template <typename... Ts_>
			requires (::patlib::simd::CFloatVector<::patlib::simd::vector<Ts_...>> && ::patlib::simd::vector<Ts_...>::ConstExpr)
		inline constexpr auto inv_pi_v<::patlib::simd::vector<Ts_...>> = ::patlib::simd::vector<Ts_...>{inv_pi_v<typename ::patlib::simd::vector_traits<::patlib::simd::vector<Ts_...>>::value_type>};
		template <typename... Ts_>
			requires (::patlib::simd::CFloatVector<::patlib::simd::vector<Ts_...>> && ::patlib::simd::vector<Ts_...>::ConstExpr)
		inline constexpr auto inv_sqrtpi_v<::patlib::simd::vector<Ts_...>> = ::patlib::simd::vector<Ts_...>{inv_sqrtpi_v<typename ::patlib::simd::vector_traits<::patlib::simd::vector<Ts_...>>::value_type>};
		template <typename... Ts_>
			requires (::patlib::simd::CFloatVector<::patlib::simd::vector<Ts_...>> && ::patlib::simd::vector<Ts_...>::ConstExpr)
		inline constexpr auto ln2_v<::patlib::simd::vector<Ts_...>> = ::patlib::simd::vector<Ts_...>{ln2_v<typename ::patlib::simd::vector_traits<::patlib::simd::vector<Ts_...>>::value_type>};
		template <typename... Ts_>
			requires (::patlib::simd::CFloatVector<::patlib::simd::vector<Ts_...>> && ::patlib::simd::vector<Ts_...>::ConstExpr)
		inline constexpr auto ln10_v<::patlib::simd::vector<Ts_...>> = ::patlib::simd::vector<Ts_...>{ln10_v<typename ::patlib::simd::vector_traits<::patlib::simd::vector<Ts_...>>::value_type>};
		template <typename... Ts_>
			requires (::patlib::simd::CFloatVector<::patlib::simd::vector<Ts_...>> && ::patlib::simd::vector<Ts_...>::ConstExpr)
		inline constexpr auto sqrt2_v<::patlib::simd::vector<Ts_...>> = ::patlib::simd::vector<Ts_...>{sqrt2_v<typename ::patlib::simd::vector_traits<::patlib::simd::vector<Ts_...>>::value_type>};
		template <typename... Ts_>
			requires (::patlib::simd::CFloatVector<::patlib::simd::vector<Ts_...>> && ::patlib::simd::vector<Ts_...>::ConstExpr)
		inline constexpr auto sqrt3_v<::patlib::simd::vector<Ts_...>> = ::patlib::simd::vector<Ts_...>{sqrt3_v<typename ::patlib::simd::vector_traits<::patlib::simd::vector<Ts_...>>::value_type>};
		template <typename... Ts_>
			requires (::patlib::simd::CFloatVector<::patlib::simd::vector<Ts_...>> && ::patlib::simd::vector<Ts_...>::ConstExpr)
		inline constexpr auto inv_sqrt3_v<::patlib::simd::vector<Ts_...>> = ::patlib::simd::vector<Ts_...>{inv_sqrt3_v<typename ::patlib::simd::vector_traits<::patlib::simd::vector<Ts_...>>::value_type>};
		template <typename... Ts_>
			requires (::patlib::simd::CFloatVector<::patlib::simd::vector<Ts_...>> && ::patlib::simd::vector<Ts_...>::ConstExpr)
		inline constexpr auto egamma_v<::patlib::simd::vector<Ts_...>> = ::patlib::simd::vector<Ts_...>{egamma_v<typename ::patlib::simd::vector_traits<::patlib::simd::vector<Ts_...>>::value_type>};
		template <typename... Ts_>
			requires (::patlib::simd::CFloatVector<::patlib::simd::vector<Ts_...>> && ::patlib::simd::vector<Ts_...>::ConstExpr)
		inline constexpr auto phi_v<::patlib::simd::vector<Ts_...>> = ::patlib::simd::vector<Ts_...>{phi_v<typename ::patlib::simd::vector_traits<::patlib::simd::vector<Ts_...>>::value_type>};
	} // namespace numbers

} // namespace std

namespace patlib::constants
{
	// BUG: These aren't picked up???
	template <typename... Ts_>
		requires (!simd::vector<Ts_...>::ConstExpr)
	inline auto one<simd::vector<Ts_...>> = simd::vector<Ts_...>{tag::one};
	template <typename... Ts_>
		requires (!simd::vector<Ts_...>::ConstExpr)
	inline auto two<simd::vector<Ts_...>> = simd::vector<Ts_...>{tag::two};
	template <typename... Ts_>
		requires (simd::CFloatVector<simd::vector<Ts_...>> && !simd::vector<Ts_...>::ConstExpr)
	inline auto half<simd::vector<Ts_...>> = simd::vector<Ts_...>{tag::half};

	template <typename... Ts_>
		requires (simd::CIntegralVector<simd::vector<Ts_...>> && !simd::vector<Ts_...>::ConstExpr)
	inline auto inflowest<simd::vector<Ts_...>> = std::numeric_limits<simd::vector<Ts_...>>::lowest();
	template <typename... Ts_>
		requires (simd::CFloatVector<simd::vector<Ts_...>> && !simd::vector<Ts_...>::ConstExpr)
	inline auto inflowest<simd::vector<Ts_...>> = -std::numeric_limits<simd::vector<Ts_...>>::infinity();

	template <typename... Ts_>
		requires (simd::CIntegralVector<simd::vector<Ts_...>> && !simd::vector<Ts_...>::ConstExpr)
	inline auto infhighest<simd::vector<Ts_...>> = std::numeric_limits<simd::vector<Ts_...>>::max();
	template <typename... Ts_>
		requires (simd::CFloatVector<simd::vector<Ts_...>> && !simd::vector<Ts_...>::ConstExpr)
	inline auto infhighest<simd::vector<Ts_...>> = std::numeric_limits<simd::vector<Ts_...>>::infinity();


	template <typename... Ts_>
		requires (simd::vector<Ts_...>::ConstExpr)
	inline constexpr auto one<simd::vector<Ts_...>> = simd::vector<Ts_...>{tag::one};
	template <typename... Ts_>
		requires (simd::vector<Ts_...>::ConstExpr)
	inline constexpr auto two<simd::vector<Ts_...>> = simd::vector<Ts_...>{tag::two};
	template <typename... Ts_>
		requires (simd::CFloatVector<simd::vector<Ts_...>> && simd::vector<Ts_...>::ConstExpr)
	inline constexpr auto half<simd::vector<Ts_...>> = simd::vector<Ts_...>{tag::half};

	template <typename... Ts_>
		requires (simd::CIntegralVector<simd::vector<Ts_...>> && simd::vector<Ts_...>::ConstExpr)
	inline constexpr auto inflowest<simd::vector<Ts_...>> = std::numeric_limits<simd::vector<Ts_...>>::lowest();
	template <typename... Ts_>
		requires (simd::CFloatVector<simd::vector<Ts_...>> && simd::vector<Ts_...>::ConstExpr)
	inline constexpr auto inflowest<simd::vector<Ts_...>> = -std::numeric_limits<simd::vector<Ts_...>>::infinity();

	template <typename... Ts_>
		requires (simd::CIntegralVector<simd::vector<Ts_...>> && simd::vector<Ts_...>::ConstExpr)
	inline constexpr auto infhighest<simd::vector<Ts_...>> = std::numeric_limits<simd::vector<Ts_...>>::max();
	template <typename... Ts_>
		requires (simd::CFloatVector<simd::vector<Ts_...>> && simd::vector<Ts_...>::ConstExpr)
	inline constexpr auto infhighest<simd::vector<Ts_...>> = std::numeric_limits<simd::vector<Ts_...>>::infinity();
} // namespace patlib::constants
