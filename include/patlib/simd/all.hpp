/*
	PatLib

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <patlib/base.hpp>

#include "basic.hpp"
#include "simd.hpp"
#include "convert.hpp"
#include "io.hpp"

namespace patlib::simd::impl
{
	// Vector implementation selection
	template <typename Type_, typename NbValues_, typename Category_>
	struct best_vector<Type_, NbValues_, Category_, specialized> { using type = basic_vector<Type_, NbValues_, Category_>; };

	#ifdef PL_LIB_VECTOR_256_DOUBLE_SIMD
		template <typename Category_>
		struct best_vector<double, size_c<4>, categories::geometric_t<Category_>, specialized> { using type = simd_256_geometric_vector<double, size_c<4>>; };

		template <>
		struct best_vector<double, size_c<4>, categories::color_t, specialized> { using type = color_vector<double, size_c<4>, types::simd_vector_t<types::simd_256_t>>; };
	#elif defined(PL_LIB_VECTOR_128_FLOAT_SIMD)
		template <typename Category_>
		struct best_vector<double, size_c<4>, categories::geometric_t<Category_>, specialized> { using type = simd_128_geometric_vector<double, size_c<4>>; };

		template <>
		struct best_vector<double, size_c<4>, categories::color_t, specialized> { using type = color_vector<double, size_c<4>, types::simd_vector_t<types::simd_128_t>>; };
	#endif

	#ifdef PL_LIB_VECTOR_128_FLOAT_SIMD
		template <typename Category_>
		struct best_vector<float, size_c<4>, categories::geometric_t<Category_>, specialized> { using type = simd_128_geometric_vector<float, size_c<4>>; };

		template <>
		struct best_vector<float, size_c<4>, categories::color_t, specialized> { using type = color_vector<float, size_c<4>, types::simd_vector_t<types::simd_128_t>>; };

		template <typename Category_>
		struct best_vector<float, size_c<4>, Category_, specialized> { using type = simd_vector<float, size_c<4>, types::simd_128_t, Category_>; };
	#endif

	#ifdef PL_LIB_VECTOR_256_FLOAT_SIMD
		template <typename Category_>
		struct best_vector<float, size_c<8>, Category_, specialized> { using type = simd_vector<float, size_c<8>, types::simd_256_t, Category_>; };
	#endif

	#ifdef PL_LIB_VECTOR_128_DOUBLE_SIMD
		template <typename Category_>
		struct best_vector<double, size_c<2>, Category_, specialized> { using type = simd_vector<double, size_c<2>, types::simd_128_t, Category_>; };
	#endif

	#ifdef PL_LIB_VECTOR_256_DOUBLE_SIMD
		template <typename Category_>
		struct best_vector<double, size_c<4>, Category_, specialized> { using type = simd_vector<double, size_c<4>, types::simd_256_t, Category_>; };
	#endif


	#ifdef PL_LIB_VECTOR_256_FLOAT_SIMD
		constexpr auto max_float_simd_size = 8;
	#elif defined(PL_LIB_VECTOR_128_FLOAT_SIMD)
		constexpr auto max_float_simd_size = 4;
	#else
		constexpr auto max_float_simd_size = 0;
	#endif

	#ifdef PL_LIB_VECTOR_256_DOUBLE_SIMD
		constexpr auto max_double_simd_size = 8;
	#elif defined(PL_LIB_VECTOR_128_DOUBLE_SIMD)
		constexpr auto max_double_simd_size = 4;
	#else
		constexpr auto max_double_simd_size = 0;
	#endif
} // patlib::simd::impl

namespace patlib::simd
{
	constexpr auto max_float_simd_size = impl::max_float_simd_size;
	constexpr auto max_double_simd_size = impl::max_double_simd_size;
}