/*
	PatLib

	Copyright (C) 2020 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "base.hpp"
#include "type.hpp"
#include "concepts.hpp"
#include "term_color.hpp"

#include <assert.h>
#include <experimental/source_location>
#include <utility>
#include <concepts>
#include <type_traits>
#include <span>
#include <cstring>
#include <bitset>
#include <bit>

#include <boost/config/pragma_message.hpp>
#ifdef PL_ASSERT_BACKTRACE
	#include <boost/stacktrace.hpp>
#endif

namespace patlib
{
	template <typename T_>
	[[nodiscard]] constexpr capture_type<T_> forward_return(T_&& v) noexcept
		{ return PL_FWD(v); }

	#define PL_FWD_RETURN(...) ::patlib::forward_return(PL_FWD(__VA_ARGS__))

	namespace impl
	{
		inline void assert_message(const std::experimental::source_location &sl, const char * const msg = nullptr) noexcept
		{
			std::cerr << termcolor::red << "ASSERT FAILED:" << (msg ? " " : "") << (msg ? msg : "") << termcolor::reset << " [" << sl.line() << "] " << sl.file_name() << ": " << sl.function_name()
				#ifdef PL_ASSERT_BACKTRACE
					<< '\n' << boost::stacktrace::stacktrace()
				#endif
				<< '\n';
		}

		inline void assert_failed() {}

		constexpr void assert_impl(bool v, const std::experimental::source_location &sl, const char * const msg = nullptr) noexcept
		{
			if(!v)
			{
				if !consteval
				{
					assert_message(sl, msg);
					assert(false);
				}
				else
				{
					#ifdef __has_builtin
						#if __has_builtin(__builtin_unreachable) && __has_builtin(__builtin_trap)
							#ifdef __OPTIMIZE__
								__builtin_unreachable();
							#else
								__builtin_trap();
							#endif
						#else
							assert_failed();
						#endif
					#elif defined(_MSC_VER)
						__assume(false);
					#else
						assert_failed();
					#endif
				}
			}
		}
	} // namespace impl

	constexpr decltype(auto) assert_(
		CExplicitConvertibleTo<bool> auto &&v,
		[[maybe_unused]] const char * const msg = nullptr,
		[[maybe_unused]] const std::experimental::source_location &sl = std::experimental::source_location::current()) noexcept
	{
		impl::assert_impl(static_cast<bool>(v), sl, msg);

		return PL_FWD_RETURN(v);
	}

	#if !defined(NDEBUG) || defined(PL_RELEASE_ASSERTS)
		#define plassert(...) ::patlib::assert_(__VA_ARGS__)

		#define PL_LIB_ASSERTS

		#if defined(NDEBUG) && defined(PL_RELEASE_ASSERTS)
			BOOST_PRAGMA_MESSAGE("PL_RELEASE_ASSERTS Enabled.")
		#endif
	#else
		#define plassert(...) ((void)0)
	#endif


	template <typename FC_>
	struct [[nodiscard]] nodiscard_lambda
	{
		FC_ f;

		constexpr nodiscard_lambda(FC_&& f_) noexcept(CNoThrowContructible<FC_, decltype(f_)>) : f{PL_FWD(f_)} {}

		[[nodiscard]] constexpr decltype(auto) operator() (auto&&... args) const noexcept(noexcept(std::invoke(f, PL_FWD(args)...)))
			{ return std::invoke(f, PL_FWD(args)...); }

		[[nodiscard]] constexpr decltype(auto) operator() (auto&&... args) noexcept(noexcept(std::invoke(f, PL_FWD(args)...)))
			{ return std::invoke(f, PL_FWD(args)...); }
	};
	template <typename FC_>
	nodiscard_lambda(FC_&&) -> nodiscard_lambda<FC_>;

	constinit const struct nodiscard_t
	{
		[[nodiscard]] constexpr auto operator| (auto&& f) const noexcept
			{ return nodiscard_lambda{PL_FWD(f)}; }
	} nodiscard_{};

	template <typename NBT_, typename T_ = NBT_>
	[[nodiscard]] constexpr T_ fill_bits(NBT_ nb, type_c<T_> = {}) noexcept
	{
		if constexpr(std::is_enum_v<T_>)
			return static_cast<T_>(fill_bits(nb, type_v<std::underlying_type_t<T_>>));
		else
		{
			T_ v{};

			while(nb)
				v |= T_{1} << --nb;

			return v;
		}
	}

	template <typename T_>
	[[nodiscard]] constexpr T_ flip_bits(T_ v) noexcept
	{
		if constexpr(std::is_enum_v<T_>)
			return static_cast<T_>(flip_bits(underlying_value(v)));
		else
			return ~v;
	}

	template <typename T1_, typename T2_>
		requires std::swappable_with<T1_ &, T2_ &>
	constexpr void adl_swap(T1_ &a, T2_ &b) noexcept(CNoThrowSwappableWith<T1_ &, T2_ &>)
	{
		using std::swap;
		swap(a, b);
	}

	constexpr bool optional_assign_to(const auto &opt, auto &v) noexcept(noexcept(v = opt.value()))
	{
		if(opt)
			v = opt.value();

		return static_cast<bool>(opt);
	}

	constexpr bool optional_move_to(const auto &opt, auto &v) noexcept(noexcept(v = std::move(opt.value())))
	{
		if(opt)
			v = std::move(opt.value());

		return static_cast<bool>(opt);
	}

	namespace util_impl
	{
		template <szt... seq>
		constexpr void tuple_for_each_index_helper(auto&& fc, auto&& t, std::index_sequence<seq...>)
		{
			( std::invoke(PL_FWD(fc), size_v<seq>, std::get<seq>(PL_FWD(t))) , ... );
		}

		template <szt... seq>
		[[nodiscard]] constexpr auto tuple_transform_helper(auto&& fc, auto&& t, std::index_sequence<seq...>)
		{
			return std::forward_as_tuple( std::invoke(PL_FWD(fc), size_v<seq>, std::get<seq>(PL_FWD(t)))... );
		}
	} // namespace util_impl

	template <typename Tuple_>
	constexpr decltype(auto) tuple_for_each_index(Tuple_&& t, auto &&fc)
	{
		util_impl::tuple_for_each_index_helper(PL_FWD(fc), PL_FWD(t), std::make_index_sequence<std::tuple_size_v<remove_cvref_t<Tuple_>>>{});
		return PL_FWD_RETURN(t);
	}

	constexpr decltype(auto) tuple_for_each(auto&& t, auto &&fc)
	{
		std::apply([&](auto&&... items) { ( std::invoke(PL_FWD(fc), PL_FWD(items)) , ... ); }, PL_FWD(t));
		return PL_FWD_RETURN(t);
	}

	template <typename Tuple_>
	[[nodiscard]] constexpr auto tuple_transform(Tuple_&& t, auto &&fc)
	{
		return util_impl::tuple_transform_helper(PL_FWD(fc), PL_FWD(t), std::make_index_sequence<std::tuple_size_v<remove_cvref_t<Tuple_>>>{});
	}

	[[nodiscard]] constexpr auto tuple_add(auto&& tpl, auto&&... newElements)
		{ return std::tuple_cat(PL_FWD(tpl), std::make_tuple(PL_FWD(newElements)...)); }

	[[nodiscard]] constexpr auto tuple_add_forward(auto&& tpl, auto&&... newElements)
		{ return std::tuple_cat(PL_FWD(tpl), std::forward_as_tuple(PL_FWD(newElements)...)); }

	[[nodiscard]] constexpr auto tuple_add_capture(auto&& tpl, auto&&... newElements)
		{ return std::tuple_cat(PL_FWD(tpl), std::tuple<capture_type<decltype(newElements)>...>{PL_FWD(newElements)...}); }

	/*template <typename T_>
	constexpr decltype(auto) apply(T_&& v, auto&& fc)
	{
		if constexpr(CTuple<std::decay_t<T_>>)
			tuple_for_each(PL_FWD(v), PL_FWD(fc));
		else
			std::invoke(PL_FWD(fc), PL_FWD(v));

		return PL_FWD_RETURN(v);
	}*/

	// In C++ 23, std::apply only work on tuple-like, which are only a few select standard types.
	// This version work on anything that's like a tuple.
	template <typename T_>
	decltype(auto) unpack_apply(auto&& fc, T_&& tpl)
	{
		return unpack_index_sequence(
			std::make_index_sequence<std::tuple_size_v<std::decay_t<T_>>>(),
			[&]<szt... I_>(const size_c<I_>...) -> decltype(auto) { return std::invoke(PL_FWD(fc), std::get<I_>(PL_FWD(tpl))...); }
		);
	}

	[[nodiscard]] constexpr auto applier(auto fc) noexcept
	{
		return [fc = std::move(fc)](auto&& tpl) noexcept(noexcept(unpack_apply(std::declval<decltype(fc)>(), PL_FWD(tpl)))) -> decltype(auto) {
			return unpack_apply(fc, PL_FWD(tpl));
		};
	}

	[[nodiscard]] constexpr auto by_value(auto&& val) { return val; }

	template <szt size>
	constexpr void index_sequence_loop(const size_c<size>, auto&& fc)
	{
		unpack_index_sequence(
			std::make_index_sequence<size>(),
			[&](const auto... i){ ( std::invoke(PL_FWD(fc), i), ... ); }
		);
	}

	template <szt size>
	constexpr decltype(auto) index_sequence_call(const size_c<size>, auto&& fc)
	{
		return unpack_index_sequence(
			std::make_index_sequence<size>(),
			[&](const auto... i) -> decltype(auto) { return std::invoke(PL_FWD(fc), i...); }
		);
	}

	constexpr void variadic_loop(auto&& fc, auto&&... args)
	{
		tuple_for_each_index(std::forward_as_tuple(PL_FWD(args)...), PL_FWD(fc));
	}

	namespace pipe_impl
	{
		template <typename FC_>
		struct piped_fc
		{
		public:
			constexpr piped_fc(FC_ fc) : mFc{std::move(fc)} {}

			[[nodiscard]] constexpr auto operator() (auto&&... args) const
			{
				return pipe_impl::piped_fc{[fc = mFc, args = capturer{PL_FWD(args)...}](auto&& firstArg) mutable -> decltype(auto) {
					return std::apply(fc, std::tuple_cat(std::forward_as_tuple(PL_FWD(firstArg)), args.vars));
				}};
			}

		private:
			FC_ mFc;

			friend constexpr decltype(auto) operator| (auto&& left, const piped_fc &piped)
				{ return std::invoke(piped.mFc, PL_FWD(left)); }

			friend constexpr decltype(auto) operator| (auto&& left, piped_fc &piped)
				{ return std::invoke(piped.mFc, PL_FWD(left)); }

			friend constexpr decltype(auto) operator| (auto&& left, piped_fc&& piped)
				{ return std::invoke(piped.mFc, PL_FWD(left)); }
		};
	} // namespace pipe_impl

	[[nodiscard]] constexpr auto pipe(auto fc, auto&&... args)
	{
		return pipe_impl::piped_fc{[fc = std::move(fc), args = capturer{PL_FWD(args)...}](auto&& firstArg, auto&&... moreArgs) -> decltype(auto) {
			return std::apply(fc, std::tuple_cat(std::forward_as_tuple(PL_FWD(firstArg)), args.vars, std::forward_as_tuple(PL_FWD(moreArgs)...)));
		}};
	}

	[[nodiscard]] constexpr auto pipe_mutable(auto fc, auto&&... args)
	{
		return pipe_impl::piped_fc{[fc = std::move(fc), args = capturer{PL_FWD(args)...}](auto&& firstArg, auto&&... moreArgs) mutable -> decltype(auto) {
			return std::apply(fc, std::tuple_cat(std::forward_as_tuple(PL_FWD(firstArg)), args.vars, std::forward_as_tuple(PL_FWD(moreArgs)...)));
		}};
	}

	namespace impl
	{
		template <typename T_, typename WithType_>
		struct common_3way_compare_type {};

		template <typename... Ts_, typename WithType_>
		struct common_3way_compare_type<std::tuple<Ts_...>, WithType_> { using type = std::common_comparison_category_t<std::compare_three_way_result_t<Ts_, WithType_>...>; };

		template <typename T_, typename WithType_>
		using common_3way_compare_type_t = typename common_3way_compare_type<T_, WithType_>::type;

		template <szt no, typename... Ts_>
		[[nodiscard]] constexpr auto compare_3way(const std::tuple<Ts_...> &tpl, size_c<no>, const auto &with)
			-> common_3way_compare_type_t<std::decay_t<decltype(tpl)>, std::decay_t<decltype(with)>>
		{
			const auto cmp = std::compare_three_way{}(std::get<no>(tpl), with);

			if constexpr(no + 1 == std::tuple_size_v<std::decay_t<decltype(tpl)>>)
				return cmp;
			else
			{
				if(cmp != 0)
					return cmp;

				return compare_3way(tpl, size_v<no + 1>, with);
			}
		}
	} // namespace impl

	template <typename... Ts_>
	[[nodiscard]] constexpr auto compare_3way(const auto &with, Ts_&&... values)
	{
		return impl::compare_3way(std::forward_as_tuple(PL_FWD(values)...), size_v<0>, with);
	}

	constexpr void call_on(auto&& fc, auto&&... args)
		{ (std::invoke(fc, args), ...); }

	inline constexpr auto true_pred = [](auto&&...) noexcept { return true; };
	using true_pred_t = decltype(true_pred);
	inline constexpr auto false_pred = [](auto&&...) noexcept { return false; };
	using false_pred_t = decltype(false_pred);

	template <typename ValueType_, ValueType_ SplitPos_ = 16>
		requires (SplitPos_ > 0 && SplitPos_ < sizeof(ValueType_) * CHAR_BIT)
	class split_value
	{
	public:
		using value_type = ValueType_;

		static constinit const value_type SplitPosition = SplitPos_;
		static constinit const value_type LowSize = value_type{1} << SplitPosition;
		static constinit const value_type HighSize = (std::numeric_limits<value_type>::max() >> SplitPosition) + 1;

		struct bits
		{
			value_type
				low : SplitPosition,
				high : sizeof(value_type) * CHAR_BIT - SplitPosition;
		};

	private:
		bits mValue{};

	public:


		constexpr split_value() = default;
		constexpr split_value(value_type i) :
			mValue{std::bit_cast<bits>(i)} {}
		constexpr split_value(value_type ii, value_type bi) :
			mValue{ii, bi} {}
		constexpr split_value(bits i) :
			mValue{i} {}

		[[nodiscard]] constexpr auto index() const noexcept { return std::bit_cast<value_type>(mValue); }
		constexpr auto &index(value_type i) noexcept { mValue = std::bit_cast<bits>(i); return *this; }

		[[nodiscard]] constexpr auto low() const noexcept
			{ return mValue.low; }
		constexpr auto &low(value_type i) noexcept
		{
			mValue.low = i;
			return *this;
		}

		[[nodiscard]] constexpr auto high() const noexcept
			{ return mValue.high; }
		constexpr auto &high(value_type i) noexcept
		{
			mValue.high = i;
			return *this;
		}
	};

	template <szt NB_>
	[[nodiscard]] constexpr auto make_bitset(size_c<NB_>, auto... bits) noexcept
	{
		using T = unsigned long long;

		const auto getv = [&](auto v) -> T
			{
				if constexpr(std::same_as<decltype(v), tag::nothing_t>)
					return 0;
				else
					return T{std::get<1>(v)} << underlying_value(std::get<0>(v));
			};
		return std::bitset<NB_>{ (T{} | ... | getv(bits)) };
	}


	template <auto T_>
	using tag_t = std::decay_t<decltype(T_)>;

	namespace adl
	{
		template <typename Tag_>
		constexpr void tag_invoke(Tag_ tag) = delete;
	}

	namespace impl_cpo
	{
		using adl::tag_invoke;

		inline constexpr auto tag_invoke_adl = [](auto&& fn, auto&&... args)
			noexcept(noexcept(tag_invoke(fn, PL_FWD(args)...)))
			-> decltype(auto)
			{ return tag_invoke(PL_FWD(fn), PL_FWD(args)...); };

		constexpr decltype(auto) tag_invoke_adl_test(auto&& fn, auto&&... args)
			noexcept(noexcept(tag_invoke(fn, PL_FWD(args)...)))
			{ return tag_invoke(PL_FWD(fn), PL_FWD(args)...); }

		template <typename Fn_, typename... Args_>
		concept c_tag_invokable = requires(Fn_&& fn, Args_&&... args) { { tag_invoke(PL_FWD(fn), PL_FWD(args)...) }; }; //std::invocable<tag_invoke_adl_test_t, Fn_, Args_...>;
		template <typename Fn_, typename... Args_>
		concept c_tag_default_invokable = requires(Fn_&& fn, Args_&&... args) { { PL_FWD(fn).default_invoke(PL_FWD(args)...) }; };

	} // namespace impl_cpo

	PL_MAKE_TAG(p_nodiscard)
	PL_MAKE_TAG(p_pass_self)

	template <CProperties Props_, typename Fn_>
	struct cpo_base
	{
		template <typename... Args_>
			requires (!Props_::has_prop(p_nodiscard) && (impl_cpo::c_tag_invokable<Fn_, Args_...> || !impl_cpo::c_tag_default_invokable<Fn_, Args_...>))
		constexpr decltype(auto) operator() (Args_&&... args) const
			noexcept(noexcept( impl_cpo::tag_invoke_adl(static_cast<const Fn_ &>(*this), PL_FWD(args)...) ))
			{ return impl_cpo::tag_invoke_adl(static_cast<const Fn_ &>(*this), PL_FWD(args)...); }

		template <typename... Args_>
			requires (!Props_::has_prop(p_nodiscard) && !impl_cpo::c_tag_invokable<Fn_, Args_...> && impl_cpo::c_tag_default_invokable<Fn_, Args_...>)
		constexpr decltype(auto) operator() (Args_&&... args) const
			noexcept(noexcept( static_cast<const Fn_ &>(*this).default_invoke(PL_FWD(args)...) ))
			{ return static_cast<const Fn_ &>(*this).default_invoke(PL_FWD(args)...); }

		template <typename... Args_>
			requires (Props_::has_prop(p_nodiscard) && (impl_cpo::c_tag_invokable<Fn_, Args_...> || !impl_cpo::c_tag_default_invokable<Fn_, Args_...>))
		[[nodiscard]] constexpr decltype(auto) operator() (Args_&&... args) const
			noexcept(noexcept( impl_cpo::tag_invoke_adl(static_cast<const Fn_ &>(*this), PL_FWD(args)...) ))
			{ return impl_cpo::tag_invoke_adl(static_cast<const Fn_ &>(*this), PL_FWD(args)...); }

		template <typename... Args_>
			requires (Props_::has_prop(p_nodiscard) && !impl_cpo::c_tag_invokable<Fn_, Args_...> && impl_cpo::c_tag_default_invokable<Fn_, Args_...>)
		[[nodiscard]] constexpr decltype(auto) operator() (Args_&&... args) const
			noexcept(noexcept( static_cast<const Fn_ &>(*this).default_invoke(PL_FWD(args)...) ))
			{ return static_cast<const Fn_ &>(*this).default_invoke(PL_FWD(args)...); }
	};

	template <CProperties Props_ = properties_t<>, typename T_ = decltype([]{})>
	struct [[nodiscard]] cpo_entry : public cpo_base<Props_, cpo_entry<Props_, T_>>
	{
		constexpr cpo_entry(Props_ = {}, T_ = {}) noexcept {}

		using cpo_base<Props_, cpo_entry<Props_, T_>>::operator();
	};

	template <CProperties Props_, typename... Ts_>
	struct [[nodiscard]] cpo_entry_with_defaults : public cpo_base<Props_, cpo_entry_with_defaults<Props_, Ts_...>>
	{
	private:
		static inline constexpr auto mFc = overload_set{Ts_{}...};

	public:
		constexpr cpo_entry_with_defaults(Props_, Ts_...) noexcept {}

		using cpo_base<Props_, cpo_entry_with_defaults<Props_, Ts_...>>::operator();

		constexpr decltype(auto) default_invoke(auto&&... args) const noexcept(noexcept(mFc(*this, PL_FWD(args)...)))
			requires (!Props_::has_prop(p_nodiscard) && Props_::has_prop(p_pass_self))
			{ return mFc(*this, PL_FWD(args)...); }

		constexpr decltype(auto) default_invoke(auto&&... args) const noexcept(noexcept(mFc(PL_FWD(args)...)))
			requires (!Props_::has_prop(p_nodiscard) && !Props_::has_prop(p_pass_self))
			{ return mFc(PL_FWD(args)...); }

		[[nodiscard]] constexpr decltype(auto) default_invoke(auto&&... args) const noexcept(noexcept(mFc(*this, PL_FWD(args)...)))
			requires (Props_::has_prop(p_nodiscard) && Props_::has_prop(p_pass_self))
			{ return mFc(*this, PL_FWD(args)...); }

		[[nodiscard]] constexpr decltype(auto) default_invoke(auto&&... args) const noexcept(noexcept(mFc(PL_FWD(args)...)))
			requires (Props_::has_prop(p_nodiscard) && !Props_::has_prop(p_pass_self))
			{ return mFc(PL_FWD(args)...); }
	};
	template <CProperties Props_, typename... Ts_>
	cpo_entry_with_defaults(Props_&&, Ts_&&...) -> cpo_entry_with_defaults<Props_, Ts_...>;


	namespace impl
	{
		[[nodiscard]] constexpr bool is_valid_default(const auto &, const tag::nothing_t) noexcept
			{ return true; }

		template <CPointerLike T_>
		[[nodiscard]] constexpr bool is_valid_default(const T_ &p, const tag::nothing_t) noexcept
			{ return p != nullptr; }

		template <typename T_>
		[[nodiscard]] constexpr bool is_valid_default(const T_ &v, const tag::nothing_t) noexcept
			requires requires(const T_ &o) { {o.is_valid()} -> CBoolLike; }
			{ return v.is_valid(); }

	} // namespace impl

	inline constexpr auto is_valid = cpo_entry_with_defaults
	{
		properties_t{p_nodiscard, p_pass_self},

		[](const auto &self, const auto &v) noexcept -> bool
			{ return self(v, tag::nothing); },

		[](const auto &, const auto &v, const tag::nothing_t t) noexcept -> bool
			{ return impl::is_valid_default(v, t); }

	};

	template <typename Category_ = tag::nothing_t>
	constexpr decltype(auto) assert_valid(
		auto&& v,
		[[maybe_unused]] Category_ tag = {},
		[[maybe_unused]] const char * const msg = nullptr,
		[[maybe_unused]] const std::experimental::source_location &sl = std::experimental::source_location::current()) noexcept
	{
		#ifdef PL_LIB_ASSERTS
			assert_(is_valid(v, tag), msg, sl);
		#endif

		return PL_FWD_RETURN(v);
	}


	namespace impl
	{
		[[nodiscard]] constexpr decltype(auto) as_const_view_default(const auto &v) noexcept
			{ return std::as_const(v); }

		template <CPointerLike PointerType_>
		[[nodiscard]] constexpr auto as_const_view_default(const PointerType_ &v) noexcept
		{
			using ET = typename std::pointer_traits<PointerType_>::element_type;
			using CPT = typename std::pointer_traits<PointerType_>::template rebind<std::add_const_t<ET>>;

			return static_cast<CPT>(v);
		}

		template <typename T_, szt Extent_>
		[[nodiscard]] constexpr auto as_const_view_default(const std::span<T_, Extent_> v) noexcept
			{ return std::span<const T_, Extent_>{v}; }
	} // namespace impl

	inline constexpr auto as_const_view = cpo_entry_with_defaults
	{
		properties_t{p_nodiscard},

		[](auto &v) noexcept
			{ return impl::as_const_view_default(v); }
	};
}
