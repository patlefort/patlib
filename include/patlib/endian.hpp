/*
	PatLib

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <istream>
#include <ostream>
#include <string>
#include <bit>

#include <boost/endian/arithmetic.hpp>

namespace patlib
{
	[[nodiscard]] inline std::string to_string(const std::endian endian)
	{
		std::string s;

		switch(endian)
		{
		case std::endian::big:
			s = "big";
			break;
		case std::endian::little:
			s = "little";
			break;
		default:
			s = "native";
		};

		return s;
	}

	inline std::endian from_string(type_c<std::endian>, std::string_view sv) noexcept
	{
		std::endian endian;

		if(sv == "big")
			endian = std::endian::big;
		else if(sv == "little")
			endian = std::endian::little;
		else
			endian = std::endian::native;

		return endian;
	}

	struct stream_endian_string
	{
		std::endian endian;

		friend inline std::ostream &operator<<(std::ostream &os, stream_endian_string e)
		{
			switch(e.endian)
			{
			case std::endian::big:
				os << "big";
				break;
			case std::endian::little:
				os << "little";
				break;
			default:
				os << "native";
			};

			return os;
		}

		friend inline std::istream &operator>>(std::istream &is, stream_endian_string &e)
		{
			std::string word;

			is >> word;

			if(word == "big")
				e.endian = std::endian::big;
			else if(word == "little")
				e.endian = std::endian::little;
			else
				e.endian = std::endian::native;

			return is;
		}
	};

	constinit const std::endian native_endian =
		#if BOOST_ENDIAN_BIG_BYTE
			std::endian::big;
		#elif BOOST_ENDIAN_LITTLE_BYTE
			std::endian::little;
		#else
			std::endian::native;
		#endif

	template <boost::endian::order order_>
	constinit const auto endian_constant_boost = std::integral_constant<boost::endian::order, order_>{};
}
