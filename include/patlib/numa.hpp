/*
	PatLib

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "base.hpp"
#include "exception.hpp"
#include "range.hpp"
#include "utility.hpp"

#include <numa.h>

#include <memory>
#include <memory_resource>
#include <cstddef>

#define PL_LIB_NUMA

namespace patlib::numa
{
	[[nodiscard]] inline int node_count() noexcept
	{
		auto mask = numa_get_run_node_mask();

		return (int)numa_bitmask_weight(mask);
	}

	[[nodiscard]] inline int node_num(int index) noexcept
	{
		auto mask = numa_get_run_node_mask();

		for(int i=0, m=numa_bitmask_nbytes(mask) * 8; i<m; ++i)
		{
			if(numa_bitmask_isbitset(mask, i))
			{
				if(!index)
					return i;

				--index;
			}
		}

		return -1;
	}

	[[nodiscard]] inline int node_index(int num) noexcept
	{
		auto mask = numa_get_run_node_mask();
		int index = 0;

		for(int i=0; i<num; ++i)
		{
			if(numa_bitmask_isbitset(mask, i))
				++index;
		}

		return index;
	}

	struct [[nodiscard]] scoped_run_on_node
	{
	private:
		bitmask *mOldMask = nullptr;

	public:

		scoped_run_on_node(int node) noexcept
		{
			if(numa_available() > -1)
			{
				mOldMask = numa_get_run_node_mask();

				numa_run_on_node(node);
				numa_set_preferred(node);
			}
		}

		~scoped_run_on_node()
		{
			if(numa_available() > -1)
			{
				numa_run_on_node_mask(mOldMask);
				numa_set_preferred(-1);
				numa_bitmask_free(mOldMask);
			}
		}
	};

	struct [[nodiscard]] scoped_alloc_on_node
	{
		scoped_alloc_on_node(int node) noexcept
		{
			if(numa_available() > -1)
				numa_set_preferred(node);
		}

		~scoped_alloc_on_node()
		{
			if(numa_available() > -1)
				numa_set_preferred(-1);
		}
	};

	class policy
	{
	private:
		struct cpumask_deleter
		{
			void operator() (bitmask * const mask) noexcept
				{ numa_free_cpumask(mask); }
		};

		using unique_cpu_mask = std::unique_ptr<bitmask, cpumask_deleter>;

		unique_cpu_mask mCpuMask;
		std::vector<unique_cpu_mask> mNodeMask;

	public:
		policy()
		{
			if(const auto m = numa_allocate_cpumask(); !m)
				throw Exception("Failed to allocate CPU mask.");
			else
				mCpuMask.reset(m);

			numa_sched_getaffinity(0, std::to_address(mCpuMask));

			const auto nbNodes = node_count();
			mNodeMask.reserve(nbNodes);

			for(const auto i : mir(nbNodes))
			{
				const auto mask = numa_allocate_cpumask();
				if(!mask)
					throw Exception("Failed to allocate CPU mask.");

				numa_node_to_cpus(i, mask);
				mNodeMask.emplace_back(mask);
			}
		}

		policy(const policy &o)
		{
			if(const auto m = numa_allocate_cpumask(); !m)
				throw Exception("Failed to allocate CPU mask.");
			else
				mCpuMask.reset(m);

			copy_bitmask_to_bitmask(std::to_address(o.mCpuMask), std::to_address(mCpuMask));

			mNodeMask.reserve(o.mNodeMask.size());
			for(const auto &obm : o.mNodeMask)
			{
				const auto mask = numa_allocate_cpumask();
				if(!mask)
					throw Exception("Failed to allocate CPU mask.");

				copy_bitmask_to_bitmask(std::to_address(obm), mask);
				mNodeMask.emplace_back(mask);
			}
		}

		policy(policy &&o) = default;

		policy &operator=(const policy &o)
		{
			return (*this = policy{o});
		}
		policy &operator=(policy &&o) = default;

		[[nodiscard]] auto cpu_mask() const noexcept { return std::to_address(mCpuMask); }
		[[nodiscard]] auto node_mask(int node) const noexcept { return std::to_address(mNodeMask[node]); }
		[[nodiscard]] auto node_cpu_count(int node) const noexcept { return numa_bitmask_weight(node_mask(node)); }
	};

	class MemoryResource : public std::pmr::memory_resource
	{
	public:
		MemoryResource(int n = 0, std::pmr::memory_resource &mr = *std::pmr::get_default_resource())
			: mMr{&mr}, mNode{n} {}

		void node(int n) noexcept { mNode = n; }
		[[nodiscard]] int node() const noexcept { return mNode; }

		void resource(std::pmr::memory_resource &mr) noexcept { mMr = &mr; }
		[[nodiscard]] auto resource() const noexcept { return mMr; }

	private:
		std::pmr::memory_resource *mMr;
		int mNode = -1;

		[[nodiscard]] virtual void *do_allocate(szt bytes, szt alignment) override
		{
			scoped_alloc_on_node ng{mNode};

			return assert_valid(mMr)->allocate(bytes, alignment);

			//std::cout << "NUMA allocation on node " << std::dec << mNode << ", size: " << bytes << ", align: " << alignment << '\n';
			/*const auto p = aligned_mem_allocate((void *)nullptr, bytes, alignment, nullptr, [this](auto, auto size){ return numa_alloc_onnode(size, mNode); });

			if(!p)
				throw std::bad_alloc();

			return p;*/
		}

		virtual void do_deallocate(void *p, szt bytes, szt alignment) override
		{
			return assert_valid(mMr)->deallocate(p, bytes, alignment);
			//std::cout << "NUMA deallocation on node " << std::dec << mNode << ", size: " << bytes << ", align: " << alignment << '\n';
			//aligned_mem_free(p, [bytes](auto data){ numa_free(static_cast<void *>(data), bytes); });
		}

		virtual bool do_is_equal(const std::pmr::memory_resource &o) const noexcept override
			{ return this == &o; }
	};
}
