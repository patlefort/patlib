/*
	PatLib

	Copyright (C) 2020 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "base.hpp"

#include <span>

#include <boost/hana/config.hpp>
#include <boost/hana/map.hpp>
#include <boost/hana/tuple.hpp>
#include <boost/hana/traits.hpp>

#include <range/v3/view/zip.hpp>
#include <range/v3/view/subrange.hpp>

namespace patlib
{
	namespace multi_tagged_span_impl
	{
		[[nodiscard]] constexpr auto type_pair_transform(auto p) noexcept
		{
			using T = typename decltype(+hn::second(p))::type *;
			return hn::make_pair(hn::first(p), T{});
		}

		[[nodiscard]] constexpr auto build_type_map(auto types) noexcept
		{
			return hn::unpack(types, [](auto... p){
				return hn::make_map( type_pair_transform(p)... );
			});
		}

		template <szt Extent_>
		struct extent
		{
			[[nodiscard]] constexpr auto size() const noexcept { return Extent_; }
		};

		template <>
		struct extent<std::dynamic_extent>
		{
			szt mNb{};

			[[nodiscard]] constexpr auto size() const noexcept { return mNb; }
		};
	} // namespace multi_tagged_span_impl

	template <typename Left_, typename Right_>
	using multi_tagged_span_pair = hn::pair<Left_, Right_>;

	namespace tag
	{
		struct multi_tagged_span_base {};
	}

	#define PL_MAKE_MULTITAGGEDSPAN_TAG(tagclass) PL_MAKE_DERIVED_HANA_TAG(tagclass, ::patlib::tag::multi_tagged_span_base)
	#define PL_MAKE_STATIC_MULTITAGGEDSPAN_TAG(tagclass) PL_MAKE_STATIC_DERIVED_HANA_TAG(tagclass, ::patlib::tag::multi_tagged_span_base)

	template <szt Extent_, typename... TypePairs_>
	class multi_tagged_span : private multi_tagged_span_impl::extent<Extent_>
	{
	private:
		static_assert(sizeof...(TypePairs_) > 0, "multi_tagged_span type pairs must not be empty.");

		template <szt, typename...>
		friend class multi_tagged_span;

		static constexpr auto TypePairs = hn::tuple<TypePairs_...>{};
		static constexpr auto TypePairsNonConst = hn::transform(TypePairs, [](auto p){
			return hn::make_pair( hn::first(p), hn::traits::remove_const(hn::second(p)) );
		});

		using type_map_type = decltype( multi_tagged_span_impl::build_type_map(TypePairs) );
		using type_map_nonconst_type = typename decltype( +[]{
			return hn::unpack(TypePairsNonConst, [](auto... pairs){
				return type_v<multi_tagged_span<Extent_, decltype(pairs)...>>;
			});
		}() )::type;

	public:

		static constinit const auto NbDimensions = sizeof...(TypePairs_);
		static constinit const bool IsConst = hn::all_of(hn::values(TypePairs), hn::traits::is_const);
		static constinit const auto extent = Extent_;
		static constinit const bool IsDynamic = extent == std::dynamic_extent;

		using size_type = szt;

	private:

		type_map_type mDataMap{};

	public:

		constexpr multi_tagged_span() = default;

		constexpr multi_tagged_span(size_type nb, decltype( multi_tagged_span_impl::type_pair_transform(TypePairs_{}) )... p)
				requires (IsDynamic) :
			multi_tagged_span_impl::extent<Extent_>{nb}, mDataMap{p...} {}

		constexpr multi_tagged_span(std::integral_constant<size_type, Extent_>, decltype( multi_tagged_span_impl::type_pair_transform(TypePairs_{}) )... p)
				requires (!IsDynamic) :
			mDataMap{p...} {}

		PL_DEFAULT_COPYMOVE(multi_tagged_span)

		constexpr multi_tagged_span(const type_map_nonconst_type &o)
				requires (!std::same_as<type_map_nonconst_type, multi_tagged_span<Extent_, TypePairs_...>>) :
			multi_tagged_span_impl::extent<Extent_>{ static_cast<const multi_tagged_span_impl::extent<Extent_> &>(o) },
			mDataMap{
				hn::make_pair(
					hn::first(TypePairs_{}),
					static_cast<decltype(mDataMap[hn::first(TypePairs_{})])>(o.mDataMap[hn::second(TypePairs_{})])
				)...
			} {}

		[[nodiscard]] constexpr auto keys() const noexcept { return hn::keys(mDataMap); }
		[[nodiscard]] constexpr auto values() const noexcept { return hn::values(mDataMap); }

		[[nodiscard]] constexpr auto nb_dimensions() const noexcept { return NbDimensions; }

		using multi_tagged_span_impl::extent<Extent_>::size;
		[[nodiscard]] constexpr bool empty() const noexcept { return !size(); }

		template <size_type count>
		[[nodiscard]] constexpr auto first() const noexcept
		{
			plassert(count <= size());

			return hn::unpack(mDataMap, [&](auto&&... p){
				return multi_tagged_span{std::integral_constant<size_type, count>{}, PL_FWD(p)...};
			});
		}

		[[nodiscard]] constexpr multi_tagged_span first(size_type count) const noexcept
		{
			plassert(count <= size());

			return hn::unpack(mDataMap, [&](auto&&... p){
				return multi_tagged_span{count, PL_FWD(p)...};
			});
		}

		template <size_type count>
		[[nodiscard]] constexpr auto last() const noexcept
		{
			plassert(count <= size());
			constexpr auto newNb = size() - count;

			return hn::unpack(mDataMap, [&](auto&&... p){
				return multi_tagged_span{std::integral_constant<size_type, newNb>{}, hn::make_pair(hn::first(p), hn::second(p) + newNb)...};
			});
		}

		[[nodiscard]] constexpr multi_tagged_span last(size_type count) const noexcept
		{
			plassert(count <= size());
			const auto newNb = size() - count;

			return hn::unpack(mDataMap, [&](auto&&... p){
				return multi_tagged_span{newNb, hn::make_pair(hn::first(p), hn::second(p) + newNb)...};
			});
		}

		template <size_type offset, size_type count>
		[[nodiscard]] constexpr auto subspan() const noexcept
		{
			plassert(offset + count <= size());

			return hn::unpack(mDataMap, [&](auto&&... p){
				return multi_tagged_span{std::integral_constant<size_type, count>{}, hn::make_pair(hn::first(p), hn::second(p) + offset)...};
			});
		}

		[[nodiscard]] constexpr multi_tagged_span subspan(size_type offset, size_type count) const noexcept
		{
			plassert(offset + count <= size());

			return hn::unpack(mDataMap, [&](auto&&... p){
				return multi_tagged_span{count, hn::make_pair(hn::first(p), hn::second(p) + offset)...};
			});
		}

		template <typename Tag_>
		[[nodiscard]] constexpr auto operator[](type_c<Tag_> tag) const noexcept
			{ return std::span{data(tag), size()}; }

		template <szt I_>
		[[nodiscard]] constexpr auto operator[](size_c<I_> index) const noexcept
			{ return hn::values(mDataMap)[index]; }

		template <typename Tag_>
		[[nodiscard]] constexpr const auto &data(type_c<Tag_> tag) const noexcept
			{ return mDataMap[tag]; }

		template <typename Tag_>
		[[nodiscard]] constexpr auto &data(type_c<Tag_> tag) noexcept
			{ return mDataMap[tag]; }

		template <szt I_>
		[[nodiscard]] constexpr const auto &data(size_c<I_> index) const noexcept
			{ return hn::values(mDataMap)[index]; }

		template <szt I_>
		[[nodiscard]] constexpr auto &data(size_c<I_> index) noexcept
			{ return hn::values(mDataMap)[index]; }

		template <typename Tag_>
		[[nodiscard]] constexpr auto range(type_c<Tag_> tag) const noexcept
			{ return rg::make_subrange(data(tag), data(tag) + size()); }

		template <szt I_>
		[[nodiscard]] constexpr auto range(size_c<I_> index) const noexcept
			{ return rg::make_subrange(data(index), data(index) + size()); }

		[[nodiscard]] constexpr auto range() const noexcept
		{
			return hn::unpack(mDataMap, [&](auto&&... p){
				return rgv::zip(range(hn::first(p))...);
			});
		}

		[[nodiscard]] friend constexpr auto tag_invoke(tag_t<as_const_view>, const multi_tagged_span &v) noexcept
			{ return multi_tagged_span<Extent_, const TypePairs_...>{v}; }

		template <typename T_>
		[[nodiscard]] friend constexpr auto &get(multi_tagged_span &sp) noexcept { return sp[type_v<T_>]; }
		template <typename T_>
		[[nodiscard]] friend constexpr const auto &get(const multi_tagged_span &sp) noexcept { return sp[type_v<T_>]; }
		template <typename T_>
		[[nodiscard]] friend constexpr auto&& get(multi_tagged_span &&sp) noexcept { return std::move(sp[type_v<T_>]); }

		template <szt I_>
		[[nodiscard]] friend constexpr auto &get(multi_tagged_span &sp) noexcept { return sp[size_v<I_>]; }
		template <szt I_>
		[[nodiscard]] friend constexpr const auto &get(const multi_tagged_span &sp) noexcept { return sp[size_v<I_>]; }
		template <szt I_>
		[[nodiscard]] friend constexpr auto&& get(multi_tagged_span &&sp) noexcept { return std::move(sp[size_v<I_>]); }
	};

	template <typename... TypePairs_>
	using dynamic_multi_tagged_span = multi_tagged_span<std::dynamic_extent, TypePairs_...>;
}
