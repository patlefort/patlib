/*
	PatLib

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

namespace patlib
{
	enum class EnumThreadPriority
	{
		Low, Normal, High
	};

	enum class ThreadEvent
	{
		Start = 0,
		End,
		Pause,
		PauseEnd,
		Restart
	};

	enum class EnumWorkUnitState
	{
		Done = 0,
		Keep,
		Discard
	};

	enum class ThreadRequest
	{
		Pause = 1,
		Unpause,
		End,
		Restart
	};

	class IDevice;
	class IThreadedDevice;

	template <typename Alloc_>
	class call_queue;

	template <typename RequestType_>
	class request_token;

	class ThreadBase;

	template <typename ThreadType_>
	class ThreadDelegate;

	template <typename ThreadType_>
	class ThreadDelegateWithin;

	class ThreadLambda;

	template <typename ThreadClass_>
	class ThreadPool;

	template <typename ThreadClass_>
	class ThreadWithPool;

	template <typename WorkUnit_>
	class work_unit;

	template <typename WorkUnit_>
	class work_unit_list;

	template <typename WorkUnit_>
	class back_buffered_work_units;

	template <typename Worker_, typename WorkUnit_, int ListSize_>
	class ThreadWorker;

	template <typename Master_, typename Worker_, typename WorkUnit_, int ListSize_>
	class ThreadMaster;
}
