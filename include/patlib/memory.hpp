/*
	PatLib

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "base.hpp"
#include "type.hpp"
#include "utility.hpp"
#include "crtp.hpp"
#include "marked_optional.hpp"

#include <memory_resource>
#include <memory>
#include <type_traits>
#include <atomic>
#include <functional>
#include <limits>
#include <utility>
#include <cstddef>
#include <algorithm>
#include <iterator>
#include <experimental/type_traits>
#include <cstring>
#include <any>
#include <list>

#include <range/v3/range/concepts.hpp>
#include <range/v3/range/traits.hpp>
#include <range/v3/algorithm/find.hpp>
#include <range/v3/algorithm/find_if.hpp>

#include <boost/container/pmr/memory_resource.hpp>

#ifdef _WIN32
	#include <sysinfoapi.h>
#else
	#include <unistd.h>
#endif

#ifdef __cpp_constexpr_dynamic_alloc
	#define PL_CONSTEXPR_IFDYNALLOC constexpr
#else
	#define PL_CONSTEXPR_IFDYNALLOC
#endif

#ifdef __cpp_lib_constexpr_dynamic_alloc
	#define PL_CONSTEXPR_IFLIBDYNALLOC constexpr
#else
	#define PL_CONSTEXPR_IFLIBDYNALLOC
#endif

namespace patlib
{
	inline constexpr auto container_memory_capacity = cpo_entry_with_defaults
	{
		properties_t{p_nodiscard},

		[]<typename T_> (const T_ &cont) noexcept -> szt
			{ return cont.capacity() * sizeof(rg::range_value_t<T_>); }
	};

	inline constexpr auto container_memory_size = cpo_entry_with_defaults
	{
		properties_t{p_nodiscard},

		[]<rg::contiguous_range T_> (const T_ &cont) noexcept -> szt
			{ return rg::size(cont) * sizeof(rg::range_value_t<T_>); }
	};

	inline constexpr auto append_copy = cpo_entry_with_defaults
	{
		properties<>,

		[](const auto &cont, auto &into) noexcept -> auto &
		{
			into.insert(into.end(), cont.begin(), cont.end());
			//std::copy(cont.begin(), cont.end(), std::back_inserter(into));
			return into;
		}
	};

	inline constexpr auto append_move = cpo_entry_with_defaults
	{
		properties<>,

		[](auto &cont, auto &into) noexcept -> auto &
		{
			into.insert(into.end(), std::make_move_iterator(cont.begin()), std::make_move_iterator(cont.end()));
			cont.clear();
			//std::move(cont.begin(), cont.end(), std::back_inserter(into));
			return into;
		}
	};


	template <typename T_, typename SizeT_>
	concept CReservable = requires(T_ c, SizeT_ s) { c.reserve(s); };
	template <typename T_>
	concept CContainerHasData =
		requires { typename std::decay_t<T_>::value_type; } &&
		requires(T_ c) { {c.data()} -> std::same_as<add_const_if<typename std::decay_t<T_>::value_type, std::is_const_v<T_>> *>; };
	template <typename T_>
	concept CContainerHasConstData =
		requires { typename std::decay_t<T_>::value_type; } &&
		requires(const T_ c) { {c.data()} -> std::same_as<const typename std::decay_t<T_>::value_type *>; };
	template <typename T_>
	concept CContainerHasAssign = requires(T_ c, typename std::decay_t<T_>::size_type nb, typename std::decay_t<T_>::value_type v) { c.assign(nb, v); };
	template <typename T_>
	concept CContainerHasFill = requires(T_ c, typename std::decay_t<T_>::value_type v) { c.fill(v); };
	template <typename T_, typename KeyT_>
	concept CContainerHasContains = requires(const T_ c, const KeyT_ v) { { c.contains(v) } -> std::same_as<bool>; };

	template <typename Container_>
	[[nodiscard]] constexpr auto range_data(Container_&& container) noexcept
	{
		if constexpr(rg::contiguous_range<Container_>)
			return rg::data(container);
		else if constexpr(std::is_pointer_v<std::decay_t<Container_>>)
			return container;
		else
			return std::data(PL_FWD(container));
	}

	template <std::size_t Len_, std::size_t Align_ = alignof(std::max_align_t)>
	struct aligned_storage
	{
		alignas(Align_) std::array<std::byte, sizeof(Len_)> data;
	};

	template <typename Container_>
	[[nodiscard]] constexpr auto range_data(Container_ *container) noexcept
		{ return container; }

	[[nodiscard]] constexpr bool contains_key(const auto &container, const auto &v)
	{
		if constexpr(CContainerHasContains<decltype(container), decltype(v)>)
			return PL_FWD(container).contains(v);
		else
			return v < rg::size(container);
	}

	template <typename T_, typename ST_>
	constexpr void try_reserve(T_ &, ST_) {}

	template <typename T_, typename ST_>
	constexpr void try_reserve(T_ &container, ST_ size)
		requires CReservable<T_, ST_>
	{ container.reserve(size); }

	template <typename T_>
	constexpr decltype(auto) concatenate(T_ &&contInto, auto&&... contSource)
	{
		const auto totalSize = ( contInto.size() + ... + rg::size(PL_FWD(contSource)) );

		try_reserve(contInto, totalSize);

		( contInto.insert(std::begin(PL_FWD(contSource)), std::end(PL_FWD(contSource))), ... );

		return PL_FWD_RETURN(contInto);
	}

	constexpr void pushback_blocked(auto &cont, szt blockSize, auto&& v)
	{
		if(cont.size() == cont.capacity())
			cont.reserve(cont.size() + blockSize);

		cont.push_back(PL_FWD(v));
	}

	constexpr void emplaceback_blocked(auto &cont, szt blockSize, auto&&... args)
	{
		if(cont.size() == cont.capacity())
			cont.reserve(cont.size() + blockSize);

		cont.emplace_back(PL_FWD(args)...);
	}

	constexpr auto &assign_copy(auto &container, auto&&... args)
	{
		container = std::remove_reference_t<decltype(container)>{ PL_FWD(args)... };
		return container;
	}

	#ifdef _WIN32

		[[nodiscard]] inline szt total_system_memory() noexcept
		{
			MEMORYSTATUSEX status;
			status.dwLength = sizeof(status);
			GlobalMemoryStatusEx(&status);

			return status.ullTotalPhys;
		}

	#else

		[[nodiscard]] inline szt total_system_memory() noexcept
			{ return sysconf(_SC_PHYS_PAGES) * sysconf(_SC_PAGE_SIZE); }

	#endif

	template <typename ToType_, typename PtrType_>
	[[nodiscard]] constexpr auto pointer_recast(PtrType_ p) noexcept
	{
		using VoidPtr = typename std::pointer_traits<PtrType_>::template rebind<void>;
		using ToTypePtr = typename std::pointer_traits<PtrType_>::template rebind<ToType_>;
		return static_cast<ToTypePtr>(static_cast<VoidPtr>(p));
	}

	template <typename ToType_, typename PtrType_>
	[[nodiscard]] constexpr auto pointer_static_cast(PtrType_ p) noexcept
	{
		using ToTypePtr = typename std::pointer_traits<PtrType_>::template rebind<ToType_>;
		return static_cast<ToTypePtr>(p);
	}

	template <typename ToType_, typename PtrType_>
	[[nodiscard]] constexpr auto pointer_dynamic_cast(PtrType_ p)
	{
		using ToTypePtr = typename std::pointer_traits<PtrType_>::template rebind<ToType_>;
		return dynamic_cast<ToTypePtr>(p);
	}

	[[nodiscard]] inline std::uintptr_t aligned_get_align_offset(void *p, std::uintptr_t align) noexcept
		{ return reinterpret_cast<std::uintptr_t>(std::to_address(p)) & (align - 1); }

	template <typename PtrType_>
	[[nodiscard]] constexpr auto aligned_get_mem_pointer(PtrType_ data) noexcept
		{ return *(pointer_recast<PtrType_>(data)-1); }

	template <typename PtrType_>
	constexpr void aligned_set_mem_pointer(PtrType_ data, PtrType_ base) noexcept
		{ *(pointer_recast<PtrType_>(data)-1) = base; }

	template <typename PtrType_>
	void aligned_mem_free(PtrType_ data, auto&& freeFc)
	{
		if(data)
			std::invoke(PL_FWD(freeFc), aligned_get_mem_pointer(data));
	}

	template <typename PtrType_>
	[[nodiscard]] PtrType_ aligned_mem_allocate(PtrType_ data, szt size, szt align, szt *allocSize, auto&& allocFc)
	{
		using BytePtrType = typename std::pointer_traits<PtrType_>::template rebind<std::byte>;

		BytePtrType ptr, res{}, oldptr{};

		const auto realAlign = std::max(align, alignof(PtrType_));

		if(data)
			oldptr = aligned_get_mem_pointer(pointer_recast<std::byte>(data));

		const auto ptrPad = std::max(sizeof(PtrType_), alignof(PtrType_));
		const auto totalSize = size + ptrPad + alignof(PtrType_) + realAlign;

		ptr = pointer_recast<std::byte>(allocFc(oldptr, totalSize));

		if(ptr)
		{
			const auto ptrAlignOffset = aligned_get_align_offset(static_cast<void *>(std::to_address(ptr)), alignof(PtrType_));
			const bool ptrAligned = !ptrAlignOffset;
			const auto start = ptr + ptrPad + (ptrAligned ? std::uintptr_t{} : std::uintptr_t(alignof(PtrType_) - ptrAlignOffset));
			const auto alignOffset = aligned_get_align_offset(static_cast<void *>(std::to_address(start)), realAlign);
			const bool aligned = !alignOffset;

			res = start + (aligned ? std::uintptr_t{} : std::uintptr_t(realAlign - alignOffset));

			//std::cout << std::to_address(res) << " | " << std::to_address(ptr) << '\n';
			plassert(res + size <= ptr + totalSize);
			plassert((std::uintptr_t)std::to_address(res) % align == 0);

			aligned_set_mem_pointer(res, ptr);

			if(oldptr)
				std::memmove(std::to_address(res), std::to_address(pointer_recast<std::byte>(data)), size);
		}

		if(allocSize)
			*allocSize = totalSize;

		return pointer_recast<typename std::pointer_traits<PtrType_>::element_type>(res);
	}

	template <rg::bidirectional_iterator DestIt_, typename Alloc_, typename Proj_ = std::identity>
	auto uninitialized_copy_alloc(rg::input_range auto&& r, DestIt_ dest, Alloc_ &alloc, auto&& proj = {})
	{
		auto it = std::begin(r);
		auto current = dest;

		try
		{
			for(auto end = std::end(r); it != end; ++it, ++current)
				std::uninitialized_construct_using_allocator(std::addressof(*current), alloc, std::invoke(proj, *it));

			return current;
		}
		catch(...)
		{
			for(; current != dest; ++dest)
				std::allocator_traits<Alloc_>::destroy(alloc, std::addressof(*dest));

			throw;
		}
	}

	template <szt Size_, szt Align_ = alignof(std::max_align_t)>
	struct unitialized_storage
	{
		inline static constexpr szt Size = Size_;
		inline static constexpr szt Align = Align_;

		alignas(Align_) std::byte data[Size_];

		[[nodiscard]] void *to_void() noexcept { return reinterpret_cast<void *>(data); }
		[[nodiscard]] const void *to_void() const noexcept { return reinterpret_cast<void *>(data); }

		[[nodiscard]] constexpr szt size() const noexcept { return Size; }
	};


	// Default memory resource that respect alignment
	class MemoryResource : public std::pmr::memory_resource
	{
	private:
		[[nodiscard]] virtual void *do_allocate(szt bytes, szt alignment) override
			{ return operator new(bytes, std::align_val_t(alignment)); }

		virtual void do_deallocate(void *p, szt /*bytes*/, szt alignment) override
			{ operator delete(p, std::align_val_t(alignment)); }

		virtual bool do_is_equal(const std::pmr::memory_resource &o) const noexcept override
			{ return this == &o; }
	};

	template <typename MemoryResourceBack_ = boost::container::pmr::memory_resource, typename MemoryResourceFront_ = std::pmr::memory_resource>
	class MemoryResourceAdaptor : public MemoryResourceFront_
	{
	public:
		MemoryResourceAdaptor() = default;
		MemoryResourceAdaptor(MemoryResourceBack_ &mr)
			: mMr{&mr} {}

		void setResource(MemoryResourceBack_ &mr) noexcept { mMr = &mr; }
		[[nodiscard]] MemoryResourceBack_ *getResource() const noexcept { return mMr; }

	private:

		MemoryResourceBack_ *mMr{};

		[[nodiscard]] virtual void *do_allocate(szt bytes, szt alignment) override
			{ return assert_valid(mMr)->allocate(bytes, alignment); }

		virtual void do_deallocate(void *p, szt bytes, szt alignment) override
			{ assert_valid(mMr)->deallocate(p, bytes, alignment); }

		virtual bool do_is_equal(const MemoryResourceFront_ &o) const noexcept override
			{ return this == &o; }
	};

	class MemoryResourceAligned : public std::pmr::memory_resource
	{
	public:

		MemoryResourceAligned(szt a = 0) : mAlign{a} {}

		[[nodiscard]] auto getAlign() const noexcept { return mAlign; }
		void setAlign(szt a) noexcept { mAlign = a; }

	protected:
		szt mAlign = 0;

		[[nodiscard]] virtual void *do_allocate(szt bytes, szt alignment) override
			{ return operator new(bytes, std::align_val_t(std::max(mAlign, alignment))); }

		virtual void do_deallocate(void *p, szt /*bytes*/, szt alignment) override
			{ operator delete(p, std::align_val_t(std::max(mAlign, alignment))); }

		virtual bool do_is_equal(const std::pmr::memory_resource &o) const noexcept override
			{ return this == &o; }
	};

	#ifdef PL_HUGEPAGES
		#define PL_LIB_HUGEPAGES

		class MemoryResourceHugePages : public std::pmr::memory_resource
		{
		public:

			MemoryResourceHugePages();

		private:

			szt mHPAlign;

			[[nodiscard]] virtual void *do_allocate(szt bytes, szt alignment) override;
			virtual void do_deallocate(void *p, szt bytes, szt alignment) override;
			virtual bool do_is_equal(const std::pmr::memory_resource &o) const noexcept override;
		};

		PL_CONST_FC( std::pmr::memory_resource PATLIB_API &hugepages_resource() );
	#endif

	// Global default memory resource
	PL_CONST_FC( std::pmr::memory_resource PATLIB_API &default_resource() );


	template <typename PointerType_>
	class IMemoryResource
	{
	public:
		virtual ~IMemoryResource() = default;

		using pointer_type = PointerType_;

		[[nodiscard]] auto allocate(szt bytes, szt alignment = alignof(std::max_align_t))
		{
			return do_allocate(bytes, alignment);
		}

		void deallocate(PointerType_ p, szt bytes, szt alignment = alignof(std::max_align_t))
		{
			do_deallocate(p, bytes, alignment);
		}

		[[nodiscard]] bool operator== (const IMemoryResource &other) const noexcept
			{ return this == &other || do_is_equal(other); }

	private:

		[[nodiscard]] virtual pointer_type do_allocate(szt bytes, szt alignment) = 0;
		virtual void do_deallocate(pointer_type p, szt bytes, szt alignment) = 0;
		virtual bool do_is_equal(const IMemoryResource &other) const noexcept = 0;
	};

	template <typename T_>
	concept CHasUpstreamAllocator = requires { typename T_::upstream_allocator_type; };

	namespace alloc_impl
	{
		template <typename Alloc_>
		struct upstreammost_allocator
		{
			using type = Alloc_;
		};

		template <CHasUpstreamAllocator Alloc_>
		struct upstreammost_allocator<Alloc_>
		{
			using type = typename upstreammost_allocator<typename Alloc_::upstream_allocator_type>::type;
		};
	} // namespace alloc_impl

	template <typename Alloc_>
	using upstreammost_allocator_t = typename alloc_impl::upstreammost_allocator<Alloc_>::type;

	template <typename T_>
	constexpr szt upstream_allocator_count = 0;
	template <CHasUpstreamAllocator T_>
	constexpr szt upstream_allocator_count<T_> = upstream_allocator_count<typename T_::upstream_allocator_type> + 1;

	template <typename T_, szt I_>
	[[nodiscard]] auto make_allocator(type_c<T_>, std::in_place_index_t<I_>, auto&&... args)
	{
		if constexpr(I_ == 0)
			return T_{PL_FWD(args)...};
		else
			return T_{std::in_place_index<I_ - 1>, PL_FWD(args)...};
	}

	template <typename T_, szt I_>
	[[nodiscard]] auto get_allocator(const T_ &alloc, std::in_place_index_t<I_>)
	{
		if constexpr(I_ == 0)
			return alloc;
		else
			return get_allocator(alloc.upstream_allocator(), std::in_place_index<I_ - 1>);
	}

	template <template<typename, typename...> typename Derived_, typename T_, typename UpstreamAllocator_, typename... DerTs_>
	class upstream_allocator_base : public crtp_base<Derived_<T_, DerTs_...>>
	{
		using crtp_base<Derived_<T_, DerTs_...>>::derived;

	public:
		template <template<typename, typename...> typename, typename, typename, typename...>
		friend class upstream_allocator_base;

		using upstream_allocator_type = UpstreamAllocator_;
		using upstream_alloc_traits = std::allocator_traits<upstream_allocator_type>;

		using value_type = T_;
		using pointer = typename std::pointer_traits<typename upstream_alloc_traits::pointer>::template rebind<T_>;
		using const_pointer = typename std::pointer_traits<typename upstream_alloc_traits::const_pointer>::template rebind<const T_>;
		using void_pointer = typename upstream_alloc_traits::void_pointer;
		using const_void_pointer = typename upstream_alloc_traits::const_void_pointer;
		using size_type = typename upstream_alloc_traits::size_type;
		using difference_type = typename upstream_alloc_traits::difference_type;

		using is_always_equal = typename upstream_alloc_traits::is_always_equal;
		using propagate_on_container_move_assignment = typename upstream_alloc_traits::propagate_on_container_move_assignment;
		using propagate_on_container_copy_assignment = typename upstream_alloc_traits::propagate_on_container_copy_assignment;
		using propagate_on_container_swap = typename upstream_alloc_traits::propagate_on_container_swap;

		[[nodiscard]] constexpr auto max_size() const noexcept { return std::allocator_traits<upstream_allocator_type>::max_size(mUpstreamAlloc); }

		constexpr upstream_allocator_base() = default;

		template <szt I_>
			requires (I_ == 0)
		constexpr upstream_allocator_base(std::in_place_index_t<I_>, auto&&... args) :
			mUpstreamAlloc(PL_FWD(args)...) {}

		template <szt I_>
			requires (I_ > 0)
		constexpr upstream_allocator_base(std::in_place_index_t<I_>, auto&&... args) :
			mUpstreamAlloc(std::in_place_index<I_ - 1>, PL_FWD(args)...) {}

		constexpr upstream_allocator_base(std::in_place_t, auto&&... args) :
			mUpstreamAlloc(PL_FWD(args)...) {}

		explicit constexpr upstream_allocator_base(const upstream_allocator_type &upa) noexcept :
			mUpstreamAlloc{upa} {}

		explicit constexpr upstream_allocator_base(const upstreammost_allocator_t<upstream_allocator_type> &a)
			requires (!std::same_as<upstream_allocator_type, upstreammost_allocator_t<upstream_allocator_type>>)
			: mUpstreamAlloc{a} {}

		template <typename OT_, typename... TA_>
		constexpr upstream_allocator_base(const Derived_<OT_, TA_...> &o) :
			mUpstreamAlloc{o.mUpstreamAlloc} {}

		template <typename OT_, typename... TA_>
		constexpr upstream_allocator_base(Derived_<OT_, TA_...> &&o) noexcept(CNoThrowMoveContructible<upstream_allocator_type>) :
			mUpstreamAlloc{std::move(o.mUpstreamAlloc)} {}

		[[nodiscard]] constexpr bool operator==(const upstream_allocator_base &o) const noexcept
		{
			if constexpr(is_always_equal::value)
				return true;
			else
				return mUpstreamAlloc == o.mUpstreamAlloc;
		}

		template <typename... Args_>
		constexpr void construct(auto p, Args_&&... args)
			{ std::uninitialized_construct_using_allocator(std::to_address(p), mUpstreamAlloc, PL_FWD(args)...); }

		constexpr void destroy(auto p) noexcept
			{ std::allocator_traits<upstream_allocator_type>::destroy(mUpstreamAlloc, std::to_address(p)); }

		[[nodiscard]] constexpr auto allocate(const size_type n)
			{ return mUpstreamAlloc.allocate(n); }

		constexpr void deallocate(pointer p, const size_type n)
			{ mUpstreamAlloc.deallocate(p, n); }

		[[nodiscard]] constexpr auto upstream_allocator() const noexcept { return mUpstreamAlloc; }

		[[nodiscard]] constexpr auto copy_construct_select() const noexcept
			{ return std::allocator_traits<upstream_allocator_type>::select_on_container_copy_construction(mUpstreamAlloc); }

	private:
		[[no_unique_address]] upstream_allocator_type mUpstreamAlloc;
	};

	template <typename T_, typename UpstreamAllocator_ = std::allocator<T_>>
	class aligned_allocator : public upstream_allocator_base<aligned_allocator, T_, typename std::allocator_traits<UpstreamAllocator_>::template rebind_alloc<std::byte>, UpstreamAllocator_>
	{
		using upstream_base_type = upstream_allocator_base<aligned_allocator, T_, typename std::allocator_traits<UpstreamAllocator_>::template rebind_alloc<std::byte>, UpstreamAllocator_>;

		PL_CRTP_DERIVED(upstream_base_type)

	public:

		template <typename, typename>
		friend class aligned_allocator;

		szt mAlignment = alignof(T_);

		using typename upstream_base_type::upstream_allocator_type;

		using typename upstream_base_type::value_type;
		using typename upstream_base_type::size_type;
		using typename upstream_base_type::pointer;
		using BytePtrType = typename std::allocator_traits<upstream_allocator_type>::pointer;

		template <typename OT_>
		struct rebind
		{
			using other = aligned_allocator<OT_, typename std::allocator_traits<UpstreamAllocator_>::template rebind_alloc<OT_>>;
		};

		[[nodiscard]] constexpr auto select_on_container_copy_construction() const
			{ return aligned_allocator{this->copy_construct_select(), mAlignment}; }

		explicit constexpr aligned_allocator(const upstream_allocator_type &a = {}, szt align = alignof(T_)) :
			upstream_base_type{a}, mAlignment{align} {}

		explicit constexpr aligned_allocator(const upstreammost_allocator_t<upstream_allocator_type> &a, szt align = alignof(T_))
			requires (!std::same_as<upstream_allocator_type, upstreammost_allocator_t<upstream_allocator_type>>)
			: upstream_base_type{a}, mAlignment{align} {}

		template <typename... TA_>
		constexpr aligned_allocator(const aligned_allocator<TA_...> &o) :
			upstream_base_type{o}, mAlignment{o.mAlignment} {}

		template <typename... TA_>
		constexpr aligned_allocator(aligned_allocator<TA_...> &&o) noexcept(CNoThrowMoveContructible<upstream_base_type>) :
			upstream_base_type{std::move(o)}, mAlignment{o.mAlignment} {}

		[[nodiscard]] constexpr bool operator==(const aligned_allocator &o) const
			{ return mAlignment == o.mAlignment && base() == o; }

		[[nodiscard]] constexpr auto allocate(const size_type n)
		{
			const auto pv = aligned_mem_allocate(BytePtrType{}, n * sizeof(T_), mAlignment, nullptr, [&](auto, auto size){ return base().allocate(size); });

			if(pv == nullptr)
				throw std::bad_alloc();

			return pointer_recast<value_type>(pv);
		}

		constexpr void deallocate(pointer p, const size_type n)
		{
			aligned_mem_free(pointer_recast<std::byte>(p),
				[&](auto data){ base().deallocate(data, n); }
			);
		}
	};


	template <typename T_, typename PointerType_>
	class pmr_allocator
	{
	public:

		using value_type = T_;
		using size_type = szt;
		using difference_type = typename std::pointer_traits<PointerType_>::difference_type;
		using pointer = PointerType_;
		using const_pointer = typename std::pointer_traits<PointerType_>::template rebind<const value_type>;
		using void_pointer = typename std::pointer_traits<PointerType_>::template rebind<void>;

		using offset_memory_resource_type = IMemoryResource<void_pointer>;

		using is_always_equal = std::false_type;
		using propagate_on_container_move_assignment = std::true_type;
		using propagate_on_container_copy_assignment = std::true_type;
		using propagate_on_container_swap = std::true_type;

		constexpr pmr_allocator(offset_memory_resource_type *mr = nullptr)
			: mMemoryResource(mr) {}

		template <typename... TA_>
		constexpr pmr_allocator(const pmr_allocator<TA_...> &o)
			: mMemoryResource(o.resource()) {}

		template <typename OT_>
		struct rebind
		{
			using other = pmr_allocator<OT_, typename std::pointer_traits<PointerType_>::template rebind<OT_>>;
		};

		[[nodiscard]] constexpr bool operator==(const pmr_allocator &other) const
		{
			return mMemoryResource == other.mMemoryResource;
		}

		[[nodiscard]] constexpr auto allocate_bytes(size_type nbytes, size_type alignment = alignof(std::max_align_t))
		{
			return assert_valid(mMemoryResource)->allocate(nbytes, alignment);
		}

		constexpr void deallocate_bytes(void_pointer p, size_type nbytes, size_type alignment = alignof(std::max_align_t))
		{
			assert_valid(mMemoryResource)->deallocate(p, nbytes, alignment);
		}

		template<typename OT_>
		[[nodiscard]] constexpr auto allocate_object(size_type n = 1)
		{
			using PT = typename std::pointer_traits<PointerType_>::template rebind<OT_>;

			if(std::numeric_limits<szt>::max() / sizeof(OT_) < n)
				throw std::length_error("Size too big for allocating objects.");

			return static_cast<PT>( allocate_bytes(n * sizeof(OT_), alignof(OT_)) );
		}

		template<typename OT_>
		constexpr void deallocate_object(typename std::pointer_traits<PointerType_>::template rebind<T_> p, size_type n = 1)
		{
			deallocate_bytes(static_cast<void_pointer>(p), n * sizeof(OT_), alignof(OT_));
		}

		template<typename OT_, typename... CtorArgs_>
		[[nodiscard]] constexpr auto new_object(CtorArgs_&&... ctor_args)
		{
			auto p = allocate_object<OT_>();

			try {
				std::allocator_traits<std::allocator<OT_>>::construct(std::allocator<OT_>{}, std::to_address(p), PL_FWD(ctor_args)...);
			} catch (...) {
				deallocate_object(p);
				throw;
			}

			return p;
		}

		template<typename OT_>
		constexpr void delete_object(typename std::pointer_traits<PointerType_>::template rebind<OT_> p)
		{
			std::allocator_traits<std::allocator<OT_>>::destroy(std::allocator<OT_>{}, std::to_address(p));
			deallocate_object(p);
		}

		[[nodiscard]] constexpr auto allocate(const size_type n)
		{
			return static_cast<pointer>( allocate_bytes(n * sizeof(value_type), alignof(value_type)) );
		}

		constexpr void deallocate(pointer p, const size_type) noexcept
		{
			deallocate_bytes(static_cast<void_pointer>(p), sizeof(value_type), alignof(value_type));
		}

		[[nodiscard]] constexpr auto *resource() const noexcept { return mMemoryResource; }

	private:
		offset_memory_resource_type *mMemoryResource = nullptr;
	};


	class MemoryResourceTypeErased : public std::pmr::memory_resource
	{
	public:

		template <typename T_>
		MemoryResourceTypeErased(std::in_place_t, T_&& o)
			: mTeAlloc{}, mMr{static_cast<std::pmr::memory_resource *>(&mTeAlloc.emplace<T_>(PL_FWD(o)))} {}
		MemoryResourceTypeErased(std::pmr::memory_resource *mr = std::pmr::get_default_resource())
			: mMr{mr} {}
		MemoryResourceTypeErased(const MemoryResourceTypeErased &o)
			: mTeAlloc{o.mTeAlloc}, mMr{mTeAlloc.has_value() ? std::any_cast<std::pmr::memory_resource *>(mTeAlloc) : o.mMr} {}
		MemoryResourceTypeErased(MemoryResourceTypeErased &&o) = default;

		MemoryResourceTypeErased &operator=(const MemoryResourceTypeErased &o)
		{
			mTeAlloc = o.mTeAlloc;
			mMr = mTeAlloc.has_value() ? std::any_cast<std::pmr::memory_resource *>(mTeAlloc) : o.mMr;
			return *this;
		}
		MemoryResourceTypeErased &operator=(MemoryResourceTypeErased &&o) = default;

		MemoryResourceTypeErased &operator=(std::pmr::memory_resource *mr)
		{
			mMr = mr;
			mTeAlloc.reset();
			return *this;
		}

	private:
		std::any mTeAlloc;
		std::pmr::memory_resource *mMr{};

		[[nodiscard]] virtual void *do_allocate(szt bytes, szt alignment) override
		{
			return assert_valid(mMr)->allocate(bytes, alignment);
		}

		virtual void do_deallocate(void *p, szt bytes, szt alignment) override
		{
			assert_valid(mMr)->deallocate(p, bytes, alignment);
		}

		virtual bool do_is_equal(const std::pmr::memory_resource &other) const noexcept override
		{
			return this == &other;
		}
	};


	class MonotonicBufferResource : public std::pmr::memory_resource
	{
	private:

		struct Block
		{
			std::byte *data;
			szt size, align;
		};

		std::pmr::memory_resource *mUpstreamMemRes = std::pmr::get_default_resource();
		std::list<Block> mBlocks;
		std::list<Block>::iterator mCurrentBlock;
		std::byte *mPtr = nullptr, *mLast = nullptr;
		szt mBlockSize = 1024 * 1024 * 16, mBlockAlign = alignof(std::max_align_t);
		bool mLifo = false;

		[[nodiscard]] std::byte *allocateBlock(szt n, szt alignment)
		{
			return static_cast<std::byte *>(mUpstreamMemRes->allocate(n, alignment));
		}

		void createNormalBlock(szt alignment)
		{
			alignment = std::max(alignment, mBlockAlign);

			auto b = allocateBlock(getBlockSize(), alignment);

			try
			{
				mBlocks.push_back({b, getBlockSize(), alignment});
			}
			catch(...)
			{
				mUpstreamMemRes->deallocate(b, getBlockSize(), alignment);
				throw;
			}

			mPtr = b;
			mLast = b + getBlockSize();
		}

		[[nodiscard]] std::byte *createLargeBlock(szt n, szt alignment)
		{
			alignment = std::max(alignment, mBlockAlign);

			auto b = allocateBlock(n, alignment);

			try
			{
				mBlocks.push_front({b, n, alignment});
			}
			catch(...)
			{
				mUpstreamMemRes->deallocate(b, n, alignment);
				throw;
			}

			return b;
		}

		[[nodiscard]] std::byte *getPtr(szt n, szt alignment) noexcept
		{
			if(mPtr >= mLast)
				return nullptr;

			szt sz = mLast - mPtr;
			void *ptr2 = static_cast<void *>(mPtr);

			return static_cast<std::byte *>(std::align(alignment, n, ptr2, sz));
		}

		void nextBlock(szt alignment) noexcept
		{
			auto cur = mCurrentBlock;

			if(++mCurrentBlock == mBlocks.end())
			{
				createNormalBlock(alignment);
				mCurrentBlock = std::next(cur);
			}
			else
			{
				mPtr = mCurrentBlock->data;
				mLast = mPtr + mCurrentBlock->size;
			}
		}

	public:

		MonotonicBufferResource(std::pmr::memory_resource &mr = *std::pmr::get_default_resource())
			: mUpstreamMemRes{&mr} {}
		MonotonicBufferResource(szt bz, szt align = alignof(std::max_align_t), std::pmr::memory_resource &mr = *std::pmr::get_default_resource(), bool lifo = false)
			{ setup(bz, align, mr, lifo); }

		~MonotonicBufferResource()
			{ free(); }

		void setup(szt bz, szt align = alignof(std::max_align_t), std::pmr::memory_resource &mr = *std::pmr::get_default_resource(), bool lifo = false)
		{
			free();
			setBlockSize(bz);
			setBlockAlign(align);
			setMemoryResource(mr);
			setLIFO(lifo);
		}

		void setBlockSize(szt n) noexcept { mBlockSize = n; }
		[[nodiscard]] szt getBlockSize() const noexcept { return mBlockSize; }

		void setBlockAlign(szt n) noexcept { mBlockAlign = n; }
		[[nodiscard]] szt getBlockAlign() const noexcept { return mBlockAlign; }

		void setLIFO(bool v) noexcept { mLifo = v; }
		[[nodiscard]] bool getLIFO() const noexcept { return mLifo; }

		void setMemoryResource(std::pmr::memory_resource &m) noexcept { mUpstreamMemRes = &m; }
		[[nodiscard]] const auto &getMemoryResource() const noexcept { return *mUpstreamMemRes; }
		[[nodiscard]] auto &getMemoryResource() noexcept { return *mUpstreamMemRes; }

		void clear() noexcept
		{
			if(mBlocks.empty())
			{
				mLast = mPtr = nullptr;
				mCurrentBlock = {};
			}
			else
			{
				mCurrentBlock = mBlocks.begin();
				mPtr = mBlocks.front().data;
				mLast = mPtr + mBlocks.front().size;
			}
		}

		void free() noexcept
		{
			assert_valid(mUpstreamMemRes);

			for(auto &b : mBlocks)
				mUpstreamMemRes->deallocate(b.data, b.size, b.align);

			mBlocks.clear();
			clear();
		}

	private:

		[[nodiscard]] virtual void *do_allocate(szt n, szt alignment) override
		{
			plassert(n % alignment == 0);

			if(n + (alignment <= mBlockAlign ? 0 : alignment) > getBlockSize())
				return createLargeBlock(n, alignment);

			if(!mPtr)
			{
				createNormalBlock(alignment);
				mCurrentBlock = std::prev(mBlocks.end());
			}

			auto res = getPtr(n, alignment);

			if(!res)
			{
				nextBlock(alignment);
				res = getPtr(n, alignment);
			}

			mPtr = res + n;

			plassert((std::ptrdiff_t)res % alignment == 0);

			return static_cast<void *>(res);
		}

		virtual void do_deallocate(void *p, szt n, szt /*alignment*/) override
		{
			if(!mLifo)
				return;
			assert_valid(mUpstreamMemRes);

			if(n > getBlockSize())
			{
				mUpstreamMemRes->deallocate(mBlocks.front().data, mBlocks.front().size, mBlocks.front().align);
				mBlocks.pop_front();
			}
			else
			{
				mPtr = static_cast<std::byte *>(p);
				if(mPtr < mBlocks.back().data || mPtr >= mLast)
				{
					mUpstreamMemRes->deallocate(mBlocks.back().data, mBlocks.back().size, mBlocks.back().align);
					--mCurrentBlock;
					mBlocks.pop_back();
					mLast = mBlocks.back().data + mBlocks.back().size;
				}
			}
		}

		virtual bool do_is_equal(const std::pmr::memory_resource &o) const noexcept override
			{ return this == &o; }

	};


	template <typename T_, typename Size_, typename UpstreamAllocator_ = std::allocator<T_>>
	class stack_allocator :
		public upstream_allocator_base<stack_allocator, T_, UpstreamAllocator_, Size_, UpstreamAllocator_>,
		copymove_initialize_only
	{
		using upstream_base_type = upstream_allocator_base<stack_allocator, T_, UpstreamAllocator_, Size_, UpstreamAllocator_>;

		PL_CRTP_DERIVED(upstream_base_type)

	public:

		template <typename, typename, typename>
		friend class stack_allocator;

		using typename upstream_base_type::upstream_allocator_type;

		using value_type = T_;
		using pointer = T_ *;
		using const_pointer = const T_ *;
		using void_pointer = void *;
		using typename upstream_base_type::size_type;
		using typename upstream_base_type::difference_type;

		using is_always_equal = std::false_type;
		using propagate_on_container_copy_assignment = std::false_type;

		template <typename OT_>
		struct rebind
		{
			using other = stack_allocator<OT_, Size_, typename std::allocator_traits<UpstreamAllocator_>::template rebind_alloc<OT_>>;
		};

		static constexpr size_type Size = Size_{};

		using stack_data = uninitialized_value<aligned_storage<Size * sizeof(T_), alignof(T_)>>;

		[[nodiscard]] constexpr auto select_on_container_copy_construction() const
			{ return stack_allocator{this->copy_construct_select(), mData}; }

		explicit constexpr stack_allocator(const upstream_allocator_type &a = {}, stack_data *sd = nullptr)
			: upstream_base_type{a}, mData{sd} {}
		explicit constexpr stack_allocator(const upstreammost_allocator_t<upstream_allocator_type> &a, stack_data *sd = nullptr)
			requires (!std::same_as<upstream_allocator_type, upstreammost_allocator_t<upstream_allocator_type>>)
			: upstream_base_type{a}, mData{sd} {}

		template <typename... TA_>
		constexpr stack_allocator(const stack_allocator<TA_...> &o) :
			upstream_base_type{o}, mData{o.mData} {}

		template <typename... TA_>
		constexpr stack_allocator(stack_allocator<TA_...> &&o) noexcept(CNoThrowMoveContructible<upstream_base_type>) :
			upstream_base_type{std::move(o)}, mData{o.mData} {}

		constexpr stack_allocator(const stack_allocator &o) = default;
		constexpr stack_allocator(stack_allocator &&o) = default;

		[[nodiscard]] constexpr bool operator==(const stack_allocator &other) const
			{ return mData == other.mData; }

		[[nodiscard]] constexpr auto allocate(const size_type n)
		{
			if(n > Size || !mData || *mData)
				return static_cast<pointer>(base().allocate(n));

			(*mData)->emplace();

			return reinterpret_cast<pointer>(&(**mData));
		}

		constexpr void deallocate(pointer p, const size_type n)
		{
			if(n > Size || !mData || !*mData || p != reinterpret_cast<pointer>(&(**mData)))
				base().deallocate(static_cast<typename upstream_base_type::pointer>(p), n);
			else
				(*mData)->reset();
		}

	private:
		stack_data *mData = nullptr;
	};


	template <typename Alloc_, typename Category_ = void>
	class static_allocator
	{
	public:

		using upstream_allocator_type = Alloc_;

		using value_type = typename std::allocator_traits<upstream_allocator_type>::value_type;
		using pointer = typename std::allocator_traits<upstream_allocator_type>::pointer;
		using const_pointer = typename std::allocator_traits<upstream_allocator_type>::const_pointer;
		using void_pointer = typename std::allocator_traits<upstream_allocator_type>::void_pointer;
		using size_type = typename std::allocator_traits<upstream_allocator_type>::size_type;
		using difference_type = typename std::allocator_traits<upstream_allocator_type>::difference_type;

		using is_always_equal = std::true_type;
		using propagate_on_container_move_assignment = std::false_type;
		using propagate_on_container_copy_assignment = std::false_type;
		using propagate_on_container_swap = std::false_type;

		[[nodiscard]] static constexpr auto max_size() noexcept { return std::allocator_traits<upstream_allocator_type>::max_size(mUpstreamAlloc); }

		template <typename... Args_>
		static constexpr void construct(value_type *p, Args_&&... args)
			{ std::allocator_traits<upstream_allocator_type>::construct(mUpstreamAlloc, p, PL_FWD(args)...); }

		static constexpr void destroy(auto p) noexcept
			{ std::allocator_traits<upstream_allocator_type>::destroy(mUpstreamAlloc, p); }

		[[nodiscard]] static constexpr auto allocate(const size_type n)
			{ return mUpstreamAlloc.allocate(n); }

		static constexpr void deallocate(pointer p, const size_type n)
			{ mUpstreamAlloc.deallocate(p, n); }

		[[nodiscard]] constexpr bool operator==(const static_allocator &) const
			{ return true; }

	private:
		static upstream_allocator_type mUpstreamAlloc;
	};

	struct allocation_stats
	{
		std::atomic<szt> totalAllocated{};
	};

	template <typename Category_>
	[[nodiscard]] allocation_stats &alloc_stat(Category_ = {})
	{
		static allocation_stats stats;
		return stats;
	}

	template <typename T_, typename Category_ = void, typename UpstreamAllocator_ = std::allocator<T_>>
	class allocator_with_stats : public upstream_allocator_base<allocator_with_stats, T_, UpstreamAllocator_, Category_, UpstreamAllocator_>
	{
		using upstream_base_type = upstream_allocator_base<allocator_with_stats, T_, UpstreamAllocator_, Category_, UpstreamAllocator_>;

		PL_CRTP_DERIVED(upstream_base_type)

	public:

		template <typename, typename, typename>
		friend class allocator_with_stats;

		using typename upstream_base_type::upstream_allocator_type;

		using typename upstream_base_type::value_type;
		using typename upstream_base_type::pointer;
		using typename upstream_base_type::size_type;

		template <typename OT_>
		struct rebind
		{
			using other = allocator_with_stats<OT_, Category_, typename std::allocator_traits<UpstreamAllocator_>::template rebind_alloc<OT_>>;
		};

		[[nodiscard]] constexpr auto select_on_container_copy_construction() const
			{ return allocator_with_stats{base().copy_construct_select()}; }

		[[nodiscard]] constexpr szt total_allocated() const noexcept
		{
			if constexpr(std::is_void_v<Category_>)
				return 0;
			else
				return alloc_stat(Category_{}).totalAllocated;
		}

		allocator_with_stats() = default;

		using upstream_base_type::upstream_base_type;

		/*template <typename... TA_>
		constexpr allocator_with_stats(const allocator_with_stats<TA_...> &o) :
			upstream_base_type{o} {}

		template <typename... TA_>
		constexpr allocator_with_stats(allocator_with_stats<TA_...> &&o) noexcept(CNoThrowMoveContructible<upstream_base_type>) :
			upstream_base_type{std::move(o)} {}*/

		[[nodiscard]] constexpr auto allocate(const size_type n)
		{
			auto ptr = base().allocate(n);

			if constexpr(!std::is_void_v<Category_>)
				alloc_stat(Category_{}).totalAllocated += n * sizeof(value_type);

			return ptr;
		}

		constexpr void deallocate(pointer p, const size_type n)
		{
			base().deallocate(p, n);

			if constexpr(!std::is_void_v<Category_>)
				alloc_stat(Category_{}).totalAllocated -= n * sizeof(value_type);
		}

	};

	template <typename T_, typename UpstreamAllocator_ = std::allocator<T_>>
	class allocator_noinit : public upstream_allocator_base<allocator_noinit, T_, UpstreamAllocator_, UpstreamAllocator_>
	{
		using upstream_base_type = upstream_allocator_base<allocator_noinit, T_, UpstreamAllocator_, UpstreamAllocator_>;

		PL_CRTP_DERIVED(upstream_base_type)

	public:

		template <typename, typename>
		friend class allocator_noinit;

		static_assert(CTriviallyDefaultContructible<T_> && CTriviallyDestructible<T_>, "Type must be trivially constructible and destructible.");

		using typename upstream_base_type::upstream_allocator_type;

		using typename upstream_base_type::value_type;
		using typename upstream_base_type::pointer;
		using typename upstream_base_type::size_type;

		template <typename OT_>
		struct rebind
		{
			using other = allocator_noinit<OT_, typename std::allocator_traits<UpstreamAllocator_>::template rebind_alloc<OT_>>;
		};

		[[nodiscard]] constexpr auto select_on_container_copy_construction() const
			{ return allocator_noinit{base().copy_construct_select()}; }

		allocator_noinit() = default;

		using upstream_base_type::upstream_base_type;

		/*template <typename... TA_>
		constexpr allocator_noinit(const allocator_noinit<TA_...> &o) :
			upstream_base_type{o} {}

		template <typename... TA_>
		constexpr allocator_noinit(allocator_noinit<TA_...> &&o) noexcept(CNoThrowMoveContructible<upstream_base_type>) :
			upstream_base_type{std::move(o)} {}*/

		using upstream_base_type::construct;
		constexpr void construct(value_type *) noexcept {}
		constexpr void destroy(auto) noexcept {}
	};

	template <typename T_, typename Alloc_>
	using allocator_noinit_if_trivial = std::conditional_t<
		CTriviallyContructible<T_> && CTriviallyDestructible<T_>,
			allocator_noinit<T_, Alloc_>,
			Alloc_
	>;


	template <typename T_>
	class pmr_allocator_fixed_size
	{
	public:

		template <typename>
		friend class pmr_allocator_fixed_size;

		using value_type = T_;
		using pointer = T_ *;
		using const_pointer = const pointer;
		using void_pointer = void *;
		using size_type = szt;
		using difference_type = std::ptrdiff_t;

		using is_always_equal = std::false_type;
		using propagate_on_container_move_assignment = std::true_type;
		using propagate_on_container_copy_assignment = std::true_type;
		using propagate_on_container_swap = std::true_type;

		std::pmr::memory_resource *mMemRes = std::pmr::get_default_resource();
		size_type mSize = sizeof(T_), mAlignment = alignof(T_);
		std::function<void(pointer p)> mContructor, mDeleter;

		pmr_allocator_fixed_size() = default;
		constexpr pmr_allocator_fixed_size(std::pmr::memory_resource *mr, size_type sz = sizeof(T_), size_type align = alignof(T_)) :
			mMemRes{mr}, mSize{sz}, mAlignment{align} {}

		pmr_allocator_fixed_size(const pmr_allocator_fixed_size &) = default;

		template <typename OT_>
		constexpr pmr_allocator_fixed_size(const pmr_allocator_fixed_size<OT_> &o) :
			mMemRes{o.mMemRes} {}

		[[nodiscard]] constexpr auto max_size() const noexcept { return std::numeric_limits<size_type>::max() / mSize; }

		template <typename U_, typename... Args_>
		constexpr void construct(U_ p, Args_&&... args)
		{
			if(mContructor) mContructor(static_cast<pointer>(p));

			if constexpr(CContructible<T_>)
			{
				if(!mContructor)
					construct_forward(p, PL_FWD(args)...);
			}
		}

		constexpr void destroy(auto p)
		{
			if(mDeleter)
				mDeleter(p);
			else
				std::destroy_at(p);
		}

		[[nodiscard]] constexpr auto allocate(const size_type n)
		{
			return static_cast<T_ *>(mMemRes->allocate(n * mSize, mAlignment));
		}

		constexpr void deallocate(pointer p, const size_type n)
		{
			mMemRes->deallocate(p, n * mSize, mAlignment);
		}

		[[nodiscard]] constexpr bool operator==(const pmr_allocator_fixed_size &other) const
		{
			return mMemRes == other.mMemRes && mSize == other.mSize && mAlignment == other.mAlignment;
		}

	};
}
