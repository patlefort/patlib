/*
	PatLib

	Copyright (C) 2021 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "base.hpp"
#include "concepts.hpp"

#ifdef _OPENMP

#include <omp.h>

namespace patlib
{
	void maybe_parallel_region(szt nbThreads, std::invocable auto&& fc)
	{
		if(omp_in_parallel())
			std::invoke(fc);
		else
		{
			#pragma omp parallel num_threads(nbThreads)
			#pragma omp single nowait
			std::invoke(fc);
		}
	}
}

#endif
