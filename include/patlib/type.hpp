/*
	PatLib

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "base.hpp"
#include "concepts.hpp"

#include <iterator>
#include <type_traits>
#include <experimental/type_traits>
#include <functional>
#include <tuple>
#include <utility>
#include <memory>
#include <cstddef>
#include <array>
#include <chrono>

#include <boost/hana/config.hpp>
#include <boost/hana/type.hpp>

namespace patlib
{
	#define PL_DEFAULT_COPYMOVE(classname) \
		classname(const classname &) = default; \
		classname(classname &&) = default; \
		classname &operator=(const classname &) = default; \
		classname &operator=(classname &&) = default;

	#define PL_VIRTUAL_COPYMOVE(baseclass, derivedclass) \
		using baseclass::operator=; \
		virtual baseclass &operator=(const baseclass &o) override \
		{ \
			*this = dynamic_cast<const derivedclass &>(o); \
			return static_cast<baseclass &>(*this); \
		} \
		 \
		virtual baseclass &operator=(baseclass &&o) noexcept override \
		{ \
			*this = std::move(dynamic_cast<derivedclass &>(o)); \
			return static_cast<baseclass &>(*this); \
		}

	#define PL_TAG_COMP_OP(tag) \
		[[nodiscard]] consteval bool operator== (const tag##_t &) const noexcept { return true; } \
		template <typename OT_> \
		[[nodiscard]] consteval bool operator== (const OT_ &) const noexcept { return false; }

	#define PL_MAKE_DERIVED_TAG(tagclass, ...) \
		struct tagclass##_t : __VA_ARGS__ { PL_TAG_COMP_OP(tagclass) }; \
		template <typename T_> concept tagclass##_c = std::same_as<std::remove_cv_t<T_>, tagclass##_t>; \
		constinit const tagclass##_t tagclass{};

	#define PL_MAKE_TAG(tagclass) PL_MAKE_DERIVED_TAG(tagclass, ::patlib::tag::base)

	#define PL_MAKE_STATIC_DERIVED_TAG(tagclass, ...) \
		struct tagclass##_t : __VA_ARGS__ { PL_TAG_COMP_OP(tagclass) }; \
		static constinit const tagclass##_t tagclass{};

	#define PL_MAKE_STATIC_TAG(tagclass) PL_MAKE_STATIC_DERIVED_TAG(tagclass, ::patlib::tag::base)

	#define PL_MAKE_DERIVED_HANA_TAG(tagclass, ...) \
		struct tagclass##_t : __VA_ARGS__ { PL_TAG_COMP_OP(tagclass) }; \
		template <typename T_> concept tagclass##_c = std::same_as<std::remove_cv_t<T_>, tagclass##_t>; \
		inline constexpr auto tagclass = boost::hana::type_c<tagclass##_t>;

	#define PL_MAKE_HANA_TAG(tagclass) PL_MAKE_DERIVED_HANA_TAG(tagclass, ::patlib::tag::base)

	#define PL_MAKE_STATIC_DERIVED_HANA_TAG(tagclass, ...) \
		struct tagclass##_t : __VA_ARGS__ { PL_TAG_COMP_OP(tagclass) }; \
		static constexpr auto tagclass = boost::hana::type_c<tagclass##_t>;

	#define PL_MAKE_STATIC_HANA_TAG(tagclass) PL_MAKE_STATIC_DERIVED_HANA_TAG(tagclass, ::patlib::tag::base)

	template <auto T_ = []{}> struct anonymous_t {};

	#define PL_NOCOPY(classname) \
		classname(const classname &) = delete; \
		classname &operator=(const classname &) = delete;

	#define PL_NOMOVE(classname) \
		classname(classname &&) = delete; \
		classname &operator=(classname &&) = delete;

	#define PL_MOVEONLY(classname) \
		PL_NOCOPY(classname) \
		classname(classname &&) = default; \
		classname &operator=(classname &&) = default;

	#define PL_COPYMOVE_INIT_ONLY(classname) \
		constexpr classname(const classname &) = default; \
		constexpr classname(classname &&) = default; \
		constexpr classname &operator=(const classname &) = delete;

	struct no_copy
	{
		no_copy() = default;
		PL_NOCOPY(no_copy)
	};

	struct no_move
	{
		no_move() = default;
		PL_NOMOVE(no_move)
	};

	struct move_only
	{
		move_only() = default;
		PL_MOVEONLY(move_only)
	};

	struct copymove_initialize_only
	{
		copymove_initialize_only() = default;
		PL_COPYMOVE_INIT_ONLY(copymove_initialize_only)
	};

	struct no_ctor
	{
		no_ctor() = delete;
	};

	// Type detection

	template <typename T_, typename IsT_>
	concept is_a = std::derived_from<std::decay_t<T_>, IsT_>;

	template <auto t1, auto t2>
	inline constexpr auto is_lesserequal_v = t1 <= t2;

	template <auto t1, auto t2>
	inline constexpr auto is_greaterequal_v = t1 >= t2;

	template <auto t1, auto t2>
	inline constexpr auto is_greater_v = t1 > t2;

	template <auto t1, auto t2>
	inline constexpr auto is_lesser_v = t1 < t2;

	template <typename T_>
	concept CHasUnderlyingType = requires { typename T_::value_type; };

	template <typename T_>
	struct underlying_type : no_ctor
	{
		using value_type = T_;
	};

	template <CHasUnderlyingType T_>
	struct underlying_type<T_> : public underlying_type<typename T_::value_type> {};

	template <typename T_>
	using underlying_type_t = typename underlying_type<T_>::value_type;

	template <typename T_>
	concept CIntegralUnder = std::integral<underlying_type_t<T_>>;

	template <typename T_>
	concept CFloatUnder = std::floating_point<underlying_type_t<T_>>;

	template <typename T_>
	concept CBoolUnder = std::same_as<underlying_type_t<T_>, bool>;

	template <typename T_, bool Condition_>
	using add_const_if = std::conditional_t<Condition_, std::add_const_t<T_>, T_>;

	// Solve a problem with restrict keyword.
	namespace type_impl
	{
		template <typename T_>
		struct remove_restrict { using value_type = T_; };

		template <typename T_>
		struct remove_restrict<T_* BOOST_RESTRICT> { using value_type = T_*; };

		template <typename T_>
		struct remove_restrict<T_& BOOST_RESTRICT> { using value_type = T_&; };
	} // namespace type_impl

	template <typename T_>
	using remove_restrict_t = typename type_impl::remove_restrict<T_>::value_type;

	template <typename T_>
	using decay_t = std::decay_t<remove_restrict_t<T_>>;

	template <typename T_>
	using remove_cvref_t = std::remove_cvref_t<remove_restrict_t<T_>>;

	namespace tag
	{
		struct base {};
		struct init {};

		PL_MAKE_DERIVED_TAG(range_init, init)
		PL_MAKE_DERIVED_TAG(array_init, init)
		PL_MAKE_DERIVED_TAG(aligned_init, init)
		PL_MAKE_DERIVED_TAG(unaligned_init, init)

		PL_MAKE_TAG(nothing)
	}

	template <typename Tag_> concept CBaseTag = std::derived_from<Tag_, tag::base>;
	template <typename Tag_> concept CInitTag = std::derived_from<Tag_, tag::init>;

	template <typename T_>
	using type_c = boost::hana::basic_type<T_>;
	template <typename T_>
	constinit const auto type_v = boost::hana::type_c<T_>;

	namespace type_impl
	{
		template <typename T_>
		void test_type_c(const hn::basic_type<T_> &) {}
	}

	template <typename T_>
	concept CTypeC = requires(T_ t) { { type_impl::test_type_c(t) }; };

	template <szt S> using size_c = hn::size_t<S>;
	template <szt S> constinit const size_c<S> size_v{};

	template <bool S> using bool_c = std::bool_constant<S>;
	template <bool S> constinit const bool_c<S> bool_v{};
	constinit const auto bool_false = bool_v<false>;
	constinit const auto bool_true = bool_v<true>;

	template<typename T_> constinit bool is_size_c = false;
	template<szt T_> constinit bool is_size_c<size_c<T_>> = true;
	template <typename T_>
	concept CSizeC = is_size_c<std::decay_t<T_>>;

	struct zero_init_t {};

	template <typename> constinit const bool dependent_false = false;
	template <auto> constinit const bool dependent_value_false = false;

	template <typename T_, typename DependentT_> using dependent_type = T_;

	template <template <typename...> class T_, typename... Args_>
	struct alias { using type = T_<Args_...>; };

	template <template <typename...> class T_, typename... Args_>
	using variadic_alias = typename alias<T_, Args_...>::type;


	template <typename T_>
	struct [[nodiscard]] param
	{
	private:
		T_ v;

	public:
		constexpr param(T_ v_) noexcept(CNoThrowMoveContructible<T_>) : v{std::move(v_)} {}

		[[nodiscard]] constexpr auto value() const noexcept { return v; }

		[[nodiscard]] constexpr operator T_() const noexcept { return v; }

		[[nodiscard]] constexpr auto &operator* () noexcept { return v; }
		[[nodiscard]] constexpr const auto &operator* () const noexcept { return v; }
		[[nodiscard]] constexpr auto operator->() noexcept { return &v; }
		[[nodiscard]] constexpr auto operator->() const noexcept { return &v; }
	};
	//template <typename T_>
	//param(T_) -> param<remove_cvref_t<T_>>;

	template <typename T_>
	struct size_p : public param<T_> { using param<T_>::param; };
		template <typename T_> size_p(T_) -> size_p<T_>;
	template <typename T_>
	struct position_p : public param<T_> { using param<T_>::param; };
		template <typename T_> position_p(T_) -> position_p<T_>;


	template <typename... Ts_>
	struct [[nodiscard]] overload_set : public Ts_...
	{
		constexpr overload_set(Ts_... fs) : Ts_{std::move(fs)}... {}
		using Ts_::operator()...;

		template <typename... OTs_>
		[[nodiscard]] constexpr auto add(OTs_&&... fs) const
		{
			return overload_set<Ts_..., std::decay_t<OTs_>...>{static_cast<Ts_>(*this)..., PL_FWD(fs)...};
		}
	};
	template <typename... Ts_>
	overload_set(Ts_...) -> overload_set<Ts_...>;

	template <>
	struct [[nodiscard]] overload_set<>
	{
		template <typename... OTs_>
		[[nodiscard]] constexpr auto add(OTs_&&... fs) const
		{
			return overload_set<std::decay_t<OTs_>...>{PL_FWD(fs)...};
		}
	};


	// Iterator type detection.
	template <typename IT_>
	using iterator_traits = std::iterator_traits<remove_cvref_t<IT_>>;

	template <typename T_>
	[[nodiscard]] constexpr auto make_type_c(T_&&) noexcept { return boost::hana::type_c<decay_t<T_>>; }

	template <typename T_>
	[[nodiscard]] constexpr bool is_same(auto&& o, type_c<T_> = {}) noexcept
		{ return std::same_as<decay_t<decltype(o)>, T_>; }


	// Perfect capture.
	template <typename T_>
	using capture_type = std::conditional_t<std::is_lvalue_reference_v<T_>, T_, std::remove_reference_t<T_>>;

	template <typename... Ts_>
	struct [[nodiscard]] capturer
	{
		[[no_unique_address]] std::tuple<Ts_...> vars;

		constexpr capturer(Ts_&&... a) noexcept( CNoThrowContructible<std::tuple<Ts_...>, Ts_...> ) :
			vars{PL_FWD(a)...} {}
	};
	template <typename... Ts_>
	capturer(Ts_&&...) -> capturer<Ts_...>;

	template <szt I, typename... Ts_>
	[[nodiscard]] constexpr decltype(auto) get(capturer<Ts_...> &v) noexcept
		{ return std::get<I>(v.vars); }
	template <szt I, typename... Ts_>
	[[nodiscard]] constexpr decltype(auto) get(const capturer<Ts_...> &v) noexcept
		{ return std::get<I>(v.vars); }


	// Aligned type container.
	template <typename T_, szt Alignment_ = alignof(T_)>
	struct alignas(Alignment_) aligned_type
	{
		using value_type = T_;

		constexpr aligned_type() = default;
		constexpr aligned_type(const T_ &v) noexcept(CNoThrowCopyContructible<T_>) : mV{v} {}
		constexpr aligned_type(T_ &&v) noexcept(CNoThrowMoveContructible<T_>) : mV{std::move(v)} {}

		constexpr aligned_type &operator=(const T_ &v) noexcept(CNoThrowCopyAssignable<T_>)
		{
			mV = v;
			return *this;
		}

		constexpr aligned_type &operator=(T_ &&v) noexcept(CNoThrowMoveAssignable<T_>)
		{
			mV = std::move(v);
			return *this;
		}

		[[nodiscard]] constexpr auto &operator *() noexcept { return mV; }
		[[nodiscard]] constexpr const auto &operator *() const noexcept { return mV; }

		[[nodiscard]] constexpr auto &operator() () noexcept { return mV; }
		[[nodiscard]] constexpr const auto &operator() () const noexcept { return mV; }

	private:
		alignas(Alignment_) T_ mV{};
	};

	template <typename T_> constinit const bool is_aligned_type_v = false;
	template <typename T_, szt alignment> constinit const bool is_aligned_type_v<aligned_type<T_, alignment>> = true;

	template <typename T_>
	concept CAlignedType = is_aligned_type_v<T_>;

	/*
		Custom operators

		Usage: v1 /tag/ v2
		Ex: v1 /odot/ v2
	*/

	namespace type_impl
	{
		template <typename FC_>
		struct [[nodiscard]] custom_operator1
		{
			FC_ fc;
		};
		template <typename FC_>
		custom_operator1(FC_&&) -> custom_operator1<FC_>;

		template <typename FC_>
		struct [[nodiscard]] custom_operator2
		{
			FC_ fc;
		};
		template <typename FC_>
		custom_operator2(FC_&&) -> custom_operator2<FC_>;

		template <typename FC_, typename T_>
		struct [[nodiscard]] custom_operation_left
		{
			FC_ fc;
			T_ v;
		};
		template <typename FC_, typename T_>
		custom_operation_left(FC_&&, T_&&) -> custom_operation_left<FC_, T_>;

		template <typename FC_>
		constexpr decltype(auto) operator/ (auto&& v1, const custom_operator1<FC_> &f) noexcept(noexcept(f.fc(PL_FWD(v1))))
			{ return f.fc(PL_FWD(v1)); }

		template <typename FC_>
		constexpr decltype(auto) operator< (auto&& v1, const custom_operator1<FC_> &f) noexcept(noexcept(f.fc(PL_FWD(v1))))
			{ return f.fc(PL_FWD(v1)); }

		template <typename FC_>
		[[nodiscard]] constexpr auto operator/ (auto&& v1, const custom_operator2<FC_> &f) noexcept
			{ return custom_operation_left{f.fc, PL_FWD(v1)}; }

		template <typename FC_>
		[[nodiscard]] constexpr auto operator< (auto&& v1, const custom_operator2<FC_> &f) noexcept
			{ return custom_operation_left{f.fc, PL_FWD(v1)}; }

		template <typename FC_, typename T_>
		constexpr decltype(auto) operator/ (const custom_operation_left<FC_, T_> &f, auto&& v2) noexcept(noexcept(f.fc(f.v, PL_FWD(v2))))
			{ return f.fc(f.v, PL_FWD(v2)); }

		template <typename FC_, typename T_>
		constexpr decltype(auto) operator> (const custom_operation_left<FC_, T_> &f, auto&& v2) noexcept(noexcept(f.fc(f.v, PL_FWD(v2))))
			{ return f.fc(f.v, PL_FWD(v2)); }
	} // namespace type_impl

	[[nodiscard]] constexpr auto make_custom_operator1(auto&& fc) noexcept
		{ return type_impl::custom_operator1{PL_FWD(fc)}; }

	[[nodiscard]] constexpr auto make_custom_operator2(auto&& fc) noexcept
		{ return type_impl::custom_operator2{PL_FWD(fc)}; }



	template <CContructible T_>
	[[nodiscard]] constexpr auto hana_type_as_value(const boost::hana::basic_type<T_>) noexcept(CNoThrowContructible<T_>)
		{ return T_{}; }


	template <typename T_, typename... Args_>
	constexpr auto &recreate(T_ &obj, Args_&&... args)
	{
		std::destroy_at(std::addressof(obj));
		new (std::addressof(obj)) T_(PL_FWD(args)...);

		return obj;
	}

	template <typename T_>
  [[nodiscard]] constexpr auto clone(const T_ &obj)
    { return new T_(obj); }

	template <typename T_>
  [[nodiscard]] constexpr auto clone_unique(const T_ &obj)
    { return std::make_unique<T_>(obj); }

	template <typename T_>
  [[nodiscard]] constexpr auto default_init()
    { T_ v; return v; }

	// Functions to construct objects whose type depends on arguments.

	[[nodiscard]] constexpr auto unwrap_ref(auto v) noexcept
		{ return v; }
	template <typename T_>
	[[nodiscard]] constexpr auto &unwrap_ref(std::reference_wrapper<T_> v) noexcept
		{ return v.get(); }

	template <typename T_>
	constexpr void construct_forward(T_ *ptr, auto&&... args)
		{ new (static_cast<void *>(ptr)) T_(PL_FWD(args)...); }

	constexpr void static_cast_into(const auto &src, auto &into)
		{ into = static_cast<decay_t<decltype(into)>>(src); }


	// Tuple

	namespace type_impl
	{
		template <typename T_>
		constinit const bool is_tuple = false;
		template <typename... Ts_>
		constinit const bool is_tuple<std::tuple<Ts_...>> = true;
		template <typename... Ts_>
		constinit const bool is_tuple<const std::tuple<Ts_...>> = true;

		template <typename V_, typename T_, szt T_end = std::tuple_size_v<T_>>
		struct is_in_tuple : std::conditional_t<
			std::is_same_v<V_, std::tuple_element_t<T_end - 1, T_>>,
			std::true_type,
			is_in_tuple<V_, T_, T_end - 1>
		> {};

		template <typename V_, typename T_>
		struct is_in_tuple<V_, T_, 0> : std::false_type {};
	} // namespace type_impl

	template <typename T_>
	concept CTuple = type_impl::is_tuple<decay_t<T_>>;
	//template <typename T_>
	//concept CWritableTuple = CTuple<T_> && requires(T_ t) { std::apply([](auto&&... v){ ((PL_FWD(v) = PL_FWD(v)), ...); }, t); };
	template <typename V_, typename T_>
	constinit const bool is_in_tuple_v = type_impl::is_in_tuple<V_, T_>::value;

	namespace type_impl
	{
		template <typename First_, typename... T_>
		struct first_type_helper
		{
			using value_type = First_;
		};
	} // namespace type_impl

	template <typename... T_>
	using first_type = typename type_impl::first_type_helper<T_...>::value_type;


	// Array

	template <typename... T_>
	[[nodiscard]] constexpr auto make_array(T_&&... args)
	{
		return std::array<std::common_type_t<T_...>, sizeof...(args)>{PL_FWD(args)...};
	}

	template <szt... Index_>
	constexpr decltype(auto) unpack_index_sequence(std::index_sequence<Index_...>, auto&& fc)
	{
		return fc(size_v<Index_>...);
	}

	template <szt size>
	[[nodiscard]] constexpr auto make_array_sequence(const size_c<size>, auto&& fc)
	{
		return unpack_index_sequence(
			std::make_index_sequence<size>(),
			[&](const auto... i){ return make_array( std::invoke(PL_FWD(fc), i)... ); }
		);
	}

	template <typename T_, szt size>
	[[nodiscard]] constexpr auto make_initializer_list(const size_c<size>, auto&& fc)
	{
		return unpack_index_sequence(
			std::make_index_sequence<size>(),
			[&](const auto... i){ return std::initializer_list<T_>{T_{std::invoke(PL_FWD(fc), i)}...}; }
		);
	}

	template <typename T_, szt size>
	[[nodiscard]] constexpr auto construct_from_index_sequence(const size_c<size>, auto &&fc)
	{
		return unpack_index_sequence(
			std::make_index_sequence<size>(),
			[&](const auto... i){ return T_{std::invoke(PL_FWD(fc), i)...}; }
		);
	}

	template <typename T_>
	[[nodiscard]] constexpr auto construct_from_variadic_loop(auto&& fc, auto&&... args)
	{
		auto&& tpl = std::forward_as_tuple(PL_FWD(args)...);

		return unpack_index_sequence(
			std::make_index_sequence<sizeof...(args)>(),
			[&]<szt... I_>(const size_c<I_>... i){ return T_{std::invoke(PL_FWD(fc), i, std::get<I_>(PL_FWD(tpl)))...}; }
		);
	}

	constexpr decltype(auto) call_from_variadic_loop(auto&& fc, auto&& proj, auto&&... args)
	{
		auto&& tpl = std::forward_as_tuple(PL_FWD(args)...);

		return unpack_index_sequence(
			std::make_index_sequence<sizeof...(args)>(),
			[&]<szt... I_>(const size_c<I_>... i){ return std::invoke(PL_FWD(fc), std::invoke(PL_FWD(proj), i, std::get<I_>(PL_FWD(tpl)))...); }
		);
	}

	template <template <typename...> typename Deferred_, typename... Params_>
	struct defer {};

	namespace type_impl
	{
		template <typename T_>
		struct get_defer { using type = T_; };

		template <template <typename...> typename Deferred_, typename... Params_>
		struct get_defer<defer<Deferred_, Params_...>> { using type = Deferred_<Params_...>; };
	} // namespace type_impl

	template <typename T_>
	using get_defer = typename type_impl::get_defer<T_>::type;

	namespace type_impl
	{
		template <bool Condition_, typename T1_, typename T2_>
		struct deferred_if {};

		template <typename T1_, typename T2_>
		struct deferred_if<true, T1_, T2_> { using type = get_defer<T1_>; };
		template <typename T1_, typename T2_>
		struct deferred_if<false, T1_, T2_> { using type = get_defer<T2_>; };
	} // namespace type_impl

	template <bool Condition_, typename T1_, typename T2_>
	using deferred_if = typename type_impl::deferred_if<Condition_, T1_, T2_>::type;

	template <typename T_>
	[[nodiscard]] constexpr T_ implicit_cast(auto&& v) noexcept(noexcept(std::declval<T_>() = PL_FWD(v)))
		{ return PL_FWD(v); }

	[[nodiscard]] constexpr auto underlying_value(auto v) noexcept
		{ return v; }

	template <CEnum T_>
	[[nodiscard]] constexpr auto underlying_value(T_ v) noexcept
		{ return static_cast<std::underlying_type_t<T_>>(v); }

	inline constexpr auto move_proj = [](auto&& v) -> auto&& { return std::move(v); };


	template <typename... T_>
	struct properties_t : public T_...
	{
		//static_assert((szt{} + ... + sizeof(T_)) == 0, "properties must be empty (sizeof == 0).");

		constexpr properties_t() = default;
		constexpr properties_t(T_...) noexcept
			requires (sizeof...(T_) > 0)
		{}

		template <typename... OT_>
		[[nodiscard]] constexpr auto add_props(OT_... ps) const noexcept
		{
			properties_t<T_..., OT_...> newProps;

			( (static_cast<T_&>(newProps) = static_cast<const T_&>(*this)), ... );
			( (static_cast<OT_&>(newProps) = ps), ... );

			return newProps;
		}

		template <typename... OT_>
		[[nodiscard]] constexpr auto append_props(properties_t<OT_...> ps) const noexcept
		{
			properties_t<T_..., OT_...> newProps;

			( (static_cast<T_&>(newProps) = static_cast<const T_&>(*this)), ... );
			( (static_cast<OT_&>(newProps) = static_cast<const OT_&>(ps)), ... );

			return newProps;
		}

		template <typename OT_>
		[[nodiscard]] static constexpr bool has_prop(OT_ = {}) noexcept { return std::is_base_of_v<OT_, properties_t<T_...>>; }
	};
	template <typename... T_>
	properties_t(T_...) -> properties_t<T_...>;

	template <typename... T_>
	constinit const properties_t<T_...> properties{};

	namespace type_impl
	{
		template <typename T_>
		constinit const bool is_properties = false;
		template <typename... Ts_>
		constinit const bool is_properties<properties_t<Ts_...>> = true;
	}

	template <typename T_>
	concept CProperties = type_impl::is_properties<T_>;
}
