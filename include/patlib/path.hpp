/*
	PatLib

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "base.hpp"

#include <filesystem>

namespace patlib::paths
{
	[[nodiscard]] std::filesystem::path PATLIB_API app_dir();
	[[nodiscard]] std::filesystem::path PATLIB_API app_prefix_dir();
	[[nodiscard]] std::filesystem::path PATLIB_API user_home_dir();
	[[nodiscard]] std::filesystem::path PATLIB_API user_config_dir();
	[[nodiscard]] std::filesystem::path PATLIB_API user_temp_dir();
	[[nodiscard]] std::filesystem::path PATLIB_API user_data_dir();
	[[nodiscard]] std::filesystem::path PATLIB_API user_cache_dir();

	[[nodiscard]] inline std::filesystem::path absolute(const std::filesystem::path &p, const std::filesystem::path &rel = {})
	{
		return (p.is_absolute() ? p : ((rel.empty() ? std::filesystem::current_path() : rel) / p))
			.lexically_normal();
	}

	[[nodiscard]] inline std::filesystem::path canonical(const std::filesystem::path &p, const std::filesystem::path &rel = {})
	{
		return std::filesystem::canonical(
			p.is_absolute() ? p : ((rel.empty() ? std::filesystem::current_path() : rel) / p));
	}
}
