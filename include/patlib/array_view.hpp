/*
	PatLib

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	BSD 2-clause “Simplified” License

	Copyright (c) 2015, Tom Ward
	All rights reserved.

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:

	* Redistributions of source code must retain the above copyright notice, this
		list of conditions and the following disclaimer.

	* Redistributions in binary form must reproduce the above copyright notice,
		this list of conditions and the following disclaimer in the documentation
		and/or other materials provided with the distribution.

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#pragma once

#include "base.hpp"
#include "range.hpp"

#include <span>

namespace patlib::av
{
	template <szt Rank> class offset;
	template <szt Rank> class bounds;
	template <szt Rank> class bounds_iterator;
	template <typename T, szt Rank> class array_view;
	template <typename T, szt Rank> class strided_array_view;

	template <szt Rank>
	class offset
	{
	public:
		// constants and types
		static constinit const szt rank = Rank;
		using reference                 = std::ptrdiff_t&;
		using const_reference           = const std::ptrdiff_t&;
		using size_type                 = szt;
		using value_type                = std::ptrdiff_t;

		static_assert(Rank > 0, "Size of Rank must be greater than 0");

		// construction
		constexpr offset() = default;
		template <szt R = Rank, typename = std::enable_if_t<R == 1>>
		constexpr offset(value_type v) noexcept { (*this)[0] = v; }
		constexpr offset(std::initializer_list<value_type> il);

		// element access
		constexpr reference       operator[](size_type n) { return offset_[n]; }
		constexpr const_reference operator[](size_type n) const { return offset_[n]; }

		// arithmetic
		template <szt R = Rank, typename = std::enable_if_t<R == 1>>
		constexpr offset& operator++()    { return ++(*this)[0]; }
		template <szt R = Rank, typename = std::enable_if_t<R == 1>>
		constexpr offset  operator++(int) { return offset<Rank>{(*this)[0]++}; }
		template <szt R = Rank, typename = std::enable_if_t<R == 1>>
		constexpr offset& operator--()    { return --(*this)[0]; }
		template <szt R = Rank, typename = std::enable_if_t<R == 1>>
		constexpr offset  operator--(int) { return offset<Rank>{(*this)[0]--}; }

		constexpr offset& operator+=(const offset& rhs);
		constexpr offset& operator-=(const offset& rhs);

		constexpr offset  operator+() const noexcept { return *this; }
		constexpr offset  operator-() const
		{
			offset<Rank> copy{*this};
			for (value_type& elem : copy.offset_) {
				elem *= -1;
			}
			return copy;
		}

		constexpr offset& operator*=(value_type v);
		constexpr offset& operator/=(value_type v);

	private:
		std::array<value_type, rank> offset_ = {};
	};

	template <szt Rank>
	constexpr offset<Rank>::offset(std::initializer_list<value_type> il)
	{
		// Note `il` is not a constant expression, hence the runtime assert for now
		plassert(il.size() == Rank);
		std::copy(il.begin(), il.end(), offset_.data());
	}

	// arithmetic
	template <szt Rank>
	constexpr offset<Rank>& offset<Rank>::operator+=(const offset& rhs)
	{
		for (size_type i=0; i<Rank; ++i) {
			(*this)[i] += rhs[i];
		}
		return *this;
	}

	template <szt Rank>
	constexpr offset<Rank>& offset<Rank>::operator-=(const offset& rhs)
	{
		for (size_type i=0; i<Rank; ++i) {
			(*this)[i] -= rhs[i];
		}
		return *this;
	}

	template <szt Rank>
	constexpr offset<Rank>& offset<Rank>::operator*=(value_type v)
	{
		for (value_type& elem : offset_) {
			elem *= v;
		}
		return *this;
	}

	template <szt Rank>
	constexpr offset<Rank>& offset<Rank>::operator/=(value_type v)
	{
		for (value_type& elem : offset_) {
			elem /= v;
		}
		return *this;
	}


	// Free functions

	// offset equality
	template <szt Rank>
	constexpr bool operator==(const offset<Rank>& lhs, const offset<Rank>& rhs) noexcept
	{
		for (szt i=0; i<Rank; ++i) {
			if (lhs[i] != rhs[i]) return false;
		}
		return true;
	}

	template <szt Rank>
	constexpr bool operator!=(const offset<Rank>& lhs, const offset<Rank>& rhs) noexcept
	{ return !(lhs == rhs); }

	// offset arithmetic
	template <szt Rank>
	constexpr offset<Rank> operator+(const offset<Rank>& lhs, const offset<Rank>& rhs)
	{ return offset<Rank>{lhs} += rhs; }

	template <szt Rank>
	constexpr offset<Rank> operator-(const offset<Rank>& lhs, const offset<Rank>& rhs)
	{ return offset<Rank>{lhs} -= rhs; }

	template <szt Rank>
	constexpr offset<Rank> operator*(const offset<Rank>& lhs, std::ptrdiff_t v)
	{ return offset<Rank>{lhs} *= v; }

	template <szt Rank>
	constexpr offset<Rank> operator*(std::ptrdiff_t v, const offset<Rank>& rhs)
	{ return offset<Rank>{rhs} *= v; }

	template <szt Rank>
	constexpr offset<Rank> operator/(const offset<Rank>& lhs, std::ptrdiff_t v)
	{ return offset<Rank>{lhs} /= v; }


	template <szt Rank>
	class bounds
	{
	public:
		// constants and types
		static constinit const szt rank = Rank;
		using reference              = std::ptrdiff_t&;
		using const_reference        = const std::ptrdiff_t&;
		using iterator               = bounds_iterator<Rank>;
		using const_iterator         = bounds_iterator<Rank>;
		using size_type              = szt;
		using value_type             = std::ptrdiff_t;

		static_assert(Rank > 0, "Size of Rank must be greater than 0");

		// construction
		constexpr bounds() = default;

		// Question: is there a reason this constructor is not `noexcept` ?
		template <szt R = Rank, typename = std::enable_if_t<R == 1>>
		constexpr bounds(value_type v) { (*this)[0] = v; postcondition(); }
		constexpr bounds(std::initializer_list<value_type> il);

		// observers
		constexpr size_type size() const noexcept;
		constexpr bool      contains(const offset<Rank>& idx) const noexcept;

		// iterators
		const_iterator begin() const noexcept { return const_iterator{*this}; };
		const_iterator end() const noexcept {
			iterator iter{*this};
			return iter._setOffTheEnd();
		}

		// element access
		constexpr reference       operator[](size_type n) { return bounds_[n]; }
		constexpr const_reference operator[](size_type n) const { return bounds_[n]; }

		// arithmetic
		constexpr bounds& operator+=(const offset<Rank>& rhs);
		constexpr bounds& operator-=(const offset<Rank>& rhs);

		constexpr bounds& operator*=(value_type v);
		constexpr bounds& operator/=(value_type v);

	private:
		std::array<value_type, rank> bounds_ = {};

		void postcondition() { /* todo */ };
	};

	// construction
	template <szt Rank>
	constexpr bounds<Rank>::bounds(const std::initializer_list<value_type> il)
	{
		plassert(il.size() == Rank);

		std::copy(il.begin(), il.end(), bounds_.data());
		postcondition();
	}

	// observers
	template <szt Rank>
	constexpr szt bounds<Rank>::size() const noexcept
	{
		size_type product{1};
		for (const value_type& elem : bounds_) {
			product *= elem;
		}
		return product;
	}

	template <szt Rank>
	constexpr bool bounds<Rank>::contains(const offset<Rank>& idx) const noexcept
	{
		for (size_type i=0; i<Rank; ++i) {
			if ( !(0 <= idx[i] && idx[i] < (*this)[i]) ) return false;
		}
		return true;
	}

	// iterators
	// todo

	// arithmetic
	template <szt Rank>
	constexpr bounds<Rank>& bounds<Rank>::operator+=(const offset<Rank>& rhs)
	{
		for (size_type i=0; i<Rank; ++i) {
			bounds_[i] += rhs[i];
		}
		postcondition();
		return *this;
	}

	template <szt Rank>
	constexpr bounds<Rank>& bounds<Rank>::operator-=(const offset<Rank>& rhs)
	{
		for (size_type i=0; i<Rank; ++i) {
			bounds_[i] -= rhs[i];
		}
		postcondition();
		return *this;
	}

	template <szt Rank>
	constexpr bounds<Rank>& bounds<Rank>::operator*=(value_type v)
	{
		for (value_type& elem : bounds_) {
			elem *= v;
		}
		postcondition();
		return *this;
	}

	template <szt Rank>
	constexpr bounds<Rank>& bounds<Rank>::operator/=(value_type v)
	{
		for (value_type& elem : bounds_) {
			elem /= v;
		}
		postcondition();
		return *this;
	}


	// Free functions

	// bounds equality
	template <szt Rank>
	constexpr bool operator==(const bounds<Rank>& lhs, const bounds<Rank>& rhs) noexcept
	{
		for (szt i=0; i<Rank; ++i) {
			if (lhs[i] != rhs[i]) return false;
		}
		return true;
	}

	template <szt Rank>
	constexpr bool operator!=(const bounds<Rank>& lhs, const bounds<Rank>& rhs) noexcept
	{ return !(lhs == rhs); }

	// bounds arithmetic
	template <szt Rank>
	constexpr bounds<Rank> operator+(const bounds<Rank>& lhs, const offset<Rank>& rhs)
	{ return bounds<Rank>{lhs} += rhs; }

	template <szt Rank>
	constexpr bounds<Rank> operator+(const offset<Rank>& lhs, const bounds<Rank>& rhs)
	{ return bounds<Rank>{rhs} += lhs; }

	template <szt Rank>
	constexpr bounds<Rank> operator-(const bounds<Rank>& lhs, const offset<Rank>& rhs)
	{ return bounds<Rank>{lhs} -= rhs; }

	template <szt Rank>
	constexpr bounds<Rank> operator*(const bounds<Rank>& lhs, std::ptrdiff_t v)
	{ return bounds<Rank>{lhs} *= v; }

	template <szt Rank>
	constexpr bounds<Rank> operator*(std::ptrdiff_t v, const bounds<Rank>& rhs)
	{ return bounds<Rank>{rhs} *= v; }

	template <szt Rank>
	constexpr bounds<Rank> operator/(const bounds<Rank>& lhs, std::ptrdiff_t v)
	{ return bounds<Rank>{lhs} /= v; }

	template <szt Rank>
	bounds_iterator<Rank> begin(const bounds<Rank>& b) noexcept
	{ return b.begin(); }

	template <szt Rank>
	bounds_iterator<Rank> end(const bounds<Rank>& b) noexcept
	{ return b.end(); }


	template <szt Rank>
	class bounds_iterator
	{
	public:
		using iterator_category = std::random_access_iterator_tag;
		using value_type        = offset<Rank>;
		using difference_type   = std::ptrdiff_t;
		using pointer           = offset<Rank>*;  // unspecified but satisfactory (?)
		using reference         = const offset<Rank>;

		static_assert(Rank > 0, "Size of Rank must be greater than 0");

		bounds_iterator() = default;

		bounds_iterator(const bounds<Rank> bounds, offset<Rank> off = offset<Rank>()) noexcept
		: bounds_(bounds), offset_(off) {}

		bool operator==(const bounds_iterator& rhs) const {
			// Requires *this and rhs are iterators over the same bounds object.
			return offset_ == rhs.offset_;
		}

		bounds_iterator& operator++();
		bounds_iterator  operator++(int);
		bounds_iterator& operator--();
		bounds_iterator  operator--(int);

		bounds_iterator  operator+(difference_type n) const;
		bounds_iterator& operator+=(difference_type n);
		bounds_iterator  operator-(difference_type n) const;
		bounds_iterator& operator-=(difference_type n);

		difference_type  operator-(const bounds_iterator& rhs) const;

		// Note this iterator is not a true random access iterator, nor meets N4512
		// + operator* returns a value type (and not a reference)
		// + operator-> returns a pointer to the current value type, which breaks N4512 as this
		//   must be considered invalidated after any subsequent operation on this iterator
		reference operator*() const { return offset_; }
		pointer   operator->() const { return &offset_; }

		reference operator[](difference_type n) const {
			bounds_iterator<Rank> iter(*this);
			return (iter += n).offset_;
		}

		bounds_iterator& _setOffTheEnd();

	private:
		bounds<Rank> bounds_;
		offset<Rank> offset_;

		auto calcOffset() const
		{
			difference_type size = 1, res = 0;

			for(int dim=((int)Rank-1); dim>=0; size *= bounds_[dim], --dim)
				res += offset_[dim] * size;

			return res;
		}
	};

	template <szt Rank>
	bounds_iterator<Rank> bounds_iterator<Rank>::operator++(int)
	{
		bounds_iterator tmp(*this);
		++(*this);
		return tmp;
	}

	template <szt Rank>
	bounds_iterator<Rank>& bounds_iterator<Rank>::operator++()
	{
		// watchit: dim must be signed in order to fail the condition dim>=0
		for (int dim=((int)Rank-1); dim>=0; --dim)
		{
			if (++offset_[dim] < bounds_[dim])
				return (*this);
			else
				offset_[dim] = 0;
		}

		// off-the-end value
		_setOffTheEnd();

		return *this;
	}

	template <szt Rank>
	bounds_iterator<Rank>& bounds_iterator<Rank>::operator--()
	{
		// watchit: dim must be signed in order to fail the condition dim>=0
		for (int dim=((int)Rank-1); dim>=0; --dim)
		{
			if (--offset_[dim] >= 0)
				return (*this);
			else
				offset_[dim] = bounds_[dim]-1;
		}

		// before-the-start value
		for (int dim=0; dim<(int)Rank-1; ++dim) {
			offset_[dim] = 0;
		}
		offset_[Rank-1] = -1;
		return *this;
	}

	template <szt Rank>
	bounds_iterator<Rank> bounds_iterator<Rank>::operator--(int)
	{
		bounds_iterator tmp(*this);
		--(*this);
		return tmp;
	}

	template <szt Rank>
	bounds_iterator<Rank>& bounds_iterator<Rank>::_setOffTheEnd()
	{
		for (szt dim=0; dim<Rank-1; ++dim) {
			offset_[dim] = bounds_[dim]-1;
		}
		offset_[Rank-1] = bounds_[Rank-1];

		return *this;
	}

	template <szt Rank>
	bounds_iterator<Rank>& bounds_iterator<Rank>::operator+=(difference_type n)
	{
		for (int dim=((int)Rank-1); dim>=0; --dim)
		{
			difference_type remainder = (n + offset_[dim]) % bounds_[dim];
			n = (n + offset_[dim]) / bounds_[dim];
			offset_[dim] = remainder;
		}
		//plassert(n == 0);  // no overflow
		return *this;
	}

	template <szt Rank>
	bounds_iterator<Rank> bounds_iterator<Rank>::operator+(difference_type n) const
	{
		bounds_iterator<Rank> iter(*this);
		return iter += n;
	}

	template <szt Rank>
	bounds_iterator<Rank>& bounds_iterator<Rank>::operator-=(difference_type n)
	{
		// take (diminished) radix compliment
		auto diminishedRadixComplement = [&]() {
			for (int dim=((int)Rank-1); dim>=0; --dim)
			{
				offset_[dim] = bounds_[dim] - offset_[dim];
			}
		};

		diminishedRadixComplement();
		*this += n;
		diminishedRadixComplement();

		return *this;
	}

	template <szt Rank>
	bounds_iterator<Rank> bounds_iterator<Rank>::operator-(difference_type n) const
	{
		bounds_iterator<Rank> iter(*this);
		return iter -= n;
	}

	template <szt Rank>
	typename bounds_iterator<Rank>::difference_type bounds_iterator<Rank>::operator-(const bounds_iterator<Rank> &rhs) const
	{
		return calcOffset() - rhs.calcOffset();
	}

	// Free functions

	template <szt Rank>
	bool operator==(const bounds_iterator<Rank>& lhs, const bounds_iterator<Rank>& rhs)
	{ return lhs.operator==(rhs); }

	template <szt Rank>
	bool operator!=(const bounds_iterator<Rank>& lhs, const bounds_iterator<Rank>& rhs)
	{ return !lhs.operator==(rhs); }

	template <szt Rank>
	bool operator<(const bounds_iterator<Rank>& lhs, const bounds_iterator<Rank>& rhs)
	{ return rhs - lhs > 0; }

	template <szt Rank>
	bool operator<=(const bounds_iterator<Rank>& lhs, const bounds_iterator<Rank>& rhs)
	{ return !(lhs > rhs); }

	template <szt Rank>
	bool operator>(const bounds_iterator<Rank>& lhs, const bounds_iterator<Rank>& rhs)
	{ return rhs < lhs; }

	template <szt Rank>
	bool operator>=(const bounds_iterator<Rank>& lhs, const bounds_iterator<Rank>& rhs)
	{ return !(lhs < rhs); }

	template <szt Rank>
	bounds_iterator<Rank> operator+(typename bounds_iterator<Rank>::difference_type n,
																	const bounds_iterator<Rank>& rhs);

	namespace {

		template <typename Viewable, typename U, typename View = std::remove_reference_t<Viewable>>
		using is_viewable_on_u = std::bool_constant<
				std::is_convertible<typename View::size_type, std::ptrdiff_t>::value &&
				std::is_convertible<typename View::value_type*, std::add_pointer_t<U>>::value &&
				std::is_same<std::remove_cv_t<typename View::value_type>, std::remove_cv_t<U>>::value

			>;

		template <typename T, typename U>
		using is_viewable_value = std::bool_constant<
				std::is_convertible<std::add_pointer_t<T>, std::add_pointer_t<U>>::value &&
				std::is_same<std::remove_cv_t<T>, std::remove_cv_t<U>>::value
			>;

		template <typename T, szt Rank>
		constexpr T& view_access(T* data, const offset<Rank>& idx, const offset<Rank>& stride)
		{
			std::ptrdiff_t off{};
			for (szt i=0; i<Rank; ++i)
			{
				off += idx[i] * stride[i];
			}
			return data[off];
		}

	} // namespace

	template <typename T, szt Rank = 1>
	class array_view
	{
	public:
		static constinit const szt rank = Rank;
		using offset_type               = offset<Rank>;
		using bounds_type               = av::bounds<Rank>;
		using size_type                 = szt;
		using difference_type           = std::ptrdiff_t;
		using value_type                = T;
		using pointer                   = T*;
		using const_pointer             = const T*;
		using reference                 = T&;
		using const_reference           = const T&;

		using iterator = pointer;
		using const_iterator = const_pointer;
		using reverse_iterator = std::reverse_iterator<iterator>;
		using const_reverse_iterator = std::reverse_iterator<const_iterator>;

		static_assert(Rank > 0, "Size of Rank must be greater than 0");

		constexpr array_view() noexcept : data_{} {}

		template <rg::contiguous_range R_>
			requires (rg::sized_range<R_> && is_viewable_on_u<R_, value_type>::value)
		constexpr array_view(R_&& r)
			: data_{std::addressof(*r.begin())}, bounds_{rg::size(r)} {}

		template <typename U, szt R = Rank,
							typename = std::enable_if_t<R == 1 && is_viewable_value<U, value_type>::value>>
			constexpr array_view(const array_view<U, R>& rhs) noexcept
				: data_(rhs.data()), bounds_(rhs.bounds()) {}

		template <szt Extent,
							typename = std::enable_if_t<Extent == 1>>
		constexpr array_view(value_type (&arr)[Extent]) noexcept
			: data_(arr), bounds_(Extent) {}

		template <typename U,
							typename = std::enable_if_t<is_viewable_value<U, value_type>::value>>
		constexpr array_view(const array_view<U, Rank>& rhs) noexcept
			: data_(rhs.data()), bounds_(rhs.bounds()) {}

		template <rg::contiguous_range R_>
			requires (rg::sized_range<R_> && is_viewable_on_u<R_, value_type>::value)
		constexpr array_view(R_&& vw, bounds_type bounds)
			: data_(vw.data()), bounds_(bounds)
		{
			plassert(bounds.size() <= vw.size());
		}

		constexpr array_view(pointer ptr, bounds_type bounds)
			: data_(ptr), bounds_(bounds) {}

		// Iterating
		[[nodiscard]] auto begin() { return iterator{data_}; }
		[[nodiscard]] auto begin() const { return const_iterator{data_}; }
		[[nodiscard]] auto cbegin() const { return const_iterator{data_}; }
		[[nodiscard]] auto end() { return iterator{data_ + size()}; }
		[[nodiscard]] auto end() const { return const_iterator{data_ + size()}; }
		[[nodiscard]] auto cend() const { return const_iterator{data_ + size()}; }

		[[nodiscard]] auto rbegin() { return reverse_iterator{end()}; }
		[[nodiscard]] auto rbegin() const { return const_reverse_iterator{end()}; }
		[[nodiscard]] auto crbegin() const { return const_reverse_iterator{cend()}; }
		[[nodiscard]] auto rend() { return reverse_iterator{begin()}; }
		[[nodiscard]] auto rend() const { return const_reverse_iterator{begin()}; }
		[[nodiscard]] auto crend() const { return const_reverse_iterator{cbegin()}; }

		// observers
		constexpr bounds_type       bounds() const noexcept { return bounds_; }
		constexpr size_type         size()   const noexcept { return bounds().size(); }
		constexpr offset_type       stride() const noexcept;
		constexpr pointer           data()   const noexcept { return data_; }

		constexpr reference operator[](const offset_type& idx) const
		{
			plassert(bounds().contains(idx) == true);
			return view_access(data_, idx, stride());
		}

		// slicing and sectioning
		template <szt R = Rank, typename = std::enable_if_t< R>=2 >>
		constexpr array_view<T, Rank-1> operator[](std::ptrdiff_t slice) const
		{
			return calc_new_bounds(data_, slice);
		}

		constexpr strided_array_view<T, Rank>
		section(const offset_type& origin, const bounds_type& section_bounds) const
		{
		// todo: requirement is for any idx in section_bounds (boundary fail)
			// plassert(bounds().contains(origin + section_bounds) == true);
			return strided_array_view<T, Rank>(&(*this)[origin], section_bounds, stride());
		}

		constexpr strided_array_view<T, Rank>
		section(const offset_type& origin) const
		{
			// todo: requires checking for any idx in bounds() - origin
		// plassert(bounds().contains(bounds()) == true);
			return strided_array_view<T, Rank>(&(*this)[origin], bounds() - origin, stride());
		}

	private:
		pointer data_;
		bounds_type bounds_;

		template <typename PT_>
		constexpr auto calc_new_bounds(PT_ *data, std::ptrdiff_t slice) const
		{
			plassert(0 <= slice && slice < bounds()[0]);

			av::bounds<Rank-1> new_bounds{};
			for (szt i=0; i<rank-1; ++i) {
				new_bounds[i] = bounds()[i+1];
			}

			std::ptrdiff_t off = slice * stride()[0];

			return array_view<PT_, Rank-1>(data + off, new_bounds);
		}

		[[nodiscard]] friend constexpr auto tag_invoke(tag_t<as_const_view>, const array_view &v) noexcept
			{ return array_view<const T, Rank>{v}; }
	};
	template <rg::contiguous_range R_>
	array_view(R_&&) -> array_view<std::remove_reference_t<rg::range_reference_t<R_>>>;
	template <rg::contiguous_range R_, szt Rank_>
	array_view(R_&&, bounds<Rank_>) -> array_view<std::remove_reference_t<rg::range_reference_t<R_>>, Rank_>;
	template <typename T_, szt Extend_>
	array_view(T_ (&ar)[Extend_]) -> array_view<T_>;

	template <typename T, szt Rank>
	constexpr typename array_view<T,Rank>::offset_type array_view<T,Rank>::stride() const noexcept
	{
		offset_type stride{};
		stride[rank-1] = 1;
		for (int dim=static_cast<int>(rank)-2; dim>=0; --dim)
		{
			stride[dim] = stride[dim+1] * bounds()[dim + 1];
		}
		return stride;
	}

	template <class T, szt Rank = 1>
	class strided_array_view
	{
	public:
		// constants and types
		static constinit const szt rank = Rank;
		using offset_type               = offset<Rank>;
		using bounds_type               = av::bounds<Rank>;
		using size_type                 = szt;
		using difference_type           = std::ptrdiff_t;
		using value_type                = T;
		using pointer                   = T*;
		using const_pointer             = const T*;
		using reference                 = T&;
		using const_reference           = const T&;

		using iterator = pointer;
		using const_iterator = const_pointer;
		using reverse_iterator = std::reverse_iterator<iterator>;
		using const_reverse_iterator = std::reverse_iterator<const_iterator>;

		// constructors, copy, and assignment
		constexpr strided_array_view() noexcept
			: data_{nullptr}, bounds_{}, stride_{} {}

		template <typename U, typename = std::enable_if_t<is_viewable_value<U, value_type>::value>>
		constexpr strided_array_view(const array_view<U, Rank>& rhs) noexcept
			: data_{rhs.data()}, bounds_{rhs.bounds()}, stride_{rhs.stride()} {}
		template <typename U, typename = std::enable_if_t<is_viewable_value<U, value_type>::value>>
		constexpr strided_array_view(const strided_array_view<U, Rank>& rhs) noexcept
			: data_{rhs.data_}, bounds_{rhs.bounds()}, stride_{rhs.stride()} {}

		constexpr strided_array_view(pointer ptr, bounds_type bounds, offset_type stride)
			: data_(ptr), bounds_(bounds), stride_(stride)
		{
			// todo: assert that sum(idx[i] * stride[i]) fits in std::ptrdiff_t
		}

		// observers
		constexpr bounds_type bounds() const noexcept { return bounds_; }
		constexpr size_type   size()   const noexcept { return bounds_.size(); }
		constexpr offset_type stride() const noexcept { return stride_; }

		// Iterating
		[[nodiscard]] auto begin() { return iterator{data_}; }
		[[nodiscard]] auto begin() const { return const_iterator{data_}; }
		[[nodiscard]] auto cbegin() const { return const_iterator{data_}; }
		[[nodiscard]] auto end() { return iterator{data_ + size()}; }
		[[nodiscard]] auto end() const { return const_iterator{data_ + size()}; }
		[[nodiscard]] auto cend() const { return const_iterator{data_ + size()}; }

		[[nodiscard]] auto rbegin() { return reverse_iterator{end()}; }
		[[nodiscard]] auto rbegin() const { return const_reverse_iterator{end()}; }
		[[nodiscard]] auto crbegin() const { return const_reverse_iterator{cend()}; }
		[[nodiscard]] auto rend() { return reverse_iterator{begin()}; }
		[[nodiscard]] auto rend() const { return const_reverse_iterator{begin()}; }
		[[nodiscard]] auto crend() const { return const_reverse_iterator{cbegin()}; }

		// element access
		constexpr reference operator[](const offset_type& idx) const
		{
			plassert(bounds().contains(idx) == true);
			return view_access(data_, idx, stride_);
		}

		// slicing and sectioning
		template <szt R = Rank, typename = std::enable_if_t< R>=2 >>
		constexpr strided_array_view<T, Rank-1> operator[](std::ptrdiff_t slice) const
		{
			return calc_new_bounds(data_, slice);
		}

		constexpr strided_array_view<T, Rank>
		section(const offset_type& origin, const bounds_type& section_bounds) const
		{
			// todo: requirement is for any idx in section_bounds (boundary fail)
			// plassert(bounds().contains(origin + section_bounds) == true);
				return strided_array_view<T, Rank>(&(*this)[origin], section_bounds, stride());
		}

		constexpr strided_array_view<T, Rank>
		section(const offset_type& origin) const
		{
			// todo: requires checking for any idx in bounds() - origin
			// plassert(bounds().contains(bounds()) == true);
				return strided_array_view<T, Rank>(&(*this)[origin], bounds() - origin, stride());
		}

	private:
		pointer     data_;
		bounds_type bounds_;
		offset_type stride_;

		template <typename PT_>
		constexpr auto calc_new_bounds(PT_ *data, std::ptrdiff_t slice) const
		{
			plassert(0 <= slice && slice < bounds()[0]);

			av::bounds<Rank-1> new_bounds{};
			for (szt i=0; i<rank-1; ++i) {
				new_bounds[i] = bounds()[i+1];
			}

			av::offset<Rank-1> new_stride{};
			for (szt i=0; i<rank-1; ++i) {
				new_stride[i] = stride()[i+1];
			}

			std::ptrdiff_t off = slice * stride()[0];

			return strided_array_view<PT_, Rank-1>(data + off, new_bounds, new_stride);
		}

		[[nodiscard]] friend constexpr auto tag_invoke(tag_t<as_const_view>, const strided_array_view &v) noexcept
			{ return strided_array_view<const T, Rank>{v}; }
	};
} // namespace patlib::av
