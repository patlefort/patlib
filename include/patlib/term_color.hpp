/*
    patlib

    Copyright (C) 2020 Patrick Northon

    I have modified this file.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//!
//! termcolor
//! ~~~~~~~~~
//!
//! termcolor is a header-only c++ library for printing colored messages
//! to the terminal. Written just for fun with a help of the Force.
//!
//! :copyright: (c) 2013 by Ihor Kalnytskyi
//! :license: BSD, see LICENSE for details
//!

// LICENSE file below:
/*
    Copyright (c) 2013, Ihor Kalnytskyi.
    All rights reserved.

    Redistribution and use in source and binary forms of the software as well
    as documentation, with or without modification, are permitted provided
    that the following conditions are met:

    * Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    * Redistributions in binary form must reproduce the above
    copyright notice, this list of conditions and the following
    disclaimer in the documentation and/or other materials provided
    with the distribution.

    * The names of the contributors may not be used to endorse or
    promote products derived from this software without specific
    prior written permission.

    THIS SOFTWARE AND DOCUMENTATION IS PROVIDED BY THE COPYRIGHT HOLDERS AND
    CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
    NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
    A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
    OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
    EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
    PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
    PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
    LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
    NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE AND DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
    DAMAGE.
*/

// I have modified this file.

#pragma once

// the following snippet of code detects the current OS and
// defines the appropriate macro that is used to wrap some
// platform specific things
#if defined(_WIN32) || defined(_WIN64)
#   define PL_TERMCOLOR_OS_WINDOWS
#elif defined(__APPLE__)
#   define PL_TERMCOLOR_OS_MACOS
#elif defined(__unix__) || defined(__unix)
#   define PL_TERMCOLOR_OS_LINUX
#endif


// This headers provides the `isatty()`/`fileno()` functions,
// which are used for testing whether a standart stream refers
// to the terminal. As for Windows, we also need WinApi funcs
// for changing colors attributes of the terminal.
#if defined(PL_TERMCOLOR_OS_MACOS) || defined(PL_TERMCOLOR_OS_LINUX)
#   include <unistd.h>
#elif defined(PL_TERMCOLOR_OS_WINDOWS)
#   include <io.h>

#   define WIN32_LEAN_AND_MEAN
#   include <windows.h>
#endif


#include <iostream>
#include <cstdio>



namespace patlib::termcolor
{
    inline bool is_colorized(std::ostream& stream);

    // Forward declaration of the `impl` namespace.
    // All comments are below.
    namespace impl
    {
        constexpr long Default = 0;
        constexpr long Colorize = 2;
        constexpr long NoColorize = 2;

        // An index to be used to access a private storage of I/O streams. See
        // colorize / nocolorize I/O manipulators for details.
        static int colorize_index = std::ios_base::xalloc();

        inline FILE* get_standard_stream(const std::ostream& stream);
        inline bool is_atty(const std::ostream& stream);

    #if defined(PL_TERMCOLOR_OS_WINDOWS)
        inline void win_change_attributes(std::ostream& stream, int foreground, int background=-1);
    #endif
    }

    inline
    std::ostream& colorize(std::ostream& stream)
    {
        stream.iword(impl::colorize_index) = impl::Colorize;
        return stream;
    }

    inline
    std::ostream& nocolorize(std::ostream& stream)
    {
        stream.iword(impl::colorize_index) = impl::NoColorize;
        return stream;
    }

    inline
    std::ostream& reset(std::ostream& stream)
    {
        if (is_colorized(stream))
        {
        #if defined(PL_TERMCOLOR_OS_MACOS) || defined(PL_TERMCOLOR_OS_LINUX)
            stream << "\033[00m";
        #elif defined(PL_TERMCOLOR_OS_WINDOWS)
            impl::win_change_attributes(stream, -1, -1);
        #endif
        }
        return stream;
    }


    inline
    std::ostream& bold(std::ostream& stream)
    {
        if (is_colorized(stream))
        {
        #if defined(PL_TERMCOLOR_OS_MACOS) || defined(PL_TERMCOLOR_OS_LINUX)
            stream << "\033[1m";
        #elif defined(PL_TERMCOLOR_OS_WINDOWS)
        #endif
        }
        return stream;
    }


    inline
    std::ostream& dark(std::ostream& stream)
    {
        if (is_colorized(stream))
        {
        #if defined(PL_TERMCOLOR_OS_MACOS) || defined(PL_TERMCOLOR_OS_LINUX)
            stream << "\033[2m";
        #elif defined(PL_TERMCOLOR_OS_WINDOWS)
        #endif
        }
        return stream;
    }


    inline
    std::ostream& underline(std::ostream& stream)
    {
        if (is_colorized(stream))
        {
        #if defined(PL_TERMCOLOR_OS_MACOS) || defined(PL_TERMCOLOR_OS_LINUX)
            stream << "\033[4m";
        #elif defined(PL_TERMCOLOR_OS_WINDOWS)
        #endif
        }
        return stream;
    }


    inline
    std::ostream& blink(std::ostream& stream)
    {
        if (is_colorized(stream))
        {
        #if defined(PL_TERMCOLOR_OS_MACOS) || defined(PL_TERMCOLOR_OS_LINUX)
            stream << "\033[5m";
        #elif defined(PL_TERMCOLOR_OS_WINDOWS)
        #endif
        }
        return stream;
    }


    inline
    std::ostream& reverse(std::ostream& stream)
    {
        if (is_colorized(stream))
        {
        #if defined(PL_TERMCOLOR_OS_MACOS) || defined(PL_TERMCOLOR_OS_LINUX)
            stream << "\033[7m";
        #elif defined(PL_TERMCOLOR_OS_WINDOWS)
        #endif
        }
        return stream;
    }


    inline
    std::ostream& concealed(std::ostream& stream)
    {
        if (is_colorized(stream))
        {
        #if defined(PL_TERMCOLOR_OS_MACOS) || defined(PL_TERMCOLOR_OS_LINUX)
            stream << "\033[8m";
        #elif defined(PL_TERMCOLOR_OS_WINDOWS)
        #endif
        }
        return stream;
    }


    inline
    std::ostream& grey(std::ostream& stream)
    {
        if (is_colorized(stream))
        {
        #if defined(PL_TERMCOLOR_OS_MACOS) || defined(PL_TERMCOLOR_OS_LINUX)
            stream << "\033[30m";
        #elif defined(PL_TERMCOLOR_OS_WINDOWS)
            impl::win_change_attributes(stream,
                0   // grey (black)
            );
        #endif
        }
        return stream;
    }

    inline
    std::ostream& red(std::ostream& stream)
    {
        if (is_colorized(stream))
        {
        #if defined(PL_TERMCOLOR_OS_MACOS) || defined(PL_TERMCOLOR_OS_LINUX)
            stream << "\033[31m";
        #elif defined(PL_TERMCOLOR_OS_WINDOWS)
            impl::win_change_attributes(stream,
                FOREGROUND_RED
            );
        #endif
        }
        return stream;
    }

    inline
    std::ostream& green(std::ostream& stream)
    {
        if (is_colorized(stream))
        {
        #if defined(PL_TERMCOLOR_OS_MACOS) || defined(PL_TERMCOLOR_OS_LINUX)
            stream << "\033[32m";
        #elif defined(PL_TERMCOLOR_OS_WINDOWS)
            impl::win_change_attributes(stream,
                FOREGROUND_GREEN
            );
        #endif
        }
        return stream;
    }

    inline
    std::ostream& yellow(std::ostream& stream)
    {
        if (is_colorized(stream))
        {
        #if defined(PL_TERMCOLOR_OS_MACOS) || defined(PL_TERMCOLOR_OS_LINUX)
            stream << "\033[33m";
        #elif defined(PL_TERMCOLOR_OS_WINDOWS)
            impl::win_change_attributes(stream,
                FOREGROUND_GREEN | FOREGROUND_RED
            );
        #endif
        }
        return stream;
    }

    inline
    std::ostream& blue(std::ostream& stream)
    {
        if (is_colorized(stream))
        {
        #if defined(PL_TERMCOLOR_OS_MACOS) || defined(PL_TERMCOLOR_OS_LINUX)
            stream << "\033[34m";
        #elif defined(PL_TERMCOLOR_OS_WINDOWS)
            impl::win_change_attributes(stream,
                FOREGROUND_BLUE
            );
        #endif
        }
        return stream;
    }

    inline
    std::ostream& magenta(std::ostream& stream)
    {
        if (is_colorized(stream))
        {
        #if defined(PL_TERMCOLOR_OS_MACOS) || defined(PL_TERMCOLOR_OS_LINUX)
            stream << "\033[35m";
        #elif defined(PL_TERMCOLOR_OS_WINDOWS)
            impl::win_change_attributes(stream,
                FOREGROUND_BLUE | FOREGROUND_RED
            );
        #endif
        }
        return stream;
    }

    inline
    std::ostream& cyan(std::ostream& stream)
    {
        if (is_colorized(stream))
        {
        #if defined(PL_TERMCOLOR_OS_MACOS) || defined(PL_TERMCOLOR_OS_LINUX)
            stream << "\033[36m";
        #elif defined(PL_TERMCOLOR_OS_WINDOWS)
            impl::win_change_attributes(stream,
                FOREGROUND_BLUE | FOREGROUND_GREEN
            );
        #endif
        }
        return stream;
    }

    inline
    std::ostream& white(std::ostream& stream)
    {
        if (is_colorized(stream))
        {
        #if defined(PL_TERMCOLOR_OS_MACOS) || defined(PL_TERMCOLOR_OS_LINUX)
            stream << "\033[37m";
        #elif defined(PL_TERMCOLOR_OS_WINDOWS)
            impl::win_change_attributes(stream,
                FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED
            );
        #endif
        }
        return stream;
    }



    inline
    std::ostream& on_grey(std::ostream& stream)
    {
        if (is_colorized(stream))
        {
        #if defined(PL_TERMCOLOR_OS_MACOS) || defined(PL_TERMCOLOR_OS_LINUX)
            stream << "\033[40m";
        #elif defined(PL_TERMCOLOR_OS_WINDOWS)
            impl::win_change_attributes(stream, -1,
                0   // grey (black)
            );
        #endif
        }
        return stream;
    }

    inline
    std::ostream& on_red(std::ostream& stream)
    {
        if (is_colorized(stream))
        {
        #if defined(PL_TERMCOLOR_OS_MACOS) || defined(PL_TERMCOLOR_OS_LINUX)
            stream << "\033[41m";
        #elif defined(PL_TERMCOLOR_OS_WINDOWS)
            impl::win_change_attributes(stream, -1,
                BACKGROUND_RED
            );
        #endif
        }
        return stream;
    }

    inline
    std::ostream& on_green(std::ostream& stream)
    {
        if (is_colorized(stream))
        {
        #if defined(PL_TERMCOLOR_OS_MACOS) || defined(PL_TERMCOLOR_OS_LINUX)
            stream << "\033[42m";
        #elif defined(PL_TERMCOLOR_OS_WINDOWS)
            impl::win_change_attributes(stream, -1,
                BACKGROUND_GREEN
            );
        #endif
        }
        return stream;
    }

    inline
    std::ostream& on_yellow(std::ostream& stream)
    {
        if (is_colorized(stream))
        {
        #if defined(PL_TERMCOLOR_OS_MACOS) || defined(PL_TERMCOLOR_OS_LINUX)
            stream << "\033[43m";
        #elif defined(PL_TERMCOLOR_OS_WINDOWS)
            impl::win_change_attributes(stream, -1,
                BACKGROUND_GREEN | BACKGROUND_RED
            );
        #endif
        }
        return stream;
    }

    inline
    std::ostream& on_blue(std::ostream& stream)
    {
        if (is_colorized(stream))
        {
        #if defined(PL_TERMCOLOR_OS_MACOS) || defined(PL_TERMCOLOR_OS_LINUX)
            stream << "\033[44m";
        #elif defined(PL_TERMCOLOR_OS_WINDOWS)
            impl::win_change_attributes(stream, -1,
                BACKGROUND_BLUE
            );
        #endif
        }
        return stream;
    }

    inline
    std::ostream& on_magenta(std::ostream& stream)
    {
        if (is_colorized(stream))
        {
        #if defined(PL_TERMCOLOR_OS_MACOS) || defined(PL_TERMCOLOR_OS_LINUX)
            stream << "\033[45m";
        #elif defined(PL_TERMCOLOR_OS_WINDOWS)
            impl::win_change_attributes(stream, -1,
                BACKGROUND_BLUE | BACKGROUND_RED
            );
        #endif
        }
        return stream;
    }

    inline
    std::ostream& on_cyan(std::ostream& stream)
    {
        if (is_colorized(stream))
        {
        #if defined(PL_TERMCOLOR_OS_MACOS) || defined(PL_TERMCOLOR_OS_LINUX)
            stream << "\033[46m";
        #elif defined(PL_TERMCOLOR_OS_WINDOWS)
            impl::win_change_attributes(stream, -1,
                BACKGROUND_GREEN | BACKGROUND_BLUE
            );
        #endif
        }
        return stream;
    }

    inline
    std::ostream& on_white(std::ostream& stream)
    {
        if (is_colorized(stream))
        {
        #if defined(PL_TERMCOLOR_OS_MACOS) || defined(PL_TERMCOLOR_OS_LINUX)
            stream << "\033[47m";
        #elif defined(PL_TERMCOLOR_OS_WINDOWS)
            impl::win_change_attributes(stream, -1,
                BACKGROUND_GREEN | BACKGROUND_BLUE | BACKGROUND_RED
            );
        #endif
        }

        return stream;
    }

    // Say if the given stream can be colorized. It usually means that it is a TTY that can output colors.
    inline
    bool can_colorize(std::ostream& stream)
    {
        return impl::is_atty(stream);
    }

    // Say whether a given stream should be colorized or not. Its true or false if the flag is set to Colorize
    // or NoColorize, otherwise it is true if can_colorize is true for the stream.
    inline
    bool is_colorized(std::ostream& stream)
    {
        const auto state = stream.iword(impl::colorize_index);
        return state == impl::Colorize || (state == impl::Default && can_colorize(stream));
    }



    //! Since C++ hasn't a way to hide something in the header from
    //! the outer access, I have to introduce this namespace which
    //! is used for internal purpose and should't be access from
    //! the user code.
    namespace impl
    {
        //! Since C++ hasn't a true way to extract stream handler
        //! from the a given `std::ostream` object, I have to write
        //! this kind of hack.
        inline
        FILE* get_standard_stream(const std::ostream& stream)
        {
            if (&stream == &std::cout)
                return stdout;
            else if ((&stream == &std::cerr) || (&stream == &std::clog))
                return stderr;

            return 0;
        }

        //! Test whether a given `std::ostream` object refers to
        //! a terminal.
        inline
        bool is_atty(const std::ostream& stream)
        {
            FILE* std_stream = get_standard_stream(stream);

            // Unfortunately, fileno() ends with segmentation fault
            // if invalid file descriptor is passed. So we need to
            // handle this case gracefully and assume it's not a tty
            // if standard stream is not detected, and 0 is returned.
            if (!std_stream)
                return false;

        #if defined(PL_TERMCOLOR_OS_MACOS) || defined(PL_TERMCOLOR_OS_LINUX)
            return ::isatty(fileno(std_stream));
        #elif defined(PL_TERMCOLOR_OS_WINDOWS)
            return ::_isatty(_fileno(std_stream));
        #endif
        }

    #if defined(PL_TERMCOLOR_OS_WINDOWS)
        //! Change Windows Terminal colors attribute. If some
        //! parameter is `-1` then attribute won't changed.
        inline void win_change_attributes(std::ostream& stream, int foreground, int background)
        {
            // yeah, i know.. it's ugly, it's windows.
            static WORD defaultAttributes = 0;

            // Windows doesn't have ANSI escape sequences and so we use special
            // API to change Terminal output color. That means we can't
            // manipulate colors by means of "std::stringstream" and hence
            // should do nothing in this case.
            if (!impl::is_atty(stream))
                return;

            // get terminal handle
            HANDLE hTerminal = INVALID_HANDLE_VALUE;
            if (&stream == &std::cout)
                hTerminal = GetStdHandle(STD_OUTPUT_HANDLE);
            else if (&stream == &std::cerr)
                hTerminal = GetStdHandle(STD_ERROR_HANDLE);

            // save default terminal attributes if it unsaved
            if (!defaultAttributes)
            {
                CONSOLE_SCREEN_BUFFER_INFO info;
                if (!GetConsoleScreenBufferInfo(hTerminal, &info))
                    return;
                defaultAttributes = info.wAttributes;
            }

            // restore all default settings
            if (foreground == -1 && background == -1)
            {
                SetConsoleTextAttribute(hTerminal, defaultAttributes);
                return;
            }

            // get current settings
            CONSOLE_SCREEN_BUFFER_INFO info;
            if (!GetConsoleScreenBufferInfo(hTerminal, &info))
                return;

            if (foreground != -1)
            {
                info.wAttributes &= ~(info.wAttributes & 0x0F);
                info.wAttributes |= static_cast<WORD>(foreground);
            }

            if (background != -1)
            {
                info.wAttributes &= ~(info.wAttributes & 0xF0);
                info.wAttributes |= static_cast<WORD>(background);
            }

            SetConsoleTextAttribute(hTerminal, info.wAttributes);
        }
    #endif // PL_TERMCOLOR_OS_WINDOWS

    } // namespace impl

}
