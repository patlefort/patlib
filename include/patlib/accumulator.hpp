/*
	PatLib

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "base.hpp"
#include "concepts.hpp"
#include "math.hpp"

#include <limits>
#include <algorithm>
#include <tuple>

#include <boost/hana.hpp>

namespace patlib::accumulators
{
	namespace impl
	{
		template <CNumberPack T_>
		struct min
		{
			using value_type = T_;
			using result_type = T_;

			constexpr min() = default;
			constexpr min(const value_type v) : mValue{std::move(v)} {}

			constexpr void operator ()(const value_type &v)
			{
				mValue = plmin(mValue, v);
			}

			[[nodiscard]] constexpr result_type result() const { return mValue; }

			constexpr void add(const min &other) { (*this)(other.mValue); }

		private:
			value_type mValue{ constants::infhighest<value_type> };
		};

		template <CNumberPack T_>
		struct max
		{
			using value_type = T_;
			using result_type = T_;

			constexpr max() = default;
			constexpr max(const value_type v) : mValue{std::move(v)} {}

			constexpr void operator ()(const value_type &v)
			{
				mValue = plmax(mValue, v);
			}

			[[nodiscard]] constexpr result_type result() const { return mValue; }

			constexpr void add(const max &other) { (*this)(other.mValue); }

		private:
			value_type mValue{ constants::inflowest<value_type> };
		};

		template <CNumberPack T_>
		struct count
		{
			using value_type = T_;
			using result_type = T_;

			constexpr count() = default;
			constexpr count(const value_type v) : mValue{std::move(v)} {}

			constexpr void operator ()(const value_type &)
				{ ++mValue; }

			[[nodiscard]] constexpr result_type result() const { return mValue; }

			constexpr void add(const count &other) { mValue += other.mValue; }

		private:
			value_type mValue{};
		};

		template <CNumberPack T_>
		struct sum
		{
			using value_type = T_;
			using result_type = T_;

			constexpr sum() = default;
			constexpr sum(const value_type v) : mValue{std::move(v)} {}

			constexpr void operator ()(const value_type &v)
				{ mValue += v; }

			[[nodiscard]] constexpr result_type result() const { return mValue; }

			constexpr void add(const sum &other) { (*this)(other.mValue); }

		private:
			value_type mValue{};
		};
	} // namespace impl

	template <typename... Tags_>
	[[nodiscard]] constexpr auto depends_on() { return hn::make_set(hn::type_c<Tags_>...); }

	namespace tag
	{
		struct min
		{
			static constexpr auto DependsOn = depends_on<>();

			template <typename T_>
			using impl_type = impl::min<T_>;
		};

		inline constexpr auto min_c = hn::type_c<min>;


		struct max
		{
			static constexpr auto DependsOn = depends_on<>();

			template <typename T_>
			using impl_type = impl::max<T_>;
		};

		inline constexpr auto max_c = hn::type_c<max>;


		struct sum
		{
			static constexpr auto DependsOn = depends_on<>();

			template <typename T_>
			using impl_type = impl::sum<T_>;
		};

		inline constexpr auto sum_c = hn::type_c<sum>;


		struct count
		{
			static constexpr auto DependsOn = depends_on<>();

			template <typename T_>
			using impl_type = impl::count<T_>;
		};

		inline constexpr auto count_c = hn::type_c<count>;
	} // namespace tag


	namespace impl
	{
		template <CNumberPack T_, CNumberPack ResultType_ = long double>
		struct mean
		{
			using value_type = T_;
			using result_type = ResultType_;

			constexpr mean() = default;
			constexpr mean(const value_type v) : mMean{std::move(v)} {}

			constexpr void operator ()(const value_type &v, const auto &exCount)
				{ mMean += (v - mMean) / exCount(); }

			[[nodiscard]] constexpr result_type result(const auto &) const { return mMean; }

			constexpr void add(const auto &exCountTotal, const mean &other, const auto &exOtherCount)
			{
				const result_type total = exCountTotal();
				const result_type thisCount = total - exOtherCount();

				mMean = pllerp(other.mMean, mMean, thisCount / total);
			}

		private:
			result_type mMean{};
		};
	} // namespace impl

	namespace tag
	{
		template <typename ResultType_ = long double>
		struct mean
		{
			static constexpr auto DependsOn = depends_on<count>();

			template <typename T_>
			using impl_type = impl::mean<T_, ResultType_>;
		};

		template <typename ResultType_ = long double>
		inline constexpr auto mean_c = hn::type_c<mean<ResultType_>>;
	} // namespace tag

	namespace impl
	{
		[[nodiscard]] constexpr auto build_accumulator_tag_set(const auto &tagSet)
		{
			const auto dependencies = hn::fold(tagSet, hn::make_set(), [&](auto&& state, auto tag) {
				return hn::union_( build_accumulator_tag_set(decltype(+tag)::type::DependsOn), state );
			});

			return hn::union_(tagSet, dependencies);
		}

		template <typename SampleType_>
		[[nodiscard]] constexpr auto make_accumulator_map(const hn::basic_type<SampleType_>, const auto &tagSet)
		{
			return hn::unpack(tagSet, [](auto... tags) {
				return hn::make_map( hn::make_pair(tags, typename decltype(+tags)::type::template impl_type<SampleType_>{})... );
			});
		}

		template <typename SampleType_>
		[[nodiscard]] constexpr auto extract_result_type(const auto tag) noexcept
		{
			constexpr auto gen_lambda = [](auto depend)
				{ return [&]() -> typename decltype(+extract_result_type<SampleType_>(depend))::type { return {}; }; };

			return hn::unpack(decltype(+tag)::type::DependsOn, [&](auto&&... depends){
				return hn::type_c<decltype(std::declval<typename decltype(+tag)::type::template impl_type<SampleType_>>().result(
					gen_lambda(depends)...
				))>;
			});
		}

		template <typename Feat_, typename SampleType_>
		constexpr void try_accumulate(typename Feat_::template impl_type<SampleType_> &f, const SampleType_ &rt) noexcept
		{
			hn::unpack(Feat_::DependsOn, [&](auto&&... depends){
				f(rt, [&]{ return std::declval<typename decltype(+impl::extract_result_type<SampleType_>(depends))::type>(); }...);
			});
		}

		template <typename Feat_, typename SampleType_>
		constexpr void try_add(typename Feat_::template impl_type<SampleType_> &f) noexcept
		{
			hn::unpack(Feat_::DependsOn, [&](auto&&... depends){
				f.add(
					[&]{ return std::declval<typename decltype(+impl::extract_result_type<SampleType_>(depends))::type>(); }...,
					f,
					[&]{ return std::declval<typename decltype(+impl::extract_result_type<SampleType_>(depends))::type>(); }...
				);
			});
		}

		template <typename T_, typename OT_>
		concept CTypeTagSameAs = std::same_as<typename T_::type, OT_>;

	} // namespace impl

	template <typename T_, typename SampleType_>
	concept CFeatureTag =
		requires
		{
			typename T_::template impl_type<SampleType_>;
			typename T_::template impl_type<SampleType_>::value_type;
			typename T_::template impl_type<SampleType_>::result_type;

			requires std::same_as<SampleType_, typename T_::template impl_type<SampleType_>::value_type>;
		} &&
		requires(typename T_::template impl_type<SampleType_> f, SampleType_ rt)
		{
			{ typename T_::template impl_type<SampleType_>{} };
			{ typename T_::template impl_type<SampleType_>{rt} };
			{ impl::try_accumulate<T_, SampleType_>(f, rt) };
			{ impl::extract_result_type<SampleType_>(hn::type_c<T_>) }
				-> impl::CTypeTagSameAs<typename T_::template impl_type<SampleType_>::result_type>;
			{ impl::try_add<T_, SampleType_>(f) };
		};

	template <typename SampleType_, CFeatureTag<SampleType_>... Features_>
	class accumulator_set
	{
	public:
		using sample_type = SampleType_;
		static constexpr hn::set<hn::type<Features_>...> Features{};

	private:

		static constexpr auto AccumulatorTags = impl::build_accumulator_tag_set(Features);
		using accumulator_map_type = decltype(impl::make_accumulator_map(hn::type_c<sample_type>, AccumulatorTags));

		[[nodiscard]] constexpr auto create_new_copy(auto newFeats) const
		{
			return hn::unpack(newFeats, [&](auto... feats){
				auto newset = accumulator_set<sample_type, typename decltype(+feats)::type...>();

				hn::for_each(
					hn::filter(hn::keys(newset.mAccumulators), [&](auto x){ return hn::contains(mAccumulators, x); }),
					[&](auto x) { newset.mAccumulators[x] = mAccumulators[x]; }
				);

				return newset;
			});
		}

	public:

		accumulator_map_type mAccumulators{};

		constexpr accumulator_set() = default;
		constexpr accumulator_set(hn::basic_type<SampleType_>, hn::basic_type<Features_>...) {}

		constexpr auto &operator() (const sample_type &sample)
		{
			hn::for_each(mAccumulators, [&](auto &acc)
			{
				hn::unpack(decltype(+hn::first(acc))::type::DependsOn, [&](auto&&... depends)
				{
					hn::second(acc)(sample, [&]{ return extract(depends); }...);
				});
			});

			return *this;
		}

		constexpr auto &operator() (std::initializer_list<sample_type> samples)
		{
			for(const auto &s : samples)
				(*this)(s);

			return *this;
		}

		template <CFeatureTag<sample_type> Cat1_, CFeatureTag<sample_type>... Tags_>
		[[nodiscard]] constexpr auto extract(const hn::basic_type<Cat1_> tag, const hn::basic_type<Tags_>... tags) const
		{
			const auto doExtract = [&]<typename Feat_>(const hn::basic_type<Feat_> tag)
				{
					return hn::unpack(Feat_::DependsOn, [&](auto&&... depends){
						return mAccumulators[tag].result( [&]{ return extract(depends); }... );
					});
				};

			if constexpr(!sizeof...(tags))
				return doExtract(tag);
			else
				return std::make_tuple(doExtract(tag), doExtract(tags)...);
		}

		[[nodiscard]] constexpr accumulator_set operator + (const accumulator_set &other) const
		{
			auto newset = *this;

			return newset += other;
		}

		constexpr accumulator_set &operator += (const accumulator_set &other)
		{
			hn::for_each(mAccumulators, [&](auto &acc)
			{
				hn::unpack(decltype(+hn::first(acc))::type::DependsOn, [&](auto&&... depends)
				{
					hn::second(acc).add(
						[&]{ return extract(depends); }...,
						other.mAccumulators[hn::first(acc)],
						[&]{ return other.extract(depends); }...
					);
				});
			});

			return *this;
		}

		template <CFeatureTag<sample_type>... Tags_>
		[[nodiscard]] constexpr auto remove_features(const hn::basic_type<Tags_>... toRemove) const
		{
			return create_new_copy( hn::difference(Features, hn::make_set(toRemove...)) );
		}

		template <CFeatureTag<sample_type>... Tags_>
		[[nodiscard]] constexpr auto add_features(const hn::basic_type<Tags_>... toAdd) const
		{
			return create_new_copy( hn::union_(Features, hn::make_set(toAdd...)) );
		}
	};
}
