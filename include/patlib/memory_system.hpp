/*
	PatLib

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "base.hpp"
#include "memory.hpp"
#include "type.hpp"

#include <type_traits>
#include <utility>
#include <string>
#include <string_view>
#include <cstddef>
#include <memory>

#include <experimental/type_traits>

#include <boost/uuid/uuid.hpp>
#include <boost/uuid/random_generator.hpp>
#include <boost/uuid/uuid_io.hpp>

#include <boost/interprocess/offset_ptr.hpp>
#include <boost/interprocess/creation_tags.hpp>

#include <boost/logic/tribool.hpp>

namespace patlib
{
	template <typename T_>
	using offset_ptr = boost::interprocess::offset_ptr<T_>;

	using void_offset_ptr = offset_ptr<void>;

	template <typename T_>
	using pmr_allocator_offset = pmr_allocator<T_, offset_ptr<T_>>;

	template <typename PointerType_>
	class IMemorySystem : public IMemoryResource<PointerType_>
	{
	public:
		virtual ~IMemorySystem() = default;

		using typename IMemoryResource<PointerType_>::pointer_type;

		[[nodiscard]] virtual std::string_view instance_id() const noexcept = 0;
		[[nodiscard]] virtual bool is_owner() const noexcept = 0;
		[[nodiscard]] virtual szt max_size() const noexcept = 0;
		[[nodiscard]] virtual bool is_shared() const noexcept = 0;
		[[nodiscard]] virtual bool is_initialized() const noexcept = 0;

		virtual void init(std::string instanceId, bool owner, szt maxMemory = 0) = 0;
		[[nodiscard]] virtual pointer_type named_allocate(const char *name, const char *typeName, szt size, szt alignment = alignof(std::max_align_t)) = 0;
		virtual void named_deallocate(const char *name, pointer_type p, szt size, szt alignment) = 0;
		[[nodiscard]] virtual std::pair<bool, pointer_type> named_find_or_allocate(const char *name, const char *typeName, szt size, szt alignment = alignof(std::max_align_t)) = 0;
		[[nodiscard]] virtual pointer_type named_find(const char *name) = 0;

		template <typename T_>
		[[nodiscard]] auto create_allocator() noexcept { return pmr_allocator<T_, typename std::pointer_traits<PointerType_>::template rebind<T_>>{this}; }

		template <typename T_>
		[[nodiscard]] auto create_allocator(type_c<T_>) noexcept
		{
			using VT = decay_t<T_>;
			return typename VT::allocator_type{create_allocator<typename VT::value_type>()};
		}

		void init_random(szt maxMemory = 0)
		{
			using std::to_string;
			boost::uuids::random_generator gen;
			boost::uuids::uuid id = gen();

			init(to_string(id), true, maxMemory);
		}

		template <typename T_, typename... Args_>
		[[nodiscard]] auto new_object(const char *name, Args_&&... args)
		{
			auto p = name && name[0] ?
				named_allocate(name, typeid(T_).name(), sizeof(T_), alignof(T_)) :
				this->allocate(sizeof(T_), alignof(T_));
			auto pt = static_cast<typename std::pointer_traits<pointer_type>::template rebind<T_>>(p);

			try {
				std::construct_at(std::to_address(pt), PL_FWD(args)...);
			} catch (...) {
				named_dealloc_impl(name, pt);
				throw;
			}

			return pt;
		}

		template <typename T_>
		[[nodiscard]] auto find_object(const char *name)
		{
			return static_cast<typename std::pointer_traits<pointer_type>::template rebind<T_>>(named_find(name));
		}

		template <typename T_, typename... Args_>
		[[nodiscard]] auto find_or_construct_object(const char *name, Args_&&... args)
		{
			auto res = name && name[0] ?
				named_find_or_allocate(name, typeid(T_).name(), sizeof(T_), alignof(T_)) :
				std::pair<bool, pointer_type>{false, this->allocate(sizeof(T_), alignof(T_))};

			using Rebinded = typename std::pointer_traits<pointer_type>::template rebind<T_>;
			auto pt = static_cast<Rebinded>(res.second);

			if(!res.first)
			{
				try {
					std::construct_at(std::to_address(pt), PL_FWD(args)...);
				} catch (...) {
					named_dealloc_impl(name, pt);
					throw;
				}
			}

			return std::pair<bool, Rebinded>{res.first, pt};
		}


		template<typename T_>
		void delete_object(const char *name, T_ p)
		{
			std::destroy_at(std::to_address(p));
			named_dealloc_impl(name, p);
		}

	protected:

		template <typename T_>
		void named_dealloc_impl(const char *name, T_ p)
		{
			using ET = typename std::pointer_traits<T_>::element_type;
			auto pt = pointer_type{ static_cast<typename std::pointer_traits<T_>::template rebind<void>>(p) };

			if(name && name[0])
				named_deallocate(name, pt, sizeof(ET), alignof(ET));
			else
				this->deallocate(pt, sizeof(ET), alignof(ET));
		}

	};

	template <typename T_, typename MemorySystem_>
	class memory_system_pointer
	{
	private:
		T_ *mPtr{};
		MemorySystem_ *mMemorySystem{};
		bool mIsOwner = false;
		std::string mName;

		void destroy() noexcept
		{
			if(mIsOwner && mPtr)
			{
				mMemorySystem->template delete_object(mName.c_str(), mPtr);
				mPtr = nullptr;
				mName.clear();
			}
		}

		void judge_ownership(bool found, boost::tribool ownership) noexcept
		{
			mIsOwner = (bool)ownership || (!found && boost::indeterminate(ownership));
		}

	public:
		~memory_system_pointer()
		{
			destroy();
		}

		memory_system_pointer() = default;
		memory_system_pointer(T_ *p, MemorySystem_ &ms, bool owner = true, const std::string &n = {}) noexcept :
			mPtr{p}, mMemorySystem{&ms}, mIsOwner{owner}, mName{n} {}
		memory_system_pointer(const memory_system_pointer &) = delete;
		memory_system_pointer(memory_system_pointer &&o) noexcept
			{ *this = std::move(o); }

		memory_system_pointer &operator=(const memory_system_pointer &) = delete;
		memory_system_pointer &operator=(memory_system_pointer &&o) noexcept
			{ destroy(); mName = std::move(o.mName); mPtr = move_pointer(o.mPtr); mIsOwner = o.mIsOwner; mMemorySystem = o.mMemorySystem; return *this; }

		memory_system_pointer &operator=(T_ *p) noexcept { mPtr = p; return *this; }

		template <typename... Args_>
		auto construct(MemorySystem_ &ms, std::string name, Args_&&... args)
		{
			destroy();
			mPtr = std::to_address(ms.template new_object<T_>(name.c_str(), PL_FWD(args)...));
			mMemorySystem = &ms;
			mIsOwner = true;
			mName = std::move(name);
			return mPtr;
		}

		auto find(MemorySystem_ &ms, std::string name, boost::tribool ownership = false)
		{
			destroy();
			mPtr = std::to_address(ms.template find_object<T_>(name.c_str()));
			mMemorySystem = &ms;
			judge_ownership(mPtr, ownership);
			mName = std::move(name);
			return mPtr;
		}

		template <typename... Args_>
		auto find_or_construct(MemorySystem_ &ms, std::string name, boost::tribool ownership = boost::indeterminate, Args_&&... args)
		{
			destroy();
			auto res = ms.template find_or_construct_object<T_>(name.c_str(), PL_FWD(args)...);
			mMemorySystem = &ms;
			judge_ownership(res.first, ownership);
			mPtr = std::to_address(res.second);
			mName = std::move(name);
			return std::pair<bool, T_ *>{res.first, mPtr};
		}

		[[nodiscard]] const auto &operator* () const noexcept { return *mPtr; }
		[[nodiscard]] auto &operator* () noexcept { return *mPtr; }
		[[nodiscard]] const auto *operator->() const noexcept { return mPtr; }
		[[nodiscard]] auto *operator->() noexcept { return mPtr; }
		[[nodiscard]] const auto *get() const noexcept { return mPtr; }
		[[nodiscard]] auto *get() noexcept { return mPtr; }

		[[nodiscard]] operator std::ptrdiff_t() const noexcept { return (std::ptrdiff_t)mPtr; }
		[[nodiscard]] operator T_ *() const noexcept { return mPtr; }
		[[nodiscard]] operator bool() const noexcept { return mPtr != nullptr; }

		[[nodiscard]] auto operator<=>(const memory_system_pointer &o) const noexcept { return mPtr <=> o.mPtr; }
	};

	namespace mem_sys_impl
	{
		struct inplace_interface
		{
			void construct_n(void *, szt) {}
			void destroy_n(void *, szt) {}
		};

		[[nodiscard]] auto named_memory_map_allocate(auto &segmentManager, const char *name, const char *typeName, bool find, szt size, szt alignment = alignof(std::max_align_t))
		{
			inplace_interface ipi{alignment, size, typeName};

			return static_cast<void *>( segmentManager.template generic_construct<std::byte>(name, 1, find, true, ipi) );
		}

		template <typename T_>
		concept CHasDeviceType = requires { typename T_::device_type; };

		void remove_mapped_memory(const char *, auto&) noexcept {}

		template <CHasDeviceType T_>
		void remove_mapped_memory(const char *name, T_ &) noexcept
			{ T_::device_type::remove(name); }
	} // namespace mem_sys_impl

	template <typename ManagedMemoryType_>
	class ManagedMemorySystemBase : public IMemorySystem<void_offset_ptr>
	{
	public:

		using typename IMemorySystem<void_offset_ptr>::pointer_type;

		virtual ~ManagedMemorySystemBase()
		{
			if(!mInstanceId.empty() && mIsOwner)
				mem_sys_impl::remove_mapped_memory(mInstanceId.c_str(), mMemoryManager);
		}

		[[nodiscard]] virtual pointer_type named_allocate(const char *name, const char *typeName, szt size, szt alignment = alignof(std::max_align_t)) override
		{
			return mem_sys_impl::named_memory_map_allocate(*mMemoryManager.get_segment_manager(), name, typeName, false, size, alignment);
		}

		// Could be unsafe
		[[nodiscard]] virtual std::pair<bool, pointer_type> named_find_or_allocate(const char *name, const char *typeName, szt size, szt alignment = alignof(std::max_align_t)) override
		{
			if(const auto p = mMemoryManager.template find<std::byte>(name).first; p)
				return {true, static_cast<pointer_type>(static_cast<void *>(p))};

			return {false, named_allocate(name, typeName, size, alignment)};
		}

		[[nodiscard]] virtual pointer_type named_find(const char *name) override
		{
			return static_cast<void *>(mMemoryManager.template find<std::byte>(name).first);
		}

		virtual void named_deallocate(const char */*name*/, pointer_type p, szt /*size*/, szt /*alignment*/) override
		{
			mMemoryManager.template destroy_ptr(static_cast<std::byte *>(std::to_address(p)));
		}

		[[nodiscard]] virtual std::string_view instance_id() const noexcept override { return mInstanceId; }
		[[nodiscard]] virtual bool is_owner() const noexcept override { return mIsOwner; }
		[[nodiscard]] virtual szt max_size() const noexcept override { return mMemoryManager.get_segment_manager()->get_size(); }
		[[nodiscard]] virtual bool is_initialized() const noexcept override { return mMemoryManager.get_segment_manager(); }

	protected:
		ManagedMemoryType_ mMemoryManager;
		std::string mInstanceId;
		bool mIsOwner = true;

	private:
		[[nodiscard]] virtual pointer_type do_allocate(szt bytes, szt alignment) override
		{
			//std::cout << "do_allocate: " << std::dec << bytes << " / " << alignment << '\n';
			return mMemoryManager.allocate_aligned(bytes, alignment);
		}

		virtual void do_deallocate(pointer_type p, szt /*bytes*/, szt /*alignment*/) override
		{
			//std::cout << "do_deallocate: " << p << '\n';
			mMemoryManager.deallocate(std::to_address(p));
		}

		virtual bool do_is_equal(const IMemoryResource &) const noexcept override
		{
			return false;
		}
	};

	template <typename ManagedMemoryType_>
	class SharedMemorySystem : public ManagedMemorySystemBase<ManagedMemoryType_>
	{
	public:

		[[nodiscard]] virtual bool is_shared() const noexcept override { return true; }

		virtual void init(std::string instanceId, bool owner, szt maxMemory = 0) override
		{
			if(!this->mInstanceId.empty() && this->mIsOwner)
				mem_sys_impl::remove_mapped_memory(this->mInstanceId.c_str(), this->mMemoryManager);

			this->mInstanceId = std::move(instanceId);
			this->mIsOwner = owner;

			if(owner)
			{
				mem_sys_impl::remove_mapped_memory(this->mInstanceId.c_str(), this->mMemoryManager);

				recreate(this->mMemoryManager,
					boost::interprocess::create_only,
					this->mInstanceId.c_str(),
					maxMemory ? maxMemory : total_system_memory()
				);
			}
			else
			{
				recreate(this->mMemoryManager,
					boost::interprocess::open_only,
					this->mInstanceId.c_str()
				);
			}
		}

	};

	template <typename ManagedMemoryType_>
	class ManagedHeapMemorySystem : public ManagedMemorySystemBase<ManagedMemoryType_>
	{
	public:

		[[nodiscard]] virtual bool is_shared() const noexcept override { return false; }

		virtual void init(std::string /*instanceId*/, bool /*owner*/, szt maxMemory = 0) override
		{
			recreate(this->mMemoryManager, maxMemory);
		}

	};

	template <typename VoidPtrType_>
	class HeapMemorySystem : public IMemorySystem<VoidPtrType_>
	{
	private:
		std::pmr::memory_resource *mMr;

	public:

		using typename IMemorySystem<VoidPtrType_>::pointer_type;

		HeapMemorySystem(std::pmr::memory_resource &mr = *std::pmr::get_default_resource())
			: mMr{&mr} {}

		void resource(std::pmr::memory_resource &m) noexcept { mMr = &m; }
		[[nodiscard]] const auto &resource() const noexcept { return *mMr; }
		[[nodiscard]] auto &resource() noexcept { return *mMr; }

		[[nodiscard]] virtual pointer_type named_allocate(const char *, const char *, szt size, szt alignment = alignof(std::max_align_t)) override
		{
			return mMr->allocate(size, alignment);
		}

		[[nodiscard]] virtual std::pair<bool, pointer_type> named_find_or_allocate(const char *, const char *, szt size, szt alignment = alignof(std::max_align_t)) override
		{
			return {false, pointer_type{mMr->allocate(size, alignment)}};
		}

		[[nodiscard]] virtual pointer_type named_find(const char *) override
		{
			return {};
		}

		virtual void named_deallocate(const char */*name*/, pointer_type p, szt size, szt alignment) override
		{
			mMr->deallocate(std::to_address(p), size, alignment);
		}

		[[nodiscard]] virtual bool is_shared() const noexcept override { return false; }
		[[nodiscard]] virtual std::string_view instance_id() const noexcept override { return mInstanceId; }
		[[nodiscard]] virtual bool is_owner() const noexcept override { return mIsOwner; }
		[[nodiscard]] virtual szt max_size() const noexcept override { return std::numeric_limits<szt>::max(); }
		[[nodiscard]] virtual bool is_initialized() const noexcept override { return true; }

		virtual void init(std::string instanceId, bool /*owner*/, szt /*maxMemory*/ = 0) override
		{
			mInstanceId = std::move(instanceId);
		}

	protected:
		std::string mInstanceId;
		bool mIsOwner = true;

	private:
		[[nodiscard]] virtual pointer_type do_allocate(szt bytes, szt alignment) override
		{
			//std::cout << "do_allocate: " << std::dec << bytes << " / " << alignment << '\n';
			return mMr->allocate(bytes, alignment);
		}

		virtual void do_deallocate(pointer_type p, szt bytes, szt alignment) override
		{
			//std::cout << "do_deallocate: " << p << '\n';
			mMr->deallocate(std::to_address(p), bytes, alignment);
		}

		virtual bool do_is_equal(const IMemoryResource<VoidPtrType_> &) const noexcept override
		{
			return false;
		}
	};

}
