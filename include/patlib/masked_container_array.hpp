/*
	PatLib

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "base.hpp"
#include "memory.hpp"
#include "concepts.hpp"
#include "utility.hpp"

#include <array>
#include <type_traits>
#include <limits>
#include <cstddef>
#include <utility>
#include <climits>

#include <range/v3/numeric/accumulate.hpp>

namespace patlib
{
	// An array of containers that reserve some bits from the index as the container index.
	// T_ must be a random access container.
	template <typename T_, std::integral IndexType_ = szt, IndexType_ BranchSize_ = 4, template<typename, szt> typename InternalArrayType_ = std::array>
	struct masked_container_array
	{
		static constexpr IndexType_ BranchSize = BranchSize_;
		static constexpr IndexType_ NbBranches = IndexType_{1} << BranchSize;
		static constexpr IndexType_ MaxSize = (std::numeric_limits<IndexType_>::max() >> BranchSize) + 1;

		using internal_array_type = InternalArrayType_<T_, NbBranches>;

		using value_type = typename internal_array_type::value_type;
		using reference = typename internal_array_type::reference;
		using const_reference = typename internal_array_type::const_reference;
		using pointer = typename internal_array_type::pointer;
		using const_pointer = typename internal_array_type::const_pointer;
		using size_type = IndexType_;
		using difference_type = typename std::incrementable_traits<size_type>::difference_type;

		using iterator = typename internal_array_type::iterator;
		using const_iterator = typename internal_array_type::const_iterator;
		using reverse_iterator = typename internal_array_type::reverse_iterator;
		using const_reverse_iterator = typename internal_array_type::const_reverse_iterator;

		using index_type = split_value<size_type, BranchSize_>;

		template <typename... Args_>
		[[nodiscard]] static constexpr index_type build_index(Args_&&... args) noexcept
			{ return {PL_FWD(args)...}; }

		internal_array_type m_branches;

		[[nodiscard]] constexpr bool contains(const IndexType_ index) const noexcept
		{
			const index_type i{index};

			return i.low() < m_branches.size() && contains_key(m_branches[i.low()], i.high());
		}

		[[nodiscard]] constexpr decltype(auto) operator [] (const IndexType_ index)
		{
			const index_type i{index};
			plassert(i.low() < m_branches.size());
			return m_branches[i.low()][i.high()];
		}
		[[nodiscard]] constexpr decltype(auto) operator [] (const IndexType_ index) const
		{
			const index_type i{index};
			plassert(i.low() < m_branches.size());
			return m_branches[i.low()][i.high()];
		}

		[[nodiscard]] constexpr auto &branch(const IndexType_ index) noexcept { plassert(index < m_branches.size()); return m_branches[index]; }
		[[nodiscard]] constexpr const auto &branch(const IndexType_ index) const noexcept { plassert(index < m_branches.size()); return m_branches[index]; }

		[[nodiscard]] constexpr auto begin() noexcept { return m_branches.begin(); }
		[[nodiscard]] constexpr auto begin() const noexcept { return m_branches.begin(); }
		[[nodiscard]] constexpr auto cbegin() const noexcept { return m_branches.cbegin(); }

		[[nodiscard]] constexpr auto rbegin() noexcept { return m_branches.rbegin(); }
		[[nodiscard]] constexpr auto rbegin() const noexcept { return m_branches.rbegin(); }
		[[nodiscard]] constexpr auto crbegin() const noexcept { return m_branches.crbegin(); }

		[[nodiscard]] constexpr auto end() noexcept { return m_branches.end(); }
		[[nodiscard]] constexpr auto end() const noexcept { return m_branches.end(); }
		[[nodiscard]] constexpr auto cend() const noexcept { return m_branches.cend(); }

		[[nodiscard]] constexpr auto rend() noexcept { return m_branches.rend(); }
		[[nodiscard]] constexpr auto rend() const noexcept { return m_branches.rend(); }
		[[nodiscard]] constexpr auto crend() const noexcept { return m_branches.crend(); }

		constexpr void swap(masked_container_array &o) noexcept(std::is_nothrow_swappable_v<T_>)
		{
			for(auto &b : rgv::zip(m_branches, o))
				adl_swap(*b[size_v<0>], *b[size_v<1>]);
		}

		friend constexpr void swap(masked_container_array &i, masked_container_array &o) noexcept(std::is_nothrow_swappable_v<T_>)
			{ i.swap(o); }

		friend constexpr void tag_invoke(tag_t<append_copy>, const masked_container_array &cont, auto &into)
		{
			for(auto &b : cont)
				append_copy(b, into);
		}

		friend constexpr void tag_invoke(tag_t<append_move>, masked_container_array &cont, auto &into)
		{
			for(auto &b : cont)
				append_move(b, into);
		}

		[[nodiscard]] friend constexpr auto tag_invoke(tag_t<container_memory_capacity>, masked_container_array &v) noexcept
		{
			return rg::accumulate(v, container_memory_capacity(v.m_branches), rg::plus{},
				[](const auto &b){ return container_memory_capacity(b); }
			);
		}

	};
}
