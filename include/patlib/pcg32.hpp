/*
	PatLib

	Copyright (C)2020 Patrick Northon

	I have modified this file.

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * Tiny self-contained version of the PCG Random Number Generation for C++
 * put together from pieces of the much larger C/C++ codebase.
 * Wenzel Jakob, February 2015
 *
 * The PCG random number generator was developed by Melissa O'Neill
 * <oneill@pcg-random.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * For additional information about the PCG random number generation scheme,
 * including its license and other licensing options, visit
 *
 *     http://www.pcg-random.org
 */

#pragma once

#include "base.hpp"

#include <cstdint>
#include <cmath>
#include <algorithm>
#include <utility>

#include <immintrin.h>

namespace patlib
{
	namespace pcg32_impl
	{
		inline constexpr auto default_state = 0x853c49e6748fea9bULL;
		inline constexpr auto default_stream = 0xda3e39cb94b95bdbULL;
		inline constexpr auto mult = 0x5851f42d4c957f2dULL;
	} // namespace pcg32_impl

	/// PCG32 Pseudorandom number generator
	struct pcg32 {
		/// Initialize the pseudorandom number generator with default seed
		pcg32() noexcept  : state(pcg32_impl::default_state), inc(pcg32_impl::default_stream) {}

		/// Initialize the pseudorandom number generator with the \ref seed() function
		pcg32(std::uint64_t initstate, std::uint64_t initseq = 1u) { seed(initstate, initseq); }

		/**
		 * \brief Seed the pseudorandom number generator
		 *
		 * Specified in two parts: a state initializer and a sequence selection
		 * constant (a.k.a. stream id)
		 */
		void seed(std::uint64_t initstate, std::uint64_t initseq = 1) noexcept  {
			state = 0U;
			inc = (initseq << 1u) | 1u;
			next_uint();
			state += initstate;
			next_uint();
		}

		/// Generate a uniformly distributed unsigned 32-bit random number
		std::uint32_t next_uint() noexcept  {
			std::uint64_t oldstate = state;
			state = oldstate * pcg32_impl::mult + inc;
			std::uint32_t xorshifted = (std::uint32_t) (((oldstate >> 18u) ^ oldstate) >> 27u);
			std::uint32_t rot = (std::uint32_t) (oldstate >> 59u);
			return (xorshifted >> rot) | (xorshifted << ((~rot + 1u) & 31));
		}

		/// Generate a uniformly distributed number, r, where 0 <= r < bound
		std::uint32_t next_uint(std::uint32_t bound) noexcept  {
			// To avoid bias, we need to make the range of the RNG a multiple of
			// bound, which we do by dropping output less than a threshold.
			// A naive scheme to calculate the threshold would be to do
			//
			//     std::uint32_t threshold = 0x100000000ull % bound;
			//
			// but 64-bit div/mod is slower than 32-bit div/mod (especially on
			// 32-bit platforms).  In essence, we do
			//
			//     std::uint32_t threshold = (0x100000000ull-bound) % bound;
			//
			// because this version will calculate the same modulus, but the LHS
			// value is less than 2^32.

			std::uint32_t threshold = (~bound+1u) % bound;

			// Uniformity guarantees that this loop will terminate.  In practice, it
			// should usually terminate quickly; on average (assuming all bounds are
			// equally likely), 82.25% of the time, we can expect it to require just
			// one iteration.  In the worst case, someone passes a bound of 2^31 + 1
			// (i.e., 2147483649), which invalidates almost 50% of the range.  In
			// practice, bounds are typically small and only a tiny amount of the range
			// is eliminated.
			for (;;) {
				std::uint32_t r = next_uint();
				if (r >= threshold)
					return r % bound;
			}
		}

		/// Generate a single precision floating point value on the interval [0, 1)
		float next_float() noexcept  {
			/* Trick from MTGP: generate an uniformly distributed
				single precision number in [1,2) and subtract 1. */
			union {
				std::uint32_t u;
				float f;
			} x;
			x.u = (next_uint() >> 9) | 0x3f800000u;
			return x.f - 1.0f;
		}

		/**
		 * \brief Generate a double precision floating point value on the interval [0, 1)
		 *
		 * \remark Since the underlying random number generator produces 32 bit output,
		 * only the first 32 mantissa bits will be filled (however, the resolution is still
		 * finer than in \ref next_float(), which only uses 23 mantissa bits)
		 */
		double next_double() noexcept {
			/* Trick from MTGP: generate an uniformly distributed
				double precision number in [1,2) and subtract 1. */
			union {
				std::uint64_t u;
				double d;
			} x;
			x.u = ((std::uint64_t) next_uint() << 20) | 0x3ff0000000000000ULL;
			return x.d - 1.0;
		}

		/**
		 * \brief Multi-step advance function (jump-ahead, jump-back)
		 *
		 * The method used here is based on Brown, "Random Number Generation
		 * with Arbitrary Stride", Transactions of the American Nuclear
		 * Society (Nov. 1994). The algorithm is very similar to fast
		 * exponentiation.
		 */
		void advance(std::int64_t delta_) noexcept {
			std::uint64_t
				cur_mult = pcg32_impl::mult,
				cur_plus = inc,
				acc_mult = 1u,
				acc_plus = 0u;

			/* Even though delta is an unsigned integer, we can pass a signed
				integer to go backwards, it just goes "the long way round". */
			std::uint64_t delta = (std::uint64_t) delta_;

			while (delta > 0) {
				if (delta & 1) {
					acc_mult *= cur_mult;
					acc_plus = acc_plus * cur_mult + cur_plus;
				}
				cur_plus = (cur_mult + 1) * cur_plus;
				cur_mult *= cur_mult;
				delta /= 2;
			}
			state = acc_mult * state + acc_plus;
		}

		/**
		 * \brief Draw uniformly distributed permutation and permute the
		 * given STL container
		 *
		 * From: Knuth, TAoCP Vol. 2 (3rd 3d), Section 3.4.2
		 */
		template <typename Iterator> void shuffle(Iterator begin, Iterator end) noexcept {
			for (Iterator it = end - 1; it > begin; --it)
				std::iter_swap(it, begin + next_uint((std::uint32_t) (it - begin + 1)));
		}

		/// Compute the distance between two PCG32 pseudorandom number generators
		std::int64_t operator-(const pcg32 &other) const noexcept {
			plassert(inc == other.inc);

			std::uint64_t
				cur_mult = pcg32_impl::mult,
				cur_plus = inc,
				cur_state = other.state,
				the_bit = 1u,
				distance = 0u;

			while (state != cur_state) {
				if ((state & the_bit) != (cur_state & the_bit)) {
					cur_state = cur_state * cur_mult + cur_plus;
					distance |= the_bit;
				}
				plassert((state & the_bit) == (cur_state & the_bit));
				the_bit <<= 1;
				cur_plus = (cur_mult + 1ULL) * cur_plus;
				cur_mult *= cur_mult;
			}

			return (std::int64_t) distance;
		}

		/// Equality operator
		//bool operator==(const pcg32 &other) const { return state == other.state && inc == other.inc; }

		/// Inequality operator
		bool operator!=(const pcg32 &other) const noexcept { return state != other.state || inc != other.inc; }

		std::uint64_t state;  // RNG state.  All values are possible.
		std::uint64_t inc;    // Controls which RNG sequence (stream) is selected. Must *always* be odd.
	};


	/// 8 parallel PCG32 pseudorandom number generators
	struct pcg32_8 {

	#ifdef __AVX2__
			__m256i state[2]; // RNG state.  All values are possible.
			__m256i inc[2];   // Controls which RNG sequence (stream) is selected. Must *always* be odd.
	#else
			/* Scalar fallback */
			pcg32 rng[8];
	#endif

			/// Initialize the pseudorandom number generator with default seed
			pcg32_8() noexcept {
					alignas(32) std::uint64_t initstate[8] = {
							pcg32_impl::default_state, pcg32_impl::default_state,
							pcg32_impl::default_state, pcg32_impl::default_state,
							pcg32_impl::default_state, pcg32_impl::default_state,
							pcg32_impl::default_state, pcg32_impl::default_state
					};

					alignas(32) std::uint64_t initseq[8] =
							{ 1, 2, 3, 4, 5, 6, 7, 8 };

					seed(initstate, initseq);
			}

			/// Initialize the pseudorandom number generator with the \ref seed() function
			pcg32_8(const std::uint64_t initstate[8], const std::uint64_t initseq[8]) noexcept {
					seed(initstate, initseq);
			}


	#ifdef __AVX2__
			/**
			 * \brief Seed the pseudorandom number generator
			 *
			 * Specified in two parts: a state initializer and a sequence selection
			 * constant (a.k.a. stream id)
			 */
			void seed(const std::uint64_t initstate[8], const std::uint64_t initseq[8]) noexcept {
					const __m256i one = _mm256_set1_epi64x((long long) 1);

					state[0] = state[1] = _mm256_setzero_si256();
					inc[0] = _mm256_or_si256(
							_mm256_slli_epi64(_mm256_load_si256((__m256i *) &initseq[0]), 1),
							one);
					inc[1] = _mm256_or_si256(
							_mm256_slli_epi64(_mm256_load_si256((__m256i *) &initseq[4]), 1),
							one);
					step();

					state[0] = _mm256_add_epi64(state[0], _mm256_load_si256((__m256i *) &initstate[0]));
					state[1] = _mm256_add_epi64(state[1], _mm256_load_si256((__m256i *) &initstate[4]));

					step();
			}

			/// Generate 8 uniformly distributed unsigned 32-bit random numbers
			void next_uint(std::uint32_t result[8]) noexcept {
					_mm256_store_si256((__m256i *) result, step());
			}

			/// Generate 8 uniformly distributed unsigned 32-bit random numbers
			__m256i next_uint() noexcept {
					return step();
			}

			/// Generate eight single precision floating point value on the interval [0, 1)
			__m256 next_float() noexcept {
					/* Trick from MTGP: generate an uniformly distributed
						single precision number in [1,2) and subtract 1. */

					const __m256i const1 = _mm256_set1_epi32((int) 0x3f800000u);

					__m256i value = step();
					__m256i fltval = _mm256_or_si256(_mm256_srli_epi32(value, 9), const1);

					return _mm256_sub_ps(_mm256_castsi256_ps(fltval),
															_mm256_castsi256_ps(const1));
			}

			/// Generate eight single precision floating point value on the interval [0, 1)
			void next_float(float result[8]) noexcept {
					_mm256_store_ps(result, next_float());
			}

			/**
			 * \brief Generate eight double precision floating point value on the interval [0, 1)
			 *
			 * \remark Since the underlying random number generator produces 32 bit output,
			 * only the first 32 mantissa bits will be filled (however, the resolution is still
			 * finer than in \ref next_float(), which only uses 23 mantissa bits)
			 */
			auto next_double() noexcept {
					/* Trick from MTGP: generate an uniformly distributed
						double precision number in [1,2) and subtract 1. */

					const __m256i const1 =
							_mm256_set1_epi64x((long long) 0x3ff0000000000000ull);

					__m256i value = step();

					__m256i lo = _mm256_cvtepu32_epi64(_mm256_castsi256_si128(value));
					__m256i hi = _mm256_cvtepu32_epi64(_mm256_extractf128_si256(value, 1));

					__m256i tlo = _mm256_or_si256(_mm256_slli_epi64(lo, 20), const1);
					__m256i thi = _mm256_or_si256(_mm256_slli_epi64(hi, 20), const1);

					__m256d flo = _mm256_sub_pd(_mm256_castsi256_pd(tlo),
																			_mm256_castsi256_pd(const1));

					__m256d fhi = _mm256_sub_pd(_mm256_castsi256_pd(thi),
																			_mm256_castsi256_pd(const1));

					return std::make_pair(flo, fhi);
			}

			/**
			 * \brief Generate eight double precision floating point value on the interval [0, 1)
			 *
			 * \remark Since the underlying random number generator produces 32 bit output,
			 * only the first 32 mantissa bits will be filled (however, the resolution is still
			 * finer than in \ref next_float(), which only uses 23 mantissa bits)
			 */
			void next_double(double result[8]) noexcept {
					const auto value = next_double();

					_mm256_store_pd(&result[0], value.first);
					_mm256_store_pd(&result[4], value.second);
			}

	private:
			__m256i step() noexcept {
					const __m256i pcg32_mult_l = _mm256_set1_epi64x((long long) (pcg32_impl::mult & 0xffffffffu));
					const __m256i pcg32_mult_h = _mm256_set1_epi64x((long long) (pcg32_impl::mult >> 32));
					const __m256i mask_l       = _mm256_set1_epi64x((long long) 0x00000000ffffffffull);
					const __m256i shift0       = _mm256_set_epi32(7, 7, 7, 7, 6, 4, 2, 0);
					const __m256i shift1       = _mm256_set_epi32(6, 4, 2, 0, 7, 7, 7, 7);
					const __m256i const32      = _mm256_set1_epi32(32);

					__m256i s0 = state[0], s1 = state[1];

					/* Extract low and high words for partial products below */
					__m256i s0_l = _mm256_and_si256(s0, mask_l);
					__m256i s0_h = _mm256_srli_epi64(s0, 32);
					__m256i s1_l = _mm256_and_si256(s1, mask_l);
					__m256i s1_h = _mm256_srli_epi64(s1, 32);

					/* Improve high bits using xorshift step */
					__m256i s0s   = _mm256_srli_epi64(s0, 18);
					__m256i s1s   = _mm256_srli_epi64(s1, 18);

					__m256i s0x   = _mm256_xor_si256(s0s, s0);
					__m256i s1x   = _mm256_xor_si256(s1s, s1);

					__m256i s0xs  = _mm256_srli_epi64(s0x, 27);
					__m256i s1xs  = _mm256_srli_epi64(s1x, 27);

					__m256i xors0 = _mm256_and_si256(mask_l, s0xs);
					__m256i xors1 = _mm256_and_si256(mask_l, s1xs);

					/* Use high bits to choose a bit-level rotation */
					__m256i rot0  = _mm256_srli_epi64(s0, 59);
					__m256i rot1  = _mm256_srli_epi64(s1, 59);

					/* 64 bit multiplication using 32 bit partial products :( */
					__m256i m0_hl = _mm256_mul_epu32(s0_h, pcg32_mult_l);
					__m256i m1_hl = _mm256_mul_epu32(s1_h, pcg32_mult_l);
					__m256i m0_lh = _mm256_mul_epu32(s0_l, pcg32_mult_h);
					__m256i m1_lh = _mm256_mul_epu32(s1_l, pcg32_mult_h);

					/* Assemble lower 32 bits, will be merged into one 256 bit vector below */
					xors0 = _mm256_permutevar8x32_epi32(xors0, shift0);
					rot0  = _mm256_permutevar8x32_epi32(rot0, shift0);
					xors1 = _mm256_permutevar8x32_epi32(xors1, shift1);
					rot1  = _mm256_permutevar8x32_epi32(rot1, shift1);

					/* Continue with partial products */
					__m256i m0_ll = _mm256_mul_epu32(s0_l, pcg32_mult_l);
					__m256i m1_ll = _mm256_mul_epu32(s1_l, pcg32_mult_l);

					__m256i m0h   = _mm256_add_epi64(m0_hl, m0_lh);
					__m256i m1h   = _mm256_add_epi64(m1_hl, m1_lh);

					__m256i m0hs  = _mm256_slli_epi64(m0h, 32);
					__m256i m1hs  = _mm256_slli_epi64(m1h, 32);

					__m256i s0n   = _mm256_add_epi64(m0hs, m0_ll);
					__m256i s1n   = _mm256_add_epi64(m1hs, m1_ll);

					__m256i xors  = _mm256_or_si256(xors0, xors1);
					__m256i rot   = _mm256_or_si256(rot0, rot1);

					state[0] = _mm256_add_epi64(s0n, inc[0]);
					state[1] = _mm256_add_epi64(s1n, inc[1]);

					/* Finally, rotate and return the result */
					__m256i result = _mm256_or_si256(
							_mm256_srlv_epi32(xors, rot),
							_mm256_sllv_epi32(xors, _mm256_sub_epi32(const32, rot))
					);

					return result;
			}
	#else
			/**
			 * \brief Seed the pseudorandom number generator
			 *
			 * Specified in two parts: a state initializer and a sequence selection
			 * constant (a.k.a. stream id)
			 */
			void seed(const std::uint64_t initstate[8], const std::uint64_t initseq[8]) noexcept {
					for (int i = 0; i < 8; ++i)
							rng[i].seed(initstate[i], initseq[i]);
			}

			/// Generate 8 uniformly distributed unsigned 32-bit random numbers
			void next_uint(std::uint32_t result[8]) noexcept {
					for (int i = 0; i < 8; ++i)
							result[i] = rng[i].next_uint();
			}

			/// Generate eight single precision floating point value on the interval [0, 1)
			void next_float(float result[8]) noexcept {
					for (int i = 0; i < 8; ++i)
							result[i] = rng[i].next_float();
			}

			/**
			 * \brief Generate eight double precision floating point value on the interval [0, 1)
			 *
			 * \remark Since the underlying random number generator produces 32 bit output,
			 * only the first 32 mantissa bits will be filled (however, the resolution is still
			 * finer than in \ref next_float(), which only uses 23 mantissa bits)
			 */
			void next_double(double result[8]) noexcept {
					for (int i = 0; i < 8; ++i)
							result[i] = rng[i].next_double();
			}
	#endif
	};
}
