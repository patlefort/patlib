/*
	PatLib

	Copyright (C) 2021 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "base.hpp"
#include "type.hpp"
#include "algorithm.hpp"

#include <variant>
#include <functional>

namespace patlib
{
	constexpr decltype(auto) visit_one(auto&& visited, auto&& fc)
		{ return std::visit(PL_FWD(fc), PL_FWD(visited)); }

	template <typename R_>
	constexpr R_ visit_one(auto&& visited, auto&& fc)
		{ return std::visit(PL_FWD(fc), PL_FWD(visited)); }

	template <typename T_, typename FC_>
	concept CVisitable = requires(T_ o, FC_ fc) { { std::visit(fc, o) }; };

	constexpr decltype(auto) try_visit(auto&& v, auto&& fc)
	{
		if constexpr(CVisitable<decltype(v), decltype(fc)>)
			return visit_one(PL_FWD(v), PL_FWD(fc));
		else
			return std::invoke(PL_FWD(fc), PL_FWD(v));
	}

	constexpr decltype(auto) visit_tuple(auto&& tpl, auto&& fc) noexcept
	{
		const auto visit_arg = [&fc](auto&& recurse, auto&& last, auto&& cur, auto&&... rest) -> decltype(auto)
			{
				return try_visit(PL_FWD(cur), [&](auto&& v) -> decltype(auto) {
					auto newTpl = tuple_add_capture(PL_FWD(last), PL_FWD(v));

					if constexpr(sizeof...(rest))
						return PL_FWD(recurse)(PL_FWD(recurse), newTpl, PL_FWD(rest)...);
					else
						return std::apply(PL_FWD(fc), newTpl);
				});
			};

		return std::apply([&](auto&&... args) -> decltype(auto) {
			return visit_arg(visit_arg, std::make_tuple(), PL_FWD(args)...);
		}, PL_FWD(tpl));
	}

	[[nodiscard]] constexpr auto applier_visitor(auto&& fc) noexcept
	{
		return [fc = PL_FWD(fc)](auto&& tpl) -> decltype(auto) { return visit_tuple(PL_FWD(tpl), fc); };
	}
}
