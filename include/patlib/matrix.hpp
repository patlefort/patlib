/*
	PatLib

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "base.hpp"
#include "math.hpp"
#include "io.hpp"
#include "type.hpp"
#include "exception.hpp"
#include "marked_optional.hpp"
#include "simd/algorithm.hpp"
#include "simd/matrix.hpp"

#include <array>
#include <initializer_list>

#include <range/v3/numeric/accumulate.hpp>

namespace patlib
{
	template <typename T_>
	struct normalize_point_p : public param<T_> { using param<T_>::param; };
		template <typename T_> normalize_point_p(T_) -> normalize_point_p<T_>;
	template <typename T_>
	struct no_transpose_p : public param<T_> { using param<T_>::param; };
		template <typename T_> no_transpose_p(T_) -> no_transpose_p<T_>;


	template <typename T_>
	concept CMatrix4x4 =
		requires
		{
			typename T_::value_type;

			requires simd::CNeutralGeometricVector<typename T_::value_type>;
			requires T_::value_type::NbValues >= 4;
		} &&
		requires(T_ m, const T_ cm, typename T_::value_type v1,
			typename T_::value_type::value_type r1, szt i, const no_transpose_p<T_> mnotranspose)
		{
			{ T_{} } noexcept;
			{ T_{tag::identity} } noexcept;
			{ T_{std::array<typename T_::value_type, 4>{}} } noexcept;
			{ T_{v1, v1, v1, v1} } noexcept;
			{ T_{
				r1, r1, r1, r1,
				r1, r1, r1, r1,
				r1, r1, r1, r1,
				r1, r1, r1, r1
			} } noexcept;

			{ m[i] } noexcept -> std::same_as<typename T_::value_type &>;
			{ cm[i] } noexcept -> std::same_as<const typename T_::value_type &>;

			{ m.set_identity() } noexcept;
			{ m.set_zero() } noexcept;
			{ cm.transpose() } noexcept -> std::same_as<T_>;
			{ m.transpose_inplace() } noexcept;
			{ m.set_rotation_x(r1) } noexcept;
			{ m.set_rotation_y(r1) } noexcept;
			{ m.set_rotation_z(r1) } noexcept;
			{ cm.determinant() } noexcept -> std::convertible_to<typename T_::value_type::value_type>;
			{ cm.adjoint() } noexcept -> std::same_as<T_>;
			{ cm.inverse() } noexcept -> std::same_as<marked_optional<T_>>;

			{ cm + cm } noexcept -> std::same_as<T_>;
			{ cm - cm } noexcept -> std::same_as<T_>;
			{ cm * cm } noexcept -> std::same_as<T_>;
			{ cm * mnotranspose } noexcept -> std::same_as<T_>;
			{ cm / cm } noexcept -> std::same_as<T_>;
			{ m += cm } noexcept -> std::same_as<T_&>;
			{ m -= cm } noexcept -> std::same_as<T_&>;
			{ m *= cm } noexcept -> std::same_as<T_&>;
			{ m *= mnotranspose } noexcept -> std::same_as<T_&>;
			{ m /= cm } noexcept -> std::same_as<T_&>;

			{ cm == cm } noexcept -> std::same_as<bool>;
			{ cm != cm } noexcept -> std::same_as<bool>;

			{ cm.is_nan() } noexcept -> std::same_as<bool>;
			{ cm.is_infinite() } noexcept -> std::same_as<bool>;
			{ cm.is_valid() } noexcept -> std::same_as<bool>;
			{ cm.is_identity() } noexcept -> std::same_as<bool>;
		};

	namespace matrix_impl
	{
		template <typename T_, typename VT_>
		concept CMatrixTransformPoint =
			requires(const T_ cm, T_ m, const VT_ v1, const normalize_point_p<VT_> vnorm)
			{
				{ cm * v1 } noexcept -> std::same_as<VT_>;
				{ cm * vnorm } noexcept -> std::same_as<VT_>;
			};

		template <typename T_, typename VT_>
		concept CMatrixTransformOther =
			requires(const T_ cm, T_ m, const VT_ v1)
			{
				{ cm * v1 } noexcept -> std::same_as<VT_>;
			};
	} // namespace impl

	template <typename T_, typename... VT_>
	concept CMatrix4x4Transform = CMatrix4x4<T_> && (... && matrix_impl::CMatrixTransformOther<T_, VT_>);

	template <typename T_, typename... VT_>
	concept CMatrix4x4TransformPoint = CMatrix4x4<T_> && (... && matrix_impl::CMatrixTransformPoint<T_, VT_>);

	template <typename T_, typename PT_, typename DirT_>
	concept CMatrix4x4TransformPointAndDir = CMatrix4x4TransformPoint<T_, PT_> && CMatrix4x4Transform<T_, DirT_>;

	// Column-major matrix
	template <simd::CNeutralGeometricVector VectorType_>
		requires (VectorType_::NbValues >= 4)
	class matrix4x4
	{
	private:
		using real_type = typename VectorType_::value_type;

	public:

		using value_type = VectorType_;

		matrix4x4() = default;
		matrix4x4(std::initializer_list<tag::zero_t>) noexcept : v{} {}
		matrix4x4(const tag::zero_t) noexcept { set_zero(); }
		matrix4x4(const tag::identity_t) noexcept { set_identity(); }
		matrix4x4(const std::array<VectorType_, 4> &vo) noexcept : v{vo} {}

		template <typename Type_, typename Impl_>
		matrix4x4(const std::array<simd::geometric_vector<Type_, size_c<4>, Impl_>, 4> &vo) noexcept : v{vo[0], vo[1], vo[2], vo[3]} {}

		template <typename VT_>
		explicit matrix4x4(const matrix4x4<VT_> &vo) noexcept : v{vo[0], vo[1], vo[2], vo[3]} {}

		matrix4x4(const VectorType_ &av0, const VectorType_ &av1, const VectorType_ &av2, const VectorType_ &av3) noexcept : v{av0, av1, av2, av3} {}
		matrix4x4(
				const real_type v00, const real_type v01, const real_type v02, const real_type v03,
				const real_type v10, const real_type v11, const real_type v12, const real_type v13,
				const real_type v20, const real_type v21, const real_type v22, const real_type v23,
				const real_type v30, const real_type v31, const real_type v32, const real_type v33) noexcept :
			v{
				VectorType_{v00, v01, v02, v03},
				VectorType_{v10, v11, v12, v13},
				VectorType_{v20, v21, v22, v23},
				VectorType_{v30, v31, v32, v33}
			}
		{}

		std::array<VectorType_, 4> v;

		/*template <simd::CNeutralGeometricVector OVT_>
		[[nodiscard]] explicit operator matrix4x4<OVT_>() const noexcept
			{ return matrix4x4<OVT_>{static_cast<OVT_>(v[0]), static_cast<OVT_>(v[1]), static_cast<OVT_>(v[2]), static_cast<OVT_>(v[3])}; }*/

		[[nodiscard]] auto &operator[] (const szt i) noexcept { return v[i]; }
		[[nodiscard]] const auto &operator[] (const szt i) const noexcept { return v[i]; }

		void set_identity() noexcept
		{
			v = {
				VectorType_{1, 0, 0, 0},
				VectorType_{0, 1, 0, 0},
				VectorType_{0, 0, 1, 0},
				VectorType_{tag::identity}
			};
		}

		void set_zero() noexcept
		{
			v[0].set_zero();
			v[1].set_zero();
			v[2].set_zero();
			v[3].set_zero();
		}

		[[nodiscard]] matrix4x4 transpose() const noexcept
		{
			return {simd::matrix_impl::matrix_transpose(v)};
		}

		void transpose_inplace() noexcept
			{ v = simd::matrix_impl::matrix_transpose(v); }

		void set_rotation_x(const real_type angle) noexcept
		{
			const auto cx = angle /ocos;
			const auto sx = angle /osin;

			v[0] = {   1,   0,   0,   0};
			v[1] = {   0,  cx, -sx,   0};
			v[2] = {   0,  sx,  cx,   0};
			v[3] = {   0,   0,   0,   1};
		}

		void set_rotation_y(const real_type angle) noexcept
		{
			const auto cy = angle /ocos;
			const auto sy = angle /osin;

			v[0] = {  cy,   0,  sy,   0};
			v[1] = {   0,   1,   0,   0};
			v[2] = { -sy,   0,  cy,   0};
			v[3] = {   0,   0,   0,   1};
		}

		void set_rotation_z(const real_type angle) noexcept
		{
			const auto cz = angle /ocos;
			const auto sz = angle /osin;

			v[0] = {  cz, -sz,   0,   0};
			v[1] = {  sz,  cz,   0,   0};
			v[2] = {   0,   0,   1,   0};
			v[3] = {   0,   0,   0,   1};
		}

		[[nodiscard]] real_type determinant() const noexcept
			{ return simd::matrix_impl::matrix_determinant(v); }

		[[nodiscard]] matrix4x4 adjoint() const noexcept
			{ return {simd::matrix_impl::matrix_adjoint(v)}; }

		template <typename OVT_ = VectorType_>
		[[nodiscard]] marked_optional<matrix4x4<OVT_>> inverse() const noexcept
		{
			const auto det = determinant();

			if(!det)
				return nullopt;

			marked_optional<matrix4x4<OVT_>> adj{adjoint()};

			for(const VectorType_ detVec {1 / det}; auto &v : adj->v)
				v *= detVec;

			return adj;
		}

		[[nodiscard]] matrix4x4 operator+ (const matrix4x4 &m) const noexcept
			{ return {v[0] + m[0], v[1] + m[1], v[2] + m[2], v[3] + m[3]}; }

		[[nodiscard]] matrix4x4 operator- (const matrix4x4 &m) const noexcept
			{ return {v[0] - m[0], v[1] - m[1], v[2] - m[2], v[3] - m[3]}; }

		[[nodiscard]] matrix4x4 operator/ (const matrix4x4 &m) const noexcept
			{ return {v[0] / m[0], v[1] / m[1], v[2] / m[2], v[3] / m[3]}; }

		[[nodiscard]] matrix4x4 operator* (const matrix4x4 &m) const noexcept
		{
			return simd::matrix_impl::matrix_mul(transpose().v, m.v);
		}

		[[nodiscard]] matrix4x4 operator* (const no_transpose_p<matrix4x4> &m) const noexcept
		{
			return simd::matrix_impl::matrix_mul(v, m->v);
		}

		auto &operator+= (const matrix4x4 &m) noexcept
		{
			*this = *this + m;
			return *this;
		}

		auto &operator-= (const matrix4x4 &m) noexcept
		{
			*this = *this - m;
			return *this;
		}

		auto &operator*= (const matrix4x4 &m) noexcept
		{
			transpose_inplace();

			return (*this = simd::matrix_impl::matrix_mul(v, m.v));
		}

		auto &operator*= (const no_transpose_p<matrix4x4> &m) noexcept
		{
			return (*this = simd::matrix_impl::matrix_mul(v, m->v));
		}

		auto &operator/= (const matrix4x4 &m) noexcept
		{
			*this = *this / m;
			return *this;
		}

		template <simd::CVector VT_>
		[[nodiscard]] auto operator* (const VT_ &v2) const noexcept
			{ return static_cast<VT_>( simd::matrix_impl::matrix_mul(v, static_cast<VectorType_>(v2)) ); }

		template <simd::CNeutralOrPointGeometricVector VT_>
		[[nodiscard]] auto operator* (const normalize_point_p<VT_> &v2) const noexcept
			{ return static_cast<VT_>( simd::matrix_impl::matrix_mul(v, static_cast<VectorType_>(*v2)).normalize_point() ); }

		[[nodiscard]] bool operator== (const matrix4x4 &m) const noexcept
		{
			return rg::all_of(
					rgv::zip(v, m.v),
					applier([](const auto &ls, const auto &rs){ return bool(ls == rs); })
				);
		}

		[[nodiscard]] bool is_nan() const noexcept
			{ return rg::any_of(v, [](const auto &v){ return mask_any(v/oisnan); }); }
		[[nodiscard]] bool is_infinite() const noexcept
			{ return rg::any_of(v, [](const auto &v){ return mask_any(v/oisinf); }); }
		[[nodiscard]] bool is_valid() const noexcept { return !is_nan() && !is_infinite(); }
		[[nodiscard]] bool is_identity() const noexcept { return *this == matrix4x4{tag::identity}; }

		template <typename CharT_, typename Traits_>
		friend auto &operator<<(std::basic_ostream<CharT_, Traits_> &os, const matrix4x4 &matrix)
			{ return (os << io::range{io::mode_select, '\n', rgv::all(matrix.v)}); }

		template <typename CharT_, typename Traits_>
		friend auto &operator>>(std::basic_istream<CharT_, Traits_> &is, matrix4x4 &matrix)
			{ return (is >> io::range{io::mode_select, '\n', rgv::all(matrix.v)}); }

		[[nodiscard]] friend bool isnan(const matrix4x4 &v) noexcept
			{ return v.is_nan(); }

		[[nodiscard]] friend bool isinf(const matrix4x4 &v) noexcept
			{ return v.is_infinite(); }

		// For marked_optional default policy.
		static inline auto constexpr MarkedOptionalInitialValue = tag::nan;
		constexpr void reset() noexcept
			{ v[0].set_nan(); }
		[[nodiscard]] constexpr bool has_value() const noexcept
			{ return mask_none(v[0] /oisnan); }
	};

	template <CMatrix4x4 T_>
	struct [[nodiscard]] scoped_transpose : no_copy
	{
		T_ &mMatrix;

		constexpr scoped_transpose(T_ &m) noexcept : mMatrix{m}
			{ mMatrix.transpose_inplace(); }

		~scoped_transpose()
			{ mMatrix.transpose_inplace(); }
	};

	[[nodiscard]] constexpr auto try_inverse(const CMatrix4x4 auto &matrix, std::string_view failureMessage = "Failed to inverse matrix.")
	{
		const auto mInv = matrix.inverse();
		if(!mInv)
			throw Exception(failureMessage);

		return *mInv;
	}
}
