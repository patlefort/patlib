/*
	PatLib

	Copyright (C) 2020 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "base.hpp"
#include "utility.hpp"
#include "iterator.hpp"
#include "pointer.hpp"

#include <range/v3/all.hpp>
//#include <ranges>

// Work around problems with diffmax_t not being compatible with OpenMP or std::ranges.
namespace ranges::detail
{
	template<std::size_t N>
	struct promote_as_signed_<N, std::enable_if_t<(N == 64)>>
	{
		#if defined(__SIZEOF_INT128__) && __SIZEOF_INT128__
			using difference_type = __int128;
		#else
			using difference_type = std::int64_t;
		#endif
	};
} // namespace ranges::detail

namespace patlib
{
	template <typename FC_, typename... I_>
	concept CIndirectlyInvocable = std::invocable<FC_, rg::iter_reference_t<I_>...>;

	template <typename T_>
	concept CForwardValueRange = rg::input_range<T_> && CForwardValueIterator<rg::iterator_t<T_>>;
	template <typename T_>
	concept CBidirectionalValueRange = CForwardValueRange<T_> && CBidirectionalValueIterator<rg::iterator_t<T_>>;
	template <typename T_>
	concept CRandomAccessValueRange = CBidirectionalValueRange<T_> && CRandomAccessValueIterator<rg::iterator_t<T_>>;
	template <typename T_, typename ValueType_>
	concept COutputValueRange = rg::range<T_> && COutputValueIterator<rg::iterator_t<T_>, ValueType_>;

	template <typename T_, typename OT_>
	concept CInputAndOutputRange = rg::input_range<T_> && rg::output_range<T_, OT_>;

	namespace impl
	{
		[[nodiscard]] constexpr auto as_const_view_default(rg::input_range auto &v) noexcept
			{ return v | rgv::const_; }
	} // namespace impl

	template <typename... Ts_>
	[[nodiscard]] constexpr auto find_view(rg::input_range auto&& r, auto&& v, Ts_&&... args) noexcept
	{
		auto it = rg::find(PL_FWD(r), PL_FWD(v));

		return rgv::single(it)
			| rgv::remove_if([end = PL_FWD(r).end()](auto it){ return it == end; })
			| rgv::transform([&r, ...args = PL_FWD(args)](auto it){ return std::make_tuple(std::ref(PL_FWD(r)), it, std::forward<Ts_>(args)...); });
	}

	[[nodiscard]] constexpr auto scaled_view(auto&& scale) noexcept
	{
		return rgv::transform([scale = PL_FWD(scale)](auto&& v) -> decltype(auto) { return PL_FWD(v) * scale; });
	}

	[[nodiscard]] constexpr auto add_view(auto&& add) noexcept
	{
		return rgv::transform([add = PL_FWD(add)](auto&& v) -> decltype(auto) { return PL_FWD(v) + add; });
	}

	template <typename T_>
	inline constexpr auto static_cast_view = rgv::transform([](auto&& v) -> decltype(auto) { return static_cast<T_>(v); });

	template <typename T_>
	inline constexpr auto dynamic_cast_view = rgv::transform([](auto&& v) -> decltype(auto) { return dynamic_cast<T_>(v); });

	template <typename T_>
	inline constexpr auto reinterpret_cast_view = rgv::transform([](auto&& v) -> decltype(auto) { return reinterpret_cast<T_>(v); });

	template <typename T_>
	inline constexpr auto type_view = rgv::transform([](auto&& v) -> decltype(auto) { return T_{v}; });

	inline namespace loop
	{
		template <typename T_ = if32>
		[[nodiscard]] constexpr auto mir(const T_ i1, const T_ i2) noexcept
			{ return rgv::indices(i1, i2); }

		template <typename T_ = if32>
		[[nodiscard]] constexpr auto mir(const T_ i2) noexcept
			{ return rgv::indices(i2); }
	} // namespace loop

	inline constexpr auto iterator_view = pipe([](rg::input_range auto&& container)
	{
		return rg::make_subrange(
			map_iterator_iterator{std::begin(PL_FWD(container))},
			map_iterator_iterator{std::end(PL_FWD(container))}
		);
	});

	inline constexpr auto pair_view = rgv::transform([](auto&& i){ return std::forward_as_tuple(i.first, i.second); });

	inline constexpr auto map_pair_to_range = pipe(
		[]<rg::input_or_output_iterator ITBegin_, rg::input_or_output_iterator ITEnd_>
		(std::pair<ITBegin_, ITEnd_> p)
		{ return rg::make_subrange(p.first, p.second); }
	);

	inline constexpr auto to_ref_view = rgv::transform([](auto&& i) -> decltype(auto) { return to_ref(PL_FWD(i)); });

	template <rg::bidirectional_range Range_>
	constexpr void exception_safe_loop(
		Range_ &&r,
		std::invocable<rg::range_reference_t<Range_>> auto&& op,
		std::invocable<rg::range_reference_t<Range_>> auto&& op_on_failure)
	{
		for(auto it = std::begin(PL_FWD(r)), end = std::end(PL_FWD(r)); it!=end; ++it)
		{
			try
			{
				std::invoke(op, PL_FWD(*it));
			}
			catch(...)
			{
				for(auto it2 = std::make_reverse_iterator(it), end2 = std::make_reverse_iterator(std::begin(PL_FWD(r))); it2!=end2; ++it2)
					std::invoke(op_on_failure, PL_FWD(*it2));

				throw;
			}
		}
	}


}
