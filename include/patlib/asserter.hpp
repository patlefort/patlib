/*
	PatLib

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "base.hpp"
#include "type.hpp"
#include "utility.hpp"
#include "math.hpp"
#include "range.hpp"
#include "io.hpp"
#include "concepts.hpp"

#include <type_traits>
#include <utility>
#include <cmath>

namespace patlib
{
	template <typename T_, typename ValueType_>
	concept CAsserter = CDefaultConstructible<T_> && std::invocable<T_, ValueType_>;

	namespace valid
	{
		inline constexpr auto not_nan = [](const auto &v) { plassert( mask_none(plisnan(v)), "(NaN)" ); };
		inline constexpr auto not_inf = [](const auto &v) { plassert( mask_none(plisinf(v)), "(Infinite)" ); };
		inline constexpr auto not_negative = [](const auto &v) { plassert( mask_none(v < 0), "(Negative)" ); };
		inline constexpr auto not_positive = [](const auto &v) { plassert( mask_none(v > 0), "(Positive)" ); };
		inline constexpr auto not_above_one = [](const auto &v) { plassert( mask_none(v > 1), "(>1)" ); };
		inline constexpr auto not_zero = [](const auto &v) { plassert( mask_none(v == 0), "(==0)" ); };
		inline constexpr auto not_one = [](const auto &v) { plassert( mask_none(v == 1), "(==1)" ); };
		inline constexpr auto not_denormalized = [](const auto &v) { plassert( std::fpclassify(v) != FP_SUBNORMAL, "(Denormalized)" ); };
		inline constexpr auto normal_fp = [](const auto &v) { plassert( std::fpclassify(v) == FP_NORMAL, "(Not normal)" ); };
		inline constexpr auto not_null = [](const auto &v) { plassert( v != nullptr, "(Null)" ); };

	} // namespace valid

	template <typename T_, CAsserter<T_>... Asserters_>
	class asserter
	{
	private:
		T_ mValue;

		constexpr void assert_impl() const
		{
			( Asserters_{}(mValue), ... );
		}

	public:

		using value_type = T_;

		template <typename... Args_>
		constexpr asserter(Args_&&... args) noexcept(noexcept(CNoThrowContructible<T_, Args_...>) && noexcept(assert_impl()))
			: mValue{PL_FWD(args)...}
		{
			assert_impl();
		}

		template <typename OT_>
		constexpr asserter &operator=(OT_ &&o) noexcept(noexcept(mValue = PL_FWD(o)) && noexcept(assert_impl()))
		{
			mValue = PL_FWD(o);
			assert_impl();
			return *this;
		}

		[[nodiscard]] constexpr operator T_() const noexcept { return mValue; }

		[[nodiscard]] constexpr const auto &get() const noexcept { return mValue; }

		constexpr auto &operator++ () requires requires(T_ lv) { {++lv}; }
			{ ++mValue; assert_impl(); return *this; }
		constexpr auto &operator-- () requires requires(T_ lv) { {--lv}; }
			{ --mValue; assert_impl(); return *this; }
		constexpr auto operator++ (int) requires requires(T_ lv) { {lv++} -> std::convertible_to<T_>; }
			{ asserter nv{mValue++}; assert_impl(); return nv; }
		constexpr auto operator-- (int) requires requires(T_ lv) { {lv--} -> std::convertible_to<T_>; }
			{ asserter nv{mValue--}; assert_impl(); return nv; }

		constexpr auto &operator+= (const T_ &o) requires requires(T_ lv, const T_ rv) { {lv += rv}; }
			{ mValue += o; assert_impl(); return *this; }
		constexpr auto &operator-= (const T_ &o) requires requires(T_ lv, const T_ rv) { {lv -= rv}; }
			{ mValue -= o; assert_impl(); return *this; }
		constexpr auto &operator*= (const T_ &o) requires requires(T_ lv, const T_ rv) { {lv *= rv}; }
			{ mValue *= o; assert_impl(); return *this; }
		constexpr auto &operator/= (const T_ &o) requires requires(T_ lv, const T_ rv) { {lv /= rv}; }
			{ mValue /= o; assert_impl(); return *this; }
		constexpr auto &operator&= (const T_ &o) requires requires(T_ lv, const T_ rv) { {lv &= rv}; }
			{ mValue &= o; assert_impl(); return *this; }
		constexpr auto &operator|= (const T_ &o) requires requires(T_ lv, const T_ rv) { {lv |= rv}; }
			{ mValue |= o; assert_impl(); return *this; }
		constexpr auto &operator^= (const T_ &o) requires requires(T_ lv, const T_ rv) { {lv ^= rv}; }
			{ mValue ^= o; assert_impl(); return *this; }
		constexpr auto &operator<<= (const T_ &o) requires requires(T_ lv, const T_ rv) { {lv <<= rv}; }
			{ mValue <<= o; assert_impl(); return *this; }
		constexpr auto &operator>>= (const T_ &o) requires requires(T_ lv, const T_ rv) { {lv >>= rv}; }
			{ mValue >>= o; assert_impl(); return *this; }

		constexpr auto &operator<<= (const auto &o)
			{ mValue <<= o; assert_impl(); return *this; }
		constexpr auto &operator>>= (const auto &o)
			{ mValue >>= o; assert_impl(); return *this; }

		constexpr auto operator+ (const asserter &rs) requires requires(const T_ lv, const T_ rv) { {lv + rv} -> std::convertible_to<T_>; }
			{ return asserter{mValue + rs.mValue}; }
		constexpr auto operator- (const asserter &rs) requires requires(const T_ lv, const T_ rv) { {lv - rv} -> std::convertible_to<T_>; }
			{ return asserter{mValue - rs.mValue}; }
		constexpr auto operator* (const asserter &rs) requires requires(const T_ lv, const T_ rv) { {lv * rv} -> std::convertible_to<T_>; }
			{ return asserter{mValue * rs.mValue}; }
		constexpr auto operator/ (const asserter &rs) requires requires(const T_ lv, const T_ rv) { {lv / rv} -> std::convertible_to<T_>; }
			{ return asserter{mValue / rs.mValue}; }
		constexpr auto operator& (const asserter &rs) requires requires(const T_ lv, const T_ rv) { {lv & rv} -> std::convertible_to<T_>; }
			{ return asserter{mValue & rs.mValue}; }
		constexpr auto operator| (const asserter &rs) requires requires(const T_ lv, const T_ rv) { {lv | rv} -> std::convertible_to<T_>; }
			{ return asserter{mValue | rs.mValue}; }
		constexpr auto operator^ (const asserter &rs) requires requires(const T_ lv, const T_ rv) { {lv ^ rv} -> std::convertible_to<T_>; }
			{ return asserter{mValue ^ rs.mValue}; }
		constexpr auto operator<< (const asserter &rs) requires requires(const T_ lv, const T_ rv) { {lv << rv} -> std::convertible_to<T_>; }
			{ return asserter{mValue << rs.mValue}; }
		constexpr auto operator>> (const asserter &rs) requires requires(const T_ lv, const T_ rv) { {lv >> rv} -> std::convertible_to<T_>; }
			{ return asserter{mValue >> rs.mValue}; }

		constexpr auto operator<< (const auto &o) const
			{ return asserter{mValue << o}; }
		constexpr auto operator>> (const auto &o) const
			{ return asserter{mValue >> o}; }

		[[nodiscard]] constexpr auto operator<=> (const T_ &o) const requires requires(T_ lv, const T_ rv) { {lv <=> rv}; }
			{ return mValue <=> o; }

		constexpr auto &operator+= (const auto &o) { return (*this = mValue + o); }
		constexpr auto &operator-= (const auto &o) { return (*this = mValue - o); }
		constexpr auto &operator*= (const auto &o) { return (*this = mValue * o); }
		constexpr auto &operator/= (const auto &o) { return (*this = mValue / o); }
		constexpr auto &operator&= (const auto &o) { return (*this = mValue & o); }
		constexpr auto &operator|= (const auto &o) { return (*this = mValue | o); }

		constexpr auto operator~ () const requires requires(T_ lv) { {~lv}; }
			{ return asserter{~mValue}; }


		constexpr decltype(auto) operator[] (auto &&index) noexcept
			{ return mValue[PL_FWD(index)]; }
		constexpr decltype(auto) operator[] (auto &&index) const noexcept
			{ return mValue[PL_FWD(index)]; }

		constexpr decltype(auto) operator *() const requires requires(const T_ lv) { {*lv}; } { return *mValue; }
		constexpr decltype(auto) operator *() requires requires(T_ lv) { {*lv}; } { return *mValue; }
		constexpr decltype(auto) operator->() const requires requires(T_ lv) { {lv.operator->()}; } { return mValue.operator->(); }
		constexpr decltype(auto) operator->() requires requires(const T_ lv) { {lv.operator->()}; } { return mValue.operator->(); }

		constexpr auto operator &() noexcept { return &mValue; }
		constexpr auto operator &() const noexcept { return &mValue; }

		template <typename OT_>
		friend constexpr void swap(asserter &a1, OT_ &a2) noexcept(CNoThrowSwappableWith<T_ &, OT_ &>)
		{
			adl_swap(a1.mValue, a2);
			a1.assert_impl();
		}

		template <typename OT_>
		friend constexpr void swap(OT_ &a2, asserter &a1) noexcept(CNoThrowSwappableWith<OT_ &, T_ &>)
		{
			adl_swap(a2, a1.mValue);
			a1.assert_impl();
		}

		template <typename OT_, typename... OV_>
		friend constexpr void swap(asserter &a1, asserter<OT_, OV_...> &a2) noexcept(CNoThrowSwappableWith<T_ &, OT_ &>)
		{
			adl_swap(a1.mValue, a2.mValue);
			a1.assert_impl();
			a2.assert_impl();
		}

		[[nodiscard]] friend constexpr bool tag_invoke(tag_t<is_valid>, const asserter &v, const auto tag) noexcept { return is_valid(v.mValue, tag); }

		template <typename CharT_, typename Traits_>
		friend auto &operator>>(std::basic_istream<CharT_, Traits_> &in, asserter &v)
			requires io::CSerializableIn<CharT_, T_>
		{
			in >> v.mValue;
			v.assert_impl();
			return in;
		}

		template <typename CharT_, typename Traits_>
		friend auto &operator<<(std::basic_ostream<CharT_, Traits_> &out, const asserter &v)
			requires io::CSerializableOut<CharT_, T_>
		{
			out << v.mValue;
			return out;
		}
	};

	inline constexpr auto validated_view = rgv::transform([](auto&& v) -> decltype(auto) { return assert_valid(PL_FWD(v)); });
}
