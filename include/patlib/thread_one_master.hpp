/*
	PatLib

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "base.hpp"
#include "thread.hpp"
#include "mutex.hpp"
#include "string.hpp"
#include "io.hpp"

#ifdef PL_NUMA
	#include "numa.hpp"
#endif

#include <mutex>
#include <cstddef>
#include <atomic>
#include <string_view>

#include <shared_access/shared.hpp>

namespace patlib
{
	template <typename Master_, typename Worker_, typename WorkUnit_>
	class ThreadWorkerFirstMaster : public ThreadBase
	{
	public:
		ThreadWorkerFirstMaster(Master_ &master, typename back_buffered_work_units<WorkUnit_>::size_type size)
			: mWorkUnits(size), mMaster{master} {}

		virtual ~ThreadWorkerFirstMaster()
		{
			#ifdef PL_LIB_NUMA
				if(affinityMask)
					numa_free_cpumask(affinityMask);
			#endif
		}

		bool mDeleted = false;
		back_buffered_work_units<WorkUnit_> mWorkUnits;
		Worker_ mWorker;
		Master_ &mMaster;
		bool mIsMaster = false;
		int mNumaNode = 0;

		#ifdef PL_LIB_NUMA
			bitmask *affinityMask = nullptr;
		#endif

		virtual void start() override
		{
			mBusyWait = true;

			if(!mDeleted)
				ThreadBase::start();
		}

		void clear_work_units()
		{
			mWorkUnits.clear();
		}

		void clear_back_work_units()
		{
			mWorkUnits.clear_back();
		}

	protected:

		virtual void on_start() override
		{
			#ifdef PL_LIB_NUMA
				if(numa_available() > -1)
				{
					numa_run_on_node(mNumaNode);
					numa_set_preferred(mNumaNode);
					if(affinityMask)
						numa_sched_setaffinity(0, affinityMask);
				}
			#endif

			mWorker.start(mWorkUnits.front().mUnits, mWorkUnits.back().mUnits);

			ThreadBase::on_start();
		}

		virtual void on_end() override
		{
			mWorker.end();
			ThreadBase::on_end();
		}

		virtual void on_restart() override
		{
			if(mMaster.mNeedRestart)
			{
				if(std::unique_lock g1(mMaster.mRestartMutex, std::try_to_lock); g1 && mMaster.mNeedRestart)
				{
					std::unique_lock g2(mMaster.mMasterMutex);

					mMaster.mMaster.restart();

					trigger_event(ThreadEvent::Restart);

					mMaster.prepare_work();

					//log("") << "Prepared work.";

					mMaster.mNeedRestart = false;
				}
			}

			mWorker.restart();
			ThreadBase::on_restart();
		}

		virtual void on_pause() override
		{
			mWorker.pause();
			ThreadBase::on_pause();
		}

		virtual void on_pause_end() override
		{
			mWorker.unpause();
			ThreadBase::on_pause_end();
		}

		bool do_master_work()
		{
			mMaster.adjust_threads(true);

			bool anyWorkDone = false;

			while( mMaster.process_and_prepare_work() > 0 )
				anyWorkDone = true;

			mMaster.mMaster.work();

			if(mMaster.mMaster.workAllDone())
				end();

			return anyWorkDone;
		}

		virtual void process_loop() override
		{
			do
			{
				std::shared_lock exeg{mMaster.execution_mutex()};

				if(!process_events())
					break;

				mWaiting = true;

				if(mIsMaster)
				{
					if(std::unique_lock g{mMaster.mMasterMutex, std::try_to_lock}; g)
						do_master_work();

					mWaiting = false;
				}

				mWorkUnits.work_on_one(
					[this](auto&& workUnit)
					{
						this->mWaiting = false;

						return this->mWorker.process(workUnit.mWorkData);
					}
				);

				if(mWaiting)
				{
					if(std::unique_lock g{mMaster.mMasterMutex, std::try_to_lock}; g)
						mWaiting = !do_master_work();
				}

				mWorker.idle();
			}
			while(true);
		}
	};

	template <typename Master_, typename Worker_, typename WorkUnit_>
	class ThreadWorkerPoolFirstMaster : public virtual IThreadedDevice, public ThreadPool<ThreadWorkerFirstMaster<ThreadWorkerPoolFirstMaster<Master_, Worker_, WorkUnit_>, Worker_, WorkUnit_>>
	{
	public:

		using worker_type = ThreadWorkerFirstMaster<ThreadWorkerPoolFirstMaster<Master_, Worker_, WorkUnit_>, Worker_, WorkUnit_>;
		using pool_type = ThreadPool<worker_type>;
		friend worker_type;

		ThreadWorkerPoolFirstMaster()
		{
			#ifdef PL_LIB_NUMA
				mNumaMask = numa_allocate_cpumask();
				numa_sched_getaffinity(0, mNumaMask);
			#endif
		}

		virtual ~ThreadWorkerPoolFirstMaster()
		{
			#ifdef PL_LIB_NUMA
				numa_free_cpumask(mNumaMask);
			#endif
		}

		Master_ mMaster;

		void nb_threads(szt nb) noexcept { mNbThreads = nb; }
		[[nodiscard]] szt nb_threads() const noexcept { return mNbThreads; }

		void work_list_size(szt nb) noexcept { mListSize = nb; }
		[[nodiscard]] szt work_list_size() const noexcept { return mListSize; }

		void flip()
		{
			flip_units();
			prepare_work();
		}

		virtual void start() override
		{
			if(!mStarted)
			{
				mMaster.start();
				adjust_threads(false);
				mStarted = true;
			}

			pool_type::start();
		}

		virtual void restart() override
		{
			if(!mNeedRestart)
			{
				mNeedRestart = true;

				flip_units();

				pool_type::restart();
			}
		}

		virtual void end() override
		{
			mStarted = false;

			pool_type::end();
		}

		virtual boost::signals2::connection listen(const ThreadEvent ev, std::function<void()> fc) override
		{
			return mEvts[(int)ev].connect(std::move(fc));
		}

		virtual void lock_execution() override { mExecutionMutex.lock(); }
		virtual void unlock_execution() override { mExecutionMutex.unlock(); }

		[[nodiscard]] auto &execution_mutex() const noexcept { return mExecutionMutex; }

		void name(std::string n) { mName = std::move(n); }
		[[nodiscard]] const auto &name() const noexcept { return mName; }

	protected:

		std::atomic<bool> mNeedRestart{false}, mStarted{false};
		szt mNbThreads = 0, mListSize = 10;
		mutable std::mutex mMasterMutex, mRestartMutex;
		std::string mName;

		mutable yamc::alternate::basic_shared_mutex<yamc::rwlock::WriterPrefer> mExecutionMutex;

		#ifdef PL_LIB_NUMA
			bitmask *mNumaMask;
		#endif

		unsigned int mCurrentCpu = 0;
		int mCurrentNode = 0;

		auto next_numa_node()
		{
			std::pair<unsigned int, int> res{};

			#ifdef PL_LIB_NUMA
				if(numa_available() > -1)
				{
					const auto nbBits = numa_bitmask_nbytes(mNumaMask) * 8;

					do
					{
						while(!numa_bitmask_isbitset(mNumaMask, mCurrentCpu))
						{
							if(++mCurrentCpu >= nbBits)
								mCurrentCpu = 0;
						}
					}
					while( (mCurrentNode = numa_node_of_cpu(mCurrentCpu)) == -1 );

					//std::cout << "Node: " << mCurrentNode << ", CPU: " << mCurrentCpu << '\n';
					res = {mCurrentCpu, mCurrentNode};

					if(++mCurrentCpu >= numa_bitmask_nbytes(mNumaMask) * 8)
						mCurrentCpu = 0;
				}
				else
			#endif
				{
					mCurrentNode = 0;
				}

			return res;
		}

		shared_access::shared<szt> mNbActive{}, mNbPaused{};

		thread_events mEvts;

		void trigger_event(ThreadEvent ev) { mEvts[(int)ev](); }

		void clear_work_units()
		{
			for(auto &t : this->mThreads)
				t.clear_work_units();
		}

		void clear_back_work_units()
		{
			for(auto &t : this->mThreads)
				t.clear_back_work_units();
		}

		void prepare_work(worker_type &t)
		{
			if(t.mDeleted)
				return;

			t.mWorkUnits.prepare_all(
				[this](auto&& workUnit)
				{
					return mMaster.prepare(workUnit.mWorkData);
				}
			);
		}

		void prepare_work()
		{
			for([[maybe_unused]] const auto i : mir(mListSize))
			{
				for(auto &t : this->mThreads)
				{
					if(t.mDeleted)
						continue;

					t.mWorkUnits.prepare_one(
						[this](auto&& workUnit)
						{
							return mMaster.prepare(workUnit.mWorkData);
						}
					);
				}
			}

		}

		if32 process_work(worker_type &t)
		{
			if(t.mDeleted)
				return 0;

			return t.mWorkUnits.process(
				[this](auto&& workUnit)
				{
					return mMaster.process(workUnit.mWorkData);
				}
			);
		}

		if32 process_work()
		{
			if32 total = 0;

			for(auto &t : this->mThreads)
				total += process_work(t);

			return total;

			/*while( rg::accumulate(this->mThreads, false, [&](const auto &acc, const auto &cur){
				return acc | t.mWorkUnits.prepare_one(
					[this](auto&& workUnit)
					{
						return mMaster.prepare(workUnit.mWorkData);
					}
				);
			}) );

			return nb;*/
		}

		if32 process_and_prepare_work()
		{
			if32 total = 0;

			for(auto &t : this->mThreads)
			{
				total += process_work(t);
				prepare_work(t);
			}

			return total;
		}

		void inc_active()
		{
			mNbActive | shared_access::mode::excl |
				[&](auto &nb)
				{
					++nb;

					if(nb == 1)
					{
						trigger_event(ThreadEvent::Start);
						clear_work_units();

						prepare_work();
					}
				};
		}

		void dec_active()
		{
			mNbActive | shared_access::mode::excl |
				[&](auto &nb)
				{
					--nb;

					if(!nb)
					{
						*mNbPaused.access(shared_access::mode::excl) = 0;
						trigger_event(ThreadEvent::End);

						mMaster.end();
					}
				};
		}

		void inc_paused()
		{
			mNbPaused | shared_access::mode::excl |
				[&](auto &nb)
				{
					++nb;

					if(nb == mNbThreads)
					{
						trigger_event(ThreadEvent::Pause);

						mMaster.pause();
					}
				};
		}

		void dec_paused()
		{
			mNbPaused | shared_access::mode::excl |
				[&](auto &nb)
				{
					--nb;

					if(nb == mNbThreads - 1)
					{
						trigger_event(ThreadEvent::PauseEnd);

						mMaster.unpause();
					}
				};
		}

		void adjust_threads(bool startNew = true)
		{
			if(mNbThreads > this->mThreads.size())
			{
				for(const auto i : mir(this->mThreads.size(), mNbThreads))
				{
					const auto node = next_numa_node();

					#ifdef PL_LIB_NUMA
						numa::scoped_run_on_node numarun{(int)mCurrentNode};
					#endif

					auto &t = this->mThreads.emplace_back(*this, mListSize);

					#ifdef PL_LIB_NUMA
						if(numa_available() > -1)
						{
							t.affinityMask = numa_allocate_cpumask();
							numa_bitmask_setbit(t.affinityMask, node.first);
						}
					#endif

					t.mNumaNode = node.second;
					t.mIsMaster = i == 0;
					t.clear_work_units();
					mMaster.prepareWorker(t.mWorker, t.mWorkUnits);
					t.priority(this->priority());

					t.listen(ThreadEvent::Start, [this]{ inc_active(); });
					t.listen(ThreadEvent::End, [this]{ dec_active(); });
					t.listen(ThreadEvent::Pause, [this]{ inc_paused(); });
					t.listen(ThreadEvent::PauseEnd, [this]{ dec_paused(); });

					if(startNew)
						t.start();
				}
			}
			else
			{
				const auto nbToRemove = this->mThreads.size() - mNbThreads;

				for(auto &t : this->mThreads | rgv::take(nbToRemove))
					t.end();

				repeat(nbToRemove, [&]
				{
					auto &t = this->mThreads.front();

					t.join();
					mMaster.deleteWorker(t.mWorker);
					t.mDeleted = true;

					this->mThreads.erase(this->mThreads.begin());
				});
			}

			for(auto&& [i, t] : rgv::enumerate(this->mThreads))
				t.name(mName.empty() ? std::string{} : strconcat(std::string{}, mName, '-', io::to_string_noloc(i)));
		}

		void flip_units()
		{
			for(auto &t : this->mThreads)
				t.mWorkUnits.flip();
		}

	};
}
