/*
	PatLib

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "base.hpp"
#include "range.hpp"
#include "concepts.hpp"
#include "utility.hpp"
#include "algorithm.hpp"
#include "memory.hpp"

#include <vector>
#include <cstddef>
#include <memory>
#include <iterator>
#include <algorithm>
#include <type_traits>
#include <cstring>
#include <utility>
#include <climits>

#include <range/v3/all.hpp>

namespace patlib
{
	/*
		This is a form of stable vector where memory is allocated in blocks of fixed size.
		$NbBitsForItems_ bits from the index is the item index inside the block and the rest is for the block index.

		Some interesting properties:
		- The location in memory of elements doesn't change on allocation. Iterators aren't invalidated.
		- Fast insertion at the end.
		- Elements can be accessed by a normal index. Only a few mask and shift operations required.
		  It however requires 2 pointer dereferences so performance will suffer slightly.
		- Elements are not contiguous outside of a block. std::vector expect all elements to be contiguous so stability cannot be achieved
		  with it.
	*/
	template <
		typename T_,
		std::unsigned_integral IndexType_ = szt,
		IndexType_ NbBitsForItems_ = 16,
		typename Alloc_ = std::allocator<T_>,
		template<typename, typename> typename BlockVectorType_ = std::vector
	>
		requires std::same_as<typename Alloc_::value_type, T_>
	class stable_block_vector
	{
	public:

		using value_type = T_;
		using reference = value_type &;
		using const_reference = const value_type &;
		using pointer = typename std::allocator_traits<Alloc_>::pointer;
		using const_pointer = typename std::allocator_traits<Alloc_>::const_pointer;
		using size_type = IndexType_;
		using difference_type = typename std::incrementable_traits<size_type>::difference_type;
		using allocator_type = Alloc_;
		using iterator = index_vector_iterator<stable_block_vector, size_type>;
		using const_iterator = index_vector_iterator<const stable_block_vector, size_type>;
		using reverse_iterator = std::reverse_iterator<iterator>;
		using const_reverse_iterator = std::reverse_iterator<const_iterator>;

		using index_type = split_value<size_type, NbBitsForItems_>;

		static constinit const size_type SplitPosition = index_type::SplitPosition;
		static constinit const auto HighSize = index_type::LowSize;

		using block_allocator_type = typename std::allocator_traits<Alloc_>::template rebind_alloc<pointer>;
		using block_vector_type = BlockVectorType_<pointer, block_allocator_type>;

		constexpr stable_block_vector() = default;

		explicit constexpr stable_block_vector(const Alloc_ &alloc) noexcept
			: mBlocks(block_allocator_type{alloc}), mAllocator{alloc}
		{}

		explicit constexpr stable_block_vector(const block_allocator_type &blockAlloc, const Alloc_ &alloc) noexcept
			: mBlocks(blockAlloc), mAllocator{alloc}
		{}

		explicit constexpr stable_block_vector(size_type count, const Alloc_ &alloc = {})
			: mBlocks(block_allocator_type{alloc}), mAllocator{alloc}
		{
			resize(count);
		}

		explicit constexpr stable_block_vector(size_type count, const T_ &value, const Alloc_ &alloc = {})
			: mBlocks(block_allocator_type{alloc}), mAllocator{alloc}
		{
			resize(count, value);
		}

		template <rg::input_iterator IT_>
		constexpr stable_block_vector(IT_ first, IT_ last, const Alloc_ &alloc = {})
			: mBlocks(block_allocator_type{alloc}), mAllocator{alloc}
		{
			insert(first, last);
		}

		constexpr stable_block_vector(std::initializer_list<T_> init, const Alloc_ &alloc = {})
			: mBlocks(block_allocator_type{alloc}), mAllocator{alloc}
		{
			insert(init.begin(), init.end());
		}

	private:
		template <typename Proj_ = std::identity>
		constexpr void copy_content(auto&& o, Proj_&& proj = {})
		{
			set_nb_blocks(o.nb_blocks());

			size_type counter = o.size();
			for(auto&& [bd, bs] : rgv::zip(mBlocks, o.mBlocks))
			{
				try
				{
					uninitialized_copy_alloc(rg::make_subrange(bs, bs + std::min(auto(HighSize), counter)), bd, mAllocator, PL_FWD(proj));
				}
				catch(...)
				{
					mNbItems = o.size() - counter;
					clear();
					throw;
				}

				counter -= HighSize;
			}

			mNbItems = o.size();
		}

	public:

		constexpr stable_block_vector(const stable_block_vector &o) :
			mAllocator{std::allocator_traits<allocator_type>::select_on_container_copy_construction(o.mAllocator)},
			mBlocks{std::allocator_traits<allocator_type>::select_on_container_copy_construction(o.mBlocks.get_allocator())}
		{
			copy_content(o);
		}

		constexpr stable_block_vector(stable_block_vector&& o) = default;

		constexpr stable_block_vector(const stable_block_vector &o, const Alloc_ &alloc)
			: mBlocks(block_allocator_type{alloc}), mAllocator{alloc}
		{
			copy_content(o);
		}

		constexpr stable_block_vector(stable_block_vector &&o, const Alloc_ &alloc)
			: mNbItems{o.mNbItems}, mBlocks(std::move(o.mBlocks), block_allocator_type{alloc}), mAllocator{alloc}
		{}

		constexpr stable_block_vector &operator=(const stable_block_vector &o)
		{
			clear();

			if constexpr(std::allocator_traits<block_allocator_type>::propagate_on_container_copy_assignment::value)
				recreate(mBlocks, o.mBlocks.get_allocator());

			if constexpr(std::allocator_traits<allocator_type>::propagate_on_container_copy_assignment::value)
				if(!std::allocator_traits<allocator_type>::is_always_equal::value && mAllocator != o.mAllocator)
					mAllocator = o.mAllocator;

			copy_content(o);

			return *this;
		}

		constexpr stable_block_vector& operator =(stable_block_vector&& o)
			noexcept(
				(std::allocator_traits<allocator_type>::propagate_on_container_move_assignment::value || std::allocator_traits<allocator_type>::is_always_equal::value) &&
				(std::allocator_traits<block_allocator_type>::propagate_on_container_move_assignment::value || std::allocator_traits<block_allocator_type>::is_always_equal::value)
			)
		{
			clear();

			if constexpr(std::allocator_traits<allocator_type>::propagate_on_container_move_assignment::value)
			{
				mAllocator = o.mAllocator;
				mBlocks = std::move(o.mBlocks);
			}
			else
			{
				if(std::allocator_traits<allocator_type>::is_always_equal::value || mAllocator == o.mAllocator)
				{
					mBlocks = std::move(o.mBlocks);
				}
				else
				{
					mBlocks = std::move(o.mBlocks);

					copy_content(o, move_proj);

					o.clear();
					o.shrink_to_fit();
				}
			}

			mNbItems = o.mNbItems;

			return *this;
		}

		constexpr stable_block_vector& operator =(std::initializer_list<value_type> ilist)
		{
			clear();
			insert(end(), ilist);

			return *this;
		}

		constexpr ~stable_block_vector()
		{
			[[maybe_unused]] auto counter = static_cast<difference_type>(size());
			for(auto &b : mBlocks)
			{
				if constexpr(!CTriviallyDestructible<value_type>)
					if(counter > 0)
					{
						std::for_each(b, b + std::min(counter, static_cast<difference_type>(HighSize)), [&](auto &e){
							std::allocator_traits<allocator_type>::destroy(mAllocator, std::addressof(e));
						});
						counter -= HighSize;
					}

				mAllocator.deallocate(b, HighSize);
			}
		}

		[[nodiscard]] constexpr auto get_allocator() const noexcept { return mAllocator; }

		constexpr void push_back(const T_ &d)
		{
			const auto b = offset_pointer(inc_count());

			std::uninitialized_construct_using_allocator(std::to_address(b), mAllocator, d);
		}

		constexpr void push_back(T_&& d)
		{
			const auto b = offset_pointer(inc_count());

			std::uninitialized_construct_using_allocator(std::to_address(b), mAllocator, std::move(d));
		}

		template <typename... Args_>
		constexpr T_ &emplace_back(Args_&&... args)
		{
			const auto b = offset_pointer(inc_count());

			std::uninitialized_construct_using_allocator(std::to_address(b), mAllocator, PL_FWD(args)...);

			return *b;
		}

		[[nodiscard]] constexpr T_ &operator [] (const size_type index)
		{
			const index_type i{index};
			plassert(index < size());
			return mBlocks[i.high()][i.low()];
		}
		[[nodiscard]] constexpr const T_ &operator [] (const size_type index) const
		{
			const index_type i{index};
			plassert(index < size());
			return mBlocks[i.high()][i.low()];
		}

		[[nodiscard]] constexpr T_ &back() { return (*this)[mNbItems - 1]; }
		[[nodiscard]] constexpr const T_ &back() const { return (*this)[mNbItems - 1]; }

		[[nodiscard]] constexpr T_ &front() { return mBlocks[0][0]; }
		[[nodiscard]] constexpr const T_ &front() const { return mBlocks[0][0]; }

		[[nodiscard]] constexpr T_ &at(const size_type index)
		{
			if(index >= mNbItems)
				throw std::out_of_range("Index out of bounds.");

			const index_type i{index};
			return mBlocks[i.high()][i.low()];
		}
		[[nodiscard]] constexpr const T_ &at(const size_type index) const
		{
			if(index >= mNbItems)
				throw std::out_of_range("Index out of bounds.");

			const index_type i{index};
			return mBlocks[i.high()][i.low()];
		}

		[[nodiscard]] constexpr bool empty() const noexcept { return !mNbItems; }
		[[nodiscard]] constexpr size_type size() const noexcept { return mNbItems; }
		[[nodiscard]] constexpr size_type max_size() const noexcept { return std::allocator_traits<allocator_type>::max_size(mAllocator); }
		[[nodiscard]] constexpr size_type capacity() const noexcept { return mBlocks.capacity() * HighSize; }
		[[nodiscard]] constexpr size_type nb_blocks() const noexcept { return mNbItems ? index_type{mNbItems - 1}.high() + 1 : 0; }

		constexpr void reserve(size_type nb)
		{
			const size_type nbBlocks = nb ? static_cast<size_type>(index_type{nb - 1}.high()) + 1 : 0,
				oldSize = mBlocks.size();

			if(nbBlocks > oldSize)
			{
				mBlocks.resize(nbBlocks, nullptr);

				for(auto &b : mBlocks | rgv::slice(oldSize, static_cast<size_type>(mBlocks.size())))
					b = mAllocator.allocate(HighSize);
			}

		}

		constexpr void shrink_to_fit()
		{
			const size_type nbBlocks = nb_blocks();

			if(nbBlocks < mBlocks.size())
			{
				for(auto &b : mBlocks | rgv::slice(nbBlocks, static_cast<size_type>(mBlocks.size())))
					mAllocator.deallocate(b, HighSize);

				mBlocks.resize(nbBlocks);
				mBlocks.shrink_to_fit();
			}
		}

		constexpr void pop_back()
		{
			--mNbItems;

			std::allocator_traits<allocator_type>::destroy(mAllocator, std::addressof((*this)[mNbItems]));
		}

		constexpr void resize(size_type nb)
		{
			resize_impl(bool_true, bool_true, nb);
		}

		constexpr void resize(size_type nb, const T_ &value)
		{
			resize_impl(bool_true, bool_true, nb, value);
		}

		constexpr void swap(stable_block_vector &other)
			noexcept(
				(!std::allocator_traits<allocator_type>::propagate_on_container_swap::value || std::is_nothrow_swappable_v<Alloc_>)
				&& std::is_nothrow_swappable_v<block_vector_type>
			)
		{
			mBlocks.swap(other.mBlocks);
			adl_swap(mNbItems, other.mNbItems);

			if constexpr(std::allocator_traits<allocator_type>::propagate_on_container_swap::value)
				adl_swap(mAllocator, other.mAllocator);
		}

		constexpr void assign(size_type count, const T_ &value)
		{
			resize(count, value);
		}

		template <rg::input_iterator IT_>
		constexpr void assign(IT_ first, IT_ last)
		{
			clear();
			insert(first, last);
		}

		constexpr void assign(std::initializer_list<T_> ilist)
		{
			clear();
			insert(ilist.begin(), ilist.end());
		}

		constexpr iterator insert(const_iterator pos, const T_ &value)
		{
			inc_count();
			move_up(pos.offset(), 1);

			std::uninitialized_construct_using_allocator(std::to_address(offset_pointer(pos.offset())), mAllocator, value);

			return {*this, pos.offset()};
		}

		constexpr iterator insert(const_iterator pos, T_ &&value)
		{
			inc_count();
			move_up(pos.offset(), 1);

			std::uninitialized_construct_using_allocator(std::to_address(offset_pointer(pos.offset())), mAllocator, std::move(value));

			return {*this, pos.offset()};
		}

		constexpr iterator insert(const_iterator pos, const size_type count, const T_ &value)
		{
			inc_count(count);

			move_up(pos.offset(), count);

			for(const auto i : mir(count))
				std::uninitialized_construct_using_allocator(std::to_address(offset_pointer(pos.offset() + i)), mAllocator, value);

			return {*this, pos.offset()};
		}

		template <rg::input_iterator InputIt_>
		constexpr iterator insert(const_iterator pos, InputIt_ first, InputIt_ last)
		{
			const size_type count = std::distance(first, last);

			inc_count(count);

			move_up(pos.offset(), count);
			for(auto&& [i, value] : rgv::enumerate( rg::make_subrange(first, last) ))
				std::uninitialized_construct_using_allocator(std::to_address(offset_pointer(pos.offset() + i)), mAllocator, PL_FWD(value));

			return {*this, pos.offset()};
		}

		constexpr iterator insert(const_iterator pos, std::initializer_list<T_> ilist)
		{
			inc_count(ilist.size());

			move_up(pos.offset(), ilist.size());
			for(auto&& [i, value] : rgv::enumerate(ilist))
				std::uninitialized_construct_using_allocator(std::to_address(offset_pointer(pos.offset() + i)), mAllocator, PL_FWD(value));

			return {*this, pos.offset()};
		}

		constexpr iterator erase(const_iterator pos)
		{
			std::allocator_traits<allocator_type>::destroy(mAllocator, std::addressof((*this)[pos.offset()]));

			move_down(pos.offset() + 1, 1);
			--mNbItems;

			return {*this, pos.offset()};
		}

		constexpr iterator erase(const_iterator first, const_iterator last)
		{
			for(auto &v : *this | rgv::slice(first.offset(), last.offset()))
				std::allocator_traits<allocator_type>::destroy(mAllocator, std::addressof(v));

			const auto nb = last - first;
			move_down(last.offset() + 1, nb);
			mNbItems -= nb;

			return {*this, first.offset()};
		}

		constexpr void clear() noexcept
		{
			difference_type counter = static_cast<difference_type>(size());
			for(auto &b : mBlocks)
			{
				if(counter > 0)
				{
					std::for_each(b, b + std::min(counter, static_cast<difference_type>(HighSize)), [&](auto &e){
						std::allocator_traits<allocator_type>::destroy(mAllocator, std::addressof(e));
					});
					counter -= HighSize;
				}
			}

			mNbItems = 0;
		}

		[[nodiscard]] constexpr size_type back_index() const noexcept { return mNbItems - 1; }

		[[nodiscard]] constexpr szt memory_capacity() const noexcept
			{ return container_memory_capacity(mBlocks) + mBlocks.capacity() * static_cast<szt>(HighSize) * sizeof(T_); }

		[[nodiscard]] constexpr auto begin() noexcept { return iterator{*this}; }
		[[nodiscard]] constexpr auto begin() const noexcept { return const_iterator{*this}; }
		[[nodiscard]] constexpr auto cbegin() const noexcept { return const_iterator{*this}; }
		[[nodiscard]] constexpr auto end() noexcept { return iterator{*this, size()}; }
		[[nodiscard]] constexpr auto end() const noexcept { return const_iterator{*this, size()}; }
		[[nodiscard]] constexpr auto cend() const noexcept { return const_iterator{*this, size()}; }

		[[nodiscard]] constexpr auto rbegin() noexcept { return reverse_iterator{end()}; }
		[[nodiscard]] constexpr auto rbegin() const noexcept { return const_reverse_iterator{end()}; }
		[[nodiscard]] constexpr auto crbegin() const noexcept { return const_reverse_iterator{cend()}; }
		[[nodiscard]] constexpr auto rend() noexcept { return reverse_iterator{begin()}; }
		[[nodiscard]] constexpr auto rend() const noexcept { return const_reverse_iterator{begin()}; }
		[[nodiscard]] constexpr auto crend() const noexcept { return const_reverse_iterator{cbegin()}; }

		template <typename Container_>
		constexpr void copy_to(Container_ &into) const
		{
			try_reserve(into, into.size() + size());

			auto curNb = size();
			for(auto &b : mBlocks)
			{
				//std::copy(b, b + std::min(curNb, HighSize), std::back_inserter(into));
				into.insert(into.end(), b, b + std::min(curNb, auto(HighSize)));

				if(curNb <= HighSize)
					break;
				curNb -= HighSize;
			}
		}

		constexpr void move_to(stable_block_vector &into) noexcept(CNoThrowMoveAssignable<stable_block_vector>)
		{
			into = std::move(*this);
		}

		template <typename Container_>
		constexpr void move_to(Container_ &into)
		{
			try_reserve(into, into.size() + size());

			auto curNb = size();
			for(auto &b : mBlocks)
			{
				into.insert(into.end(), std::make_move_iterator(b), std::make_move_iterator(b + std::min(curNb, auto(HighSize))));

				mAllocator.deallocate(b, HighSize);
				if(curNb <= HighSize)
					break;
				curNb -= HighSize;
			}

			mBlocks.clear();
			mNbItems = 0;
		}

		friend constexpr void tag_invoke(tag_t<append_copy>, const stable_block_vector &cont, auto &into)
			{ cont.copy_to(into); }

		friend constexpr void tag_invoke(tag_t<append_move>, stable_block_vector &cont, auto &into)
			{ cont.move_to(into); }

		friend constexpr void swap(stable_block_vector &i, stable_block_vector &o) noexcept(noexcept(i.swap(o)))
			{ i.swap(o); }

		[[nodiscard]] friend constexpr auto tag_invoke(tag_t<container_memory_capacity>, stable_block_vector &v) noexcept
			{ v.memory_capacity(); }

		[[nodiscard]] constexpr const auto &blocks() const noexcept { return mBlocks; }

	private:

		size_type mNbItems{};
		block_vector_type mBlocks;
		[[no_unique_address]] Alloc_ mAllocator;

		[[nodiscard]] constexpr auto offset_pointer(size_type offset) const noexcept
		{
			const index_type bits{offset};
			return mBlocks[bits.high()] + bits.low();
		}

		[[nodiscard]] constexpr auto offset_pointer(index_type index) const noexcept
		{
			return mBlocks[index.high()] + index.low();
		}

		constexpr index_type inc_count(size_type nb = 1)
		{
			mNbItems += nb;

			const auto newBlockIndex = index_type{mNbItems - 1};

			if(const auto newBlockCnt = static_cast<size_type>(newBlockIndex.high()) + 1; newBlockCnt > static_cast<size_type>(mBlocks.size()))
			{
				for([[maybe_unused]] auto i : mir( newBlockCnt - mBlocks.size() ))
					mBlocks.push_back(mAllocator.allocate(HighSize));
			}

			return newBlockIndex;
		}

		constexpr void move_up(size_type index, size_type nb)
		{
			for(
				auto it = rbegin(),
					it2 = it + nb,
					itEnd = it + (size() - index);
				it2 != itEnd;
				++it, ++it2
			)
			{
				if(!nb)
					*it = std::move(*it2);
				else
				{
					std::uninitialized_construct_using_allocator(std::to_address(offset_pointer(it.base().offset())), mAllocator, std::move(*it2));
					--nb;
				}
			}
		}

		constexpr void move_down(size_type index, size_type nb)
		{
			for(
				auto it = begin() + index - nb,
					it2 = it + nb,
					itEnd = end();
				it2 != itEnd;
				++it, ++it2
			)
				*it = std::move(*it2);
		}

		template <bool DoCtor_, bool DoDtor_>
		constexpr void resize_impl(bool_c<DoCtor_>, bool_c<DoDtor_>, size_type nb, auto&&... args)
		{
			const size_type nbBlocks = nb ? static_cast<size_type>(index_type{nb - 1}.high()) + 1 : 0,
				oldSize = mBlocks.size();

			if(static_cast<difference_type>(nb) - static_cast<difference_type>(size()) > 0)
			{
				mBlocks.resize(nbBlocks, nullptr);

				for(auto &b : mBlocks | rgv::slice(oldSize, static_cast<size_type>(mBlocks.size())))
					b = mAllocator.allocate(HighSize);

				if constexpr(DoCtor_)
				{
					size_type start = size() ? index_type{size() - 1}.low() + 1 : 0;
					for(auto &b : mBlocks | rgv::slice(oldSize - 1, static_cast<size_type>(mBlocks.size()) - 1))
					{
						for(const auto i : mir(start, auto(HighSize)))
						{
							try
							{
								std::uninitialized_construct_using_allocator(std::to_address(b + i), mAllocator, PL_FWD(args)...);
							} catch(...) {

								if constexpr(!CTriviallyDestructible<T_>)
									for(const auto c : mir(start, i))
										std::allocator_traits<allocator_type>::destroy(mAllocator, std::addressof(b[c]));

								throw;
							}
						}

						mNbItems += HighSize;
						start = 0;
					}
				}
			}
			else
			{
				size_type start = nb ? index_type{nb - 1}.low() + 1 : 0;
				for(auto &b : mBlocks | rgv::slice(static_cast<size_type>(index_type{start}.high()), static_cast<size_type>(mBlocks.size())))
				{
					if constexpr(DoDtor_ && !CTriviallyDestructible<T_>)
						for(const auto i : mir(start, auto(HighSize)))
							std::allocator_traits<allocator_type>::destroy(mAllocator, std::addressof(b[i]));

					if(!start)
						mAllocator.deallocate(b, HighSize);

					start = 0;
				}

				mBlocks.resize(nbBlocks);
			}

			mNbItems = nb;
		}

		constexpr void set_nb_blocks(szt nb)
		{
			if(nb < mBlocks.size())
			{
				for(const auto &b : mBlocks | rgv::drop_exactly(mBlocks.size() - nb))
					mAllocator.deallocate(b, HighSize);

				mBlocks.resize(nb, nullptr);
			}
			else
			{
				mBlocks.reserve(nb);

				repeat(nb, [&]
				{
					mBlocks.push_back(mAllocator.allocate(HighSize));
				});
			}
		}

	};

}
