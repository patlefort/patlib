/*
	PatLib

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "base.hpp"
#include "concepts.hpp"
#include "pointer.hpp"
#include "type.hpp"
#include "utility.hpp"

#include <type_traits>
#include <utility>
#include <memory>
#include <tuple>

namespace patlib
{
	template <std::invocable FC_>
	class [[nodiscard]] scoped_guard : no_copy
	{
		[[no_unique_address]] FC_ mFc;

	public:

		constexpr scoped_guard(FC_ f) noexcept(CNoThrowMoveContructible<FC_>)
			: mFc{std::move(f)} {}

		~scoped_guard() noexcept(noexcept(mFc()))
		{
			try
			{
				mFc();
			}
			catch(...)
			{
				std::destroy_at(&mFc);
				throw;
			}
		}
	};
	template <typename FC_>
	scoped_guard(FC_) -> scoped_guard<FC_>;

	template <typename T_>
	concept CReversible = requires(T_ r) { {r.reverse()}; };
	template <typename T_>
	concept CNothrowReversible = requires(T_ r) { {r.reverse()} noexcept; };

	// Take a group of reversible objects and reverse them back to normal when destroyed
	template <CNothrowReversible... T_>
	struct [[nodiscard]] scoped_reverse : no_copy
	{
		std::tuple<T_&...> mT;

		constexpr scoped_reverse(T_&... args) noexcept : mT{args...} {}

		~scoped_reverse() noexcept
		{
			if(mReversed)
				doReverse();
		}

		constexpr void reverse() noexcept
		{
			doReverse();
			mReversed = !mReversed;
		}

		constexpr void to_normal() noexcept
		{
			if(mReversed)
			{
				doReverse();
				mReversed = false;
			}
		}

		[[nodiscard]] constexpr bool reversed() const noexcept { return mReversed; }

	private:
		bool mReversed = false;

		constexpr void doReverse() noexcept
		{
			tuple_for_each(mT, [](auto&& e){ e.reverse(); });
		}
	};
	template <typename... Ts_>
	scoped_reverse(Ts_&&...) -> scoped_reverse<Ts_...>;

	// Assign a value to a variable and restore its old value on destruction
	template <typename Type_>
	class [[nodiscard]] scoped_assignment : move_only
	{
	public:
		constexpr scoped_assignment() noexcept(CNoThrowContructible<Type_>) : mOldVar{} {}

		constexpr scoped_assignment(Type_ &var) noexcept(CNoThrowCopyContructible<Type_>) : mOldVar{&var}, mOldValue{var} {}

		template <typename T_>
		constexpr scoped_assignment(Type_ &var, T_&& newValue)
			noexcept(CNoThrowCopyContructible<Type_> && noexcept(var = PL_FWD(newValue))) :
			mOldVar{&var}, mOldValue{var}
			{ var = PL_FWD(newValue); }

		constexpr scoped_assignment(scoped_assignment &&other) noexcept(CNoThrowMoveContructible<Type_>) :
			mOldVar(move_pointer(other.mOldVar)),
			mOldValue(std::move(other.mOldValue)) {}

		~scoped_assignment() noexcept(CNoThrowCopyAssignable<Type_>)
			{ if(mOldVar) *mOldVar = mOldValue; }

		constexpr scoped_assignment &operator= (scoped_assignment &&other) noexcept(CNoThrowMoveAssignable<Type_>)
		{
			mOldValue = std::move(other.mOldValue);
			move_pointer(mOldVar, other.mOldVar);
			return *this;
		}

		[[nodiscard]] constexpr const Type_ &old_value() const noexcept { return mOldValue; }
		[[nodiscard]] constexpr const Type_ &operator *() const noexcept { return mOldValue; }

	private:
		Type_ *mOldVar, mOldValue;

	};

	template <typename Type_>
	class [[nodiscard]] fast_scoped_assignment : no_copy
	{
	public:
		constexpr fast_scoped_assignment(Type_ &var) noexcept(CNoThrowCopyContructible<Type_>) :
			mOldVar{var}, mOldValue{var} {}

		template <typename T_>
		constexpr fast_scoped_assignment(Type_ &var, T_&& newValue)
			noexcept(CNoThrowCopyContructible<Type_> && noexcept(var = PL_FWD(newValue))) :
			mOldVar{var}, mOldValue{var}
			{ var = PL_FWD(newValue); }

		~fast_scoped_assignment() noexcept(CNoThrowCopyAssignable<Type_>)
			{ mOldVar = mOldValue; }

		[[nodiscard]] constexpr const Type_ &old_value() const noexcept { return mOldValue; }
		[[nodiscard]] constexpr const Type_ &operator *() const noexcept { return mOldValue; }

	private:
		Type_ &mOldVar;
		Type_ mOldValue;

	};

	template <typename Type1_, typename Type2_>
		requires std::swappable_with<Type1_ &, Type2_ &>
	class [[nodiscard]] scoped_swap : no_copy
	{
	public:
		constexpr scoped_swap(Type1_ &s, Type2_ &d) noexcept(CNoThrowSwappableWith<Type1_, Type2_>)
			: mSrc{s}, mDest{d} { adl_swap(mSrc, mDest); }
		~scoped_swap() noexcept(CNoThrowSwappableWith<Type1_, Type2_>)
			{ adl_swap(mSrc, mDest); }

	private:
		Type1_ &mSrc;
		Type2_ &mDest;
	};

	template <std::destructible T_>
	class [[nodiscard]] scoped_pointer_destruct
	{
	public:

		constexpr scoped_pointer_destruct(T_ *p) noexcept : mPtr{p} {}
		~scoped_pointer_destruct() noexcept(CNoThrowDestructible<T_>)
			{ if(mPtr) std::destroy_at(mPtr.get()); }

		[[nodiscard]] constexpr auto operator->() const noexcept { return mPtr.get(); }
		[[nodiscard]] constexpr decltype(auto) operator*() const noexcept { return *mPtr; }

		constexpr auto release() noexcept { return std::exchange(mPtr, nullptr).get(); }

	private:

		unique_unmanaged_pointer<T_> mPtr;
	};
}
