/*
	PatLib

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <patlib/task.hpp>

namespace patlib::task
{
	const char *TaskException::module() const noexcept
		{ return "Task"; }

	Scheduler::Scheduler(size_type size)
		: mQueue(size)
	{
		name("pl-task-system");
		mPushBack.reserve(size);
	}

	Scheduler::~Scheduler()
	{
		mQueue.consume_all([](auto&& t) { t->destroyPromise(); delete t; });
	}

	void Scheduler::pause()
	{
		ThreadBase::pause();
		mQueueCond.notify_all();
	}

	void Scheduler::end()
	{
		ThreadBase::end();
		mQueueCond.notify_all();
	}

	bool Scheduler::prepare_to_end()
		{ return mReadyToEnd; }

	void Scheduler::process_loop()
	{
	#ifdef PL_OPENMP_TASKS

		const auto process_queue = [&]
			{
				mPushBack.clear();

				const auto nb = mQueue.consume_all(
					[&](queued_task *t)
					{
						enum class TaskAction
						{
							Run, RunLater, Destroy
						};

						const TaskAction action = *t->attribs | shared_access::mode::excl |
							[&](auto &attribs)
							{
								if(attribs.cancelled)
									return TaskAction::Destroy;

								if(std::chrono::steady_clock::now() >= attribs.time)
								{
									attribs.running = true;
									return TaskAction::Run;
								}

								return TaskAction::RunLater;
							};

						switch(action)
						{
							case TaskAction::Run:
								mRunningTasks.push_front(t);

								#pragma omp task firstprivate(t)
								{
									t->fc();
									t->attribs->access(shared_access::mode::excl)->running = false;
								}
								break;
							case TaskAction::RunLater:
								mPushBack.push_back(t);
								break;
							case TaskAction::Destroy:
								t->destroyPromise();
								delete t;
								break;
						}
					}
				);

				for(auto &t : mPushBack)
					mQueue.push(t);

				mPushBack.clear();

				return nb;
			};

		const auto process_running_tasks = [&]
			{
				for(auto it = mRunningTasks.before_begin() ;;)
				{
					auto next = std::next(it);
					if(next == mRunningTasks.end())
						break;

					if(!(*next)->attribs->access(shared_access::mode::shared)->running)
					{
						delete *next;
						mRunningTasks.erase_after(it);
					}
					else
						++it;
				}
			};

		#pragma omp parallel
		{
			#pragma omp single nowait
			{
				work_loop([&]{
					process_running_tasks();
					const auto nb = process_queue();

					if(mRequests.requested(ThreadRequest::End))
						task_wait();

					if(!nb)
					{
						if(mRunningTasks.empty() && mRequests.requested(ThreadRequest::End))
							mReadyToEnd = true;
						else
						{
							std::unique_lock waitLock{mQueueCondMutex};
							mQueueCond.wait_for(waitLock, mSleepDur);
						}
					}

					return true;
				});
			}
		}

	#endif
	}

	namespace impl
	{
		Scheduler &task_scheduler()
		{
			static Scheduler tasks;
			return tasks;
		}
	}
}
