/*
	PatLib

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <patlib/log.hpp>

namespace patlib::log_impl
{
	static StdLogger<logtag::message_t> stdLogMessage;
	static StdLogger<logtag::error_t> stdLogError;

	global_logger_type &get_logger()
	{
		static global_logger_type s{
			hn::make_pair(logtag::message, &stdLogMessage),
			hn::make_pair(logtag::error, &stdLogError)
		};

		return s;
	}

	void set_standard_loggers()
	{
		logger.set_logger(stdLogMessage);
		logger.set_logger(stdLogError);
	}
}
