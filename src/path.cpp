/*
	PatLib

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <patlib/path.hpp>

#include <cstdlib>

#include <boost/dll/runtime_symbol_info.hpp>

namespace patlib::paths
{
	std::filesystem::path app_dir()
	{
		return boost::dll::program_location().remove_filename().string();
	}

	std::filesystem::path app_prefix_dir()
	{
		return app_dir().parent_path();
	}
}

#ifdef _WIN32

	#include <shlobj.h>

	namespace patlib::paths
	{
		std::filesystem::path user_home_dir()
		{
			char windowspath[MAX_PATH + 1];

			if(SUCCEEDED(SHGetFolderPath(NULL, CSIDL_PROFILE, NULL, 0, windowspath)))
				return windowspath;

			return {};
		}

		std::filesystem::path user_config_dir()
		{
			char windowspath[MAX_PATH + 1];

			if(SUCCEEDED(SHGetFolderPath(NULL, CSIDL_APPDATA, NULL, 0, windowspath)))
			{
				std::filesystem::path p{windowspath};
				return p;
			}

			return {};
		}

		std::filesystem::path user_data_dir()
		{
			char windowspath[MAX_PATH + 1];

			if(SUCCEEDED(SHGetFolderPath(NULL, CSIDL_APPDATA, NULL, 0, windowspath)))
			{
				std::filesystem::path p{windowspath};
				return p;
			}

			return {};
		}

		std::filesystem::path user_cache_dir()
		{
			return user_data_dir();
		}

		std::filesystem::path user_temp_dir()
		{
			return std::filesystem::temp_directory_path();
		}
	}

#else

	namespace patlib::paths
	{
		std::filesystem::path user_home_dir()
		{
			return std::getenv("HOME");
		}

		std::filesystem::path user_config_dir()
		{
			if(auto xdgConfig = std::getenv("XDG_CONFIG_HOME"); xdgConfig)
				return std::filesystem::path{xdgConfig};

			return user_home_dir() / ".config";
		}

		std::filesystem::path user_temp_dir()
		{
			if(auto xdgConfig = std::getenv("XDG_RUNTIME_DIR"); xdgConfig)
				return std::filesystem::path{xdgConfig};

			return std::filesystem::temp_directory_path();
		}

		std::filesystem::path user_data_dir()
		{
			if(auto xdgConfig = std::getenv("XDG_DATA_HOME"); xdgConfig)
				return std::filesystem::path{xdgConfig};

			return user_home_dir() / ".local"/"share";
		}

		std::filesystem::path user_cache_dir()
		{
			if(auto xdgConfig = std::getenv("XDG_CACHE_HOME"); xdgConfig)
				return std::filesystem::path{xdgConfig};

			return user_home_dir() / ".cache";
		}
	}

#endif
