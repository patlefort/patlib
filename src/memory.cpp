/*
	PatLib

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <patlib/memory.hpp>

#ifdef PL_HUGEPAGES
	extern "C" {
		#include <hugetlbfs.h>
	}

	#include <sys/mman.h>
#endif

namespace patlib
{
	std::pmr::memory_resource &default_resource()
	{
		static MemoryResource mr;

		return mr;
	}

	#ifdef PL_HUGEPAGES

		[[nodiscard]] constexpr szt select_align(szt bytes, szt alignment, szt HPAlign) noexcept
			{ return bytes >= HPAlign ? std::max(HPAlign, alignment) : alignment; }

		MemoryResourceHugePages::MemoryResourceHugePages()
		{
			mHPAlign = ::gethugepagesize();
		}

		[[nodiscard]] void *MemoryResourceHugePages::do_allocate(szt bytes, szt alignment)
		{
			alignment = select_align(bytes, alignment, mHPAlign);
			const auto p = operator new(bytes, std::align_val_t(alignment));

			if(alignment == mHPAlign)
				::madvise(static_cast<void *>(std::to_address(p)), bytes, MADV_HUGEPAGE);

			return p;
		}

		void MemoryResourceHugePages::do_deallocate(void *p, szt bytes, szt alignment)
			{ operator delete(p, std::align_val_t(select_align(bytes, alignment, mHPAlign))); }

		bool MemoryResourceHugePages::do_is_equal(const std::pmr::memory_resource &o) const noexcept
			{ return this == &o; }

		std::pmr::memory_resource &hugepages_resource()
		{
			static MemoryResourceHugePages mr;

			return mr;
		}
	#endif
}

