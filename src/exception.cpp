/*
	PatLib

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <patlib/exception.hpp>
#include <patlib/io.hpp>
#include <patlib/string.hpp>

namespace patlib
{
	Exception::Exception(std::string_view message, const std::experimental::source_location &sl) noexcept
		: mMessage{ strconcat(std::string{}, '[', sl.file_name(), "] ", io::to_string_loc(sl.line(), io::try_get_locale("")), ": \n", message) }
	{}
}
