/*
	PatLib

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <patlib/utility.hpp>
#include <patlib/thread.hpp>
#include <patlib/chrono.hpp>

#ifdef _WIN32
	#define WIN32_LEAN_AND_MEAN
	#include <windows.h>
	#include <processthreadsapi.h>
#else
	#include <pthread.h>
#endif

#include <bit>

namespace patlib
{
	using namespace std::chrono_literals;

	static void set_thread_priority(std::thread &t, const EnumThreadPriority p)
	{
		if(t.native_handle())
		{
			#ifdef _WIN32

				const HANDLE hThread = std::bit_cast<HANDLE>(t.native_handle());
				int wPriority;

				switch(p)
				{
				case EnumThreadPriority::Low:
					wPriority = THREAD_PRIORITY_LOWEST;
					break;
				case EnumThreadPriority::High:
					wPriority = THREAD_PRIORITY_HIGHEST;
					break;
				default:
					wPriority = THREAD_PRIORITY_NORMAL;
				}

				SetThreadPriority(hThread, wPriority);

			#else

				int policy;
				sched_param sch;

				pthread_getschedparam(t.native_handle(), &policy, &sch);

				switch(p)
				{
				case EnumThreadPriority::Low:
					policy = SCHED_IDLE;
					break;
				case EnumThreadPriority::High:
					policy = SCHED_OTHER;
					break;
				default:
					policy = SCHED_BATCH;
				}

				pthread_setschedparam(t.native_handle(), policy, &sch);

			#endif
		}
	}

	static void set_thread_name(std::thread &t, [[maybe_unused]] const char *name)
	{
		if(t.native_handle())
		{
			#ifdef _WIN32

				using SetThreadDescriptionFc = HRESULT(WINAPI*)(HANDLE hThread, PCWSTR lpThreadDescription);
				auto setNameFc = reinterpret_cast<SetThreadDescriptionFc>(::GetProcAddress(::GetModuleHandle("Kernel32.dll"), "SetThreadDescription"));

				if(setNameFc)
				{
					const auto wideLength = ::MultiByteToWideChar(CP_UTF8, 0, name, -1, 0, 0);
					std::wstring wideName(wideLength, L'\0');

					::MultiByteToWideChar(CP_UTF8, 0, name, -1, wideName.data(), (int)wideName.length());

					setNameFc(std::bit_cast<HANDLE>(t.native_handle()), wideName.c_str());
				}

			#elif defined(__USE_GNU)

				pthread_setname_np(t.native_handle(), name);

			#endif
		}
	}

	ThreadBase::~ThreadBase()
	{
		if(mThread.joinable())
			mThread.join();
	}

	boost::signals2::connection ThreadBase::listen(const ThreadEvent ev, std::function<void()> fc)
	{
		return mEvts[(int)ev].connect(std::move(fc));
	}

	void ThreadBase::name(std::string n)
	{
		mName = std::move(n);
		set_thread_name(mThread, mName.c_str());
	}

	void ThreadBase::priority(EnumThreadPriority p)
	{
		set_thread_priority(mThread, p);
		mPriority = p;
	}

	void ThreadBase::start()
	{
		if(mPaused)
		{
			mRequests.request(ThreadRequest::Unpause);
		}
		else
		{
			if(!mActive && ready_to_start())
			{
				if(mThread.joinable())
					mThread.join();

				mWaitCount = 0;
				mActive = true;

				mRequests.enabled(true);

				mThread = std::thread{[this] { this->process(); }};
				set_thread_priority(mThread, mPriority);
				set_thread_name(mThread, mName.c_str());
			}
		}
	}

	void ThreadBase::end()
	{
		mRequests.request(ThreadRequest::End);
	}

	void ThreadBase::pause()
	{
		mRequests.request(ThreadRequest::Pause);
	}

	void ThreadBase::restart()
	{
		mRequests.request(ThreadRequest::Restart);
	}

	void ThreadBase::on_end()
	{
		trigger_event(ThreadEvent::End);
	}

	void ThreadBase::on_pause()
	{
		trigger_event(ThreadEvent::Pause);
	}

	void ThreadBase::on_pause_end()
	{
		trigger_event(ThreadEvent::PauseEnd);
	}

	void ThreadBase::on_start()
	{
		trigger_event(ThreadEvent::Start);
	}

	void ThreadBase::on_restart()
	{
		mWaitCount = 0;

		trigger_event(ThreadEvent::Restart);
	}

	void ThreadBase::join()
	{
		if(mThread.joinable())
			mThread.join();
	}

	bool ThreadBase::process_events()
	{
		if(mRequests.handle(ThreadRequest::Restart))
			on_restart();

		if(mRequests.requested(ThreadRequest::Pause) && prepare_to_pause())
		{
			mRequests.clear();

			mPaused = true;
			on_pause();

			mRequests.wait_request([this]{ return !mPaused; });

			mPaused = false;

			if(mRequests.requested(ThreadRequest::End) && prepare_to_end())
				return false;

			on_pause_end();
		}

		mRequests.handle(ThreadRequest::Unpause);

		if(waiting())
		{
			++mWaitCount;

			if(!mBusyWait)
				sleep_for(1ms);
		}

		if(mRequests.requested(ThreadRequest::End) && prepare_to_end())
			return false;

		return true;
	}

	void ThreadBase::process_loop()
	{
		work_loop([this]{ return do_iteration(); });
	}

	void ThreadBase::process()
	{
		on_start();

		process_loop();

		mRequests.enabled(false);
		mActive = false;
		mPaused = false;
		mWaiting = false;

		on_end();
	}


}
